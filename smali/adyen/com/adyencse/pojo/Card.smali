.class public Ladyen/com/adyencse/pojo/Card;
.super Ljava/lang/Object;
.source "Card.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ladyen/com/adyencse/pojo/Card$Builder;
    }
.end annotation


# static fields
.field private static final GENERATION_DATE_FORMAT:Ljava/text/SimpleDateFormat;

.field private static final tag:Ljava/lang/String; = "Card"


# instance fields
.field private cardHolderName:Ljava/lang/String;

.field private cvc:Ljava/lang/String;

.field private expiryMonth:Ljava/lang/String;

.field private expiryYear:Ljava/lang/String;

.field private generationTime:Ljava/util/Date;

.field private number:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 22
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Ladyen/com/adyencse/pojo/Card;->GENERATION_DATE_FORMAT:Ljava/text/SimpleDateFormat;

    .line 32
    sget-object v0, Ladyen/com/adyencse/pojo/Card;->GENERATION_DATE_FORMAT:Ljava/text/SimpleDateFormat;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ladyen/com/adyencse/pojo/Card;)Ljava/util/Date;
    .locals 0

    .line 19
    iget-object p0, p0, Ladyen/com/adyencse/pojo/Card;->generationTime:Ljava/util/Date;

    return-object p0
.end method

.method static synthetic access$002(Ladyen/com/adyencse/pojo/Card;Ljava/util/Date;)Ljava/util/Date;
    .locals 0

    .line 19
    iput-object p1, p0, Ladyen/com/adyencse/pojo/Card;->generationTime:Ljava/util/Date;

    return-object p1
.end method

.method static synthetic access$100(Ladyen/com/adyencse/pojo/Card;)Ljava/lang/String;
    .locals 0

    .line 19
    iget-object p0, p0, Ladyen/com/adyencse/pojo/Card;->number:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$102(Ladyen/com/adyencse/pojo/Card;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 19
    iput-object p1, p0, Ladyen/com/adyencse/pojo/Card;->number:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Ladyen/com/adyencse/pojo/Card;)Ljava/lang/String;
    .locals 0

    .line 19
    iget-object p0, p0, Ladyen/com/adyencse/pojo/Card;->cardHolderName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$202(Ladyen/com/adyencse/pojo/Card;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 19
    iput-object p1, p0, Ladyen/com/adyencse/pojo/Card;->cardHolderName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Ladyen/com/adyencse/pojo/Card;)Ljava/lang/String;
    .locals 0

    .line 19
    iget-object p0, p0, Ladyen/com/adyencse/pojo/Card;->cvc:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$302(Ladyen/com/adyencse/pojo/Card;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 19
    iput-object p1, p0, Ladyen/com/adyencse/pojo/Card;->cvc:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Ladyen/com/adyencse/pojo/Card;)Ljava/lang/String;
    .locals 0

    .line 19
    iget-object p0, p0, Ladyen/com/adyencse/pojo/Card;->expiryMonth:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$402(Ladyen/com/adyencse/pojo/Card;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 19
    iput-object p1, p0, Ladyen/com/adyencse/pojo/Card;->expiryMonth:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Ladyen/com/adyencse/pojo/Card;)Ljava/lang/String;
    .locals 0

    .line 19
    iget-object p0, p0, Ladyen/com/adyencse/pojo/Card;->expiryYear:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$502(Ladyen/com/adyencse/pojo/Card;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 19
    iput-object p1, p0, Ladyen/com/adyencse/pojo/Card;->expiryYear:Ljava/lang/String;

    return-object p1
.end method

.method private encryptData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ladyen/com/adyencse/encrypter/exception/EncrypterException;
        }
    .end annotation

    .line 164
    :try_start_0
    new-instance v0, Ladyen/com/adyencse/encrypter/ClientSideEncrypter;

    invoke-direct {v0, p2}, Ladyen/com/adyencse/encrypter/ClientSideEncrypter;-><init>(Ljava/lang/String;)V

    .line 165
    invoke-virtual {v0, p1}, Ladyen/com/adyencse/encrypter/ClientSideEncrypter;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ladyen/com/adyencse/encrypter/exception/EncrypterException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 167
    throw p1
.end method

.method private getLastFourDigitsFromCardNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p1, :cond_0

    .line 138
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 139
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x4

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, ""

    return-object p1
.end method

.method private getMaskingChars(I)Ljava/lang/String;
    .locals 2

    add-int/lit8 p1, p1, -0x4

    if-gtz p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 149
    :cond_0
    new-array v0, p1, [C

    :goto_0
    if-lez p1, :cond_1

    add-int/lit8 p1, p1, -0x1

    const/16 v1, 0x2a

    .line 152
    aput-char v1, v0, p1

    goto :goto_0

    .line 154
    :cond_1
    new-instance p1, Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/String;-><init>([C)V

    return-object p1
.end method


# virtual methods
.method public getCardHolderName()Ljava/lang/String;
    .locals 1

    .line 71
    iget-object v0, p0, Ladyen/com/adyencse/pojo/Card;->cardHolderName:Ljava/lang/String;

    return-object v0
.end method

.method public getCvc()Ljava/lang/String;
    .locals 1

    .line 80
    iget-object v0, p0, Ladyen/com/adyencse/pojo/Card;->cvc:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiryMonth()Ljava/lang/String;
    .locals 1

    .line 53
    iget-object v0, p0, Ladyen/com/adyencse/pojo/Card;->expiryMonth:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiryYear()Ljava/lang/String;
    .locals 1

    .line 62
    iget-object v0, p0, Ladyen/com/adyencse/pojo/Card;->expiryYear:Ljava/lang/String;

    return-object v0
.end method

.method public getGenerationTime()Ljava/util/Date;
    .locals 1

    .line 89
    iget-object v0, p0, Ladyen/com/adyencse/pojo/Card;->generationTime:Ljava/util/Date;

    return-object v0
.end method

.method public getNumber()Ljava/lang/String;
    .locals 1

    .line 44
    iget-object v0, p0, Ladyen/com/adyencse/pojo/Card;->number:Ljava/lang/String;

    return-object v0
.end method

.method public serialize(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ladyen/com/adyencse/encrypter/exception/EncrypterException;
        }
    .end annotation

    .line 105
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "generationtime"

    .line 109
    sget-object v2, Ladyen/com/adyencse/pojo/Card;->GENERATION_DATE_FORMAT:Ljava/text/SimpleDateFormat;

    iget-object v3, p0, Ladyen/com/adyencse/pojo/Card;->generationTime:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "number"

    .line 110
    iget-object v2, p0, Ladyen/com/adyencse/pojo/Card;->number:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "holderName"

    .line 111
    iget-object v2, p0, Ladyen/com/adyencse/pojo/Card;->cardHolderName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "cvc"

    .line 112
    iget-object v2, p0, Ladyen/com/adyencse/pojo/Card;->cvc:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "expiryMonth"

    .line 113
    iget-object v2, p0, Ladyen/com/adyencse/pojo/Card;->expiryMonth:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "expiryYear"

    .line 114
    iget-object v2, p0, Ladyen/com/adyencse/pojo/Card;->expiryYear:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 116
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Ladyen/com/adyencse/pojo/Card;->encryptData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 118
    sget-object v0, Ladyen/com/adyencse/pojo/Card;->tag:Ljava/lang/String;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public setCardHolderName(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 76
    iput-object p1, p0, Ladyen/com/adyencse/pojo/Card;->cardHolderName:Ljava/lang/String;

    return-void
.end method

.method public setCvc(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 85
    iput-object p1, p0, Ladyen/com/adyencse/pojo/Card;->cvc:Ljava/lang/String;

    return-void
.end method

.method public setExpiryMonth(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 58
    iput-object p1, p0, Ladyen/com/adyencse/pojo/Card;->expiryMonth:Ljava/lang/String;

    return-void
.end method

.method public setExpiryYear(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 67
    iput-object p1, p0, Ladyen/com/adyencse/pojo/Card;->expiryYear:Ljava/lang/String;

    return-void
.end method

.method public setGenerationTime(Ljava/util/Date;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 94
    iput-object p1, p0, Ladyen/com/adyencse/pojo/Card;->generationTime:Ljava/util/Date;

    return-void
.end method

.method public setNumber(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 49
    iput-object p1, p0, Ladyen/com/adyencse/pojo/Card;->number:Ljava/lang/String;

    return-void
.end method

.method public toMaskedCardNumber()Ljava/lang/String;
    .locals 2

    .line 128
    iget-object v0, p0, Ladyen/com/adyencse/pojo/Card;->number:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    goto :goto_0

    .line 131
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Ladyen/com/adyencse/pojo/Card;->number:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 133
    iget-object v1, p0, Ladyen/com/adyencse/pojo/Card;->number:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {p0, v1}, Ladyen/com/adyencse/pojo/Card;->getMaskingChars(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ladyen/com/adyencse/pojo/Card;->number:Ljava/lang/String;

    invoke-direct {p0, v1}, Ladyen/com/adyencse/pojo/Card;->getLastFourDigitsFromCardNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const-string v0, ""

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 175
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "generationtime"

    .line 178
    sget-object v2, Ladyen/com/adyencse/pojo/Card;->GENERATION_DATE_FORMAT:Ljava/text/SimpleDateFormat;

    iget-object v3, p0, Ladyen/com/adyencse/pojo/Card;->generationTime:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 179
    iget-object v1, p0, Ladyen/com/adyencse/pojo/Card;->number:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x4

    if-lt v1, v2, :cond_0

    const-string v1, "number"

    .line 180
    iget-object v2, p0, Ladyen/com/adyencse/pojo/Card;->number:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    const-string v1, "holderName"

    .line 182
    iget-object v2, p0, Ladyen/com/adyencse/pojo/Card;->cardHolderName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 184
    sget-object v2, Ladyen/com/adyencse/pojo/Card;->tag:Ljava/lang/String;

    invoke-virtual {v1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 187
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
