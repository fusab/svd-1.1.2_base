.class public Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;
.super Lcom/facebook/react/bridge/ReactContextBaseJavaModule;
.source "RNLocalNotificationsModule.java"


# instance fields
.field alarmManager:Landroid/app/AlarmManager;

.field largeIconName:Ljava/lang/String;

.field largeIconType:Ljava/lang/String;

.field reactContext:Lcom/facebook/react/bridge/ReactApplicationContext;

.field smallIconName:Ljava/lang/String;

.field smallIconType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V
    .locals 1

    .line 28
    invoke-direct {p0, p1}, Lcom/facebook/react/bridge/ReactContextBaseJavaModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    const-string v0, "ic_launcher"

    .line 22
    iput-object v0, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->largeIconName:Ljava/lang/String;

    const-string v0, "mipmap"

    .line 23
    iput-object v0, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->largeIconType:Ljava/lang/String;

    const-string v0, "notification_small"

    .line 24
    iput-object v0, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->smallIconName:Ljava/lang/String;

    const-string v0, "drawable"

    .line 25
    iput-object v0, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->smallIconType:Ljava/lang/String;

    .line 29
    iput-object p1, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->reactContext:Lcom/facebook/react/bridge/ReactApplicationContext;

    const-string v0, "alarm"

    .line 30
    invoke-virtual {p1, v0}, Lcom/facebook/react/bridge/ReactApplicationContext;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/AlarmManager;

    iput-object p1, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->alarmManager:Landroid/app/AlarmManager;

    return-void
.end method


# virtual methods
.method public createAlarm(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 4

    if-eqz p5, :cond_0

    .line 63
    invoke-virtual {p0, p1}, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->deleteAlarm(Ljava/lang/Integer;)V

    .line 66
    :cond_0
    new-instance p5, Ljava/text/SimpleDateFormat;

    const-string/jumbo v0, "yyyy-MM-dd kk:mm"

    invoke-direct {p5, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 69
    :try_start_0
    invoke-virtual {p5, p3}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p5

    .line 72
    invoke-virtual {p5}, Ljava/text/ParseException;->printStackTrace()V

    .line 74
    :goto_0
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p5

    .line 76
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->reactContext:Lcom/facebook/react/bridge/ReactApplicationContext;

    const-class v2, Lcom/github/wumke/RNLocalNotifications/AlarmReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.github.wumke.RNLocalNotifications.showAlarm"

    .line 77
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "id"

    .line 78
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "text"

    .line 79
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p2, "datetime"

    .line 80
    invoke-virtual {v0, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p2, "sound"

    .line 81
    invoke-virtual {v0, p2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p2, "hiddendata"

    .line 82
    invoke-virtual {v0, p2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    iget-object p2, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->largeIconName:Ljava/lang/String;

    const-string p3, "largeIconName"

    invoke-virtual {v0, p3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 84
    iget-object p2, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->largeIconType:Ljava/lang/String;

    const-string p3, "largeIconType"

    invoke-virtual {v0, p3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    iget-object p2, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->smallIconName:Ljava/lang/String;

    const-string p3, "smallIconName"

    invoke-virtual {v0, p3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    iget-object p2, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->smallIconType:Ljava/lang/String;

    const-string p3, "smallIconType"

    invoke-virtual {v0, p3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    iget-object p2, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->reactContext:Lcom/facebook/react/bridge/ReactApplicationContext;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 p3, 0x0

    invoke-static {p2, p1, v0, p3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p1

    .line 90
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object p2

    .line 91
    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    cmp-long p2, v0, v2

    if-lez p2, :cond_4

    .line 92
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p4, 0x17

    if-lt p2, p4, :cond_1

    .line 93
    iget-object p2, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->alarmManager:Landroid/app/AlarmManager;

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide p4

    invoke-virtual {p2, p3, p4, p5, p1}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    goto :goto_1

    .line 95
    :cond_1
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p4, 0x15

    if-lt p2, p4, :cond_2

    .line 96
    new-instance p2, Landroid/app/AlarmManager$AlarmClockInfo;

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide p3

    invoke-direct {p2, p3, p4, p1}, Landroid/app/AlarmManager$AlarmClockInfo;-><init>(JLandroid/app/PendingIntent;)V

    .line 97
    iget-object p3, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->alarmManager:Landroid/app/AlarmManager;

    invoke-virtual {p3, p2, p1}, Landroid/app/AlarmManager;->setAlarmClock(Landroid/app/AlarmManager$AlarmClockInfo;Landroid/app/PendingIntent;)V

    goto :goto_1

    .line 100
    :cond_2
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p4, 0x13

    if-lt p2, p4, :cond_3

    .line 101
    iget-object p2, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->alarmManager:Landroid/app/AlarmManager;

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide p4

    invoke-virtual {p2, p3, p4, p5, p1}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    goto :goto_1

    .line 104
    :cond_3
    iget-object p2, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->alarmManager:Landroid/app/AlarmManager;

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide p4

    invoke-virtual {p2, p3, p4, p5, p1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    :cond_4
    :goto_1
    return-void
.end method

.method public createNotification(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    .line 40
    invoke-virtual/range {v0 .. v6}, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->createAlarm(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    return-void
.end method

.method public deleteAlarm(Ljava/lang/Integer;)V
    .locals 3

    .line 110
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->reactContext:Lcom/facebook/react/bridge/ReactApplicationContext;

    const-class v2, Lcom/github/wumke/RNLocalNotifications/AlarmReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.github.wumke.RNLocalNotifications.showAlarm"

    .line 111
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    iget-object v1, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->reactContext:Lcom/facebook/react/bridge/ReactApplicationContext;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/high16 v2, 0x20000000

    invoke-static {v1, p1, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 116
    invoke-virtual {p1}, Landroid/app/PendingIntent;->cancel()V

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->alarmManager:Landroid/app/AlarmManager;

    invoke-virtual {v0, p1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public deleteNotification(Ljava/lang/Integer;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 45
    invoke-virtual {p0, p1}, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->deleteAlarm(Ljava/lang/Integer;)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "RNLocalNotifications"

    return-object v0
.end method

.method public setAndroidIcons(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 55
    iput-object p1, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->largeIconName:Ljava/lang/String;

    .line 56
    iput-object p2, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->largeIconType:Ljava/lang/String;

    .line 57
    iput-object p3, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->smallIconName:Ljava/lang/String;

    .line 58
    iput-object p4, p0, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->smallIconType:Ljava/lang/String;

    return-void
.end method

.method public updateNotification(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    .line 50
    invoke-virtual/range {v0 .. v6}, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsModule;->createAlarm(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    return-void
.end method
