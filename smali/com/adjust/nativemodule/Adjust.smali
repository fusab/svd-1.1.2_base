.class public Lcom/adjust/nativemodule/Adjust;
.super Lcom/facebook/react/bridge/ReactContextBaseJavaModule;
.source "Adjust.java"

# interfaces
.implements Lcom/facebook/react/bridge/LifecycleEventListener;
.implements Lcom/adjust/sdk/OnAttributionChangedListener;
.implements Lcom/adjust/sdk/OnEventTrackingSucceededListener;
.implements Lcom/adjust/sdk/OnEventTrackingFailedListener;
.implements Lcom/adjust/sdk/OnSessionTrackingSucceededListener;
.implements Lcom/adjust/sdk/OnSessionTrackingFailedListener;
.implements Lcom/adjust/sdk/OnDeeplinkResponseListener;


# static fields
.field private static TAG:Ljava/lang/String; = "AdjustBridge"


# instance fields
.field private attributionCallback:Z

.field private deferredDeeplinkCallback:Z

.field private eventTrackingFailedCallback:Z

.field private eventTrackingSucceededCallback:Z

.field private sessionTrackingFailedCallback:Z

.field private sessionTrackingSucceededCallback:Z

.field private shouldLaunchDeeplink:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/facebook/react/bridge/ReactContextBaseJavaModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    const/4 p1, 0x1

    .line 37
    iput-boolean p1, p0, Lcom/adjust/nativemodule/Adjust;->shouldLaunchDeeplink:Z

    return-void
.end method

.method private checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z
    .locals 1

    .line 615
    invoke-interface {p1, p2}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, p2}, Lcom/facebook/react/bridge/ReadableMap;->isNull(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private sendEvent(Lcom/facebook/react/bridge/ReactContext;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V
    .locals 1
    .param p3    # Lcom/facebook/react/bridge/WritableMap;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .line 609
    const-class v0, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    .line 610
    invoke-virtual {p1, v0}, Lcom/facebook/react/bridge/ReactContext;->getJSModule(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object p1

    check-cast p1, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    .line 611
    invoke-interface {p1, p2, p3}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public addSessionCallbackParameter(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 399
    invoke-static {p1, p2}, Lcom/adjust/sdk/Adjust;->addSessionCallbackParameter(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public addSessionPartnerParameter(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 404
    invoke-static {p1, p2}, Lcom/adjust/sdk/Adjust;->addSessionPartnerParameter(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public appWillOpenUrl(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 378
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 379
    invoke-virtual {p0}, Lcom/adjust/nativemodule/Adjust;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/adjust/sdk/Adjust;->appWillOpenUrl(Landroid/net/Uri;Landroid/content/Context;)V

    return-void
.end method

.method public convertUniversalLink(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Callback;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    const/4 p1, 0x1

    .line 474
    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    const-string v0, ""

    aput-object v0, p1, p2

    invoke-interface {p3, p1}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V

    return-void
.end method

.method public create(Lcom/facebook/react/bridge/ReadableMap;)V
    .locals 19
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const/4 v2, 0x0

    const-string v3, "logLevel"

    .line 124
    invoke-direct {v0, v1, v3}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v4

    const-string v5, "SUPPRESS"

    if-eqz v4, :cond_1

    .line 125
    invoke-interface {v1, v3}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 126
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v2, 0x1

    :cond_1
    const-string v4, "appToken"

    .line 132
    invoke-direct {v0, v1, v4}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v6

    const/4 v7, 0x0

    if-eqz v6, :cond_2

    .line 133
    invoke-interface {v1, v4}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_2
    move-object v4, v7

    :goto_0
    const-string v6, "environment"

    .line 137
    invoke-direct {v0, v1, v6}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 138
    invoke-interface {v1, v6}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 141
    :cond_3
    new-instance v6, Lcom/adjust/sdk/AdjustConfig;

    invoke-virtual/range {p0 .. p0}, Lcom/adjust/nativemodule/Adjust;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v8

    invoke-direct {v6, v8, v4, v7, v2}, Lcom/adjust/sdk/AdjustConfig;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 142
    invoke-virtual {v6}, Lcom/adjust/sdk/AdjustConfig;->isValid()Z

    move-result v2

    if-nez v2, :cond_4

    return-void

    .line 147
    :cond_4
    invoke-direct {v0, v1, v3}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 148
    invoke-interface {v1, v3}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "VERBOSE"

    .line 149
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 150
    sget-object v2, Lcom/adjust/sdk/LogLevel;->VERBOSE:Lcom/adjust/sdk/LogLevel;

    invoke-virtual {v6, v2}, Lcom/adjust/sdk/AdjustConfig;->setLogLevel(Lcom/adjust/sdk/LogLevel;)V

    goto :goto_1

    :cond_5
    const-string v3, "DEBUG"

    .line 151
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 152
    sget-object v2, Lcom/adjust/sdk/LogLevel;->DEBUG:Lcom/adjust/sdk/LogLevel;

    invoke-virtual {v6, v2}, Lcom/adjust/sdk/AdjustConfig;->setLogLevel(Lcom/adjust/sdk/LogLevel;)V

    goto :goto_1

    :cond_6
    const-string v3, "INFO"

    .line 153
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 154
    sget-object v2, Lcom/adjust/sdk/LogLevel;->INFO:Lcom/adjust/sdk/LogLevel;

    invoke-virtual {v6, v2}, Lcom/adjust/sdk/AdjustConfig;->setLogLevel(Lcom/adjust/sdk/LogLevel;)V

    goto :goto_1

    :cond_7
    const-string v3, "WARN"

    .line 155
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 156
    sget-object v2, Lcom/adjust/sdk/LogLevel;->WARN:Lcom/adjust/sdk/LogLevel;

    invoke-virtual {v6, v2}, Lcom/adjust/sdk/AdjustConfig;->setLogLevel(Lcom/adjust/sdk/LogLevel;)V

    goto :goto_1

    :cond_8
    const-string v3, "ERROR"

    .line 157
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 158
    sget-object v2, Lcom/adjust/sdk/LogLevel;->ERROR:Lcom/adjust/sdk/LogLevel;

    invoke-virtual {v6, v2}, Lcom/adjust/sdk/AdjustConfig;->setLogLevel(Lcom/adjust/sdk/LogLevel;)V

    goto :goto_1

    :cond_9
    const-string v3, "ASSERT"

    .line 159
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 160
    sget-object v2, Lcom/adjust/sdk/LogLevel;->ASSERT:Lcom/adjust/sdk/LogLevel;

    invoke-virtual {v6, v2}, Lcom/adjust/sdk/AdjustConfig;->setLogLevel(Lcom/adjust/sdk/LogLevel;)V

    goto :goto_1

    .line 161
    :cond_a
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 162
    sget-object v2, Lcom/adjust/sdk/LogLevel;->SUPRESS:Lcom/adjust/sdk/LogLevel;

    invoke-virtual {v6, v2}, Lcom/adjust/sdk/AdjustConfig;->setLogLevel(Lcom/adjust/sdk/LogLevel;)V

    goto :goto_1

    .line 164
    :cond_b
    sget-object v2, Lcom/adjust/sdk/LogLevel;->INFO:Lcom/adjust/sdk/LogLevel;

    invoke-virtual {v6, v2}, Lcom/adjust/sdk/AdjustConfig;->setLogLevel(Lcom/adjust/sdk/LogLevel;)V

    :cond_c
    :goto_1
    const-string v2, "eventBufferingEnabled"

    .line 169
    invoke-direct {v0, v1, v2}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 170
    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 171
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/adjust/sdk/AdjustConfig;->setEventBufferingEnabled(Ljava/lang/Boolean;)V

    :cond_d
    const-string v2, "sdkPrefix"

    .line 175
    invoke-direct {v0, v1, v2}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 176
    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 177
    invoke-virtual {v6, v2}, Lcom/adjust/sdk/AdjustConfig;->setSdkPrefix(Ljava/lang/String;)V

    :cond_e
    const-string v2, "processName"

    .line 181
    invoke-direct {v0, v1, v2}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 182
    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 183
    invoke-virtual {v6, v2}, Lcom/adjust/sdk/AdjustConfig;->setProcessName(Ljava/lang/String;)V

    :cond_f
    const-string v2, "defaultTracker"

    .line 187
    invoke-direct {v0, v1, v2}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 188
    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 189
    invoke-virtual {v6, v2}, Lcom/adjust/sdk/AdjustConfig;->setDefaultTracker(Ljava/lang/String;)V

    :cond_10
    const-string/jumbo v2, "userAgent"

    .line 193
    invoke-direct {v0, v1, v2}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 194
    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 195
    invoke-virtual {v6, v2}, Lcom/adjust/sdk/AdjustConfig;->setUserAgent(Ljava/lang/String;)V

    :cond_11
    const-string v2, "secretId"

    .line 199
    invoke-direct {v0, v1, v2}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    const-string v3, "info1"

    .line 200
    invoke-direct {v0, v1, v3}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    const-string v4, "info2"

    .line 201
    invoke-direct {v0, v1, v4}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    const-string v4, "info3"

    .line 202
    invoke-direct {v0, v1, v4}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    const-string v4, "info4"

    .line 203
    invoke-direct {v0, v1, v4}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 205
    :try_start_0
    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0xa

    invoke-static {v2, v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v9

    .line 206
    invoke-interface {v1, v3}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v11

    const-string v2, "info2"

    .line 207
    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v13

    const-string v2, "info3"

    .line 208
    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v15

    const-string v2, "info4"

    .line 209
    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v17

    move-object v8, v6

    .line 210
    invoke-virtual/range {v8 .. v18}, Lcom/adjust/sdk/AdjustConfig;->setAppSecret(JJJJJ)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    nop

    :cond_12
    :goto_2
    const-string v2, "sendInBackground"

    .line 215
    invoke-direct {v0, v1, v2}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 216
    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 217
    invoke-virtual {v6, v2}, Lcom/adjust/sdk/AdjustConfig;->setSendInBackground(Z)V

    :cond_13
    const-string v2, "isDeviceKnown"

    .line 221
    invoke-direct {v0, v1, v2}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 222
    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 223
    invoke-virtual {v6, v2}, Lcom/adjust/sdk/AdjustConfig;->setDeviceKnown(Z)V

    :cond_14
    const-string v2, "shouldLaunchDeeplink"

    .line 234
    invoke-direct {v0, v1, v2}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    const-string v2, "shouldLaunchDeeplink"

    .line 235
    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 236
    iput-boolean v2, v0, Lcom/adjust/nativemodule/Adjust;->shouldLaunchDeeplink:Z

    :cond_15
    const-string v2, "delayStart"

    .line 240
    invoke-direct {v0, v1, v2}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    const-string v2, "delayStart"

    .line 241
    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    .line 242
    invoke-virtual {v6, v1, v2}, Lcom/adjust/sdk/AdjustConfig;->setDelayStart(D)V

    .line 246
    :cond_16
    iget-boolean v1, v0, Lcom/adjust/nativemodule/Adjust;->attributionCallback:Z

    if-eqz v1, :cond_17

    .line 247
    invoke-virtual {v6, v0}, Lcom/adjust/sdk/AdjustConfig;->setOnAttributionChangedListener(Lcom/adjust/sdk/OnAttributionChangedListener;)V

    .line 251
    :cond_17
    iget-boolean v1, v0, Lcom/adjust/nativemodule/Adjust;->eventTrackingSucceededCallback:Z

    if-eqz v1, :cond_18

    .line 252
    invoke-virtual {v6, v0}, Lcom/adjust/sdk/AdjustConfig;->setOnEventTrackingSucceededListener(Lcom/adjust/sdk/OnEventTrackingSucceededListener;)V

    .line 256
    :cond_18
    iget-boolean v1, v0, Lcom/adjust/nativemodule/Adjust;->eventTrackingFailedCallback:Z

    if-eqz v1, :cond_19

    .line 257
    invoke-virtual {v6, v0}, Lcom/adjust/sdk/AdjustConfig;->setOnEventTrackingFailedListener(Lcom/adjust/sdk/OnEventTrackingFailedListener;)V

    .line 261
    :cond_19
    iget-boolean v1, v0, Lcom/adjust/nativemodule/Adjust;->sessionTrackingSucceededCallback:Z

    if-eqz v1, :cond_1a

    .line 262
    invoke-virtual {v6, v0}, Lcom/adjust/sdk/AdjustConfig;->setOnSessionTrackingSucceededListener(Lcom/adjust/sdk/OnSessionTrackingSucceededListener;)V

    .line 266
    :cond_1a
    iget-boolean v1, v0, Lcom/adjust/nativemodule/Adjust;->sessionTrackingFailedCallback:Z

    if-eqz v1, :cond_1b

    .line 267
    invoke-virtual {v6, v0}, Lcom/adjust/sdk/AdjustConfig;->setOnSessionTrackingFailedListener(Lcom/adjust/sdk/OnSessionTrackingFailedListener;)V

    .line 271
    :cond_1b
    iget-boolean v1, v0, Lcom/adjust/nativemodule/Adjust;->deferredDeeplinkCallback:Z

    if-eqz v1, :cond_1c

    .line 272
    invoke-virtual {v6, v0}, Lcom/adjust/sdk/AdjustConfig;->setOnDeeplinkResponseListener(Lcom/adjust/sdk/OnDeeplinkResponseListener;)V

    .line 275
    :cond_1c
    invoke-static {v6}, Lcom/adjust/sdk/Adjust;->onCreate(Lcom/adjust/sdk/AdjustConfig;)V

    .line 276
    invoke-static {}, Lcom/adjust/sdk/Adjust;->onResume()V

    return-void
.end method

.method public gdprForgetMe()V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 429
    invoke-virtual {p0}, Lcom/adjust/nativemodule/Adjust;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    invoke-static {v0}, Lcom/adjust/sdk/Adjust;->gdprForgetMe(Landroid/content/Context;)V

    return-void
.end method

.method public getAdid(Lcom/facebook/react/bridge/Callback;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    const/4 v0, 0x1

    .line 449
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {}, Lcom/adjust/sdk/Adjust;->getAdid()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V

    return-void
.end method

.method public getAmazonAdId(Lcom/facebook/react/bridge/Callback;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    const/4 v0, 0x1

    .line 454
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/adjust/nativemodule/Adjust;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v1

    invoke-static {v1}, Lcom/adjust/sdk/Adjust;->getAmazonAdId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V

    return-void
.end method

.method public getAttribution(Lcom/facebook/react/bridge/Callback;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    const/4 v0, 0x1

    .line 459
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {}, Lcom/adjust/sdk/Adjust;->getAttribution()Lcom/adjust/sdk/AdjustAttribution;

    move-result-object v1

    invoke-static {v1}, Lcom/adjust/nativemodule/AdjustUtil;->attributionToMap(Lcom/adjust/sdk/AdjustAttribution;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V

    return-void
.end method

.method public getGoogleAdId(Lcom/facebook/react/bridge/Callback;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 439
    invoke-virtual {p0}, Lcom/adjust/nativemodule/Adjust;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    new-instance v1, Lcom/adjust/nativemodule/Adjust$1;

    invoke-direct {v1, p0, p1}, Lcom/adjust/nativemodule/Adjust$1;-><init>(Lcom/adjust/nativemodule/Adjust;Lcom/facebook/react/bridge/Callback;)V

    invoke-static {v0, v1}, Lcom/adjust/sdk/Adjust;->getGoogleAdId(Landroid/content/Context;Lcom/adjust/sdk/OnDeviceIdsRead;)V

    return-void
.end method

.method public getIdfa(Lcom/facebook/react/bridge/Callback;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    const/4 v0, 0x1

    .line 434
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v0, v1

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "Adjust"

    return-object v0
.end method

.method public getSdkVersion(Ljava/lang/String;Lcom/facebook/react/bridge/Callback;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 464
    invoke-static {}, Lcom/adjust/sdk/Adjust;->getSdkVersion()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    .line 466
    new-array p1, v2, [Ljava/lang/Object;

    const-string v0, ""

    aput-object v0, p1, v1

    invoke-interface {p2, p1}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V

    goto :goto_0

    .line 468
    :cond_0
    new-array v2, v2, [Ljava/lang/Object;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "@"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v1

    invoke-interface {p2, v2}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public initialize()V
    .locals 1

    .line 50
    invoke-virtual {p0}, Lcom/adjust/nativemodule/Adjust;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/facebook/react/bridge/ReactApplicationContext;->addLifecycleEventListener(Lcom/facebook/react/bridge/LifecycleEventListener;)V

    return-void
.end method

.method public isEnabled(Lcom/facebook/react/bridge/Callback;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    const/4 v0, 0x1

    .line 358
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {}, Lcom/adjust/sdk/Adjust;->isEnabled()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V

    return-void
.end method

.method public launchReceivedDeeplink(Landroid/net/Uri;)Z
    .locals 2

    .line 93
    invoke-virtual {p0}, Lcom/adjust/nativemodule/Adjust;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    invoke-static {p1}, Lcom/adjust/nativemodule/AdjustUtil;->deferredDeeplinkToMap(Landroid/net/Uri;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object p1

    const-string v1, "adjust_deferredDeeplink"

    invoke-direct {p0, v0, v1, p1}, Lcom/adjust/nativemodule/Adjust;->sendEvent(Lcom/facebook/react/bridge/ReactContext;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    .line 94
    iget-boolean p1, p0, Lcom/adjust/nativemodule/Adjust;->shouldLaunchDeeplink:Z

    return p1
.end method

.method public onAttributionChanged(Lcom/adjust/sdk/AdjustAttribution;)V
    .locals 2

    .line 68
    invoke-virtual {p0}, Lcom/adjust/nativemodule/Adjust;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    invoke-static {p1}, Lcom/adjust/nativemodule/AdjustUtil;->attributionToMap(Lcom/adjust/sdk/AdjustAttribution;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object p1

    const-string v1, "adjust_attribution"

    invoke-direct {p0, v0, v1, p1}, Lcom/adjust/nativemodule/Adjust;->sendEvent(Lcom/facebook/react/bridge/ReactContext;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    return-void
.end method

.method public onFinishedEventTrackingFailed(Lcom/adjust/sdk/AdjustEventFailure;)V
    .locals 2

    .line 78
    invoke-virtual {p0}, Lcom/adjust/nativemodule/Adjust;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    invoke-static {p1}, Lcom/adjust/nativemodule/AdjustUtil;->eventFailureToMap(Lcom/adjust/sdk/AdjustEventFailure;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object p1

    const-string v1, "adjust_eventTrackingFailed"

    invoke-direct {p0, v0, v1, p1}, Lcom/adjust/nativemodule/Adjust;->sendEvent(Lcom/facebook/react/bridge/ReactContext;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    return-void
.end method

.method public onFinishedEventTrackingSucceeded(Lcom/adjust/sdk/AdjustEventSuccess;)V
    .locals 2

    .line 73
    invoke-virtual {p0}, Lcom/adjust/nativemodule/Adjust;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    invoke-static {p1}, Lcom/adjust/nativemodule/AdjustUtil;->eventSuccessToMap(Lcom/adjust/sdk/AdjustEventSuccess;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object p1

    const-string v1, "adjust_eventTrackingSucceeded"

    invoke-direct {p0, v0, v1, p1}, Lcom/adjust/nativemodule/Adjust;->sendEvent(Lcom/facebook/react/bridge/ReactContext;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    return-void
.end method

.method public onFinishedSessionTrackingFailed(Lcom/adjust/sdk/AdjustSessionFailure;)V
    .locals 2

    .line 88
    invoke-virtual {p0}, Lcom/adjust/nativemodule/Adjust;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    invoke-static {p1}, Lcom/adjust/nativemodule/AdjustUtil;->sessionFailureToMap(Lcom/adjust/sdk/AdjustSessionFailure;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object p1

    const-string v1, "adjust_sessionTrackingFailed"

    invoke-direct {p0, v0, v1, p1}, Lcom/adjust/nativemodule/Adjust;->sendEvent(Lcom/facebook/react/bridge/ReactContext;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    return-void
.end method

.method public onFinishedSessionTrackingSucceeded(Lcom/adjust/sdk/AdjustSessionSuccess;)V
    .locals 2

    .line 83
    invoke-virtual {p0}, Lcom/adjust/nativemodule/Adjust;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    invoke-static {p1}, Lcom/adjust/nativemodule/AdjustUtil;->sessionSuccessToMap(Lcom/adjust/sdk/AdjustSessionSuccess;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object p1

    const-string v1, "adjust_sessionTrackingSucceeded"

    invoke-direct {p0, v0, v1, p1}, Lcom/adjust/nativemodule/Adjust;->sendEvent(Lcom/facebook/react/bridge/ReactContext;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    return-void
.end method

.method public onHostDestroy()V
    .locals 0

    return-void
.end method

.method public onHostPause()V
    .locals 0

    .line 55
    invoke-static {}, Lcom/adjust/sdk/Adjust;->onPause()V

    return-void
.end method

.method public onHostResume()V
    .locals 0

    .line 60
    invoke-static {}, Lcom/adjust/sdk/Adjust;->onResume()V

    return-void
.end method

.method public onPause()V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 605
    invoke-static {}, Lcom/adjust/sdk/Adjust;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 600
    invoke-static {}, Lcom/adjust/sdk/Adjust;->onResume()V

    return-void
.end method

.method public removeSessionCallbackParameter(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 409
    invoke-static {p1}, Lcom/adjust/sdk/Adjust;->removeSessionCallbackParameter(Ljava/lang/String;)V

    return-void
.end method

.method public removeSessionPartnerParameter(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 414
    invoke-static {p1}, Lcom/adjust/sdk/Adjust;->removeSessionPartnerParameter(Ljava/lang/String;)V

    return-void
.end method

.method public resetSessionCallbackParameters()V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 419
    invoke-static {}, Lcom/adjust/sdk/Adjust;->resetSessionCallbackParameters()V

    return-void
.end method

.method public resetSessionPartnerParameters()V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 424
    invoke-static {}, Lcom/adjust/sdk/Adjust;->resetSessionPartnerParameters()V

    return-void
.end method

.method public sendFirstPackages()V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 384
    invoke-static {}, Lcom/adjust/sdk/Adjust;->sendFirstPackages()V

    return-void
.end method

.method public setAttributionCallbackListener()V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    const/4 v0, 0x1

    .line 479
    iput-boolean v0, p0, Lcom/adjust/nativemodule/Adjust;->attributionCallback:Z

    return-void
.end method

.method public setDeferredDeeplinkCallbackListener()V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    const/4 v0, 0x1

    .line 504
    iput-boolean v0, p0, Lcom/adjust/nativemodule/Adjust;->deferredDeeplinkCallback:Z

    return-void
.end method

.method public setEnabled(Ljava/lang/Boolean;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 353
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-static {p1}, Lcom/adjust/sdk/Adjust;->setEnabled(Z)V

    return-void
.end method

.method public setEventTrackingFailedCallbackListener()V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    const/4 v0, 0x1

    .line 489
    iput-boolean v0, p0, Lcom/adjust/nativemodule/Adjust;->eventTrackingFailedCallback:Z

    return-void
.end method

.method public setEventTrackingSucceededCallbackListener()V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    const/4 v0, 0x1

    .line 484
    iput-boolean v0, p0, Lcom/adjust/nativemodule/Adjust;->eventTrackingSucceededCallback:Z

    return-void
.end method

.method public setOfflineMode(Ljava/lang/Boolean;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 368
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-static {p1}, Lcom/adjust/sdk/Adjust;->setOfflineMode(Z)V

    return-void
.end method

.method public setPushToken(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 373
    invoke-virtual {p0}, Lcom/adjust/nativemodule/Adjust;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/adjust/sdk/Adjust;->setPushToken(Ljava/lang/String;Landroid/content/Context;)V

    return-void
.end method

.method public setReferrer(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 363
    invoke-virtual {p0}, Lcom/adjust/nativemodule/Adjust;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/adjust/sdk/Adjust;->setReferrer(Ljava/lang/String;Landroid/content/Context;)V

    return-void
.end method

.method public setSessionTrackingFailedCallbackListener()V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    const/4 v0, 0x1

    .line 499
    iput-boolean v0, p0, Lcom/adjust/nativemodule/Adjust;->sessionTrackingFailedCallback:Z

    return-void
.end method

.method public setSessionTrackingSucceededCallbackListener()V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    const/4 v0, 0x1

    .line 494
    iput-boolean v0, p0, Lcom/adjust/nativemodule/Adjust;->sessionTrackingSucceededCallback:Z

    return-void
.end method

.method public setTestOptions(Lcom/facebook/react/bridge/ReadableMap;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    if-nez p1, :cond_0

    return-void

    .line 523
    :cond_0
    new-instance v0, Lcom/adjust/sdk/AdjustTestOptions;

    invoke-direct {v0}, Lcom/adjust/sdk/AdjustTestOptions;-><init>()V

    const-string v1, "hasContext"

    .line 524
    invoke-direct {p0, p1, v1}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 525
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableMap;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 527
    invoke-virtual {p0}, Lcom/adjust/nativemodule/Adjust;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v1

    iput-object v1, v0, Lcom/adjust/sdk/AdjustTestOptions;->context:Landroid/content/Context;

    :cond_1
    const-string v1, "baseUrl"

    .line 530
    invoke-direct {p0, p1, v1}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 531
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 532
    iput-object v1, v0, Lcom/adjust/sdk/AdjustTestOptions;->baseUrl:Ljava/lang/String;

    :cond_2
    const-string v1, "gdprUrl"

    .line 534
    invoke-direct {p0, p1, v1}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 535
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 536
    iput-object v1, v0, Lcom/adjust/sdk/AdjustTestOptions;->gdprUrl:Ljava/lang/String;

    :cond_3
    const-string v1, "basePath"

    .line 538
    invoke-direct {p0, p1, v1}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 539
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 540
    iput-object v1, v0, Lcom/adjust/sdk/AdjustTestOptions;->basePath:Ljava/lang/String;

    :cond_4
    const-string v1, "gdprPath"

    .line 542
    invoke-direct {p0, p1, v1}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 543
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 544
    iput-object v1, v0, Lcom/adjust/sdk/AdjustTestOptions;->gdprPath:Ljava/lang/String;

    :cond_5
    const-string/jumbo v1, "useTestConnectionOptions"

    .line 546
    invoke-direct {p0, p1, v1}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 547
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableMap;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 548
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/adjust/sdk/AdjustTestOptions;->useTestConnectionOptions:Ljava/lang/Boolean;

    :cond_6
    const-string v1, "timerIntervalInMilliseconds"

    .line 550
    invoke-direct {p0, p1, v1}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v2

    const-string v3, "Can\'t format number"

    if-eqz v2, :cond_7

    .line 552
    :try_start_0
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 553
    iput-object v1, v0, Lcom/adjust/sdk/AdjustTestOptions;->timerIntervalInMilliseconds:Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 555
    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 556
    sget-object v1, Lcom/adjust/nativemodule/Adjust;->TAG:Ljava/lang/String;

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    :goto_0
    const-string v1, "timerStartInMilliseconds"

    .line 559
    invoke-direct {p0, p1, v1}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 561
    :try_start_1
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 562
    iput-object v1, v0, Lcom/adjust/sdk/AdjustTestOptions;->timerStartInMilliseconds:Ljava/lang/Long;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    .line 564
    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 565
    sget-object v1, Lcom/adjust/nativemodule/Adjust;->TAG:Ljava/lang/String;

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    :goto_1
    const-string v1, "sessionIntervalInMilliseconds"

    .line 568
    invoke-direct {p0, p1, v1}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 570
    :try_start_2
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 571
    iput-object v1, v0, Lcom/adjust/sdk/AdjustTestOptions;->sessionIntervalInMilliseconds:Ljava/lang/Long;
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    move-exception v1

    .line 573
    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 574
    sget-object v1, Lcom/adjust/nativemodule/Adjust;->TAG:Ljava/lang/String;

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    :goto_2
    const-string v1, "subsessionIntervalInMilliseconds"

    .line 577
    invoke-direct {p0, p1, v1}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 579
    :try_start_3
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 580
    iput-object v1, v0, Lcom/adjust/sdk/AdjustTestOptions;->subsessionIntervalInMilliseconds:Ljava/lang/Long;
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_3

    :catch_3
    move-exception v1

    .line 582
    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 583
    sget-object v1, Lcom/adjust/nativemodule/Adjust;->TAG:Ljava/lang/String;

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    :goto_3
    const-string v1, "noBackoffWait"

    .line 586
    invoke-direct {p0, p1, v1}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 587
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableMap;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 588
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/adjust/sdk/AdjustTestOptions;->noBackoffWait:Ljava/lang/Boolean;

    :cond_b
    const-string v1, "teardown"

    .line 590
    invoke-direct {p0, p1, v1}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 591
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableMap;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    .line 592
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, v0, Lcom/adjust/sdk/AdjustTestOptions;->teardown:Ljava/lang/Boolean;

    .line 595
    :cond_c
    invoke-static {v0}, Lcom/adjust/sdk/Adjust;->setTestOptions(Lcom/adjust/sdk/AdjustTestOptions;)V

    return-void
.end method

.method public teardown()V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    const/4 v0, 0x0

    .line 509
    iput-boolean v0, p0, Lcom/adjust/nativemodule/Adjust;->attributionCallback:Z

    .line 510
    iput-boolean v0, p0, Lcom/adjust/nativemodule/Adjust;->eventTrackingSucceededCallback:Z

    .line 511
    iput-boolean v0, p0, Lcom/adjust/nativemodule/Adjust;->eventTrackingFailedCallback:Z

    .line 512
    iput-boolean v0, p0, Lcom/adjust/nativemodule/Adjust;->sessionTrackingSucceededCallback:Z

    .line 513
    iput-boolean v0, p0, Lcom/adjust/nativemodule/Adjust;->sessionTrackingFailedCallback:Z

    .line 514
    iput-boolean v0, p0, Lcom/adjust/nativemodule/Adjust;->deferredDeeplinkCallback:Z

    return-void
.end method

.method public trackAdRevenue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 390
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 391
    invoke-static {p1, v0}, Lcom/adjust/sdk/Adjust;->trackAdRevenue(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 393
    :catch_0
    sget-object p1, Lcom/adjust/nativemodule/Adjust;->TAG:Ljava/lang/String;

    const-string p2, "Give ad revenue payload is not a valid JSON string"

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public trackEvent(Lcom/facebook/react/bridge/ReadableMap;)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    const/4 v2, 0x0

    const-string v3, "eventToken"

    .line 294
    invoke-direct {p0, p1, v3}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 295
    invoke-interface {p1, v3}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 298
    :cond_1
    new-instance v3, Lcom/adjust/sdk/AdjustEvent;

    invoke-direct {v3, v2}, Lcom/adjust/sdk/AdjustEvent;-><init>(Ljava/lang/String;)V

    .line 299
    invoke-virtual {v3}, Lcom/adjust/sdk/AdjustEvent;->isValid()Z

    move-result v2

    if-nez v2, :cond_2

    return-void

    :cond_2
    const-string v2, "revenue"

    .line 304
    invoke-direct {p0, p1, v2}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v4

    const-string v5, "currency"

    if-nez v4, :cond_3

    invoke-direct {p0, p1, v5}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 306
    :cond_3
    :try_start_0
    invoke-interface {p1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 308
    :catch_0
    invoke-interface {p1, v5}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 309
    invoke-virtual {v3, v0, v1, v2}, Lcom/adjust/sdk/AdjustEvent;->setRevenue(DLjava/lang/String;)V

    :cond_4
    const-string v0, "callbackParameters"

    .line 313
    invoke-direct {p0, p1, v0}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 314
    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/ReadableMap;->getMap(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableMap;

    move-result-object v0

    invoke-static {v0}, Lcom/adjust/nativemodule/AdjustUtil;->toMap(Lcom/facebook/react/bridge/ReadableMap;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 316
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 317
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, Lcom/adjust/sdk/AdjustEvent;->addCallbackParameter(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v0, "partnerParameters"

    .line 323
    invoke-direct {p0, p1, v0}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 324
    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/ReadableMap;->getMap(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableMap;

    move-result-object v0

    invoke-static {v0}, Lcom/adjust/nativemodule/AdjustUtil;->toMap(Lcom/facebook/react/bridge/ReadableMap;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 326
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 327
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, Lcom/adjust/sdk/AdjustEvent;->addPartnerParameter(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    const-string v0, "transactionId"

    .line 333
    invoke-direct {p0, p1, v0}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 334
    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 336
    invoke-virtual {v3, v0}, Lcom/adjust/sdk/AdjustEvent;->setOrderId(Ljava/lang/String;)V

    :cond_7
    const-string v0, "callbackId"

    .line 341
    invoke-direct {p0, p1, v0}, Lcom/adjust/nativemodule/Adjust;->checkKey(Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 342
    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_8

    .line 344
    invoke-virtual {v3, p1}, Lcom/adjust/sdk/AdjustEvent;->setCallbackId(Ljava/lang/String;)V

    .line 348
    :cond_8
    invoke-static {v3}, Lcom/adjust/sdk/Adjust;->trackEvent(Lcom/adjust/sdk/AdjustEvent;)V

    return-void
.end method
