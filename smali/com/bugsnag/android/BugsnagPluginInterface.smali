.class public final Lcom/bugsnag/android/BugsnagPluginInterface;
.super Ljava/lang/Object;
.source "BugsnagPluginInterface.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBugsnagPluginInterface.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BugsnagPluginInterface.kt\ncom/bugsnag/android/BugsnagPluginInterface\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,31:1\n1353#2,9:32\n1574#2,2:41\n1362#2:43\n1574#2,2:44\n*E\n*S KotlinDebug\n*F\n+ 1 BugsnagPluginInterface.kt\ncom/bugsnag/android/BugsnagPluginInterface\n*L\n19#1,9:32\n19#1,2:41\n19#1:43\n20#1,2:44\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\u0010\u0008\u001a\u0006\u0012\u0002\u0008\u00030\u0005H\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0001J\u0012\u0010\r\u001a\u00020\n2\n\u0010\u000e\u001a\u0006\u0012\u0002\u0008\u00030\u0005R\u0018\u0010\u0003\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/bugsnag/android/BugsnagPluginInterface;",
        "",
        "()V",
        "plugins",
        "",
        "Ljava/lang/Class;",
        "convertClzToPlugin",
        "Lcom/bugsnag/android/BugsnagPlugin;",
        "it",
        "loadPlugins",
        "",
        "client",
        "Lcom/bugsnag/android/Client;",
        "registerPlugin",
        "clz",
        "bugsnag-android-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/bugsnag/android/BugsnagPluginInterface;

.field private static final plugins:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Class<",
            "*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    new-instance v0, Lcom/bugsnag/android/BugsnagPluginInterface;

    invoke-direct {v0}, Lcom/bugsnag/android/BugsnagPluginInterface;-><init>()V

    sput-object v0, Lcom/bugsnag/android/BugsnagPluginInterface;->INSTANCE:Lcom/bugsnag/android/BugsnagPluginInterface;

    .line 9
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v0, Ljava/util/Set;

    sput-object v0, Lcom/bugsnag/android/BugsnagPluginInterface;->plugins:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final convertClzToPlugin(Ljava/lang/Class;)Lcom/bugsnag/android/BugsnagPlugin;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)",
            "Lcom/bugsnag/android/BugsnagPlugin;"
        }
    .end annotation

    .line 25
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/bugsnag/android/BugsnagPlugin;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.bugsnag.android.BugsnagPlugin"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method


# virtual methods
.method public final loadPlugins(Lcom/bugsnag/android/Client;)V
    .locals 4
    .param p1    # Lcom/bugsnag/android/Client;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lkotlin/jvm/JvmName;
        name = "loadPlugins"
    .end annotation

    const-string v0, "client"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    sget-object v0, Lcom/bugsnag/android/BugsnagPluginInterface;->plugins:Ljava/util/Set;

    check-cast v0, Ljava/lang/Iterable;

    .line 18
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 32
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 41
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 40
    check-cast v2, Ljava/lang/Class;

    .line 19
    sget-object v3, Lcom/bugsnag/android/BugsnagPluginInterface;->INSTANCE:Lcom/bugsnag/android/BugsnagPluginInterface;

    invoke-direct {v3, v2}, Lcom/bugsnag/android/BugsnagPluginInterface;->convertClzToPlugin(Ljava/lang/Class;)Lcom/bugsnag/android/BugsnagPlugin;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 40
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 43
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 44
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bugsnag/android/BugsnagPlugin;

    .line 20
    invoke-interface {v1, p1}, Lcom/bugsnag/android/BugsnagPlugin;->initialisePlugin(Lcom/bugsnag/android/Client;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public final registerPlugin(Ljava/lang/Class;)V
    .locals 1
    .param p1    # Ljava/lang/Class;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "clz"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    sget-object v0, Lcom/bugsnag/android/BugsnagPluginInterface;->plugins:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method
