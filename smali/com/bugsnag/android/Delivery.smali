.class public interface abstract Lcom/bugsnag/android/Delivery;
.super Ljava/lang/Object;
.source "Delivery.java"


# virtual methods
.method public abstract deliver(Lcom/bugsnag/android/Report;Lcom/bugsnag/android/Configuration;)V
    .param p1    # Lcom/bugsnag/android/Report;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bugsnag/android/Configuration;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/bugsnag/android/DeliveryFailureException;
        }
    .end annotation
.end method

.method public abstract deliver(Lcom/bugsnag/android/SessionTrackingPayload;Lcom/bugsnag/android/Configuration;)V
    .param p1    # Lcom/bugsnag/android/SessionTrackingPayload;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bugsnag/android/Configuration;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/bugsnag/android/DeliveryFailureException;
        }
    .end annotation
.end method
