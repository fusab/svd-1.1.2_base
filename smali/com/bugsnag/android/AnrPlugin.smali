.class public final Lcom/bugsnag/android/AnrPlugin;
.super Ljava/lang/Object;
.source "AnrPlugin.kt"

# interfaces
.implements Lcom/bugsnag/android/BugsnagPlugin;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0002J\u0010\u0010\u000b\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\nH\u0016J\u0011\u0010\u000c\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u000eH\u0082 R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/bugsnag/android/AnrPlugin;",
        "Lcom/bugsnag/android/BugsnagPlugin;",
        "()V",
        "collector",
        "Lcom/bugsnag/android/AnrDetailsCollector;",
        "handleAnr",
        "",
        "thread",
        "Ljava/lang/Thread;",
        "client",
        "Lcom/bugsnag/android/Client;",
        "initialisePlugin",
        "installAnrDetection",
        "sentinelBuffer",
        "Ljava/nio/ByteBuffer;",
        "bugsnag-plugin-android-anr_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private final collector:Lcom/bugsnag/android/AnrDetailsCollector;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Lcom/bugsnag/android/AnrDetailsCollector;

    invoke-direct {v0}, Lcom/bugsnag/android/AnrDetailsCollector;-><init>()V

    iput-object v0, p0, Lcom/bugsnag/android/AnrPlugin;->collector:Lcom/bugsnag/android/AnrDetailsCollector;

    return-void
.end method

.method public static final synthetic access$handleAnr(Lcom/bugsnag/android/AnrPlugin;Ljava/lang/Thread;Lcom/bugsnag/android/Client;)V
    .locals 0

    .line 5
    invoke-direct {p0, p1, p2}, Lcom/bugsnag/android/AnrPlugin;->handleAnr(Ljava/lang/Thread;Lcom/bugsnag/android/Client;)V

    return-void
.end method

.method private final handleAnr(Ljava/lang/Thread;Lcom/bugsnag/android/Client;)V
    .locals 10

    .line 23
    new-instance v0, Lcom/bugsnag/android/BugsnagException;

    invoke-virtual {p1}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    const-string v2, "Application did not respond to UI input"

    const-string v3, "ANR"

    invoke-direct {v0, v3, v2, v1}, Lcom/bugsnag/android/BugsnagException;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/StackTraceElement;)V

    .line 24
    new-instance v1, Lcom/bugsnag/android/Error$Builder;

    iget-object v5, p2, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    move-object v6, v0

    check-cast v6, Ljava/lang/Throwable;

    iget-object v7, p2, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    const/4 v9, 0x1

    move-object v4, v1

    move-object v8, p1

    invoke-direct/range {v4 .. v9}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/Throwable;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;Z)V

    .line 25
    sget-object p1, Lcom/bugsnag/android/Severity;->ERROR:Lcom/bugsnag/android/Severity;

    invoke-virtual {v1, p1}, Lcom/bugsnag/android/Error$Builder;->severity(Lcom/bugsnag/android/Severity;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    const-string v0, "anrError"

    .line 26
    invoke-virtual {p1, v0}, Lcom/bugsnag/android/Error$Builder;->severityReasonType(Ljava/lang/String;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 27
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    .line 31
    iget-object v0, p0, Lcom/bugsnag/android/AnrPlugin;->collector:Lcom/bugsnag/android/AnrDetailsCollector;

    const-string v1, "error"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p2, p1}, Lcom/bugsnag/android/AnrDetailsCollector;->collectAnrErrorDetails$bugsnag_plugin_android_anr_release(Lcom/bugsnag/android/Client;Lcom/bugsnag/android/Error;)V

    return-void
.end method

.method private final native installAnrDetection(Ljava/nio/ByteBuffer;)V
.end method


# virtual methods
.method public initialisePlugin(Lcom/bugsnag/android/Client;)V
    .locals 2
    .param p1    # Lcom/bugsnag/android/Client;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "client"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bugsnag-plugin-android-anr"

    .line 12
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 13
    new-instance v0, Lcom/bugsnag/android/AnrPlugin$initialisePlugin$delegate$1;

    invoke-direct {v0, p0, p1}, Lcom/bugsnag/android/AnrPlugin$initialisePlugin$delegate$1;-><init>(Lcom/bugsnag/android/AnrPlugin;Lcom/bugsnag/android/Client;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 14
    new-instance p1, Lcom/bugsnag/android/AppNotRespondingMonitor;

    new-instance v1, Lcom/bugsnag/android/AnrPlugin$sam$com_bugsnag_android_AppNotRespondingMonitor_Delegate$0;

    invoke-direct {v1, v0}, Lcom/bugsnag/android/AnrPlugin$sam$com_bugsnag_android_AppNotRespondingMonitor_Delegate$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/bugsnag/android/AppNotRespondingMonitor$Delegate;

    invoke-direct {p1, v1}, Lcom/bugsnag/android/AppNotRespondingMonitor;-><init>(Lcom/bugsnag/android/AppNotRespondingMonitor$Delegate;)V

    .line 15
    invoke-virtual {p1}, Lcom/bugsnag/android/AppNotRespondingMonitor;->start()V

    .line 16
    invoke-virtual {p1}, Lcom/bugsnag/android/AppNotRespondingMonitor;->getSentinelBuffer()Ljava/nio/ByteBuffer;

    move-result-object p1

    const-string v0, "monitor.sentinelBuffer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/bugsnag/android/AnrPlugin;->installAnrDetection(Ljava/nio/ByteBuffer;)V

    const-string p1, "Initialised ANR Plugin"

    .line 17
    invoke-static {p1}, Lcom/bugsnag/android/Logger;->info(Ljava/lang/String;)V

    return-void
.end method
