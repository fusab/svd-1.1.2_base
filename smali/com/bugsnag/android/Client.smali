.class public Lcom/bugsnag/android/Client;
.super Ljava/util/Observable;
.source "Client.java"

# interfaces
.implements Ljava/util/Observer;


# static fields
.field private static final BLOCKING:Z = true

.field static final INTERNAL_DIAGNOSTICS_TAB:Ljava/lang/String; = "BugsnagDiagnostics"

.field private static final SHARED_PREF_KEY:Ljava/lang/String; = "com.bugsnag.android"

.field private static final USER_EMAIL_KEY:Ljava/lang/String; = "user.email"

.field private static final USER_ID_KEY:Ljava/lang/String; = "user.id"

.field private static final USER_NAME_KEY:Ljava/lang/String; = "user.name"


# instance fields
.field final appContext:Landroid/content/Context;

.field protected final appData:Lcom/bugsnag/android/AppData;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field final breadcrumbs:Lcom/bugsnag/android/Breadcrumbs;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field protected final config:Lcom/bugsnag/android/Configuration;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private final connectivity:Lcom/bugsnag/android/Connectivity;

.field protected final deviceData:Lcom/bugsnag/android/DeviceData;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field protected final errorStore:Lcom/bugsnag/android/ErrorStore;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field final eventReceiver:Lcom/bugsnag/android/EventReceiver;

.field private final orientationListener:Landroid/view/OrientationEventListener;

.field final sessionStore:Lcom/bugsnag/android/SessionStore;

.field final sessionTracker:Lcom/bugsnag/android/SessionTracker;

.field final sharedPrefs:Landroid/content/SharedPreferences;

.field final storageManager:Landroid/os/storage/StorageManager;

.field private final user:Lcom/bugsnag/android/User;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 93
    invoke-direct {p0, p1, v0, v1}, Lcom/bugsnag/android/Client;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/bugsnag/android/Configuration;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bugsnag/android/Configuration;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 126
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 71
    new-instance v0, Lcom/bugsnag/android/User;

    invoke-direct {v0}, Lcom/bugsnag/android/User;-><init>()V

    iput-object v0, p0, Lcom/bugsnag/android/Client;->user:Lcom/bugsnag/android/User;

    .line 127
    invoke-static {p1}, Lcom/bugsnag/android/Client;->warnIfNotAppContext(Landroid/content/Context;)V

    .line 128
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    .line 129
    iput-object p2, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    .line 130
    new-instance p1, Lcom/bugsnag/android/SessionStore;

    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {p1, v0, v1, v2}, Lcom/bugsnag/android/SessionStore;-><init>(Lcom/bugsnag/android/Configuration;Landroid/content/Context;Lcom/bugsnag/android/FileStore$Delegate;)V

    iput-object p1, p0, Lcom/bugsnag/android/Client;->sessionStore:Lcom/bugsnag/android/SessionStore;

    .line 131
    iget-object p1, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    const-string v0, "storage"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/storage/StorageManager;

    iput-object p1, p0, Lcom/bugsnag/android/Client;->storageManager:Landroid/os/storage/StorageManager;

    .line 133
    new-instance p1, Lcom/bugsnag/android/ConnectivityCompat;

    iget-object v0, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    new-instance v1, Lcom/bugsnag/android/Client$1;

    invoke-direct {v1, p0}, Lcom/bugsnag/android/Client$1;-><init>(Lcom/bugsnag/android/Client;)V

    invoke-direct {p1, v0, v1}, Lcom/bugsnag/android/ConnectivityCompat;-><init>(Landroid/content/Context;Lkotlin/jvm/functions/Function1;)V

    iput-object p1, p0, Lcom/bugsnag/android/Client;->connectivity:Lcom/bugsnag/android/Connectivity;

    .line 144
    invoke-virtual {p2}, Lcom/bugsnag/android/Configuration;->getDelivery()Lcom/bugsnag/android/Delivery;

    move-result-object p1

    if-nez p1, :cond_0

    .line 145
    new-instance p1, Lcom/bugsnag/android/DefaultDelivery;

    iget-object v0, p0, Lcom/bugsnag/android/Client;->connectivity:Lcom/bugsnag/android/Connectivity;

    invoke-direct {p1, v0}, Lcom/bugsnag/android/DefaultDelivery;-><init>(Lcom/bugsnag/android/Connectivity;)V

    invoke-virtual {p2, p1}, Lcom/bugsnag/android/Configuration;->setDelivery(Lcom/bugsnag/android/Delivery;)V

    .line 148
    :cond_0
    new-instance p1, Lcom/bugsnag/android/SessionTracker;

    iget-object v0, p0, Lcom/bugsnag/android/Client;->sessionStore:Lcom/bugsnag/android/SessionStore;

    invoke-direct {p1, p2, p0, v0}, Lcom/bugsnag/android/SessionTracker;-><init>(Lcom/bugsnag/android/Configuration;Lcom/bugsnag/android/Client;Lcom/bugsnag/android/SessionStore;)V

    iput-object p1, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 150
    new-instance p1, Lcom/bugsnag/android/EventReceiver;

    invoke-direct {p1, p0}, Lcom/bugsnag/android/EventReceiver;-><init>(Lcom/bugsnag/android/Client;)V

    iput-object p1, p0, Lcom/bugsnag/android/Client;->eventReceiver:Lcom/bugsnag/android/EventReceiver;

    .line 153
    iget-object p1, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    const/4 v0, 0x0

    const-string v1, "com.bugsnag.android"

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/bugsnag/android/Client;->sharedPrefs:Landroid/content/SharedPreferences;

    .line 155
    new-instance p1, Lcom/bugsnag/android/AppData;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v5, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    invoke-direct {p1, v1, v3, v4, v5}, Lcom/bugsnag/android/AppData;-><init>(Landroid/content/Context;Landroid/content/pm/PackageManager;Lcom/bugsnag/android/Configuration;Lcom/bugsnag/android/SessionTracker;)V

    iput-object p1, p0, Lcom/bugsnag/android/Client;->appData:Lcom/bugsnag/android/AppData;

    .line 156
    iget-object p1, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 157
    new-instance v1, Lcom/bugsnag/android/DeviceData;

    iget-object v3, p0, Lcom/bugsnag/android/Client;->connectivity:Lcom/bugsnag/android/Connectivity;

    iget-object v4, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/bugsnag/android/Client;->sharedPrefs:Landroid/content/SharedPreferences;

    invoke-direct {v1, v3, v4, p1, v5}, Lcom/bugsnag/android/DeviceData;-><init>(Lcom/bugsnag/android/Connectivity;Landroid/content/Context;Landroid/content/res/Resources;Landroid/content/SharedPreferences;)V

    iput-object v1, p0, Lcom/bugsnag/android/Client;->deviceData:Lcom/bugsnag/android/DeviceData;

    .line 160
    new-instance p1, Lcom/bugsnag/android/Breadcrumbs;

    invoke-direct {p1, p2}, Lcom/bugsnag/android/Breadcrumbs;-><init>(Lcom/bugsnag/android/Configuration;)V

    iput-object p1, p0, Lcom/bugsnag/android/Client;->breadcrumbs:Lcom/bugsnag/android/Breadcrumbs;

    .line 163
    iget-object p1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {p1}, Lcom/bugsnag/android/Configuration;->getProjectPackages()[Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x1

    if-nez p1, :cond_1

    .line 164
    new-array p1, p2, [Ljava/lang/String;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    invoke-virtual {p0, p1}, Lcom/bugsnag/android/Client;->setProjectPackages([Ljava/lang/String;)V

    .line 167
    :cond_1
    iget-object p1, p0, Lcom/bugsnag/android/Client;->deviceData:Lcom/bugsnag/android/DeviceData;

    invoke-virtual {p1}, Lcom/bugsnag/android/DeviceData;->getId()Ljava/lang/String;

    move-result-object p1

    .line 169
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0}, Lcom/bugsnag/android/Configuration;->getPersistUserBetweenSessions()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 171
    iget-object v0, p0, Lcom/bugsnag/android/Client;->user:Lcom/bugsnag/android/User;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "user.id"

    invoke-interface {v1, v3, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/User;->setId(Ljava/lang/String;)V

    .line 172
    iget-object p1, p0, Lcom/bugsnag/android/Client;->user:Lcom/bugsnag/android/User;

    iget-object v0, p0, Lcom/bugsnag/android/Client;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "user.name"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bugsnag/android/User;->setName(Ljava/lang/String;)V

    .line 173
    iget-object p1, p0, Lcom/bugsnag/android/Client;->user:Lcom/bugsnag/android/User;

    iget-object v0, p0, Lcom/bugsnag/android/Client;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "user.email"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bugsnag/android/User;->setEmail(Ljava/lang/String;)V

    goto :goto_0

    .line 175
    :cond_2
    iget-object v0, p0, Lcom/bugsnag/android/Client;->user:Lcom/bugsnag/android/User;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/User;->setId(Ljava/lang/String;)V

    .line 178
    :goto_0
    iget-object p1, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    instance-of v0, p1, Landroid/app/Application;

    if-eqz v0, :cond_3

    .line 179
    check-cast p1, Landroid/app/Application;

    .line 180
    iget-object v0, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    invoke-virtual {p1, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    goto :goto_1

    :cond_3
    const-string p1, "Bugsnag is unable to setup automatic activity lifecycle breadcrumbs on API Levels below 14."

    .line 182
    invoke-static {p1}, Lcom/bugsnag/android/Logger;->warn(Ljava/lang/String;)V

    .line 188
    :goto_1
    iget-object p1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {p1}, Lcom/bugsnag/android/Configuration;->getBuildUUID()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_4

    .line 191
    :try_start_0
    iget-object p1, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    .line 192
    iget-object v0, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x80

    .line 194
    invoke-virtual {p1, v0, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object p1

    .line 195
    iget-object p1, p1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v0, "com.bugsnag.android.BUILD_UUID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    const-string p1, "Bugsnag is unable to read build UUID from manifest."

    .line 197
    invoke-static {p1}, Lcom/bugsnag/android/Logger;->warn(Ljava/lang/String;)V

    :goto_2
    if-eqz v2, :cond_4

    .line 200
    iget-object p1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {p1, v2}, Lcom/bugsnag/android/Configuration;->setBuildUUID(Ljava/lang/String;)V

    .line 205
    :cond_4
    new-instance p1, Lcom/bugsnag/android/ErrorStore;

    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    new-instance v2, Lcom/bugsnag/android/Client$2;

    invoke-direct {v2, p0}, Lcom/bugsnag/android/Client$2;-><init>(Lcom/bugsnag/android/Client;)V

    invoke-direct {p1, v0, v1, v2}, Lcom/bugsnag/android/ErrorStore;-><init>(Lcom/bugsnag/android/Configuration;Landroid/content/Context;Lcom/bugsnag/android/FileStore$Delegate;)V

    iput-object p1, p0, Lcom/bugsnag/android/Client;->errorStore:Lcom/bugsnag/android/ErrorStore;

    .line 229
    iget-object p1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {p1}, Lcom/bugsnag/android/Configuration;->getEnableExceptionHandler()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 230
    invoke-virtual {p0}, Lcom/bugsnag/android/Client;->enableExceptionHandler()V

    .line 236
    :cond_5
    :try_start_1
    new-instance p1, Lcom/bugsnag/android/Client$3;

    invoke-direct {p1, p0}, Lcom/bugsnag/android/Client$3;-><init>(Lcom/bugsnag/android/Client;)V

    invoke-static {p1}, Lcom/bugsnag/android/Async;->run(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception p1

    const-string v0, "Failed to register for automatic breadcrumb broadcasts"

    .line 243
    invoke-static {v0, p1}, Lcom/bugsnag/android/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 245
    :goto_3
    iget-object p1, p0, Lcom/bugsnag/android/Client;->connectivity:Lcom/bugsnag/android/Connectivity;

    invoke-interface {p1}, Lcom/bugsnag/android/Connectivity;->registerForNetworkChanges()V

    .line 247
    iget-object p1, p0, Lcom/bugsnag/android/Client;->appData:Lcom/bugsnag/android/AppData;

    .line 248
    invoke-virtual {p1}, Lcom/bugsnag/android/AppData;->guessReleaseStage()Ljava/lang/String;

    move-result-object p1

    const-string v0, "production"

    .line 247
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    xor-int/2addr p1, p2

    .line 249
    invoke-static {p1}, Lcom/bugsnag/android/Logger;->setEnabled(Z)V

    .line 251
    iget-object p1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {p1, p0}, Lcom/bugsnag/android/Configuration;->addObserver(Ljava/util/Observer;)V

    .line 252
    iget-object p1, p0, Lcom/bugsnag/android/Client;->breadcrumbs:Lcom/bugsnag/android/Breadcrumbs;

    invoke-virtual {p1, p0}, Lcom/bugsnag/android/Breadcrumbs;->addObserver(Ljava/util/Observer;)V

    .line 253
    iget-object p1, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    invoke-virtual {p1, p0}, Lcom/bugsnag/android/SessionTracker;->addObserver(Ljava/util/Observer;)V

    .line 254
    iget-object p1, p0, Lcom/bugsnag/android/Client;->user:Lcom/bugsnag/android/User;

    invoke-virtual {p1, p0}, Lcom/bugsnag/android/User;->addObserver(Ljava/util/Observer;)V

    .line 257
    new-instance p1, Lcom/bugsnag/android/Client$4;

    iget-object p2, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    invoke-direct {p1, p0, p2, p0}, Lcom/bugsnag/android/Client$4;-><init>(Lcom/bugsnag/android/Client;Landroid/content/Context;Lcom/bugsnag/android/Client;)V

    iput-object p1, p0, Lcom/bugsnag/android/Client;->orientationListener:Landroid/view/OrientationEventListener;

    .line 266
    :try_start_2
    iget-object p1, p0, Lcom/bugsnag/android/Client;->orientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {p1}, Landroid/view/OrientationEventListener;->enable()V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_4

    :catch_2
    move-exception p1

    .line 268
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Failed to set up orientation tracking: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/bugsnag/android/Logger;->warn(Ljava/lang/String;)V

    .line 272
    :goto_4
    iget-object p1, p0, Lcom/bugsnag/android/Client;->errorStore:Lcom/bugsnag/android/ErrorStore;

    invoke-virtual {p1}, Lcom/bugsnag/android/ErrorStore;->flushOnLaunch()V

    .line 273
    invoke-direct {p0}, Lcom/bugsnag/android/Client;->loadPlugins()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    .line 103
    invoke-direct {p0, p1, p2, v0}, Lcom/bugsnag/android/Client;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 117
    invoke-static {p1, p2, p3}, Lcom/bugsnag/android/ConfigFactory;->createNewConfiguration(Landroid/content/Context;Ljava/lang/String;Z)Lcom/bugsnag/android/Configuration;

    move-result-object p2

    .line 116
    invoke-direct {p0, p1, p2}, Lcom/bugsnag/android/Client;-><init>(Landroid/content/Context;Lcom/bugsnag/android/Configuration;)V

    return-void
.end method

.method static synthetic access$000(Lcom/bugsnag/android/Client;)V
    .locals 0

    .line 48
    invoke-virtual {p0}, Lcom/bugsnag/android/Client;->setChanged()V

    return-void
.end method

.method private deliverReportAsync(Lcom/bugsnag/android/Error;Lcom/bugsnag/android/Report;)V
    .locals 1
    .param p1    # Lcom/bugsnag/android/Error;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1073
    :try_start_0
    new-instance v0, Lcom/bugsnag/android/Client$7;

    invoke-direct {v0, p0, p2, p1}, Lcom/bugsnag/android/Client$7;-><init>(Lcom/bugsnag/android/Client;Lcom/bugsnag/android/Report;Lcom/bugsnag/android/Error;)V

    invoke-static {v0}, Lcom/bugsnag/android/Async;->run(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1080
    :catch_0
    iget-object p2, p0, Lcom/bugsnag/android/Client;->errorStore:Lcom/bugsnag/android/ErrorStore;

    invoke-virtual {p2, p1}, Lcom/bugsnag/android/ErrorStore;->write(Lcom/bugsnag/android/JsonStream$Streamable;)Ljava/lang/String;

    const-string p1, "Exceeded max queue count, saving to disk to send later"

    .line 1081
    invoke-static {p1}, Lcom/bugsnag/android/Logger;->warn(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private getKeyFromClientData(Ljava/util/Map;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 1285
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    .line 1286
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1287
    check-cast p1, Ljava/lang/String;

    return-object p1

    :cond_0
    if-nez p3, :cond_1

    const/4 p1, 0x0

    return-object p1

    .line 1289
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Failed to set "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " in client data!"

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private leaveErrorBreadcrumb(Lcom/bugsnag/android/Error;)V
    .locals 4
    .param p1    # Lcom/bugsnag/android/Error;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1087
    invoke-virtual {p1}, Lcom/bugsnag/android/Error;->getExceptionMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "message"

    .line 1088
    invoke-static {v1, v0}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    .line 1089
    iget-object v1, p0, Lcom/bugsnag/android/Client;->breadcrumbs:Lcom/bugsnag/android/Breadcrumbs;

    new-instance v2, Lcom/bugsnag/android/Breadcrumb;

    invoke-virtual {p1}, Lcom/bugsnag/android/Error;->getExceptionName()Ljava/lang/String;

    move-result-object p1

    sget-object v3, Lcom/bugsnag/android/BreadcrumbType;->ERROR:Lcom/bugsnag/android/BreadcrumbType;

    invoke-direct {v2, p1, v3, v0}, Lcom/bugsnag/android/Breadcrumb;-><init>(Ljava/lang/String;Lcom/bugsnag/android/BreadcrumbType;Ljava/util/Map;)V

    invoke-virtual {v1, v2}, Lcom/bugsnag/android/Breadcrumbs;->add(Lcom/bugsnag/android/Breadcrumb;)V

    return-void
.end method

.method private loadPlugins()V
    .locals 2

    .line 293
    invoke-static {p0}, Lcom/bugsnag/android/NativeInterface;->setClient(Lcom/bugsnag/android/Client;)V

    .line 294
    sget-object v0, Lcom/bugsnag/android/BugsnagPluginInterface;->INSTANCE:Lcom/bugsnag/android/BugsnagPluginInterface;

    .line 296
    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v1}, Lcom/bugsnag/android/Configuration;->getDetectNdkCrashes()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    const-string v1, "com.bugsnag.android.NdkPlugin"

    .line 298
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bugsnag/android/BugsnagPluginInterface;->registerPlugin(Ljava/lang/Class;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v1, "bugsnag-plugin-android-ndk artefact not found on classpath, NDK errors will not be captured."

    .line 300
    invoke-static {v1}, Lcom/bugsnag/android/Logger;->warn(Ljava/lang/String;)V

    .line 304
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v1}, Lcom/bugsnag/android/Configuration;->getDetectAnrs()Z

    move-result v1

    if-eqz v1, :cond_1

    :try_start_1
    const-string v1, "com.bugsnag.android.AnrPlugin"

    .line 306
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bugsnag/android/BugsnagPluginInterface;->registerPlugin(Ljava/lang/Class;)V
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    const-string v1, "bugsnag-plugin-android-anr artefact not found on classpath, ANR errors will not be captured."

    .line 308
    invoke-static {v1}, Lcom/bugsnag/android/Logger;->warn(Ljava/lang/String;)V

    .line 312
    :cond_1
    :goto_1
    invoke-virtual {v0, p0}, Lcom/bugsnag/android/BugsnagPluginInterface;->loadPlugins(Lcom/bugsnag/android/Client;)V

    return-void
.end method

.method private notify(Lcom/bugsnag/android/Error;Z)V
    .locals 1
    .param p1    # Lcom/bugsnag/android/Error;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    if-eqz p2, :cond_0

    .line 926
    sget-object p2, Lcom/bugsnag/android/DeliveryStyle;->SAME_THREAD:Lcom/bugsnag/android/DeliveryStyle;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/bugsnag/android/DeliveryStyle;->ASYNC:Lcom/bugsnag/android/DeliveryStyle;

    :goto_0
    const/4 v0, 0x0

    .line 927
    invoke-virtual {p0, p1, p2, v0}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Lcom/bugsnag/android/DeliveryStyle;Lcom/bugsnag/android/Callback;)V

    return-void
.end method

.method private runBeforeBreadcrumbTasks(Lcom/bugsnag/android/Breadcrumb;)Z
    .locals 3
    .param p1    # Lcom/bugsnag/android/Breadcrumb;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1480
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0}, Lcom/bugsnag/android/Configuration;->getBeforeRecordBreadcrumbTasks()Ljava/util/Collection;

    move-result-object v0

    .line 1481
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bugsnag/android/BeforeRecordBreadcrumb;

    .line 1483
    :try_start_0
    invoke-interface {v1, p1}, Lcom/bugsnag/android/BeforeRecordBreadcrumb;->shouldRecord(Lcom/bugsnag/android/Breadcrumb;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    const/4 p1, 0x0

    return p1

    :catch_0
    move-exception v1

    const-string v2, "BeforeRecordBreadcrumb threw an Exception"

    .line 1487
    invoke-static {v2, v1}, Lcom/bugsnag/android/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method private runBeforeNotifyTasks(Lcom/bugsnag/android/Error;)Z
    .locals 3

    .line 1465
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0}, Lcom/bugsnag/android/Configuration;->getBeforeNotifyTasks()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bugsnag/android/BeforeNotify;

    .line 1467
    :try_start_0
    invoke-interface {v1, p1}, Lcom/bugsnag/android/BeforeNotify;->run(Lcom/bugsnag/android/Error;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    const/4 p1, 0x0

    return p1

    :catch_0
    move-exception v1

    const-string v2, "BeforeNotify threw an Exception"

    .line 1471
    invoke-static {v2, v1}, Lcom/bugsnag/android/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method private runBeforeSendTasks(Lcom/bugsnag/android/Report;)Z
    .locals 3

    .line 1442
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0}, Lcom/bugsnag/android/Configuration;->getBeforeSendTasks()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bugsnag/android/BeforeSend;

    .line 1444
    :try_start_0
    invoke-interface {v1, p1}, Lcom/bugsnag/android/BeforeSend;->run(Lcom/bugsnag/android/Report;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    const/4 p1, 0x0

    return p1

    :catch_0
    move-exception v1

    const-string v2, "BeforeSend threw an Exception"

    .line 1448
    invoke-static {v2, v1}, Lcom/bugsnag/android/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method private storeInSharedPrefs(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 1501
    iget-object v0, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    const-string v1, "com.bugsnag.android"

    const/4 v2, 0x0

    .line 1502
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1503
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private static warnIfNotAppContext(Landroid/content/Context;)V
    .locals 0

    .line 1529
    instance-of p0, p0, Landroid/app/Application;

    if-nez p0, :cond_0

    const-string p0, "Warning - Non-Application context detected! Please ensure that you are initializing Bugsnag from a custom Application class."

    .line 1530
    invoke-static {p0}, Lcom/bugsnag/android/Logger;->warn(Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public addToTab(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1308
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0}, Lcom/bugsnag/android/Configuration;->getMetaData()Lcom/bugsnag/android/MetaData;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/bugsnag/android/MetaData;->addToTab(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public beforeNotify(Lcom/bugsnag/android/BeforeNotify;)V
    .locals 1
    .param p1    # Lcom/bugsnag/android/BeforeNotify;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 746
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/Configuration;->beforeNotify(Lcom/bugsnag/android/BeforeNotify;)V

    return-void
.end method

.method public beforeRecordBreadcrumb(Lcom/bugsnag/android/BeforeRecordBreadcrumb;)V
    .locals 1
    .param p1    # Lcom/bugsnag/android/BeforeRecordBreadcrumb;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 768
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/Configuration;->beforeRecordBreadcrumb(Lcom/bugsnag/android/BeforeRecordBreadcrumb;)V

    return-void
.end method

.method cacheAndNotify(Ljava/lang/Throwable;Lcom/bugsnag/android/Severity;Lcom/bugsnag/android/MetaData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Thread;)V
    .locals 7
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1430
    new-instance v6, Lcom/bugsnag/android/Error$Builder;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v3, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    const/4 v5, 0x1

    move-object v0, v6

    move-object v2, p1

    move-object v4, p6

    invoke-direct/range {v0 .. v5}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/Throwable;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;Z)V

    .line 1432
    invoke-virtual {v6, p2}, Lcom/bugsnag/android/Error$Builder;->severity(Lcom/bugsnag/android/Severity;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1433
    invoke-virtual {p1, p3}, Lcom/bugsnag/android/Error$Builder;->metaData(Lcom/bugsnag/android/MetaData;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1434
    invoke-virtual {p1, p4}, Lcom/bugsnag/android/Error$Builder;->severityReasonType(Ljava/lang/String;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1435
    invoke-virtual {p1, p5}, Lcom/bugsnag/android/Error$Builder;->attributeValue(Ljava/lang/String;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1436
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    .line 1438
    sget-object p2, Lcom/bugsnag/android/DeliveryStyle;->ASYNC_WITH_CACHE:Lcom/bugsnag/android/DeliveryStyle;

    const/4 p3, 0x0

    invoke-virtual {p0, p1, p2, p3}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Lcom/bugsnag/android/DeliveryStyle;Lcom/bugsnag/android/Callback;)V

    return-void
.end method

.method public clearBreadcrumbs()V
    .locals 1

    .line 1383
    iget-object v0, p0, Lcom/bugsnag/android/Client;->breadcrumbs:Lcom/bugsnag/android/Breadcrumbs;

    invoke-virtual {v0}, Lcom/bugsnag/android/Breadcrumbs;->clear()V

    return-void
.end method

.method public clearTab(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1317
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0}, Lcom/bugsnag/android/Configuration;->getMetaData()Lcom/bugsnag/android/MetaData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/MetaData;->clearTab(Ljava/lang/String;)V

    return-void
.end method

.method public clearUser()V
    .locals 3

    .line 635
    iget-object v0, p0, Lcom/bugsnag/android/Client;->user:Lcom/bugsnag/android/User;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->deviceData:Lcom/bugsnag/android/DeviceData;

    invoke-virtual {v1}, Lcom/bugsnag/android/DeviceData;->getDeviceData()Ljava/util/Map;

    move-result-object v1

    const-string v2, "id"

    invoke-static {v2, v1}, Lcom/bugsnag/android/MapUtils;->getStringFromMap(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bugsnag/android/User;->setId(Ljava/lang/String;)V

    .line 636
    iget-object v0, p0, Lcom/bugsnag/android/Client;->user:Lcom/bugsnag/android/User;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bugsnag/android/User;->setEmail(Ljava/lang/String;)V

    .line 637
    iget-object v0, p0, Lcom/bugsnag/android/Client;->user:Lcom/bugsnag/android/User;

    invoke-virtual {v0, v1}, Lcom/bugsnag/android/User;->setName(Ljava/lang/String;)V

    .line 639
    iget-object v0, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    const-string v1, "com.bugsnag.android"

    const/4 v2, 0x0

    .line 640
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 641
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "user.id"

    .line 642
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "user.email"

    .line 643
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "user.name"

    .line 644
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 645
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method close()V
    .locals 1

    .line 1571
    iget-object v0, p0, Lcom/bugsnag/android/Client;->orientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 1572
    iget-object v0, p0, Lcom/bugsnag/android/Client;->connectivity:Lcom/bugsnag/android/Connectivity;

    invoke-interface {v0}, Lcom/bugsnag/android/Connectivity;->unregisterForNetworkChanges()V

    return-void
.end method

.method deliver(Lcom/bugsnag/android/Report;Lcom/bugsnag/android/Error;)V
    .locals 2
    .param p1    # Lcom/bugsnag/android/Report;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bugsnag/android/Error;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1402
    invoke-direct {p0, p1}, Lcom/bugsnag/android/Client;->runBeforeSendTasks(Lcom/bugsnag/android/Report;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string p1, "Skipping notification - beforeSend task returned false"

    .line 1403
    invoke-static {p1}, Lcom/bugsnag/android/Logger;->info(Ljava/lang/String;)V

    return-void

    .line 1407
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0}, Lcom/bugsnag/android/Configuration;->getDelivery()Lcom/bugsnag/android/Delivery;

    move-result-object v0

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-interface {v0, p1, v1}, Lcom/bugsnag/android/Delivery;->deliver(Lcom/bugsnag/android/Report;Lcom/bugsnag/android/Configuration;)V

    const-string v0, "Sent 1 new error to Bugsnag"

    .line 1408
    invoke-static {v0}, Lcom/bugsnag/android/Logger;->info(Ljava/lang/String;)V

    .line 1409
    invoke-direct {p0, p2}, Lcom/bugsnag/android/Client;->leaveErrorBreadcrumb(Lcom/bugsnag/android/Error;)V
    :try_end_0
    .catch Lcom/bugsnag/android/DeliveryFailureException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string p2, "Problem sending error to Bugsnag"

    .line 1418
    invoke-static {p2, p1}, Lcom/bugsnag/android/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    .line 1411
    invoke-virtual {p1}, Lcom/bugsnag/android/Report;->isCachingDisabled()Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "Could not send error(s) to Bugsnag, saving to disk to send later"

    .line 1412
    invoke-static {p1, v0}, Lcom/bugsnag/android/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1414
    iget-object p1, p0, Lcom/bugsnag/android/Client;->errorStore:Lcom/bugsnag/android/ErrorStore;

    invoke-virtual {p1, p2}, Lcom/bugsnag/android/ErrorStore;->write(Lcom/bugsnag/android/JsonStream$Streamable;)Ljava/lang/String;

    .line 1415
    invoke-direct {p0, p2}, Lcom/bugsnag/android/Client;->leaveErrorBreadcrumb(Lcom/bugsnag/android/Error;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public disableExceptionHandler()V
    .locals 0

    .line 1398
    invoke-static {p0}, Lcom/bugsnag/android/ExceptionHandler;->disable(Lcom/bugsnag/android/Client;)V

    return-void
.end method

.method public enableExceptionHandler()V
    .locals 0

    .line 1391
    invoke-static {p0}, Lcom/bugsnag/android/ExceptionHandler;->enable(Lcom/bugsnag/android/Client;)V

    return-void
.end method

.method enqueuePendingNativeReports()V
    .locals 3

    .line 334
    invoke-virtual {p0}, Lcom/bugsnag/android/Client;->setChanged()V

    .line 335
    new-instance v0, Lcom/bugsnag/android/NativeInterface$Message;

    sget-object v1, Lcom/bugsnag/android/NativeInterface$MessageType;->DELIVER_PENDING:Lcom/bugsnag/android/NativeInterface$MessageType;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/bugsnag/android/NativeInterface$Message;-><init>(Lcom/bugsnag/android/NativeInterface$MessageType;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/bugsnag/android/Client;->notifyObservers(Ljava/lang/Object;)V

    return-void
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1517
    iget-object v0, p0, Lcom/bugsnag/android/Client;->eventReceiver:Lcom/bugsnag/android/EventReceiver;

    if-eqz v0, :cond_0

    .line 1519
    :try_start_0
    iget-object v1, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v0, "Receiver not registered"

    .line 1521
    invoke-static {v0}, Lcom/bugsnag/android/Logger;->warn(Ljava/lang/String;)V

    .line 1525
    :cond_0
    :goto_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void
.end method

.method getAndSetDeliveryCompat()Lcom/bugsnag/android/DeliveryCompat;
    .locals 2

    .line 692
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0}, Lcom/bugsnag/android/Configuration;->getDelivery()Lcom/bugsnag/android/Delivery;

    move-result-object v0

    .line 694
    instance-of v1, v0, Lcom/bugsnag/android/DeliveryCompat;

    if-eqz v1, :cond_0

    .line 695
    check-cast v0, Lcom/bugsnag/android/DeliveryCompat;

    return-object v0

    .line 697
    :cond_0
    new-instance v0, Lcom/bugsnag/android/DeliveryCompat;

    invoke-direct {v0}, Lcom/bugsnag/android/DeliveryCompat;-><init>()V

    .line 698
    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v1, v0}, Lcom/bugsnag/android/Configuration;->setDelivery(Lcom/bugsnag/android/Delivery;)V

    return-object v0
.end method

.method public getAppData()Lcom/bugsnag/android/AppData;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 622
    iget-object v0, p0, Lcom/bugsnag/android/Client;->appData:Lcom/bugsnag/android/AppData;

    return-object v0
.end method

.method public getBreadcrumbs()Ljava/util/Collection;
    .locals 2
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/bugsnag/android/Breadcrumb;",
            ">;"
        }
    .end annotation

    .line 616
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->breadcrumbs:Lcom/bugsnag/android/Breadcrumbs;

    iget-object v1, v1, Lcom/bugsnag/android/Breadcrumbs;->store:Ljava/util/Queue;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getConfig()Lcom/bugsnag/android/Configuration;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1554
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    return-object v0
.end method

.method public getContext()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 438
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0}, Lcom/bugsnag/android/Configuration;->getContext()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceData()Lcom/bugsnag/android/DeviceData;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 628
    iget-object v0, p0, Lcom/bugsnag/android/Client;->deviceData:Lcom/bugsnag/android/DeviceData;

    return-object v0
.end method

.method getErrorStore()Lcom/bugsnag/android/ErrorStore;
    .locals 1

    .line 1507
    iget-object v0, p0, Lcom/bugsnag/android/Client;->errorStore:Lcom/bugsnag/android/ErrorStore;

    return-object v0
.end method

.method public getLaunchTimeMs()J
    .locals 2

    .line 1563
    invoke-static {}, Lcom/bugsnag/android/AppData;->getDurationMs()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMetaData()Lcom/bugsnag/android/MetaData;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1326
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0}, Lcom/bugsnag/android/Configuration;->getMetaData()Lcom/bugsnag/android/MetaData;

    move-result-object v0

    return-object v0
.end method

.method getOrientationListener()Landroid/view/OrientationEventListener;
    .locals 1

    .line 1457
    iget-object v0, p0, Lcom/bugsnag/android/Client;->orientationListener:Landroid/view/OrientationEventListener;

    return-object v0
.end method

.method getSessionTracker()Lcom/bugsnag/android/SessionTracker;
    .locals 1

    .line 1461
    iget-object v0, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    return-object v0
.end method

.method public getUser()Lcom/bugsnag/android/User;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 610
    iget-object v0, p0, Lcom/bugsnag/android/Client;->user:Lcom/bugsnag/android/User;

    return-object v0
.end method

.method public internalClientNotify(Ljava/lang/Throwable;Ljava/util/Map;ZLcom/bugsnag/android/Callback;)V
    .locals 9
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/bugsnag/android/Callback;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z",
            "Lcom/bugsnag/android/Callback;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x1

    const-string v1, "severity"

    .line 1260
    invoke-direct {p0, p2, v1, v0}, Lcom/bugsnag/android/Client;->getKeyFromClientData(Ljava/util/Map;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    const-string v2, "severityReason"

    .line 1262
    invoke-direct {p0, p2, v2, v0}, Lcom/bugsnag/android/Client;->getKeyFromClientData(Ljava/util/Map;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, "logLevel"

    .line 1263
    invoke-direct {p0, p2, v4, v3}, Lcom/bugsnag/android/Client;->getKeyFromClientData(Ljava/util/Map;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p2

    const/4 v4, 0x2

    .line 1265
    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    aput-object v2, v4, v0

    const-string v0, "Internal client notify, severity = \'%s\', severityReason = \'%s\'"

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1267
    invoke-static {v0}, Lcom/bugsnag/android/Logger;->info(Ljava/lang/String;)V

    .line 1270
    new-instance v0, Lcom/bugsnag/android/Error$Builder;

    iget-object v4, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v6, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 1271
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    const/4 v8, 0x0

    move-object v3, v0

    move-object v5, p1

    invoke-direct/range {v3 .. v8}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/Throwable;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;Z)V

    .line 1272
    invoke-static {v1}, Lcom/bugsnag/android/Severity;->fromString(Ljava/lang/String;)Lcom/bugsnag/android/Severity;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/Error$Builder;->severity(Lcom/bugsnag/android/Severity;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1273
    invoke-virtual {p1, v2}, Lcom/bugsnag/android/Error$Builder;->severityReasonType(Ljava/lang/String;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1274
    invoke-virtual {p1, p2}, Lcom/bugsnag/android/Error$Builder;->attributeValue(Ljava/lang/String;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1275
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    if-eqz p3, :cond_0

    .line 1277
    sget-object p2, Lcom/bugsnag/android/DeliveryStyle;->SAME_THREAD:Lcom/bugsnag/android/DeliveryStyle;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/bugsnag/android/DeliveryStyle;->ASYNC:Lcom/bugsnag/android/DeliveryStyle;

    .line 1278
    :goto_0
    invoke-virtual {p0, p1, p2, p4}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Lcom/bugsnag/android/DeliveryStyle;Lcom/bugsnag/android/Callback;)V

    return-void
.end method

.method public leaveBreadcrumb(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1345
    new-instance v0, Lcom/bugsnag/android/Breadcrumb;

    invoke-direct {v0, p1}, Lcom/bugsnag/android/Breadcrumb;-><init>(Ljava/lang/String;)V

    .line 1347
    invoke-direct {p0, v0}, Lcom/bugsnag/android/Client;->runBeforeBreadcrumbTasks(Lcom/bugsnag/android/Breadcrumb;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1348
    iget-object p1, p0, Lcom/bugsnag/android/Client;->breadcrumbs:Lcom/bugsnag/android/Breadcrumbs;

    invoke-virtual {p1, v0}, Lcom/bugsnag/android/Breadcrumbs;->add(Lcom/bugsnag/android/Breadcrumb;)V

    :cond_0
    return-void
.end method

.method public leaveBreadcrumb(Ljava/lang/String;Lcom/bugsnag/android/BreadcrumbType;Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bugsnag/android/BreadcrumbType;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/bugsnag/android/BreadcrumbType;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1359
    new-instance v0, Lcom/bugsnag/android/Breadcrumb;

    invoke-direct {v0, p1, p2, p3}, Lcom/bugsnag/android/Breadcrumb;-><init>(Ljava/lang/String;Lcom/bugsnag/android/BreadcrumbType;Ljava/util/Map;)V

    .line 1361
    invoke-direct {p0, v0}, Lcom/bugsnag/android/Client;->runBeforeBreadcrumbTasks(Lcom/bugsnag/android/Breadcrumb;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1362
    iget-object p1, p0, Lcom/bugsnag/android/Client;->breadcrumbs:Lcom/bugsnag/android/Breadcrumbs;

    invoke-virtual {p1, v0}, Lcom/bugsnag/android/Breadcrumbs;->add(Lcom/bugsnag/android/Breadcrumb;)V

    :cond_0
    return-void
.end method

.method notify(Lcom/bugsnag/android/Error;Lcom/bugsnag/android/DeliveryStyle;Lcom/bugsnag/android/Callback;)V
    .locals 4
    .param p1    # Lcom/bugsnag/android/Error;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bugsnag/android/DeliveryStyle;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/bugsnag/android/Callback;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 934
    invoke-virtual {p1}, Lcom/bugsnag/android/Error;->shouldIgnoreClass()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 939
    :cond_0
    iget-object v0, p0, Lcom/bugsnag/android/Client;->appData:Lcom/bugsnag/android/AppData;

    invoke-virtual {v0}, Lcom/bugsnag/android/AppData;->getAppData()Ljava/util/Map;

    move-result-object v0

    const-string v1, "releaseStage"

    .line 942
    invoke-static {v1, v0}, Lcom/bugsnag/android/MapUtils;->getStringFromMap(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    .line 944
    iget-object v2, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v2, v1}, Lcom/bugsnag/android/Configuration;->shouldNotifyForReleaseStage(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    .line 949
    :cond_1
    iget-object v1, p0, Lcom/bugsnag/android/Client;->deviceData:Lcom/bugsnag/android/DeviceData;

    invoke-virtual {v1}, Lcom/bugsnag/android/DeviceData;->getDeviceData()Ljava/util/Map;

    move-result-object v1

    .line 950
    invoke-virtual {p1, v1}, Lcom/bugsnag/android/Error;->setDeviceData(Ljava/util/Map;)V

    .line 951
    invoke-virtual {p1}, Lcom/bugsnag/android/Error;->getMetaData()Lcom/bugsnag/android/MetaData;

    move-result-object v1

    iget-object v1, v1, Lcom/bugsnag/android/MetaData;->store:Ljava/util/Map;

    iget-object v2, p0, Lcom/bugsnag/android/Client;->deviceData:Lcom/bugsnag/android/DeviceData;

    invoke-virtual {v2}, Lcom/bugsnag/android/DeviceData;->getDeviceMetaData()Ljava/util/Map;

    move-result-object v2

    const-string v3, "device"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 955
    invoke-virtual {p1, v0}, Lcom/bugsnag/android/Error;->setAppData(Ljava/util/Map;)V

    .line 956
    invoke-virtual {p1}, Lcom/bugsnag/android/Error;->getMetaData()Lcom/bugsnag/android/MetaData;

    move-result-object v0

    iget-object v0, v0, Lcom/bugsnag/android/MetaData;->store:Ljava/util/Map;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->appData:Lcom/bugsnag/android/AppData;

    invoke-virtual {v1}, Lcom/bugsnag/android/AppData;->getAppDataMetaData()Ljava/util/Map;

    move-result-object v1

    const-string v2, "app"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 959
    iget-object v0, p0, Lcom/bugsnag/android/Client;->breadcrumbs:Lcom/bugsnag/android/Breadcrumbs;

    invoke-virtual {p1, v0}, Lcom/bugsnag/android/Error;->setBreadcrumbs(Lcom/bugsnag/android/Breadcrumbs;)V

    .line 962
    iget-object v0, p0, Lcom/bugsnag/android/Client;->user:Lcom/bugsnag/android/User;

    invoke-virtual {p1, v0}, Lcom/bugsnag/android/Error;->setUser(Lcom/bugsnag/android/User;)V

    .line 965
    invoke-virtual {p1}, Lcom/bugsnag/android/Error;->getContext()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 966
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0}, Lcom/bugsnag/android/Configuration;->getContext()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 967
    :cond_2
    iget-object v0, p0, Lcom/bugsnag/android/Client;->appData:Lcom/bugsnag/android/AppData;

    invoke-virtual {v0}, Lcom/bugsnag/android/AppData;->getActiveScreenClass()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/bugsnag/android/Error;->setContext(Ljava/lang/String;)V

    .line 971
    :cond_3
    invoke-direct {p0, p1}, Lcom/bugsnag/android/Client;->runBeforeNotifyTasks(Lcom/bugsnag/android/Error;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string p1, "Skipping notification - beforeNotify task returned false"

    .line 972
    invoke-static {p1}, Lcom/bugsnag/android/Logger;->info(Ljava/lang/String;)V

    return-void

    .line 977
    :cond_4
    new-instance v0, Lcom/bugsnag/android/Report;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v1}, Lcom/bugsnag/android/Configuration;->getApiKey()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/bugsnag/android/Report;-><init>(Ljava/lang/String;Lcom/bugsnag/android/Error;)V

    if-eqz p3, :cond_5

    .line 980
    invoke-interface {p3, v0}, Lcom/bugsnag/android/Callback;->beforeNotify(Lcom/bugsnag/android/Report;)V

    .line 983
    :cond_5
    invoke-virtual {p1}, Lcom/bugsnag/android/Error;->getSession()Lcom/bugsnag/android/Session;

    move-result-object p3

    if-eqz p3, :cond_7

    .line 984
    invoke-virtual {p0}, Lcom/bugsnag/android/Client;->setChanged()V

    .line 986
    invoke-virtual {p1}, Lcom/bugsnag/android/Error;->getHandledState()Lcom/bugsnag/android/HandledState;

    move-result-object p3

    invoke-virtual {p3}, Lcom/bugsnag/android/HandledState;->isUnhandled()Z

    move-result p3

    if-eqz p3, :cond_6

    .line 987
    new-instance p3, Lcom/bugsnag/android/NativeInterface$Message;

    sget-object v1, Lcom/bugsnag/android/NativeInterface$MessageType;->NOTIFY_UNHANDLED:Lcom/bugsnag/android/NativeInterface$MessageType;

    const/4 v2, 0x0

    invoke-direct {p3, v1, v2}, Lcom/bugsnag/android/NativeInterface$Message;-><init>(Lcom/bugsnag/android/NativeInterface$MessageType;Ljava/lang/Object;)V

    invoke-virtual {p0, p3}, Lcom/bugsnag/android/Client;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_1

    .line 990
    :cond_6
    new-instance p3, Lcom/bugsnag/android/NativeInterface$Message;

    sget-object v1, Lcom/bugsnag/android/NativeInterface$MessageType;->NOTIFY_HANDLED:Lcom/bugsnag/android/NativeInterface$MessageType;

    .line 991
    invoke-virtual {p1}, Lcom/bugsnag/android/Error;->getExceptionName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p3, v1, v2}, Lcom/bugsnag/android/NativeInterface$Message;-><init>(Lcom/bugsnag/android/NativeInterface$MessageType;Ljava/lang/Object;)V

    .line 990
    invoke-virtual {p0, p3}, Lcom/bugsnag/android/Client;->notifyObservers(Ljava/lang/Object;)V

    .line 995
    :cond_7
    :goto_1
    sget-object p3, Lcom/bugsnag/android/Client$8;->$SwitchMap$com$bugsnag$android$DeliveryStyle:[I

    invoke-virtual {p2}, Lcom/bugsnag/android/DeliveryStyle;->ordinal()I

    move-result p2

    aget p2, p3, p2

    const/4 p3, 0x1

    if-eq p2, p3, :cond_b

    const/4 v1, 0x2

    if-eq p2, v1, :cond_a

    const/4 p3, 0x3

    if-eq p2, p3, :cond_9

    const/4 p3, 0x4

    if-eq p2, p3, :cond_8

    goto :goto_2

    .line 1007
    :cond_8
    iget-object p2, p0, Lcom/bugsnag/android/Client;->errorStore:Lcom/bugsnag/android/ErrorStore;

    invoke-virtual {p2, p1}, Lcom/bugsnag/android/ErrorStore;->write(Lcom/bugsnag/android/JsonStream$Streamable;)Ljava/lang/String;

    .line 1008
    iget-object p1, p0, Lcom/bugsnag/android/Client;->errorStore:Lcom/bugsnag/android/ErrorStore;

    invoke-virtual {p1}, Lcom/bugsnag/android/ErrorStore;->flushAsync()V

    goto :goto_2

    .line 1004
    :cond_9
    invoke-direct {p0, p1, v0}, Lcom/bugsnag/android/Client;->deliverReportAsync(Lcom/bugsnag/android/Error;Lcom/bugsnag/android/Report;)V

    goto :goto_2

    .line 1000
    :cond_a
    invoke-virtual {v0, p3}, Lcom/bugsnag/android/Report;->setCachingDisabled(Z)V

    .line 1001
    invoke-direct {p0, p1, v0}, Lcom/bugsnag/android/Client;->deliverReportAsync(Lcom/bugsnag/android/Error;Lcom/bugsnag/android/Report;)V

    goto :goto_2

    .line 997
    :cond_b
    invoke-virtual {p0, v0, p1}, Lcom/bugsnag/android/Client;->deliver(Lcom/bugsnag/android/Report;Lcom/bugsnag/android/Error;)V

    :goto_2
    return-void
.end method

.method public notify(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/StackTraceElement;Lcom/bugsnag/android/Severity;Lcom/bugsnag/android/MetaData;)V
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # [Ljava/lang/StackTraceElement;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Lcom/bugsnag/android/Severity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p6    # Lcom/bugsnag/android/MetaData;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 916
    new-instance v7, Lcom/bugsnag/android/Error$Builder;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v5, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 917
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    move-object v0, v7

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/StackTraceElement;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;)V

    .line 918
    invoke-virtual {v7, p5}, Lcom/bugsnag/android/Error$Builder;->severity(Lcom/bugsnag/android/Severity;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 919
    invoke-virtual {p1, p6}, Lcom/bugsnag/android/Error$Builder;->metaData(Lcom/bugsnag/android/MetaData;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 920
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    .line 921
    invoke-virtual {p1, p3}, Lcom/bugsnag/android/Error;->setContext(Ljava/lang/String;)V

    const/4 p2, 0x0

    .line 922
    invoke-direct {p0, p1, p2}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Z)V

    return-void
.end method

.method public notify(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/StackTraceElement;Lcom/bugsnag/android/Callback;)V
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # [Ljava/lang/StackTraceElement;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/bugsnag/android/Callback;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 812
    new-instance v7, Lcom/bugsnag/android/Error$Builder;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v5, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 813
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    move-object v0, v7

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/StackTraceElement;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;)V

    const-string p1, "handledException"

    .line 814
    invoke-virtual {v7, p1}, Lcom/bugsnag/android/Error$Builder;->severityReasonType(Ljava/lang/String;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 815
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    .line 816
    sget-object p2, Lcom/bugsnag/android/DeliveryStyle;->ASYNC:Lcom/bugsnag/android/DeliveryStyle;

    invoke-virtual {p0, p1, p2, p4}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Lcom/bugsnag/android/DeliveryStyle;Lcom/bugsnag/android/Callback;)V

    return-void
.end method

.method public notify(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/StackTraceElement;Lcom/bugsnag/android/Severity;Lcom/bugsnag/android/MetaData;)V
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # [Ljava/lang/StackTraceElement;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/bugsnag/android/Severity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Lcom/bugsnag/android/MetaData;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 888
    new-instance v7, Lcom/bugsnag/android/Error$Builder;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v5, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 889
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    move-object v0, v7

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/StackTraceElement;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;)V

    .line 890
    invoke-virtual {v7, p4}, Lcom/bugsnag/android/Error$Builder;->severity(Lcom/bugsnag/android/Severity;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 891
    invoke-virtual {p1, p5}, Lcom/bugsnag/android/Error$Builder;->metaData(Lcom/bugsnag/android/MetaData;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 892
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    const/4 p2, 0x0

    .line 893
    invoke-direct {p0, p1, p2}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Z)V

    return-void
.end method

.method public notify(Ljava/lang/Throwable;)V
    .locals 7
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 777
    new-instance v6, Lcom/bugsnag/android/Error$Builder;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v3, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 778
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/Throwable;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;Z)V

    const-string p1, "handledException"

    .line 779
    invoke-virtual {v6, p1}, Lcom/bugsnag/android/Error$Builder;->severityReasonType(Ljava/lang/String;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 780
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    const/4 v0, 0x0

    .line 781
    invoke-direct {p0, p1, v0}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Z)V

    return-void
.end method

.method public notify(Ljava/lang/Throwable;Lcom/bugsnag/android/Callback;)V
    .locals 7
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bugsnag/android/Callback;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 792
    new-instance v6, Lcom/bugsnag/android/Error$Builder;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v3, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 793
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/Throwable;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;Z)V

    const-string p1, "handledException"

    .line 794
    invoke-virtual {v6, p1}, Lcom/bugsnag/android/Error$Builder;->severityReasonType(Ljava/lang/String;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 795
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    .line 796
    sget-object v0, Lcom/bugsnag/android/DeliveryStyle;->ASYNC:Lcom/bugsnag/android/DeliveryStyle;

    invoke-virtual {p0, p1, v0, p2}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Lcom/bugsnag/android/DeliveryStyle;Lcom/bugsnag/android/Callback;)V

    return-void
.end method

.method public notify(Ljava/lang/Throwable;Lcom/bugsnag/android/MetaData;)V
    .locals 7
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bugsnag/android/MetaData;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 844
    new-instance v6, Lcom/bugsnag/android/Error$Builder;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v3, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 845
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/Throwable;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;Z)V

    .line 846
    invoke-virtual {v6, p2}, Lcom/bugsnag/android/Error$Builder;->metaData(Lcom/bugsnag/android/MetaData;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    const-string p2, "handledException"

    .line 847
    invoke-virtual {p1, p2}, Lcom/bugsnag/android/Error$Builder;->severityReasonType(Ljava/lang/String;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 848
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    const/4 p2, 0x0

    .line 849
    invoke-direct {p0, p1, p2}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Z)V

    return-void
.end method

.method public notify(Ljava/lang/Throwable;Lcom/bugsnag/android/Severity;)V
    .locals 7
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bugsnag/android/Severity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 827
    new-instance v6, Lcom/bugsnag/android/Error$Builder;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v3, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 828
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/Throwable;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;Z)V

    .line 829
    invoke-virtual {v6, p2}, Lcom/bugsnag/android/Error$Builder;->severity(Lcom/bugsnag/android/Severity;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 830
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    const/4 p2, 0x0

    .line 831
    invoke-direct {p0, p1, p2}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Z)V

    return-void
.end method

.method public notify(Ljava/lang/Throwable;Lcom/bugsnag/android/Severity;Lcom/bugsnag/android/MetaData;)V
    .locals 7
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bugsnag/android/Severity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/bugsnag/android/MetaData;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 864
    new-instance v6, Lcom/bugsnag/android/Error$Builder;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v3, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 865
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/Throwable;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;Z)V

    .line 866
    invoke-virtual {v6, p3}, Lcom/bugsnag/android/Error$Builder;->metaData(Lcom/bugsnag/android/MetaData;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 867
    invoke-virtual {p1, p2}, Lcom/bugsnag/android/Error$Builder;->severity(Lcom/bugsnag/android/Severity;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 868
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    const/4 p2, 0x0

    .line 869
    invoke-direct {p0, p1, p2}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Z)V

    return-void
.end method

.method public notifyBlocking(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/StackTraceElement;Lcom/bugsnag/android/Severity;Lcom/bugsnag/android/MetaData;)V
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # [Ljava/lang/StackTraceElement;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Lcom/bugsnag/android/Severity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p6    # Lcom/bugsnag/android/MetaData;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1224
    new-instance v7, Lcom/bugsnag/android/Error$Builder;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v5, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 1225
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    move-object v0, v7

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/StackTraceElement;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;)V

    .line 1226
    invoke-virtual {v7, p5}, Lcom/bugsnag/android/Error$Builder;->severity(Lcom/bugsnag/android/Severity;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1227
    invoke-virtual {p1, p6}, Lcom/bugsnag/android/Error$Builder;->metaData(Lcom/bugsnag/android/MetaData;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1228
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    .line 1229
    invoke-virtual {p1, p3}, Lcom/bugsnag/android/Error;->setContext(Ljava/lang/String;)V

    const/4 p2, 0x1

    .line 1230
    invoke-direct {p0, p1, p2}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Z)V

    return-void
.end method

.method public notifyBlocking(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/StackTraceElement;Lcom/bugsnag/android/Callback;)V
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # [Ljava/lang/StackTraceElement;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/bugsnag/android/Callback;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1133
    new-instance v7, Lcom/bugsnag/android/Error$Builder;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v5, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 1134
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    move-object v0, v7

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/StackTraceElement;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;)V

    const-string p1, "handledException"

    .line 1135
    invoke-virtual {v7, p1}, Lcom/bugsnag/android/Error$Builder;->severityReasonType(Ljava/lang/String;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1136
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    .line 1137
    sget-object p2, Lcom/bugsnag/android/DeliveryStyle;->SAME_THREAD:Lcom/bugsnag/android/DeliveryStyle;

    invoke-virtual {p0, p1, p2, p4}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Lcom/bugsnag/android/DeliveryStyle;Lcom/bugsnag/android/Callback;)V

    return-void
.end method

.method public notifyBlocking(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/StackTraceElement;Lcom/bugsnag/android/Severity;Lcom/bugsnag/android/MetaData;)V
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # [Ljava/lang/StackTraceElement;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/bugsnag/android/Severity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Lcom/bugsnag/android/MetaData;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1196
    new-instance v7, Lcom/bugsnag/android/Error$Builder;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v5, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 1197
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    move-object v0, v7

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/StackTraceElement;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;)V

    .line 1198
    invoke-virtual {v7, p4}, Lcom/bugsnag/android/Error$Builder;->severity(Lcom/bugsnag/android/Severity;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1199
    invoke-virtual {p1, p5}, Lcom/bugsnag/android/Error$Builder;->metaData(Lcom/bugsnag/android/MetaData;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1200
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    const/4 p2, 0x1

    .line 1201
    invoke-direct {p0, p1, p2}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Z)V

    return-void
.end method

.method public notifyBlocking(Ljava/lang/Throwable;)V
    .locals 7
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1098
    new-instance v6, Lcom/bugsnag/android/Error$Builder;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v3, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 1099
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/Throwable;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;Z)V

    const-string p1, "handledException"

    .line 1100
    invoke-virtual {v6, p1}, Lcom/bugsnag/android/Error$Builder;->severityReasonType(Ljava/lang/String;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1101
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    const/4 v0, 0x1

    .line 1102
    invoke-direct {p0, p1, v0}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Z)V

    return-void
.end method

.method public notifyBlocking(Ljava/lang/Throwable;Lcom/bugsnag/android/Callback;)V
    .locals 7
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bugsnag/android/Callback;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1113
    new-instance v6, Lcom/bugsnag/android/Error$Builder;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v3, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 1114
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/Throwable;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;Z)V

    const-string p1, "handledException"

    .line 1115
    invoke-virtual {v6, p1}, Lcom/bugsnag/android/Error$Builder;->severityReasonType(Ljava/lang/String;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1116
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    .line 1117
    sget-object v0, Lcom/bugsnag/android/DeliveryStyle;->SAME_THREAD:Lcom/bugsnag/android/DeliveryStyle;

    invoke-virtual {p0, p1, v0, p2}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Lcom/bugsnag/android/DeliveryStyle;Lcom/bugsnag/android/Callback;)V

    return-void
.end method

.method public notifyBlocking(Ljava/lang/Throwable;Lcom/bugsnag/android/MetaData;)V
    .locals 7
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bugsnag/android/MetaData;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1150
    new-instance v6, Lcom/bugsnag/android/Error$Builder;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v3, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 1151
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/Throwable;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;Z)V

    const-string p1, "handledException"

    .line 1152
    invoke-virtual {v6, p1}, Lcom/bugsnag/android/Error$Builder;->severityReasonType(Ljava/lang/String;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1153
    invoke-virtual {p1, p2}, Lcom/bugsnag/android/Error$Builder;->metaData(Lcom/bugsnag/android/MetaData;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1154
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    const/4 p2, 0x1

    .line 1155
    invoke-direct {p0, p1, p2}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Z)V

    return-void
.end method

.method public notifyBlocking(Ljava/lang/Throwable;Lcom/bugsnag/android/Severity;)V
    .locals 7
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bugsnag/android/Severity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1241
    new-instance v6, Lcom/bugsnag/android/Error$Builder;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v3, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 1242
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/Throwable;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;Z)V

    .line 1243
    invoke-virtual {v6, p2}, Lcom/bugsnag/android/Error$Builder;->severity(Lcom/bugsnag/android/Severity;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1244
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    const/4 p2, 0x1

    .line 1245
    invoke-direct {p0, p1, p2}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Z)V

    return-void
.end method

.method public notifyBlocking(Ljava/lang/Throwable;Lcom/bugsnag/android/Severity;Lcom/bugsnag/android/MetaData;)V
    .locals 7
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bugsnag/android/Severity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/bugsnag/android/MetaData;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1170
    new-instance v6, Lcom/bugsnag/android/Error$Builder;

    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    iget-object v3, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    .line 1171
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/bugsnag/android/Error$Builder;-><init>(Lcom/bugsnag/android/Configuration;Ljava/lang/Throwable;Lcom/bugsnag/android/SessionTracker;Ljava/lang/Thread;Z)V

    .line 1172
    invoke-virtual {v6, p3}, Lcom/bugsnag/android/Error$Builder;->metaData(Lcom/bugsnag/android/MetaData;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1173
    invoke-virtual {p1, p2}, Lcom/bugsnag/android/Error$Builder;->severity(Lcom/bugsnag/android/Severity;)Lcom/bugsnag/android/Error$Builder;

    move-result-object p1

    .line 1174
    invoke-virtual {p1}, Lcom/bugsnag/android/Error$Builder;->build()Lcom/bugsnag/android/Error;

    move-result-object p1

    const/4 p2, 0x1

    .line 1175
    invoke-direct {p0, p1, p2}, Lcom/bugsnag/android/Client;->notify(Lcom/bugsnag/android/Error;Z)V

    return-void
.end method

.method recordStorageCacheBehavior(Lcom/bugsnag/android/MetaData;)V
    .locals 4

    const-string v0, "BugsnagDiagnostics"

    .line 277
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-lt v1, v2, :cond_0

    .line 278
    iget-object v1, p0, Lcom/bugsnag/android/Client;->appContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    .line 279
    new-instance v2, Ljava/io/File;

    const-string v3, "bugsnag-errors"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 282
    :try_start_0
    iget-object v1, p0, Lcom/bugsnag/android/Client;->storageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v1, v2}, Landroid/os/storage/StorageManager;->isCacheBehaviorTombstone(Ljava/io/File;)Z

    move-result v1

    .line 283
    iget-object v3, p0, Lcom/bugsnag/android/Client;->storageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v3, v2}, Landroid/os/storage/StorageManager;->isCacheBehaviorGroup(Ljava/io/File;)Z

    move-result v2

    const-string v3, "cacheTombstone"

    .line 284
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v3, v1}, Lcom/bugsnag/android/MetaData;->addToTab(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v1, "cacheGroup"

    .line 285
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/bugsnag/android/MetaData;->addToTab(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "Failed to record cache behaviour, skipping diagnostics"

    .line 287
    invoke-static {v0, p1}, Lcom/bugsnag/android/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method

.method reportInternalBugsnagError(Lcom/bugsnag/android/Error;)V
    .locals 5
    .param p1    # Lcom/bugsnag/android/Error;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1021
    iget-object v0, p0, Lcom/bugsnag/android/Client;->appData:Lcom/bugsnag/android/AppData;

    invoke-virtual {v0}, Lcom/bugsnag/android/AppData;->getAppDataSummary()Ljava/util/Map;

    move-result-object v0

    .line 1022
    invoke-static {}, Lcom/bugsnag/android/AppData;->getDurationMs()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "duration"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1023
    iget-object v1, p0, Lcom/bugsnag/android/Client;->appData:Lcom/bugsnag/android/AppData;

    invoke-virtual {v1}, Lcom/bugsnag/android/AppData;->calculateDurationInForeground()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "durationInForeground"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1024
    iget-object v1, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    invoke-virtual {v1}, Lcom/bugsnag/android/SessionTracker;->isInForeground()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "inForeground"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1025
    invoke-virtual {p1, v0}, Lcom/bugsnag/android/Error;->setAppData(Ljava/util/Map;)V

    .line 1027
    iget-object v0, p0, Lcom/bugsnag/android/Client;->deviceData:Lcom/bugsnag/android/DeviceData;

    invoke-virtual {v0}, Lcom/bugsnag/android/DeviceData;->getDeviceDataSummary()Ljava/util/Map;

    move-result-object v0

    .line 1028
    iget-object v1, p0, Lcom/bugsnag/android/Client;->deviceData:Lcom/bugsnag/android/DeviceData;

    invoke-virtual {v1}, Lcom/bugsnag/android/DeviceData;->calculateFreeDisk()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "freeDisk"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1029
    invoke-virtual {p1, v0}, Lcom/bugsnag/android/Error;->setDeviceData(Ljava/util/Map;)V

    .line 1031
    invoke-virtual {p1}, Lcom/bugsnag/android/Error;->getMetaData()Lcom/bugsnag/android/MetaData;

    move-result-object v0

    .line 1032
    invoke-static {}, Lcom/bugsnag/android/Notifier;->getInstance()Lcom/bugsnag/android/Notifier;

    move-result-object v1

    .line 1033
    invoke-virtual {v1}, Lcom/bugsnag/android/Notifier;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "BugsnagDiagnostics"

    const-string v4, "notifierName"

    invoke-virtual {v0, v3, v4, v2}, Lcom/bugsnag/android/MetaData;->addToTab(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1034
    invoke-virtual {v1}, Lcom/bugsnag/android/Notifier;->getVersion()Ljava/lang/String;

    move-result-object v1

    const-string v2, "notifierVersion"

    invoke-virtual {v0, v3, v2, v1}, Lcom/bugsnag/android/MetaData;->addToTab(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1035
    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v1}, Lcom/bugsnag/android/Configuration;->getApiKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "apiKey"

    invoke-virtual {v0, v3, v2, v1}, Lcom/bugsnag/android/MetaData;->addToTab(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1037
    iget-object v1, p0, Lcom/bugsnag/android/Client;->appData:Lcom/bugsnag/android/AppData;

    invoke-virtual {v1}, Lcom/bugsnag/android/AppData;->getAppData()Ljava/util/Map;

    move-result-object v1

    const-string v2, "packageName"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1038
    invoke-virtual {v0, v3, v2, v1}, Lcom/bugsnag/android/MetaData;->addToTab(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1040
    new-instance v0, Lcom/bugsnag/android/Report;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1}, Lcom/bugsnag/android/Report;-><init>(Ljava/lang/String;Lcom/bugsnag/android/Error;)V

    .line 1042
    :try_start_0
    new-instance p1, Lcom/bugsnag/android/Client$6;

    invoke-direct {p1, p0, v0}, Lcom/bugsnag/android/Client$6;-><init>(Lcom/bugsnag/android/Client;Lcom/bugsnag/android/Report;)V

    invoke-static {p1}, Lcom/bugsnag/android/Async;->run(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public final resumeSession()Z
    .locals 1

    .line 409
    iget-object v0, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    invoke-virtual {v0}, Lcom/bugsnag/android/SessionTracker;->resumeSession()Z

    move-result v0

    return v0
.end method

.method sendNativeSetupNotification()V
    .locals 3

    .line 316
    invoke-virtual {p0}, Lcom/bugsnag/android/Client;->setChanged()V

    .line 317
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 318
    iget-object v1, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 320
    new-instance v1, Lcom/bugsnag/android/NativeInterface$Message;

    sget-object v2, Lcom/bugsnag/android/NativeInterface$MessageType;->INSTALL:Lcom/bugsnag/android/NativeInterface$MessageType;

    invoke-direct {v1, v2, v0}, Lcom/bugsnag/android/NativeInterface$Message;-><init>(Lcom/bugsnag/android/NativeInterface$MessageType;Ljava/lang/Object;)V

    invoke-super {p0, v1}, Ljava/util/Observable;->notifyObservers(Ljava/lang/Object;)V

    .line 322
    :try_start_0
    new-instance v0, Lcom/bugsnag/android/Client$5;

    invoke-direct {v0, p0}, Lcom/bugsnag/android/Client$5;-><init>(Lcom/bugsnag/android/Client;)V

    invoke-static {v0}, Lcom/bugsnag/android/Async;->run(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Failed to enqueue native reports, will retry next launch: "

    .line 329
    invoke-static {v1, v0}, Lcom/bugsnag/android/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public setAppVersion(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 429
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/Configuration;->setAppVersion(Ljava/lang/String;)V

    return-void
.end method

.method public setAutoCaptureSessions(Z)V
    .locals 1

    .line 577
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/Configuration;->setAutoCaptureSessions(Z)V

    if-eqz p1, :cond_0

    .line 580
    iget-object p1, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    invoke-virtual {p1}, Lcom/bugsnag/android/SessionTracker;->onAutoCaptureEnabled()V

    :cond_0
    return-void
.end method

.method setBinaryArch(Ljava/lang/String;)V
    .locals 1

    .line 1567
    invoke-virtual {p0}, Lcom/bugsnag/android/Client;->getAppData()Lcom/bugsnag/android/AppData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/AppData;->setBinaryArch(Ljava/lang/String;)V

    return-void
.end method

.method public setBuildUUID(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 477
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/Configuration;->setBuildUUID(Ljava/lang/String;)V

    return-void
.end method

.method public setContext(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 449
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/Configuration;->setContext(Ljava/lang/String;)V

    return-void
.end method

.method public setEndpoint(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 464
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/Configuration;->setEndpoint(Ljava/lang/String;)V

    return-void
.end method

.method setErrorReportApiClient(Lcom/bugsnag/android/ErrorReportApiClient;)V
    .locals 1
    .param p1    # Lcom/bugsnag/android/ErrorReportApiClient;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    if-eqz p1, :cond_0

    .line 709
    invoke-virtual {p0}, Lcom/bugsnag/android/Client;->getAndSetDeliveryCompat()Lcom/bugsnag/android/DeliveryCompat;

    move-result-object v0

    .line 710
    iput-object p1, v0, Lcom/bugsnag/android/DeliveryCompat;->errorReportApiClient:Lcom/bugsnag/android/ErrorReportApiClient;

    return-void

    .line 707
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "ErrorReportApiClient cannot be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public varargs setFilters([Ljava/lang/String;)V
    .locals 1
    .param p1    # [Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 495
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/Configuration;->setFilters([Ljava/lang/String;)V

    return-void
.end method

.method public varargs setIgnoreClasses([Ljava/lang/String;)V
    .locals 1
    .param p1    # [Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 508
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/Configuration;->setIgnoreClasses([Ljava/lang/String;)V

    return-void
.end method

.method public setLoggingEnabled(Z)V
    .locals 0

    .line 1545
    invoke-static {p1}, Lcom/bugsnag/android/Logger;->setEnabled(Z)V

    return-void
.end method

.method public setMaxBreadcrumbs(I)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1376
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/Configuration;->setMaxBreadcrumbs(I)V

    return-void
.end method

.method public setMetaData(Lcom/bugsnag/android/MetaData;)V
    .locals 1
    .param p1    # Lcom/bugsnag/android/MetaData;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1335
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/Configuration;->setMetaData(Lcom/bugsnag/android/MetaData;)V

    return-void
.end method

.method public varargs setNotifyReleaseStages([Ljava/lang/String;)V
    .locals 1
    .param p1    # [Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 523
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/Configuration;->setNotifyReleaseStages([Ljava/lang/String;)V

    return-void
.end method

.method public varargs setProjectPackages([Ljava/lang/String;)V
    .locals 1
    .param p1    # [Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 541
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/Configuration;->setProjectPackages([Ljava/lang/String;)V

    return-void
.end method

.method public setReleaseStage(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 553
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/Configuration;->setReleaseStage(Ljava/lang/String;)V

    const-string v0, "production"

    .line 554
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/bugsnag/android/Logger;->setEnabled(Z)V

    return-void
.end method

.method public setSendThreads(Z)V
    .locals 1

    .line 564
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/Configuration;->setSendThreads(Z)V

    return-void
.end method

.method setSessionTrackingApiClient(Lcom/bugsnag/android/SessionTrackingApiClient;)V
    .locals 1
    .param p1    # Lcom/bugsnag/android/SessionTrackingApiClient;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    if-eqz p1, :cond_0

    .line 719
    invoke-virtual {p0}, Lcom/bugsnag/android/Client;->getAndSetDeliveryCompat()Lcom/bugsnag/android/DeliveryCompat;

    move-result-object v0

    .line 720
    iput-object p1, v0, Lcom/bugsnag/android/DeliveryCompat;->sessionTrackingApiClient:Lcom/bugsnag/android/SessionTrackingApiClient;

    return-void

    .line 717
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "SessionTrackingApiClient cannot be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setUser(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 597
    invoke-virtual {p0, p1}, Lcom/bugsnag/android/Client;->setUserId(Ljava/lang/String;)V

    .line 598
    invoke-virtual {p0, p2}, Lcom/bugsnag/android/Client;->setUserEmail(Ljava/lang/String;)V

    .line 599
    invoke-virtual {p0, p3}, Lcom/bugsnag/android/Client;->setUserName(Ljava/lang/String;)V

    return-void
.end method

.method public setUserEmail(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 670
    iget-object v0, p0, Lcom/bugsnag/android/Client;->user:Lcom/bugsnag/android/User;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/User;->setEmail(Ljava/lang/String;)V

    .line 672
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0}, Lcom/bugsnag/android/Configuration;->getPersistUserBetweenSessions()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "user.email"

    .line 673
    invoke-direct {p0, v0, p1}, Lcom/bugsnag/android/Client;->storeInSharedPrefs(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setUserId(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 656
    iget-object v0, p0, Lcom/bugsnag/android/Client;->user:Lcom/bugsnag/android/User;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/User;->setId(Ljava/lang/String;)V

    .line 658
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0}, Lcom/bugsnag/android/Configuration;->getPersistUserBetweenSessions()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "user.id"

    .line 659
    invoke-direct {p0, v0, p1}, Lcom/bugsnag/android/Client;->storeInSharedPrefs(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 684
    iget-object v0, p0, Lcom/bugsnag/android/Client;->user:Lcom/bugsnag/android/User;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/User;->setName(Ljava/lang/String;)V

    .line 686
    iget-object v0, p0, Lcom/bugsnag/android/Client;->config:Lcom/bugsnag/android/Configuration;

    invoke-virtual {v0}, Lcom/bugsnag/android/Configuration;->getPersistUserBetweenSessions()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "user.name"

    .line 687
    invoke-direct {p0, v0, p1}, Lcom/bugsnag/android/Client;->storeInSharedPrefs(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public startFirstSession(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 419
    iget-object v0, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/SessionTracker;->startFirstSession(Landroid/app/Activity;)V

    return-void
.end method

.method public startSession()V
    .locals 2

    .line 364
    iget-object v0, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bugsnag/android/SessionTracker;->startSession(Z)Lcom/bugsnag/android/Session;

    return-void
.end method

.method public final stopSession()V
    .locals 1

    .line 383
    iget-object v0, p0, Lcom/bugsnag/android/Client;->sessionTracker:Lcom/bugsnag/android/SessionTracker;

    invoke-virtual {v0}, Lcom/bugsnag/android/SessionTracker;->stopSession()V

    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/util/Observable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 341
    instance-of p1, p2, Lcom/bugsnag/android/NativeInterface$Message;

    if-eqz p1, :cond_0

    .line 342
    invoke-virtual {p0}, Lcom/bugsnag/android/Client;->setChanged()V

    .line 343
    invoke-super {p0, p2}, Ljava/util/Observable;->notifyObservers(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
