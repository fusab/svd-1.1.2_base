.class public Lcom/bugsnag/android/NativeInterface$Message;
.super Ljava/lang/Object;
.source "NativeInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bugsnag/android/NativeInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Message"
.end annotation


# instance fields
.field public final type:Lcom/bugsnag/android/NativeInterface$MessageType;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final value:Ljava/lang/Object;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/bugsnag/android/NativeInterface$MessageType;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lcom/bugsnag/android/NativeInterface$MessageType;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    iput-object p1, p0, Lcom/bugsnag/android/NativeInterface$Message;->type:Lcom/bugsnag/android/NativeInterface$MessageType;

    .line 139
    iput-object p2, p0, Lcom/bugsnag/android/NativeInterface$Message;->value:Ljava/lang/Object;

    return-void
.end method
