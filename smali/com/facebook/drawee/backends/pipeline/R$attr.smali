.class public final Lcom/facebook/drawee/backends/pipeline/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/drawee/backends/pipeline/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final actualImageResource:I = 0x7f040022

.field public static final actualImageScaleType:I = 0x7f040023

.field public static final actualImageUri:I = 0x7f040024

.field public static final backgroundImage:I = 0x7f04003a

.field public static final fadeDuration:I = 0x7f040112

.field public static final failureImage:I = 0x7f040113

.field public static final failureImageScaleType:I = 0x7f040114

.field public static final overlayImage:I = 0x7f0401a5

.field public static final placeholderImage:I = 0x7f0401b2

.field public static final placeholderImageScaleType:I = 0x7f0401b3

.field public static final pressedStateOverlayImage:I = 0x7f0401bb

.field public static final progressBarAutoRotateInterval:I = 0x7f0401bd

.field public static final progressBarImage:I = 0x7f0401be

.field public static final progressBarImageScaleType:I = 0x7f0401bf

.field public static final retryImage:I = 0x7f0401ca

.field public static final retryImageScaleType:I = 0x7f0401cb

.field public static final roundAsCircle:I = 0x7f0401cf

.field public static final roundBottomEnd:I = 0x7f0401d0

.field public static final roundBottomLeft:I = 0x7f0401d1

.field public static final roundBottomRight:I = 0x7f0401d2

.field public static final roundBottomStart:I = 0x7f0401d3

.field public static final roundTopEnd:I = 0x7f0401d4

.field public static final roundTopLeft:I = 0x7f0401d5

.field public static final roundTopRight:I = 0x7f0401d6

.field public static final roundTopStart:I = 0x7f0401d7

.field public static final roundWithOverlayColor:I = 0x7f0401d8

.field public static final roundedCornerRadius:I = 0x7f0401d9

.field public static final roundingBorderColor:I = 0x7f0401da

.field public static final roundingBorderPadding:I = 0x7f0401db

.field public static final roundingBorderWidth:I = 0x7f0401dc

.field public static final viewAspectRatio:I = 0x7f04027a


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
