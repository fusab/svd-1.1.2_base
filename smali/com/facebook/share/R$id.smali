.class public final Lcom/facebook/share/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/share/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action0:I = 0x7f09000b

.field public static final action_bar:I = 0x7f09000c

.field public static final action_bar_activity_content:I = 0x7f09000d

.field public static final action_bar_container:I = 0x7f09000f

.field public static final action_bar_root:I = 0x7f090010

.field public static final action_bar_spinner:I = 0x7f090011

.field public static final action_bar_subtitle:I = 0x7f090012

.field public static final action_bar_title:I = 0x7f090013

.field public static final action_container:I = 0x7f090014

.field public static final action_context_bar:I = 0x7f090015

.field public static final action_divider:I = 0x7f090016

.field public static final action_image:I = 0x7f090017

.field public static final action_menu_divider:I = 0x7f090018

.field public static final action_menu_presenter:I = 0x7f090019

.field public static final action_mode_bar:I = 0x7f09001a

.field public static final action_mode_bar_stub:I = 0x7f09001b

.field public static final action_mode_close_button:I = 0x7f09001c

.field public static final action_text:I = 0x7f09001d

.field public static final actions:I = 0x7f09001e

.field public static final activity_chooser_view_content:I = 0x7f09001f

.field public static final add:I = 0x7f090022

.field public static final alertTitle:I = 0x7f09002d

.field public static final async:I = 0x7f090038

.field public static final blocking:I = 0x7f09003d

.field public static final bottom:I = 0x7f09003f

.field public static final box_count:I = 0x7f090040

.field public static final button:I = 0x7f090046

.field public static final buttonPanel:I = 0x7f090047

.field public static final cancel_action:I = 0x7f09004e

.field public static final cancel_button:I = 0x7f09004f

.field public static final center:I = 0x7f090055

.field public static final checkbox:I = 0x7f09005e

.field public static final chronometer:I = 0x7f09005f

.field public static final com_facebook_device_auth_instructions:I = 0x7f090069

.field public static final com_facebook_fragment_container:I = 0x7f09006a

.field public static final com_facebook_login_fragment_progress_bar:I = 0x7f09006b

.field public static final com_facebook_smart_instructions_0:I = 0x7f09006c

.field public static final com_facebook_smart_instructions_or:I = 0x7f09006d

.field public static final confirmation_code:I = 0x7f090071

.field public static final contentPanel:I = 0x7f090078

.field public static final custom:I = 0x7f090088

.field public static final customPanel:I = 0x7f090089

.field public static final decor_content_parent:I = 0x7f09008c

.field public static final default_activity_button:I = 0x7f09008d

.field public static final edit_query:I = 0x7f090099

.field public static final end_padder:I = 0x7f09009c

.field public static final expand_activities_button:I = 0x7f0900bc

.field public static final expanded_menu:I = 0x7f0900bd

.field public static final forever:I = 0x7f0900d0

.field public static final home:I = 0x7f0900df

.field public static final icon:I = 0x7f0900e5

.field public static final icon_group:I = 0x7f0900e6

.field public static final image:I = 0x7f0900e9

.field public static final info:I = 0x7f0900eb

.field public static final inline:I = 0x7f0900ec

.field public static final italic:I = 0x7f0900f1

.field public static final left:I = 0x7f0900fb

.field public static final line1:I = 0x7f090103

.field public static final line3:I = 0x7f090104

.field public static final listMode:I = 0x7f090106

.field public static final list_item:I = 0x7f090107

.field public static final media_actions:I = 0x7f090110

.field public static final message:I = 0x7f090111

.field public static final multiply:I = 0x7f090118

.field public static final none:I = 0x7f09011d

.field public static final normal:I = 0x7f09011e

.field public static final notification_background:I = 0x7f09011f

.field public static final notification_main_column:I = 0x7f090120

.field public static final notification_main_column_container:I = 0x7f090121

.field public static final open_graph:I = 0x7f090126

.field public static final page:I = 0x7f090129

.field public static final parentPanel:I = 0x7f09012b

.field public static final progress_bar:I = 0x7f090138

.field public static final progress_circular:I = 0x7f090139

.field public static final progress_horizontal:I = 0x7f09013a

.field public static final radio:I = 0x7f09013c

.field public static final right:I = 0x7f090144

.field public static final right_icon:I = 0x7f09014b

.field public static final right_side:I = 0x7f09014c

.field public static final screen:I = 0x7f09015f

.field public static final scrollIndicatorDown:I = 0x7f090161

.field public static final scrollIndicatorUp:I = 0x7f090162

.field public static final scrollView:I = 0x7f090163

.field public static final search_badge:I = 0x7f090166

.field public static final search_bar:I = 0x7f090167

.field public static final search_button:I = 0x7f090168

.field public static final search_close_btn:I = 0x7f090169

.field public static final search_edit_frame:I = 0x7f09016a

.field public static final search_go_btn:I = 0x7f09016b

.field public static final search_mag_icon:I = 0x7f09016c

.field public static final search_plate:I = 0x7f09016d

.field public static final search_src_text:I = 0x7f09016e

.field public static final search_voice_btn:I = 0x7f09016f

.field public static final select_dialog_listview:I = 0x7f090171

.field public static final shortcut:I = 0x7f090175

.field public static final spacer:I = 0x7f090182

.field public static final split_action_bar:I = 0x7f090184

.field public static final src_atop:I = 0x7f090185

.field public static final src_in:I = 0x7f090186

.field public static final src_over:I = 0x7f090187

.field public static final standard:I = 0x7f090188

.field public static final status_bar_latest_event_content:I = 0x7f09018c

.field public static final submenuarrow:I = 0x7f090191

.field public static final submit_area:I = 0x7f090192

.field public static final tabMode:I = 0x7f090194

.field public static final tag_transition_group:I = 0x7f090195

.field public static final text:I = 0x7f09019c

.field public static final text2:I = 0x7f09019d

.field public static final textSpacerNoButtons:I = 0x7f09019e

.field public static final textSpacerNoTitle:I = 0x7f09019f

.field public static final time:I = 0x7f0901a7

.field public static final title:I = 0x7f0901a8

.field public static final titleDividerNoCustom:I = 0x7f0901a9

.field public static final title_template:I = 0x7f0901aa

.field public static final top:I = 0x7f0901ad

.field public static final topPanel:I = 0x7f0901ae

.field public static final uniform:I = 0x7f0901b5

.field public static final unknown:I = 0x7f0901b6

.field public static final up:I = 0x7f0901b8

.field public static final wrap_content:I = 0x7f0901c5


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 664
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
