.class public final Lcom/facebook/ads/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/ads/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final common_google_signin_btn_text_dark:I = 0x7f060053

.field public static final common_google_signin_btn_text_dark_default:I = 0x7f060054

.field public static final common_google_signin_btn_text_dark_disabled:I = 0x7f060055

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f060056

.field public static final common_google_signin_btn_text_dark_pressed:I = 0x7f060057

.field public static final common_google_signin_btn_text_light:I = 0x7f060058

.field public static final common_google_signin_btn_text_light_default:I = 0x7f060059

.field public static final common_google_signin_btn_text_light_disabled:I = 0x7f06005a

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f06005b

.field public static final common_google_signin_btn_text_light_pressed:I = 0x7f06005c

.field public static final common_google_signin_btn_tint:I = 0x7f06005d

.field public static final notification_action_color_filter:I = 0x7f0600a8

.field public static final notification_icon_bg_color:I = 0x7f0600a9

.field public static final notification_material_background_media_default_color:I = 0x7f0600aa

.field public static final primary_text_default_material_dark:I = 0x7f0600af

.field public static final ripple_material_light:I = 0x7f0600b5

.field public static final secondary_text_default_material_dark:I = 0x7f0600b6

.field public static final secondary_text_default_material_light:I = 0x7f0600b7

.field public static final wallet_bright_foreground_disabled_holo_light:I = 0x7f0600c5

.field public static final wallet_bright_foreground_holo_dark:I = 0x7f0600c6

.field public static final wallet_bright_foreground_holo_light:I = 0x7f0600c7

.field public static final wallet_dim_foreground_disabled_holo_dark:I = 0x7f0600c8

.field public static final wallet_dim_foreground_holo_dark:I = 0x7f0600c9

.field public static final wallet_highlighted_text_holo_dark:I = 0x7f0600ca

.field public static final wallet_highlighted_text_holo_light:I = 0x7f0600cb

.field public static final wallet_hint_foreground_holo_dark:I = 0x7f0600cc

.field public static final wallet_hint_foreground_holo_light:I = 0x7f0600cd

.field public static final wallet_holo_blue_light:I = 0x7f0600ce

.field public static final wallet_link_text_light:I = 0x7f0600cf

.field public static final wallet_primary_text_holo_light:I = 0x7f0600d0

.field public static final wallet_secondary_text_holo_dark:I = 0x7f0600d1


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
