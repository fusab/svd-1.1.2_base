.class public final Lcom/facebook/ads/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/ads/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final ambientEnabled:I = 0x7f04002e

.field public static final appTheme:I = 0x7f04002f

.field public static final buttonSize:I = 0x7f04005f

.field public static final buyButtonAppearance:I = 0x7f040064

.field public static final buyButtonHeight:I = 0x7f040065

.field public static final buyButtonText:I = 0x7f040066

.field public static final buyButtonWidth:I = 0x7f040067

.field public static final cameraBearing:I = 0x7f040068

.field public static final cameraMaxZoomPreference:I = 0x7f040069

.field public static final cameraMinZoomPreference:I = 0x7f04006a

.field public static final cameraTargetLat:I = 0x7f04006b

.field public static final cameraTargetLng:I = 0x7f04006c

.field public static final cameraTilt:I = 0x7f04006d

.field public static final cameraZoom:I = 0x7f04006e

.field public static final circleCrop:I = 0x7f04008e

.field public static final colorScheme:I = 0x7f0400a5

.field public static final environment:I = 0x7f040100

.field public static final fastScrollEnabled:I = 0x7f040115

.field public static final fastScrollHorizontalThumbDrawable:I = 0x7f040116

.field public static final fastScrollHorizontalTrackDrawable:I = 0x7f040117

.field public static final fastScrollVerticalThumbDrawable:I = 0x7f040118

.field public static final fastScrollVerticalTrackDrawable:I = 0x7f040119

.field public static final font:I = 0x7f04011d

.field public static final fontProviderAuthority:I = 0x7f04011f

.field public static final fontProviderCerts:I = 0x7f040120

.field public static final fontProviderFetchStrategy:I = 0x7f040121

.field public static final fontProviderFetchTimeout:I = 0x7f040122

.field public static final fontProviderPackage:I = 0x7f040123

.field public static final fontProviderQuery:I = 0x7f040124

.field public static final fontStyle:I = 0x7f040125

.field public static final fontWeight:I = 0x7f040127

.field public static final fragmentMode:I = 0x7f040129

.field public static final fragmentStyle:I = 0x7f04012a

.field public static final imageAspectRatio:I = 0x7f040146

.field public static final imageAspectRatioAdjust:I = 0x7f040147

.field public static final latLngBoundsNorthEastLatitude:I = 0x7f04015e

.field public static final latLngBoundsNorthEastLongitude:I = 0x7f04015f

.field public static final latLngBoundsSouthWestLatitude:I = 0x7f040160

.field public static final latLngBoundsSouthWestLongitude:I = 0x7f040161

.field public static final layoutManager:I = 0x7f040163

.field public static final liteMode:I = 0x7f04017c

.field public static final mapType:I = 0x7f04018b

.field public static final maskedWalletDetailsBackground:I = 0x7f04018c

.field public static final maskedWalletDetailsButtonBackground:I = 0x7f04018d

.field public static final maskedWalletDetailsButtonTextAppearance:I = 0x7f04018e

.field public static final maskedWalletDetailsHeaderTextAppearance:I = 0x7f04018f

.field public static final maskedWalletDetailsLogoImageType:I = 0x7f040190

.field public static final maskedWalletDetailsLogoTextColor:I = 0x7f040191

.field public static final maskedWalletDetailsTextAppearance:I = 0x7f040192

.field public static final reverseLayout:I = 0x7f0401cc

.field public static final scopeUris:I = 0x7f0401de

.field public static final spanCount:I = 0x7f0401fb

.field public static final stackFromEnd:I = 0x7f040201

.field public static final toolbarTextColorStyle:I = 0x7f040264

.field public static final uiCompass:I = 0x7f04026d

.field public static final uiMapToolbar:I = 0x7f04026e

.field public static final uiRotateGestures:I = 0x7f04026f

.field public static final uiScrollGestures:I = 0x7f040270

.field public static final uiTiltGestures:I = 0x7f040272

.field public static final uiZoomControls:I = 0x7f040273

.field public static final uiZoomGestures:I = 0x7f040274

.field public static final useViewLifecycle:I = 0x7f040277

.field public static final windowTransitionStyle:I = 0x7f040287

.field public static final zOrderOnTop:I = 0x7f040288


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
