.class public Lcom/facebook/ads/internal/u/c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/ads/internal/u/c$a;,
        Lcom/facebook/ads/internal/u/c$c;,
        Lcom/facebook/ads/internal/u/c$b;
    }
.end annotation


# static fields
.field private static a:Lcom/facebook/ads/internal/u/c$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private static final j:Lcom/facebook/ads/internal/w/b/n;

.field private static final k:Ljava/util/concurrent/ThreadPoolExecutor;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/facebook/ads/internal/u/d;

.field private final d:Lcom/facebook/ads/internal/r/a;

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/facebook/ads/internal/u/c$b;

.field private g:Lcom/facebook/ads/internal/u/b;

.field private h:Lcom/facebook/ads/internal/v/a/a;

.field private final i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/facebook/ads/internal/w/b/n;

    invoke-direct {v0}, Lcom/facebook/ads/internal/w/b/n;-><init>()V

    sput-object v0, Lcom/facebook/ads/internal/u/c;->j:Lcom/facebook/ads/internal/w/b/n;

    sget-object v0, Lcom/facebook/ads/internal/u/c;->j:Lcom/facebook/ads/internal/w/b/n;

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ThreadPoolExecutor;

    sput-object v0, Lcom/facebook/ads/internal/u/c;->k:Ljava/util/concurrent/ThreadPoolExecutor;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/ads/internal/u/c;->b:Landroid/content/Context;

    invoke-static {}, Lcom/facebook/ads/internal/u/d;->a()Lcom/facebook/ads/internal/u/d;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/ads/internal/u/c;->c:Lcom/facebook/ads/internal/u/d;

    iget-object p1, p0, Lcom/facebook/ads/internal/u/c;->b:Landroid/content/Context;

    invoke-static {p1}, Lcom/facebook/ads/internal/r/a;->ah(Landroid/content/Context;)Lcom/facebook/ads/internal/r/a;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/ads/internal/u/c;->d:Lcom/facebook/ads/internal/r/a;

    invoke-static {}, Lcom/facebook/ads/internal/settings/AdInternalSettings;->getUrlPrefix()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, "https://graph.facebook.com/network_ads_common"

    goto :goto_0

    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string p1, "https://graph.%s.facebook.com/network_ads_common"

    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/facebook/ads/internal/u/c;->i:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/facebook/ads/internal/u/c;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/facebook/ads/internal/u/c;->b:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic a(Lcom/facebook/ads/internal/u/c;Lcom/facebook/ads/internal/v/a/a;)Lcom/facebook/ads/internal/v/a/a;
    .locals 0

    iput-object p1, p0, Lcom/facebook/ads/internal/u/c;->h:Lcom/facebook/ads/internal/v/a/a;

    return-object p1
.end method

.method static synthetic a(Lcom/facebook/ads/internal/u/c;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    iput-object p1, p0, Lcom/facebook/ads/internal/u/c;->e:Ljava/util/Map;

    return-object p1
.end method

.method private a(Lcom/facebook/ads/internal/protocol/a;)V
    .locals 1

    iget-object v0, p0, Lcom/facebook/ads/internal/u/c;->f:Lcom/facebook/ads/internal/u/c$b;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/facebook/ads/internal/u/c$b;->a(Lcom/facebook/ads/internal/protocol/a;)V

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/ads/internal/u/c;->a()V

    return-void
.end method

.method static synthetic a(Lcom/facebook/ads/internal/u/c;Lcom/facebook/ads/internal/protocol/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/facebook/ads/internal/u/c;->a(Lcom/facebook/ads/internal/protocol/a;)V

    return-void
.end method

.method static synthetic a(Lcom/facebook/ads/internal/u/c;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/facebook/ads/internal/u/c;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/facebook/ads/internal/u/f;)V
    .locals 1

    iget-object v0, p0, Lcom/facebook/ads/internal/u/c;->f:Lcom/facebook/ads/internal/u/c$b;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/facebook/ads/internal/u/c$b;->a(Lcom/facebook/ads/internal/u/f;)V

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/ads/internal/u/c;->a()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 13

    const-string v0, "SHA-1"

    const-string v1, "iso-8859-1"

    :try_start_0
    iget-object v2, p0, Lcom/facebook/ads/internal/u/c;->c:Lcom/facebook/ads/internal/u/d;

    invoke-virtual {v2, p1}, Lcom/facebook/ads/internal/u/d;->a(Ljava/lang/String;)Lcom/facebook/ads/internal/u/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ads/internal/u/e;->a()Lcom/facebook/ads/internal/m/c;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v3, :cond_8

    iget-object v7, p0, Lcom/facebook/ads/internal/u/c;->d:Lcom/facebook/ads/internal/r/a;

    invoke-virtual {v3}, Lcom/facebook/ads/internal/m/c;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/ads/internal/r/a;->a(Ljava/lang/String;)V

    sget-boolean v7, Lcom/facebook/ads/internal/settings/AdInternalSettings;->d:Z

    if-eqz v7, :cond_7

    iget-object v7, p0, Lcom/facebook/ads/internal/u/c;->b:Landroid/content/Context;

    invoke-static {v7}, Lcom/facebook/ads/internal/r/a;->V(Landroid/content/Context;)Z

    move-result v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    const-string v8, "ipc"

    const-string v9, "com.facebook.ads.ipc"

    if-eqz v7, :cond_3

    :try_start_1
    iget-object v7, p0, Lcom/facebook/ads/internal/u/c;->b:Landroid/content/Context;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    if-nez v7, :cond_0

    const/4 v10, 0x0

    goto :goto_0

    :cond_0
    const/4 v10, 0x1

    :goto_0
    if-eqz v10, :cond_1

    :try_start_2
    new-instance v11, Ljava/io/File;

    invoke-virtual {v7}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v12

    invoke-direct {v11, v12, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_1

    invoke-virtual {v11}, Ljava/io/File;->createNewFile()Z

    move-result v10

    goto :goto_1

    :catch_0
    move-exception v9

    goto :goto_2

    :cond_1
    :goto_1
    if-nez v10, :cond_2

    new-instance v9, Ljava/lang/Exception;

    const-string v10, "Can\'t create ipc marker."

    invoke-direct {v9, v10}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    :cond_2
    move-object v9, v4

    :goto_2
    if-eqz v9, :cond_7

    :try_start_3
    sget v10, Lcom/facebook/ads/internal/w/h/b;->ac:I

    :goto_3
    invoke-static {v7, v8, v10, v9}, Lcom/facebook/ads/internal/w/h/a;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/Exception;)V

    goto :goto_7

    :cond_3
    iget-object v7, p0, Lcom/facebook/ads/internal/u/c;->b:Landroid/content/Context;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    if-nez v7, :cond_4

    const/4 v10, 0x0

    goto :goto_4

    :cond_4
    const/4 v10, 0x1

    :goto_4
    if-eqz v10, :cond_5

    :try_start_4
    new-instance v11, Ljava/io/File;

    invoke-virtual {v7}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v12

    invoke-direct {v11, v12, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {v11}, Ljava/io/File;->delete()Z

    move-result v10

    goto :goto_5

    :catch_1
    move-exception v9

    goto :goto_6

    :cond_5
    :goto_5
    if-nez v10, :cond_6

    new-instance v9, Ljava/lang/Exception;

    const-string v10, "Can\'t delete ipc marker."

    invoke-direct {v9, v10}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_6

    :cond_6
    move-object v9, v4

    :goto_6
    if-eqz v9, :cond_7

    :try_start_5
    sget v10, Lcom/facebook/ads/internal/w/h/b;->ac:I

    goto :goto_3

    :cond_7
    :goto_7
    iget-object v7, p0, Lcom/facebook/ads/internal/u/c;->b:Landroid/content/Context;

    invoke-virtual {v3}, Lcom/facebook/ads/internal/m/c;->c()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/facebook/ads/internal/f/a;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/facebook/ads/internal/m/c;->a()Lcom/facebook/ads/internal/m/d;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/ads/internal/m/d;->d()J

    move-result-wide v7

    iget-object v9, p0, Lcom/facebook/ads/internal/u/c;->g:Lcom/facebook/ads/internal/u/b;

    invoke-static {v7, v8, v9}, Lcom/facebook/ads/internal/u/a;->a(JLcom/facebook/ads/internal/u/b;)V

    iget-object v7, p0, Lcom/facebook/ads/internal/u/c;->b:Landroid/content/Context;

    sget-object v8, Lcom/facebook/ads/internal/u/c;->k:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-static {v7, v8, v3}, Lcom/facebook/ads/internal/w/g/a;->a(Landroid/content/Context;Ljava/util/concurrent/ThreadPoolExecutor;Lcom/facebook/ads/internal/m/c;)V

    :cond_8
    sget-object v7, Lcom/facebook/ads/internal/u/c$3;->a:[I

    invoke-virtual {v2}, Lcom/facebook/ads/internal/u/e;->b()Lcom/facebook/ads/internal/u/e$a;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/ads/internal/u/e$a;->ordinal()I

    move-result v8

    aget v7, v7, v8

    if-eq v7, v5, :cond_b

    const/4 v0, 0x2

    if-eq v7, v0, :cond_9

    sget-object v0, Lcom/facebook/ads/internal/protocol/AdErrorType;->UNKNOWN_RESPONSE:Lcom/facebook/ads/internal/protocol/AdErrorType;

    invoke-static {v0, p1}, Lcom/facebook/ads/internal/protocol/a;->a(Lcom/facebook/ads/internal/protocol/AdErrorType;Ljava/lang/String;)Lcom/facebook/ads/internal/protocol/a;

    move-result-object p1

    :goto_8
    invoke-direct {p0, p1}, Lcom/facebook/ads/internal/u/c;->a(Lcom/facebook/ads/internal/protocol/a;)V

    goto/16 :goto_c

    :cond_9
    check-cast v2, Lcom/facebook/ads/internal/u/g;

    invoke-virtual {v2}, Lcom/facebook/ads/internal/u/g;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/ads/internal/u/g;->g()I

    move-result v1

    sget-object v2, Lcom/facebook/ads/internal/protocol/AdErrorType;->ERROR_MESSAGE:Lcom/facebook/ads/internal/protocol/AdErrorType;

    invoke-static {v1, v2}, Lcom/facebook/ads/internal/protocol/AdErrorType;->adErrorTypeFromCode(ILcom/facebook/ads/internal/protocol/AdErrorType;)Lcom/facebook/ads/internal/protocol/AdErrorType;

    move-result-object v1

    if-eqz v0, :cond_a

    move-object p1, v0

    :cond_a
    invoke-static {v1, p1}, Lcom/facebook/ads/internal/protocol/a;->a(Lcom/facebook/ads/internal/protocol/AdErrorType;Ljava/lang/String;)Lcom/facebook/ads/internal/protocol/a;

    move-result-object p1

    goto :goto_8

    :cond_b
    iget-object v5, p0, Lcom/facebook/ads/internal/u/c;->b:Landroid/content/Context;

    invoke-static {v5}, Lcom/facebook/ads/internal/r/a;->z(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_c

    iget-object v5, p0, Lcom/facebook/ads/internal/u/c;->b:Landroid/content/Context;

    invoke-direct {p0}, Lcom/facebook/ads/internal/u/c;->c()Lcom/facebook/ads/internal/v/a/b;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/facebook/ads/internal/p/a;->a(Landroid/content/Context;Lcom/facebook/ads/internal/v/a/b;)V

    :cond_c
    move-object v5, v2

    check-cast v5, Lcom/facebook/ads/internal/u/f;

    if-eqz v3, :cond_18

    invoke-virtual {v3}, Lcom/facebook/ads/internal/m/c;->a()Lcom/facebook/ads/internal/m/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ads/internal/m/d;->e()Z

    move-result v3

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/facebook/ads/internal/u/c;->g:Lcom/facebook/ads/internal/u/b;

    invoke-static {p1, v3}, Lcom/facebook/ads/internal/u/a;->a(Ljava/lang/String;Lcom/facebook/ads/internal/u/b;)V

    :cond_d
    iget-object p1, p0, Lcom/facebook/ads/internal/u/c;->e:Ljava/util/Map;

    if-eqz p1, :cond_e

    iget-object p1, p0, Lcom/facebook/ads/internal/u/c;->e:Ljava/util/Map;

    const-string v3, "CLIENT_REQUEST_ID"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v4, p1

    check-cast v4, Ljava/lang/String;

    :cond_e
    invoke-virtual {v2}, Lcom/facebook/ads/internal/u/e;->c()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_17

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_17

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v7, 0x0

    :goto_9
    const/16 v8, 0x20

    if-ge v7, v8, :cond_15

    const-string v8, "73q8p304q6q511r89s8os2801s1o9sq1"

    invoke-virtual {v8, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x61

    if-lt v8, v9, :cond_f

    const/16 v9, 0x6d

    if-le v8, v9, :cond_10

    :cond_f
    const/16 v9, 0x41

    if-lt v8, v9, :cond_11

    const/16 v9, 0x4d

    if-gt v8, v9, :cond_11

    :cond_10
    add-int/lit8 v8, v8, 0xd

    :goto_a
    int-to-char v8, v8

    goto :goto_b

    :cond_11
    const/16 v9, 0x6e

    if-lt v8, v9, :cond_12

    const/16 v9, 0x7a

    if-le v8, v9, :cond_13

    :cond_12
    const/16 v9, 0x4e

    if-lt v8, v9, :cond_14

    const/16 v9, 0x5a

    if-gt v8, v9, :cond_14

    :cond_13
    add-int/lit8 v8, v8, -0xd

    goto :goto_a

    :cond_14
    :goto_b
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v7, v7, 0x1

    goto :goto_9

    :cond_15
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v8

    array-length v9, v7

    invoke-virtual {v8, v7, v6, v9}, Ljava/security/MessageDigest;->update([BII)V

    invoke-virtual {v8}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/ads/internal/w/b/i;->a([B)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Lcom/facebook/ads/internal/u/e;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_16

    iget-object v7, p0, Lcom/facebook/ads/internal/u/c;->b:Landroid/content/Context;

    const-string v8, "network"

    sget v9, Lcom/facebook/ads/internal/w/h/b;->t:I

    new-instance v10, Lcom/facebook/ads/internal/protocol/h;

    invoke-direct {v10}, Lcom/facebook/ads/internal/protocol/h;-><init>()V

    invoke-static {v7, v8, v9, v10}, Lcom/facebook/ads/internal/w/h/a;->b(Landroid/content/Context;Ljava/lang/String;ILjava/lang/Exception;)V

    :cond_16
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    array-length v3, v1

    invoke-virtual {v0, v1, v6, v3}, Ljava/security/MessageDigest;->update([BII)V

    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/ads/internal/w/b/i;->a([B)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/facebook/ads/internal/k/a;

    invoke-direct {v1, p1, v0}, Lcom/facebook/ads/internal/k/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/facebook/ads/internal/u/c;->b:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/facebook/ads/internal/k/e;->a(Lcom/facebook/ads/internal/k/d;Landroid/content/Context;)V

    :cond_17
    invoke-virtual {v2}, Lcom/facebook/ads/internal/u/e;->e()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_18

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_18

    new-instance p1, Lcom/facebook/ads/internal/q/a;

    iget-object v0, p0, Lcom/facebook/ads/internal/u/c;->b:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/facebook/ads/internal/u/e;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v4, v1}, Lcom/facebook/ads/internal/q/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/ads/internal/q/a;->a()V

    :cond_18
    invoke-direct {p0, v5}, Lcom/facebook/ads/internal/u/c;->a(Lcom/facebook/ads/internal/u/f;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_c

    :catch_2
    move-exception p1

    sget-object v0, Lcom/facebook/ads/internal/protocol/AdErrorType;->PARSER_FAILURE:Lcom/facebook/ads/internal/protocol/AdErrorType;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/facebook/ads/internal/protocol/a;->a(Lcom/facebook/ads/internal/protocol/AdErrorType;Ljava/lang/String;)Lcom/facebook/ads/internal/protocol/a;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/facebook/ads/internal/u/c;->a(Lcom/facebook/ads/internal/protocol/a;)V

    :goto_c
    return-void
.end method

.method static synthetic b()Lcom/facebook/ads/internal/u/c$a;
    .locals 1

    sget-object v0, Lcom/facebook/ads/internal/u/c;->a:Lcom/facebook/ads/internal/u/c$a;

    return-object v0
.end method

.method static synthetic b(Lcom/facebook/ads/internal/u/c;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/facebook/ads/internal/u/c;->e:Ljava/util/Map;

    return-object p0
.end method

.method private c()Lcom/facebook/ads/internal/v/a/b;
    .locals 1

    new-instance v0, Lcom/facebook/ads/internal/u/c$2;

    invoke-direct {v0, p0}, Lcom/facebook/ads/internal/u/c$2;-><init>(Lcom/facebook/ads/internal/u/c;)V

    return-object v0
.end method

.method static synthetic c(Lcom/facebook/ads/internal/u/c;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/facebook/ads/internal/u/c;->i:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic d(Lcom/facebook/ads/internal/u/c;)Lcom/facebook/ads/internal/v/a/a;
    .locals 0

    iget-object p0, p0, Lcom/facebook/ads/internal/u/c;->h:Lcom/facebook/ads/internal/v/a/a;

    return-object p0
.end method

.method static synthetic e(Lcom/facebook/ads/internal/u/c;)Lcom/facebook/ads/internal/v/a/b;
    .locals 0

    invoke-direct {p0}, Lcom/facebook/ads/internal/u/c;->c()Lcom/facebook/ads/internal/v/a/b;

    move-result-object p0

    return-object p0
.end method

.method static synthetic f(Lcom/facebook/ads/internal/u/c;)Lcom/facebook/ads/internal/u/b;
    .locals 0

    iget-object p0, p0, Lcom/facebook/ads/internal/u/c;->g:Lcom/facebook/ads/internal/u/b;

    return-object p0
.end method

.method static synthetic g(Lcom/facebook/ads/internal/u/c;)Lcom/facebook/ads/internal/u/d;
    .locals 0

    iget-object p0, p0, Lcom/facebook/ads/internal/u/c;->c:Lcom/facebook/ads/internal/u/d;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/facebook/ads/internal/u/c;->h:Lcom/facebook/ads/internal/v/a/a;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ads/internal/v/a/a;->c(I)V

    iget-object v0, p0, Lcom/facebook/ads/internal/u/c;->h:Lcom/facebook/ads/internal/v/a/a;

    invoke-virtual {v0, v1}, Lcom/facebook/ads/internal/v/a/a;->b(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ads/internal/u/c;->h:Lcom/facebook/ads/internal/v/a/a;

    :cond_0
    return-void
.end method

.method public a(Lcom/facebook/ads/internal/u/b;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/ads/internal/u/c;->a(Lcom/facebook/ads/internal/u/b;Z)V

    return-void
.end method

.method public a(Lcom/facebook/ads/internal/u/b;Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/facebook/ads/internal/u/c;->a()V

    if-nez p2, :cond_1

    sget-object v0, Lcom/facebook/ads/internal/u/c;->a:Lcom/facebook/ads/internal/u/c$a;

    if-eqz v0, :cond_1

    invoke-interface {v0, p0, p1}, Lcom/facebook/ads/internal/u/c$a;->a(Lcom/facebook/ads/internal/u/c;Lcom/facebook/ads/internal/u/b;)Lcom/facebook/ads/internal/u/c$c;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/facebook/ads/internal/u/c$c;->a:Lcom/facebook/ads/internal/u/f;

    if-eqz v1, :cond_0

    iget-object p1, v0, Lcom/facebook/ads/internal/u/c$c;->a:Lcom/facebook/ads/internal/u/f;

    invoke-direct {p0, p1}, Lcom/facebook/ads/internal/u/c;->a(Lcom/facebook/ads/internal/u/f;)V

    return-void

    :cond_0
    iget-object v1, v0, Lcom/facebook/ads/internal/u/c$c;->b:Lcom/facebook/ads/internal/protocol/a;

    if-eqz v1, :cond_1

    iget-object p1, v0, Lcom/facebook/ads/internal/u/c$c;->b:Lcom/facebook/ads/internal/protocol/a;

    invoke-direct {p0, p1}, Lcom/facebook/ads/internal/u/c;->a(Lcom/facebook/ads/internal/protocol/a;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/facebook/ads/internal/u/c;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/ads/internal/w/b/u;->a(Landroid/content/Context;)Lcom/facebook/ads/internal/w/b/u$a;

    move-result-object v0

    sget-object v1, Lcom/facebook/ads/internal/w/b/u$a;->b:Lcom/facebook/ads/internal/w/b/u$a;

    if-ne v0, v1, :cond_2

    new-instance p1, Lcom/facebook/ads/internal/protocol/a;

    sget-object p2, Lcom/facebook/ads/internal/protocol/AdErrorType;->NETWORK_ERROR:Lcom/facebook/ads/internal/protocol/AdErrorType;

    const-string v0, "No network connection"

    invoke-direct {p1, p2, v0}, Lcom/facebook/ads/internal/protocol/a;-><init>(Lcom/facebook/ads/internal/protocol/AdErrorType;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/facebook/ads/internal/u/c;->a(Lcom/facebook/ads/internal/protocol/a;)V

    return-void

    :cond_2
    iput-object p1, p0, Lcom/facebook/ads/internal/u/c;->g:Lcom/facebook/ads/internal/u/b;

    iget-object v0, p0, Lcom/facebook/ads/internal/u/c;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/ads/internal/l/a;->a(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/facebook/ads/internal/u/a;->a(Lcom/facebook/ads/internal/u/b;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1}, Lcom/facebook/ads/internal/u/a;->c(Lcom/facebook/ads/internal/u/b;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-direct {p0, p1}, Lcom/facebook/ads/internal/u/c;->a(Ljava/lang/String;)V

    return-void

    :cond_3
    sget-object p1, Lcom/facebook/ads/internal/protocol/AdErrorType;->LOAD_TOO_FREQUENTLY:Lcom/facebook/ads/internal/protocol/AdErrorType;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/facebook/ads/internal/protocol/a;->a(Lcom/facebook/ads/internal/protocol/AdErrorType;Ljava/lang/String;)Lcom/facebook/ads/internal/protocol/a;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/facebook/ads/internal/u/c;->a(Lcom/facebook/ads/internal/protocol/a;)V

    return-void

    :cond_4
    sget-object v0, Lcom/facebook/ads/internal/u/c;->k:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v1, Lcom/facebook/ads/internal/u/c$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/ads/internal/u/c$1;-><init>(Lcom/facebook/ads/internal/u/c;Lcom/facebook/ads/internal/u/b;Z)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public a(Lcom/facebook/ads/internal/u/c$b;)V
    .locals 0

    iput-object p1, p0, Lcom/facebook/ads/internal/u/c;->f:Lcom/facebook/ads/internal/u/c$b;

    return-void
.end method
