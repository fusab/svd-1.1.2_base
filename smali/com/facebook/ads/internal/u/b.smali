.class public Lcom/facebook/ads/internal/u/b;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/facebook/ads/internal/protocol/c;

.field private final c:Lcom/facebook/ads/internal/protocol/AdPlacementType;

.field private final d:Ljava/lang/String;

.field private e:Landroid/content/Context;

.field private f:Lcom/facebook/ads/internal/protocol/e;

.field private g:Z

.field private h:Z

.field private i:I

.field private j:Lcom/facebook/ads/internal/w/b/m;

.field private final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcom/facebook/ads/internal/protocol/g;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/ads/internal/n/d;Ljava/lang/String;Lcom/facebook/ads/internal/w/b/m;Lcom/facebook/ads/internal/protocol/e;Ljava/lang/String;IZZLcom/facebook/ads/internal/protocol/g;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/facebook/ads/internal/u/b;->e:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/facebook/ads/internal/n/d;->b()Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/ads/internal/u/b;->k:Ljava/util/Map;

    iput-object p3, p0, Lcom/facebook/ads/internal/u/b;->a:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/ads/internal/u/b;->j:Lcom/facebook/ads/internal/w/b/m;

    iput-object p5, p0, Lcom/facebook/ads/internal/u/b;->f:Lcom/facebook/ads/internal/protocol/e;

    iput-object p6, p0, Lcom/facebook/ads/internal/u/b;->d:Ljava/lang/String;

    iput p7, p0, Lcom/facebook/ads/internal/u/b;->i:I

    iput-boolean p8, p0, Lcom/facebook/ads/internal/u/b;->g:Z

    iput-boolean p9, p0, Lcom/facebook/ads/internal/u/b;->h:Z

    iput-object p10, p0, Lcom/facebook/ads/internal/u/b;->l:Lcom/facebook/ads/internal/protocol/g;

    invoke-static {p5}, Lcom/facebook/ads/internal/protocol/c;->a(Lcom/facebook/ads/internal/protocol/e;)Lcom/facebook/ads/internal/protocol/c;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/ads/internal/u/b;->b:Lcom/facebook/ads/internal/protocol/c;

    iget-object p1, p0, Lcom/facebook/ads/internal/u/b;->b:Lcom/facebook/ads/internal/protocol/c;

    invoke-virtual {p1}, Lcom/facebook/ads/internal/protocol/c;->a()Lcom/facebook/ads/internal/protocol/AdPlacementType;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/ads/internal/u/b;->c:Lcom/facebook/ads/internal/protocol/AdPlacementType;

    iput-object p11, p0, Lcom/facebook/ads/internal/u/b;->m:Ljava/lang/String;

    iput-object p12, p0, Lcom/facebook/ads/internal/u/b;->n:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a()Lcom/facebook/ads/internal/protocol/e;
    .locals 1

    iget-object v0, p0, Lcom/facebook/ads/internal/u/b;->f:Lcom/facebook/ads/internal/protocol/e;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/facebook/ads/internal/u/b;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()Lcom/facebook/ads/internal/protocol/c;
    .locals 1

    iget-object v0, p0, Lcom/facebook/ads/internal/u/b;->b:Lcom/facebook/ads/internal/protocol/c;

    return-object v0
.end method

.method public d()Lcom/facebook/ads/internal/w/b/m;
    .locals 1

    iget-object v0, p0, Lcom/facebook/ads/internal/u/b;->j:Lcom/facebook/ads/internal/w/b/m;

    return-object v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lcom/facebook/ads/internal/u/b;->i:I

    return v0
.end method

.method public f()Lcom/facebook/ads/internal/protocol/g;
    .locals 1

    iget-object v0, p0, Lcom/facebook/ads/internal/u/b;->l:Lcom/facebook/ads/internal/protocol/g;

    return-object v0
.end method

.method public g()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/facebook/ads/internal/u/b;->k:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    sget-object v1, Lcom/facebook/ads/internal/g/b;->b:Ljava/lang/String;

    const-string v2, "IDFA"

    invoke-direct {p0, v0, v2, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v1, Lcom/facebook/ads/internal/g/b;->c:Z

    const-string v2, "1"

    if-eqz v1, :cond_0

    const-string v1, "0"

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    const-string v3, "IDFA_FLAG"

    invoke-direct {p0, v0, v3, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/facebook/ads/internal/u/b;->h:Z

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    const-string v3, "COPPA"

    invoke-direct {p0, v0, v3, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/ads/internal/u/b;->a:Ljava/lang/String;

    const-string v3, "PLACEMENT_ID"

    invoke-direct {p0, v0, v3, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/ads/internal/u/b;->c:Lcom/facebook/ads/internal/protocol/AdPlacementType;

    sget-object v3, Lcom/facebook/ads/internal/protocol/AdPlacementType;->UNKNOWN:Lcom/facebook/ads/internal/protocol/AdPlacementType;

    if-eq v1, v3, :cond_1

    iget-object v1, p0, Lcom/facebook/ads/internal/u/b;->c:Lcom/facebook/ads/internal/protocol/AdPlacementType;

    invoke-virtual {v1}, Lcom/facebook/ads/internal/protocol/AdPlacementType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v3, "PLACEMENT_TYPE"

    invoke-direct {p0, v0, v3, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lcom/facebook/ads/internal/u/b;->j:Lcom/facebook/ads/internal/w/b/m;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/ads/internal/w/b/m;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "WIDTH"

    invoke-direct {p0, v0, v3, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/ads/internal/u/b;->j:Lcom/facebook/ads/internal/w/b/m;

    invoke-virtual {v1}, Lcom/facebook/ads/internal/w/b/m;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "HEIGHT"

    invoke-direct {p0, v0, v3, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/facebook/ads/internal/u/b;->f:Lcom/facebook/ads/internal/protocol/e;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/facebook/ads/internal/protocol/e;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "TEMPLATE_ID"

    invoke-direct {p0, v0, v3, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-boolean v1, p0, Lcom/facebook/ads/internal/u/b;->g:Z

    if-eqz v1, :cond_4

    const-string v1, "TEST_MODE"

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v1, p0, Lcom/facebook/ads/internal/u/b;->d:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v2, "DEMO_AD_ID"

    invoke-direct {p0, v0, v2, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget v1, p0, Lcom/facebook/ads/internal/u/b;->i:I

    if-eqz v1, :cond_6

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "NUM_ADS_REQUESTED"

    invoke-direct {p0, v0, v2, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    invoke-static {}, Lcom/facebook/ads/internal/o/b;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CLIENT_EVENTS"

    invoke-direct {p0, v0, v2, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/ads/internal/u/b;->e:Landroid/content/Context;

    invoke-static {v1}, Lcom/facebook/ads/internal/w/b/z;->a(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    const-string v2, "KG_RESTRICTED"

    invoke-direct {p0, v0, v2, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/facebook/ads/internal/w/b/v;->b(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "REQUEST_TIME"

    invoke-direct {p0, v0, v2, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/ads/internal/u/b;->l:Lcom/facebook/ads/internal/protocol/g;

    invoke-virtual {v1}, Lcom/facebook/ads/internal/protocol/g;->c()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/facebook/ads/internal/u/b;->l:Lcom/facebook/ads/internal/protocol/g;

    invoke-virtual {v1}, Lcom/facebook/ads/internal/protocol/g;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BID_ID"

    invoke-direct {p0, v0, v2, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    iget-object v1, p0, Lcom/facebook/ads/internal/u/b;->m:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v2, "STACK_TRACE"

    invoke-direct {p0, v0, v2, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CLIENT_REQUEST_ID"

    invoke-direct {p0, v0, v2, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/ads/internal/u/b;->e:Landroid/content/Context;

    invoke-static {v1}, Lcom/facebook/ads/internal/f/a;->a(Landroid/content/Context;)J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/facebook/ads/internal/w/b/v;->a(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "AD_REPORTING_CONFIG_LAST_UPDATE_TIME"

    invoke-direct {p0, v0, v2, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/ads/internal/u/b;->n:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v2, "EXTRA_HINTS"

    invoke-direct {p0, v0, v2, v1}, Lcom/facebook/ads/internal/u/b;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    return-object v0
.end method
