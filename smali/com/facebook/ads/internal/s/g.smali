.class public final enum Lcom/facebook/ads/internal/s/g;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/facebook/ads/internal/s/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/facebook/ads/internal/s/g;

.field public static final enum b:Lcom/facebook/ads/internal/s/g;

.field public static final enum c:Lcom/facebook/ads/internal/s/g;

.field public static final enum d:Lcom/facebook/ads/internal/s/g;

.field public static final enum e:Lcom/facebook/ads/internal/s/g;

.field public static final enum f:Lcom/facebook/ads/internal/s/g;

.field public static final enum g:Lcom/facebook/ads/internal/s/g;

.field public static final enum h:Lcom/facebook/ads/internal/s/g;

.field public static final enum i:Lcom/facebook/ads/internal/s/g;

.field public static final enum j:Lcom/facebook/ads/internal/s/g;

.field public static final enum k:Lcom/facebook/ads/internal/s/g;

.field public static final enum l:Lcom/facebook/ads/internal/s/g;

.field public static final enum m:Lcom/facebook/ads/internal/s/g;

.field public static final enum n:Lcom/facebook/ads/internal/s/g;

.field public static final enum o:Lcom/facebook/ads/internal/s/g;

.field public static final enum p:Lcom/facebook/ads/internal/s/g;

.field public static final enum q:Lcom/facebook/ads/internal/s/g;

.field public static final enum r:Lcom/facebook/ads/internal/s/g;

.field public static final enum s:Lcom/facebook/ads/internal/s/g;

.field public static final enum t:Lcom/facebook/ads/internal/s/g;

.field public static final enum u:Lcom/facebook/ads/internal/s/g;

.field private static final synthetic w:[Lcom/facebook/ads/internal/s/g;


# instance fields
.field private v:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const/4 v1, 0x0

    const-string v2, "TEST"

    const-string v3, "test"

    invoke-direct {v0, v2, v1, v3}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->a:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const/4 v2, 0x1

    const-string v3, "BROWSER_SESSION"

    const-string v4, "browser_session"

    invoke-direct {v0, v3, v2, v4}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->b:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const/4 v3, 0x2

    const-string v4, "CLOSE"

    const-string v5, "close"

    invoke-direct {v0, v4, v3, v5}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->c:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const/4 v4, 0x3

    const-string v5, "SHOW_AD_CALLED"

    const-string v6, "show_ad_called"

    invoke-direct {v0, v5, v4, v6}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->d:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const/4 v5, 0x4

    const-string v6, "IMPRESSION"

    const-string v7, "impression"

    invoke-direct {v0, v6, v5, v7}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->e:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const/4 v6, 0x5

    const-string v7, "INVALIDATION"

    const-string v8, "invalidation"

    invoke-direct {v0, v7, v6, v8}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->f:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const/4 v7, 0x6

    const-string v8, "STORE"

    const-string v9, "store"

    invoke-direct {v0, v8, v7, v9}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->g:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const/4 v8, 0x7

    const-string v9, "OFF_TARGET_CLICK"

    const-string v10, "off_target_click"

    invoke-direct {v0, v9, v8, v10}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->h:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const/16 v9, 0x8

    const-string v10, "OPEN_LINK"

    const-string v11, "open_link"

    invoke-direct {v0, v10, v9, v11}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->i:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const/16 v10, 0x9

    const-string v11, "NATIVE_VIEW"

    const-string v12, "native_view"

    invoke-direct {v0, v11, v10, v12}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->j:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const/16 v11, 0xa

    const-string v12, "VIDEO"

    const-string/jumbo v13, "video"

    invoke-direct {v0, v12, v11, v13}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->k:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const/16 v12, 0xb

    const-string v13, "USER_RETURN"

    const-string/jumbo v14, "user_return"

    invoke-direct {v0, v13, v12, v14}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->l:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const/16 v13, 0xc

    const-string v14, "AD_REPORTING"

    const-string/jumbo v15, "x_out"

    invoke-direct {v0, v14, v13, v15}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->m:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const/16 v14, 0xd

    const-string v15, "PREVIEW_IMPRESSION"

    const-string v13, "preview_impression"

    invoke-direct {v0, v15, v14, v13}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->n:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const/16 v13, 0xe

    const-string v15, "AD_SELECTION"

    const-string v14, "ad_selection"

    invoke-direct {v0, v15, v13, v14}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->o:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const-string v14, "CLICK_GUARD"

    const/16 v15, 0xf

    const-string v13, "click_guard"

    invoke-direct {v0, v14, v15, v13}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->p:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const-string v13, "TWO_STEP"

    const/16 v14, 0x10

    const-string v15, "two_step_dialog"

    invoke-direct {v0, v13, v14, v15}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->q:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const-string v13, "TWO_STEP_CANCEL"

    const/16 v14, 0x11

    const-string v15, "two_step_cancel"

    invoke-direct {v0, v13, v14, v15}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->r:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const-string v13, "SWIPE_TO_CLICK"

    const/16 v14, 0x12

    const-string v15, "swipe_click"

    invoke-direct {v0, v13, v14, v15}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->s:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const-string v13, "CLOSE_BROWSE_VIEW"

    const/16 v14, 0x13

    const-string v15, "browse_view_closed"

    invoke-direct {v0, v13, v14, v15}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->t:Lcom/facebook/ads/internal/s/g;

    new-instance v0, Lcom/facebook/ads/internal/s/g;

    const-string v13, "WATCH_AND_X_MINIMIZED"

    const/16 v14, 0x14

    const-string/jumbo v15, "watch_and_x_minimized"

    invoke-direct {v0, v13, v14, v15}, Lcom/facebook/ads/internal/s/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/ads/internal/s/g;->u:Lcom/facebook/ads/internal/s/g;

    const/16 v0, 0x15

    new-array v0, v0, [Lcom/facebook/ads/internal/s/g;

    sget-object v13, Lcom/facebook/ads/internal/s/g;->a:Lcom/facebook/ads/internal/s/g;

    aput-object v13, v0, v1

    sget-object v1, Lcom/facebook/ads/internal/s/g;->b:Lcom/facebook/ads/internal/s/g;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/ads/internal/s/g;->c:Lcom/facebook/ads/internal/s/g;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/ads/internal/s/g;->d:Lcom/facebook/ads/internal/s/g;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/ads/internal/s/g;->e:Lcom/facebook/ads/internal/s/g;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/ads/internal/s/g;->f:Lcom/facebook/ads/internal/s/g;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/ads/internal/s/g;->g:Lcom/facebook/ads/internal/s/g;

    aput-object v1, v0, v7

    sget-object v1, Lcom/facebook/ads/internal/s/g;->h:Lcom/facebook/ads/internal/s/g;

    aput-object v1, v0, v8

    sget-object v1, Lcom/facebook/ads/internal/s/g;->i:Lcom/facebook/ads/internal/s/g;

    aput-object v1, v0, v9

    sget-object v1, Lcom/facebook/ads/internal/s/g;->j:Lcom/facebook/ads/internal/s/g;

    aput-object v1, v0, v10

    sget-object v1, Lcom/facebook/ads/internal/s/g;->k:Lcom/facebook/ads/internal/s/g;

    aput-object v1, v0, v11

    sget-object v1, Lcom/facebook/ads/internal/s/g;->l:Lcom/facebook/ads/internal/s/g;

    aput-object v1, v0, v12

    sget-object v1, Lcom/facebook/ads/internal/s/g;->m:Lcom/facebook/ads/internal/s/g;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/ads/internal/s/g;->n:Lcom/facebook/ads/internal/s/g;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/ads/internal/s/g;->o:Lcom/facebook/ads/internal/s/g;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/ads/internal/s/g;->p:Lcom/facebook/ads/internal/s/g;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/ads/internal/s/g;->q:Lcom/facebook/ads/internal/s/g;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/ads/internal/s/g;->r:Lcom/facebook/ads/internal/s/g;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/ads/internal/s/g;->s:Lcom/facebook/ads/internal/s/g;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/ads/internal/s/g;->t:Lcom/facebook/ads/internal/s/g;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/ads/internal/s/g;->u:Lcom/facebook/ads/internal/s/g;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sput-object v0, Lcom/facebook/ads/internal/s/g;->w:[Lcom/facebook/ads/internal/s/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/facebook/ads/internal/s/g;->v:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/ads/internal/s/g;
    .locals 5

    invoke-static {}, Lcom/facebook/ads/internal/s/g;->values()[Lcom/facebook/ads/internal/s/g;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    iget-object v4, v3, Lcom/facebook/ads/internal/s/g;->v:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/ads/internal/s/g;
    .locals 1

    const-class v0, Lcom/facebook/ads/internal/s/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/facebook/ads/internal/s/g;

    return-object p0
.end method

.method public static values()[Lcom/facebook/ads/internal/s/g;
    .locals 1

    sget-object v0, Lcom/facebook/ads/internal/s/g;->w:[Lcom/facebook/ads/internal/s/g;

    invoke-virtual {v0}, [Lcom/facebook/ads/internal/s/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/ads/internal/s/g;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/facebook/ads/internal/s/g;->v:Ljava/lang/String;

    return-object v0
.end method
