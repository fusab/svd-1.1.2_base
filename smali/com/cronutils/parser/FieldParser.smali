.class public Lcom/cronutils/parser/FieldParser;
.super Ljava/lang/Object;
.source "FieldParser.java"


# instance fields
.field private fieldConstraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

.field private lPattern:Ljava/util/regex/Pattern;

.field private final specialCharsMinusStar:[C

.field private wPattern:Ljava/util/regex/Pattern;


# direct methods
.method public constructor <init>(Lcom/cronutils/model/field/constraint/FieldConstraints;)V
    .locals 2

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    .line 32
    new-array v0, v0, [C

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/cronutils/parser/FieldParser;->specialCharsMinusStar:[C

    const/4 v0, 0x2

    const-string v1, "[0-9]L"

    .line 33
    invoke-static {v1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v1

    iput-object v1, p0, Lcom/cronutils/parser/FieldParser;->lPattern:Ljava/util/regex/Pattern;

    const-string v1, "[0-9]W"

    .line 34
    invoke-static {v1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/cronutils/parser/FieldParser;->wPattern:Ljava/util/regex/Pattern;

    const/4 v0, 0x0

    .line 38
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "FieldConstraints must not be null"

    invoke-static {p1, v1, v0}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/constraint/FieldConstraints;

    iput-object p1, p0, Lcom/cronutils/parser/FieldParser;->fieldConstraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

    return-void

    :array_0
    .array-data 2
        0x2fs
        0x2ds
        0x2cs
    .end array-data
.end method


# virtual methods
.method intToInt(Ljava/lang/Integer;)I
    .locals 1

    .line 224
    iget-object v0, p0, Lcom/cronutils/parser/FieldParser;->fieldConstraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getIntMapping()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/cronutils/parser/FieldParser;->fieldConstraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getIntMapping()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    .line 227
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1
.end method

.method map(Ljava/lang/String;)Lcom/cronutils/model/field/value/FieldValue;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 189
    invoke-static {}, Lcom/cronutils/model/field/value/SpecialChar;->values()[Lcom/cronutils/model/field/value/SpecialChar;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 190
    invoke-virtual {v3}, Lcom/cronutils/model/field/value/SpecialChar;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 191
    new-instance p1, Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    invoke-direct {p1, v3}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;-><init>(Lcom/cronutils/model/field/value/SpecialChar;)V

    return-object p1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 194
    :cond_1
    new-instance v0, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-virtual {p0, p1}, Lcom/cronutils/parser/FieldParser;->stringToInt(Ljava/lang/String;)I

    move-result p1

    invoke-direct {v0, p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    return-object v0
.end method

.method mapToIntegerFieldValue(Ljava/lang/String;)Lcom/cronutils/model/field/value/IntegerFieldValue;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 181
    :try_start_0
    new-instance v0, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-virtual {p0, p1}, Lcom/cronutils/parser/FieldParser;->stringToInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/cronutils/parser/FieldParser;->intToInt(Ljava/lang/Integer;)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 183
    :catch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string p1, "Invalid value. Expected some integer, found %s"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public parse(Ljava/lang/String;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 6

    .line 47
    iget-object v0, p0, Lcom/cronutils/parser/FieldParser;->specialCharsMinusStar:[C

    invoke-static {p1, v0}, Lorg/apache/commons/lang3/StringUtils;->containsAny(Ljava/lang/CharSequence;[C)Z

    move-result v0

    const-string v1, "*"

    if-nez v0, :cond_2

    .line 48
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    new-instance p1, Lcom/cronutils/model/field/expression/Always;

    invoke-direct {p1}, Lcom/cronutils/model/field/expression/Always;-><init>()V

    return-object p1

    :cond_0
    const-string v0, "?"

    .line 51
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    new-instance p1, Lcom/cronutils/model/field/expression/QuestionMark;

    invoke-direct {p1}, Lcom/cronutils/model/field/expression/QuestionMark;-><init>()V

    return-object p1

    .line 54
    :cond_1
    invoke-virtual {p0, p1}, Lcom/cronutils/parser/FieldParser;->parseOn(Ljava/lang/String;)Lcom/cronutils/model/field/expression/On;

    move-result-object p1

    return-object p1

    :cond_2
    const-string v0, ","

    .line 57
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 58
    array-length v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-le v2, v4, :cond_4

    .line 59
    new-instance p1, Lcom/cronutils/model/field/expression/And;

    invoke-direct {p1}, Lcom/cronutils/model/field/expression/And;-><init>()V

    .line 60
    array-length v1, v0

    :goto_0
    if-ge v3, v1, :cond_3

    aget-object v2, v0, v3

    .line 61
    invoke-virtual {p0, v2}, Lcom/cronutils/parser/FieldParser;->parse(Ljava/lang/String;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/cronutils/model/field/expression/And;->and(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/And;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    return-object p1

    :cond_4
    const-string v0, "-"

    .line 65
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 66
    array-length v2, v0

    if-le v2, v4, :cond_5

    .line 67
    invoke-virtual {p0, v0}, Lcom/cronutils/parser/FieldParser;->parseBetween([Ljava/lang/String;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    return-object p1

    :cond_5
    const-string v0, "/"

    .line 69
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 70
    array-length v2, v0

    const/4 v5, 0x2

    if-ne v2, v5, :cond_8

    .line 71
    aget-object p1, v0, v3

    .line 72
    aget-object v0, v0, v4

    .line 73
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    goto :goto_1

    .line 76
    :cond_6
    new-instance v1, Lcom/cronutils/model/field/expression/Every;

    new-instance v2, Lcom/cronutils/model/field/expression/On;

    new-instance v3, Lcom/cronutils/model/field/value/IntegerFieldValue;

    .line 77
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    invoke-direct {v3, p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {v2, v3}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    new-instance p1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    .line 78
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p1, v0}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {v1, v2, p1}, Lcom/cronutils/model/field/expression/Every;-><init>(Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-object v1

    .line 74
    :cond_7
    :goto_1
    new-instance p1, Lcom/cronutils/model/field/expression/Every;

    new-instance v1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {v1, v0}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {p1, v1}, Lcom/cronutils/model/field/expression/Every;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-object p1

    .line 81
    :cond_8
    array-length v0, v0

    if-ne v0, v4, :cond_9

    .line 82
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Missing steps for expression: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid expression: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method parseBetween([Ljava/lang/String;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    const/4 v0, 0x1

    .line 93
    aget-object v1, p1, v0

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 94
    aget-object v1, p1, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 95
    new-instance v2, Lcom/cronutils/model/field/expression/Every;

    new-instance v4, Lcom/cronutils/model/field/expression/Between;

    aget-object p1, p1, v3

    invoke-virtual {p0, p1}, Lcom/cronutils/parser/FieldParser;->map(Ljava/lang/String;)Lcom/cronutils/model/field/value/FieldValue;

    move-result-object p1

    aget-object v3, v1, v3

    invoke-virtual {p0, v3}, Lcom/cronutils/parser/FieldParser;->map(Ljava/lang/String;)Lcom/cronutils/model/field/value/FieldValue;

    move-result-object v3

    invoke-direct {v4, p1, v3}, Lcom/cronutils/model/field/expression/Between;-><init>(Lcom/cronutils/model/field/value/FieldValue;Lcom/cronutils/model/field/value/FieldValue;)V

    aget-object p1, v1, v0

    invoke-virtual {p0, p1}, Lcom/cronutils/parser/FieldParser;->mapToIntegerFieldValue(Ljava/lang/String;)Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    invoke-direct {v2, v4, p1}, Lcom/cronutils/model/field/expression/Every;-><init>(Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-object v2

    .line 97
    :cond_0
    new-instance v1, Lcom/cronutils/model/field/expression/Between;

    aget-object v2, p1, v3

    invoke-virtual {p0, v2}, Lcom/cronutils/parser/FieldParser;->map(Ljava/lang/String;)Lcom/cronutils/model/field/value/FieldValue;

    move-result-object v2

    aget-object p1, p1, v0

    invoke-virtual {p0, p1}, Lcom/cronutils/parser/FieldParser;->map(Ljava/lang/String;)Lcom/cronutils/model/field/value/FieldValue;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/cronutils/model/field/expression/Between;-><init>(Lcom/cronutils/model/field/value/FieldValue;Lcom/cronutils/model/field/value/FieldValue;)V

    return-object v1
.end method

.method parseOn(Ljava/lang/String;)Lcom/cronutils/model/field/expression/On;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    const-string v0, "?"

    .line 103
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    invoke-virtual {p0, p1}, Lcom/cronutils/parser/FieldParser;->parseOnWithQuestionMark(Ljava/lang/String;)Lcom/cronutils/model/field/expression/On;

    move-result-object p1

    return-object p1

    :cond_0
    const-string v0, "#"

    .line 106
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107
    invoke-virtual {p0, p1}, Lcom/cronutils/parser/FieldParser;->parseOnWithHash(Ljava/lang/String;)Lcom/cronutils/model/field/expression/On;

    move-result-object p1

    return-object p1

    :cond_1
    const-string v0, "LW"

    .line 109
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 110
    invoke-virtual {p0, p1}, Lcom/cronutils/parser/FieldParser;->parseOnWithLW(Ljava/lang/String;)Lcom/cronutils/model/field/expression/On;

    move-result-object p1

    return-object p1

    .line 112
    :cond_2
    iget-object v0, p0, Lcom/cronutils/parser/FieldParser;->lPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "L"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_0

    .line 115
    :cond_3
    iget-object v0, p0, Lcom/cronutils/parser/FieldParser;->wPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 116
    invoke-virtual {p0, p1}, Lcom/cronutils/parser/FieldParser;->parseOnWithW(Ljava/lang/String;)Lcom/cronutils/model/field/expression/On;

    move-result-object p1

    return-object p1

    .line 118
    :cond_4
    new-instance v0, Lcom/cronutils/model/field/expression/On;

    .line 119
    invoke-virtual {p0, p1}, Lcom/cronutils/parser/FieldParser;->mapToIntegerFieldValue(Ljava/lang/String;)Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    new-instance v1, Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    sget-object v2, Lcom/cronutils/model/field/value/SpecialChar;->NONE:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-direct {v1, v2}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;-><init>(Lcom/cronutils/model/field/value/SpecialChar;)V

    new-instance v2, Lcom/cronutils/model/field/value/IntegerFieldValue;

    const/4 v3, -0x1

    invoke-direct {v2, v3}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {v0, p1, v1, v2}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;Lcom/cronutils/model/field/value/SpecialCharFieldValue;Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-object v0

    .line 113
    :cond_5
    :goto_0
    invoke-virtual {p0, p1}, Lcom/cronutils/parser/FieldParser;->parseOnWithL(Ljava/lang/String;)Lcom/cronutils/model/field/expression/On;

    move-result-object p1

    return-object p1
.end method

.method parseOnWithHash(Ljava/lang/String;)Lcom/cronutils/model/field/expression/On;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 127
    new-instance v0, Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->HASH:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-direct {v0, v1}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;-><init>(Lcom/cronutils/model/field/value/SpecialChar;)V

    const-string v1, "#"

    .line 128
    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    .line 129
    aget-object v1, p1, v1

    invoke-virtual {p0, v1}, Lcom/cronutils/parser/FieldParser;->mapToIntegerFieldValue(Ljava/lang/String;)Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v1

    const/4 v2, 0x0

    .line 130
    aget-object v3, p1, v2

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 133
    new-instance v3, Lcom/cronutils/model/field/expression/On;

    aget-object p1, p1, v2

    invoke-virtual {p0, p1}, Lcom/cronutils/parser/FieldParser;->mapToIntegerFieldValue(Ljava/lang/String;)Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    invoke-direct {v3, p1, v0, v1}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;Lcom/cronutils/model/field/value/SpecialCharFieldValue;Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-object v3

    .line 131
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Time should be specified!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method parseOnWithL(Ljava/lang/String;)Lcom/cronutils/model/field/expression/On;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 160
    new-instance v0, Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->L:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-direct {v0, v1}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;-><init>(Lcom/cronutils/model/field/value/SpecialChar;)V

    const-string v1, ""

    const-string v2, "L"

    .line 161
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 162
    new-instance v2, Lcom/cronutils/model/field/value/IntegerFieldValue;

    const/4 v3, -0x1

    invoke-direct {v2, v3}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    .line 163
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 164
    invoke-virtual {p0, p1}, Lcom/cronutils/parser/FieldParser;->mapToIntegerFieldValue(Ljava/lang/String;)Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v2

    .line 166
    :cond_0
    new-instance p1, Lcom/cronutils/model/field/expression/On;

    new-instance v1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-direct {v1, v3}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {p1, v2, v0, v1}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;Lcom/cronutils/model/field/value/SpecialCharFieldValue;Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-object p1
.end method

.method parseOnWithLW(Ljava/lang/String;)Lcom/cronutils/model/field/expression/On;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 149
    new-instance v0, Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->LW:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-direct {v0, v1}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;-><init>(Lcom/cronutils/model/field/value/SpecialChar;)V

    const-string v1, ""

    const-string v2, "LW"

    .line 150
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 151
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    new-instance p1, Lcom/cronutils/model/field/expression/On;

    new-instance v1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    new-instance v3, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-direct {v3, v2}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {p1, v1, v0, v3}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;Lcom/cronutils/model/field/value/SpecialCharFieldValue;Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-object p1

    .line 154
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string p1, "Expected: LW, found: %s"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method parseOnWithQuestionMark(Ljava/lang/String;)Lcom/cronutils/model/field/expression/On;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 138
    new-instance v0, Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->QUESTION_MARK:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-direct {v0, v1}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;-><init>(Lcom/cronutils/model/field/value/SpecialChar;)V

    const-string v1, ""

    const-string v2, "?"

    .line 139
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 140
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    new-instance p1, Lcom/cronutils/model/field/expression/On;

    new-instance v1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    new-instance v3, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-direct {v3, v2}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {p1, v1, v0, v3}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;Lcom/cronutils/model/field/value/SpecialCharFieldValue;Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-object p1

    .line 143
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string p1, "Expected: \'?\', found: %s"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method parseOnWithW(Ljava/lang/String;)Lcom/cronutils/model/field/expression/On;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 171
    new-instance v0, Lcom/cronutils/model/field/expression/On;

    const-string v1, "W"

    const-string v2, ""

    .line 172
    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/cronutils/parser/FieldParser;->mapToIntegerFieldValue(Ljava/lang/String;)Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    new-instance v1, Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    sget-object v2, Lcom/cronutils/model/field/value/SpecialChar;->W:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-direct {v1, v2}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;-><init>(Lcom/cronutils/model/field/value/SpecialChar;)V

    new-instance v2, Lcom/cronutils/model/field/value/IntegerFieldValue;

    const/4 v3, -0x1

    invoke-direct {v2, v3}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {v0, p1, v1, v2}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;Lcom/cronutils/model/field/value/SpecialCharFieldValue;Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-object v0
.end method

.method stringToInt(Ljava/lang/String;)I
    .locals 4

    .line 204
    iget-object v0, p0, Lcom/cronutils/parser/FieldParser;->fieldConstraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getStringMapping()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/cronutils/parser/FieldParser;->fieldConstraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getStringMapping()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    .line 208
    :cond_0
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 210
    :catch_0
    new-instance v0, Lcom/cronutils/StringValidations;

    iget-object v1, p0, Lcom/cronutils/parser/FieldParser;->fieldConstraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

    invoke-direct {v0, v1}, Lcom/cronutils/StringValidations;-><init>(Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    invoke-virtual {v0, p1}, Lcom/cronutils/StringValidations;->removeValidChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 211
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    aput-object v0, v2, p1

    const-string p1, "Invalid chars in expression! Expression: %s Invalid chars: %s"

    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
