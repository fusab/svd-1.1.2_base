.class public Lcom/cronutils/parser/CronParserField;
.super Ljava/lang/Object;
.source "CronParserField.java"


# instance fields
.field private constraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

.field private field:Lcom/cronutils/model/field/CronFieldName;

.field private parser:Lcom/cronutils/parser/FieldParser;


# direct methods
.method public constructor <init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/constraint/FieldConstraints;)V
    .locals 3

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 35
    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "CronFieldName must not be null"

    invoke-static {p1, v2, v1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/CronFieldName;

    iput-object p1, p0, Lcom/cronutils/parser/CronParserField;->field:Lcom/cronutils/model/field/CronFieldName;

    .line 36
    new-array p1, v0, [Ljava/lang/Object;

    const-string v0, "FieldConstraints must not be null"

    invoke-static {p2, v0, p1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/constraint/FieldConstraints;

    iput-object p1, p0, Lcom/cronutils/parser/CronParserField;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

    .line 37
    new-instance p1, Lcom/cronutils/parser/FieldParser;

    invoke-direct {p1, p2}, Lcom/cronutils/parser/FieldParser;-><init>(Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    iput-object p1, p0, Lcom/cronutils/parser/CronParserField;->parser:Lcom/cronutils/parser/FieldParser;

    return-void
.end method

.method public static createFieldTypeComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator<",
            "Lcom/cronutils/parser/CronParserField;",
            ">;"
        }
    .end annotation

    .line 63
    new-instance v0, Lcom/cronutils/parser/CronParserField$1;

    invoke-direct {v0}, Lcom/cronutils/parser/CronParserField$1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getField()Lcom/cronutils/model/field/CronFieldName;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/cronutils/parser/CronParserField;->field:Lcom/cronutils/model/field/CronFieldName;

    return-object v0
.end method

.method public parse(Ljava/lang/String;)Lcom/cronutils/model/field/CronField;
    .locals 3

    .line 55
    new-instance v0, Lcom/cronutils/model/field/CronField;

    iget-object v1, p0, Lcom/cronutils/parser/CronParserField;->field:Lcom/cronutils/model/field/CronFieldName;

    iget-object v2, p0, Lcom/cronutils/parser/CronParserField;->parser:Lcom/cronutils/parser/FieldParser;

    invoke-virtual {v2, p1}, Lcom/cronutils/parser/FieldParser;->parse(Ljava/lang/String;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    iget-object v2, p0, Lcom/cronutils/parser/CronParserField;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

    invoke-direct {v0, v1, p1, v2}, Lcom/cronutils/model/field/CronField;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    return-object v0
.end method
