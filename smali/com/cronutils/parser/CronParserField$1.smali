.class final Lcom/cronutils/parser/CronParserField$1;
.super Ljava/lang/Object;
.source "CronParserField.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/parser/CronParserField;->createFieldTypeComparator()Ljava/util/Comparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/cronutils/parser/CronParserField;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/cronutils/parser/CronParserField;Lcom/cronutils/parser/CronParserField;)I
    .locals 0

    .line 66
    invoke-virtual {p1}, Lcom/cronutils/parser/CronParserField;->getField()Lcom/cronutils/model/field/CronFieldName;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/CronFieldName;->getOrder()I

    move-result p1

    invoke-virtual {p2}, Lcom/cronutils/parser/CronParserField;->getField()Lcom/cronutils/model/field/CronFieldName;

    move-result-object p2

    invoke-virtual {p2}, Lcom/cronutils/model/field/CronFieldName;->getOrder()I

    move-result p2

    sub-int/2addr p1, p2

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 63
    check-cast p1, Lcom/cronutils/parser/CronParserField;

    check-cast p2, Lcom/cronutils/parser/CronParserField;

    invoke-virtual {p0, p1, p2}, Lcom/cronutils/parser/CronParserField$1;->compare(Lcom/cronutils/parser/CronParserField;Lcom/cronutils/parser/CronParserField;)I

    move-result p1

    return p1
.end method
