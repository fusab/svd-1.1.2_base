.class public Lcom/cronutils/parser/CronParser;
.super Ljava/lang/Object;
.source "CronParser.java"


# instance fields
.field private cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

.field private expressions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lcom/cronutils/parser/CronParserField;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/cronutils/model/definition/CronDefinition;)V
    .locals 2

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/cronutils/parser/CronParser;->expressions:Ljava/util/Map;

    const/4 v0, 0x0

    .line 43
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CronDefinition must not be null"

    invoke-static {p1, v1, v0}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cronutils/model/definition/CronDefinition;

    iput-object v0, p0, Lcom/cronutils/parser/CronParser;->cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

    .line 44
    invoke-direct {p0, p1}, Lcom/cronutils/parser/CronParser;->buildPossibleExpressions(Lcom/cronutils/model/definition/CronDefinition;)V

    return-void
.end method

.method private buildPossibleExpressions(Lcom/cronutils/model/definition/CronDefinition;)V
    .locals 5

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 54
    invoke-virtual {p1}, Lcom/cronutils/model/definition/CronDefinition;->getFieldDefinitions()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cronutils/model/field/definition/FieldDefinition;

    .line 55
    new-instance v3, Lcom/cronutils/parser/CronParserField;

    invoke-virtual {v2}, Lcom/cronutils/model/field/definition/FieldDefinition;->getFieldName()Lcom/cronutils/model/field/CronFieldName;

    move-result-object v4

    invoke-virtual {v2}, Lcom/cronutils/model/field/definition/FieldDefinition;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lcom/cronutils/parser/CronParserField;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 57
    :cond_0
    invoke-static {}, Lcom/cronutils/parser/CronParserField;->createFieldTypeComparator()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 58
    iget-object v1, p0, Lcom/cronutils/parser/CronParser;->expressions:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    invoke-virtual {p1}, Lcom/cronutils/model/definition/CronDefinition;->isLastFieldOptional()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 61
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 62
    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 63
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 64
    iget-object v0, p0, Lcom/cronutils/parser/CronParser;->expressions:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method


# virtual methods
.method public parse(Ljava/lang/String;)Lcom/cronutils/model/Cron;
    .locals 5

    const/4 v0, 0x0

    .line 75
    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "Expression must not be null"

    invoke-static {p1, v2, v1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, " "

    const-string v2, "\\s+"

    .line 76
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 77
    invoke-static {p1}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 80
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    .line 81
    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 82
    array-length v1, p1

    .line 83
    iget-object v2, p0, Lcom/cronutils/parser/CronParser;->expressions:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 90
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 91
    iget-object v3, p0, Lcom/cronutils/parser/CronParser;->expressions:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 93
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 94
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cronutils/parser/CronParserField;

    .line 95
    aget-object v4, p1, v0

    invoke-virtual {v3, v4}, Lcom/cronutils/parser/CronParserField;->parse(Ljava/lang/String;)Lcom/cronutils/model/field/CronField;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 97
    :cond_0
    new-instance p1, Lcom/cronutils/model/Cron;

    iget-object v0, p0, Lcom/cronutils/parser/CronParser;->cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

    invoke-direct {p1, v0, v2}, Lcom/cronutils/model/Cron;-><init>(Lcom/cronutils/model/definition/CronDefinition;Ljava/util/List;)V

    invoke-virtual {p1}, Lcom/cronutils/model/Cron;->validate()Lcom/cronutils/model/Cron;

    move-result-object p1

    return-object p1

    .line 84
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 86
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v0

    iget-object v0, p0, Lcom/cronutils/parser/CronParser;->expressions:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, v2, v1

    const-string v0, "Cron expression contains %s parts but we expect one of %s"

    .line 85
    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 78
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Empty expression!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
