.class Lcom/cronutils/descriptor/TimeDescriptionStrategy$6;
.super Ljava/lang/Object;
.source "TimeDescriptionStrategy.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/descriptor/TimeDescriptionStrategy;->registerFunctions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function<",
        "Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;


# direct methods
.method constructor <init>(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)V
    .locals 0

    .line 206
    iput-object p1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$6;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 206
    check-cast p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy$6;->apply(Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;)Ljava/lang/String;
    .locals 14

    .line 209
    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->hours:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/On;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/Between;

    if-eqz v0, :cond_1

    .line 211
    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/On;

    const/4 v1, 0x7

    const/4 v2, 0x6

    const-string v3, "and"

    const/4 v4, 0x5

    const/4 v5, 0x4

    const/4 v6, 0x3

    const-string v7, "between"

    const/4 v8, 0x2

    const/4 v9, 0x1

    const-string v10, "every"

    const/4 v11, 0x0

    const/16 v12, 0x8

    const-string v13, "%s %s %s %02d:%02d %s %02d:%02d"

    if-eqz v0, :cond_0

    .line 212
    new-array v0, v12, [Ljava/lang/Object;

    iget-object v12, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$6;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v12, v12, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    .line 213
    invoke-virtual {v12, v10}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v0, v11

    iget-object v10, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$6;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v10, v10, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v11, "minute"

    .line 214
    invoke-virtual {v10, v11}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v0, v9

    iget-object v9, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$6;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v9, v9, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    .line 215
    invoke-virtual {v9, v7}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v0, v8

    iget-object v7, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->hours:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast v7, Lcom/cronutils/model/field/expression/On;

    .line 216
    invoke-virtual {v7}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v7

    invoke-virtual {v7}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v0, v6

    iget-object v6, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast v6, Lcom/cronutils/model/field/expression/Between;

    .line 217
    invoke-virtual {v6}, Lcom/cronutils/model/field/expression/Between;->getFrom()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object v6

    invoke-virtual {v6}, Lcom/cronutils/model/field/value/FieldValue;->getValue()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v0, v5

    iget-object v5, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$6;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v5, v5, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    .line 218
    invoke-virtual {v5, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v4

    iget-object v3, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->hours:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast v3, Lcom/cronutils/model/field/expression/On;

    .line 219
    invoke-virtual {v3}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    iget-object p1, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast p1, Lcom/cronutils/model/field/expression/Between;

    .line 220
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getTo()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/value/FieldValue;->getValue()Ljava/lang/Object;

    move-result-object p1

    aput-object p1, v0, v1

    .line 212
    invoke-static {v13, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 222
    :cond_0
    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/Always;

    if-eqz v0, :cond_1

    .line 223
    new-array v0, v12, [Ljava/lang/Object;

    iget-object v12, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$6;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v12, v12, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    .line 224
    invoke-virtual {v12, v10}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v0, v11

    iget-object v10, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$6;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v10, v10, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v11, "second"

    .line 225
    invoke-virtual {v10, v11}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v0, v9

    iget-object v9, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$6;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v9, v9, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    .line 226
    invoke-virtual {v9, v7}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v0, v8

    iget-object v7, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->hours:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast v7, Lcom/cronutils/model/field/expression/On;

    .line 227
    invoke-virtual {v7}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v7

    invoke-virtual {v7}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v0, v6

    iget-object v6, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast v6, Lcom/cronutils/model/field/expression/Between;

    .line 228
    invoke-virtual {v6}, Lcom/cronutils/model/field/expression/Between;->getFrom()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object v6

    invoke-virtual {v6}, Lcom/cronutils/model/field/value/FieldValue;->getValue()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v0, v5

    iget-object v5, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$6;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v5, v5, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    .line 229
    invoke-virtual {v5, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v4

    iget-object v3, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->hours:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast v3, Lcom/cronutils/model/field/expression/On;

    .line 230
    invoke-virtual {v3}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    iget-object p1, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast p1, Lcom/cronutils/model/field/expression/Between;

    .line 231
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getTo()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/value/FieldValue;->getValue()Ljava/lang/Object;

    move-result-object p1

    aput-object p1, v0, v1

    .line 223
    invoke-static {v13, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    const-string p1, ""

    return-object p1
.end method
