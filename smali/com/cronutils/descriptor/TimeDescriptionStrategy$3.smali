.class Lcom/cronutils/descriptor/TimeDescriptionStrategy$3;
.super Ljava/lang/Object;
.source "TimeDescriptionStrategy.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/descriptor/TimeDescriptionStrategy;->registerFunctions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function<",
        "Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;


# direct methods
.method constructor <init>(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)V
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$3;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 152
    check-cast p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy$3;->apply(Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;)Ljava/lang/String;
    .locals 3

    .line 155
    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->hours:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/On;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/On;

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of p1, p1, Lcom/cronutils/model/field/expression/Always;

    if-eqz p1, :cond_0

    const/4 p1, 0x5

    .line 158
    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$3;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v1, v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v2, "every"

    invoke-virtual {v1, v2}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$3;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v1, v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v2, "second"

    .line 159
    invoke-virtual {v1, v2}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$3;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v1, v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v2, "at"

    invoke-virtual {v1, v2}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$3;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    .line 160
    invoke-static {v1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->access$100(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v1

    check-cast v1, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {v1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v0

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$3;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    invoke-static {v1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->access$200(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v1

    check-cast v1, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {v1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v0

    const-string v0, "%s %s %s %02d:%02d"

    .line 158
    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, ""

    return-object p1
.end method
