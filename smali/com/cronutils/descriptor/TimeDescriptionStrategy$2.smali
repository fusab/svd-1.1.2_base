.class Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;
.super Ljava/lang/Object;
.source "TimeDescriptionStrategy.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/descriptor/TimeDescriptionStrategy;->registerFunctions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function<",
        "Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;


# direct methods
.method constructor <init>(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)V
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 125
    check-cast p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;->apply(Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;)Ljava/lang/String;
    .locals 12

    .line 128
    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->hours:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/Always;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/On;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/On;

    if-eqz v0, :cond_2

    .line 131
    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v1, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast v1, Lcom/cronutils/model/field/expression/On;

    invoke-static {v0, v1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->access$000(Lcom/cronutils/descriptor/TimeDescriptionStrategy;Lcom/cronutils/model/field/expression/On;)Z

    move-result v0

    const/4 v1, 0x5

    const/4 v2, 0x4

    const-string v3, "minute"

    const/4 v4, 0x3

    const-string v5, "at"

    const/4 v6, 0x2

    const-string v7, "hour"

    const/4 v8, 0x1

    const-string v9, "every"

    const/4 v10, 0x0

    if-eqz v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v11, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast v11, Lcom/cronutils/model/field/expression/On;

    invoke-static {v0, v11}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->access$000(Lcom/cronutils/descriptor/TimeDescriptionStrategy;Lcom/cronutils/model/field/expression/On;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    new-array p1, v6, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v0, v0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v0, v9}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v10

    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v0, v0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v0, v7}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v8

    const-string v0, "%s %s "

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 135
    :cond_0
    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v1, v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v1, v9}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v10

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v1, v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    .line 136
    invoke-virtual {v1, v7}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v8

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v1, v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v1, v5}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v1, v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    .line 137
    invoke-virtual {v1, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    iget-object p1, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast p1, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v2

    const-string p1, "%s %s %s %s %s"

    .line 135
    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    const/16 v0, 0x8

    .line 139
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v11, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v11, v11, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v11, v9}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v0, v10

    iget-object v9, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v9, v9, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    .line 140
    invoke-virtual {v9, v7}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v0, v8

    iget-object v7, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v7, v7, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v7, v5}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v6

    iget-object v5, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v5, v5, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    .line 141
    invoke-virtual {v5, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v4

    iget-object v3, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast v3, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {v3}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    iget-object v2, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v2, v2, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v3, "and"

    .line 142
    invoke-virtual {v2, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v2, v2, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v3, "second"

    invoke-virtual {v2, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object p1, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast p1, Lcom/cronutils/model/field/expression/On;

    .line 143
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "%s %s %s %s %s %s %s %s"

    .line 139
    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    const-string p1, ""

    return-object p1
.end method
