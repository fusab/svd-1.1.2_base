.class Lcom/cronutils/descriptor/TimeDescriptionStrategy$7;
.super Ljava/lang/Object;
.source "TimeDescriptionStrategy.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/descriptor/TimeDescriptionStrategy;->registerFunctions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function<",
        "Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;


# direct methods
.method constructor <init>(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)V
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$7;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 240
    check-cast p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy$7;->apply(Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;)Ljava/lang/String;
    .locals 7

    .line 243
    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->hours:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/Always;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/Every;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/On;

    if-eqz v0, :cond_2

    .line 246
    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast v0, Lcom/cronutils/model/field/expression/Every;

    .line 248
    invoke-virtual {v0}, Lcom/cronutils/model/field/expression/Every;->getPeriod()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const-string v2, "%s %s"

    const-string v3, "every"

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-ne v1, v6, :cond_0

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$7;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object p1, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast p1, Lcom/cronutils/model/field/expression/On;

    .line 249
    invoke-static {v1, p1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->access$000(Lcom/cronutils/descriptor/TimeDescriptionStrategy;Lcom/cronutils/model/field/expression/On;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 250
    new-array p1, v4, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$7;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v1, v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v1, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v5

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$7;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v1, v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v3, "minute"

    invoke-virtual {v1, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v6

    invoke-static {v2, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x3

    .line 252
    new-array p1, p1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$7;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v1, v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v1, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v5

    .line 253
    invoke-virtual {v0}, Lcom/cronutils/model/field/expression/Every;->getPeriod()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v6

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$7;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v1, v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v3, "minutes"

    invoke-virtual {v1, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v4

    const-string v1, "%s %s %s "

    .line 252
    invoke-static {v1, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 255
    :goto_0
    invoke-virtual {v0}, Lcom/cronutils/model/field/expression/Every;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v1

    instance-of v1, v1, Lcom/cronutils/model/field/expression/Between;

    if-eqz v1, :cond_1

    .line 256
    new-array v1, v4, [Ljava/lang/Object;

    aput-object p1, v1, v5

    iget-object p1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$7;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    invoke-virtual {v0}, Lcom/cronutils/model/field/expression/Every;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->describe(Lcom/cronutils/model/field/expression/FieldExpression;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v6

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :cond_1
    return-object p1

    :cond_2
    const-string p1, ""

    return-object p1
.end method
