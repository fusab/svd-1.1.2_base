.class public Lcom/cronutils/descriptor/CronDescriptor;
.super Ljava/lang/Object;
.source "CronDescriptor.java"


# static fields
.field private static final BUNDLE:Ljava/lang/String; = "CronUtilsI18N"

.field public static final DEFAULT_LOCALE:Ljava/util/Locale;


# instance fields
.field private bundle:Ljava/util/ResourceBundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    sget-object v0, Ljava/util/Locale;->UK:Ljava/util/Locale;

    sput-object v0, Lcom/cronutils/descriptor/CronDescriptor;->DEFAULT_LOCALE:Ljava/util/Locale;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    sget-object v0, Lcom/cronutils/descriptor/CronDescriptor;->DEFAULT_LOCALE:Ljava/util/Locale;

    const-string v1, "CronUtilsI18N"

    invoke-static {v1, v0}, Ljava/util/ResourceBundle;->getBundle(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/ResourceBundle;

    move-result-object v0

    iput-object v0, p0, Lcom/cronutils/descriptor/CronDescriptor;->bundle:Ljava/util/ResourceBundle;

    return-void
.end method

.method private constructor <init>(Ljava/util/Locale;)V
    .locals 1

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "CronUtilsI18N"

    .line 39
    invoke-static {v0, p1}, Ljava/util/ResourceBundle;->getBundle(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/ResourceBundle;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/descriptor/CronDescriptor;->bundle:Ljava/util/ResourceBundle;

    return-void
.end method

.method private addExpressions(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "%s"

    .line 130
    invoke-virtual {p1, v0, p2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "%p"

    .line 131
    invoke-virtual {p1, p2, p3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private addTimeExpressions(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "%s"

    .line 111
    invoke-virtual {p1, v0, p2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "%p"

    .line 112
    invoke-virtual {p1, p2, p3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private describeDayOfMonth(Ljava/util/Map;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/cronutils/model/field/CronFieldName;",
            "Lcom/cronutils/model/field/CronField;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/cronutils/descriptor/CronDescriptor;->bundle:Ljava/util/ResourceBundle;

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    .line 90
    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/CronField;

    invoke-virtual {p1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 88
    :goto_0
    invoke-static {v0, p1}, Lcom/cronutils/descriptor/DescriptionStrategyFactory;->daysOfMonthInstance(Ljava/util/ResourceBundle;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/descriptor/DescriptionStrategy;

    move-result-object p1

    .line 91
    invoke-virtual {p1}, Lcom/cronutils/descriptor/DescriptionStrategy;->describe()Ljava/lang/String;

    move-result-object p1

    .line 92
    iget-object v0, p0, Lcom/cronutils/descriptor/CronDescriptor;->bundle:Ljava/util/ResourceBundle;

    const-string v1, "day"

    invoke-virtual {v0, v1}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/cronutils/descriptor/CronDescriptor;->bundle:Ljava/util/ResourceBundle;

    const-string v2, "days"

    invoke-virtual {v1, v2}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/cronutils/descriptor/CronDescriptor;->addTimeExpressions(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private describeDayOfWeek(Ljava/util/Map;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/cronutils/model/field/CronFieldName;",
            "Lcom/cronutils/model/field/CronField;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 121
    iget-object v0, p0, Lcom/cronutils/descriptor/CronDescriptor;->bundle:Ljava/util/ResourceBundle;

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    .line 123
    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/CronField;

    invoke-virtual {p1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 121
    :goto_0
    invoke-static {v0, p1}, Lcom/cronutils/descriptor/DescriptionStrategyFactory;->daysOfWeekInstance(Ljava/util/ResourceBundle;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/descriptor/DescriptionStrategy;

    move-result-object p1

    .line 124
    invoke-virtual {p1}, Lcom/cronutils/descriptor/DescriptionStrategy;->describe()Ljava/lang/String;

    move-result-object p1

    .line 125
    iget-object v0, p0, Lcom/cronutils/descriptor/CronDescriptor;->bundle:Ljava/util/ResourceBundle;

    const-string v1, "day"

    invoke-virtual {v0, v1}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/cronutils/descriptor/CronDescriptor;->bundle:Ljava/util/ResourceBundle;

    const-string v2, "days"

    invoke-virtual {v1, v2}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/cronutils/descriptor/CronDescriptor;->addExpressions(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private describeHHmmss(Ljava/util/Map;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/cronutils/model/field/CronFieldName;",
            "Lcom/cronutils/model/field/CronField;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/cronutils/descriptor/CronDescriptor;->bundle:Ljava/util/ResourceBundle;

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->HOUR:Lcom/cronutils/model/field/CronFieldName;

    .line 76
    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->HOUR:Lcom/cronutils/model/field/CronFieldName;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    sget-object v3, Lcom/cronutils/model/field/CronFieldName;->MINUTE:Lcom/cronutils/model/field/CronFieldName;

    .line 77
    invoke-interface {p1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/cronutils/model/field/CronFieldName;->MINUTE:Lcom/cronutils/model/field/CronFieldName;

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cronutils/model/field/CronField;

    invoke-virtual {v3}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v3

    goto :goto_1

    :cond_1
    move-object v3, v2

    :goto_1
    sget-object v4, Lcom/cronutils/model/field/CronFieldName;->SECOND:Lcom/cronutils/model/field/CronFieldName;

    .line 78
    invoke-interface {p1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v2, Lcom/cronutils/model/field/CronFieldName;->SECOND:Lcom/cronutils/model/field/CronFieldName;

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/CronField;

    invoke-virtual {p1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v2

    .line 74
    :cond_2
    invoke-static {v0, v1, v3, v2}, Lcom/cronutils/descriptor/DescriptionStrategyFactory;->hhMMssInstance(Ljava/util/ResourceBundle;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/descriptor/DescriptionStrategy;

    move-result-object p1

    .line 79
    invoke-virtual {p1}, Lcom/cronutils/descriptor/DescriptionStrategy;->describe()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private describeMonth(Ljava/util/Map;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/cronutils/model/field/CronFieldName;",
            "Lcom/cronutils/model/field/CronField;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 101
    iget-object v0, p0, Lcom/cronutils/descriptor/CronDescriptor;->bundle:Ljava/util/ResourceBundle;

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->MONTH:Lcom/cronutils/model/field/CronFieldName;

    .line 103
    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->MONTH:Lcom/cronutils/model/field/CronFieldName;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/CronField;

    invoke-virtual {p1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 101
    :goto_0
    invoke-static {v0, p1}, Lcom/cronutils/descriptor/DescriptionStrategyFactory;->monthsInstance(Ljava/util/ResourceBundle;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/descriptor/DescriptionStrategy;

    move-result-object p1

    .line 104
    invoke-virtual {p1}, Lcom/cronutils/descriptor/DescriptionStrategy;->describe()Ljava/lang/String;

    move-result-object p1

    .line 106
    iget-object v0, p0, Lcom/cronutils/descriptor/CronDescriptor;->bundle:Ljava/util/ResourceBundle;

    const-string v1, "month"

    invoke-virtual {v0, v1}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/cronutils/descriptor/CronDescriptor;->bundle:Ljava/util/ResourceBundle;

    const-string v2, "months"

    invoke-virtual {v1, v2}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/cronutils/descriptor/CronDescriptor;->addTimeExpressions(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private describeYear(Ljava/util/Map;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/cronutils/model/field/CronFieldName;",
            "Lcom/cronutils/model/field/CronField;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 140
    iget-object v0, p0, Lcom/cronutils/descriptor/CronDescriptor;->bundle:Ljava/util/ResourceBundle;

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->YEAR:Lcom/cronutils/model/field/CronFieldName;

    .line 143
    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->YEAR:Lcom/cronutils/model/field/CronFieldName;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/CronField;

    invoke-virtual {p1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 141
    :goto_0
    invoke-static {v0, p1}, Lcom/cronutils/descriptor/DescriptionStrategyFactory;->plainInstance(Ljava/util/ResourceBundle;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/descriptor/DescriptionStrategy;

    move-result-object p1

    .line 144
    invoke-virtual {p1}, Lcom/cronutils/descriptor/DescriptionStrategy;->describe()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/cronutils/descriptor/CronDescriptor;->bundle:Ljava/util/ResourceBundle;

    const-string/jumbo v3, "year"

    .line 145
    invoke-virtual {v2, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 140
    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public static instance()Lcom/cronutils/descriptor/CronDescriptor;
    .locals 1

    .line 153
    new-instance v0, Lcom/cronutils/descriptor/CronDescriptor;

    invoke-direct {v0}, Lcom/cronutils/descriptor/CronDescriptor;-><init>()V

    return-object v0
.end method

.method public static instance(Ljava/util/Locale;)Lcom/cronutils/descriptor/CronDescriptor;
    .locals 1

    .line 162
    new-instance v0, Lcom/cronutils/descriptor/CronDescriptor;

    invoke-direct {v0, p0}, Lcom/cronutils/descriptor/CronDescriptor;-><init>(Ljava/util/Locale;)V

    return-object v0
.end method


# virtual methods
.method public describe(Lcom/cronutils/model/Cron;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    .line 56
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Cron must not be null"

    invoke-static {p1, v1, v0}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    invoke-virtual {p1}, Lcom/cronutils/model/Cron;->retrieveFieldsAsMap()Ljava/util/Map;

    move-result-object p1

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    invoke-direct {p0, p1}, Lcom/cronutils/descriptor/CronDescriptor;->describeHHmmss(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    invoke-direct {p0, p1}, Lcom/cronutils/descriptor/CronDescriptor;->describeDayOfMonth(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    invoke-direct {p0, p1}, Lcom/cronutils/descriptor/CronDescriptor;->describeMonth(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    invoke-direct {p0, p1}, Lcom/cronutils/descriptor/CronDescriptor;->describeDayOfWeek(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    invoke-direct {p0, p1}, Lcom/cronutils/descriptor/CronDescriptor;->describeYear(Ljava/util/Map;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "\\s+"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
