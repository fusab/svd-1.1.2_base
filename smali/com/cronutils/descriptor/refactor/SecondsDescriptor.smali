.class Lcom/cronutils/descriptor/refactor/SecondsDescriptor;
.super Ljava/lang/Object;
.source "SecondsDescriptor.java"

# interfaces
.implements Lcom/cronutils/model/field/expression/visitor/FieldExpressionVisitor;


# instance fields
.field protected bundle:Ljava/util/ResourceBundle;


# direct methods
.method constructor <init>(Ljava/util/ResourceBundle;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->bundle:Ljava/util/ResourceBundle;

    return-void
.end method


# virtual methods
.method createAndDescription(Ljava/lang/StringBuilder;Ljava/util/List;)Ljava/lang/StringBuilder;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/List<",
            "Lcom/cronutils/model/field/expression/FieldExpression;",
            ">;)",
            "Ljava/lang/StringBuilder;"
        }
    .end annotation

    .line 119
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    const-string v1, " %s "

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ltz v0, :cond_1

    const/4 v0, 0x0

    .line 120
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    if-ge v0, v4, :cond_0

    .line 121
    new-array v4, v3, [Ljava/lang/Object;

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-virtual {p0, v5, v3}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->describe(Lcom/cronutils/model/field/expression/FieldExpression;Z)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    const-string v5, " %s, "

    invoke-static {v5, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    :cond_0
    new-array v0, v3, [Ljava/lang/Object;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-virtual {p0, v4, v3}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->describe(Lcom/cronutils/model/field/expression/FieldExpression;Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v2

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    :cond_1
    new-array v0, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->bundle:Ljava/util/ResourceBundle;

    const-string v5, "and"

    invoke-virtual {v4, v5}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v2

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v0, v3

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-virtual {p0, p2, v3}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->describe(Lcom/cronutils/model/field/expression/FieldExpression;Z)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object p1
.end method

.method protected describe(Lcom/cronutils/model/field/expression/Always;Z)Ljava/lang/String;
    .locals 0

    .line 81
    iget-object p1, p0, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->bundle:Ljava/util/ResourceBundle;

    const-string p2, "every"

    invoke-virtual {p1, p2}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected describe(Lcom/cronutils/model/field/expression/And;)Ljava/lang/String;
    .locals 4

    .line 90
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 91
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 92
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/And;->getExpressions()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cronutils/model/field/expression/FieldExpression;

    .line 93
    instance-of v3, v2, Lcom/cronutils/model/field/expression/On;

    if-eqz v3, :cond_0

    .line 94
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 96
    :cond_0
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 99
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 101
    iget-object v2, p0, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->bundle:Ljava/util/ResourceBundle;

    const-string v3, "at"

    invoke-virtual {v2, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    invoke-virtual {p0, p1, v1}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->createAndDescription(Ljava/lang/StringBuilder;Ljava/util/List;)Ljava/lang/StringBuilder;

    .line 104
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 105
    invoke-virtual {p0, p1, v0}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->createAndDescription(Ljava/lang/StringBuilder;Ljava/util/List;)Ljava/lang/StringBuilder;

    .line 108
    :cond_3
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected describe(Lcom/cronutils/model/field/expression/Between;Z)Ljava/lang/String;
    .locals 4

    .line 136
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->bundle:Ljava/util/ResourceBundle;

    const-string v1, "between_x_and_y"

    .line 139
    invoke-virtual {v0, v1}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 140
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getFrom()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->nominalValue(Lcom/cronutils/model/field/value/FieldValue;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 141
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getTo()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->nominalValue(Lcom/cronutils/model/field/value/FieldValue;)Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x1

    aput-object p1, v1, v2

    .line 138
    invoke-static {v0, v1}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 137
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " "

    .line 144
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected describe(Lcom/cronutils/model/field/expression/Every;Z)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public describe(Lcom/cronutils/model/field/expression/FieldExpression;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 29
    invoke-virtual {p0, p1, v0}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->describe(Lcom/cronutils/model/field/expression/FieldExpression;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected describe(Lcom/cronutils/model/field/expression/FieldExpression;Z)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    .line 40
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CronFieldExpression should not be null!"

    invoke-static {p1, v1, v0}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    instance-of v0, p1, Lcom/cronutils/model/field/expression/Always;

    if-eqz v0, :cond_0

    .line 42
    check-cast p1, Lcom/cronutils/model/field/expression/Always;

    invoke-virtual {p0, p1, p2}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->describe(Lcom/cronutils/model/field/expression/Always;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 44
    :cond_0
    instance-of v0, p1, Lcom/cronutils/model/field/expression/And;

    if-eqz v0, :cond_1

    .line 45
    check-cast p1, Lcom/cronutils/model/field/expression/And;

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->describe(Lcom/cronutils/model/field/expression/And;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 47
    :cond_1
    instance-of v0, p1, Lcom/cronutils/model/field/expression/Between;

    if-eqz v0, :cond_2

    .line 48
    check-cast p1, Lcom/cronutils/model/field/expression/Between;

    invoke-virtual {p0, p1, p2}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->describe(Lcom/cronutils/model/field/expression/Between;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 50
    :cond_2
    instance-of v0, p1, Lcom/cronutils/model/field/expression/Every;

    if-eqz v0, :cond_3

    .line 51
    check-cast p1, Lcom/cronutils/model/field/expression/Every;

    invoke-virtual {p0, p1, p2}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->describe(Lcom/cronutils/model/field/expression/Every;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 53
    :cond_3
    instance-of v0, p1, Lcom/cronutils/model/field/expression/On;

    if-eqz v0, :cond_4

    .line 54
    check-cast p1, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {p0, p1, p2}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->describe(Lcom/cronutils/model/field/expression/On;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_4
    const-string p1, ""

    return-object p1
.end method

.method protected describe(Lcom/cronutils/model/field/expression/On;Z)Ljava/lang/String;
    .locals 4

    if-eqz p2, :cond_0

    .line 159
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->nominalValue(Lcom/cronutils/model/field/value/FieldValue;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 161
    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->bundle:Ljava/util/ResourceBundle;

    const-string v3, "at"

    invoke-virtual {v2, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->nominalValue(Lcom/cronutils/model/field/value/FieldValue;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "%s %s "

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "%s"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected nominalValue(Lcom/cronutils/model/field/value/FieldValue;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    .line 68
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "FieldValue must not be null"

    invoke-static {p1, v1, v0}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    instance-of v0, p1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    if-eqz v0, :cond_0

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    check-cast p1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-virtual {p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 72
    :cond_0
    invoke-virtual {p1}, Lcom/cronutils/model/field/value/FieldValue;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public visit(Lcom/cronutils/model/field/expression/Always;)Lcom/cronutils/model/field/expression/Always;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public visit(Lcom/cronutils/model/field/expression/And;)Lcom/cronutils/model/field/expression/And;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public visit(Lcom/cronutils/model/field/expression/Between;)Lcom/cronutils/model/field/expression/Between;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public visit(Lcom/cronutils/model/field/expression/Every;)Lcom/cronutils/model/field/expression/Every;
    .locals 6

    .line 192
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Every;->getPeriod()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v1, "every"

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v5, v1}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Every;->getPeriod()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->nominalValue(Lcom/cronutils/model/field/value/FieldValue;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v2

    const-string v1, "%s %s "

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " %p "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0

    .line 195
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v2, v1}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " %s "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic visit(Lcom/cronutils/model/field/expression/Always;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    .line 15
    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->visit(Lcom/cronutils/model/field/expression/Always;)Lcom/cronutils/model/field/expression/Always;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic visit(Lcom/cronutils/model/field/expression/And;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    .line 15
    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->visit(Lcom/cronutils/model/field/expression/And;)Lcom/cronutils/model/field/expression/And;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic visit(Lcom/cronutils/model/field/expression/Between;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    .line 15
    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->visit(Lcom/cronutils/model/field/expression/Between;)Lcom/cronutils/model/field/expression/Between;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic visit(Lcom/cronutils/model/field/expression/Every;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    .line 15
    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->visit(Lcom/cronutils/model/field/expression/Every;)Lcom/cronutils/model/field/expression/Every;

    move-result-object p1

    return-object p1
.end method

.method public visit(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic visit(Lcom/cronutils/model/field/expression/On;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    .line 15
    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->visit(Lcom/cronutils/model/field/expression/On;)Lcom/cronutils/model/field/expression/On;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic visit(Lcom/cronutils/model/field/expression/QuestionMark;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    .line 15
    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/refactor/SecondsDescriptor;->visit(Lcom/cronutils/model/field/expression/QuestionMark;)Lcom/cronutils/model/field/expression/QuestionMark;

    move-result-object p1

    return-object p1
.end method

.method public visit(Lcom/cronutils/model/field/expression/On;)Lcom/cronutils/model/field/expression/On;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public visit(Lcom/cronutils/model/field/expression/QuestionMark;)Lcom/cronutils/model/field/expression/QuestionMark;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method
