.class Lcom/cronutils/descriptor/TimeDescriptionStrategy$4;
.super Ljava/lang/Object;
.source "TimeDescriptionStrategy.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/descriptor/TimeDescriptionStrategy;->registerFunctions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function<",
        "Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;


# direct methods
.method constructor <init>(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)V
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$4;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 169
    check-cast p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy$4;->apply(Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;)Ljava/lang/String;
    .locals 6

    .line 172
    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->hours:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/On;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/On;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/On;

    if-eqz v0, :cond_1

    .line 175
    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$4;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object p1, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast p1, Lcom/cronutils/model/field/expression/On;

    invoke-static {v0, p1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->access$000(Lcom/cronutils/descriptor/TimeDescriptionStrategy;Lcom/cronutils/model/field/expression/On;)Z

    move-result p1

    const/4 v0, 0x3

    const/4 v1, 0x2

    const/4 v2, 0x1

    const-string v3, "at"

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    .line 176
    new-array p1, v0, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$4;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v0, v0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v0, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v4

    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$4;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    .line 177
    invoke-static {v0}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->access$100(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    check-cast v0, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {v0}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v2

    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$4;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    .line 178
    invoke-static {v0}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->access$200(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    check-cast v0, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {v0}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v1

    const-string v0, "%s %02d:%02d"

    .line 176
    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x4

    .line 180
    new-array p1, p1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$4;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v5, v5, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v5, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, p1, v4

    iget-object v3, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$4;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    .line 181
    invoke-static {v3}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->access$100(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v3

    check-cast v3, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {v3}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, p1, v2

    iget-object v2, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$4;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    .line 182
    invoke-static {v2}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->access$200(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v2

    check-cast v2, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {v2}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p1, v1

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$4;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    invoke-static {v1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->access$300(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v1

    check-cast v1, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {v1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v0

    const-string v0, "%s %02d:%02d:%02d"

    .line 180
    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    const-string p1, ""

    return-object p1
.end method
