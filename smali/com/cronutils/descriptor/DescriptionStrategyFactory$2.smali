.class final Lcom/cronutils/descriptor/DescriptionStrategyFactory$2;
.super Ljava/lang/Object;
.source "DescriptionStrategyFactory.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/descriptor/DescriptionStrategyFactory;->daysOfWeekInstance(Ljava/util/ResourceBundle;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/descriptor/DescriptionStrategy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function<",
        "Lcom/cronutils/model/field/expression/FieldExpression;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$bundle:Ljava/util/ResourceBundle;

.field final synthetic val$nominal:Lcom/google/common/base/Function;


# direct methods
.method constructor <init>(Lcom/google/common/base/Function;Ljava/util/ResourceBundle;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/cronutils/descriptor/DescriptionStrategyFactory$2;->val$nominal:Lcom/google/common/base/Function;

    iput-object p2, p0, Lcom/cronutils/descriptor/DescriptionStrategyFactory$2;->val$bundle:Ljava/util/ResourceBundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/DescriptionStrategyFactory$2;->apply(Lcom/cronutils/model/field/expression/FieldExpression;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/cronutils/model/field/expression/FieldExpression;)Ljava/lang/String;
    .locals 8

    .line 44
    instance-of v0, p1, Lcom/cronutils/model/field/expression/On;

    const-string v1, ""

    if-eqz v0, :cond_2

    .line 45
    check-cast p1, Lcom/cronutils/model/field/expression/On;

    .line 46
    sget-object v0, Lcom/cronutils/descriptor/DescriptionStrategyFactory$5;->$SwitchMap$com$cronutils$model$field$value$SpecialChar:[I

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getSpecialChar()Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;->getValue()Lcom/cronutils/model/field/value/SpecialChar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cronutils/model/field/value/SpecialChar;->ordinal()I

    move-result v2

    aget v0, v0, v2

    const-string v2, "of_every_month"

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "%s %s %s "

    const/4 v6, 0x2

    const/4 v7, 0x1

    if-eq v0, v7, :cond_1

    if-eq v0, v6, :cond_0

    return-object v1

    .line 50
    :cond_0
    new-array v0, v4, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/cronutils/descriptor/DescriptionStrategyFactory$2;->val$bundle:Ljava/util/ResourceBundle;

    const-string v4, "last"

    invoke-virtual {v1, v4}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/cronutils/descriptor/DescriptionStrategyFactory$2;->val$nominal:Lcom/google/common/base/Function;

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    aput-object p1, v0, v7

    iget-object p1, p0, Lcom/cronutils/descriptor/DescriptionStrategyFactory$2;->val$bundle:Ljava/util/ResourceBundle;

    invoke-virtual {p1, v2}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v6

    invoke-static {v5, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 48
    :cond_1
    new-array v0, v4, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/cronutils/descriptor/DescriptionStrategyFactory$2;->val$nominal:Lcom/google/common/base/Function;

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v4

    invoke-virtual {v4}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getNth()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    aput-object p1, v0, v7

    iget-object p1, p0, Lcom/cronutils/descriptor/DescriptionStrategyFactory$2;->val$bundle:Ljava/util/ResourceBundle;

    invoke-virtual {p1, v2}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v6

    invoke-static {v5, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    return-object v1
.end method
