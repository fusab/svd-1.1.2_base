.class Lcom/cronutils/descriptor/TimeDescriptionStrategy$1;
.super Ljava/lang/Object;
.source "TimeDescriptionStrategy.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/descriptor/TimeDescriptionStrategy;->registerFunctions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function<",
        "Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;


# direct methods
.method constructor <init>(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$1;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 101
    check-cast p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy$1;->apply(Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;)Ljava/lang/String;
    .locals 8

    .line 104
    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->hours:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/Always;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/Always;

    if-eqz v0, :cond_2

    .line 106
    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/Always;

    const-string v1, "second"

    const-string v2, "%s %s "

    const/4 v3, 0x1

    const-string v4, "every"

    const/4 v5, 0x0

    const/4 v6, 0x2

    if-eqz v0, :cond_0

    .line 107
    new-array p1, v6, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$1;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v0, v0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v0, v4}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v5

    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$1;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v0, v0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v0, v1}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v3

    invoke-static {v2, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 109
    :cond_0
    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/On;

    if-eqz v0, :cond_2

    .line 110
    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$1;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v7, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast v7, Lcom/cronutils/model/field/expression/On;

    invoke-static {v0, v7}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->access$000(Lcom/cronutils/descriptor/TimeDescriptionStrategy;Lcom/cronutils/model/field/expression/On;)Z

    move-result v0

    const-string v7, "minute"

    if-eqz v0, :cond_1

    .line 111
    new-array p1, v6, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$1;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v0, v0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v0, v4}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v5

    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$1;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v0, v0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v0, v7}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v3

    invoke-static {v2, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 v0, 0x5

    .line 113
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$1;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v2, v2, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v2, v4}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    iget-object v2, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$1;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v2, v2, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    .line 114
    invoke-virtual {v2, v7}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    iget-object v2, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$1;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v2, v2, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v3, "at"

    invoke-virtual {v2, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$1;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v3, v3, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    .line 115
    invoke-virtual {v3, v1}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x4

    iget-object p1, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast p1, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "%s %s %s %s %02d"

    .line 113
    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    const-string p1, ""

    return-object p1
.end method
