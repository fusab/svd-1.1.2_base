.class Lcom/cronutils/descriptor/NominalDescriptionStrategy;
.super Lcom/cronutils/descriptor/DescriptionStrategy;
.source "NominalDescriptionStrategy.java"


# instance fields
.field private descriptions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/google/common/base/Function<",
            "Lcom/cronutils/model/field/expression/FieldExpression;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private expression:Lcom/cronutils/model/field/expression/FieldExpression;


# direct methods
.method public constructor <init>(Ljava/util/ResourceBundle;Lcom/google/common/base/Function;Lcom/cronutils/model/field/expression/FieldExpression;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ResourceBundle;",
            "Lcom/google/common/base/Function<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/cronutils/model/field/expression/FieldExpression;",
            ")V"
        }
    .end annotation

    .line 42
    invoke-direct {p0, p1}, Lcom/cronutils/descriptor/DescriptionStrategy;-><init>(Ljava/util/ResourceBundle;)V

    .line 43
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/descriptor/NominalDescriptionStrategy;->descriptions:Ljava/util/Set;

    if-eqz p2, :cond_0

    .line 45
    iput-object p2, p0, Lcom/cronutils/descriptor/NominalDescriptionStrategy;->nominalValueFunction:Lcom/google/common/base/Function;

    :cond_0
    if-eqz p3, :cond_1

    .line 48
    iput-object p3, p0, Lcom/cronutils/descriptor/NominalDescriptionStrategy;->expression:Lcom/cronutils/model/field/expression/FieldExpression;

    goto :goto_0

    .line 50
    :cond_1
    new-instance p1, Lcom/cronutils/model/field/expression/Always;

    invoke-direct {p1}, Lcom/cronutils/model/field/expression/Always;-><init>()V

    iput-object p1, p0, Lcom/cronutils/descriptor/NominalDescriptionStrategy;->expression:Lcom/cronutils/model/field/expression/FieldExpression;

    :goto_0
    return-void
.end method


# virtual methods
.method public addDescription(Lcom/google/common/base/Function;)Lcom/cronutils/descriptor/NominalDescriptionStrategy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Function<",
            "Lcom/cronutils/model/field/expression/FieldExpression;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/cronutils/descriptor/NominalDescriptionStrategy;"
        }
    .end annotation

    .line 73
    iget-object v0, p0, Lcom/cronutils/descriptor/NominalDescriptionStrategy;->descriptions:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public describe()Ljava/lang/String;
    .locals 4

    .line 56
    iget-object v0, p0, Lcom/cronutils/descriptor/NominalDescriptionStrategy;->descriptions:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/base/Function;

    .line 57
    iget-object v2, p0, Lcom/cronutils/descriptor/NominalDescriptionStrategy;->expression:Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-interface {v1, v2}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 58
    iget-object v0, p0, Lcom/cronutils/descriptor/NominalDescriptionStrategy;->expression:Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-interface {v1, v0}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/cronutils/descriptor/NominalDescriptionStrategy;->expression:Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-virtual {p0, v0}, Lcom/cronutils/descriptor/NominalDescriptionStrategy;->describe(Lcom/cronutils/model/field/expression/FieldExpression;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
