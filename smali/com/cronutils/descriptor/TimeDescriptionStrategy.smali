.class Lcom/cronutils/descriptor/TimeDescriptionStrategy;
.super Lcom/cronutils/descriptor/DescriptionStrategy;
.source "TimeDescriptionStrategy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;
    }
.end annotation


# instance fields
.field private defaultSeconds:I

.field private descriptions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/google/common/base/Function<",
            "Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private hours:Lcom/cronutils/model/field/expression/FieldExpression;

.field private minutes:Lcom/cronutils/model/field/expression/FieldExpression;

.field private seconds:Lcom/cronutils/model/field/expression/FieldExpression;


# direct methods
.method constructor <init>(Ljava/util/ResourceBundle;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/expression/FieldExpression;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1}, Lcom/cronutils/descriptor/DescriptionStrategy;-><init>(Ljava/util/ResourceBundle;)V

    const/4 p1, 0x0

    .line 34
    iput p1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->defaultSeconds:I

    .line 46
    new-instance p1, Lcom/cronutils/model/field/expression/Always;

    invoke-direct {p1}, Lcom/cronutils/model/field/expression/Always;-><init>()V

    invoke-direct {p0, p2, p1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->ensureInstance(Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->hours:Lcom/cronutils/model/field/expression/FieldExpression;

    .line 47
    new-instance p1, Lcom/cronutils/model/field/expression/Always;

    invoke-direct {p1}, Lcom/cronutils/model/field/expression/Always;-><init>()V

    invoke-direct {p0, p3, p1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->ensureInstance(Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    .line 48
    new-instance p1, Lcom/cronutils/model/field/expression/On;

    new-instance p2, Lcom/cronutils/model/field/value/IntegerFieldValue;

    iget p3, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->defaultSeconds:I

    invoke-direct {p2, p3}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {p1, p2}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    invoke-direct {p0, p4, p1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->ensureInstance(Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    .line 49
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->descriptions:Ljava/util/Set;

    .line 50
    invoke-direct {p0}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->registerFunctions()V

    return-void
.end method

.method static synthetic access$000(Lcom/cronutils/descriptor/TimeDescriptionStrategy;Lcom/cronutils/model/field/expression/On;)Z
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->isDefault(Lcom/cronutils/model/field/expression/On;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$100(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->hours:Lcom/cronutils/model/field/expression/FieldExpression;

    return-object p0
.end method

.method static synthetic access$200(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    return-object p0
.end method

.method static synthetic access$300(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    return-object p0
.end method

.method private addTimeExpressions(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "%s"

    .line 90
    invoke-virtual {p1, v0, p2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "%p"

    .line 91
    invoke-virtual {p1, p2, p3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private ensureInstance(Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 2

    const/4 v0, 0x0

    .line 60
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Default expression must not be null"

    invoke-static {p2, v1, v0}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_0

    return-object p1

    :cond_0
    return-object p2
.end method

.method private isDefault(Lcom/cronutils/model/field/expression/On;)Z
    .locals 1

    .line 314
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->defaultSeconds:I

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private registerFunctions()V
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->descriptions:Ljava/util/Set;

    new-instance v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$1;

    invoke-direct {v1, p0}, Lcom/cronutils/descriptor/TimeDescriptionStrategy$1;-><init>(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 124
    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->descriptions:Ljava/util/Set;

    new-instance v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;

    invoke-direct {v1, p0}, Lcom/cronutils/descriptor/TimeDescriptionStrategy$2;-><init>(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 151
    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->descriptions:Ljava/util/Set;

    new-instance v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$3;

    invoke-direct {v1, p0}, Lcom/cronutils/descriptor/TimeDescriptionStrategy$3;-><init>(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 168
    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->descriptions:Ljava/util/Set;

    new-instance v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$4;

    invoke-direct {v1, p0}, Lcom/cronutils/descriptor/TimeDescriptionStrategy$4;-><init>(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 190
    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->descriptions:Ljava/util/Set;

    new-instance v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$5;

    invoke-direct {v1, p0}, Lcom/cronutils/descriptor/TimeDescriptionStrategy$5;-><init>(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 205
    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->descriptions:Ljava/util/Set;

    new-instance v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$6;

    invoke-direct {v1, p0}, Lcom/cronutils/descriptor/TimeDescriptionStrategy$6;-><init>(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 239
    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->descriptions:Ljava/util/Set;

    new-instance v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$7;

    invoke-direct {v1, p0}, Lcom/cronutils/descriptor/TimeDescriptionStrategy$7;-><init>(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 265
    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->descriptions:Ljava/util/Set;

    new-instance v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$8;

    invoke-direct {v1, p0}, Lcom/cronutils/descriptor/TimeDescriptionStrategy$8;-><init>(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public describe()Ljava/lang/String;
    .locals 6

    .line 70
    new-instance v0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->hours:Lcom/cronutils/model/field/expression/FieldExpression;

    iget-object v2, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    iget-object v3, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;-><init>(Lcom/cronutils/descriptor/TimeDescriptionStrategy;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/expression/FieldExpression;)V

    .line 71
    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->descriptions:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-string v3, ""

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/common/base/Function;

    .line 72
    invoke-interface {v2, v0}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 73
    invoke-interface {v2, v0}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 78
    :cond_1
    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->hours:Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-virtual {p0, v0}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->describe(Lcom/cronutils/model/field/expression/FieldExpression;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v2, "hour"

    invoke-virtual {v1, v2}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v4, "hours"

    invoke-virtual {v2, v4}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->addTimeExpressions(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v2, v1, Lcom/cronutils/model/field/expression/On;

    if-eqz v2, :cond_3

    check-cast v1, Lcom/cronutils/model/field/expression/On;

    invoke-direct {p0, v1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->isDefault(Lcom/cronutils/model/field/expression/On;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    move-object v1, v3

    goto :goto_1

    .line 80
    :cond_3
    :goto_0
    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-virtual {p0, v1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->describe(Lcom/cronutils/model/field/expression/FieldExpression;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v4, "second"

    invoke-virtual {v2, v4}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v5, "seconds"

    invoke-virtual {v4, v5}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v1, v2, v4}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->addTimeExpressions(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 82
    :goto_1
    iget-object v2, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v4, v2, Lcom/cronutils/model/field/expression/On;

    if-eqz v4, :cond_4

    check-cast v2, Lcom/cronutils/model/field/expression/On;

    invoke-direct {p0, v2}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->isDefault(Lcom/cronutils/model/field/expression/On;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 83
    :cond_4
    iget-object v2, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-virtual {p0, v2}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->describe(Lcom/cronutils/model/field/expression/FieldExpression;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v4, "minute"

    invoke-virtual {v3, v4}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v5, "minutes"

    invoke-virtual {v4, v5}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->addTimeExpressions(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_5
    const/4 v2, 0x3

    .line 85
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v2, v4

    const/4 v1, 0x1

    aput-object v3, v2, v1

    const/4 v1, 0x2

    aput-object v0, v2, v1

    const-string v0, "%s %s %s"

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
