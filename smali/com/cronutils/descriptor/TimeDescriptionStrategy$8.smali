.class Lcom/cronutils/descriptor/TimeDescriptionStrategy$8;
.super Ljava/lang/Object;
.source "TimeDescriptionStrategy.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/descriptor/TimeDescriptionStrategy;->registerFunctions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function<",
        "Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;


# direct methods
.method constructor <init>(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)V
    .locals 0

    .line 266
    iput-object p1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$8;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 266
    check-cast p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy$8;->apply(Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;)Ljava/lang/String;
    .locals 8

    .line 269
    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->hours:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/Every;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/On;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    instance-of v0, v0, Lcom/cronutils/model/field/expression/On;

    if-eqz v0, :cond_2

    .line 273
    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->minutes:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast v0, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {v0}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v1, "every"

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast v0, Lcom/cronutils/model/field/expression/On;

    .line 274
    invoke-virtual {v0}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 275
    new-array p1, v2, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$8;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v0, v0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v0, v1}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v4

    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$8;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v0, v0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v1, "hour"

    invoke-virtual {v0, v1}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v3

    const-string v0, "%s %s"

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 v0, 0x6

    .line 277
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$8;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v5, v5, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    .line 278
    invoke-virtual {v5, v1}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$8;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    invoke-static {v1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->access$100(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v1

    check-cast v1, Lcom/cronutils/model/field/expression/Every;

    invoke-virtual {v1}, Lcom/cronutils/model/field/expression/Every;->getPeriod()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$8;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v1, v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v5, "hours"

    invoke-virtual {v1, v5}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$8;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v1, v1, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v5, "at"

    .line 279
    invoke-virtual {v1, v5}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x3

    aput-object v1, v0, v5

    const/4 v1, 0x4

    iget-object v6, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$8;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v6, v6, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v7, "minute"

    invoke-virtual {v6, v7}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v1

    const/4 v1, 0x5

    iget-object v6, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$8;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    invoke-static {v6}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->access$200(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v6

    check-cast v6, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {v6}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v6

    invoke-virtual {v6}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v1

    const-string v1, "%s %s %s %s %s %s "

    .line 277
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 280
    iget-object v1, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$8;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object p1, p1, Lcom/cronutils/descriptor/TimeDescriptionStrategy$TimeFields;->seconds:Lcom/cronutils/model/field/expression/FieldExpression;

    check-cast p1, Lcom/cronutils/model/field/expression/On;

    invoke-static {v1, p1}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->access$000(Lcom/cronutils/descriptor/TimeDescriptionStrategy;Lcom/cronutils/model/field/expression/On;)Z

    move-result p1

    if-eqz p1, :cond_1

    return-object v0

    .line 283
    :cond_1
    new-array p1, v5, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$8;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v0, v0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v1, "and"

    invoke-virtual {v0, v1}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v4

    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$8;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    iget-object v0, v0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v1, "second"

    .line 284
    invoke-virtual {v0, v1}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v3

    iget-object v0, p0, Lcom/cronutils/descriptor/TimeDescriptionStrategy$8;->this$0:Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    invoke-static {v0}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;->access$300(Lcom/cronutils/descriptor/TimeDescriptionStrategy;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    check-cast v0, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {v0}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v2

    const-string v0, "%s %s %s"

    .line 283
    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    const-string p1, ""

    return-object p1
.end method
