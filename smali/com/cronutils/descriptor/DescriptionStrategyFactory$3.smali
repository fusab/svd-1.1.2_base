.class final Lcom/cronutils/descriptor/DescriptionStrategyFactory$3;
.super Ljava/lang/Object;
.source "DescriptionStrategyFactory.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/descriptor/DescriptionStrategyFactory;->daysOfMonthInstance(Ljava/util/ResourceBundle;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/descriptor/DescriptionStrategy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function<",
        "Lcom/cronutils/model/field/expression/FieldExpression;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$bundle:Ljava/util/ResourceBundle;


# direct methods
.method constructor <init>(Ljava/util/ResourceBundle;)V
    .locals 0

    .line 70
    iput-object p1, p0, Lcom/cronutils/descriptor/DescriptionStrategyFactory$3;->val$bundle:Ljava/util/ResourceBundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 70
    check-cast p1, Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/DescriptionStrategyFactory$3;->apply(Lcom/cronutils/model/field/expression/FieldExpression;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/cronutils/model/field/expression/FieldExpression;)Ljava/lang/String;
    .locals 5

    .line 73
    instance-of v0, p1, Lcom/cronutils/model/field/expression/On;

    const-string v1, ""

    if-eqz v0, :cond_3

    .line 74
    check-cast p1, Lcom/cronutils/model/field/expression/On;

    .line 75
    sget-object v0, Lcom/cronutils/descriptor/DescriptionStrategyFactory$5;->$SwitchMap$com$cronutils$model$field$value$SpecialChar:[I

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getSpecialChar()Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;->getValue()Lcom/cronutils/model/field/value/SpecialChar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cronutils/model/field/value/SpecialChar;->ordinal()I

    move-result v2

    aget v0, v0, v2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v3, 0x3

    if-eq v0, v3, :cond_1

    const/4 p1, 0x4

    if-eq v0, p1, :cond_0

    return-object v1

    .line 81
    :cond_0
    iget-object p1, p0, Lcom/cronutils/descriptor/DescriptionStrategyFactory$3;->val$bundle:Ljava/util/ResourceBundle;

    const-string v0, "last_weekday_of_month"

    invoke-virtual {p1, v0}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 77
    :cond_1
    new-array v0, v3, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/cronutils/descriptor/DescriptionStrategyFactory$3;->val$bundle:Ljava/util/ResourceBundle;

    const-string v4, "the_nearest_weekday_to_the"

    invoke-virtual {v3, v4}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v1

    iget-object p1, p0, Lcom/cronutils/descriptor/DescriptionStrategyFactory$3;->val$bundle:Ljava/util/ResourceBundle;

    const-string v1, "of_the_month"

    invoke-virtual {p1, v1}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    const-string p1, "%s %s %s "

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 79
    :cond_2
    iget-object p1, p0, Lcom/cronutils/descriptor/DescriptionStrategyFactory$3;->val$bundle:Ljava/util/ResourceBundle;

    const-string v0, "last_day_of_month"

    invoke-virtual {p1, v0}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    return-object v1
.end method
