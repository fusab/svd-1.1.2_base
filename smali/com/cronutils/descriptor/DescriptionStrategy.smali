.class abstract Lcom/cronutils/descriptor/DescriptionStrategy;
.super Ljava/lang/Object;
.source "DescriptionStrategy.java"


# instance fields
.field protected bundle:Ljava/util/ResourceBundle;

.field protected nominalValueFunction:Lcom/google/common/base/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Function<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/ResourceBundle;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/cronutils/descriptor/DescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    .line 37
    new-instance p1, Lcom/cronutils/descriptor/DescriptionStrategy$1;

    invoke-direct {p1, p0}, Lcom/cronutils/descriptor/DescriptionStrategy$1;-><init>(Lcom/cronutils/descriptor/DescriptionStrategy;)V

    iput-object p1, p0, Lcom/cronutils/descriptor/DescriptionStrategy;->nominalValueFunction:Lcom/google/common/base/Function;

    return-void
.end method

.method private createAndDescription(Ljava/lang/StringBuilder;Ljava/util/List;)Ljava/lang/StringBuilder;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/List<",
            "Lcom/cronutils/model/field/expression/FieldExpression;",
            ">;)",
            "Ljava/lang/StringBuilder;"
        }
    .end annotation

    .line 146
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    const-string v1, " %s "

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ltz v0, :cond_1

    const/4 v0, 0x0

    .line 147
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    if-ge v0, v4, :cond_0

    .line 148
    new-array v4, v3, [Ljava/lang/Object;

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-virtual {p0, v5, v3}, Lcom/cronutils/descriptor/DescriptionStrategy;->describe(Lcom/cronutils/model/field/expression/FieldExpression;Z)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    const-string v5, " %s, "

    invoke-static {v5, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 150
    :cond_0
    new-array v0, v3, [Ljava/lang/Object;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-virtual {p0, v4, v3}, Lcom/cronutils/descriptor/DescriptionStrategy;->describe(Lcom/cronutils/model/field/expression/FieldExpression;Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v2

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    :cond_1
    new-array v0, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/cronutils/descriptor/DescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v5, "and"

    invoke-virtual {v4, v5}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v2

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v0, v3

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-virtual {p0, p2, v3}, Lcom/cronutils/descriptor/DescriptionStrategy;->describe(Lcom/cronutils/model/field/expression/FieldExpression;Z)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object p1
.end method


# virtual methods
.method public abstract describe()Ljava/lang/String;
.end method

.method protected describe(Lcom/cronutils/model/field/expression/Always;Z)Ljava/lang/String;
    .locals 0

    const-string p1, ""

    return-object p1
.end method

.method protected describe(Lcom/cronutils/model/field/expression/And;)Ljava/lang/String;
    .locals 4

    .line 118
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 119
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 120
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/And;->getExpressions()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cronutils/model/field/expression/FieldExpression;

    .line 121
    instance-of v3, v2, Lcom/cronutils/model/field/expression/On;

    if-eqz v3, :cond_0

    .line 122
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 124
    :cond_0
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 127
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 128
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 129
    iget-object v2, p0, Lcom/cronutils/descriptor/DescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v3, "at"

    invoke-virtual {v2, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    invoke-direct {p0, p1, v1}, Lcom/cronutils/descriptor/DescriptionStrategy;->createAndDescription(Ljava/lang/StringBuilder;Ljava/util/List;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " %p"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 133
    invoke-direct {p0, p1, v0}, Lcom/cronutils/descriptor/DescriptionStrategy;->createAndDescription(Ljava/lang/StringBuilder;Ljava/util/List;)Ljava/lang/StringBuilder;

    .line 136
    :cond_3
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected describe(Lcom/cronutils/model/field/expression/Between;Z)Ljava/lang/String;
    .locals 4

    .line 163
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/cronutils/descriptor/DescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v1, "every"

    invoke-virtual {v0, v1}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " %s "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/cronutils/descriptor/DescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v1, "between_x_and_y"

    .line 165
    invoke-virtual {v0, v1}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 166
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getFrom()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/cronutils/descriptor/DescriptionStrategy;->nominalValue(Lcom/cronutils/model/field/value/FieldValue;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 167
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getTo()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/DescriptionStrategy;->nominalValue(Lcom/cronutils/model/field/value/FieldValue;)Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x1

    aput-object p1, v1, v2

    .line 164
    invoke-static {v0, v1}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected describe(Lcom/cronutils/model/field/expression/Every;Z)Ljava/lang/String;
    .locals 6

    .line 178
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Every;->getPeriod()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p2

    invoke-virtual {p2}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    const/4 v0, 0x0

    const/4 v1, 0x2

    const-string v2, "every"

    const/4 v3, 0x1

    if-le p2, v3, :cond_0

    .line 179
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/cronutils/descriptor/DescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v5, v2}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Every;->getPeriod()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/cronutils/descriptor/DescriptionStrategy;->nominalValue(Lcom/cronutils/model/field/value/FieldValue;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v3

    const-string v2, "%s %s "

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " %p "

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 181
    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/cronutils/descriptor/DescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v4, v2}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " %s "

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 183
    :goto_0
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Every;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v2

    instance-of v2, v2, Lcom/cronutils/model/field/expression/Between;

    if-eqz v2, :cond_1

    .line 184
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Every;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/expression/Between;

    .line 185
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/cronutils/descriptor/DescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v4, "between_x_and_y"

    .line 186
    invoke-virtual {p2, v4}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    new-array v1, v1, [Ljava/lang/Object;

    .line 187
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getFrom()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/cronutils/descriptor/DescriptionStrategy;->nominalValue(Lcom/cronutils/model/field/value/FieldValue;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v0

    .line 188
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getTo()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/DescriptionStrategy;->nominalValue(Lcom/cronutils/model/field/value/FieldValue;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v3

    .line 185
    invoke-static {p2, v1}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    :cond_1
    return-object p2
.end method

.method protected describe(Lcom/cronutils/model/field/expression/FieldExpression;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 57
    invoke-virtual {p0, p1, v0}, Lcom/cronutils/descriptor/DescriptionStrategy;->describe(Lcom/cronutils/model/field/expression/FieldExpression;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected describe(Lcom/cronutils/model/field/expression/FieldExpression;Z)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    .line 68
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CronFieldExpression should not be null!"

    invoke-static {p1, v1, v0}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    instance-of v0, p1, Lcom/cronutils/model/field/expression/Always;

    if-eqz v0, :cond_0

    .line 70
    check-cast p1, Lcom/cronutils/model/field/expression/Always;

    invoke-virtual {p0, p1, p2}, Lcom/cronutils/descriptor/DescriptionStrategy;->describe(Lcom/cronutils/model/field/expression/Always;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 72
    :cond_0
    instance-of v0, p1, Lcom/cronutils/model/field/expression/And;

    if-eqz v0, :cond_1

    .line 73
    check-cast p1, Lcom/cronutils/model/field/expression/And;

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/DescriptionStrategy;->describe(Lcom/cronutils/model/field/expression/And;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 75
    :cond_1
    instance-of v0, p1, Lcom/cronutils/model/field/expression/Between;

    if-eqz v0, :cond_2

    .line 76
    check-cast p1, Lcom/cronutils/model/field/expression/Between;

    invoke-virtual {p0, p1, p2}, Lcom/cronutils/descriptor/DescriptionStrategy;->describe(Lcom/cronutils/model/field/expression/Between;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 78
    :cond_2
    instance-of v0, p1, Lcom/cronutils/model/field/expression/Every;

    if-eqz v0, :cond_3

    .line 79
    check-cast p1, Lcom/cronutils/model/field/expression/Every;

    invoke-virtual {p0, p1, p2}, Lcom/cronutils/descriptor/DescriptionStrategy;->describe(Lcom/cronutils/model/field/expression/Every;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 81
    :cond_3
    instance-of v0, p1, Lcom/cronutils/model/field/expression/On;

    if-eqz v0, :cond_4

    .line 82
    check-cast p1, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {p0, p1, p2}, Lcom/cronutils/descriptor/DescriptionStrategy;->describe(Lcom/cronutils/model/field/expression/On;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_4
    const-string p1, ""

    return-object p1
.end method

.method protected describe(Lcom/cronutils/model/field/expression/On;Z)Ljava/lang/String;
    .locals 4

    if-eqz p2, :cond_0

    .line 201
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/DescriptionStrategy;->nominalValue(Lcom/cronutils/model/field/value/FieldValue;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 203
    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/cronutils/descriptor/DescriptionStrategy;->bundle:Ljava/util/ResourceBundle;

    const-string v3, "at"

    invoke-virtual {v2, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/cronutils/descriptor/DescriptionStrategy;->nominalValue(Lcom/cronutils/model/field/value/FieldValue;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "%s %s "

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "%s"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected nominalValue(Lcom/cronutils/model/field/value/FieldValue;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    .line 96
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "FieldValue must not be null"

    invoke-static {p1, v1, v0}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    instance-of v0, p1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/cronutils/descriptor/DescriptionStrategy;->nominalValueFunction:Lcom/google/common/base/Function;

    check-cast p1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-virtual {p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1

    .line 100
    :cond_0
    invoke-virtual {p1}, Lcom/cronutils/model/field/value/FieldValue;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
