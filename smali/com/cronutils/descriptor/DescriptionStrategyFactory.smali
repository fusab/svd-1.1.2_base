.class Lcom/cronutils/descriptor/DescriptionStrategyFactory;
.super Ljava/lang/Object;
.source "DescriptionStrategyFactory.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static daysOfMonthInstance(Ljava/util/ResourceBundle;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/descriptor/DescriptionStrategy;
    .locals 2

    .line 68
    new-instance v0, Lcom/cronutils/descriptor/NominalDescriptionStrategy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/cronutils/descriptor/NominalDescriptionStrategy;-><init>(Ljava/util/ResourceBundle;Lcom/google/common/base/Function;Lcom/cronutils/model/field/expression/FieldExpression;)V

    .line 70
    new-instance p1, Lcom/cronutils/descriptor/DescriptionStrategyFactory$3;

    invoke-direct {p1, p0}, Lcom/cronutils/descriptor/DescriptionStrategyFactory$3;-><init>(Ljava/util/ResourceBundle;)V

    invoke-virtual {v0, p1}, Lcom/cronutils/descriptor/NominalDescriptionStrategy;->addDescription(Lcom/google/common/base/Function;)Lcom/cronutils/descriptor/NominalDescriptionStrategy;

    return-object v0
.end method

.method public static daysOfWeekInstance(Ljava/util/ResourceBundle;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/descriptor/DescriptionStrategy;
    .locals 2

    .line 32
    new-instance v0, Lcom/cronutils/descriptor/DescriptionStrategyFactory$1;

    invoke-direct {v0, p0}, Lcom/cronutils/descriptor/DescriptionStrategyFactory$1;-><init>(Ljava/util/ResourceBundle;)V

    .line 39
    new-instance v1, Lcom/cronutils/descriptor/NominalDescriptionStrategy;

    invoke-direct {v1, p0, v0, p1}, Lcom/cronutils/descriptor/NominalDescriptionStrategy;-><init>(Ljava/util/ResourceBundle;Lcom/google/common/base/Function;Lcom/cronutils/model/field/expression/FieldExpression;)V

    .line 41
    new-instance p1, Lcom/cronutils/descriptor/DescriptionStrategyFactory$2;

    invoke-direct {p1, v0, p0}, Lcom/cronutils/descriptor/DescriptionStrategyFactory$2;-><init>(Lcom/google/common/base/Function;Ljava/util/ResourceBundle;)V

    invoke-virtual {v1, p1}, Lcom/cronutils/descriptor/NominalDescriptionStrategy;->addDescription(Lcom/google/common/base/Function;)Lcom/cronutils/descriptor/NominalDescriptionStrategy;

    return-object v1
.end method

.method public static hhMMssInstance(Ljava/util/ResourceBundle;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/descriptor/DescriptionStrategy;
    .locals 1

    .line 128
    new-instance v0, Lcom/cronutils/descriptor/TimeDescriptionStrategy;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/cronutils/descriptor/TimeDescriptionStrategy;-><init>(Ljava/util/ResourceBundle;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/expression/FieldExpression;)V

    return-object v0
.end method

.method public static monthsInstance(Ljava/util/ResourceBundle;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/descriptor/DescriptionStrategy;
    .locals 2

    .line 99
    new-instance v0, Lcom/cronutils/descriptor/NominalDescriptionStrategy;

    new-instance v1, Lcom/cronutils/descriptor/DescriptionStrategyFactory$4;

    invoke-direct {v1, p0}, Lcom/cronutils/descriptor/DescriptionStrategyFactory$4;-><init>(Ljava/util/ResourceBundle;)V

    invoke-direct {v0, p0, v1, p1}, Lcom/cronutils/descriptor/NominalDescriptionStrategy;-><init>(Ljava/util/ResourceBundle;Lcom/google/common/base/Function;Lcom/cronutils/model/field/expression/FieldExpression;)V

    return-object v0
.end method

.method public static plainInstance(Ljava/util/ResourceBundle;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/descriptor/DescriptionStrategy;
    .locals 2

    .line 118
    new-instance v0, Lcom/cronutils/descriptor/NominalDescriptionStrategy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/cronutils/descriptor/NominalDescriptionStrategy;-><init>(Ljava/util/ResourceBundle;Lcom/google/common/base/Function;Lcom/cronutils/model/field/expression/FieldExpression;)V

    return-object v0
.end method
