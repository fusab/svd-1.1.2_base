.class final enum Lcom/cronutils/TimeConstants;
.super Ljava/lang/Enum;
.source "TimeConstants.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/cronutils/TimeConstants;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cronutils/TimeConstants;

.field public static final enum DAY:Lcom/cronutils/TimeConstants;

.field public static final enum HOUR:Lcom/cronutils/TimeConstants;

.field public static final enum MINUTE:Lcom/cronutils/TimeConstants;

.field public static final enum SECOND:Lcom/cronutils/TimeConstants;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 16
    new-instance v0, Lcom/cronutils/TimeConstants;

    const/4 v1, 0x0

    const-string v2, "SECOND"

    invoke-direct {v0, v2, v1}, Lcom/cronutils/TimeConstants;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cronutils/TimeConstants;->SECOND:Lcom/cronutils/TimeConstants;

    new-instance v0, Lcom/cronutils/TimeConstants;

    const/4 v2, 0x1

    const-string v3, "MINUTE"

    invoke-direct {v0, v3, v2}, Lcom/cronutils/TimeConstants;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cronutils/TimeConstants;->MINUTE:Lcom/cronutils/TimeConstants;

    new-instance v0, Lcom/cronutils/TimeConstants;

    const/4 v3, 0x2

    const-string v4, "HOUR"

    invoke-direct {v0, v4, v3}, Lcom/cronutils/TimeConstants;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cronutils/TimeConstants;->HOUR:Lcom/cronutils/TimeConstants;

    new-instance v0, Lcom/cronutils/TimeConstants;

    const/4 v4, 0x3

    const-string v5, "DAY"

    invoke-direct {v0, v5, v4}, Lcom/cronutils/TimeConstants;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cronutils/TimeConstants;->DAY:Lcom/cronutils/TimeConstants;

    const/4 v0, 0x4

    .line 15
    new-array v0, v0, [Lcom/cronutils/TimeConstants;

    sget-object v5, Lcom/cronutils/TimeConstants;->SECOND:Lcom/cronutils/TimeConstants;

    aput-object v5, v0, v1

    sget-object v1, Lcom/cronutils/TimeConstants;->MINUTE:Lcom/cronutils/TimeConstants;

    aput-object v1, v0, v2

    sget-object v1, Lcom/cronutils/TimeConstants;->HOUR:Lcom/cronutils/TimeConstants;

    aput-object v1, v0, v3

    sget-object v1, Lcom/cronutils/TimeConstants;->DAY:Lcom/cronutils/TimeConstants;

    aput-object v1, v0, v4

    sput-object v0, Lcom/cronutils/TimeConstants;->$VALUES:[Lcom/cronutils/TimeConstants;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cronutils/TimeConstants;
    .locals 1

    .line 15
    const-class v0, Lcom/cronutils/TimeConstants;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/cronutils/TimeConstants;

    return-object p0
.end method

.method public static values()[Lcom/cronutils/TimeConstants;
    .locals 1

    .line 15
    sget-object v0, Lcom/cronutils/TimeConstants;->$VALUES:[Lcom/cronutils/TimeConstants;

    invoke-virtual {v0}, [Lcom/cronutils/TimeConstants;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cronutils/TimeConstants;

    return-object v0
.end method
