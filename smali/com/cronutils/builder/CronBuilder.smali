.class public Lcom/cronutils/builder/CronBuilder;
.super Ljava/lang/Object;
.source "CronBuilder.java"


# instance fields
.field private definition:Lcom/cronutils/model/definition/CronDefinition;

.field private fields:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/cronutils/model/field/CronFieldName;",
            "Lcom/cronutils/model/field/CronField;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/cronutils/model/definition/CronDefinition;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/cronutils/builder/CronBuilder;->definition:Lcom/cronutils/model/definition/CronDefinition;

    .line 37
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/builder/CronBuilder;->fields:Ljava/util/Map;

    return-void
.end method

.method public static cron(Lcom/cronutils/model/definition/CronDefinition;)Lcom/cronutils/builder/CronBuilder;
    .locals 1

    .line 41
    new-instance v0, Lcom/cronutils/builder/CronBuilder;

    invoke-direct {v0, p0}, Lcom/cronutils/builder/CronBuilder;-><init>(Lcom/cronutils/model/definition/CronDefinition;)V

    return-object v0
.end method


# virtual methods
.method addField(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/cronutils/builder/CronBuilder;->definition:Lcom/cronutils/model/definition/CronDefinition;

    invoke-virtual {v0, p1}, Lcom/cronutils/model/definition/CronDefinition;->getFieldDefinition(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/definition/FieldDefinition;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinition;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v0

    .line 79
    new-instance v1, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;

    iget-object v2, p0, Lcom/cronutils/builder/CronBuilder;->definition:Lcom/cronutils/model/definition/CronDefinition;

    invoke-virtual {v2}, Lcom/cronutils/model/definition/CronDefinition;->isStrictRanges()Z

    move-result v2

    invoke-direct {v1, v0, v2}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;-><init>(Lcom/cronutils/model/field/constraint/FieldConstraints;Z)V

    invoke-virtual {p2, v1}, Lcom/cronutils/model/field/expression/FieldExpression;->accept(Lcom/cronutils/model/field/expression/visitor/FieldExpressionVisitor;)Lcom/cronutils/model/field/expression/FieldExpression;

    .line 80
    iget-object v1, p0, Lcom/cronutils/builder/CronBuilder;->fields:Ljava/util/Map;

    new-instance v2, Lcom/cronutils/model/field/CronField;

    invoke-direct {v2, p1, p2, v0}, Lcom/cronutils/model/field/CronField;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public instance()Lcom/cronutils/model/Cron;
    .locals 3

    .line 73
    new-instance v0, Lcom/cronutils/model/Cron;

    iget-object v1, p0, Lcom/cronutils/builder/CronBuilder;->definition:Lcom/cronutils/model/definition/CronDefinition;

    iget-object v2, p0, Lcom/cronutils/builder/CronBuilder;->fields:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/cronutils/model/Cron;-><init>(Lcom/cronutils/model/definition/CronDefinition;Ljava/util/List;)V

    invoke-virtual {v0}, Lcom/cronutils/model/Cron;->validate()Lcom/cronutils/model/Cron;

    move-result-object v0

    return-object v0
.end method

.method public withDoM(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;
    .locals 1

    .line 49
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p0, v0, p1}, Lcom/cronutils/builder/CronBuilder;->addField(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    move-result-object p1

    return-object p1
.end method

.method public withDoW(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;
    .locals 1

    .line 57
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p0, v0, p1}, Lcom/cronutils/builder/CronBuilder;->addField(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    move-result-object p1

    return-object p1
.end method

.method public withHour(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;
    .locals 1

    .line 61
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->HOUR:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p0, v0, p1}, Lcom/cronutils/builder/CronBuilder;->addField(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    move-result-object p1

    return-object p1
.end method

.method public withMinute(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;
    .locals 1

    .line 65
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->MINUTE:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p0, v0, p1}, Lcom/cronutils/builder/CronBuilder;->addField(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    move-result-object p1

    return-object p1
.end method

.method public withMonth(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;
    .locals 1

    .line 53
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->MONTH:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p0, v0, p1}, Lcom/cronutils/builder/CronBuilder;->addField(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    move-result-object p1

    return-object p1
.end method

.method public withSecond(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;
    .locals 1

    .line 69
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->SECOND:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p0, v0, p1}, Lcom/cronutils/builder/CronBuilder;->addField(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    move-result-object p1

    return-object p1
.end method

.method public withYear(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;
    .locals 1

    .line 45
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->YEAR:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p0, v0, p1}, Lcom/cronutils/builder/CronBuilder;->addField(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    move-result-object p1

    return-object p1
.end method
