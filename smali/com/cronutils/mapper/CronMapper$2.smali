.class final Lcom/cronutils/mapper/CronMapper$2;
.super Ljava/lang/Object;
.source "CronMapper.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/mapper/CronMapper;->setQuestionMark()Lcom/google/common/base/Function;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function<",
        "Lcom/cronutils/model/Cron;",
        "Lcom/cronutils/model/Cron;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/cronutils/model/Cron;)Lcom/cronutils/model/Cron;
    .locals 6

    .line 131
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p1, v0}, Lcom/cronutils/model/Cron;->retrieve(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/CronField;

    move-result-object v0

    .line 132
    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p1, v1}, Lcom/cronutils/model/Cron;->retrieve(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/CronField;

    move-result-object v1

    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 134
    invoke-virtual {v0}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v2

    instance-of v2, v2, Lcom/cronutils/model/field/expression/QuestionMark;

    if-nez v2, :cond_3

    invoke-virtual {v1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v2

    instance-of v2, v2, Lcom/cronutils/model/field/expression/QuestionMark;

    if-eqz v2, :cond_0

    goto :goto_1

    .line 137
    :cond_0
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    .line 138
    invoke-virtual {p1}, Lcom/cronutils/model/Cron;->retrieveFieldsAsMap()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 139
    invoke-virtual {v0}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    instance-of v0, v0, Lcom/cronutils/model/field/expression/Always;

    if-eqz v0, :cond_1

    .line 140
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    new-instance v1, Lcom/cronutils/model/field/CronField;

    sget-object v3, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    new-instance v4, Lcom/cronutils/model/field/expression/QuestionMark;

    invoke-direct {v4}, Lcom/cronutils/model/field/expression/QuestionMark;-><init>()V

    sget-object v5, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cronutils/model/field/CronField;

    invoke-virtual {v5}, Lcom/cronutils/model/field/CronField;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v5

    invoke-direct {v1, v3, v4, v5}, Lcom/cronutils/model/field/CronField;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 142
    :cond_1
    invoke-virtual {v1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    instance-of v0, v0, Lcom/cronutils/model/field/expression/Always;

    if-eqz v0, :cond_2

    .line 143
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    new-instance v1, Lcom/cronutils/model/field/CronField;

    sget-object v3, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    new-instance v4, Lcom/cronutils/model/field/expression/QuestionMark;

    invoke-direct {v4}, Lcom/cronutils/model/field/expression/QuestionMark;-><init>()V

    sget-object v5, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cronutils/model/field/CronField;

    invoke-virtual {v5}, Lcom/cronutils/model/field/CronField;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v5

    invoke-direct {v1, v3, v4, v5}, Lcom/cronutils/model/field/CronField;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 145
    :cond_2
    invoke-virtual {p1}, Lcom/cronutils/model/Cron;->validate()Lcom/cronutils/model/Cron;

    .line 148
    :goto_0
    new-instance v0, Lcom/cronutils/model/Cron;

    invoke-virtual {p1}, Lcom/cronutils/model/Cron;->getCronDefinition()Lcom/cronutils/model/definition/CronDefinition;

    move-result-object p1

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/cronutils/model/Cron;-><init>(Lcom/cronutils/model/definition/CronDefinition;Ljava/util/List;)V

    return-object v0

    :cond_3
    :goto_1
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 128
    check-cast p1, Lcom/cronutils/model/Cron;

    invoke-virtual {p0, p1}, Lcom/cronutils/mapper/CronMapper$2;->apply(Lcom/cronutils/model/Cron;)Lcom/cronutils/model/Cron;

    move-result-object p1

    return-object p1
.end method
