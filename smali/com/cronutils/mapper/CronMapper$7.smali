.class final Lcom/cronutils/mapper/CronMapper$7;
.super Ljava/lang/Object;
.source "CronMapper.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/mapper/CronMapper;->dayOfMonthMapping(Lcom/cronutils/model/field/definition/FieldDefinition;Lcom/cronutils/model/field/definition/FieldDefinition;)Lcom/google/common/base/Function;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function<",
        "Lcom/cronutils/model/field/CronField;",
        "Lcom/cronutils/model/field/CronField;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$targetDef:Lcom/cronutils/model/field/definition/FieldDefinition;


# direct methods
.method constructor <init>(Lcom/cronutils/model/field/definition/FieldDefinition;)V
    .locals 0

    .line 297
    iput-object p1, p0, Lcom/cronutils/mapper/CronMapper$7;->val$targetDef:Lcom/cronutils/model/field/definition/FieldDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/field/CronField;
    .locals 3

    .line 300
    invoke-virtual {p1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    .line 302
    instance-of v0, p1, Lcom/cronutils/model/field/expression/QuestionMark;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/cronutils/mapper/CronMapper$7;->val$targetDef:Lcom/cronutils/model/field/definition/FieldDefinition;

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinition;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getSpecialChars()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->QUESTION_MARK:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 304
    new-instance p1, Lcom/cronutils/model/field/expression/Always;

    invoke-direct {p1}, Lcom/cronutils/model/field/expression/Always;-><init>()V

    .line 307
    :cond_0
    new-instance v0, Lcom/cronutils/model/field/CronField;

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    iget-object v2, p0, Lcom/cronutils/mapper/CronMapper$7;->val$targetDef:Lcom/cronutils/model/field/definition/FieldDefinition;

    invoke-virtual {v2}, Lcom/cronutils/model/field/definition/FieldDefinition;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2}, Lcom/cronutils/model/field/CronField;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 297
    check-cast p1, Lcom/cronutils/model/field/CronField;

    invoke-virtual {p0, p1}, Lcom/cronutils/mapper/CronMapper$7;->apply(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/field/CronField;

    move-result-object p1

    return-object p1
.end method
