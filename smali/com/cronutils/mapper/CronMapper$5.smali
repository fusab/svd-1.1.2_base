.class final Lcom/cronutils/mapper/CronMapper$5;
.super Ljava/lang/Object;
.source "CronMapper.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/mapper/CronMapper;->returnAlwaysExpression(Lcom/cronutils/model/field/CronFieldName;)Lcom/google/common/base/Function;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function<",
        "Lcom/cronutils/model/field/CronField;",
        "Lcom/cronutils/model/field/CronField;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$name:Lcom/cronutils/model/field/CronFieldName;


# direct methods
.method constructor <init>(Lcom/cronutils/model/field/CronFieldName;)V
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/cronutils/mapper/CronMapper$5;->val$name:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/field/CronField;
    .locals 4

    .line 253
    new-instance p1, Lcom/cronutils/model/field/CronField;

    iget-object v0, p0, Lcom/cronutils/mapper/CronMapper$5;->val$name:Lcom/cronutils/model/field/CronFieldName;

    new-instance v1, Lcom/cronutils/model/field/expression/Always;

    invoke-direct {v1}, Lcom/cronutils/model/field/expression/Always;-><init>()V

    invoke-static {}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->instance()Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/cronutils/mapper/CronMapper$5;->val$name:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {v2, v3}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->forField(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->createConstraintsInstance()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v2

    invoke-direct {p1, v0, v1, v2}, Lcom/cronutils/model/field/CronField;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 250
    check-cast p1, Lcom/cronutils/model/field/CronField;

    invoke-virtual {p0, p1}, Lcom/cronutils/mapper/CronMapper$5;->apply(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/field/CronField;

    move-result-object p1

    return-object p1
.end method
