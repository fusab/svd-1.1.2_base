.class public Lcom/cronutils/mapper/WeekDay;
.super Ljava/lang/Object;
.source "WeekDay.java"


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field private firstDayZero:Z

.field private mondayDoWValue:I


# direct methods
.method public constructor <init>(IZ)V
    .locals 3

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    if-ltz p1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 25
    :goto_0
    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "Monday Day of Week value must be greater or equal to zero"

    invoke-static {v1, v2, v0}, Lorg/apache/commons/lang3/Validate;->isTrue(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 26
    iput p1, p0, Lcom/cronutils/mapper/WeekDay;->mondayDoWValue:I

    .line 27
    iput-boolean p2, p0, Lcom/cronutils/mapper/WeekDay;->firstDayZero:Z

    return-void
.end method

.method private bothSameStartOfRange(IILcom/cronutils/mapper/WeekDay;Lcom/cronutils/mapper/WeekDay;)Lcom/google/common/base/Function;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/cronutils/mapper/WeekDay;",
            "Lcom/cronutils/mapper/WeekDay;",
            ")",
            "Lcom/google/common/base/Function<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 63
    new-instance v6, Lcom/cronutils/mapper/WeekDay$1;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p4

    move-object v3, p3

    move v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/cronutils/mapper/WeekDay$1;-><init>(Lcom/cronutils/mapper/WeekDay;Lcom/cronutils/mapper/WeekDay;Lcom/cronutils/mapper/WeekDay;II)V

    return-object v6
.end method


# virtual methods
.method public getMondayDoWValue()I
    .locals 1

    .line 31
    iget v0, p0, Lcom/cronutils/mapper/WeekDay;->mondayDoWValue:I

    return v0
.end method

.method public isFirstDayZero()Z
    .locals 1

    .line 35
    iget-boolean v0, p0, Lcom/cronutils/mapper/WeekDay;->firstDayZero:Z

    return v0
.end method

.method public mapTo(ILcom/cronutils/mapper/WeekDay;)I
    .locals 3

    .line 46
    iget-boolean v0, p0, Lcom/cronutils/mapper/WeekDay;->firstDayZero:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/cronutils/mapper/WeekDay;->isFirstDayZero()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    .line 47
    invoke-direct {p0, v1, v0, p0, p2}, Lcom/cronutils/mapper/WeekDay;->bothSameStartOfRange(IILcom/cronutils/mapper/WeekDay;Lcom/cronutils/mapper/WeekDay;)Lcom/google/common/base/Function;

    move-result-object p2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    .line 49
    :cond_0
    iget-boolean v0, p0, Lcom/cronutils/mapper/WeekDay;->firstDayZero:Z

    const/4 v2, 0x1

    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/cronutils/mapper/WeekDay;->isFirstDayZero()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x7

    .line 50
    invoke-direct {p0, v2, v0, p0, p2}, Lcom/cronutils/mapper/WeekDay;->bothSameStartOfRange(IILcom/cronutils/mapper/WeekDay;Lcom/cronutils/mapper/WeekDay;)Lcom/google/common/base/Function;

    move-result-object p2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    .line 53
    :cond_1
    invoke-virtual {p2}, Lcom/cronutils/mapper/WeekDay;->isFirstDayZero()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 55
    new-instance v0, Lcom/cronutils/mapper/WeekDay;

    invoke-virtual {p2}, Lcom/cronutils/mapper/WeekDay;->getMondayDoWValue()I

    move-result p2

    add-int/2addr p2, v2

    invoke-direct {v0, p2, v1}, Lcom/cronutils/mapper/WeekDay;-><init>(IZ)V

    invoke-virtual {p0, p1, v0}, Lcom/cronutils/mapper/WeekDay;->mapTo(ILcom/cronutils/mapper/WeekDay;)I

    move-result p1

    sub-int/2addr p1, v2

    return p1

    .line 58
    :cond_2
    new-instance v0, Lcom/cronutils/mapper/WeekDay;

    invoke-virtual {p2}, Lcom/cronutils/mapper/WeekDay;->getMondayDoWValue()I

    move-result p2

    sub-int/2addr p2, v2

    invoke-direct {v0, p2, v2}, Lcom/cronutils/mapper/WeekDay;-><init>(IZ)V

    invoke-virtual {p0, p1, v0}, Lcom/cronutils/mapper/WeekDay;->mapTo(ILcom/cronutils/mapper/WeekDay;)I

    move-result p1

    add-int/2addr p1, v2

    return p1
.end method
