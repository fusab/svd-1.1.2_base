.class final Lcom/cronutils/mapper/CronMapper$6;
.super Ljava/lang/Object;
.source "CronMapper.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/mapper/CronMapper;->dayOfWeekMapping(Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;)Lcom/google/common/base/Function;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function<",
        "Lcom/cronutils/model/field/CronField;",
        "Lcom/cronutils/model/field/CronField;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$sourceDef:Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;

.field final synthetic val$targetDef:Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;


# direct methods
.method constructor <init>(Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;)V
    .locals 0

    .line 261
    iput-object p1, p0, Lcom/cronutils/mapper/CronMapper$6;->val$sourceDef:Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;

    iput-object p2, p0, Lcom/cronutils/mapper/CronMapper$6;->val$targetDef:Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/field/CronField;
    .locals 3

    .line 264
    invoke-virtual {p1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    .line 266
    new-instance v0, Lcom/cronutils/model/field/expression/visitor/ValueMappingFieldExpressionVisitor;

    new-instance v1, Lcom/cronutils/mapper/CronMapper$6$1;

    invoke-direct {v1, p0}, Lcom/cronutils/mapper/CronMapper$6$1;-><init>(Lcom/cronutils/mapper/CronMapper$6;)V

    invoke-direct {v0, v1}, Lcom/cronutils/model/field/expression/visitor/ValueMappingFieldExpressionVisitor;-><init>(Lcom/google/common/base/Function;)V

    invoke-virtual {p1, v0}, Lcom/cronutils/model/field/expression/FieldExpression;->accept(Lcom/cronutils/model/field/expression/visitor/FieldExpressionVisitor;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    .line 285
    instance-of p1, p1, Lcom/cronutils/model/field/expression/QuestionMark;

    if-eqz p1, :cond_0

    .line 286
    iget-object p1, p0, Lcom/cronutils/mapper/CronMapper$6;->val$targetDef:Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;

    invoke-virtual {p1}, Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getSpecialChars()Ljava/util/Set;

    move-result-object p1

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->QUESTION_MARK:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-interface {p1, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 287
    new-instance v0, Lcom/cronutils/model/field/expression/Always;

    invoke-direct {v0}, Lcom/cronutils/model/field/expression/Always;-><init>()V

    .line 290
    :cond_0
    new-instance p1, Lcom/cronutils/model/field/CronField;

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    iget-object v2, p0, Lcom/cronutils/mapper/CronMapper$6;->val$targetDef:Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;

    invoke-virtual {v2}, Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v2

    invoke-direct {p1, v1, v0, v2}, Lcom/cronutils/model/field/CronField;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 261
    check-cast p1, Lcom/cronutils/model/field/CronField;

    invoke-virtual {p0, p1}, Lcom/cronutils/mapper/CronMapper$6;->apply(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/field/CronField;

    move-result-object p1

    return-object p1
.end method
