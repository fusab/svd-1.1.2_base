.class public Lcom/cronutils/mapper/ConstantsMapper;
.super Ljava/lang/Object;
.source "ConstantsMapper.java"


# static fields
.field public static final CRONTAB_WEEK_DAY:Lcom/cronutils/mapper/WeekDay;

.field public static final JODATIME_WEEK_DAY:Lcom/cronutils/mapper/WeekDay;

.field public static final QUARTZ_WEEK_DAY:Lcom/cronutils/mapper/WeekDay;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 17
    new-instance v0, Lcom/cronutils/mapper/WeekDay;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {v0, v2, v1}, Lcom/cronutils/mapper/WeekDay;-><init>(IZ)V

    sput-object v0, Lcom/cronutils/mapper/ConstantsMapper;->QUARTZ_WEEK_DAY:Lcom/cronutils/mapper/WeekDay;

    .line 18
    new-instance v0, Lcom/cronutils/mapper/WeekDay;

    const/4 v2, 0x1

    invoke-direct {v0, v2, v1}, Lcom/cronutils/mapper/WeekDay;-><init>(IZ)V

    sput-object v0, Lcom/cronutils/mapper/ConstantsMapper;->JODATIME_WEEK_DAY:Lcom/cronutils/mapper/WeekDay;

    .line 19
    new-instance v0, Lcom/cronutils/mapper/WeekDay;

    invoke-direct {v0, v2, v2}, Lcom/cronutils/mapper/WeekDay;-><init>(IZ)V

    sput-object v0, Lcom/cronutils/mapper/ConstantsMapper;->CRONTAB_WEEK_DAY:Lcom/cronutils/mapper/WeekDay;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static weekDayMapping(Lcom/cronutils/mapper/WeekDay;Lcom/cronutils/mapper/WeekDay;I)I
    .locals 0

    .line 29
    invoke-virtual {p0, p2, p1}, Lcom/cronutils/mapper/WeekDay;->mapTo(ILcom/cronutils/mapper/WeekDay;)I

    move-result p0

    return p0
.end method
