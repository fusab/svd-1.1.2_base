.class final Lcom/cronutils/mapper/CronMapper$4;
.super Ljava/lang/Object;
.source "CronMapper.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/mapper/CronMapper;->returnOnZeroExpression(Lcom/cronutils/model/field/CronFieldName;)Lcom/google/common/base/Function;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function<",
        "Lcom/cronutils/model/field/CronField;",
        "Lcom/cronutils/model/field/CronField;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$name:Lcom/cronutils/model/field/CronFieldName;


# direct methods
.method constructor <init>(Lcom/cronutils/model/field/CronFieldName;)V
    .locals 0

    .line 234
    iput-object p1, p0, Lcom/cronutils/mapper/CronMapper$4;->val$name:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/field/CronField;
    .locals 5

    .line 237
    invoke-static {}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->instance()Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    move-result-object p1

    iget-object v0, p0, Lcom/cronutils/mapper/CronMapper$4;->val$name:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p1, v0}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->forField(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->createConstraintsInstance()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object p1

    .line 238
    new-instance v0, Lcom/cronutils/model/field/CronField;

    iget-object v1, p0, Lcom/cronutils/mapper/CronMapper$4;->val$name:Lcom/cronutils/model/field/CronFieldName;

    new-instance v2, Lcom/cronutils/model/field/expression/On;

    new-instance v3, Lcom/cronutils/model/field/value/IntegerFieldValue;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {v2, v3}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    invoke-direct {v0, v1, v2, p1}, Lcom/cronutils/model/field/CronField;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 234
    check-cast p1, Lcom/cronutils/model/field/CronField;

    invoke-virtual {p0, p1}, Lcom/cronutils/mapper/CronMapper$4;->apply(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/field/CronField;

    move-result-object p1

    return-object p1
.end method
