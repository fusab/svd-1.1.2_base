.class public Lcom/cronutils/mapper/CronMapper;
.super Ljava/lang/Object;
.source "CronMapper.java"


# instance fields
.field private cronRules:Lcom/google/common/base/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Function<",
            "Lcom/cronutils/model/Cron;",
            "Lcom/cronutils/model/Cron;",
            ">;"
        }
    .end annotation
.end field

.field private mappings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/cronutils/model/field/CronFieldName;",
            "Lcom/google/common/base/Function<",
            "Lcom/cronutils/model/field/CronField;",
            "Lcom/cronutils/model/field/CronField;",
            ">;>;"
        }
    .end annotation
.end field

.field private to:Lcom/cronutils/model/definition/CronDefinition;


# direct methods
.method public constructor <init>(Lcom/cronutils/model/definition/CronDefinition;Lcom/cronutils/model/definition/CronDefinition;Lcom/google/common/base/Function;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cronutils/model/definition/CronDefinition;",
            "Lcom/cronutils/model/definition/CronDefinition;",
            "Lcom/google/common/base/Function<",
            "Lcom/cronutils/model/Cron;",
            "Lcom/cronutils/model/Cron;",
            ">;)V"
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 54
    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "Source CronDefinition must not be null"

    invoke-static {p1, v2, v1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "Destination CronDefinition must not be null"

    invoke-static {p2, v2, v1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cronutils/model/definition/CronDefinition;

    iput-object v1, p0, Lcom/cronutils/mapper/CronMapper;->to:Lcom/cronutils/model/definition/CronDefinition;

    .line 56
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CronRules must not be null"

    invoke-static {p3, v1, v0}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/google/common/base/Function;

    iput-object p3, p0, Lcom/cronutils/mapper/CronMapper;->cronRules:Lcom/google/common/base/Function;

    .line 57
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object p3

    iput-object p3, p0, Lcom/cronutils/mapper/CronMapper;->mappings:Ljava/util/Map;

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/cronutils/mapper/CronMapper;->buildMappings(Lcom/cronutils/model/definition/CronDefinition;Lcom/cronutils/model/definition/CronDefinition;)V

    return-void
.end method

.method private buildMappings(Lcom/cronutils/model/definition/CronDefinition;Lcom/cronutils/model/definition/CronDefinition;)V
    .locals 9

    .line 166
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    .line 167
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    .line 168
    invoke-virtual {p1}, Lcom/cronutils/model/definition/CronDefinition;->getFieldDefinitions()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cronutils/model/field/definition/FieldDefinition;

    .line 169
    invoke-virtual {v2}, Lcom/cronutils/model/field/definition/FieldDefinition;->getFieldName()Lcom/cronutils/model/field/CronFieldName;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 171
    :cond_0
    invoke-virtual {p2}, Lcom/cronutils/model/definition/CronDefinition;->getFieldDefinitions()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/cronutils/model/field/definition/FieldDefinition;

    .line 172
    invoke-virtual {p2}, Lcom/cronutils/model/field/definition/FieldDefinition;->getFieldName()Lcom/cronutils/model/field/CronFieldName;

    move-result-object v2

    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 176
    :cond_1
    invoke-static {}, Lcom/cronutils/model/field/CronFieldName;->values()[Lcom/cronutils/model/field/CronFieldName;

    move-result-object p1

    array-length p2, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_2
    if-ge v2, p2, :cond_a

    aget-object v5, p1, v2

    .line 177
    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    const/4 v7, 0x1

    if-eqz v6, :cond_2

    const/4 v3, 0x1

    .line 180
    :cond_2
    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_3

    const/4 v4, 0x1

    :cond_3
    if-eqz v3, :cond_4

    .line 183
    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_4

    goto/16 :goto_4

    :cond_4
    if-nez v4, :cond_5

    .line 187
    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_5

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 188
    iget-object v6, p0, Lcom/cronutils/mapper/CronMapper;->mappings:Ljava/util/Map;

    invoke-static {v5}, Lcom/cronutils/mapper/CronMapper;->returnOnZeroExpression(Lcom/cronutils/model/field/CronFieldName;)Lcom/google/common/base/Function;

    move-result-object v7

    invoke-interface {v6, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    if-eqz v4, :cond_6

    .line 191
    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_6

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 192
    iget-object v6, p0, Lcom/cronutils/mapper/CronMapper;->mappings:Ljava/util/Map;

    invoke-static {v5}, Lcom/cronutils/mapper/CronMapper;->returnAlwaysExpression(Lcom/cronutils/model/field/CronFieldName;)Lcom/google/common/base/Function;

    move-result-object v7

    invoke-interface {v6, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    :cond_6
    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_9

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 195
    sget-object v6, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {v6, v5}, Lcom/cronutils/model/field/CronFieldName;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 196
    iget-object v6, p0, Lcom/cronutils/mapper/CronMapper;->mappings:Ljava/util/Map;

    .line 198
    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;

    .line 199
    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;

    .line 197
    invoke-static {v7, v8}, Lcom/cronutils/mapper/CronMapper;->dayOfWeekMapping(Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;)Lcom/google/common/base/Function;

    move-result-object v7

    .line 196
    invoke-interface {v6, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 203
    :cond_7
    sget-object v6, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {v6, v5}, Lcom/cronutils/model/field/CronFieldName;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 204
    iget-object v6, p0, Lcom/cronutils/mapper/CronMapper;->mappings:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cronutils/model/field/definition/FieldDefinition;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/cronutils/model/field/definition/FieldDefinition;

    invoke-static {v7, v8}, Lcom/cronutils/mapper/CronMapper;->dayOfMonthMapping(Lcom/cronutils/model/field/definition/FieldDefinition;Lcom/cronutils/model/field/definition/FieldDefinition;)Lcom/google/common/base/Function;

    move-result-object v7

    invoke-interface {v6, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 206
    :cond_8
    iget-object v6, p0, Lcom/cronutils/mapper/CronMapper;->mappings:Ljava/util/Map;

    invoke-static {}, Lcom/cronutils/mapper/CronMapper;->returnSameExpression()Lcom/google/common/base/Function;

    move-result-object v7

    invoke-interface {v6, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    :cond_a
    :goto_4
    return-void
.end method

.method static dayOfMonthMapping(Lcom/cronutils/model/field/definition/FieldDefinition;Lcom/cronutils/model/field/definition/FieldDefinition;)Lcom/google/common/base/Function;
    .locals 0
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cronutils/model/field/definition/FieldDefinition;",
            "Lcom/cronutils/model/field/definition/FieldDefinition;",
            ")",
            "Lcom/google/common/base/Function<",
            "Lcom/cronutils/model/field/CronField;",
            "Lcom/cronutils/model/field/CronField;",
            ">;"
        }
    .end annotation

    .line 297
    new-instance p0, Lcom/cronutils/mapper/CronMapper$7;

    invoke-direct {p0, p1}, Lcom/cronutils/mapper/CronMapper$7;-><init>(Lcom/cronutils/model/field/definition/FieldDefinition;)V

    return-object p0
.end method

.method static dayOfWeekMapping(Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;)Lcom/google/common/base/Function;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;",
            "Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;",
            ")",
            "Lcom/google/common/base/Function<",
            "Lcom/cronutils/model/field/CronField;",
            "Lcom/cronutils/model/field/CronField;",
            ">;"
        }
    .end annotation

    .line 261
    new-instance v0, Lcom/cronutils/mapper/CronMapper$6;

    invoke-direct {v0, p0, p1}, Lcom/cronutils/mapper/CronMapper$6;-><init>(Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;)V

    return-object v0
.end method

.method public static fromCron4jToQuartz()Lcom/cronutils/mapper/CronMapper;
    .locals 4

    .line 80
    new-instance v0, Lcom/cronutils/mapper/CronMapper;

    sget-object v1, Lcom/cronutils/model/CronType;->CRON4J:Lcom/cronutils/model/CronType;

    .line 81
    invoke-static {v1}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->instanceDefinitionFor(Lcom/cronutils/model/CronType;)Lcom/cronutils/model/definition/CronDefinition;

    move-result-object v1

    sget-object v2, Lcom/cronutils/model/CronType;->QUARTZ:Lcom/cronutils/model/CronType;

    .line 82
    invoke-static {v2}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->instanceDefinitionFor(Lcom/cronutils/model/CronType;)Lcom/cronutils/model/definition/CronDefinition;

    move-result-object v2

    .line 83
    invoke-static {}, Lcom/cronutils/mapper/CronMapper;->setQuestionMark()Lcom/google/common/base/Function;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/cronutils/mapper/CronMapper;-><init>(Lcom/cronutils/model/definition/CronDefinition;Lcom/cronutils/model/definition/CronDefinition;Lcom/google/common/base/Function;)V

    return-object v0
.end method

.method public static fromQuartzToCron4j()Lcom/cronutils/mapper/CronMapper;
    .locals 4

    .line 88
    new-instance v0, Lcom/cronutils/mapper/CronMapper;

    sget-object v1, Lcom/cronutils/model/CronType;->QUARTZ:Lcom/cronutils/model/CronType;

    .line 89
    invoke-static {v1}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->instanceDefinitionFor(Lcom/cronutils/model/CronType;)Lcom/cronutils/model/definition/CronDefinition;

    move-result-object v1

    sget-object v2, Lcom/cronutils/model/CronType;->CRON4J:Lcom/cronutils/model/CronType;

    .line 90
    invoke-static {v2}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->instanceDefinitionFor(Lcom/cronutils/model/CronType;)Lcom/cronutils/model/definition/CronDefinition;

    move-result-object v2

    .line 91
    invoke-static {}, Lcom/cronutils/mapper/CronMapper;->sameCron()Lcom/google/common/base/Function;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/cronutils/mapper/CronMapper;-><init>(Lcom/cronutils/model/definition/CronDefinition;Lcom/cronutils/model/definition/CronDefinition;Lcom/google/common/base/Function;)V

    return-object v0
.end method

.method public static fromQuartzToUnix()Lcom/cronutils/mapper/CronMapper;
    .locals 4

    .line 96
    new-instance v0, Lcom/cronutils/mapper/CronMapper;

    sget-object v1, Lcom/cronutils/model/CronType;->QUARTZ:Lcom/cronutils/model/CronType;

    .line 97
    invoke-static {v1}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->instanceDefinitionFor(Lcom/cronutils/model/CronType;)Lcom/cronutils/model/definition/CronDefinition;

    move-result-object v1

    sget-object v2, Lcom/cronutils/model/CronType;->UNIX:Lcom/cronutils/model/CronType;

    .line 98
    invoke-static {v2}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->instanceDefinitionFor(Lcom/cronutils/model/CronType;)Lcom/cronutils/model/definition/CronDefinition;

    move-result-object v2

    .line 99
    invoke-static {}, Lcom/cronutils/mapper/CronMapper;->sameCron()Lcom/google/common/base/Function;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/cronutils/mapper/CronMapper;-><init>(Lcom/cronutils/model/definition/CronDefinition;Lcom/cronutils/model/definition/CronDefinition;Lcom/google/common/base/Function;)V

    return-object v0
.end method

.method public static fromUnixToQuartz()Lcom/cronutils/mapper/CronMapper;
    .locals 4

    .line 104
    new-instance v0, Lcom/cronutils/mapper/CronMapper;

    sget-object v1, Lcom/cronutils/model/CronType;->UNIX:Lcom/cronutils/model/CronType;

    .line 105
    invoke-static {v1}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->instanceDefinitionFor(Lcom/cronutils/model/CronType;)Lcom/cronutils/model/definition/CronDefinition;

    move-result-object v1

    sget-object v2, Lcom/cronutils/model/CronType;->QUARTZ:Lcom/cronutils/model/CronType;

    .line 106
    invoke-static {v2}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->instanceDefinitionFor(Lcom/cronutils/model/CronType;)Lcom/cronutils/model/definition/CronDefinition;

    move-result-object v2

    .line 107
    invoke-static {}, Lcom/cronutils/mapper/CronMapper;->setQuestionMark()Lcom/google/common/base/Function;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/cronutils/mapper/CronMapper;-><init>(Lcom/cronutils/model/definition/CronDefinition;Lcom/cronutils/model/definition/CronDefinition;Lcom/google/common/base/Function;)V

    return-object v0
.end method

.method static returnAlwaysExpression(Lcom/cronutils/model/field/CronFieldName;)Lcom/google/common/base/Function;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cronutils/model/field/CronFieldName;",
            ")",
            "Lcom/google/common/base/Function<",
            "Lcom/cronutils/model/field/CronField;",
            "Lcom/cronutils/model/field/CronField;",
            ">;"
        }
    .end annotation

    .line 250
    new-instance v0, Lcom/cronutils/mapper/CronMapper$5;

    invoke-direct {v0, p0}, Lcom/cronutils/mapper/CronMapper$5;-><init>(Lcom/cronutils/model/field/CronFieldName;)V

    return-object v0
.end method

.method static returnOnZeroExpression(Lcom/cronutils/model/field/CronFieldName;)Lcom/google/common/base/Function;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cronutils/model/field/CronFieldName;",
            ")",
            "Lcom/google/common/base/Function<",
            "Lcom/cronutils/model/field/CronField;",
            "Lcom/cronutils/model/field/CronField;",
            ">;"
        }
    .end annotation

    .line 234
    new-instance v0, Lcom/cronutils/mapper/CronMapper$4;

    invoke-direct {v0, p0}, Lcom/cronutils/mapper/CronMapper$4;-><init>(Lcom/cronutils/model/field/CronFieldName;)V

    return-object v0
.end method

.method static returnSameExpression()Lcom/google/common/base/Function;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Function<",
            "Lcom/cronutils/model/field/CronField;",
            "Lcom/cronutils/model/field/CronField;",
            ">;"
        }
    .end annotation

    .line 219
    new-instance v0, Lcom/cronutils/mapper/CronMapper$3;

    invoke-direct {v0}, Lcom/cronutils/mapper/CronMapper$3;-><init>()V

    return-object v0
.end method

.method public static sameCron(Lcom/cronutils/model/definition/CronDefinition;)Lcom/cronutils/mapper/CronMapper;
    .locals 2

    .line 112
    new-instance v0, Lcom/cronutils/mapper/CronMapper;

    invoke-static {}, Lcom/cronutils/mapper/CronMapper;->sameCron()Lcom/google/common/base/Function;

    move-result-object v1

    invoke-direct {v0, p0, p0, v1}, Lcom/cronutils/mapper/CronMapper;-><init>(Lcom/cronutils/model/definition/CronDefinition;Lcom/cronutils/model/definition/CronDefinition;Lcom/google/common/base/Function;)V

    return-object v0
.end method

.method private static sameCron()Lcom/google/common/base/Function;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Function<",
            "Lcom/cronutils/model/Cron;",
            "Lcom/cronutils/model/Cron;",
            ">;"
        }
    .end annotation

    .line 119
    new-instance v0, Lcom/cronutils/mapper/CronMapper$1;

    invoke-direct {v0}, Lcom/cronutils/mapper/CronMapper$1;-><init>()V

    return-object v0
.end method

.method private static setQuestionMark()Lcom/google/common/base/Function;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Function<",
            "Lcom/cronutils/model/Cron;",
            "Lcom/cronutils/model/Cron;",
            ">;"
        }
    .end annotation

    .line 128
    new-instance v0, Lcom/cronutils/mapper/CronMapper$2;

    invoke-direct {v0}, Lcom/cronutils/mapper/CronMapper$2;-><init>()V

    return-object v0
.end method


# virtual methods
.method public map(Lcom/cronutils/model/Cron;)Lcom/cronutils/model/Cron;
    .locals 6

    const/4 v0, 0x0

    .line 68
    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "Cron must not be null"

    invoke-static {p1, v2, v1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 70
    invoke-static {}, Lcom/cronutils/model/field/CronFieldName;->values()[Lcom/cronutils/model/field/CronFieldName;

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 71
    iget-object v5, p0, Lcom/cronutils/mapper/CronMapper;->mappings:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 72
    iget-object v5, p0, Lcom/cronutils/mapper/CronMapper;->mappings:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/common/base/Function;

    invoke-virtual {p1, v4}, Lcom/cronutils/model/Cron;->retrieve(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/CronField;

    move-result-object v4

    invoke-interface {v5, v4}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 75
    :cond_1
    iget-object p1, p0, Lcom/cronutils/mapper/CronMapper;->cronRules:Lcom/google/common/base/Function;

    new-instance v0, Lcom/cronutils/model/Cron;

    iget-object v2, p0, Lcom/cronutils/mapper/CronMapper;->to:Lcom/cronutils/model/definition/CronDefinition;

    invoke-direct {v0, v2, v1}, Lcom/cronutils/model/Cron;-><init>(Lcom/cronutils/model/definition/CronDefinition;Ljava/util/List;)V

    invoke-interface {p1, v0}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/Cron;

    invoke-virtual {p1}, Lcom/cronutils/model/Cron;->validate()Lcom/cronutils/model/Cron;

    move-result-object p1

    return-object p1
.end method
