.class Lcom/cronutils/mapper/CronMapper$6$1;
.super Ljava/lang/Object;
.source "CronMapper.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/mapper/CronMapper$6;->apply(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/field/CronField;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function<",
        "Lcom/cronutils/model/field/value/FieldValue;",
        "Lcom/cronutils/model/field/value/FieldValue;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cronutils/mapper/CronMapper$6;


# direct methods
.method constructor <init>(Lcom/cronutils/mapper/CronMapper$6;)V
    .locals 0

    .line 268
    iput-object p1, p0, Lcom/cronutils/mapper/CronMapper$6$1;->this$0:Lcom/cronutils/mapper/CronMapper$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/cronutils/model/field/value/FieldValue;)Lcom/cronutils/model/field/value/FieldValue;
    .locals 3

    .line 271
    instance-of v0, p1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    if-eqz v0, :cond_0

    .line 272
    new-instance v0, Lcom/cronutils/model/field/value/IntegerFieldValue;

    iget-object v1, p0, Lcom/cronutils/mapper/CronMapper$6$1;->this$0:Lcom/cronutils/mapper/CronMapper$6;

    iget-object v1, v1, Lcom/cronutils/mapper/CronMapper$6;->val$sourceDef:Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;

    .line 274
    invoke-virtual {v1}, Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;->getMondayDoWValue()Lcom/cronutils/mapper/WeekDay;

    move-result-object v1

    iget-object v2, p0, Lcom/cronutils/mapper/CronMapper$6$1;->this$0:Lcom/cronutils/mapper/CronMapper$6;

    iget-object v2, v2, Lcom/cronutils/mapper/CronMapper$6;->val$targetDef:Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;

    .line 275
    invoke-virtual {v2}, Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;->getMondayDoWValue()Lcom/cronutils/mapper/WeekDay;

    move-result-object v2

    check-cast p1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    .line 276
    invoke-virtual {p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 273
    invoke-static {v1, v2, p1}, Lcom/cronutils/mapper/ConstantsMapper;->weekDayMapping(Lcom/cronutils/mapper/WeekDay;Lcom/cronutils/mapper/WeekDay;I)I

    move-result p1

    invoke-direct {v0, p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    return-object v0

    :cond_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 268
    check-cast p1, Lcom/cronutils/model/field/value/FieldValue;

    invoke-virtual {p0, p1}, Lcom/cronutils/mapper/CronMapper$6$1;->apply(Lcom/cronutils/model/field/value/FieldValue;)Lcom/cronutils/model/field/value/FieldValue;

    move-result-object p1

    return-object p1
.end method
