.class public Lcom/cronutils/htime/jodatime/JodaTimeHDateTimeFormat;
.super Ljava/lang/Object;
.source "JodaTimeHDateTimeFormat.java"

# interfaces
.implements Lcom/cronutils/htime/HDateTimeFormat;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/cronutils/htime/HDateTimeFormat<",
        "Lorg/joda/time/format/DateTimeFormatter;",
        ">;"
    }
.end annotation


# instance fields
.field private constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

.field private locale:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Ljava/util/Locale;)V
    .locals 2

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 17
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Locale should not be null"

    invoke-static {p1, v1, v0}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    iput-object p1, p0, Lcom/cronutils/htime/jodatime/JodaTimeHDateTimeFormat;->locale:Ljava/util/Locale;

    .line 19
    new-instance p1, Lcom/cronutils/htime/jodatime/JodaTimeDatetimeFormatterConstants;

    invoke-direct {p1}, Lcom/cronutils/htime/jodatime/JodaTimeDatetimeFormatterConstants;-><init>()V

    iput-object p1, p0, Lcom/cronutils/htime/jodatime/JodaTimeHDateTimeFormat;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    return-void
.end method


# virtual methods
.method public bridge synthetic forPattern(Ljava/lang/String;)Ljava/lang/Object;
    .locals 0

    .line 12
    invoke-virtual {p0, p1}, Lcom/cronutils/htime/jodatime/JodaTimeHDateTimeFormat;->forPattern(Ljava/lang/String;)Lorg/joda/time/format/DateTimeFormatter;

    move-result-object p1

    return-object p1
.end method

.method public forPattern(Ljava/lang/String;)Lorg/joda/time/format/DateTimeFormatter;
    .locals 7

    const/4 v0, 0x0

    .line 23
    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "Expression must not be blank or null"

    invoke-static {p1, v2, v1}, Lorg/apache/commons/lang3/Validate;->notBlank(Ljava/lang/CharSequence;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/CharSequence;

    .line 24
    new-instance v1, Lcom/cronutils/htime/DateTimeFormatParser;

    iget-object v2, p0, Lcom/cronutils/htime/jodatime/JodaTimeHDateTimeFormat;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    iget-object v3, p0, Lcom/cronutils/htime/jodatime/JodaTimeHDateTimeFormat;->locale:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Lcom/cronutils/htime/DateTimeFormatParser;-><init>(Lcom/cronutils/htime/DatetimeFormatterConstants;Ljava/util/Locale;)V

    const-string v2, " "

    const-string v3, "\\s+"

    .line 25
    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v3, " AM"

    const-string v4, "AM"

    .line 26
    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    const-string v3, " am"

    const-string v4, "am"

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    const-string v3, " PM"

    const-string v4, "PM"

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    const-string v3, " pm"

    const-string v4, "pm"

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 27
    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 28
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    array-length v3, p1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    aget-object v5, p1, v4

    const/4 v6, 0x1

    .line 30
    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v5}, Lcom/cronutils/htime/DateTimeFormatParser;->parsePattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v6, v0

    const-string v5, "%s "

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 32
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lorg/joda/time/format/DateTimeFormat;->forPattern(Ljava/lang/String;)Lorg/joda/time/format/DateTimeFormatter;

    move-result-object p1

    return-object p1
.end method
