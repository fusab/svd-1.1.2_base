.class public Lcom/cronutils/htime/HDateTimeFormatBuilder$JodaTimeHDateTimeFormatBuilder;
.super Ljava/lang/Object;
.source "HDateTimeFormatBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cronutils/htime/HDateTimeFormatBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "JodaTimeHDateTimeFormatBuilder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFormatter()Lcom/cronutils/htime/HDateTimeFormat;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/cronutils/htime/HDateTimeFormat<",
            "Lorg/joda/time/format/DateTimeFormatter;",
            ">;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/cronutils/htime/jodatime/JodaTimeHDateTimeFormat;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1}, Lcom/cronutils/htime/jodatime/JodaTimeHDateTimeFormat;-><init>(Ljava/util/Locale;)V

    return-object v0
.end method

.method public getFormatter(Ljava/util/Locale;)Lcom/cronutils/htime/HDateTimeFormat;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Locale;",
            ")",
            "Lcom/cronutils/htime/HDateTimeFormat<",
            "Lorg/joda/time/format/DateTimeFormatter;",
            ">;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/cronutils/htime/jodatime/JodaTimeHDateTimeFormat;

    invoke-direct {v0, p1}, Lcom/cronutils/htime/jodatime/JodaTimeHDateTimeFormat;-><init>(Ljava/util/Locale;)V

    return-object v0
.end method
