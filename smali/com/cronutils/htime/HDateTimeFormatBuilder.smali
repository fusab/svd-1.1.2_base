.class public Lcom/cronutils/htime/HDateTimeFormatBuilder;
.super Ljava/lang/Object;
.source "HDateTimeFormatBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cronutils/htime/HDateTimeFormatBuilder$JDK12TimeHDateTimeFormatBuilder;,
        Lcom/cronutils/htime/HDateTimeFormatBuilder$JodaTimeHDateTimeFormatBuilder;
    }
.end annotation


# static fields
.field private static final instance:Lcom/cronutils/htime/HDateTimeFormatBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/cronutils/htime/HDateTimeFormatBuilder;

    invoke-direct {v0}, Lcom/cronutils/htime/HDateTimeFormatBuilder;-><init>()V

    sput-object v0, Lcom/cronutils/htime/HDateTimeFormatBuilder;->instance:Lcom/cronutils/htime/HDateTimeFormatBuilder;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/cronutils/htime/HDateTimeFormatBuilder;
    .locals 1

    .line 29
    sget-object v0, Lcom/cronutils/htime/HDateTimeFormatBuilder;->instance:Lcom/cronutils/htime/HDateTimeFormatBuilder;

    return-object v0
.end method


# virtual methods
.method public forJDK12()Lcom/cronutils/htime/HDateTimeFormatBuilder$JDK12TimeHDateTimeFormatBuilder;
    .locals 1

    .line 37
    new-instance v0, Lcom/cronutils/htime/HDateTimeFormatBuilder$JDK12TimeHDateTimeFormatBuilder;

    invoke-direct {v0}, Lcom/cronutils/htime/HDateTimeFormatBuilder$JDK12TimeHDateTimeFormatBuilder;-><init>()V

    return-object v0
.end method

.method public forJodaTime()Lcom/cronutils/htime/HDateTimeFormatBuilder$JodaTimeHDateTimeFormatBuilder;
    .locals 1

    .line 33
    new-instance v0, Lcom/cronutils/htime/HDateTimeFormatBuilder$JodaTimeHDateTimeFormatBuilder;

    invoke-direct {v0}, Lcom/cronutils/htime/HDateTimeFormatBuilder$JodaTimeHDateTimeFormatBuilder;-><init>()V

    return-object v0
.end method
