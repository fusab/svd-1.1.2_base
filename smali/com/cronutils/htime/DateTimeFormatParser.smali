.class public Lcom/cronutils/htime/DateTimeFormatParser;
.super Ljava/lang/Object;
.source "DateTimeFormatParser.java"


# instance fields
.field private constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

.field private dateFormatByCountry:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private dayOfWeek:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private locale:Ljava/util/Locale;

.field private months:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/cronutils/htime/DatetimeFormatterConstants;Ljava/util/Locale;)V
    .locals 3

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 32
    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "ConstantsConfiguration must not be null"

    invoke-static {p1, v2, v1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/htime/DatetimeFormatterConstants;

    iput-object p1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    .line 33
    new-array p1, v0, [Ljava/lang/Object;

    const-string v0, "Locale must not be null"

    invoke-static {p2, v0, p1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Locale;

    iput-object p1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->locale:Ljava/util/Locale;

    .line 34
    invoke-virtual {p0, p2}, Lcom/cronutils/htime/DateTimeFormatParser;->initDaysOfWeek(Ljava/util/Locale;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->dayOfWeek:Ljava/util/Map;

    .line 35
    invoke-virtual {p0, p2}, Lcom/cronutils/htime/DateTimeFormatParser;->initMonths(Ljava/util/Locale;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->months:Ljava/util/Map;

    .line 36
    invoke-virtual {p0}, Lcom/cronutils/htime/DateTimeFormatParser;->initDateFormatByCountry()Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->dateFormatByCountry:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method initDateFormatByCountry()Ljava/util/Map;
    .locals 10
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x6

    .line 174
    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    .line 175
    invoke-interface {v2}, Lcom/cronutils/htime/DatetimeFormatterConstants;->monthOfYear()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v2}, Lcom/cronutils/htime/DatetimeFormatterConstants;->monthOfYear()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    .line 176
    invoke-interface {v2}, Lcom/cronutils/htime/DatetimeFormatterConstants;->dayOfMonth()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x2

    aput-object v2, v1, v5

    iget-object v2, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v2}, Lcom/cronutils/htime/DatetimeFormatterConstants;->dayOfMonth()Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x3

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    .line 177
    invoke-interface {v2}, Lcom/cronutils/htime/DatetimeFormatterConstants;->year()Ljava/lang/String;

    move-result-object v2

    const/4 v7, 0x4

    aput-object v2, v1, v7

    iget-object v2, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v2}, Lcom/cronutils/htime/DatetimeFormatterConstants;->year()Ljava/lang/String;

    move-result-object v2

    const/4 v8, 0x5

    aput-object v2, v1, v8

    const-string v2, "%s%s/%s%s/%s%s"

    .line 174
    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 179
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    .line 180
    invoke-interface {v9}, Lcom/cronutils/htime/DatetimeFormatterConstants;->year()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v0, v3

    iget-object v3, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v3}, Lcom/cronutils/htime/DatetimeFormatterConstants;->year()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v4

    iget-object v3, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    .line 181
    invoke-interface {v3}, Lcom/cronutils/htime/DatetimeFormatterConstants;->monthOfYear()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v5

    iget-object v3, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v3}, Lcom/cronutils/htime/DatetimeFormatterConstants;->monthOfYear()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v6

    iget-object v3, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    .line 182
    invoke-interface {v3}, Lcom/cronutils/htime/DatetimeFormatterConstants;->dayOfMonth()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v7

    iget-object v3, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v3}, Lcom/cronutils/htime/DatetimeFormatterConstants;->dayOfMonth()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v8

    .line 179
    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 184
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    const-string v3, "US"

    .line 185
    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "BZ"

    .line 186
    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "CN"

    .line 188
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "KR"

    .line 189
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "KP"

    .line 190
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "TW"

    .line 191
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "HU"

    .line 192
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "IR"

    .line 193
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "JP"

    .line 194
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "LT"

    .line 195
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "MN"

    .line 196
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v2
.end method

.method initDaysOfWeek(Ljava/util/Locale;)Ljava/util/Map;
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Locale;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x4

    .line 133
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    .line 135
    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->dayOfWeekName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->dayOfWeekName()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    .line 136
    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->dayOfWeekName()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x2

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->dayOfWeekName()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x3

    aput-object v1, v0, v4

    const-string v1, "%s%s%s%s"

    .line 134
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 138
    new-instance v1, Lorg/joda/time/LocalDate;

    invoke-direct {v1}, Lorg/joda/time/LocalDate;-><init>()V

    .line 139
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v5

    :goto_0
    const/16 v6, 0x8

    if-ge v3, v6, :cond_0

    .line 141
    invoke-virtual {v1, v3}, Lorg/joda/time/LocalDate;->withDayOfWeek(I)Lorg/joda/time/LocalDate;

    move-result-object v6

    invoke-virtual {v6}, Lorg/joda/time/LocalDate;->dayOfWeek()Lorg/joda/time/LocalDate$Property;

    move-result-object v6

    invoke-virtual {v6, p1}, Lorg/joda/time/LocalDate$Property;->getAsText(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    .line 142
    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    invoke-virtual {v6, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v7}, Lcom/cronutils/htime/DatetimeFormatterConstants;->dayOfWeekName()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v5
.end method

.method initMonths(Ljava/util/Locale;)Ljava/util/Map;
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Locale;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x4

    .line 150
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    .line 152
    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->monthOfYear()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->monthOfYear()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    .line 153
    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->monthOfYear()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x2

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->monthOfYear()Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x3

    aput-object v1, v0, v5

    const-string v1, "%s%s%s%s"

    .line 151
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 155
    new-array v1, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v6}, Lcom/cronutils/htime/DatetimeFormatterConstants;->monthOfYear()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v2

    iget-object v6, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    .line 156
    invoke-interface {v6}, Lcom/cronutils/htime/DatetimeFormatterConstants;->monthOfYear()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v3

    iget-object v6, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v6}, Lcom/cronutils/htime/DatetimeFormatterConstants;->monthOfYear()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v4

    const-string v4, "%s%s%s"

    .line 155
    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 157
    new-instance v4, Lorg/joda/time/LocalDate;

    invoke-direct {v4}, Lorg/joda/time/LocalDate;-><init>()V

    .line 158
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v6

    :goto_0
    const/16 v7, 0xd

    if-ge v3, v7, :cond_0

    .line 160
    invoke-virtual {v4, v3}, Lorg/joda/time/LocalDate;->withMonthOfYear(I)Lorg/joda/time/LocalDate;

    move-result-object v7

    invoke-virtual {v7}, Lorg/joda/time/LocalDate;->monthOfYear()Lorg/joda/time/LocalDate$Property;

    move-result-object v7

    invoke-virtual {v7, p1}, Lorg/joda/time/LocalDate$Property;->getAsText(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    .line 161
    invoke-interface {v6, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    invoke-virtual {v7, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v6
.end method

.method isNumberPattern(Ljava/lang/String;)Z
    .locals 0
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 97
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    const/4 p1, 0x0

    return p1
.end method

.method isTimezone(Ljava/lang/String;)Z
    .locals 0
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 119
    :try_start_0
    invoke-static {p1}, Lorg/joda/time/DateTimeZone;->forID(Ljava/lang/String;)Lorg/joda/time/DateTimeZone;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    const/4 p1, 0x0

    return p1
.end method

.method numberPattern(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 106
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-eq p1, v2, :cond_1

    const/4 v3, 0x4

    if-eq p1, v3, :cond_0

    .line 112
    iget-object p1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {p1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->dayOfMonth()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 108
    :cond_0
    new-array p1, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v3}, Lcom/cronutils/htime/DatetimeFormatterConstants;->year()Ljava/lang/String;

    move-result-object v3

    aput-object v3, p1, v1

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->year()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    iget-object v0, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v0}, Lcom/cronutils/htime/DatetimeFormatterConstants;->year()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->year()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    const-string v0, "%s%s%s%s"

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 110
    :cond_1
    new-array p1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v2}, Lcom/cronutils/htime/DatetimeFormatterConstants;->dayOfMonth()Ljava/lang/String;

    move-result-object v2

    aput-object v2, p1, v1

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->dayOfMonth()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    const-string v0, "%s%s"

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method parseDateSlashes(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 67
    iget-object p1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->dateFormatByCountry:Ljava/util/Map;

    iget-object v0, p0, Lcom/cronutils/htime/DateTimeFormatParser;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->dateFormatByCountry:Ljava/util/Map;

    iget-object v0, p0, Lcom/cronutils/htime/DateTimeFormatParser;->locale:Ljava/util/Locale;

    .line 68
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p1, 0x6

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    .line 69
    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->dayOfMonth()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->dayOfMonth()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    .line 70
    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->monthOfYear()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->monthOfYear()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    .line 71
    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->year()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->year()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    const-string v0, "%s%s/%s%s/%s%s"

    .line 69
    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public parsePattern(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, ""

    const-string v1, "[^A-Za-z0-9/_:\\-\\+ ]"

    .line 40
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/cronutils/htime/DateTimeFormatParser;->isTimezone(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 41
    invoke-virtual {p0, p1}, Lcom/cronutils/htime/DateTimeFormatParser;->timezonePattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 43
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    const-string v1, "/"

    .line 44
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 45
    invoke-virtual {p0, p1}, Lcom/cronutils/htime/DateTimeFormatParser;->parseDateSlashes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    const-string v1, ":"

    .line 47
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 48
    invoke-virtual {p0, p1}, Lcom/cronutils/htime/DateTimeFormatParser;->parseTimeWithColons(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    const-string v1, "[^A-Za-z0-9 ]"

    .line 51
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 52
    invoke-virtual {p0, v0}, Lcom/cronutils/htime/DateTimeFormatParser;->isNumberPattern(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 53
    invoke-virtual {p0, v0}, Lcom/cronutils/htime/DateTimeFormatParser;->numberPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 55
    :cond_3
    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->dayOfWeek:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 56
    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->dayOfWeek:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 58
    :cond_4
    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->months:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 59
    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->months:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    :cond_5
    return-object p1
.end method

.method parseTimeWithColons(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    const/4 v0, 0x2

    .line 77
    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v2}, Lcom/cronutils/htime/DatetimeFormatterConstants;->hourOfDay()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v2}, Lcom/cronutils/htime/DatetimeFormatterConstants;->hourOfDay()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "%s%s"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v5, "am"

    .line 79
    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "pm"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_0

    :cond_0
    const-string v2, "%s"

    goto :goto_1

    .line 80
    :cond_1
    :goto_0
    new-array v1, v0, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v5}, Lcom/cronutils/htime/DatetimeFormatterConstants;->clockhourOfHalfday()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v3

    iget-object v5, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v5}, Lcom/cronutils/htime/DatetimeFormatterConstants;->clockhourOfHalfday()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 81
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "%s "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v5}, Lcom/cronutils/htime/DatetimeFormatterConstants;->halfOfDay()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    const-string v5, ":"

    .line 83
    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 84
    array-length p1, p1

    const/4 v5, 0x3

    if-ne p1, v0, :cond_2

    .line 85
    new-array p1, v4, [Ljava/lang/Object;

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v3

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->minuteOfHour()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v4

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->minuteOfHour()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    const-string v0, "%s:%s%s"

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v3

    invoke-static {v2, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 87
    :cond_2
    new-array p1, v4, [Ljava/lang/Object;

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v3

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    .line 89
    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->minuteOfHour()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v4

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->minuteOfHour()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    iget-object v0, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    .line 90
    invoke-interface {v0}, Lcom/cronutils/htime/DatetimeFormatterConstants;->secondOfMinute()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v5

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {v1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->secondOfMinute()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    const-string v0, "%s:%s%s:%s%s"

    .line 88
    invoke-static {v0, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v3

    .line 87
    invoke-static {v2, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method timezonePattern(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 128
    iget-object p1, p0, Lcom/cronutils/htime/DateTimeFormatParser;->constants:Lcom/cronutils/htime/DatetimeFormatterConstants;

    invoke-interface {p1}, Lcom/cronutils/htime/DatetimeFormatterConstants;->timezoneRFC822()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
