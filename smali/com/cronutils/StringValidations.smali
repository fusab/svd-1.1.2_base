.class public Lcom/cronutils/StringValidations;
.super Ljava/lang/Object;
.source "StringValidations.java"


# instance fields
.field private constraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

.field private lwPattern:Ljava/util/regex/Pattern;

.field private numsAndCharsPattern:Ljava/util/regex/Pattern;

.field private strictRanges:Z

.field private stringToIntKeysPattern:Ljava/util/regex/Pattern;


# direct methods
.method public constructor <init>(Lcom/cronutils/model/field/constraint/FieldConstraints;)V
    .locals 1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-virtual {p1}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getSpecialChars()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cronutils/StringValidations;->buildLWPattern(Ljava/util/Set;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/cronutils/StringValidations;->lwPattern:Ljava/util/regex/Pattern;

    .line 33
    invoke-virtual {p1}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getStringMapping()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/cronutils/StringValidations;->buildStringToIntPattern(Ljava/util/Set;)Ljava/util/regex/Pattern;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/StringValidations;->stringToIntKeysPattern:Ljava/util/regex/Pattern;

    const-string p1, "[#\\?/\\*0-9]"

    .line 34
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/StringValidations;->numsAndCharsPattern:Ljava/util/regex/Pattern;

    return-void
.end method


# virtual methods
.method buildLWPattern(Ljava/util/Set;)Ljava/util/regex/Pattern;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/cronutils/model/field/value/SpecialChar;",
            ">;)",
            "Ljava/util/regex/Pattern;"
        }
    .end annotation

    .line 53
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    const/4 v1, 0x3

    .line 54
    new-array v1, v1, [Lcom/cronutils/model/field/value/SpecialChar;

    sget-object v2, Lcom/cronutils/model/field/value/SpecialChar;->L:Lcom/cronutils/model/field/value/SpecialChar;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/cronutils/model/field/value/SpecialChar;->LW:Lcom/cronutils/model/field/value/SpecialChar;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    sget-object v2, Lcom/cronutils/model/field/value/SpecialChar;->W:Lcom/cronutils/model/field/value/SpecialChar;

    const/4 v4, 0x2

    aput-object v2, v1, v4

    array-length v2, v1

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    .line 55
    invoke-interface {p1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 56
    invoke-virtual {v4}, Lcom/cronutils/model/field/value/SpecialChar;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 59
    :cond_1
    invoke-virtual {p0, v0}, Lcom/cronutils/StringValidations;->buildWordsPattern(Ljava/util/Set;)Ljava/util/regex/Pattern;

    move-result-object p1

    return-object p1
.end method

.method buildStringToIntPattern(Ljava/util/Set;)Ljava/util/regex/Pattern;
    .locals 0
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/regex/Pattern;"
        }
    .end annotation

    .line 39
    invoke-virtual {p0, p1}, Lcom/cronutils/StringValidations;->buildWordsPattern(Ljava/util/Set;)Ljava/util/regex/Pattern;

    move-result-object p1

    return-object p1
.end method

.method buildWordsPattern(Ljava/util/Set;)Ljava/util/regex/Pattern;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/regex/Pattern;"
        }
    .end annotation

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\\b("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 66
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v1, 0x1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-nez v1, :cond_0

    const-string/jumbo v3, "|"

    .line 68
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    .line 72
    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    const-string p1, ")\\b"

    .line 74
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p1

    return-object p1
.end method

.method public removeValidChars(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 44
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    .line 45
    iget-object v0, p0, Lcom/cronutils/StringValidations;->numsAndCharsPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    .line 46
    iget-object v0, p0, Lcom/cronutils/StringValidations;->stringToIntKeysPattern:Ljava/util/regex/Pattern;

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    .line 47
    iget-object v0, p0, Lcom/cronutils/StringValidations;->lwPattern:Ljava/util/regex/Pattern;

    invoke-virtual {p1, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    .line 48
    invoke-virtual {p1, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "\\s+"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, ","

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "-"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
