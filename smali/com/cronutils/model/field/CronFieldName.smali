.class public final enum Lcom/cronutils/model/field/CronFieldName;
.super Ljava/lang/Enum;
.source "CronFieldName.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/cronutils/model/field/CronFieldName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cronutils/model/field/CronFieldName;

.field public static final enum DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

.field public static final enum DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

.field public static final enum HOUR:Lcom/cronutils/model/field/CronFieldName;

.field public static final enum MINUTE:Lcom/cronutils/model/field/CronFieldName;

.field public static final enum MONTH:Lcom/cronutils/model/field/CronFieldName;

.field public static final enum SECOND:Lcom/cronutils/model/field/CronFieldName;

.field public static final enum YEAR:Lcom/cronutils/model/field/CronFieldName;


# instance fields
.field private order:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 20
    new-instance v0, Lcom/cronutils/model/field/CronFieldName;

    const/4 v1, 0x0

    const-string v2, "SECOND"

    invoke-direct {v0, v2, v1, v1}, Lcom/cronutils/model/field/CronFieldName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cronutils/model/field/CronFieldName;->SECOND:Lcom/cronutils/model/field/CronFieldName;

    new-instance v0, Lcom/cronutils/model/field/CronFieldName;

    const/4 v2, 0x1

    const-string v3, "MINUTE"

    invoke-direct {v0, v3, v2, v2}, Lcom/cronutils/model/field/CronFieldName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cronutils/model/field/CronFieldName;->MINUTE:Lcom/cronutils/model/field/CronFieldName;

    new-instance v0, Lcom/cronutils/model/field/CronFieldName;

    const/4 v3, 0x2

    const-string v4, "HOUR"

    invoke-direct {v0, v4, v3, v3}, Lcom/cronutils/model/field/CronFieldName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cronutils/model/field/CronFieldName;->HOUR:Lcom/cronutils/model/field/CronFieldName;

    new-instance v0, Lcom/cronutils/model/field/CronFieldName;

    const/4 v4, 0x3

    const-string v5, "DAY_OF_MONTH"

    invoke-direct {v0, v5, v4, v4}, Lcom/cronutils/model/field/CronFieldName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    new-instance v0, Lcom/cronutils/model/field/CronFieldName;

    const/4 v5, 0x4

    const-string v6, "MONTH"

    invoke-direct {v0, v6, v5, v5}, Lcom/cronutils/model/field/CronFieldName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cronutils/model/field/CronFieldName;->MONTH:Lcom/cronutils/model/field/CronFieldName;

    new-instance v0, Lcom/cronutils/model/field/CronFieldName;

    const/4 v6, 0x5

    const-string v7, "DAY_OF_WEEK"

    invoke-direct {v0, v7, v6, v6}, Lcom/cronutils/model/field/CronFieldName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    new-instance v0, Lcom/cronutils/model/field/CronFieldName;

    const/4 v7, 0x6

    const-string v8, "YEAR"

    invoke-direct {v0, v8, v7, v7}, Lcom/cronutils/model/field/CronFieldName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cronutils/model/field/CronFieldName;->YEAR:Lcom/cronutils/model/field/CronFieldName;

    const/4 v0, 0x7

    .line 19
    new-array v0, v0, [Lcom/cronutils/model/field/CronFieldName;

    sget-object v8, Lcom/cronutils/model/field/CronFieldName;->SECOND:Lcom/cronutils/model/field/CronFieldName;

    aput-object v8, v0, v1

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->MINUTE:Lcom/cronutils/model/field/CronFieldName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->HOUR:Lcom/cronutils/model/field/CronFieldName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->MONTH:Lcom/cronutils/model/field/CronFieldName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->YEAR:Lcom/cronutils/model/field/CronFieldName;

    aput-object v1, v0, v7

    sput-object v0, Lcom/cronutils/model/field/CronFieldName;->$VALUES:[Lcom/cronutils/model/field/CronFieldName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    iput p3, p0, Lcom/cronutils/model/field/CronFieldName;->order:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cronutils/model/field/CronFieldName;
    .locals 1

    .line 19
    const-class v0, Lcom/cronutils/model/field/CronFieldName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/cronutils/model/field/CronFieldName;

    return-object p0
.end method

.method public static values()[Lcom/cronutils/model/field/CronFieldName;
    .locals 1

    .line 19
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->$VALUES:[Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {v0}, [Lcom/cronutils/model/field/CronFieldName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cronutils/model/field/CronFieldName;

    return-object v0
.end method


# virtual methods
.method public getOrder()I
    .locals 1

    .line 38
    iget v0, p0, Lcom/cronutils/model/field/CronFieldName;->order:I

    return v0
.end method
