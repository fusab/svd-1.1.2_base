.class public Lcom/cronutils/model/field/CronField;
.super Ljava/lang/Object;
.source "CronField.java"


# instance fields
.field private constraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

.field private expression:Lcom/cronutils/model/field/expression/FieldExpression;

.field private field:Lcom/cronutils/model/field/CronFieldName;


# direct methods
.method public constructor <init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/constraint/FieldConstraints;)V
    .locals 2

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/cronutils/model/field/CronField;->field:Lcom/cronutils/model/field/CronFieldName;

    const/4 p1, 0x0

    .line 28
    new-array v0, p1, [Ljava/lang/Object;

    const-string v1, "FieldExpression must not be null"

    invoke-static {p2, v1, v0}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/cronutils/model/field/expression/FieldExpression;

    iput-object p2, p0, Lcom/cronutils/model/field/CronField;->expression:Lcom/cronutils/model/field/expression/FieldExpression;

    .line 29
    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "FieldConstraints must not be null"

    invoke-static {p3, p2, p1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/constraint/FieldConstraints;

    iput-object p1, p0, Lcom/cronutils/model/field/CronField;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

    return-void
.end method

.method public static createFieldComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator<",
            "Lcom/cronutils/model/field/CronField;",
            ">;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/cronutils/model/field/CronField$1;

    invoke-direct {v0}, Lcom/cronutils/model/field/CronField$1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/cronutils/model/field/CronField;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

    return-object v0
.end method

.method public getExpression()Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/cronutils/model/field/CronField;->expression:Lcom/cronutils/model/field/expression/FieldExpression;

    return-object v0
.end method

.method public getField()Lcom/cronutils/model/field/CronFieldName;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/cronutils/model/field/CronField;->field:Lcom/cronutils/model/field/CronFieldName;

    return-object v0
.end method
