.class public Lcom/cronutils/model/field/value/SpecialCharFieldValue;
.super Lcom/cronutils/model/field/value/FieldValue;
.source "SpecialCharFieldValue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/cronutils/model/field/value/FieldValue<",
        "Lcom/cronutils/model/field/value/SpecialChar;",
        ">;"
    }
.end annotation


# instance fields
.field private specialChar:Lcom/cronutils/model/field/value/SpecialChar;


# direct methods
.method public constructor <init>(Lcom/cronutils/model/field/value/SpecialChar;)V
    .locals 2

    .line 21
    invoke-direct {p0}, Lcom/cronutils/model/field/value/FieldValue;-><init>()V

    .line 19
    sget-object v0, Lcom/cronutils/model/field/value/SpecialChar;->NONE:Lcom/cronutils/model/field/value/SpecialChar;

    iput-object v0, p0, Lcom/cronutils/model/field/value/SpecialCharFieldValue;->specialChar:Lcom/cronutils/model/field/value/SpecialChar;

    const/4 v0, 0x0

    .line 22
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "special char must not be null"

    invoke-static {p1, v1, v0}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    iput-object p1, p0, Lcom/cronutils/model/field/value/SpecialCharFieldValue;->specialChar:Lcom/cronutils/model/field/value/SpecialChar;

    return-void
.end method


# virtual methods
.method public getValue()Lcom/cronutils/model/field/value/SpecialChar;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/cronutils/model/field/value/SpecialCharFieldValue;->specialChar:Lcom/cronutils/model/field/value/SpecialChar;

    return-object v0
.end method

.method public bridge synthetic getValue()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;->getValue()Lcom/cronutils/model/field/value/SpecialChar;

    move-result-object v0

    return-object v0
.end method
