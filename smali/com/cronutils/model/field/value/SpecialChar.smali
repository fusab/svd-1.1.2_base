.class public final enum Lcom/cronutils/model/field/value/SpecialChar;
.super Ljava/lang/Enum;
.source "SpecialChar.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/cronutils/model/field/value/SpecialChar;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cronutils/model/field/value/SpecialChar;

.field public static final enum HASH:Lcom/cronutils/model/field/value/SpecialChar;

.field public static final enum L:Lcom/cronutils/model/field/value/SpecialChar;

.field public static final enum LW:Lcom/cronutils/model/field/value/SpecialChar;

.field public static final enum NONE:Lcom/cronutils/model/field/value/SpecialChar;

.field public static final enum QUESTION_MARK:Lcom/cronutils/model/field/value/SpecialChar;

.field public static final enum W:Lcom/cronutils/model/field/value/SpecialChar;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 19
    new-instance v0, Lcom/cronutils/model/field/value/SpecialChar;

    const/4 v1, 0x0

    const-string v2, "LW"

    invoke-direct {v0, v2, v1}, Lcom/cronutils/model/field/value/SpecialChar;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cronutils/model/field/value/SpecialChar;->LW:Lcom/cronutils/model/field/value/SpecialChar;

    new-instance v0, Lcom/cronutils/model/field/value/SpecialChar;

    const/4 v2, 0x1

    const-string v3, "L"

    invoke-direct {v0, v3, v2}, Lcom/cronutils/model/field/value/SpecialChar;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cronutils/model/field/value/SpecialChar;->L:Lcom/cronutils/model/field/value/SpecialChar;

    new-instance v0, Lcom/cronutils/model/field/value/SpecialChar;

    const/4 v3, 0x2

    const-string v4, "W"

    invoke-direct {v0, v4, v3}, Lcom/cronutils/model/field/value/SpecialChar;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cronutils/model/field/value/SpecialChar;->W:Lcom/cronutils/model/field/value/SpecialChar;

    new-instance v0, Lcom/cronutils/model/field/value/SpecialChar;

    const/4 v4, 0x3

    const-string v5, "HASH"

    invoke-direct {v0, v5, v4}, Lcom/cronutils/model/field/value/SpecialChar;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cronutils/model/field/value/SpecialChar;->HASH:Lcom/cronutils/model/field/value/SpecialChar;

    new-instance v0, Lcom/cronutils/model/field/value/SpecialChar;

    const/4 v5, 0x4

    const-string v6, "QUESTION_MARK"

    invoke-direct {v0, v6, v5}, Lcom/cronutils/model/field/value/SpecialChar;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cronutils/model/field/value/SpecialChar;->QUESTION_MARK:Lcom/cronutils/model/field/value/SpecialChar;

    new-instance v0, Lcom/cronutils/model/field/value/SpecialChar;

    const/4 v6, 0x5

    const-string v7, "NONE"

    invoke-direct {v0, v7, v6}, Lcom/cronutils/model/field/value/SpecialChar;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cronutils/model/field/value/SpecialChar;->NONE:Lcom/cronutils/model/field/value/SpecialChar;

    const/4 v0, 0x6

    .line 18
    new-array v0, v0, [Lcom/cronutils/model/field/value/SpecialChar;

    sget-object v7, Lcom/cronutils/model/field/value/SpecialChar;->LW:Lcom/cronutils/model/field/value/SpecialChar;

    aput-object v7, v0, v1

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->L:Lcom/cronutils/model/field/value/SpecialChar;

    aput-object v1, v0, v2

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->W:Lcom/cronutils/model/field/value/SpecialChar;

    aput-object v1, v0, v3

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->HASH:Lcom/cronutils/model/field/value/SpecialChar;

    aput-object v1, v0, v4

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->QUESTION_MARK:Lcom/cronutils/model/field/value/SpecialChar;

    aput-object v1, v0, v5

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->NONE:Lcom/cronutils/model/field/value/SpecialChar;

    aput-object v1, v0, v6

    sput-object v0, Lcom/cronutils/model/field/value/SpecialChar;->$VALUES:[Lcom/cronutils/model/field/value/SpecialChar;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cronutils/model/field/value/SpecialChar;
    .locals 1

    .line 18
    const-class v0, Lcom/cronutils/model/field/value/SpecialChar;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/cronutils/model/field/value/SpecialChar;

    return-object p0
.end method

.method public static values()[Lcom/cronutils/model/field/value/SpecialChar;
    .locals 1

    .line 18
    sget-object v0, Lcom/cronutils/model/field/value/SpecialChar;->$VALUES:[Lcom/cronutils/model/field/value/SpecialChar;

    invoke-virtual {v0}, [Lcom/cronutils/model/field/value/SpecialChar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cronutils/model/field/value/SpecialChar;

    return-object v0
.end method
