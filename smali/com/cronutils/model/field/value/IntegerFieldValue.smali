.class public Lcom/cronutils/model/field/value/IntegerFieldValue;
.super Lcom/cronutils/model/field/value/FieldValue;
.source "IntegerFieldValue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/cronutils/model/field/value/FieldValue<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private value:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/cronutils/model/field/value/FieldValue;-><init>()V

    .line 19
    iput p1, p0, Lcom/cronutils/model/field/value/IntegerFieldValue;->value:I

    return-void
.end method


# virtual methods
.method public getValue()Ljava/lang/Integer;
    .locals 1

    .line 24
    iget v0, p0, Lcom/cronutils/model/field/value/IntegerFieldValue;->value:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getValue()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
