.class public Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;
.super Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;
.source "FieldSpecialCharsDefinitionBuilder.java"


# direct methods
.method public constructor <init>(Lcom/cronutils/model/definition/CronDefinitionBuilder;Lcom/cronutils/model/field/CronFieldName;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;-><init>(Lcom/cronutils/model/definition/CronDefinitionBuilder;Lcom/cronutils/model/field/CronFieldName;)V

    return-void
.end method


# virtual methods
.method public supportsHash()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->addHashSupport()Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    return-object p0
.end method

.method public supportsL()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->addLSupport()Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    return-object p0
.end method

.method public supportsLW()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->addLWSupport()Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    return-object p0
.end method

.method public supportsQuestionMark()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->addQuestionMarkSupport()Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    return-object p0
.end method

.method public supportsW()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->addWSupport()Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    return-object p0
.end method

.method public bridge synthetic withIntMapping(II)Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;
    .locals 0

    .line 22
    invoke-virtual {p0, p1, p2}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->withIntMapping(II)Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object p1

    return-object p1
.end method

.method public withIntMapping(II)Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;
    .locals 0

    .line 85
    invoke-super {p0, p1, p2}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->withIntMapping(II)Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    return-object p0
.end method

.method public bridge synthetic withValidRange(II)Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;
    .locals 0

    .line 22
    invoke-virtual {p0, p1, p2}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->withValidRange(II)Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object p1

    return-object p1
.end method

.method public withValidRange(II)Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;
    .locals 0

    .line 96
    invoke-super {p0, p1, p2}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->withValidRange(II)Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    return-object p0
.end method
