.class final Lcom/cronutils/model/field/definition/FieldDefinition$1;
.super Ljava/lang/Object;
.source "FieldDefinition.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/model/field/definition/FieldDefinition;->createFieldDefinitionComparator()Ljava/util/Comparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/cronutils/model/field/definition/FieldDefinition;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/cronutils/model/field/definition/FieldDefinition;Lcom/cronutils/model/field/definition/FieldDefinition;)I
    .locals 0

    .line 65
    invoke-virtual {p1}, Lcom/cronutils/model/field/definition/FieldDefinition;->getFieldName()Lcom/cronutils/model/field/CronFieldName;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/CronFieldName;->getOrder()I

    move-result p1

    invoke-virtual {p2}, Lcom/cronutils/model/field/definition/FieldDefinition;->getFieldName()Lcom/cronutils/model/field/CronFieldName;

    move-result-object p2

    invoke-virtual {p2}, Lcom/cronutils/model/field/CronFieldName;->getOrder()I

    move-result p2

    sub-int/2addr p1, p2

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 62
    check-cast p1, Lcom/cronutils/model/field/definition/FieldDefinition;

    check-cast p2, Lcom/cronutils/model/field/definition/FieldDefinition;

    invoke-virtual {p0, p1, p2}, Lcom/cronutils/model/field/definition/FieldDefinition$1;->compare(Lcom/cronutils/model/field/definition/FieldDefinition;Lcom/cronutils/model/field/definition/FieldDefinition;)I

    move-result p1

    return p1
.end method
