.class public Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;
.super Lcom/cronutils/model/field/definition/FieldDefinition;
.source "DayOfWeekFieldDefinition.java"


# instance fields
.field private mondayDoWValue:Lcom/cronutils/mapper/WeekDay;


# direct methods
.method public constructor <init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/constraint/FieldConstraints;Lcom/cronutils/mapper/WeekDay;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/cronutils/model/field/definition/FieldDefinition;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    .line 33
    iput-object p3, p0, Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;->mondayDoWValue:Lcom/cronutils/mapper/WeekDay;

    return-void
.end method


# virtual methods
.method public getMondayDoWValue()Lcom/cronutils/mapper/WeekDay;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;->mondayDoWValue:Lcom/cronutils/mapper/WeekDay;

    return-object v0
.end method
