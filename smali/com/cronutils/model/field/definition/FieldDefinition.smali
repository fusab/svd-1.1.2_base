.class public Lcom/cronutils/model/field/definition/FieldDefinition;
.super Ljava/lang/Object;
.source "FieldDefinition.java"


# instance fields
.field private constraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

.field private fieldName:Lcom/cronutils/model/field/CronFieldName;


# direct methods
.method public constructor <init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/constraint/FieldConstraints;)V
    .locals 3

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 37
    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "CronFieldName must not be null"

    invoke-static {p1, v2, v1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/CronFieldName;

    iput-object p1, p0, Lcom/cronutils/model/field/definition/FieldDefinition;->fieldName:Lcom/cronutils/model/field/CronFieldName;

    .line 38
    new-array p1, v0, [Ljava/lang/Object;

    const-string v0, "FieldConstraints must not be null"

    invoke-static {p2, v0, p1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/constraint/FieldConstraints;

    iput-object p1, p0, Lcom/cronutils/model/field/definition/FieldDefinition;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

    return-void
.end method

.method public static createFieldDefinitionComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator<",
            "Lcom/cronutils/model/field/definition/FieldDefinition;",
            ">;"
        }
    .end annotation

    .line 62
    new-instance v0, Lcom/cronutils/model/field/definition/FieldDefinition$1;

    invoke-direct {v0}, Lcom/cronutils/model/field/definition/FieldDefinition$1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/cronutils/model/field/definition/FieldDefinition;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

    return-object v0
.end method

.method public getFieldName()Lcom/cronutils/model/field/CronFieldName;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/cronutils/model/field/definition/FieldDefinition;->fieldName:Lcom/cronutils/model/field/CronFieldName;

    return-object v0
.end method
