.class public Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;
.super Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;
.source "FieldDayOfWeekDefinitionBuilder.java"


# instance fields
.field private mondayDoWValue:I


# direct methods
.method public constructor <init>(Lcom/cronutils/model/definition/CronDefinitionBuilder;Lcom/cronutils/model/field/CronFieldName;)V
    .locals 1

    .line 30
    invoke-direct {p0, p1, p2}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;-><init>(Lcom/cronutils/model/definition/CronDefinitionBuilder;Lcom/cronutils/model/field/CronFieldName;)V

    const/4 p1, 0x1

    .line 21
    iput p1, p0, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->mondayDoWValue:I

    .line 31
    sget-object p1, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p1, p2}, Lcom/cronutils/model/field/CronFieldName;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    const-string v0, "CronFieldName must be DAY_OF_WEEK"

    invoke-static {p1, v0, p2}, Lorg/apache/commons/lang3/Validate;->isTrue(ZLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public and()Lcom/cronutils/model/definition/CronDefinitionBuilder;
    .locals 7

    .line 51
    iget-object v0, p0, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->createConstraintsInstance()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/cronutils/model/field/constraint/FieldConstraints;->isInRange(I)Z

    move-result v0

    .line 52
    iget-object v1, p0, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->cronDefinitionBuilder:Lcom/cronutils/model/definition/CronDefinitionBuilder;

    new-instance v2, Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;

    iget-object v3, p0, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->fieldName:Lcom/cronutils/model/field/CronFieldName;

    iget-object v4, p0, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    invoke-virtual {v4}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->createConstraintsInstance()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v4

    new-instance v5, Lcom/cronutils/mapper/WeekDay;

    iget v6, p0, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->mondayDoWValue:I

    invoke-direct {v5, v6, v0}, Lcom/cronutils/mapper/WeekDay;-><init>(IZ)V

    invoke-direct {v2, v3, v4, v5}, Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/constraint/FieldConstraints;Lcom/cronutils/mapper/WeekDay;)V

    invoke-virtual {v1, v2}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->register(Lcom/cronutils/model/field/definition/FieldDefinition;)V

    .line 53
    iget-object v0, p0, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->cronDefinitionBuilder:Lcom/cronutils/model/definition/CronDefinitionBuilder;

    return-object v0
.end method

.method public withIntMapping(II)Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;
    .locals 0

    .line 74
    invoke-super {p0, p1, p2}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->withIntMapping(II)Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    return-object p0
.end method

.method public bridge synthetic withIntMapping(II)Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;
    .locals 0

    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->withIntMapping(II)Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic withIntMapping(II)Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;
    .locals 0

    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->withIntMapping(II)Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    move-result-object p1

    return-object p1
.end method

.method public withMondayDoWValue(I)Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;
    .locals 2

    .line 39
    iget v0, p0, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->mondayDoWValue:I

    if-eq p1, v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    iget v1, p0, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->mondayDoWValue:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->withShiftedStringMapping(I)Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    .line 42
    :cond_0
    iput p1, p0, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->mondayDoWValue:I

    return-object p0
.end method

.method public withValidRange(II)Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;
    .locals 0

    .line 63
    invoke-super {p0, p1, p2}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->withValidRange(II)Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    return-object p0
.end method

.method public bridge synthetic withValidRange(II)Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;
    .locals 0

    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->withValidRange(II)Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic withValidRange(II)Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;
    .locals 0

    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->withValidRange(II)Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    move-result-object p1

    return-object p1
.end method
