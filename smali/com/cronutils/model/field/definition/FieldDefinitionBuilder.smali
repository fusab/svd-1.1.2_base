.class public Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;
.super Ljava/lang/Object;
.source "FieldDefinitionBuilder.java"


# instance fields
.field protected constraints:Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

.field protected cronDefinitionBuilder:Lcom/cronutils/model/definition/CronDefinitionBuilder;

.field protected final fieldName:Lcom/cronutils/model/field/CronFieldName;


# direct methods
.method public constructor <init>(Lcom/cronutils/model/definition/CronDefinitionBuilder;Lcom/cronutils/model/field/CronFieldName;)V
    .locals 3

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 36
    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "ParserBuilder must not be null"

    invoke-static {p1, v2, v1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/definition/CronDefinitionBuilder;

    iput-object p1, p0, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->cronDefinitionBuilder:Lcom/cronutils/model/definition/CronDefinitionBuilder;

    .line 37
    new-array p1, v0, [Ljava/lang/Object;

    const-string v0, "CronFieldName must not be null"

    invoke-static {p2, v0, p1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/CronFieldName;

    iput-object p1, p0, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->fieldName:Lcom/cronutils/model/field/CronFieldName;

    .line 38
    invoke-static {}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->instance()Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->forField(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    return-void
.end method


# virtual methods
.method public and()Lcom/cronutils/model/definition/CronDefinitionBuilder;
    .locals 4

    .line 69
    iget-object v0, p0, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->cronDefinitionBuilder:Lcom/cronutils/model/definition/CronDefinitionBuilder;

    new-instance v1, Lcom/cronutils/model/field/definition/FieldDefinition;

    iget-object v2, p0, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->fieldName:Lcom/cronutils/model/field/CronFieldName;

    iget-object v3, p0, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    invoke-virtual {v3}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->createConstraintsInstance()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/cronutils/model/field/definition/FieldDefinition;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    invoke-virtual {v0, v1}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->register(Lcom/cronutils/model/field/definition/FieldDefinition;)V

    .line 70
    iget-object v0, p0, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->cronDefinitionBuilder:Lcom/cronutils/model/definition/CronDefinitionBuilder;

    return-object v0
.end method

.method public withIntMapping(II)Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    invoke-virtual {v0, p1, p2}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->withIntValueMapping(II)Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    return-object p0
.end method

.method public withValidRange(II)Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    invoke-virtual {v0, p1, p2}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->withValidRange(II)Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    return-object p0
.end method
