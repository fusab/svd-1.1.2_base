.class public Lcom/cronutils/model/field/expression/visitor/ValueMappingFieldExpressionVisitor;
.super Ljava/lang/Object;
.source "ValueMappingFieldExpressionVisitor.java"

# interfaces
.implements Lcom/cronutils/model/field/expression/visitor/FieldExpressionVisitor;


# instance fields
.field private transform:Lcom/google/common/base/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Function<",
            "Lcom/cronutils/model/field/value/FieldValue;",
            "Lcom/cronutils/model/field/value/FieldValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/common/base/Function;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Function<",
            "Lcom/cronutils/model/field/value/FieldValue;",
            "Lcom/cronutils/model/field/value/FieldValue;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/cronutils/model/field/expression/visitor/ValueMappingFieldExpressionVisitor;->transform:Lcom/google/common/base/Function;

    return-void
.end method


# virtual methods
.method public visit(Lcom/cronutils/model/field/expression/Always;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    return-object p1
.end method

.method public visit(Lcom/cronutils/model/field/expression/And;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 2

    .line 39
    new-instance v0, Lcom/cronutils/model/field/expression/And;

    invoke-direct {v0}, Lcom/cronutils/model/field/expression/And;-><init>()V

    .line 40
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/And;->getExpressions()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cronutils/model/field/expression/FieldExpression;

    .line 41
    invoke-virtual {p0, v1}, Lcom/cronutils/model/field/expression/visitor/ValueMappingFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cronutils/model/field/expression/And;->and(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/And;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public visit(Lcom/cronutils/model/field/expression/Between;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/cronutils/model/field/expression/visitor/ValueMappingFieldExpressionVisitor;->transform:Lcom/google/common/base/Function;

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getFrom()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cronutils/model/field/value/FieldValue;

    .line 49
    iget-object v1, p0, Lcom/cronutils/model/field/expression/visitor/ValueMappingFieldExpressionVisitor;->transform:Lcom/google/common/base/Function;

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getTo()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/value/FieldValue;

    .line 50
    new-instance v1, Lcom/cronutils/model/field/expression/Between;

    invoke-direct {v1, v0, p1}, Lcom/cronutils/model/field/expression/Between;-><init>(Lcom/cronutils/model/field/value/FieldValue;Lcom/cronutils/model/field/value/FieldValue;)V

    return-object v1
.end method

.method public visit(Lcom/cronutils/model/field/expression/Every;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 2

    .line 55
    new-instance v0, Lcom/cronutils/model/field/expression/Every;

    iget-object v1, p0, Lcom/cronutils/model/field/expression/visitor/ValueMappingFieldExpressionVisitor;->transform:Lcom/google/common/base/Function;

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Every;->getPeriod()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-direct {v0, p1}, Lcom/cronutils/model/field/expression/Every;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-object v0
.end method

.method public visit(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 1

    .line 70
    instance-of v0, p1, Lcom/cronutils/model/field/expression/Always;

    if-eqz v0, :cond_0

    .line 71
    check-cast p1, Lcom/cronutils/model/field/expression/Always;

    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValueMappingFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/Always;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    return-object p1

    .line 73
    :cond_0
    instance-of v0, p1, Lcom/cronutils/model/field/expression/And;

    if-eqz v0, :cond_1

    .line 74
    check-cast p1, Lcom/cronutils/model/field/expression/And;

    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValueMappingFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/And;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    return-object p1

    .line 76
    :cond_1
    instance-of v0, p1, Lcom/cronutils/model/field/expression/Between;

    if-eqz v0, :cond_2

    .line 77
    check-cast p1, Lcom/cronutils/model/field/expression/Between;

    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValueMappingFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/Between;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    return-object p1

    .line 79
    :cond_2
    instance-of v0, p1, Lcom/cronutils/model/field/expression/Every;

    if-eqz v0, :cond_3

    .line 80
    check-cast p1, Lcom/cronutils/model/field/expression/Every;

    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValueMappingFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/Every;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    return-object p1

    .line 82
    :cond_3
    instance-of v0, p1, Lcom/cronutils/model/field/expression/On;

    if-eqz v0, :cond_4

    .line 83
    check-cast p1, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValueMappingFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/On;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    return-object p1

    .line 85
    :cond_4
    instance-of v0, p1, Lcom/cronutils/model/field/expression/QuestionMark;

    if-eqz v0, :cond_5

    .line 86
    check-cast p1, Lcom/cronutils/model/field/expression/QuestionMark;

    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValueMappingFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/QuestionMark;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    :cond_5
    return-object p1
.end method

.method public visit(Lcom/cronutils/model/field/expression/On;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 3

    .line 60
    new-instance v0, Lcom/cronutils/model/field/expression/On;

    iget-object v1, p0, Lcom/cronutils/model/field/expression/visitor/ValueMappingFieldExpressionVisitor;->transform:Lcom/google/common/base/Function;

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getSpecialChar()Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    move-result-object v2

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getNth()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    invoke-direct {v0, v1, v2, p1}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;Lcom/cronutils/model/field/value/SpecialCharFieldValue;Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-object v0
.end method

.method public visit(Lcom/cronutils/model/field/expression/QuestionMark;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    .line 65
    new-instance p1, Lcom/cronutils/model/field/expression/QuestionMark;

    invoke-direct {p1}, Lcom/cronutils/model/field/expression/QuestionMark;-><init>()V

    return-object p1
.end method
