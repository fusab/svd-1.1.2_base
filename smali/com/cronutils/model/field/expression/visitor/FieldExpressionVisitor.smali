.class public interface abstract Lcom/cronutils/model/field/expression/visitor/FieldExpressionVisitor;
.super Ljava/lang/Object;
.source "FieldExpressionVisitor.java"


# virtual methods
.method public abstract visit(Lcom/cronutils/model/field/expression/Always;)Lcom/cronutils/model/field/expression/FieldExpression;
.end method

.method public abstract visit(Lcom/cronutils/model/field/expression/And;)Lcom/cronutils/model/field/expression/FieldExpression;
.end method

.method public abstract visit(Lcom/cronutils/model/field/expression/Between;)Lcom/cronutils/model/field/expression/FieldExpression;
.end method

.method public abstract visit(Lcom/cronutils/model/field/expression/Every;)Lcom/cronutils/model/field/expression/FieldExpression;
.end method

.method public abstract visit(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/FieldExpression;
.end method

.method public abstract visit(Lcom/cronutils/model/field/expression/On;)Lcom/cronutils/model/field/expression/FieldExpression;
.end method

.method public abstract visit(Lcom/cronutils/model/field/expression/QuestionMark;)Lcom/cronutils/model/field/expression/FieldExpression;
.end method
