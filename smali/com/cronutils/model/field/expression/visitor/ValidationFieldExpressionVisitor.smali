.class public Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;
.super Ljava/lang/Object;
.source "ValidationFieldExpressionVisitor.java"

# interfaces
.implements Lcom/cronutils/model/field/expression/visitor/FieldExpressionVisitor;


# static fields
.field private static final OORANGE:Ljava/lang/String; = "Value %s not in range [%s, %s]"


# instance fields
.field private constraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

.field private strictRanges:Z

.field private stringValidations:Lcom/cronutils/StringValidations;


# direct methods
.method protected constructor <init>(Lcom/cronutils/model/field/constraint/FieldConstraints;Lcom/cronutils/StringValidations;Z)V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

    .line 38
    iput-object p2, p0, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->stringValidations:Lcom/cronutils/StringValidations;

    .line 39
    iput-boolean p3, p0, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->strictRanges:Z

    return-void
.end method

.method public constructor <init>(Lcom/cronutils/model/field/constraint/FieldConstraints;Z)V
    .locals 1

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

    .line 32
    new-instance v0, Lcom/cronutils/StringValidations;

    invoke-direct {v0, p1}, Lcom/cronutils/StringValidations;-><init>(Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    iput-object v0, p0, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->stringValidations:Lcom/cronutils/StringValidations;

    .line 33
    iput-boolean p2, p0, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->strictRanges:Z

    return-void
.end method


# virtual methods
.method isDefault(Lcom/cronutils/model/field/value/FieldValue;)Z
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 147
    instance-of v0, p1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-virtual {p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method isInRange(Lcom/cronutils/model/field/value/FieldValue;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 137
    instance-of v0, p1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    if-eqz v0, :cond_1

    .line 138
    check-cast p1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-virtual {p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 139
    iget-object v0, p0, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

    invoke-virtual {v0, p1}, Lcom/cronutils/model/field/constraint/FieldConstraints;->isInRange(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 140
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v2

    const/4 p1, 0x1

    iget-object v2, p0, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

    invoke-virtual {v2}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getStartRange()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, p1

    const/4 p1, 0x2

    iget-object v2, p0, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->constraints:Lcom/cronutils/model/field/constraint/FieldConstraints;

    invoke-virtual {v2}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getEndRange()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, p1

    const-string p1, "Value %s not in range [%s, %s]"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return-void
.end method

.method isSpecialCharNotL(Lcom/cronutils/model/field/value/FieldValue;)Z
    .locals 1

    .line 151
    instance-of v0, p1, Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/cronutils/model/field/value/SpecialChar;->L:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-virtual {p1}, Lcom/cronutils/model/field/value/FieldValue;->getValue()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/cronutils/model/field/value/SpecialChar;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public visit(Lcom/cronutils/model/field/expression/Always;)Lcom/cronutils/model/field/expression/Always;
    .locals 0

    return-object p1
.end method

.method public visit(Lcom/cronutils/model/field/expression/And;)Lcom/cronutils/model/field/expression/And;
    .locals 2

    .line 75
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/And;->getExpressions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cronutils/model/field/expression/FieldExpression;

    .line 76
    invoke-virtual {p0, v1}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/FieldExpression;

    goto :goto_0

    :cond_0
    return-object p1
.end method

.method public visit(Lcom/cronutils/model/field/expression/Between;)Lcom/cronutils/model/field/expression/Between;
    .locals 4

    .line 83
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getFrom()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->isInRange(Lcom/cronutils/model/field/value/FieldValue;)V

    .line 84
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getTo()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->isInRange(Lcom/cronutils/model/field/value/FieldValue;)V

    .line 85
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getFrom()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->isSpecialCharNotL(Lcom/cronutils/model/field/value/FieldValue;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getTo()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->isSpecialCharNotL(Lcom/cronutils/model/field/value/FieldValue;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 88
    iget-boolean v0, p0, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->strictRanges:Z

    if-eqz v0, :cond_1

    .line 89
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getFrom()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object v0

    instance-of v0, v0, Lcom/cronutils/model/field/value/IntegerFieldValue;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getTo()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object v0

    instance-of v0, v0, Lcom/cronutils/model/field/value/IntegerFieldValue;

    if-eqz v0, :cond_1

    .line 90
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getFrom()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object v0

    check-cast v0, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-virtual {v0}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 91
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getTo()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object v1

    check-cast v1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-virtual {v1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v0, v1, :cond_0

    goto :goto_0

    .line 93
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v0

    const-string v0, "Invalid range! [%s,%s]"

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    return-object p1

    .line 86
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "No special characters allowed in range, except for \'L\'"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public visit(Lcom/cronutils/model/field/expression/Every;)Lcom/cronutils/model/field/expression/Every;
    .locals 1

    .line 102
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Every;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    instance-of v0, v0, Lcom/cronutils/model/field/expression/Between;

    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Every;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    check-cast v0, Lcom/cronutils/model/field/expression/Between;

    invoke-virtual {p0, v0}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/Between;)Lcom/cronutils/model/field/expression/Between;

    .line 105
    :cond_0
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Every;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    instance-of v0, v0, Lcom/cronutils/model/field/expression/On;

    if-eqz v0, :cond_1

    .line 106
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Every;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    check-cast v0, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {p0, v0}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/On;)Lcom/cronutils/model/field/expression/On;

    .line 108
    :cond_1
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Every;->getPeriod()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->isInRange(Lcom/cronutils/model/field/value/FieldValue;)V

    return-object p1
.end method

.method public bridge synthetic visit(Lcom/cronutils/model/field/expression/Always;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/Always;)Lcom/cronutils/model/field/expression/Always;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic visit(Lcom/cronutils/model/field/expression/And;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/And;)Lcom/cronutils/model/field/expression/And;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic visit(Lcom/cronutils/model/field/expression/Between;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/Between;)Lcom/cronutils/model/field/expression/Between;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic visit(Lcom/cronutils/model/field/expression/Every;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/Every;)Lcom/cronutils/model/field/expression/Every;

    move-result-object p1

    return-object p1
.end method

.method public visit(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 4

    .line 44
    iget-object v0, p0, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->stringValidations:Lcom/cronutils/StringValidations;

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/FieldExpression;->asString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cronutils/StringValidations;->removeValidChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    .line 45
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 46
    instance-of v1, p1, Lcom/cronutils/model/field/expression/Always;

    if-eqz v1, :cond_0

    .line 47
    check-cast p1, Lcom/cronutils/model/field/expression/Always;

    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/Always;)Lcom/cronutils/model/field/expression/Always;

    move-result-object p1

    return-object p1

    .line 49
    :cond_0
    instance-of v1, p1, Lcom/cronutils/model/field/expression/And;

    if-eqz v1, :cond_1

    .line 50
    check-cast p1, Lcom/cronutils/model/field/expression/And;

    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/And;)Lcom/cronutils/model/field/expression/And;

    move-result-object p1

    return-object p1

    .line 52
    :cond_1
    instance-of v1, p1, Lcom/cronutils/model/field/expression/Between;

    if-eqz v1, :cond_2

    .line 53
    check-cast p1, Lcom/cronutils/model/field/expression/Between;

    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/Between;)Lcom/cronutils/model/field/expression/Between;

    move-result-object p1

    return-object p1

    .line 55
    :cond_2
    instance-of v1, p1, Lcom/cronutils/model/field/expression/Every;

    if-eqz v1, :cond_3

    .line 56
    check-cast p1, Lcom/cronutils/model/field/expression/Every;

    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/Every;)Lcom/cronutils/model/field/expression/Every;

    move-result-object p1

    return-object p1

    .line 58
    :cond_3
    instance-of v1, p1, Lcom/cronutils/model/field/expression/On;

    if-eqz v1, :cond_4

    .line 59
    check-cast p1, Lcom/cronutils/model/field/expression/On;

    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/On;)Lcom/cronutils/model/field/expression/On;

    move-result-object p1

    return-object p1

    .line 61
    :cond_4
    instance-of v1, p1, Lcom/cronutils/model/field/expression/QuestionMark;

    if-eqz v1, :cond_5

    .line 62
    check-cast p1, Lcom/cronutils/model/field/expression/QuestionMark;

    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/QuestionMark;)Lcom/cronutils/model/field/expression/QuestionMark;

    move-result-object p1

    return-object p1

    .line 65
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/FieldExpression;->asString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v3

    const/4 p1, 0x1

    aput-object v0, v2, p1

    const-string p1, "Invalid chars in expression! Expression: %s Invalid chars: %s"

    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public bridge synthetic visit(Lcom/cronutils/model/field/expression/On;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/On;)Lcom/cronutils/model/field/expression/On;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic visit(Lcom/cronutils/model/field/expression/QuestionMark;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/QuestionMark;)Lcom/cronutils/model/field/expression/QuestionMark;

    move-result-object p1

    return-object p1
.end method

.method public visit(Lcom/cronutils/model/field/expression/On;)Lcom/cronutils/model/field/expression/On;
    .locals 1

    .line 114
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->isDefault(Lcom/cronutils/model/field/value/FieldValue;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->isInRange(Lcom/cronutils/model/field/value/FieldValue;)V

    .line 117
    :cond_0
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getNth()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->isDefault(Lcom/cronutils/model/field/value/FieldValue;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 118
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/On;->getNth()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;->isInRange(Lcom/cronutils/model/field/value/FieldValue;)V

    :cond_1
    return-object p1
.end method

.method public visit(Lcom/cronutils/model/field/expression/QuestionMark;)Lcom/cronutils/model/field/expression/QuestionMark;
    .locals 0

    return-object p1
.end method
