.class public abstract Lcom/cronutils/model/field/expression/FieldExpression;
.super Ljava/lang/Object;
.source "FieldExpression.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/cronutils/model/field/expression/visitor/FieldExpressionVisitor;)Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 2

    const/4 v0, 0x0

    .line 38
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "FieldExpressionVisitor must not be null"

    invoke-static {p1, v1, v0}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    invoke-interface {p1, p0}, Lcom/cronutils/model/field/expression/visitor/FieldExpressionVisitor;->visit(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    return-object p1
.end method

.method public and(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/And;
    .locals 1

    .line 21
    new-instance v0, Lcom/cronutils/model/field/expression/And;

    invoke-direct {v0}, Lcom/cronutils/model/field/expression/And;-><init>()V

    invoke-virtual {v0, p0}, Lcom/cronutils/model/field/expression/And;->and(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/And;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/cronutils/model/field/expression/And;->and(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/And;

    move-result-object p1

    return-object p1
.end method

.method public abstract asString()Ljava/lang/String;
.end method
