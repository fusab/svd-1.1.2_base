.class public Lcom/cronutils/model/field/expression/Between;
.super Lcom/cronutils/model/field/expression/FieldExpression;
.source "Between.java"


# instance fields
.field private from:Lcom/cronutils/model/field/value/FieldValue;

.field private to:Lcom/cronutils/model/field/value/FieldValue;


# direct methods
.method public constructor <init>(Lcom/cronutils/model/field/expression/Between;)V
    .locals 1

    .line 32
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getFrom()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object v0

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Between;->getTo()Lcom/cronutils/model/field/value/FieldValue;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/cronutils/model/field/expression/Between;-><init>(Lcom/cronutils/model/field/value/FieldValue;Lcom/cronutils/model/field/value/FieldValue;)V

    return-void
.end method

.method public constructor <init>(Lcom/cronutils/model/field/value/FieldValue;Lcom/cronutils/model/field/value/FieldValue;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/cronutils/model/field/expression/FieldExpression;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/cronutils/model/field/expression/Between;->from:Lcom/cronutils/model/field/value/FieldValue;

    .line 28
    iput-object p2, p0, Lcom/cronutils/model/field/expression/Between;->to:Lcom/cronutils/model/field/value/FieldValue;

    return-void
.end method


# virtual methods
.method public asString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x2

    .line 45
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/cronutils/model/field/expression/Between;->from:Lcom/cronutils/model/field/value/FieldValue;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/cronutils/model/field/expression/Between;->to:Lcom/cronutils/model/field/value/FieldValue;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "%s-%s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFrom()Lcom/cronutils/model/field/value/FieldValue;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/cronutils/model/field/expression/Between;->from:Lcom/cronutils/model/field/value/FieldValue;

    return-object v0
.end method

.method public getTo()Lcom/cronutils/model/field/value/FieldValue;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/cronutils/model/field/expression/Between;->to:Lcom/cronutils/model/field/value/FieldValue;

    return-object v0
.end method
