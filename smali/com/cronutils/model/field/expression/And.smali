.class public Lcom/cronutils/model/field/expression/And;
.super Lcom/cronutils/model/field/expression/FieldExpression;
.source "And.java"


# instance fields
.field private expressions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/cronutils/model/field/expression/FieldExpression;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 27
    invoke-direct {p0}, Lcom/cronutils/model/field/expression/FieldExpression;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/cronutils/model/field/expression/And;->expressions:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Lcom/cronutils/model/field/expression/And;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/cronutils/model/field/expression/FieldExpression;-><init>()V

    .line 32
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/And;->getExpressions()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/model/field/expression/And;->expressions:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public and(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/And;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/cronutils/model/field/expression/And;->expressions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public asString()Ljava/lang/String;
    .locals 3

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    .line 44
    :goto_0
    iget-object v2, p0, Lcom/cronutils/model/field/expression/And;->expressions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 45
    iget-object v2, p0, Lcom/cronutils/model/field/expression/And;->expressions:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-virtual {v2}, Lcom/cronutils/model/field/expression/FieldExpression;->asString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    iget-object v2, p0, Lcom/cronutils/model/field/expression/And;->expressions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    const-string v2, ","

    .line 47
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 50
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExpressions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/cronutils/model/field/expression/FieldExpression;",
            ">;"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/cronutils/model/field/expression/And;->expressions:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
