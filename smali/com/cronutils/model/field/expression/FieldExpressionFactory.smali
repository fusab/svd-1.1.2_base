.class public Lcom/cronutils/model/field/expression/FieldExpressionFactory;
.super Ljava/lang/Object;
.source "FieldExpressionFactory.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static always()Lcom/cronutils/model/field/expression/Always;
    .locals 1

    .line 25
    new-instance v0, Lcom/cronutils/model/field/expression/Always;

    invoke-direct {v0}, Lcom/cronutils/model/field/expression/Always;-><init>()V

    return-object v0
.end method

.method public static and(Ljava/util/List;)Lcom/cronutils/model/field/expression/And;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/cronutils/model/field/expression/FieldExpression;",
            ">;)",
            "Lcom/cronutils/model/field/expression/And;"
        }
    .end annotation

    .line 65
    new-instance v0, Lcom/cronutils/model/field/expression/And;

    invoke-direct {v0}, Lcom/cronutils/model/field/expression/And;-><init>()V

    .line 66
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cronutils/model/field/expression/FieldExpression;

    .line 67
    invoke-virtual {v0, v1}, Lcom/cronutils/model/field/expression/And;->and(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/model/field/expression/And;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static between(II)Lcom/cronutils/model/field/expression/Between;
    .locals 2

    .line 29
    new-instance v0, Lcom/cronutils/model/field/expression/Between;

    new-instance v1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-direct {v1, p0}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    new-instance p0, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-direct {p0, p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {v0, v1, p0}, Lcom/cronutils/model/field/expression/Between;-><init>(Lcom/cronutils/model/field/value/FieldValue;Lcom/cronutils/model/field/value/FieldValue;)V

    return-object v0
.end method

.method public static between(Lcom/cronutils/model/field/value/SpecialChar;I)Lcom/cronutils/model/field/expression/Between;
    .locals 2

    .line 33
    new-instance v0, Lcom/cronutils/model/field/expression/Between;

    new-instance v1, Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    invoke-direct {v1, p0}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;-><init>(Lcom/cronutils/model/field/value/SpecialChar;)V

    new-instance p0, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-direct {p0, p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {v0, v1, p0}, Lcom/cronutils/model/field/expression/Between;-><init>(Lcom/cronutils/model/field/value/FieldValue;Lcom/cronutils/model/field/value/FieldValue;)V

    return-object v0
.end method

.method public static every(I)Lcom/cronutils/model/field/expression/Every;
    .locals 2

    .line 37
    new-instance v0, Lcom/cronutils/model/field/expression/Every;

    new-instance v1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-direct {v1, p0}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {v0, v1}, Lcom/cronutils/model/field/expression/Every;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-object v0
.end method

.method public static every(Lcom/cronutils/model/field/expression/FieldExpression;I)Lcom/cronutils/model/field/expression/Every;
    .locals 2

    .line 41
    new-instance v0, Lcom/cronutils/model/field/expression/Every;

    new-instance v1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-direct {v1, p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {v0, p0, v1}, Lcom/cronutils/model/field/expression/Every;-><init>(Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-object v0
.end method

.method public static on(I)Lcom/cronutils/model/field/expression/On;
    .locals 2

    .line 49
    new-instance v0, Lcom/cronutils/model/field/expression/On;

    new-instance v1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-direct {v1, p0}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {v0, v1}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-object v0
.end method

.method public static on(ILcom/cronutils/model/field/value/SpecialChar;)Lcom/cronutils/model/field/expression/On;
    .locals 2

    .line 53
    new-instance v0, Lcom/cronutils/model/field/expression/On;

    new-instance v1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-direct {v1, p0}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    new-instance p0, Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    invoke-direct {p0, p1}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;-><init>(Lcom/cronutils/model/field/value/SpecialChar;)V

    invoke-direct {v0, v1, p0}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;Lcom/cronutils/model/field/value/SpecialCharFieldValue;)V

    return-object v0
.end method

.method public static on(ILcom/cronutils/model/field/value/SpecialChar;I)Lcom/cronutils/model/field/expression/On;
    .locals 2

    .line 57
    new-instance v0, Lcom/cronutils/model/field/expression/On;

    new-instance v1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-direct {v1, p0}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    new-instance p0, Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    invoke-direct {p0, p1}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;-><init>(Lcom/cronutils/model/field/value/SpecialChar;)V

    new-instance p1, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-direct {p1, p2}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {v0, v1, p0, p1}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;Lcom/cronutils/model/field/value/SpecialCharFieldValue;Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-object v0
.end method

.method public static on(Lcom/cronutils/model/field/value/SpecialChar;)Lcom/cronutils/model/field/expression/On;
    .locals 2

    .line 45
    new-instance v0, Lcom/cronutils/model/field/expression/On;

    new-instance v1, Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    invoke-direct {v1, p0}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;-><init>(Lcom/cronutils/model/field/value/SpecialChar;)V

    invoke-direct {v0, v1}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/SpecialCharFieldValue;)V

    return-object v0
.end method

.method public static questionMark()Lcom/cronutils/model/field/expression/QuestionMark;
    .locals 1

    .line 61
    new-instance v0, Lcom/cronutils/model/field/expression/QuestionMark;

    invoke-direct {v0}, Lcom/cronutils/model/field/expression/QuestionMark;-><init>()V

    return-object v0
.end method
