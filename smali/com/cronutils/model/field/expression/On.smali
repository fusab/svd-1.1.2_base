.class public Lcom/cronutils/model/field/expression/On;
.super Lcom/cronutils/model/field/expression/FieldExpression;
.source "On.java"


# static fields
.field private static final DEFAULT_NTH_VALUE:I = -0x1


# instance fields
.field private nth:Lcom/cronutils/model/field/value/IntegerFieldValue;

.field private specialChar:Lcom/cronutils/model/field/value/SpecialCharFieldValue;

.field private time:Lcom/cronutils/model/field/value/IntegerFieldValue;


# direct methods
.method private constructor <init>(Lcom/cronutils/model/field/expression/On;)V
    .locals 2

    .line 27
    iget-object v0, p1, Lcom/cronutils/model/field/expression/On;->time:Lcom/cronutils/model/field/value/IntegerFieldValue;

    iget-object v1, p1, Lcom/cronutils/model/field/expression/On;->specialChar:Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    iget-object p1, p1, Lcom/cronutils/model/field/expression/On;->nth:Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-direct {p0, v0, v1, p1}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;Lcom/cronutils/model/field/value/SpecialCharFieldValue;Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-void
.end method

.method public constructor <init>(Lcom/cronutils/model/field/value/IntegerFieldValue;)V
    .locals 2

    .line 35
    new-instance v0, Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->NONE:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-direct {v0, v1}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;-><init>(Lcom/cronutils/model/field/value/SpecialChar;)V

    invoke-direct {p0, p1, v0}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;Lcom/cronutils/model/field/value/SpecialCharFieldValue;)V

    return-void
.end method

.method public constructor <init>(Lcom/cronutils/model/field/value/IntegerFieldValue;Lcom/cronutils/model/field/value/SpecialCharFieldValue;)V
    .locals 2

    .line 39
    new-instance v0, Lcom/cronutils/model/field/value/IntegerFieldValue;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {p0, p1, p2, v0}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;Lcom/cronutils/model/field/value/SpecialCharFieldValue;Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    .line 40
    invoke-virtual {p2}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;->getValue()Lcom/cronutils/model/field/value/SpecialChar;

    move-result-object p1

    sget-object p2, Lcom/cronutils/model/field/value/SpecialChar;->HASH:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-virtual {p1, p2}, Lcom/cronutils/model/field/value/SpecialChar;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 41
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo p2, "value missing for a#b cron expression"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Lcom/cronutils/model/field/value/IntegerFieldValue;Lcom/cronutils/model/field/value/SpecialCharFieldValue;Lcom/cronutils/model/field/value/IntegerFieldValue;)V
    .locals 3

    .line 45
    invoke-direct {p0}, Lcom/cronutils/model/field/expression/FieldExpression;-><init>()V

    const/4 v0, 0x0

    .line 46
    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "time must not be null"

    invoke-static {p1, v2, v1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "special char must not null"

    invoke-static {p2, v2, v1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "nth value must not be null"

    invoke-static {p3, v1, v0}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    iput-object p1, p0, Lcom/cronutils/model/field/expression/On;->time:Lcom/cronutils/model/field/value/IntegerFieldValue;

    .line 50
    iput-object p2, p0, Lcom/cronutils/model/field/expression/On;->specialChar:Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    .line 51
    iput-object p3, p0, Lcom/cronutils/model/field/expression/On;->nth:Lcom/cronutils/model/field/value/IntegerFieldValue;

    return-void
.end method

.method public constructor <init>(Lcom/cronutils/model/field/value/SpecialCharFieldValue;)V
    .locals 2

    .line 31
    new-instance v0, Lcom/cronutils/model/field/value/IntegerFieldValue;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {p0, v0, p1}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;Lcom/cronutils/model/field/value/SpecialCharFieldValue;)V

    return-void
.end method

.method private isDefault(Lcom/cronutils/model/field/value/IntegerFieldValue;)Z
    .locals 1

    .line 91
    invoke-virtual {p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public asString()Ljava/lang/String;
    .locals 4

    .line 68
    sget-object v0, Lcom/cronutils/model/field/expression/On$1;->$SwitchMap$com$cronutils$model$field$value$SpecialChar:[I

    iget-object v1, p0, Lcom/cronutils/model/field/expression/On;->specialChar:Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    invoke-virtual {v1}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;->getValue()Lcom/cronutils/model/field/value/SpecialChar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cronutils/model/field/value/SpecialChar;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-eq v0, v2, :cond_4

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_0

    .line 86
    iget-object v0, p0, Lcom/cronutils/model/field/expression/On;->specialChar:Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    invoke-virtual {v0}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/cronutils/model/field/expression/On;->isDefault(Lcom/cronutils/model/field/value/IntegerFieldValue;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "L"

    return-object v0

    .line 83
    :cond_1
    new-array v0, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "%sL"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 74
    :cond_2
    invoke-virtual {p0}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/cronutils/model/field/expression/On;->isDefault(Lcom/cronutils/model/field/value/IntegerFieldValue;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "W"

    return-object v0

    .line 77
    :cond_3
    new-array v0, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "%sW"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 72
    :cond_4
    new-array v0, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v2

    aput-object v2, v0, v3

    invoke-virtual {p0}, Lcom/cronutils/model/field/expression/On;->getNth()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "%s#%s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 70
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNth()Lcom/cronutils/model/field/value/IntegerFieldValue;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/cronutils/model/field/expression/On;->nth:Lcom/cronutils/model/field/value/IntegerFieldValue;

    return-object v0
.end method

.method public getSpecialChar()Lcom/cronutils/model/field/value/SpecialCharFieldValue;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/cronutils/model/field/expression/On;->specialChar:Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    return-object v0
.end method

.method public getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/cronutils/model/field/expression/On;->time:Lcom/cronutils/model/field/value/IntegerFieldValue;

    return-object v0
.end method
