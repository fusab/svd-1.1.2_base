.class public Lcom/cronutils/model/field/expression/Every;
.super Lcom/cronutils/model/field/expression/FieldExpression;
.source "Every.java"


# instance fields
.field private expression:Lcom/cronutils/model/field/expression/FieldExpression;

.field private period:Lcom/cronutils/model/field/value/IntegerFieldValue;


# direct methods
.method private constructor <init>(Lcom/cronutils/model/field/expression/Every;)V
    .locals 1

    .line 40
    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Every;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/Every;->getPeriod()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/cronutils/model/field/expression/Every;-><init>(Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-void
.end method

.method public constructor <init>(Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/value/IntegerFieldValue;)V
    .locals 2

    .line 31
    invoke-direct {p0}, Lcom/cronutils/model/field/expression/FieldExpression;-><init>()V

    const/4 v0, 0x0

    .line 32
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Expression must not be null"

    invoke-static {p1, v1, v0}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/expression/FieldExpression;

    iput-object p1, p0, Lcom/cronutils/model/field/expression/Every;->expression:Lcom/cronutils/model/field/expression/FieldExpression;

    if-nez p2, :cond_0

    .line 34
    new-instance p2, Lcom/cronutils/model/field/value/IntegerFieldValue;

    const/4 p1, 0x1

    invoke-direct {p2, p1}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    .line 36
    :cond_0
    iput-object p2, p0, Lcom/cronutils/model/field/expression/Every;->period:Lcom/cronutils/model/field/value/IntegerFieldValue;

    return-void
.end method

.method public constructor <init>(Lcom/cronutils/model/field/value/IntegerFieldValue;)V
    .locals 1

    .line 28
    new-instance v0, Lcom/cronutils/model/field/expression/Always;

    invoke-direct {v0}, Lcom/cronutils/model/field/expression/Always;-><init>()V

    invoke-direct {p0, v0, p1}, Lcom/cronutils/model/field/expression/Every;-><init>(Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    return-void
.end method


# virtual methods
.method public asString()Ljava/lang/String;
    .locals 4

    .line 53
    iget-object v0, p0, Lcom/cronutils/model/field/expression/Every;->period:Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-virtual {v0}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 54
    iget-object v0, p0, Lcom/cronutils/model/field/expression/Every;->expression:Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-virtual {v0}, Lcom/cronutils/model/field/expression/FieldExpression;->asString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cronutils/model/field/expression/Every;->expression:Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-virtual {v0}, Lcom/cronutils/model/field/expression/FieldExpression;->asString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x2

    .line 56
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/cronutils/model/field/expression/Every;->expression:Lcom/cronutils/model/field/expression/FieldExpression;

    invoke-virtual {v3}, Lcom/cronutils/model/field/expression/FieldExpression;->asString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-virtual {p0}, Lcom/cronutils/model/field/expression/Every;->getPeriod()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "%s/%s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExpression()Lcom/cronutils/model/field/expression/FieldExpression;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/cronutils/model/field/expression/Every;->expression:Lcom/cronutils/model/field/expression/FieldExpression;

    return-object v0
.end method

.method public getPeriod()Lcom/cronutils/model/field/value/IntegerFieldValue;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/cronutils/model/field/expression/Every;->period:Lcom/cronutils/model/field/value/IntegerFieldValue;

    return-object v0
.end method
