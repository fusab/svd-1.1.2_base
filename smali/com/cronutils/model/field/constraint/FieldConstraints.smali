.class public Lcom/cronutils/model/field/constraint/FieldConstraints;
.super Ljava/lang/Object;
.source "FieldConstraints.java"


# instance fields
.field private endRange:I

.field private intMapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private specialChars:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/cronutils/model/field/value/SpecialChar;",
            ">;"
        }
    .end annotation
.end field

.field private startRange:I

.field private stringMapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;II)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/cronutils/model/field/value/SpecialChar;",
            ">;II)V"
        }
    .end annotation

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 52
    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "String mapping must not be null"

    invoke-static {p1, v2, v1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/model/field/constraint/FieldConstraints;->stringMapping:Ljava/util/Map;

    .line 53
    new-array p1, v0, [Ljava/lang/Object;

    const-string v1, "Integer mapping must not be null"

    invoke-static {p2, v1, p1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/model/field/constraint/FieldConstraints;->intMapping:Ljava/util/Map;

    .line 54
    new-array p1, v0, [Ljava/lang/Object;

    const-string p2, "Special (non-standard) chars set must not be null"

    invoke-static {p3, p2, p1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Set;

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/model/field/constraint/FieldConstraints;->specialChars:Ljava/util/Set;

    .line 55
    iput p4, p0, Lcom/cronutils/model/field/constraint/FieldConstraints;->startRange:I

    .line 56
    iput p5, p0, Lcom/cronutils/model/field/constraint/FieldConstraints;->endRange:I

    return-void
.end method


# virtual methods
.method public getEndRange()I
    .locals 1

    .line 64
    iget v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraints;->endRange:I

    return v0
.end method

.method public getIntMapping()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraints;->intMapping:Ljava/util/Map;

    return-object v0
.end method

.method public getSpecialChars()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/cronutils/model/field/value/SpecialChar;",
            ">;"
        }
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraints;->specialChars:Ljava/util/Set;

    return-object v0
.end method

.method public getStartRange()I
    .locals 1

    .line 60
    iget v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraints;->startRange:I

    return v0
.end method

.method public getStringMapping()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraints;->stringMapping:Ljava/util/Map;

    return-object v0
.end method

.method public isInRange(I)Z
    .locals 1

    .line 76
    invoke-virtual {p0}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getStartRange()I

    move-result v0

    if-lt p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getEndRange()I

    move-result v0

    if-gt p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
