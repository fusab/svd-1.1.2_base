.class public Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;
.super Ljava/lang/Object;
.source "FieldConstraintsBuilder.java"


# instance fields
.field private endRange:I

.field private intMapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private specialChars:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/cronutils/model/field/value/SpecialChar;",
            ">;"
        }
    .end annotation
.end field

.field private startRange:I

.field private stringMapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->stringMapping:Ljava/util/Map;

    .line 39
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->intMapping:Ljava/util/Map;

    const/4 v0, 0x0

    .line 40
    iput v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->startRange:I

    const v0, 0x7fffffff

    .line 41
    iput v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->endRange:I

    .line 42
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->specialChars:Ljava/util/Set;

    .line 43
    iget-object v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->specialChars:Ljava/util/Set;

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->NONE:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private static daysOfWeekMapping()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 180
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    const/4 v1, 0x1

    .line 181
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "MON"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x2

    .line 182
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "TUE"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x3

    .line 183
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "WED"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x4

    .line 184
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "THU"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x5

    .line 185
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "FRI"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x6

    .line 186
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "SAT"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x7

    .line 187
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "SUN"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public static instance()Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;
    .locals 1

    .line 218
    new-instance v0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    invoke-direct {v0}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;-><init>()V

    return-object v0
.end method

.method private static monthsMapping()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 197
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    const/4 v1, 0x1

    .line 198
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "JAN"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x2

    .line 199
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "FEB"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x3

    .line 200
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "MAR"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x4

    .line 201
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "APR"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x5

    .line 202
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "MAY"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x6

    .line 203
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "JUN"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x7

    .line 204
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "JUL"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x8

    .line 205
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "AUG"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x9

    .line 206
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "SEP"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0xa

    .line 207
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "OCT"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0xb

    .line 208
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "NOV"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0xc

    .line 209
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "DEC"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public addHashSupport()Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;
    .locals 2

    .line 83
    iget-object v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->specialChars:Ljava/util/Set;

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->HASH:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addLSupport()Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->specialChars:Ljava/util/Set;

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->L:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addLWSupport()Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->specialChars:Ljava/util/Set;

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->LW:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addQuestionMarkSupport()Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->specialChars:Ljava/util/Set;

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->QUESTION_MARK:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addWSupport()Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->specialChars:Ljava/util/Set;

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->W:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public createConstraintsInstance()Lcom/cronutils/model/field/constraint/FieldConstraints;
    .locals 7

    .line 171
    new-instance v6, Lcom/cronutils/model/field/constraint/FieldConstraints;

    iget-object v1, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->stringMapping:Ljava/util/Map;

    iget-object v2, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->intMapping:Ljava/util/Map;

    iget-object v3, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->specialChars:Ljava/util/Set;

    iget v4, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->startRange:I

    iget v5, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->endRange:I

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/cronutils/model/field/constraint/FieldConstraints;-><init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;II)V

    return-object v6
.end method

.method public forField(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;
    .locals 1

    .line 52
    sget-object v0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder$1;->$SwitchMap$com$cronutils$model$field$CronFieldName:[I

    invoke-virtual {p1}, Lcom/cronutils/model/field/CronFieldName;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    packed-switch p1, :pswitch_data_0

    return-object p0

    .line 69
    :pswitch_0
    invoke-static {}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->monthsMapping()Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->stringMapping:Ljava/util/Map;

    .line 70
    iput v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->startRange:I

    const/16 p1, 0xc

    .line 71
    iput p1, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->endRange:I

    return-object p0

    .line 65
    :pswitch_1
    iput v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->startRange:I

    const/16 p1, 0x1f

    .line 66
    iput p1, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->endRange:I

    return-object p0

    .line 61
    :pswitch_2
    invoke-static {}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->daysOfWeekMapping()Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->stringMapping:Ljava/util/Map;

    const/4 p1, 0x6

    .line 62
    iput p1, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->endRange:I

    return-object p0

    :pswitch_3
    const/16 p1, 0x17

    .line 58
    iput p1, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->endRange:I

    return-object p0

    :pswitch_4
    const/16 p1, 0x3b

    .line 55
    iput p1, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->endRange:I

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public withIntValueMapping(II)Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->intMapping:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public withShiftedStringMapping(I)Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;
    .locals 5

    .line 152
    iget-object v0, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->stringMapping:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 153
    iget-object v2, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->stringMapping:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 154
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, p1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 155
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v4, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->endRange:I

    if-le v3, v4, :cond_0

    .line 156
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget v3, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->endRange:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 158
    :cond_0
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v4, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->startRange:I

    if-ge v3, v4, :cond_1

    .line 159
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget v3, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->startRange:I

    iget v4, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->endRange:I

    sub-int/2addr v3, v4

    add-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 161
    :cond_1
    iget-object v3, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->stringMapping:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-object p0
.end method

.method public withValidRange(II)Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;
    .locals 0

    .line 141
    iput p1, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->startRange:I

    .line 142
    iput p2, p0, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->endRange:I

    return-object p0
.end method
