.class public Lcom/cronutils/model/definition/CronDefinitionBuilder;
.super Ljava/lang/Object;
.source "CronDefinitionBuilder.java"


# instance fields
.field private cronConstraints:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/cronutils/model/definition/CronConstraint;",
            ">;"
        }
    .end annotation
.end field

.field private enforceStrictRanges:Z

.field private fields:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/cronutils/model/field/CronFieldName;",
            "Lcom/cronutils/model/field/definition/FieldDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private lastFieldOptional:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/cronutils/model/definition/CronDefinitionBuilder;->fields:Ljava/util/Map;

    .line 45
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/cronutils/model/definition/CronDefinitionBuilder;->cronConstraints:Ljava/util/Set;

    const/4 v0, 0x0

    .line 46
    iput-boolean v0, p0, Lcom/cronutils/model/definition/CronDefinitionBuilder;->lastFieldOptional:Z

    .line 47
    iput-boolean v0, p0, Lcom/cronutils/model/definition/CronDefinitionBuilder;->enforceStrictRanges:Z

    return-void
.end method

.method private static cron4j()Lcom/cronutils/model/definition/CronDefinition;
    .locals 3

    .line 164
    invoke-static {}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->defineCron()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withMinutes()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withHours()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withDayOfMonth()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withMonth()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 169
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withDayOfWeek()Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->withValidRange(II)Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->withMondayDoWValue(I)Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 170
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->enforceStrictRanges()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 171
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->instance()Lcom/cronutils/model/definition/CronDefinition;

    move-result-object v0

    return-object v0
.end method

.method public static defineCron()Lcom/cronutils/model/definition/CronDefinitionBuilder;
    .locals 1

    .line 55
    new-instance v0, Lcom/cronutils/model/definition/CronDefinitionBuilder;

    invoke-direct {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;-><init>()V

    return-object v0
.end method

.method public static instanceDefinitionFor(Lcom/cronutils/model/CronType;)Lcom/cronutils/model/definition/CronDefinition;
    .locals 3

    .line 225
    sget-object v0, Lcom/cronutils/model/definition/CronDefinitionBuilder$2;->$SwitchMap$com$cronutils$model$CronType:[I

    invoke-virtual {p0}, Lcom/cronutils/model/CronType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    .line 231
    invoke-static {}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->unixCrontab()Lcom/cronutils/model/definition/CronDefinition;

    move-result-object p0

    return-object p0

    .line 233
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const-string p0, "No cron definition found for %s"

    invoke-static {p0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229
    :cond_1
    invoke-static {}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->quartz()Lcom/cronutils/model/definition/CronDefinition;

    move-result-object p0

    return-object p0

    .line 227
    :cond_2
    invoke-static {}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->cron4j()Lcom/cronutils/model/definition/CronDefinition;

    move-result-object p0

    return-object p0
.end method

.method private static quartz()Lcom/cronutils/model/definition/CronDefinition;
    .locals 3

    .line 179
    invoke-static {}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->defineCron()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 180
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withSeconds()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withMinutes()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 182
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withHours()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withDayOfMonth()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->supportsHash()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->supportsL()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->supportsW()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->supportsLW()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->supportsQuestionMark()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 184
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withMonth()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 185
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withDayOfWeek()Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->withValidRange(II)Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->withMondayDoWValue(I)Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->supportsHash()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->supportsL()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->supportsW()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->supportsQuestionMark()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 186
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withYear()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    const/16 v1, 0x7b2

    const/16 v2, 0x833

    invoke-virtual {v0, v1, v2}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->withValidRange(II)Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 187
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->lastFieldOptional()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    new-instance v1, Lcom/cronutils/model/definition/CronDefinitionBuilder$1;

    const-string v2, "Both, a day-of-week AND a day-of-month parameter, are not supported."

    invoke-direct {v1, v2}, Lcom/cronutils/model/definition/CronDefinitionBuilder$1;-><init>(Ljava/lang/String;)V

    .line 188
    invoke-virtual {v0, v1}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withCronValidation(Lcom/cronutils/model/definition/CronConstraint;)Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 201
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->instance()Lcom/cronutils/model/definition/CronDefinition;

    move-result-object v0

    return-object v0
.end method

.method private static unixCrontab()Lcom/cronutils/model/definition/CronDefinition;
    .locals 4

    .line 209
    invoke-static {}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->defineCron()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 210
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withMinutes()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 211
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withHours()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 212
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withDayOfMonth()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 213
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withMonth()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 214
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withDayOfWeek()Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    move-result-object v0

    const/4 v1, 0x7

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->withValidRange(II)Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->withMondayDoWValue(I)Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->withIntMapping(II)Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 215
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->enforceStrictRanges()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 216
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->instance()Lcom/cronutils/model/definition/CronDefinition;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public enforceStrictRanges()Lcom/cronutils/model/definition/CronDefinitionBuilder;
    .locals 1

    const/4 v0, 0x1

    .line 128
    iput-boolean v0, p0, Lcom/cronutils/model/definition/CronDefinitionBuilder;->enforceStrictRanges:Z

    return-object p0
.end method

.method public instance()Lcom/cronutils/model/definition/CronDefinition;
    .locals 5

    .line 154
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 155
    iget-object v1, p0, Lcom/cronutils/model/definition/CronDefinitionBuilder;->cronConstraints:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 156
    new-instance v1, Lcom/cronutils/model/definition/CronDefinition;

    iget-object v2, p0, Lcom/cronutils/model/definition/CronDefinitionBuilder;->fields:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    iget-boolean v3, p0, Lcom/cronutils/model/definition/CronDefinitionBuilder;->lastFieldOptional:Z

    iget-boolean v4, p0, Lcom/cronutils/model/definition/CronDefinitionBuilder;->enforceStrictRanges:Z

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/cronutils/model/definition/CronDefinition;-><init>(Ljava/util/List;Ljava/util/Set;ZZ)V

    return-object v1
.end method

.method public lastFieldOptional()Lcom/cronutils/model/definition/CronDefinitionBuilder;
    .locals 1

    const/4 v0, 0x1

    .line 119
    iput-boolean v0, p0, Lcom/cronutils/model/definition/CronDefinitionBuilder;->lastFieldOptional:Z

    return-object p0
.end method

.method public register(Lcom/cronutils/model/field/definition/FieldDefinition;)V
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/cronutils/model/definition/CronDefinitionBuilder;->fields:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/cronutils/model/field/definition/FieldDefinition;->getFieldName()Lcom/cronutils/model/field/CronFieldName;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public withCronValidation(Lcom/cronutils/model/definition/CronConstraint;)Lcom/cronutils/model/definition/CronDefinitionBuilder;
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/cronutils/model/definition/CronDefinitionBuilder;->cronConstraints:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public withDayOfMonth()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;
    .locals 2

    .line 87
    new-instance v0, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {v0, p0, v1}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;-><init>(Lcom/cronutils/model/definition/CronDefinitionBuilder;Lcom/cronutils/model/field/CronFieldName;)V

    return-object v0
.end method

.method public withDayOfWeek()Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;
    .locals 2

    .line 103
    new-instance v0, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {v0, p0, v1}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;-><init>(Lcom/cronutils/model/definition/CronDefinitionBuilder;Lcom/cronutils/model/field/CronFieldName;)V

    return-object v0
.end method

.method public withHours()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;
    .locals 2

    .line 79
    new-instance v0, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->HOUR:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {v0, p0, v1}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;-><init>(Lcom/cronutils/model/definition/CronDefinitionBuilder;Lcom/cronutils/model/field/CronFieldName;)V

    return-object v0
.end method

.method public withMinutes()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;
    .locals 2

    .line 71
    new-instance v0, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->MINUTE:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {v0, p0, v1}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;-><init>(Lcom/cronutils/model/definition/CronDefinitionBuilder;Lcom/cronutils/model/field/CronFieldName;)V

    return-object v0
.end method

.method public withMonth()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;
    .locals 2

    .line 95
    new-instance v0, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->MONTH:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {v0, p0, v1}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;-><init>(Lcom/cronutils/model/definition/CronDefinitionBuilder;Lcom/cronutils/model/field/CronFieldName;)V

    return-object v0
.end method

.method public withSeconds()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;
    .locals 2

    .line 63
    new-instance v0, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->SECOND:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {v0, p0, v1}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;-><init>(Lcom/cronutils/model/definition/CronDefinitionBuilder;Lcom/cronutils/model/field/CronFieldName;)V

    return-object v0
.end method

.method public withYear()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;
    .locals 2

    .line 111
    new-instance v0, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->YEAR:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {v0, p0, v1}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;-><init>(Lcom/cronutils/model/definition/CronDefinitionBuilder;Lcom/cronutils/model/field/CronFieldName;)V

    return-object v0
.end method
