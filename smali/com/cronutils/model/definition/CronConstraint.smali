.class public abstract Lcom/cronutils/model/definition/CronConstraint;
.super Ljava/lang/Object;
.source "CronConstraint.java"


# instance fields
.field private description:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/cronutils/model/definition/CronConstraint;->description:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/cronutils/model/definition/CronConstraint;->description:Ljava/lang/String;

    return-object v0
.end method

.method public abstract validate(Lcom/cronutils/model/Cron;)Z
.end method
