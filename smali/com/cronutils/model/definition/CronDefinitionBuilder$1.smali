.class final Lcom/cronutils/model/definition/CronDefinitionBuilder$1;
.super Lcom/cronutils/model/definition/CronConstraint;
.source "CronDefinitionBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/model/definition/CronDefinitionBuilder;->quartz()Lcom/cronutils/model/definition/CronDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 191
    invoke-direct {p0, p1}, Lcom/cronutils/model/definition/CronConstraint;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public validate(Lcom/cronutils/model/Cron;)Z
    .locals 1

    .line 194
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p1, v0}, Lcom/cronutils/model/Cron;->retrieve(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/CronField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    instance-of v0, v0, Lcom/cronutils/model/field/expression/QuestionMark;

    if-nez v0, :cond_0

    .line 195
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p1, v0}, Lcom/cronutils/model/Cron;->retrieve(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/CronField;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    instance-of p1, p1, Lcom/cronutils/model/field/expression/QuestionMark;

    return p1

    .line 197
    :cond_0
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p1, v0}, Lcom/cronutils/model/Cron;->retrieve(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/CronField;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    instance-of p1, p1, Lcom/cronutils/model/field/expression/QuestionMark;

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
