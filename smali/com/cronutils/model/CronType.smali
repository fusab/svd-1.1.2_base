.class public final enum Lcom/cronutils/model/CronType;
.super Ljava/lang/Enum;
.source "CronType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/cronutils/model/CronType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cronutils/model/CronType;

.field public static final enum CRON4J:Lcom/cronutils/model/CronType;

.field public static final enum QUARTZ:Lcom/cronutils/model/CronType;

.field public static final enum UNIX:Lcom/cronutils/model/CronType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 20
    new-instance v0, Lcom/cronutils/model/CronType;

    const/4 v1, 0x0

    const-string v2, "CRON4J"

    invoke-direct {v0, v2, v1}, Lcom/cronutils/model/CronType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cronutils/model/CronType;->CRON4J:Lcom/cronutils/model/CronType;

    new-instance v0, Lcom/cronutils/model/CronType;

    const/4 v2, 0x1

    const-string v3, "QUARTZ"

    invoke-direct {v0, v3, v2}, Lcom/cronutils/model/CronType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cronutils/model/CronType;->QUARTZ:Lcom/cronutils/model/CronType;

    new-instance v0, Lcom/cronutils/model/CronType;

    const/4 v3, 0x2

    const-string v4, "UNIX"

    invoke-direct {v0, v4, v3}, Lcom/cronutils/model/CronType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cronutils/model/CronType;->UNIX:Lcom/cronutils/model/CronType;

    const/4 v0, 0x3

    .line 19
    new-array v0, v0, [Lcom/cronutils/model/CronType;

    sget-object v4, Lcom/cronutils/model/CronType;->CRON4J:Lcom/cronutils/model/CronType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/cronutils/model/CronType;->QUARTZ:Lcom/cronutils/model/CronType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/cronutils/model/CronType;->UNIX:Lcom/cronutils/model/CronType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/cronutils/model/CronType;->$VALUES:[Lcom/cronutils/model/CronType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cronutils/model/CronType;
    .locals 1

    .line 19
    const-class v0, Lcom/cronutils/model/CronType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/cronutils/model/CronType;

    return-object p0
.end method

.method public static values()[Lcom/cronutils/model/CronType;
    .locals 1

    .line 19
    sget-object v0, Lcom/cronutils/model/CronType;->$VALUES:[Lcom/cronutils/model/CronType;

    invoke-virtual {v0}, [Lcom/cronutils/model/CronType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cronutils/model/CronType;

    return-object v0
.end method
