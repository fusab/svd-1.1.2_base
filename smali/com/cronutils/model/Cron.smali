.class public Lcom/cronutils/model/Cron;
.super Ljava/lang/Object;
.source "Cron.java"


# instance fields
.field private asString:Ljava/lang/String;

.field private cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

.field private fields:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/cronutils/model/field/CronFieldName;",
            "Lcom/cronutils/model/field/CronField;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/cronutils/model/definition/CronDefinition;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cronutils/model/definition/CronDefinition;",
            "Ljava/util/List<",
            "Lcom/cronutils/model/field/CronField;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 41
    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "CronDefinition must not be null"

    invoke-static {p1, v2, v1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/definition/CronDefinition;

    iput-object p1, p0, Lcom/cronutils/model/Cron;->cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

    .line 42
    new-array p1, v0, [Ljava/lang/Object;

    const-string v0, "CronFields cannot be null"

    invoke-static {p2, v0, p1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/model/Cron;->fields:Ljava/util/Map;

    .line 44
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/cronutils/model/field/CronField;

    .line 45
    iget-object v0, p0, Lcom/cronutils/model/Cron;->fields:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/cronutils/model/field/CronField;->getField()Lcom/cronutils/model/field/CronFieldName;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public asString()Ljava/lang/String;
    .locals 6

    .line 68
    iget-object v0, p0, Lcom/cronutils/model/Cron;->asString:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/cronutils/model/Cron;->fields:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 70
    invoke-static {}, Lcom/cronutils/model/field/CronField;->createFieldComparator()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 71
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 72
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    const/4 v4, 0x1

    .line 73
    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cronutils/model/field/CronField;

    invoke-virtual {v5}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v5

    invoke-virtual {v5}, Lcom/cronutils/model/field/expression/FieldExpression;->asString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    const-string v5, "%s "

    invoke-static {v5, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 75
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cronutils/model/Cron;->asString:Ljava/lang/String;

    .line 77
    :cond_1
    iget-object v0, p0, Lcom/cronutils/model/Cron;->asString:Ljava/lang/String;

    return-object v0
.end method

.method public equivalent(Lcom/cronutils/mapper/CronMapper;Lcom/cronutils/model/Cron;)Z
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/cronutils/model/Cron;->asString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2}, Lcom/cronutils/mapper/CronMapper;->map(Lcom/cronutils/model/Cron;)Lcom/cronutils/model/Cron;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/Cron;->asString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public equivalent(Lcom/cronutils/model/Cron;)Z
    .locals 1

    .line 116
    invoke-virtual {p0}, Lcom/cronutils/model/Cron;->asString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/cronutils/model/Cron;->asString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getCronDefinition()Lcom/cronutils/model/definition/CronDefinition;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/cronutils/model/Cron;->cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

    return-object v0
.end method

.method public retrieve(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/CronField;
    .locals 3

    .line 56
    iget-object v0, p0, Lcom/cronutils/model/Cron;->fields:Ljava/util/Map;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "CronFieldName must not be null"

    invoke-static {p1, v2, v1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/CronField;

    return-object p1
.end method

.method public retrieveFieldsAsMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/cronutils/model/field/CronFieldName;",
            "Lcom/cronutils/model/field/CronField;",
            ">;"
        }
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/cronutils/model/Cron;->fields:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public validate()Lcom/cronutils/model/Cron;
    .locals 5

    .line 85
    invoke-virtual {p0}, Lcom/cronutils/model/Cron;->retrieveFieldsAsMap()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 86
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cronutils/model/field/CronFieldName;

    .line 87
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v1

    new-instance v3, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;

    .line 88
    invoke-virtual {p0}, Lcom/cronutils/model/Cron;->getCronDefinition()Lcom/cronutils/model/definition/CronDefinition;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/cronutils/model/definition/CronDefinition;->getFieldDefinition(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/definition/FieldDefinition;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cronutils/model/field/definition/FieldDefinition;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v2

    iget-object v4, p0, Lcom/cronutils/model/Cron;->cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

    invoke-virtual {v4}, Lcom/cronutils/model/definition/CronDefinition;->isStrictRanges()Z

    move-result v4

    invoke-direct {v3, v2, v4}, Lcom/cronutils/model/field/expression/visitor/ValidationFieldExpressionVisitor;-><init>(Lcom/cronutils/model/field/constraint/FieldConstraints;Z)V

    .line 87
    invoke-virtual {v1, v3}, Lcom/cronutils/model/field/expression/FieldExpression;->accept(Lcom/cronutils/model/field/expression/visitor/FieldExpressionVisitor;)Lcom/cronutils/model/field/expression/FieldExpression;

    goto :goto_0

    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/cronutils/model/Cron;->getCronDefinition()Lcom/cronutils/model/definition/CronDefinition;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinition;->getCronConstraints()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cronutils/model/definition/CronConstraint;

    .line 92
    invoke-virtual {v1, p0}, Lcom/cronutils/model/definition/CronConstraint;->validate(Lcom/cronutils/model/Cron;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    .line 93
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/cronutils/model/Cron;->asString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v1}, Lcom/cronutils/model/definition/CronConstraint;->getDescription()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    const-string v1, "Invalid cron expression: %s. %s"

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object p0
.end method
