.class Lcom/cronutils/model/time/ExecutionTimeBuilder;
.super Ljava/lang/Object;
.source "ExecutionTimeBuilder.java"


# instance fields
.field private cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

.field private daysOfMonthCronField:Lcom/cronutils/model/field/CronField;

.field private daysOfWeekCronField:Lcom/cronutils/model/field/CronField;

.field private hours:Lcom/cronutils/model/time/TimeNode;

.field private minutes:Lcom/cronutils/model/time/TimeNode;

.field private months:Lcom/cronutils/model/time/TimeNode;

.field private seconds:Lcom/cronutils/model/time/TimeNode;

.field private yearsValueGenerator:Lcom/cronutils/model/time/generator/FieldValueGenerator;


# direct methods
.method constructor <init>(Lcom/cronutils/model/definition/CronDefinition;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

    return-void
.end method

.method private getConstraint(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/constraint/FieldConstraints;
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

    invoke-virtual {v0, p1}, Lcom/cronutils/model/definition/CronDefinition;->getFieldDefinition(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/definition/FieldDefinition;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

    invoke-virtual {v0, p1}, Lcom/cronutils/model/definition/CronDefinition;->getFieldDefinition(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/definition/FieldDefinition;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/definition/FieldDefinition;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->instance()Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->forField(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/constraint/FieldConstraintsBuilder;->createConstraintsInstance()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private timeNodeAlways(Lcom/cronutils/model/field/CronFieldName;II)Lcom/cronutils/model/time/TimeNode;
    .locals 4

    .line 145
    new-instance v0, Lcom/cronutils/model/time/TimeNode;

    new-instance v1, Lcom/cronutils/model/field/CronField;

    new-instance v2, Lcom/cronutils/model/field/expression/Always;

    invoke-direct {v2}, Lcom/cronutils/model/field/expression/Always;-><init>()V

    .line 147
    invoke-direct {p0, p1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->getConstraint(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v3

    invoke-direct {v1, p1, v2, v3}, Lcom/cronutils/model/field/CronField;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    .line 146
    invoke-static {v1}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->forCronField(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p1

    .line 148
    invoke-virtual {p1, p2, p3}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/cronutils/model/time/TimeNode;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method private timeNodeLowest(Lcom/cronutils/model/field/CronFieldName;II)Lcom/cronutils/model/time/TimeNode;
    .locals 5

    .line 137
    invoke-direct {p0, p1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->getConstraint(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v0

    .line 138
    new-instance v1, Lcom/cronutils/model/time/TimeNode;

    new-instance v2, Lcom/cronutils/model/field/CronField;

    new-instance v3, Lcom/cronutils/model/field/expression/On;

    new-instance v4, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-direct {v4, p2}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {v3, v4}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    invoke-direct {v2, p1, v3, v0}, Lcom/cronutils/model/field/CronField;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    .line 139
    invoke-static {v2}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->forCronField(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p1

    .line 141
    invoke-virtual {p1, p2, p3}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/cronutils/model/time/TimeNode;-><init>(Ljava/util/List;)V

    return-object v1
.end method

.method private validate(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/CronField;)V
    .locals 4

    const/4 v0, 0x0

    .line 152
    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "Reference CronFieldName cannot be null"

    invoke-static {p1, v2, v1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    invoke-virtual {p2}, Lcom/cronutils/model/field/CronField;->getField()Lcom/cronutils/model/field/CronFieldName;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Object;

    const-string v3, "CronField\'s CronFieldName cannot be null"

    invoke-static {v1, v3, v2}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    invoke-virtual {p2}, Lcom/cronutils/model/field/CronField;->getField()Lcom/cronutils/model/field/CronFieldName;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/cronutils/model/field/CronFieldName;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 155
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 156
    invoke-virtual {p2}, Lcom/cronutils/model/field/CronField;->getField()Lcom/cronutils/model/field/CronFieldName;

    move-result-object p2

    aput-object p2, v2, v0

    const/4 p2, 0x1

    aput-object p1, v2, p2

    const-string p1, "Invalid argument! Expected CronField instance for field %s but found %s"

    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method build()Lcom/cronutils/model/time/ExecutionTime;
    .locals 10

    .line 89
    iget-object v0, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->seconds:Lcom/cronutils/model/time/TimeNode;

    const/16 v1, 0x3b

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v0, :cond_0

    .line 90
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->SECOND:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0, v0, v3, v1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->timeNodeLowest(Lcom/cronutils/model/field/CronFieldName;II)Lcom/cronutils/model/time/TimeNode;

    move-result-object v0

    iput-object v0, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->seconds:Lcom/cronutils/model/time/TimeNode;

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 94
    :goto_0
    iget-object v4, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->minutes:Lcom/cronutils/model/time/TimeNode;

    if-nez v4, :cond_2

    if-eqz v0, :cond_1

    .line 95
    sget-object v4, Lcom/cronutils/model/field/CronFieldName;->MINUTE:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0, v4, v3, v1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->timeNodeAlways(Lcom/cronutils/model/field/CronFieldName;II)Lcom/cronutils/model/time/TimeNode;

    move-result-object v1

    goto :goto_1

    :cond_1
    sget-object v4, Lcom/cronutils/model/field/CronFieldName;->MINUTE:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0, v4, v3, v1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->timeNodeLowest(Lcom/cronutils/model/field/CronFieldName;II)Lcom/cronutils/model/time/TimeNode;

    move-result-object v1

    :goto_1
    iput-object v1, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->minutes:Lcom/cronutils/model/time/TimeNode;

    goto :goto_2

    :cond_2
    const/4 v0, 0x1

    .line 99
    :goto_2
    iget-object v1, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->hours:Lcom/cronutils/model/time/TimeNode;

    if-nez v1, :cond_4

    const/16 v1, 0x17

    if-eqz v0, :cond_3

    .line 100
    sget-object v4, Lcom/cronutils/model/field/CronFieldName;->HOUR:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0, v4, v3, v1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->timeNodeAlways(Lcom/cronutils/model/field/CronFieldName;II)Lcom/cronutils/model/time/TimeNode;

    move-result-object v1

    goto :goto_3

    :cond_3
    sget-object v4, Lcom/cronutils/model/field/CronFieldName;->HOUR:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0, v4, v3, v1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->timeNodeLowest(Lcom/cronutils/model/field/CronFieldName;II)Lcom/cronutils/model/time/TimeNode;

    move-result-object v1

    :goto_3
    iput-object v1, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->hours:Lcom/cronutils/model/time/TimeNode;

    goto :goto_4

    :cond_4
    const/4 v0, 0x1

    .line 104
    :goto_4
    iget-object v1, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->daysOfMonthCronField:Lcom/cronutils/model/field/CronField;

    if-nez v1, :cond_6

    .line 105
    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0, v1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->getConstraint(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v1

    if-eqz v0, :cond_5

    .line 106
    new-instance v4, Lcom/cronutils/model/field/CronField;

    sget-object v5, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    new-instance v6, Lcom/cronutils/model/field/expression/Always;

    invoke-direct {v6}, Lcom/cronutils/model/field/expression/Always;-><init>()V

    invoke-direct {v4, v5, v6, v1}, Lcom/cronutils/model/field/CronField;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    goto :goto_5

    :cond_5
    new-instance v4, Lcom/cronutils/model/field/CronField;

    sget-object v5, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    new-instance v6, Lcom/cronutils/model/field/expression/On;

    new-instance v7, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-direct {v7, v3}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {v6, v7}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    invoke-direct {v4, v5, v6, v1}, Lcom/cronutils/model/field/CronField;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    :goto_5
    iput-object v4, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->daysOfMonthCronField:Lcom/cronutils/model/field/CronField;

    goto :goto_6

    :cond_6
    const/4 v0, 0x1

    .line 112
    :goto_6
    iget-object v1, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->daysOfWeekCronField:Lcom/cronutils/model/field/CronField;

    if-nez v1, :cond_8

    .line 113
    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0, v1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->getConstraint(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v1

    if-eqz v0, :cond_7

    .line 114
    new-instance v2, Lcom/cronutils/model/field/CronField;

    sget-object v4, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    new-instance v5, Lcom/cronutils/model/field/expression/Always;

    invoke-direct {v5}, Lcom/cronutils/model/field/expression/Always;-><init>()V

    invoke-direct {v2, v4, v5, v1}, Lcom/cronutils/model/field/CronField;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    goto :goto_7

    :cond_7
    new-instance v2, Lcom/cronutils/model/field/CronField;

    sget-object v4, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    new-instance v5, Lcom/cronutils/model/field/expression/On;

    new-instance v6, Lcom/cronutils/model/field/value/IntegerFieldValue;

    invoke-direct {v6, v3}, Lcom/cronutils/model/field/value/IntegerFieldValue;-><init>(I)V

    invoke-direct {v5, v6}, Lcom/cronutils/model/field/expression/On;-><init>(Lcom/cronutils/model/field/value/IntegerFieldValue;)V

    invoke-direct {v2, v4, v5, v1}, Lcom/cronutils/model/field/CronField;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    :goto_7
    iput-object v2, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->daysOfWeekCronField:Lcom/cronutils/model/field/CronField;

    goto :goto_8

    :cond_8
    const/4 v0, 0x1

    .line 120
    :goto_8
    iget-object v1, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->months:Lcom/cronutils/model/time/TimeNode;

    if-nez v1, :cond_a

    const/16 v1, 0x1f

    if-eqz v0, :cond_9

    .line 121
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->MONTH:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0, v0, v3, v1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->timeNodeAlways(Lcom/cronutils/model/field/CronFieldName;II)Lcom/cronutils/model/time/TimeNode;

    move-result-object v0

    goto :goto_9

    :cond_9
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->MONTH:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0, v0, v3, v1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->timeNodeLowest(Lcom/cronutils/model/field/CronFieldName;II)Lcom/cronutils/model/time/TimeNode;

    move-result-object v0

    :goto_9
    iput-object v0, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->months:Lcom/cronutils/model/time/TimeNode;

    .line 123
    :cond_a
    iget-object v0, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->yearsValueGenerator:Lcom/cronutils/model/time/generator/FieldValueGenerator;

    if-nez v0, :cond_b

    .line 124
    new-instance v0, Lcom/cronutils/model/field/CronField;

    sget-object v1, Lcom/cronutils/model/field/CronFieldName;->YEAR:Lcom/cronutils/model/field/CronFieldName;

    new-instance v2, Lcom/cronutils/model/field/expression/Always;

    invoke-direct {v2}, Lcom/cronutils/model/field/expression/Always;-><init>()V

    sget-object v3, Lcom/cronutils/model/field/CronFieldName;->YEAR:Lcom/cronutils/model/field/CronFieldName;

    .line 126
    invoke-direct {p0, v3}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->getConstraint(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/cronutils/model/field/CronField;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    .line 125
    invoke-static {v0}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->forCronField(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object v0

    iput-object v0, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->yearsValueGenerator:Lcom/cronutils/model/time/generator/FieldValueGenerator;

    .line 130
    :cond_b
    new-instance v0, Lcom/cronutils/model/time/ExecutionTime;

    iget-object v2, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

    iget-object v3, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->yearsValueGenerator:Lcom/cronutils/model/time/generator/FieldValueGenerator;

    iget-object v4, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->daysOfWeekCronField:Lcom/cronutils/model/field/CronField;

    iget-object v5, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->daysOfMonthCronField:Lcom/cronutils/model/field/CronField;

    iget-object v6, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->months:Lcom/cronutils/model/time/TimeNode;

    iget-object v7, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->hours:Lcom/cronutils/model/time/TimeNode;

    iget-object v8, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->minutes:Lcom/cronutils/model/time/TimeNode;

    iget-object v9, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->seconds:Lcom/cronutils/model/time/TimeNode;

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/cronutils/model/time/ExecutionTime;-><init>(Lcom/cronutils/model/definition/CronDefinition;Lcom/cronutils/model/time/generator/FieldValueGenerator;Lcom/cronutils/model/field/CronField;Lcom/cronutils/model/field/CronField;Lcom/cronutils/model/time/TimeNode;Lcom/cronutils/model/time/TimeNode;Lcom/cronutils/model/time/TimeNode;Lcom/cronutils/model/time/TimeNode;)V

    return-object v0
.end method

.method forDaysOfMonthMatching(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/ExecutionTimeBuilder;
    .locals 1

    .line 82
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0, v0, p1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->validate(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/CronField;)V

    .line 83
    iput-object p1, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->daysOfMonthCronField:Lcom/cronutils/model/field/CronField;

    return-object p0
.end method

.method forDaysOfWeekMatching(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/ExecutionTimeBuilder;
    .locals 1

    .line 76
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0, v0, p1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->validate(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/CronField;)V

    .line 77
    iput-object p1, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->daysOfWeekCronField:Lcom/cronutils/model/field/CronField;

    return-object p0
.end method

.method forHoursMatching(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/ExecutionTimeBuilder;
    .locals 3

    .line 58
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->HOUR:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0, v0, p1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->validate(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/CronField;)V

    .line 59
    new-instance v0, Lcom/cronutils/model/time/TimeNode;

    invoke-static {p1}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->forCronField(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p1

    const/4 v1, 0x0

    const/16 v2, 0x17

    invoke-virtual {p1, v1, v2}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/cronutils/model/time/TimeNode;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->hours:Lcom/cronutils/model/time/TimeNode;

    return-object p0
.end method

.method forMinutesMatching(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/ExecutionTimeBuilder;
    .locals 3

    .line 52
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->MINUTE:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0, v0, p1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->validate(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/CronField;)V

    .line 53
    new-instance v0, Lcom/cronutils/model/time/TimeNode;

    invoke-static {p1}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->forCronField(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p1

    const/4 v1, 0x0

    const/16 v2, 0x3b

    invoke-virtual {p1, v1, v2}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/cronutils/model/time/TimeNode;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->minutes:Lcom/cronutils/model/time/TimeNode;

    return-object p0
.end method

.method forMonthsMatching(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/ExecutionTimeBuilder;
    .locals 3

    .line 64
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->MONTH:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0, v0, p1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->validate(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/CronField;)V

    .line 65
    new-instance v0, Lcom/cronutils/model/time/TimeNode;

    invoke-static {p1}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->forCronField(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p1

    const/4 v1, 0x1

    const/16 v2, 0xc

    invoke-virtual {p1, v1, v2}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/cronutils/model/time/TimeNode;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->months:Lcom/cronutils/model/time/TimeNode;

    return-object p0
.end method

.method forSecondsMatching(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/ExecutionTimeBuilder;
    .locals 3

    .line 46
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->SECOND:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0, v0, p1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->validate(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/CronField;)V

    .line 47
    new-instance v0, Lcom/cronutils/model/time/TimeNode;

    invoke-static {p1}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->forCronField(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p1

    const/4 v1, 0x0

    const/16 v2, 0x3b

    invoke-virtual {p1, v1, v2}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/cronutils/model/time/TimeNode;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->seconds:Lcom/cronutils/model/time/TimeNode;

    return-object p0
.end method

.method forYearsMatching(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/ExecutionTimeBuilder;
    .locals 1

    .line 70
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->YEAR:Lcom/cronutils/model/field/CronFieldName;

    invoke-direct {p0, v0, p1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->validate(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/CronField;)V

    .line 71
    invoke-static {p1}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->forCronField(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p1

    iput-object p1, p0, Lcom/cronutils/model/time/ExecutionTimeBuilder;->yearsValueGenerator:Lcom/cronutils/model/time/generator/FieldValueGenerator;

    return-object p0
.end method
