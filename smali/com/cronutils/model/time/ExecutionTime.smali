.class public Lcom/cronutils/model/time/ExecutionTime;
.super Ljava/lang/Object;
.source "ExecutionTime.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

.field private daysOfMonthCronField:Lcom/cronutils/model/field/CronField;

.field private daysOfWeekCronField:Lcom/cronutils/model/field/CronField;

.field private hours:Lcom/cronutils/model/time/TimeNode;

.field private minutes:Lcom/cronutils/model/time/TimeNode;

.field private months:Lcom/cronutils/model/time/TimeNode;

.field private seconds:Lcom/cronutils/model/time/TimeNode;

.field private yearsValueGenerator:Lcom/cronutils/model/time/generator/FieldValueGenerator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 51
    const-class v0, Lcom/cronutils/model/time/ExecutionTime;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/cronutils/model/time/ExecutionTime;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method constructor <init>(Lcom/cronutils/model/definition/CronDefinition;Lcom/cronutils/model/time/generator/FieldValueGenerator;Lcom/cronutils/model/field/CronField;Lcom/cronutils/model/field/CronField;Lcom/cronutils/model/time/TimeNode;Lcom/cronutils/model/time/TimeNode;Lcom/cronutils/model/time/TimeNode;Lcom/cronutils/model/time/TimeNode;)V
    .locals 0
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    invoke-static {p1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/definition/CronDefinition;

    iput-object p1, p0, Lcom/cronutils/model/time/ExecutionTime;->cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

    .line 68
    invoke-static {p2}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/time/generator/FieldValueGenerator;

    iput-object p1, p0, Lcom/cronutils/model/time/ExecutionTime;->yearsValueGenerator:Lcom/cronutils/model/time/generator/FieldValueGenerator;

    .line 69
    invoke-static {p3}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/CronField;

    iput-object p1, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfWeekCronField:Lcom/cronutils/model/field/CronField;

    .line 70
    invoke-static {p4}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/CronField;

    iput-object p1, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfMonthCronField:Lcom/cronutils/model/field/CronField;

    .line 71
    invoke-static {p5}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/time/TimeNode;

    iput-object p1, p0, Lcom/cronutils/model/time/ExecutionTime;->months:Lcom/cronutils/model/time/TimeNode;

    .line 72
    invoke-static {p6}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/time/TimeNode;

    iput-object p1, p0, Lcom/cronutils/model/time/ExecutionTime;->hours:Lcom/cronutils/model/time/TimeNode;

    .line 73
    invoke-static {p7}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/time/TimeNode;

    iput-object p1, p0, Lcom/cronutils/model/time/ExecutionTime;->minutes:Lcom/cronutils/model/time/TimeNode;

    .line 74
    invoke-static {p8}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/time/TimeNode;

    iput-object p1, p0, Lcom/cronutils/model/time/ExecutionTime;->seconds:Lcom/cronutils/model/time/TimeNode;

    return-void
.end method

.method private ensureSameDate(Lorg/joda/time/DateTime;IIIIIILorg/joda/time/DateTimeZone;)Lorg/joda/time/DateTime;
    .locals 0

    .line 451
    invoke-virtual {p1}, Lorg/joda/time/DateTime;->getSecondOfMinute()I

    move-result p8

    if-eq p8, p7, :cond_0

    .line 452
    invoke-virtual {p1}, Lorg/joda/time/DateTime;->getSecondOfMinute()I

    move-result p8

    sub-int/2addr p7, p8

    invoke-virtual {p1, p7}, Lorg/joda/time/DateTime;->plusSeconds(I)Lorg/joda/time/DateTime;

    move-result-object p1

    .line 454
    :cond_0
    invoke-virtual {p1}, Lorg/joda/time/DateTime;->getMinuteOfHour()I

    move-result p7

    if-eq p7, p6, :cond_1

    .line 455
    invoke-virtual {p1}, Lorg/joda/time/DateTime;->getMinuteOfHour()I

    move-result p7

    sub-int/2addr p6, p7

    invoke-virtual {p1, p6}, Lorg/joda/time/DateTime;->plusMinutes(I)Lorg/joda/time/DateTime;

    move-result-object p1

    .line 457
    :cond_1
    invoke-virtual {p1}, Lorg/joda/time/DateTime;->getHourOfDay()I

    move-result p6

    if-eq p6, p5, :cond_2

    .line 458
    invoke-virtual {p1}, Lorg/joda/time/DateTime;->getHourOfDay()I

    move-result p6

    sub-int/2addr p5, p6

    invoke-virtual {p1, p5}, Lorg/joda/time/DateTime;->plusHours(I)Lorg/joda/time/DateTime;

    move-result-object p1

    .line 460
    :cond_2
    invoke-virtual {p1}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result p5

    if-eq p5, p4, :cond_3

    .line 461
    invoke-virtual {p1}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result p5

    sub-int/2addr p4, p5

    invoke-virtual {p1, p4}, Lorg/joda/time/DateTime;->plusDays(I)Lorg/joda/time/DateTime;

    move-result-object p1

    .line 463
    :cond_3
    invoke-virtual {p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result p4

    if-eq p4, p3, :cond_4

    .line 464
    invoke-virtual {p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result p4

    sub-int/2addr p3, p4

    invoke-virtual {p1, p3}, Lorg/joda/time/DateTime;->plusMonths(I)Lorg/joda/time/DateTime;

    move-result-object p1

    .line 466
    :cond_4
    invoke-virtual {p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result p3

    if-eq p3, p2, :cond_5

    .line 467
    invoke-virtual {p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result p3

    sub-int/2addr p2, p3

    invoke-virtual {p1, p2}, Lorg/joda/time/DateTime;->plusYears(I)Lorg/joda/time/DateTime;

    move-result-object p1

    :cond_5
    return-object p1
.end method

.method public static forCron(Lcom/cronutils/model/Cron;)Lcom/cronutils/model/time/ExecutionTime;
    .locals 7

    .line 83
    invoke-virtual {p0}, Lcom/cronutils/model/Cron;->retrieveFieldsAsMap()Ljava/util/Map;

    move-result-object v0

    .line 84
    new-instance v1, Lcom/cronutils/model/time/ExecutionTimeBuilder;

    invoke-virtual {p0}, Lcom/cronutils/model/Cron;->getCronDefinition()Lcom/cronutils/model/definition/CronDefinition;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/cronutils/model/time/ExecutionTimeBuilder;-><init>(Lcom/cronutils/model/definition/CronDefinition;)V

    .line 85
    invoke-static {}, Lcom/cronutils/model/field/CronFieldName;->values()[Lcom/cronutils/model/field/CronFieldName;

    move-result-object p0

    array-length v2, p0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, p0, v3

    .line 86
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 87
    sget-object v5, Lcom/cronutils/model/time/ExecutionTime$1;->$SwitchMap$com$cronutils$model$field$CronFieldName:[I

    invoke-virtual {v4}, Lcom/cronutils/model/field/CronFieldName;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    goto :goto_1

    .line 107
    :pswitch_0
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1, v4}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->forYearsMatching(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/ExecutionTimeBuilder;

    goto :goto_1

    .line 104
    :pswitch_1
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1, v4}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->forMonthsMatching(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/ExecutionTimeBuilder;

    goto :goto_1

    .line 101
    :pswitch_2
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1, v4}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->forDaysOfMonthMatching(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/ExecutionTimeBuilder;

    goto :goto_1

    .line 98
    :pswitch_3
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1, v4}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->forDaysOfWeekMatching(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/ExecutionTimeBuilder;

    goto :goto_1

    .line 95
    :pswitch_4
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1, v4}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->forHoursMatching(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/ExecutionTimeBuilder;

    goto :goto_1

    .line 92
    :pswitch_5
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1, v4}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->forMinutesMatching(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/ExecutionTimeBuilder;

    goto :goto_1

    .line 89
    :pswitch_6
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1, v4}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->forSecondsMatching(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/ExecutionTimeBuilder;

    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 112
    :cond_1
    invoke-virtual {v1}, Lcom/cronutils/model/time/ExecutionTimeBuilder;->build()Lcom/cronutils/model/time/ExecutionTime;

    move-result-object p0

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private generateDayCandidatesQuestionMarkNotSupported(IILcom/cronutils/mapper/WeekDay;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/cronutils/mapper/WeekDay;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 386
    new-instance v6, Lorg/joda/time/DateTime;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object v0, v6

    move v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lorg/joda/time/DateTime;-><init>(IIIII)V

    .line 387
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    .line 388
    iget-object v1, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfMonthCronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v1

    instance-of v1, v1, Lcom/cronutils/model/field/expression/Always;

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfWeekCronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v1

    instance-of v1, v1, Lcom/cronutils/model/field/expression/Always;

    if-eqz v1, :cond_0

    .line 389
    iget-object p3, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfMonthCronField:Lcom/cronutils/model/field/CronField;

    invoke-static {p3, p1, p2}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->createDayOfMonthValueGeneratorInstance(Lcom/cronutils/model/field/CronField;II)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p1

    .line 390
    invoke-virtual {v6}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object p2

    invoke-virtual {p2}, Lorg/joda/time/DateTime$Property;->getMaximumValue()I

    move-result p2

    .line 389
    invoke-virtual {p1, v2, p2}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 392
    :cond_0
    iget-object v1, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfMonthCronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v1

    instance-of v1, v1, Lcom/cronutils/model/field/expression/Always;

    if-eqz v1, :cond_1

    .line 393
    iget-object v1, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfWeekCronField:Lcom/cronutils/model/field/CronField;

    invoke-static {v1, p1, p2, p3}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->createDayOfWeekValueGeneratorInstance(Lcom/cronutils/model/field/CronField;IILcom/cronutils/mapper/WeekDay;)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p1

    .line 394
    invoke-virtual {v6}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object p2

    invoke-virtual {p2}, Lorg/joda/time/DateTime$Property;->getMaximumValue()I

    move-result p2

    invoke-virtual {p1, v2, p2}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object p1

    .line 393
    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 396
    :cond_1
    iget-object v1, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfWeekCronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v1

    instance-of v1, v1, Lcom/cronutils/model/field/expression/Always;

    if-eqz v1, :cond_2

    .line 397
    iget-object p3, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfMonthCronField:Lcom/cronutils/model/field/CronField;

    invoke-static {p3, p1, p2}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->createDayOfMonthValueGeneratorInstance(Lcom/cronutils/model/field/CronField;II)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p1

    .line 398
    invoke-virtual {v6}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object p2

    invoke-virtual {p2}, Lorg/joda/time/DateTime$Property;->getMaximumValue()I

    move-result p2

    .line 397
    invoke-virtual {p1, v2, p2}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 400
    :cond_2
    iget-object v1, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfWeekCronField:Lcom/cronutils/model/field/CronField;

    invoke-static {v1, p1, p2, p3}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->createDayOfWeekValueGeneratorInstance(Lcom/cronutils/model/field/CronField;IILcom/cronutils/mapper/WeekDay;)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p3

    .line 401
    invoke-virtual {v6}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object v1

    invoke-virtual {v1}, Lorg/joda/time/DateTime$Property;->getMaximumValue()I

    move-result v1

    invoke-virtual {p3, v2, v1}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object p3

    .line 400
    invoke-interface {v0, p3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 402
    iget-object p3, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfMonthCronField:Lcom/cronutils/model/field/CronField;

    invoke-static {p3, p1, p2}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->createDayOfMonthValueGeneratorInstance(Lcom/cronutils/model/field/CronField;II)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p1

    .line 403
    invoke-virtual {v6}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object p2

    invoke-virtual {p2}, Lorg/joda/time/DateTime$Property;->getMaximumValue()I

    move-result p2

    .line 402
    invoke-virtual {p1, v2, p2}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 407
    :goto_0
    invoke-static {v0}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object p1

    .line 408
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-object p1
.end method

.method private generateDayCandidatesQuestionMarkSupported(IILcom/cronutils/mapper/WeekDay;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/cronutils/mapper/WeekDay;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 413
    new-instance v6, Lorg/joda/time/DateTime;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object v0, v6

    move v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lorg/joda/time/DateTime;-><init>(IIIII)V

    .line 414
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    .line 415
    iget-object v1, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfMonthCronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v1

    instance-of v1, v1, Lcom/cronutils/model/field/expression/Always;

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfWeekCronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v1

    instance-of v1, v1, Lcom/cronutils/model/field/expression/Always;

    if-eqz v1, :cond_0

    .line 416
    iget-object p3, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfMonthCronField:Lcom/cronutils/model/field/CronField;

    invoke-static {p3, p1, p2}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->createDayOfMonthValueGeneratorInstance(Lcom/cronutils/model/field/CronField;II)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p1

    invoke-virtual {v6}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object p2

    invoke-virtual {p2}, Lorg/joda/time/DateTime$Property;->getMaximumValue()I

    move-result p2

    invoke-virtual {p1, v2, p2}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 418
    :cond_0
    iget-object v1, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfMonthCronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v1

    instance-of v1, v1, Lcom/cronutils/model/field/expression/QuestionMark;

    if-eqz v1, :cond_1

    .line 420
    iget-object v1, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfWeekCronField:Lcom/cronutils/model/field/CronField;

    invoke-static {v1, p1, p2, p3}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->createDayOfWeekValueGeneratorInstance(Lcom/cronutils/model/field/CronField;IILcom/cronutils/mapper/WeekDay;)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p1

    const/4 p2, -0x1

    invoke-virtual {v6}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object p3

    invoke-virtual {p3}, Lorg/joda/time/DateTime$Property;->getMaximumValue()I

    move-result p3

    invoke-virtual {p1, p2, p3}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 422
    :cond_1
    iget-object v1, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfWeekCronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v1

    instance-of v1, v1, Lcom/cronutils/model/field/expression/QuestionMark;

    if-eqz v1, :cond_2

    .line 423
    iget-object p3, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfMonthCronField:Lcom/cronutils/model/field/CronField;

    invoke-static {p3, p1, p2}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->createDayOfMonthValueGeneratorInstance(Lcom/cronutils/model/field/CronField;II)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p1

    invoke-virtual {v6}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object p2

    invoke-virtual {p2}, Lorg/joda/time/DateTime$Property;->getMaximumValue()I

    move-result p2

    invoke-virtual {p1, v2, p2}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 425
    :cond_2
    iget-object v1, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfWeekCronField:Lcom/cronutils/model/field/CronField;

    invoke-static {v1, p1, p2, p3}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->createDayOfWeekValueGeneratorInstance(Lcom/cronutils/model/field/CronField;IILcom/cronutils/mapper/WeekDay;)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p3

    invoke-virtual {v6}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object v1

    invoke-virtual {v1}, Lorg/joda/time/DateTime$Property;->getMaximumValue()I

    move-result v1

    invoke-virtual {p3, v2, v1}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object p3

    invoke-interface {v0, p3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 426
    iget-object p3, p0, Lcom/cronutils/model/time/ExecutionTime;->daysOfMonthCronField:Lcom/cronutils/model/field/CronField;

    invoke-static {p3, p1, p2}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->createDayOfMonthValueGeneratorInstance(Lcom/cronutils/model/field/CronField;II)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p1

    invoke-virtual {v6}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object p2

    invoke-virtual {p2}, Lorg/joda/time/DateTime$Property;->getMaximumValue()I

    move-result p2

    invoke-virtual {p1, v2, p2}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 430
    :goto_0
    invoke-static {v0}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object p1

    .line 431
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-object p1
.end method

.method private initDateTime(IIIIIILorg/joda/time/DateTimeZone;)Lorg/joda/time/DateTime;
    .locals 18

    .line 437
    new-instance v8, Lorg/joda/time/DateTime;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v8

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Lorg/joda/time/DateTime;-><init>(IIIIIILorg/joda/time/DateTimeZone;)V

    move/from16 v0, p1

    .line 439
    invoke-virtual {v8, v0}, Lorg/joda/time/DateTime;->plusYears(I)Lorg/joda/time/DateTime;

    move-result-object v1

    add-int/lit8 v2, p2, -0x1

    .line 440
    invoke-virtual {v1, v2}, Lorg/joda/time/DateTime;->plusMonths(I)Lorg/joda/time/DateTime;

    move-result-object v1

    add-int/lit8 v2, p3, -0x1

    .line 441
    invoke-virtual {v1, v2}, Lorg/joda/time/DateTime;->plusDays(I)Lorg/joda/time/DateTime;

    move-result-object v1

    move/from16 v2, p4

    .line 442
    invoke-virtual {v1, v2}, Lorg/joda/time/DateTime;->plusHours(I)Lorg/joda/time/DateTime;

    move-result-object v1

    move/from16 v3, p5

    .line 443
    invoke-virtual {v1, v3}, Lorg/joda/time/DateTime;->plusMinutes(I)Lorg/joda/time/DateTime;

    move-result-object v1

    move/from16 v4, p6

    .line 444
    invoke-virtual {v1, v4}, Lorg/joda/time/DateTime;->plusSeconds(I)Lorg/joda/time/DateTime;

    move-result-object v10

    move-object/from16 v9, p0

    move/from16 v11, p1

    move/from16 v12, p2

    move/from16 v13, p3

    move/from16 v14, p4

    move/from16 v15, p5

    move/from16 v16, p6

    move-object/from16 v17, p7

    .line 445
    invoke-direct/range {v9 .. v17}, Lcom/cronutils/model/time/ExecutionTime;->ensureSameDate(Lorg/joda/time/DateTime;IIIIIILorg/joda/time/DateTimeZone;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method generateDays(Lcom/cronutils/model/definition/CronDefinition;Lorg/joda/time/DateTime;)Lcom/cronutils/model/time/TimeNode;
    .locals 3

    .line 317
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    .line 318
    invoke-virtual {p1, v0}, Lcom/cronutils/model/definition/CronDefinition;->getFieldDefinition(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/definition/FieldDefinition;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinition;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getSpecialChars()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->QUESTION_MARK:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    new-instance v0, Lcom/cronutils/model/time/TimeNode;

    .line 322
    invoke-virtual {p2}, Lorg/joda/time/DateTime;->getYear()I

    move-result v1

    invoke-virtual {p2}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result p2

    sget-object v2, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    .line 324
    invoke-virtual {p1, v2}, Lcom/cronutils/model/definition/CronDefinition;->getFieldDefinition(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/definition/FieldDefinition;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;

    .line 325
    invoke-virtual {p1}, Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;->getMondayDoWValue()Lcom/cronutils/mapper/WeekDay;

    move-result-object p1

    .line 321
    invoke-direct {p0, v1, p2, p1}, Lcom/cronutils/model/time/ExecutionTime;->generateDayCandidatesQuestionMarkSupported(IILcom/cronutils/mapper/WeekDay;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/cronutils/model/time/TimeNode;-><init>(Ljava/util/List;)V

    return-object v0

    .line 329
    :cond_0
    new-instance v0, Lcom/cronutils/model/time/TimeNode;

    .line 331
    invoke-virtual {p2}, Lorg/joda/time/DateTime;->getYear()I

    move-result v1

    invoke-virtual {p2}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result p2

    sget-object v2, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    .line 333
    invoke-virtual {p1, v2}, Lcom/cronutils/model/definition/CronDefinition;->getFieldDefinition(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/definition/FieldDefinition;

    move-result-object p1

    check-cast p1, Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;

    .line 334
    invoke-virtual {p1}, Lcom/cronutils/model/field/definition/DayOfWeekFieldDefinition;->getMondayDoWValue()Lcom/cronutils/mapper/WeekDay;

    move-result-object p1

    .line 330
    invoke-direct {p0, v1, p2, p1}, Lcom/cronutils/model/time/ExecutionTime;->generateDayCandidatesQuestionMarkNotSupported(IILcom/cronutils/mapper/WeekDay;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/cronutils/model/time/TimeNode;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public isMatch(Lorg/joda/time/DateTime;)Z
    .locals 1

    .line 382
    invoke-virtual {p0, p1}, Lcom/cronutils/model/time/ExecutionTime;->lastExecution(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cronutils/model/time/ExecutionTime;->nextExecution(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/joda/time/DateTime;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public lastExecution(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;
    .locals 2

    .line 355
    invoke-static {p1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/cronutils/model/time/ExecutionTime;->previousClosestMatch(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0

    .line 358
    invoke-virtual {v0, p1}, Lorg/joda/time/DateTime;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 359
    invoke-virtual {p1, v0}, Lorg/joda/time/DateTime;->minusSeconds(I)Lorg/joda/time/DateTime;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/cronutils/model/time/ExecutionTime;->previousClosestMatch(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0
    :try_end_0
    .catch Lcom/cronutils/model/time/generator/NoSuchValueException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-object v0

    :catch_0
    move-exception p1

    .line 363
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method nextClosestMatch(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cronutils/model/time/generator/NoSuchValueException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v0, p1

    .line 141
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->yearsValueGenerator:Lcom/cronutils/model/time/generator/FieldValueGenerator;

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object v1

    .line 143
    iget-object v2, v9, Lcom/cronutils/model/time/ExecutionTime;->months:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v2}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 144
    iget-object v2, v9, Lcom/cronutils/model/time/ExecutionTime;->hours:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v2}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 145
    iget-object v2, v9, Lcom/cronutils/model/time/ExecutionTime;->minutes:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v2}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 146
    iget-object v2, v9, Lcom/cronutils/model/time/ExecutionTime;->seconds:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v2}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 150
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 151
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->yearsValueGenerator:Lcom/cronutils/model/time/generator/FieldValueGenerator;

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateNextValue(I)I

    move-result v11

    .line 152
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

    new-instance v2, Lorg/joda/time/DateTime;

    const/4 v13, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v10, v2

    move v12, v4

    invoke-direct/range {v10 .. v15}, Lorg/joda/time/DateTime;-><init>(IIIII)V

    invoke-virtual {v9, v1, v2}, Lcom/cronutils/model/time/ExecutionTime;->generateDays(Lcom/cronutils/model/definition/CronDefinition;Lorg/joda/time/DateTime;)Lcom/cronutils/model/time/TimeNode;

    move-result-object v1

    .line 153
    iget-object v2, v9, Lcom/cronutils/model/time/ExecutionTime;->yearsValueGenerator:Lcom/cronutils/model/time/generator/FieldValueGenerator;

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v8

    invoke-virtual {v2, v8}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateNextValue(I)I

    move-result v2

    invoke-virtual {v1}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v0

    move-object/from16 v1, p0

    move v3, v4

    move v4, v8

    move-object v8, v0

    invoke-direct/range {v1 .. v8}, Lcom/cronutils/model/time/ExecutionTime;->initDateTime(IIIIIILorg/joda/time/DateTimeZone;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 155
    :cond_0
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->months:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v1}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_3

    .line 156
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->months:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v4

    invoke-virtual {v1, v4, v3}, Lcom/cronutils/model/time/TimeNode;->getNextValue(II)Lcom/cronutils/model/time/NearestValue;

    move-result-object v1

    .line 157
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getValue()I

    move-result v4

    .line 158
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v8

    if-lez v8, :cond_1

    .line 159
    new-instance v2, Lorg/joda/time/DateTime;

    .line 160
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v11

    const/4 v12, 0x1

    const/4 v13, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v17

    move-object v10, v2

    invoke-direct/range {v10 .. v17}, Lorg/joda/time/DateTime;-><init>(IIIIIILorg/joda/time/DateTimeZone;)V

    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v0

    invoke-virtual {v2, v0}, Lorg/joda/time/DateTime;->plusYears(I)Lorg/joda/time/DateTime;

    move-result-object v0

    .line 161
    invoke-virtual {v9, v0}, Lcom/cronutils/model/time/ExecutionTime;->nextClosestMatch(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 163
    :cond_1
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getValue()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v8

    if-ge v1, v8, :cond_2

    .line 164
    invoke-virtual {v0, v2}, Lorg/joda/time/DateTime;->plusYears(I)Lorg/joda/time/DateTime;

    move-result-object v0

    .line 166
    :cond_2
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

    new-instance v2, Lorg/joda/time/DateTime;

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getYear()I

    move-result v11

    const/4 v13, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v10, v2

    move v12, v4

    invoke-direct/range {v10 .. v15}, Lorg/joda/time/DateTime;-><init>(IIIII)V

    invoke-virtual {v9, v1, v2}, Lcom/cronutils/model/time/ExecutionTime;->generateDays(Lcom/cronutils/model/definition/CronDefinition;Lorg/joda/time/DateTime;)Lcom/cronutils/model/time/TimeNode;

    move-result-object v1

    .line 167
    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getYear()I

    move-result v2

    invoke-virtual {v1}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v0

    move-object/from16 v1, p0

    move v3, v4

    move v4, v8

    move-object v8, v0

    invoke-direct/range {v1 .. v8}, Lcom/cronutils/model/time/ExecutionTime;->initDateTime(IIIIIILorg/joda/time/DateTimeZone;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 169
    :cond_3
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

    invoke-virtual {v9, v1, v0}, Lcom/cronutils/model/time/ExecutionTime;->generateDays(Lcom/cronutils/model/definition/CronDefinition;Lorg/joda/time/DateTime;)Lcom/cronutils/model/time/TimeNode;

    move-result-object v1

    .line 170
    invoke-virtual {v1}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 171
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result v4

    invoke-virtual {v1, v4, v3}, Lcom/cronutils/model/time/TimeNode;->getNextValue(II)Lcom/cronutils/model/time/NearestValue;

    move-result-object v1

    .line 172
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v3

    if-lez v3, :cond_4

    .line 173
    new-instance v2, Lorg/joda/time/DateTime;

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v12

    const/4 v13, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v17

    move-object v10, v2

    invoke-direct/range {v10 .. v17}, Lorg/joda/time/DateTime;-><init>(IIIIIILorg/joda/time/DateTimeZone;)V

    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v0

    invoke-virtual {v2, v0}, Lorg/joda/time/DateTime;->plusMonths(I)Lorg/joda/time/DateTime;

    move-result-object v0

    .line 174
    invoke-virtual {v9, v0}, Lcom/cronutils/model/time/ExecutionTime;->nextClosestMatch(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 176
    :cond_4
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getValue()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result v4

    if-ge v3, v4, :cond_5

    .line 177
    invoke-virtual {v0, v2}, Lorg/joda/time/DateTime;->plusMonths(I)Lorg/joda/time/DateTime;

    move-result-object v0

    .line 179
    :cond_5
    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getYear()I

    move-result v2

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v3

    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getValue()I

    move-result v4

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v8

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/cronutils/model/time/ExecutionTime;->initDateTime(IIIIIILorg/joda/time/DateTimeZone;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 181
    :cond_6
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->hours:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v1}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getHourOfDay()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 182
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->hours:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getHourOfDay()I

    move-result v4

    invoke-virtual {v1, v4, v3}, Lcom/cronutils/model/time/TimeNode;->getNextValue(II)Lcom/cronutils/model/time/NearestValue;

    move-result-object v1

    .line 183
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getValue()I

    move-result v5

    .line 184
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v3

    if-lez v3, :cond_7

    .line 185
    new-instance v2, Lorg/joda/time/DateTime;

    .line 186
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v12

    .line 187
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v17

    move-object v10, v2

    invoke-direct/range {v10 .. v17}, Lorg/joda/time/DateTime;-><init>(IIIIIILorg/joda/time/DateTimeZone;)V

    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v0

    invoke-virtual {v2, v0}, Lorg/joda/time/DateTime;->plusDays(I)Lorg/joda/time/DateTime;

    move-result-object v0

    .line 188
    invoke-virtual {v9, v0}, Lcom/cronutils/model/time/ExecutionTime;->nextClosestMatch(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 190
    :cond_7
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getValue()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getHourOfDay()I

    move-result v3

    if-ge v1, v3, :cond_8

    .line 191
    invoke-virtual {v0, v2}, Lorg/joda/time/DateTime;->plusDays(I)Lorg/joda/time/DateTime;

    move-result-object v0

    .line 193
    :cond_8
    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getYear()I

    move-result v2

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v3

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result v4

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v8

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/cronutils/model/time/ExecutionTime;->initDateTime(IIIIIILorg/joda/time/DateTimeZone;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 195
    :cond_9
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->minutes:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v1}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMinuteOfHour()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 196
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->minutes:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMinuteOfHour()I

    move-result v4

    invoke-virtual {v1, v4, v3}, Lcom/cronutils/model/time/TimeNode;->getNextValue(II)Lcom/cronutils/model/time/NearestValue;

    move-result-object v1

    .line 197
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getValue()I

    move-result v6

    .line 198
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v3

    if-lez v3, :cond_a

    .line 199
    new-instance v2, Lorg/joda/time/DateTime;

    .line 200
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v12

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result v13

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getHourOfDay()I

    move-result v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 201
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v17

    move-object v10, v2

    invoke-direct/range {v10 .. v17}, Lorg/joda/time/DateTime;-><init>(IIIIIILorg/joda/time/DateTimeZone;)V

    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v0

    invoke-virtual {v2, v0}, Lorg/joda/time/DateTime;->plusHours(I)Lorg/joda/time/DateTime;

    move-result-object v0

    .line 202
    invoke-virtual {v9, v0}, Lcom/cronutils/model/time/ExecutionTime;->nextClosestMatch(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 204
    :cond_a
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getValue()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMinuteOfHour()I

    move-result v3

    if-ge v1, v3, :cond_b

    .line 205
    invoke-virtual {v0, v2}, Lorg/joda/time/DateTime;->plusHours(I)Lorg/joda/time/DateTime;

    move-result-object v0

    .line 207
    :cond_b
    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getYear()I

    move-result v2

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v3

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result v4

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getHourOfDay()I

    move-result v5

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v8

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/cronutils/model/time/ExecutionTime;->initDateTime(IIIIIILorg/joda/time/DateTimeZone;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 209
    :cond_c
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->seconds:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v1}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getSecondOfMinute()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 210
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->seconds:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getSecondOfMinute()I

    move-result v4

    invoke-virtual {v1, v4, v3}, Lcom/cronutils/model/time/TimeNode;->getNextValue(II)Lcom/cronutils/model/time/NearestValue;

    move-result-object v1

    .line 211
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getValue()I

    move-result v7

    .line 212
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v3

    if-lez v3, :cond_d

    .line 213
    new-instance v2, Lorg/joda/time/DateTime;

    .line 214
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v12

    .line 215
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result v13

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getHourOfDay()I

    move-result v14

    .line 216
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMinuteOfHour()I

    move-result v15

    const/16 v16, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v17

    move-object v10, v2

    invoke-direct/range {v10 .. v17}, Lorg/joda/time/DateTime;-><init>(IIIIIILorg/joda/time/DateTimeZone;)V

    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v0

    invoke-virtual {v2, v0}, Lorg/joda/time/DateTime;->plusMinutes(I)Lorg/joda/time/DateTime;

    move-result-object v0

    .line 217
    :try_start_0
    invoke-virtual {v9, v0}, Lcom/cronutils/model/time/ExecutionTime;->nextClosestMatch(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 219
    :cond_d
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getValue()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getSecondOfMinute()I

    move-result v3

    if-ge v1, v3, :cond_e

    .line 220
    invoke-virtual {v0, v2}, Lorg/joda/time/DateTime;->plusMinutes(I)Lorg/joda/time/DateTime;

    move-result-object v0

    .line 222
    :cond_e
    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getYear()I

    move-result v2

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v3

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result v4

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getHourOfDay()I

    move-result v5

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getMinuteOfHour()I

    move-result v6

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v8

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/cronutils/model/time/ExecutionTime;->initDateTime(IIIIIILorg/joda/time/DateTimeZone;)Lorg/joda/time/DateTime;

    move-result-object v0

    :cond_f
    return-object v0

    :catch_0
    move-exception v0

    move-object v1, v0

    .line 217
    throw v1
.end method

.method public nextExecution(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;
    .locals 2

    .line 121
    invoke-static {p1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/cronutils/model/time/ExecutionTime;->nextClosestMatch(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0

    .line 124
    invoke-virtual {v0, p1}, Lorg/joda/time/DateTime;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 125
    invoke-virtual {p1, v0}, Lorg/joda/time/DateTime;->plusSeconds(I)Lorg/joda/time/DateTime;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/cronutils/model/time/ExecutionTime;->nextClosestMatch(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0
    :try_end_0
    .catch Lcom/cronutils/model/time/generator/NoSuchValueException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-object v0

    :catch_0
    move-exception p1

    .line 129
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method previousClosestMatch(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cronutils/model/time/generator/NoSuchValueException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v0, p1

    .line 235
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->yearsValueGenerator:Lcom/cronutils/model/time/generator/FieldValueGenerator;

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object v1

    .line 236
    iget-object v2, v9, Lcom/cronutils/model/time/ExecutionTime;->cronDefinition:Lcom/cronutils/model/definition/CronDefinition;

    invoke-virtual {v9, v2, v0}, Lcom/cronutils/model/time/ExecutionTime;->generateDays(Lcom/cronutils/model/definition/CronDefinition;Lorg/joda/time/DateTime;)Lcom/cronutils/model/time/TimeNode;

    move-result-object v2

    .line 237
    iget-object v3, v9, Lcom/cronutils/model/time/ExecutionTime;->months:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v3}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v3

    iget-object v4, v9, Lcom/cronutils/model/time/ExecutionTime;->months:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v4}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    sub-int/2addr v4, v5

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 238
    invoke-virtual {v2}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    sub-int/2addr v6, v5

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 239
    iget-object v6, v9, Lcom/cronutils/model/time/ExecutionTime;->hours:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v6}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v6

    iget-object v7, v9, Lcom/cronutils/model/time/ExecutionTime;->hours:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v7}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    sub-int/2addr v7, v5

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 240
    iget-object v7, v9, Lcom/cronutils/model/time/ExecutionTime;->minutes:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v7}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v7

    iget-object v8, v9, Lcom/cronutils/model/time/ExecutionTime;->minutes:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v8}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    sub-int/2addr v8, v5

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 241
    iget-object v8, v9, Lcom/cronutils/model/time/ExecutionTime;->seconds:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v8}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v8

    iget-object v10, v9, Lcom/cronutils/model/time/ExecutionTime;->seconds:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v10}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    sub-int/2addr v10, v5

    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 245
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 246
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->yearsValueGenerator:Lcom/cronutils/model/time/generator/FieldValueGenerator;

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v10

    invoke-virtual {v1, v10}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generatePreviousValue(I)I

    move-result v16

    const/16 v1, 0x1c

    if-le v4, v1, :cond_1

    .line 248
    new-instance v1, Lorg/joda/time/DateTime;

    const/4 v13, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v10, v1

    move/from16 v11, v16

    move v12, v3

    invoke-direct/range {v10 .. v15}, Lorg/joda/time/DateTime;-><init>(IIIII)V

    invoke-virtual {v1}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object v1

    invoke-virtual {v1}, Lorg/joda/time/DateTime$Property;->getMaximumValue()I

    move-result v1

    if-le v4, v1, :cond_1

    .line 250
    invoke-virtual {v2, v4, v5}, Lcom/cronutils/model/time/TimeNode;->getPreviousValue(II)Lcom/cronutils/model/time/NearestValue;

    move-result-object v1

    .line 251
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v2

    if-lez v2, :cond_0

    .line 252
    new-instance v2, Lorg/joda/time/DateTime;

    const/4 v13, 0x1

    const/16 v14, 0x17

    const/16 v15, 0x3b

    const/16 v4, 0x3b

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v17

    move-object v10, v2

    move/from16 v11, v16

    move v12, v3

    move/from16 v16, v4

    invoke-direct/range {v10 .. v17}, Lorg/joda/time/DateTime;-><init>(IIIIIILorg/joda/time/DateTimeZone;)V

    .line 253
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v0

    invoke-virtual {v2, v0}, Lorg/joda/time/DateTime;->minusMonths(I)Lorg/joda/time/DateTime;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/time/DateTime$Property;->withMaximumValue()Lorg/joda/time/DateTime;

    move-result-object v0

    .line 254
    invoke-virtual {v9, v0}, Lcom/cronutils/model/time/ExecutionTime;->previousClosestMatch(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 256
    :cond_0
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getValue()I

    move-result v1

    move v4, v1

    .line 260
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v0

    move-object/from16 v1, p0

    move/from16 v2, v16

    move v5, v6

    move v6, v7

    move v7, v8

    move-object v8, v0

    invoke-direct/range {v1 .. v8}, Lcom/cronutils/model/time/ExecutionTime;->initDateTime(IIIIIILorg/joda/time/DateTimeZone;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 262
    :cond_2
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->months:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v1}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    const/4 v3, 0x0

    if-nez v1, :cond_4

    .line 263
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->months:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v2

    invoke-virtual {v1, v2, v3}, Lcom/cronutils/model/time/TimeNode;->getPreviousValue(II)Lcom/cronutils/model/time/NearestValue;

    move-result-object v1

    .line 264
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getValue()I

    move-result v3

    .line 265
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v2

    if-lez v2, :cond_3

    .line 266
    new-instance v2, Lorg/joda/time/DateTime;

    .line 267
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v11

    const/16 v12, 0xc

    const/16 v13, 0x1f

    const/16 v14, 0x17

    const/16 v15, 0x3b

    const/16 v16, 0x3b

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v17

    move-object v10, v2

    invoke-direct/range {v10 .. v17}, Lorg/joda/time/DateTime;-><init>(IIIIIILorg/joda/time/DateTimeZone;)V

    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v0

    invoke-virtual {v2, v0}, Lorg/joda/time/DateTime;->minusYears(I)Lorg/joda/time/DateTime;

    move-result-object v0

    .line 268
    invoke-virtual {v9, v0}, Lcom/cronutils/model/time/ExecutionTime;->previousClosestMatch(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 270
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v0

    move-object/from16 v1, p0

    move v5, v6

    move v6, v7

    move v7, v8

    move-object v8, v0

    invoke-direct/range {v1 .. v8}, Lcom/cronutils/model/time/ExecutionTime;->initDateTime(IIIIIILorg/joda/time/DateTimeZone;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 272
    :cond_4
    invoke-virtual {v2}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 273
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result v1

    invoke-virtual {v2, v1, v3}, Lcom/cronutils/model/time/TimeNode;->getPreviousValue(II)Lcom/cronutils/model/time/NearestValue;

    move-result-object v1

    .line 274
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v2

    if-lez v2, :cond_5

    .line 275
    new-instance v2, Lorg/joda/time/DateTime;

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v12

    const/4 v13, 0x1

    const/16 v14, 0x17

    const/16 v15, 0x3b

    const/16 v16, 0x3b

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v17

    move-object v10, v2

    invoke-direct/range {v10 .. v17}, Lorg/joda/time/DateTime;-><init>(IIIIIILorg/joda/time/DateTimeZone;)V

    .line 276
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v0

    invoke-virtual {v2, v0}, Lorg/joda/time/DateTime;->minusMonths(I)Lorg/joda/time/DateTime;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/time/DateTime$Property;->withMaximumValue()Lorg/joda/time/DateTime;

    move-result-object v0

    .line 277
    invoke-virtual {v9, v0}, Lcom/cronutils/model/time/ExecutionTime;->previousClosestMatch(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 279
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v3

    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getValue()I

    move-result v4

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v0

    move-object/from16 v1, p0

    move v5, v6

    move v6, v7

    move v7, v8

    move-object v8, v0

    invoke-direct/range {v1 .. v8}, Lcom/cronutils/model/time/ExecutionTime;->initDateTime(IIIIIILorg/joda/time/DateTimeZone;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 281
    :cond_6
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->hours:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v1}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getHourOfDay()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 282
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->hours:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getHourOfDay()I

    move-result v2

    invoke-virtual {v1, v2, v3}, Lcom/cronutils/model/time/TimeNode;->getPreviousValue(II)Lcom/cronutils/model/time/NearestValue;

    move-result-object v1

    .line 283
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v2

    if-lez v2, :cond_7

    .line 284
    new-instance v2, Lorg/joda/time/DateTime;

    .line 285
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v12

    .line 286
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result v13

    const/16 v14, 0x17

    const/16 v15, 0x3b

    const/16 v16, 0x3b

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v17

    move-object v10, v2

    invoke-direct/range {v10 .. v17}, Lorg/joda/time/DateTime;-><init>(IIIIIILorg/joda/time/DateTimeZone;)V

    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v0

    invoke-virtual {v2, v0}, Lorg/joda/time/DateTime;->minusDays(I)Lorg/joda/time/DateTime;

    move-result-object v0

    .line 287
    invoke-virtual {v9, v0}, Lcom/cronutils/model/time/ExecutionTime;->previousClosestMatch(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 289
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result v4

    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getValue()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v0

    move-object/from16 v1, p0

    move v6, v7

    move v7, v8

    move-object v8, v0

    invoke-direct/range {v1 .. v8}, Lcom/cronutils/model/time/ExecutionTime;->initDateTime(IIIIIILorg/joda/time/DateTimeZone;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 291
    :cond_8
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->minutes:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v1}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMinuteOfHour()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 292
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->minutes:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMinuteOfHour()I

    move-result v2

    invoke-virtual {v1, v2, v3}, Lcom/cronutils/model/time/TimeNode;->getPreviousValue(II)Lcom/cronutils/model/time/NearestValue;

    move-result-object v1

    .line 293
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v2

    if-lez v2, :cond_9

    .line 294
    new-instance v2, Lorg/joda/time/DateTime;

    .line 295
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v12

    .line 296
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result v13

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getHourOfDay()I

    move-result v14

    const/16 v15, 0x3b

    const/16 v16, 0x3b

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v17

    move-object v10, v2

    invoke-direct/range {v10 .. v17}, Lorg/joda/time/DateTime;-><init>(IIIIIILorg/joda/time/DateTimeZone;)V

    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v0

    invoke-virtual {v2, v0}, Lorg/joda/time/DateTime;->minusHours(I)Lorg/joda/time/DateTime;

    move-result-object v0

    .line 297
    invoke-virtual {v9, v0}, Lcom/cronutils/model/time/ExecutionTime;->previousClosestMatch(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 299
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result v4

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getHourOfDay()I

    move-result v5

    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getValue()I

    move-result v6

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v0

    move-object/from16 v1, p0

    move v7, v8

    move-object v8, v0

    invoke-direct/range {v1 .. v8}, Lcom/cronutils/model/time/ExecutionTime;->initDateTime(IIIIIILorg/joda/time/DateTimeZone;)Lorg/joda/time/DateTime;

    move-result-object v0

    return-object v0

    .line 301
    :cond_a
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->seconds:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual {v1}, Lcom/cronutils/model/time/TimeNode;->getValues()Ljava/util/List;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getSecondOfMinute()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 302
    iget-object v1, v9, Lcom/cronutils/model/time/ExecutionTime;->seconds:Lcom/cronutils/model/time/TimeNode;

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getSecondOfMinute()I

    move-result v2

    invoke-virtual {v1, v2, v3}, Lcom/cronutils/model/time/TimeNode;->getPreviousValue(II)Lcom/cronutils/model/time/NearestValue;

    move-result-object v1

    .line 303
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getValue()I

    move-result v7

    .line 304
    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v2

    if-lez v2, :cond_b

    .line 305
    new-instance v2, Lorg/joda/time/DateTime;

    .line 306
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v12

    .line 307
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result v13

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getHourOfDay()I

    move-result v14

    .line 308
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMinuteOfHour()I

    move-result v15

    const/16 v16, 0x3b

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v17

    move-object v10, v2

    invoke-direct/range {v10 .. v17}, Lorg/joda/time/DateTime;-><init>(IIIIIILorg/joda/time/DateTimeZone;)V

    invoke-virtual {v1}, Lcom/cronutils/model/time/NearestValue;->getShifts()I

    move-result v0

    invoke-virtual {v2, v0}, Lorg/joda/time/DateTime;->minusMinutes(I)Lorg/joda/time/DateTime;

    move-result-object v0

    .line 309
    :try_start_0
    invoke-virtual {v9, v0}, Lcom/cronutils/model/time/ExecutionTime;->previousClosestMatch(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 311
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getYear()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMonthOfYear()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getDayOfMonth()I

    move-result v4

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getHourOfDay()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getMinuteOfHour()I

    move-result v6

    invoke-virtual/range {p1 .. p1}, Lorg/joda/time/DateTime;->getZone()Lorg/joda/time/DateTimeZone;

    move-result-object v8

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/cronutils/model/time/ExecutionTime;->initDateTime(IIIIIILorg/joda/time/DateTimeZone;)Lorg/joda/time/DateTime;

    move-result-object v0

    :cond_c
    return-object v0

    :catch_0
    move-exception v0

    move-object v1, v0

    .line 309
    throw v1
.end method

.method public timeFromLastExecution(Lorg/joda/time/DateTime;)Lorg/joda/time/Duration;
    .locals 2

    .line 373
    new-instance v0, Lorg/joda/time/Interval;

    invoke-virtual {p0, p1}, Lcom/cronutils/model/time/ExecutionTime;->lastExecution(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/joda/time/Interval;-><init>(Lorg/joda/time/ReadableInstant;Lorg/joda/time/ReadableInstant;)V

    invoke-virtual {v0}, Lorg/joda/time/Interval;->toDuration()Lorg/joda/time/Duration;

    move-result-object p1

    return-object p1
.end method

.method public timeToNextExecution(Lorg/joda/time/DateTime;)Lorg/joda/time/Duration;
    .locals 2

    .line 346
    new-instance v0, Lorg/joda/time/Interval;

    invoke-virtual {p0, p1}, Lcom/cronutils/model/time/ExecutionTime;->nextExecution(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lorg/joda/time/Interval;-><init>(Lorg/joda/time/ReadableInstant;Lorg/joda/time/ReadableInstant;)V

    invoke-virtual {v0}, Lorg/joda/time/Interval;->toDuration()Lorg/joda/time/Duration;

    move-result-object p1

    return-object p1
.end method
