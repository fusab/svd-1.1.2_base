.class Lcom/cronutils/model/time/NearestValue;
.super Ljava/lang/Object;
.source "NearestValue.java"


# instance fields
.field private shifts:I

.field private value:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput p2, p0, Lcom/cronutils/model/time/NearestValue;->shifts:I

    .line 20
    iput p1, p0, Lcom/cronutils/model/time/NearestValue;->value:I

    return-void
.end method


# virtual methods
.method public getShifts()I
    .locals 1

    .line 28
    iget v0, p0, Lcom/cronutils/model/time/NearestValue;->shifts:I

    return v0
.end method

.method public getValue()I
    .locals 1

    .line 24
    iget v0, p0, Lcom/cronutils/model/time/NearestValue;->value:I

    return v0
.end method
