.class Lcom/cronutils/model/time/generator/AndDayOfWeekValueGenerator;
.super Lcom/cronutils/model/time/generator/FieldValueGenerator;
.source "AndDayOfWeekValueGenerator.java"


# instance fields
.field private mondayDoWValue:Lcom/cronutils/mapper/WeekDay;

.field private month:I

.field private year:I


# direct methods
.method public constructor <init>(Lcom/cronutils/model/field/CronField;IILcom/cronutils/mapper/WeekDay;)V
    .locals 2

    .line 19
    invoke-direct {p0, p1}, Lcom/cronutils/model/time/generator/FieldValueGenerator;-><init>(Lcom/cronutils/model/field/CronField;)V

    .line 20
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p1}, Lcom/cronutils/model/field/CronField;->getField()Lcom/cronutils/model/field/CronFieldName;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/cronutils/model/field/CronFieldName;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CronField does not belong to day of week"

    invoke-static {p1, v1, v0}, Lorg/apache/commons/lang3/Validate;->isTrue(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 21
    iput p2, p0, Lcom/cronutils/model/time/generator/AndDayOfWeekValueGenerator;->year:I

    .line 22
    iput p3, p0, Lcom/cronutils/model/time/generator/AndDayOfWeekValueGenerator;->month:I

    .line 23
    iput-object p4, p0, Lcom/cronutils/model/time/generator/AndDayOfWeekValueGenerator;->mondayDoWValue:Lcom/cronutils/mapper/WeekDay;

    return-void
.end method


# virtual methods
.method protected generateCandidatesNotIncludingIntervalExtremes(II)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 27
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/cronutils/model/time/generator/AndDayOfWeekValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v1

    check-cast v1, Lcom/cronutils/model/field/expression/And;

    .line 30
    invoke-virtual {v1}, Lcom/cronutils/model/field/expression/And;->getExpressions()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cronutils/model/field/expression/FieldExpression;

    .line 31
    new-instance v3, Lcom/cronutils/model/field/CronField;

    sget-object v4, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    iget-object v5, p0, Lcom/cronutils/model/time/generator/AndDayOfWeekValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v5}, Lcom/cronutils/model/field/CronField;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v5

    invoke-direct {v3, v4, v2, v5}, Lcom/cronutils/model/field/CronField;-><init>(Lcom/cronutils/model/field/CronFieldName;Lcom/cronutils/model/field/expression/FieldExpression;Lcom/cronutils/model/field/constraint/FieldConstraints;)V

    .line 32
    iget v2, p0, Lcom/cronutils/model/time/generator/AndDayOfWeekValueGenerator;->year:I

    iget v4, p0, Lcom/cronutils/model/time/generator/AndDayOfWeekValueGenerator;->month:I

    iget-object v5, p0, Lcom/cronutils/model/time/generator/AndDayOfWeekValueGenerator;->mondayDoWValue:Lcom/cronutils/mapper/WeekDay;

    invoke-static {v3, v2, v4, v5}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->createDayOfWeekValueGeneratorInstance(Lcom/cronutils/model/field/CronField;IILcom/cronutils/mapper/WeekDay;)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidates(II)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 36
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public generateNextValue(I)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cronutils/model/time/generator/NoSuchValueException;
        }
    .end annotation

    const/4 p1, 0x0

    return p1
.end method

.method public generatePreviousValue(I)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cronutils/model/time/generator/NoSuchValueException;
        }
    .end annotation

    const/4 p1, 0x0

    return p1
.end method

.method public isMatch(I)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method protected matchesFieldExpressionClass(Lcom/cronutils/model/field/expression/FieldExpression;)Z
    .locals 0

    .line 45
    instance-of p1, p1, Lcom/cronutils/model/field/expression/And;

    return p1
.end method
