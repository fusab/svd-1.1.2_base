.class Lcom/cronutils/model/time/generator/AndFieldValueGenerator$1;
.super Ljava/lang/Object;
.source "AndFieldValueGenerator.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/model/time/generator/AndFieldValueGenerator;->generateNextValue(I)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function<",
        "Lcom/cronutils/model/time/generator/FieldValueGenerator;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cronutils/model/time/generator/AndFieldValueGenerator;

.field final synthetic val$reference:I


# direct methods
.method constructor <init>(Lcom/cronutils/model/time/generator/AndFieldValueGenerator;I)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/cronutils/model/time/generator/AndFieldValueGenerator$1;->this$0:Lcom/cronutils/model/time/generator/AndFieldValueGenerator;

    iput p2, p0, Lcom/cronutils/model/time/generator/AndFieldValueGenerator$1;->val$reference:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/cronutils/model/time/generator/FieldValueGenerator;)Ljava/lang/Integer;
    .locals 1

    .line 38
    :try_start_0
    iget v0, p0, Lcom/cronutils/model/time/generator/AndFieldValueGenerator$1;->val$reference:I

    invoke-virtual {p1, v0}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateNextValue(I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1
    :try_end_0
    .catch Lcom/cronutils/model/time/generator/NoSuchValueException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 40
    :catch_0
    sget p1, Lcom/cronutils/model/time/generator/FieldValueGenerator;->NO_VALUE:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/cronutils/model/time/generator/FieldValueGenerator;

    invoke-virtual {p0, p1}, Lcom/cronutils/model/time/generator/AndFieldValueGenerator$1;->apply(Lcom/cronutils/model/time/generator/FieldValueGenerator;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method
