.class Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;
.super Lcom/cronutils/model/time/generator/FieldValueGenerator;
.source "OnDayOfMonthValueGenerator.java"


# instance fields
.field private month:I

.field private year:I


# direct methods
.method public constructor <init>(Lcom/cronutils/model/field/CronField;II)V
    .locals 2

    .line 28
    invoke-direct {p0, p1}, Lcom/cronutils/model/time/generator/FieldValueGenerator;-><init>(Lcom/cronutils/model/field/CronField;)V

    .line 29
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p1}, Lcom/cronutils/model/field/CronField;->getField()Lcom/cronutils/model/field/CronFieldName;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/cronutils/model/field/CronFieldName;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CronField does not belong to day of month"

    invoke-static {p1, v1, v0}, Lorg/apache/commons/lang3/Validate;->isTrue(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 30
    iput p2, p0, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;->year:I

    .line 31
    iput p3, p0, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;->month:I

    return-void
.end method

.method private generateValue(Lcom/cronutils/model/field/expression/On;II)I
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cronutils/model/time/generator/NoSuchValueException;
        }
    .end annotation

    .line 83
    invoke-virtual/range {p1 .. p1}, Lcom/cronutils/model/field/expression/On;->getTime()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 84
    sget-object v1, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator$1;->$SwitchMap$com$cronutils$model$field$value$SpecialChar:[I

    invoke-virtual/range {p1 .. p1}, Lcom/cronutils/model/field/expression/On;->getSpecialChar()Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;->getValue()Lcom/cronutils/model/field/value/SpecialChar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cronutils/model/field/value/SpecialChar;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v7, 0x1

    if-eq v1, v7, :cond_6

    const/4 v2, 0x2

    const/4 v8, 0x3

    if-eq v1, v2, :cond_2

    if-ne v1, v8, :cond_1

    .line 102
    new-instance v0, Lorg/joda/time/DateTime;

    new-instance v7, Lorg/joda/time/DateTime;

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object v1, v7

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-direct/range {v1 .. v6}, Lorg/joda/time/DateTime;-><init>(IIIII)V

    .line 104
    invoke-virtual {v7}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object v1

    invoke-virtual {v1}, Lorg/joda/time/DateTime$Property;->getMaximumValue()I

    move-result v12

    const/4 v13, 0x1

    const/4 v14, 0x1

    move-object v9, v0

    move/from16 v10, p2

    move/from16 v11, p3

    invoke-direct/range {v9 .. v14}, Lorg/joda/time/DateTime;-><init>(IIIII)V

    .line 105
    invoke-virtual {v0}, Lorg/joda/time/DateTime;->getDayOfWeek()I

    move-result v1

    add-int/lit8 v1, v1, -0x5

    if-lez v1, :cond_0

    .line 108
    invoke-virtual {v0, v1}, Lorg/joda/time/DateTime;->minusDays(I)Lorg/joda/time/DateTime;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/time/DateTime$Property;->get()I

    move-result v0

    return v0

    .line 110
    :cond_0
    invoke-virtual {v0}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/time/DateTime$Property;->get()I

    move-result v0

    return v0

    .line 112
    :cond_1
    new-instance v0, Lcom/cronutils/model/time/generator/NoSuchValueException;

    invoke-direct {v0}, Lcom/cronutils/model/time/generator/NoSuchValueException;-><init>()V

    throw v0

    .line 88
    :cond_2
    new-instance v9, Lorg/joda/time/DateTime;

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object v1, v9

    move/from16 v2, p2

    move/from16 v3, p3

    move v4, v0

    invoke-direct/range {v1 .. v6}, Lorg/joda/time/DateTime;-><init>(IIIII)V

    .line 89
    invoke-virtual {v9}, Lorg/joda/time/DateTime;->getDayOfWeek()I

    move-result v1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_4

    if-ne v0, v7, :cond_3

    return v8

    :cond_3
    sub-int/2addr v0, v7

    return v0

    .line 95
    :cond_4
    invoke-virtual {v9}, Lorg/joda/time/DateTime;->getDayOfWeek()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_5

    add-int/lit8 v1, v0, 0x1

    .line 96
    invoke-virtual {v9}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object v2

    invoke-virtual {v2}, Lorg/joda/time/DateTime$Property;->getMaximumValue()I

    move-result v2

    if-gt v1, v2, :cond_5

    return v1

    :cond_5
    return v0

    .line 86
    :cond_6
    new-instance v0, Lorg/joda/time/DateTime;

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v3, v0

    move/from16 v4, p2

    move/from16 v5, p3

    invoke-direct/range {v3 .. v8}, Lorg/joda/time/DateTime;-><init>(IIIII)V

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->dayOfMonth()Lorg/joda/time/DateTime$Property;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/time/DateTime$Property;->getMaximumValue()I

    move-result v0

    return v0
.end method


# virtual methods
.method protected generateCandidatesNotIncludingIntervalExtremes(II)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 57
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 59
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;->generateNextValue(I)I

    move-result p1

    :goto_0
    if-ge p1, p2, :cond_0

    .line 61
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    invoke-virtual {p0, p1}, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;->generateNextValue(I)I

    move-result p1
    :try_end_0
    .catch Lcom/cronutils/model/time/generator/NoSuchValueException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :cond_0
    return-object v0
.end method

.method public generateNextValue(I)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cronutils/model/time/generator/NoSuchValueException;
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v0}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    check-cast v0, Lcom/cronutils/model/field/expression/On;

    .line 37
    iget v1, p0, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;->year:I

    iget v2, p0, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;->month:I

    invoke-direct {p0, v0, v1, v2}, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;->generateValue(Lcom/cronutils/model/field/expression/On;II)I

    move-result v0

    if-le v0, p1, :cond_0

    return v0

    .line 40
    :cond_0
    new-instance p1, Lcom/cronutils/model/time/generator/NoSuchValueException;

    invoke-direct {p1}, Lcom/cronutils/model/time/generator/NoSuchValueException;-><init>()V

    throw p1
.end method

.method public generatePreviousValue(I)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cronutils/model/time/generator/NoSuchValueException;
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v0}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    check-cast v0, Lcom/cronutils/model/field/expression/On;

    .line 48
    iget v1, p0, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;->year:I

    iget v2, p0, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;->month:I

    invoke-direct {p0, v0, v1, v2}, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;->generateValue(Lcom/cronutils/model/field/expression/On;II)I

    move-result v0

    if-ge v0, p1, :cond_0

    return v0

    .line 50
    :cond_0
    new-instance p1, Lcom/cronutils/model/time/generator/NoSuchValueException;

    invoke-direct {p1}, Lcom/cronutils/model/time/generator/NoSuchValueException;-><init>()V

    throw p1
.end method

.method public isMatch(I)Z
    .locals 4

    .line 70
    iget-object v0, p0, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v0}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    check-cast v0, Lcom/cronutils/model/field/expression/On;

    const/4 v1, 0x0

    .line 72
    :try_start_0
    iget v2, p0, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;->year:I

    iget v3, p0, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;->month:I

    invoke-direct {p0, v0, v2, v3}, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;->generateValue(Lcom/cronutils/model/field/expression/On;II)I

    move-result v0
    :try_end_0
    .catch Lcom/cronutils/model/time/generator/NoSuchValueException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne p1, v0, :cond_0

    const/4 v1, 0x1

    :catch_0
    :cond_0
    return v1
.end method

.method protected matchesFieldExpressionClass(Lcom/cronutils/model/field/expression/FieldExpression;)Z
    .locals 0

    .line 79
    instance-of p1, p1, Lcom/cronutils/model/field/expression/On;

    return p1
.end method
