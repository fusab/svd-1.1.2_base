.class Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;
.super Lcom/cronutils/model/time/generator/FieldValueGenerator;
.source "EveryFieldValueGenerator.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    const-class v0, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/cronutils/model/field/CronField;)V
    .locals 3

    .line 29
    invoke-direct {p0, p1}, Lcom/cronutils/model/time/generator/FieldValueGenerator;-><init>(Lcom/cronutils/model/field/CronField;)V

    .line 30
    sget-object v0, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;->log:Lorg/slf4j/Logger;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 32
    invoke-virtual {p1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/expression/FieldExpression;->asString()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {}, Lorg/joda/time/DateTime;->now()Lorg/joda/time/DateTime;

    move-result-object p1

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const-string p1, "processing \"%s\" at %s"

    .line 30
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected generateCandidatesNotIncludingIntervalExtremes(II)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 71
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 73
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;->generateNextValue(I)I

    move-result p1

    :goto_0
    if-ge p1, p2, :cond_0

    .line 75
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    invoke-virtual {p0, p1}, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;->generateNextValue(I)I

    move-result p1
    :try_end_0
    .catch Lcom/cronutils/model/time/generator/NoSuchValueException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 79
    sget-object p2, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;->log:Lorg/slf4j/Logger;

    const-string v1, "Failed to generate candidates"

    invoke-interface {p2, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return-object v0
.end method

.method public generateNextValue(I)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cronutils/model/time/generator/NoSuchValueException;
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v0}, Lcom/cronutils/model/field/CronField;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getEndRange()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 42
    iget-object v0, p0, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v0}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    check-cast v0, Lcom/cronutils/model/field/expression/Every;

    .line 43
    invoke-virtual {p0}, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;->offset()I

    move-result v1

    sub-int v1, p1, v1

    .line 44
    invoke-virtual {v0}, Lcom/cronutils/model/field/expression/Every;->getPeriod()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 45
    rem-int/2addr v1, v0

    sub-int/2addr v0, v1

    add-int/2addr p1, v0

    .line 48
    iget-object v0, p0, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v0}, Lcom/cronutils/model/field/CronField;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getStartRange()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 49
    iget-object p1, p0, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {p1}, Lcom/cronutils/model/field/CronField;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getStartRange()I

    move-result p1

    return p1

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v0}, Lcom/cronutils/model/field/CronField;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getEndRange()I

    move-result v0

    if-gt p1, v0, :cond_1

    return p1

    .line 52
    :cond_1
    new-instance p1, Lcom/cronutils/model/time/generator/NoSuchValueException;

    invoke-direct {p1}, Lcom/cronutils/model/time/generator/NoSuchValueException;-><init>()V

    throw p1

    .line 40
    :cond_2
    new-instance p1, Lcom/cronutils/model/time/generator/NoSuchValueException;

    invoke-direct {p1}, Lcom/cronutils/model/time/generator/NoSuchValueException;-><init>()V

    throw p1
.end method

.method public generatePreviousValue(I)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cronutils/model/time/generator/NoSuchValueException;
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v0}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    check-cast v0, Lcom/cronutils/model/field/expression/Every;

    .line 60
    invoke-virtual {v0}, Lcom/cronutils/model/field/expression/Every;->getPeriod()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 61
    rem-int v1, p1, v0

    if-nez v1, :cond_0

    sub-int/2addr p1, v0

    return p1

    :cond_0
    sub-int/2addr p1, v1

    return p1
.end method

.method public isMatch(I)Z
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v0}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    check-cast v0, Lcom/cronutils/model/field/expression/Every;

    .line 87
    iget-object v1, p0, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v1}, Lcom/cronutils/model/field/CronField;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getStartRange()I

    move-result v1

    sub-int/2addr p1, v1

    .line 88
    invoke-virtual {v0}, Lcom/cronutils/model/field/expression/Every;->getPeriod()Lcom/cronutils/model/field/value/IntegerFieldValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/value/IntegerFieldValue;->getValue()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    rem-int/2addr p1, v0

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method protected matchesFieldExpressionClass(Lcom/cronutils/model/field/expression/FieldExpression;)Z
    .locals 0

    .line 93
    instance-of p1, p1, Lcom/cronutils/model/field/expression/Every;

    return p1
.end method

.method offset()I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v0}, Lcom/cronutils/model/field/CronField;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getStartRange()I

    move-result v0

    return v0
.end method
