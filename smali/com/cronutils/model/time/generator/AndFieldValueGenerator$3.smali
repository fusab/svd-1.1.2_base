.class Lcom/cronutils/model/time/generator/AndFieldValueGenerator$3;
.super Ljava/lang/Object;
.source "AndFieldValueGenerator.java"

# interfaces
.implements Lcom/google/common/base/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cronutils/model/time/generator/AndFieldValueGenerator;->computeCandidates(Lcom/google/common/base/Function;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Predicate<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cronutils/model/time/generator/AndFieldValueGenerator;


# direct methods
.method constructor <init>(Lcom/cronutils/model/time/generator/AndFieldValueGenerator;)V
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/cronutils/model/time/generator/AndFieldValueGenerator$3;->this$0:Lcom/cronutils/model/time/generator/AndFieldValueGenerator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Ljava/lang/Integer;)Z
    .locals 0

    .line 112
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 0

    .line 109
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/cronutils/model/time/generator/AndFieldValueGenerator$3;->apply(Ljava/lang/Integer;)Z

    move-result p1

    return p1
.end method
