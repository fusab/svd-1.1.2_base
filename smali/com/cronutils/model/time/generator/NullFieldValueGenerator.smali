.class Lcom/cronutils/model/time/generator/NullFieldValueGenerator;
.super Lcom/cronutils/model/time/generator/FieldValueGenerator;
.source "NullFieldValueGenerator.java"


# direct methods
.method public constructor <init>(Lcom/cronutils/model/field/CronField;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/cronutils/model/time/generator/FieldValueGenerator;-><init>(Lcom/cronutils/model/field/CronField;)V

    return-void
.end method


# virtual methods
.method protected generateCandidatesNotIncludingIntervalExtremes(II)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 37
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method public generateNextValue(I)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cronutils/model/time/generator/NoSuchValueException;
        }
    .end annotation

    .line 27
    new-instance p1, Lcom/cronutils/model/time/generator/NoSuchValueException;

    invoke-direct {p1}, Lcom/cronutils/model/time/generator/NoSuchValueException;-><init>()V

    throw p1
.end method

.method public generatePreviousValue(I)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cronutils/model/time/generator/NoSuchValueException;
        }
    .end annotation

    .line 32
    new-instance p1, Lcom/cronutils/model/time/generator/NoSuchValueException;

    invoke-direct {p1}, Lcom/cronutils/model/time/generator/NoSuchValueException;-><init>()V

    throw p1
.end method

.method public isMatch(I)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method protected matchesFieldExpressionClass(Lcom/cronutils/model/field/expression/FieldExpression;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method
