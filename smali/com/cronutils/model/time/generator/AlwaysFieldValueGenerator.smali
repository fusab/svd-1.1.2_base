.class Lcom/cronutils/model/time/generator/AlwaysFieldValueGenerator;
.super Lcom/cronutils/model/time/generator/FieldValueGenerator;
.source "AlwaysFieldValueGenerator.java"


# direct methods
.method public constructor <init>(Lcom/cronutils/model/field/CronField;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/cronutils/model/time/generator/FieldValueGenerator;-><init>(Lcom/cronutils/model/field/CronField;)V

    return-void
.end method


# virtual methods
.method protected generateCandidatesNotIncludingIntervalExtremes(II)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 48
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    .line 50
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public generateNextValue(I)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cronutils/model/time/generator/NoSuchValueException;
        }
    .end annotation

    add-int/lit8 p1, p1, 0x1

    .line 29
    iget-object v0, p0, Lcom/cronutils/model/time/generator/AlwaysFieldValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v0}, Lcom/cronutils/model/field/CronField;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getEndRange()I

    move-result v0

    if-gt p1, v0, :cond_0

    return p1

    .line 32
    :cond_0
    new-instance p1, Lcom/cronutils/model/time/generator/NoSuchValueException;

    invoke-direct {p1}, Lcom/cronutils/model/time/generator/NoSuchValueException;-><init>()V

    throw p1
.end method

.method public generatePreviousValue(I)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cronutils/model/time/generator/NoSuchValueException;
        }
    .end annotation

    add-int/lit8 p1, p1, -0x1

    .line 39
    iget-object v0, p0, Lcom/cronutils/model/time/generator/AlwaysFieldValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v0}, Lcom/cronutils/model/field/CronField;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/constraint/FieldConstraints;->getStartRange()I

    move-result v0

    if-lt p1, v0, :cond_0

    return p1

    .line 42
    :cond_0
    new-instance p1, Lcom/cronutils/model/time/generator/NoSuchValueException;

    invoke-direct {p1}, Lcom/cronutils/model/time/generator/NoSuchValueException;-><init>()V

    throw p1
.end method

.method public isMatch(I)Z
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/cronutils/model/time/generator/AlwaysFieldValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    invoke-virtual {v0}, Lcom/cronutils/model/field/CronField;->getConstraints()Lcom/cronutils/model/field/constraint/FieldConstraints;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/cronutils/model/field/constraint/FieldConstraints;->isInRange(I)Z

    move-result p1

    return p1
.end method

.method protected matchesFieldExpressionClass(Lcom/cronutils/model/field/expression/FieldExpression;)Z
    .locals 0

    .line 62
    instance-of p1, p1, Lcom/cronutils/model/field/expression/Always;

    return p1
.end method
