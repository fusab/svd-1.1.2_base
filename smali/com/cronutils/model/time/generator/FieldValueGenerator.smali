.class public abstract Lcom/cronutils/model/time/generator/FieldValueGenerator;
.super Ljava/lang/Object;
.source "FieldValueGenerator.java"


# static fields
.field protected static NO_VALUE:I = -0x80000000


# instance fields
.field protected cronField:Lcom/cronutils/model/field/CronField;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/cronutils/model/field/CronField;)V
    .locals 3

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 33
    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "CronField must not be null"

    invoke-static {p1, v2, v1}, Lorg/apache/commons/lang3/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cronutils/model/field/CronField;

    iput-object v1, p0, Lcom/cronutils/model/time/generator/FieldValueGenerator;->cronField:Lcom/cronutils/model/field/CronField;

    .line 34
    invoke-virtual {p1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->matchesFieldExpressionClass(Lcom/cronutils/model/field/expression/FieldExpression;)Z

    move-result p1

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "FieldExpression does not match required class"

    invoke-static {p1, v1, v0}, Lorg/apache/commons/lang3/Validate;->isTrue(ZLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final generateCandidates(II)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 56
    invoke-virtual {p0, p1, p2}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->generateCandidatesNotIncludingIntervalExtremes(II)Ljava/util/List;

    move-result-object v0

    .line 57
    invoke-virtual {p0, p1}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->isMatch(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    :cond_0
    invoke-virtual {p0, p2}, Lcom/cronutils/model/time/generator/FieldValueGenerator;->isMatch(I)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 61
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-object v0
.end method

.method protected abstract generateCandidatesNotIncludingIntervalExtremes(II)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract generateNextValue(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cronutils/model/time/generator/NoSuchValueException;
        }
    .end annotation
.end method

.method public abstract generatePreviousValue(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cronutils/model/time/generator/NoSuchValueException;
        }
    .end annotation
.end method

.method public abstract isMatch(I)Z
.end method

.method protected abstract matchesFieldExpressionClass(Lcom/cronutils/model/field/expression/FieldExpression;)Z
.end method
