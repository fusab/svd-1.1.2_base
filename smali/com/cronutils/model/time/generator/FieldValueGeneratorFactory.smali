.class public Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;
.super Ljava/lang/Object;
.source "FieldValueGeneratorFactory.java"


# static fields
.field private static factory:Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    const-class v0, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->log:Lorg/slf4j/Logger;

    .line 25
    new-instance v0, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;

    invoke-direct {v0}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;-><init>()V

    sput-object v0, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->factory:Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createDayOfMonthValueGeneratorInstance(Lcom/cronutils/model/field/CronField;II)Lcom/cronutils/model/time/generator/FieldValueGenerator;
    .locals 2

    .line 57
    invoke-virtual {p0}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    .line 58
    instance-of v1, v0, Lcom/cronutils/model/field/expression/On;

    if-eqz v1, :cond_0

    .line 59
    check-cast v0, Lcom/cronutils/model/field/expression/On;

    .line 60
    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->NONE:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-virtual {v0}, Lcom/cronutils/model/field/expression/On;->getSpecialChar()Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;->getValue()Lcom/cronutils/model/field/value/SpecialChar;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/cronutils/model/field/value/SpecialChar;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;

    invoke-direct {v0, p0, p1, p2}, Lcom/cronutils/model/time/generator/OnDayOfMonthValueGenerator;-><init>(Lcom/cronutils/model/field/CronField;II)V

    return-object v0

    .line 64
    :cond_0
    invoke-static {p0}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->forCronField(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p0

    return-object p0
.end method

.method public static createDayOfWeekValueGeneratorInstance(Lcom/cronutils/model/field/CronField;IILcom/cronutils/mapper/WeekDay;)Lcom/cronutils/model/time/generator/FieldValueGenerator;
    .locals 2

    .line 68
    invoke-virtual {p0}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    .line 69
    instance-of v1, v0, Lcom/cronutils/model/field/expression/On;

    if-eqz v1, :cond_0

    .line 70
    new-instance v0, Lcom/cronutils/model/time/generator/OnDayOfWeekValueGenerator;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/cronutils/model/time/generator/OnDayOfWeekValueGenerator;-><init>(Lcom/cronutils/model/field/CronField;IILcom/cronutils/mapper/WeekDay;)V

    return-object v0

    .line 73
    :cond_0
    instance-of v1, v0, Lcom/cronutils/model/field/expression/Between;

    if-eqz v1, :cond_1

    .line 74
    new-instance v0, Lcom/cronutils/model/time/generator/BetweenDayOfWeekValueGenerator;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/cronutils/model/time/generator/BetweenDayOfWeekValueGenerator;-><init>(Lcom/cronutils/model/field/CronField;IILcom/cronutils/mapper/WeekDay;)V

    return-object v0

    .line 77
    :cond_1
    instance-of v0, v0, Lcom/cronutils/model/field/expression/And;

    if-eqz v0, :cond_2

    .line 78
    new-instance v0, Lcom/cronutils/model/time/generator/AndDayOfWeekValueGenerator;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/cronutils/model/time/generator/AndDayOfWeekValueGenerator;-><init>(Lcom/cronutils/model/field/CronField;IILcom/cronutils/mapper/WeekDay;)V

    return-object v0

    .line 80
    :cond_2
    invoke-static {p0}, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->forCronField(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/generator/FieldValueGenerator;

    move-result-object p0

    return-object p0
.end method

.method public static forCronField(Lcom/cronutils/model/field/CronField;)Lcom/cronutils/model/time/generator/FieldValueGenerator;
    .locals 3

    .line 33
    invoke-virtual {p0}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    .line 34
    instance-of v1, v0, Lcom/cronutils/model/field/expression/Always;

    if-eqz v1, :cond_0

    .line 35
    new-instance v0, Lcom/cronutils/model/time/generator/AlwaysFieldValueGenerator;

    invoke-direct {v0, p0}, Lcom/cronutils/model/time/generator/AlwaysFieldValueGenerator;-><init>(Lcom/cronutils/model/field/CronField;)V

    return-object v0

    .line 37
    :cond_0
    instance-of v1, v0, Lcom/cronutils/model/field/expression/And;

    if-eqz v1, :cond_1

    .line 38
    new-instance v0, Lcom/cronutils/model/time/generator/AndFieldValueGenerator;

    invoke-direct {v0, p0}, Lcom/cronutils/model/time/generator/AndFieldValueGenerator;-><init>(Lcom/cronutils/model/field/CronField;)V

    return-object v0

    .line 40
    :cond_1
    instance-of v1, v0, Lcom/cronutils/model/field/expression/Between;

    if-eqz v1, :cond_2

    .line 41
    new-instance v0, Lcom/cronutils/model/time/generator/BetweenFieldValueGenerator;

    invoke-direct {v0, p0}, Lcom/cronutils/model/time/generator/BetweenFieldValueGenerator;-><init>(Lcom/cronutils/model/field/CronField;)V

    return-object v0

    .line 43
    :cond_2
    instance-of v1, v0, Lcom/cronutils/model/field/expression/Every;

    if-eqz v1, :cond_3

    .line 44
    new-instance v0, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;

    invoke-direct {v0, p0}, Lcom/cronutils/model/time/generator/EveryFieldValueGenerator;-><init>(Lcom/cronutils/model/field/CronField;)V

    return-object v0

    .line 46
    :cond_3
    instance-of v1, v0, Lcom/cronutils/model/field/expression/On;

    if-eqz v1, :cond_5

    .line 47
    check-cast v0, Lcom/cronutils/model/field/expression/On;

    .line 48
    sget-object v1, Lcom/cronutils/model/field/value/SpecialChar;->NONE:Lcom/cronutils/model/field/value/SpecialChar;

    invoke-virtual {v0}, Lcom/cronutils/model/field/expression/On;->getSpecialChar()Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cronutils/model/field/value/SpecialCharFieldValue;->getValue()Lcom/cronutils/model/field/value/SpecialChar;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/cronutils/model/field/value/SpecialChar;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 51
    new-instance v0, Lcom/cronutils/model/time/generator/OnFieldValueGenerator;

    invoke-direct {v0, p0}, Lcom/cronutils/model/time/generator/OnFieldValueGenerator;-><init>(Lcom/cronutils/model/field/CronField;)V

    return-object v0

    .line 49
    :cond_4
    new-instance p0, Ljava/lang/RuntimeException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/cronutils/model/field/expression/On;->getSpecialChar()Lcom/cronutils/model/field/value/SpecialCharFieldValue;

    move-result-object v0

    aput-object v0, v1, v2

    const-string v0, "Cannot create instance for On instance with %s value"

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 53
    :cond_5
    new-instance v0, Lcom/cronutils/model/time/generator/NullFieldValueGenerator;

    invoke-direct {v0, p0}, Lcom/cronutils/model/time/generator/NullFieldValueGenerator;-><init>(Lcom/cronutils/model/field/CronField;)V

    return-object v0
.end method

.method public static instance()Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;
    .locals 1

    .line 29
    sget-object v0, Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;->factory:Lcom/cronutils/model/time/generator/FieldValueGeneratorFactory;

    return-object v0
.end method
