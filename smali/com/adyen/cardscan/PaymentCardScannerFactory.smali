.class public interface abstract Lcom/adyen/cardscan/PaymentCardScannerFactory;
.super Ljava/lang/Object;
.source "PaymentCardScannerFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/cardscan/PaymentCardScannerFactory$Loader;
    }
.end annotation


# static fields
.field public static final MANIFEST_KEY:Ljava/lang/String; = "CheckoutPaymentCardScannerFactory"


# virtual methods
.method public abstract getPaymentCardScanners(Landroid/app/Activity;)Ljava/util/List;
    .param p1    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            ")",
            "Ljava/util/List<",
            "Lcom/adyen/cardscan/PaymentCardScanner;",
            ">;"
        }
    .end annotation
.end method
