.class public final Lcom/adyen/cardscan/PaymentCardScannerFactory$Loader;
.super Ljava/lang/Object;
.source "PaymentCardScannerFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/cardscan/PaymentCardScannerFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Loader"
.end annotation


# static fields
.field private static instance:Lcom/adyen/cardscan/PaymentCardScannerFactory;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPaymentCardScannerFactory(Landroid/content/Context;)Lcom/adyen/cardscan/PaymentCardScannerFactory;
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 35
    sget-object v0, Lcom/adyen/cardscan/PaymentCardScannerFactory$Loader;->instance:Lcom/adyen/cardscan/PaymentCardScannerFactory;

    if-nez v0, :cond_0

    .line 36
    invoke-static {p0}, Lcom/adyen/cardscan/PaymentCardScannerFactory$Loader;->instantiate(Landroid/content/Context;)Lcom/adyen/cardscan/PaymentCardScannerFactory;

    move-result-object p0

    sput-object p0, Lcom/adyen/cardscan/PaymentCardScannerFactory$Loader;->instance:Lcom/adyen/cardscan/PaymentCardScannerFactory;

    .line 39
    :cond_0
    sget-object p0, Lcom/adyen/cardscan/PaymentCardScannerFactory$Loader;->instance:Lcom/adyen/cardscan/PaymentCardScannerFactory;

    return-object p0
.end method

.method private static instantiate(Landroid/content/Context;)Lcom/adyen/cardscan/PaymentCardScannerFactory;
    .locals 3
    .param p0    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 v0, 0x0

    .line 45
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 46
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    const/16 v2, 0x80

    invoke-virtual {p0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object p0

    .line 47
    iget-object v1, p0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    iget-object p0, p0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v1, "CheckoutPaymentCardScannerFactory"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    move-object p0, v0

    :goto_0
    if-eqz p0, :cond_1

    .line 50
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/adyen/cardscan/PaymentCardScannerFactory;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    :cond_1
    return-object v0
.end method
