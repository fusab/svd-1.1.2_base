.class public abstract Lcom/adyen/cardscan/PaymentCardScanner;
.super Ljava/lang/Object;
.source "PaymentCardScanner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/cardscan/PaymentCardScanner$Listener;
    }
.end annotation


# instance fields
.field private activity:Landroid/app/Activity;

.field private listener:Lcom/adyen/cardscan/PaymentCardScanner$Listener;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/adyen/cardscan/PaymentCardScanner;->activity:Landroid/app/Activity;

    return-void
.end method

.method static synthetic access$000(Lcom/adyen/cardscan/PaymentCardScanner;)Lcom/adyen/cardscan/PaymentCardScanner$Listener;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/adyen/cardscan/PaymentCardScanner;->listener:Lcom/adyen/cardscan/PaymentCardScanner$Listener;

    return-object p0
.end method


# virtual methods
.method public getActivity()Landroid/app/Activity;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 77
    iget-object v0, p0, Lcom/adyen/cardscan/PaymentCardScanner;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method public abstract getDisplayDescription()Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract getDisplayIcon()Landroid/graphics/drawable/Drawable;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method protected notifyScanCompleted(Lcom/adyen/cardscan/PaymentCard;)V
    .locals 2
    .param p1    # Lcom/adyen/cardscan/PaymentCard;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 132
    iget-object v0, p0, Lcom/adyen/cardscan/PaymentCardScanner;->listener:Lcom/adyen/cardscan/PaymentCardScanner$Listener;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/adyen/cardscan/PaymentCardScanner;->activity:Landroid/app/Activity;

    new-instance v1, Lcom/adyen/cardscan/PaymentCardScanner$2;

    invoke-direct {v1, p0, p1}, Lcom/adyen/cardscan/PaymentCardScanner$2;-><init>(Lcom/adyen/cardscan/PaymentCardScanner;Lcom/adyen/cardscan/PaymentCard;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method protected notifyScanError(Ljava/lang/Throwable;)V
    .locals 2
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 146
    iget-object v0, p0, Lcom/adyen/cardscan/PaymentCardScanner;->listener:Lcom/adyen/cardscan/PaymentCardScanner$Listener;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/adyen/cardscan/PaymentCardScanner;->activity:Landroid/app/Activity;

    new-instance v1, Lcom/adyen/cardscan/PaymentCardScanner$3;

    invoke-direct {v1, p0, p1}, Lcom/adyen/cardscan/PaymentCardScanner$3;-><init>(Lcom/adyen/cardscan/PaymentCardScanner;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method protected notifyScanStarted()V
    .locals 2

    .line 118
    iget-object v0, p0, Lcom/adyen/cardscan/PaymentCardScanner;->listener:Lcom/adyen/cardscan/PaymentCardScanner$Listener;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/adyen/cardscan/PaymentCardScanner;->activity:Landroid/app/Activity;

    new-instance v1, Lcom/adyen/cardscan/PaymentCardScanner$1;

    invoke-direct {v1, p0}, Lcom/adyen/cardscan/PaymentCardScanner$1;-><init>(Lcom/adyen/cardscan/PaymentCardScanner;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p3    # Landroid/content/Intent;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    return-void
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 0

    return-void
.end method

.method public setListener(Lcom/adyen/cardscan/PaymentCardScanner$Listener;)V
    .locals 0
    .param p1    # Lcom/adyen/cardscan/PaymentCardScanner$Listener;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 86
    iput-object p1, p0, Lcom/adyen/cardscan/PaymentCardScanner;->listener:Lcom/adyen/cardscan/PaymentCardScanner$Listener;

    return-void
.end method

.method protected startActivityForResult(Landroid/content/Intent;I)V
    .locals 1
    .param p1    # Landroid/content/Intent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    .line 96
    invoke-virtual {p0, p1, p2, v0}, Lcom/adyen/cardscan/PaymentCardScanner;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    return-void
.end method

.method protected startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/content/Intent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 107
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 108
    iget-object v0, p0, Lcom/adyen/cardscan/PaymentCardScanner;->activity:Landroid/app/Activity;

    invoke-virtual {v0, p1, p2, p3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    goto :goto_0

    .line 110
    :cond_0
    iget-object p3, p0, Lcom/adyen/cardscan/PaymentCardScanner;->activity:Landroid/app/Activity;

    invoke-virtual {p3, p1, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void
.end method

.method public abstract startScan()V
.end method
