.class public final Lcom/adyen/cardscan/PaymentCard;
.super Ljava/lang/Object;
.source "PaymentCard.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/cardscan/PaymentCard$Builder;
    }
.end annotation


# instance fields
.field private cardNumber:Ljava/lang/String;

.field private expiryMonth:Ljava/lang/Integer;

.field private expiryYear:Ljava/lang/Integer;

.field private securityCode:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/adyen/cardscan/PaymentCard$1;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/adyen/cardscan/PaymentCard;-><init>()V

    return-void
.end method

.method static synthetic access$102(Lcom/adyen/cardscan/PaymentCard;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 9
    iput-object p1, p0, Lcom/adyen/cardscan/PaymentCard;->cardNumber:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/adyen/cardscan/PaymentCard;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .line 9
    iput-object p1, p0, Lcom/adyen/cardscan/PaymentCard;->expiryMonth:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$302(Lcom/adyen/cardscan/PaymentCard;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .line 9
    iput-object p1, p0, Lcom/adyen/cardscan/PaymentCard;->expiryYear:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$402(Lcom/adyen/cardscan/PaymentCard;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 9
    iput-object p1, p0, Lcom/adyen/cardscan/PaymentCard;->securityCode:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public getCardNumber()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/adyen/cardscan/PaymentCard;->cardNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiryMonth()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/adyen/cardscan/PaymentCard;->expiryMonth:Ljava/lang/Integer;

    return-object v0
.end method

.method public getExpiryYear()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/adyen/cardscan/PaymentCard;->expiryYear:Ljava/lang/Integer;

    return-object v0
.end method

.method public getSecurityCode()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/adyen/cardscan/PaymentCard;->securityCode:Ljava/lang/String;

    return-object v0
.end method
