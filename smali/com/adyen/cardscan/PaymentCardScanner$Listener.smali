.class public interface abstract Lcom/adyen/cardscan/PaymentCardScanner$Listener;
.super Ljava/lang/Object;
.source "PaymentCardScanner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/cardscan/PaymentCardScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onScanCompleted(Lcom/adyen/cardscan/PaymentCardScanner;Lcom/adyen/cardscan/PaymentCard;)V
    .param p1    # Lcom/adyen/cardscan/PaymentCardScanner;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/adyen/cardscan/PaymentCard;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onScanError(Lcom/adyen/cardscan/PaymentCardScanner;Ljava/lang/Throwable;)V
    .param p1    # Lcom/adyen/cardscan/PaymentCardScanner;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract onScanStarted(Lcom/adyen/cardscan/PaymentCardScanner;)V
    .param p1    # Lcom/adyen/cardscan/PaymentCardScanner;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method
