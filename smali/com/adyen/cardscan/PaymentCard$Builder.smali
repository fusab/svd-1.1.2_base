.class public final Lcom/adyen/cardscan/PaymentCard$Builder;
.super Ljava/lang/Object;
.source "PaymentCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/cardscan/PaymentCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private mPaymentCard:Lcom/adyen/cardscan/PaymentCard;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Lcom/adyen/cardscan/PaymentCard;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/adyen/cardscan/PaymentCard;-><init>(Lcom/adyen/cardscan/PaymentCard$1;)V

    iput-object v0, p0, Lcom/adyen/cardscan/PaymentCard$Builder;->mPaymentCard:Lcom/adyen/cardscan/PaymentCard;

    return-void
.end method


# virtual methods
.method public build()Lcom/adyen/cardscan/PaymentCard;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/adyen/cardscan/PaymentCard$Builder;->mPaymentCard:Lcom/adyen/cardscan/PaymentCard;

    return-object v0
.end method

.method public setCardNumber(Ljava/lang/String;)Lcom/adyen/cardscan/PaymentCard$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/adyen/cardscan/PaymentCard$Builder;->mPaymentCard:Lcom/adyen/cardscan/PaymentCard;

    invoke-static {v0, p1}, Lcom/adyen/cardscan/PaymentCard;->access$102(Lcom/adyen/cardscan/PaymentCard;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setExpiryMonth(Ljava/lang/Integer;)Lcom/adyen/cardscan/PaymentCard$Builder;
    .locals 1
    .param p1    # Ljava/lang/Integer;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/adyen/cardscan/PaymentCard$Builder;->mPaymentCard:Lcom/adyen/cardscan/PaymentCard;

    invoke-static {v0, p1}, Lcom/adyen/cardscan/PaymentCard;->access$202(Lcom/adyen/cardscan/PaymentCard;Ljava/lang/Integer;)Ljava/lang/Integer;

    return-object p0
.end method

.method public setExpiryYear(Ljava/lang/Integer;)Lcom/adyen/cardscan/PaymentCard$Builder;
    .locals 1
    .param p1    # Ljava/lang/Integer;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/adyen/cardscan/PaymentCard$Builder;->mPaymentCard:Lcom/adyen/cardscan/PaymentCard;

    invoke-static {v0, p1}, Lcom/adyen/cardscan/PaymentCard;->access$302(Lcom/adyen/cardscan/PaymentCard;Ljava/lang/Integer;)Ljava/lang/Integer;

    return-object p0
.end method

.method public setSecurityCode(Ljava/lang/String;)Lcom/adyen/cardscan/PaymentCard$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/adyen/cardscan/PaymentCard$Builder;->mPaymentCard:Lcom/adyen/cardscan/PaymentCard;

    invoke-static {v0, p1}, Lcom/adyen/cardscan/PaymentCard;->access$402(Lcom/adyen/cardscan/PaymentCard;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method
