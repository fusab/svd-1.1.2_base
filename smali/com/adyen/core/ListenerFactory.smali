.class final Lcom/adyen/core/ListenerFactory;
.super Ljava/lang/Object;
.source "ListenerFactory.java"


# static fields
.field private static final DEFAULT_PAYMENT_REQUEST_DETAILS_LISTENER:Ljava/lang/String; = "com.adyen.ui.DefaultPaymentRequestDetailsListener"

.field private static final DEFAULT_PAYMENT_REQUEST_LISTENER:Ljava/lang/String; = "com.adyen.ui.DefaultPaymentRequestListener"

.field private static final ERROR_MESSAGE:Ljava/lang/String; = "UI module not available. Import adyen-ui or provide a PaymentRequestDetailsListener to PaymentRequest."


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static createAdyenPaymentRequestDetailsListener(Landroid/content/Context;)Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/adyen/core/exceptions/UIModuleNotAvailableException;
        }
    .end annotation

    const-string v0, "com.adyen.ui.DefaultPaymentRequestDetailsListener"

    .line 32
    invoke-static {p0, v0}, Lcom/adyen/core/ListenerFactory;->getClassInstance(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;

    if-eqz p0, :cond_0

    return-object p0

    .line 34
    :cond_0
    new-instance p0, Lcom/adyen/core/exceptions/UIModuleNotAvailableException;

    const-string v0, "UI module not available. Import adyen-ui or provide a PaymentRequestDetailsListener to PaymentRequest."

    invoke-direct {p0, v0}, Lcom/adyen/core/exceptions/UIModuleNotAvailableException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method static createAdyenPaymentRequestListener(Landroid/content/Context;)Lcom/adyen/core/interfaces/PaymentRequestListener;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/adyen/core/exceptions/UIModuleNotAvailableException;
        }
    .end annotation

    const-string v0, "com.adyen.ui.DefaultPaymentRequestListener"

    .line 23
    invoke-static {p0, v0}, Lcom/adyen/core/ListenerFactory;->getClassInstance(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/adyen/core/interfaces/PaymentRequestListener;

    if-eqz p0, :cond_0

    return-object p0

    .line 25
    :cond_0
    new-instance p0, Lcom/adyen/core/exceptions/UIModuleNotAvailableException;

    const-string v0, "UI module not available. Import adyen-ui or provide a PaymentRequestDetailsListener to PaymentRequest."

    invoke-direct {p0, v0}, Lcom/adyen/core/exceptions/UIModuleNotAvailableException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static getClassInstance(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;
    .locals 4

    .line 42
    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p1

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Class;

    const-class v2, Landroid/content/Context;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p1, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object p1

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p0, v0, v3

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    .line 52
    invoke-virtual {p0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 50
    invoke-virtual {p0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 48
    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception p0

    .line 46
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception p0

    .line 44
    invoke-virtual {p0}, Ljava/lang/InstantiationException;->printStackTrace()V

    :goto_0
    const/4 p0, 0x0

    :goto_1
    return-object p0
.end method
