.class Lcom/adyen/core/PaymentBroadcastReceivers$1;
.super Ljava/lang/Object;
.source "PaymentBroadcastReceivers.java"

# interfaces
.implements Lcom/adyen/core/interfaces/PaymentMethodCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/PaymentBroadcastReceivers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/core/PaymentBroadcastReceivers;


# direct methods
.method constructor <init>(Lcom/adyen/core/PaymentBroadcastReceivers;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/adyen/core/PaymentBroadcastReceivers$1;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public completionWithPaymentMethod(Lcom/adyen/core/models/PaymentMethod;)V
    .locals 1
    .param p1    # Lcom/adyen/core/models/PaymentMethod;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 41
    iget-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers$1;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-static {v0}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$000(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/PaymentStateHandler;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/adyen/core/PaymentStateHandler;->setPaymentMethod(Lcom/adyen/core/models/PaymentMethod;)V

    .line 43
    invoke-virtual {p1}, Lcom/adyen/core/models/PaymentMethod;->getInputDetails()Ljava/util/Collection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/adyen/core/models/PaymentMethod;->getInputDetails()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    .line 44
    iget-object p1, p0, Lcom/adyen/core/PaymentBroadcastReceivers$1;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-static {p1}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$000(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/PaymentStateHandler;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adyen/core/PaymentStateHandler;->getPaymentProcessorStateMachine()Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    move-result-object p1

    sget-object v0, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_DETAILS_REQUIRED:Lcom/adyen/core/internals/PaymentTrigger;

    invoke-virtual {p1, v0}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;

    goto :goto_0

    .line 47
    :cond_0
    iget-object p1, p0, Lcom/adyen/core/PaymentBroadcastReceivers$1;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-static {p1}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$000(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/PaymentStateHandler;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adyen/core/PaymentStateHandler;->getPaymentProcessorStateMachine()Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    move-result-object p1

    sget-object v0, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_DETAILS_NOT_REQUIRED:Lcom/adyen/core/internals/PaymentTrigger;

    invoke-virtual {p1, v0}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;

    :goto_0
    return-void
.end method
