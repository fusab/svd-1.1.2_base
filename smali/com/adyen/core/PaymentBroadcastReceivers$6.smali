.class Lcom/adyen/core/PaymentBroadcastReceivers$6;
.super Landroid/content/BroadcastReceiver;
.source "PaymentBroadcastReceivers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/PaymentBroadcastReceivers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/core/PaymentBroadcastReceivers;


# direct methods
.method constructor <init>(Lcom/adyen/core/PaymentBroadcastReceivers;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/adyen/core/PaymentBroadcastReceivers$6;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    const-string p1, "PaymentDetails"

    .line 103
    invoke-virtual {p2, p1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    invoke-virtual {p2, p1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    instance-of v0, v0, Lcom/adyen/core/models/paymentdetails/PaymentDetails;

    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {p2, p1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/adyen/core/models/paymentdetails/PaymentDetails;

    .line 106
    iget-object p2, p0, Lcom/adyen/core/PaymentBroadcastReceivers$6;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-static {p2}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$400(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/interfaces/PaymentDetailsCallback;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/adyen/core/interfaces/PaymentDetailsCallback;->completionWithPaymentDetails(Lcom/adyen/core/models/paymentdetails/PaymentDetails;)V

    :cond_0
    return-void
.end method
