.class Lcom/adyen/core/PaymentStateHandler$4;
.super Ljava/lang/Object;
.source "PaymentStateHandler.java"

# interfaces
.implements Lcom/adyen/core/interfaces/HttpResponseCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/core/PaymentStateHandler;->deletePreferredPaymentMethod(Lcom/adyen/core/models/PaymentMethod;Lcom/adyen/core/interfaces/DeletePreferredPaymentMethodListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/core/PaymentStateHandler;

.field final synthetic val$listener:Lcom/adyen/core/interfaces/DeletePreferredPaymentMethodListener;

.field final synthetic val$paymentMethod:Lcom/adyen/core/models/PaymentMethod;


# direct methods
.method constructor <init>(Lcom/adyen/core/PaymentStateHandler;Lcom/adyen/core/interfaces/DeletePreferredPaymentMethodListener;Lcom/adyen/core/models/PaymentMethod;)V
    .locals 0

    .line 500
    iput-object p1, p0, Lcom/adyen/core/PaymentStateHandler$4;->this$0:Lcom/adyen/core/PaymentStateHandler;

    iput-object p2, p0, Lcom/adyen/core/PaymentStateHandler$4;->val$listener:Lcom/adyen/core/interfaces/DeletePreferredPaymentMethodListener;

    iput-object p3, p0, Lcom/adyen/core/PaymentStateHandler$4;->val$paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .line 525
    invoke-static {}, Lcom/adyen/core/PaymentStateHandler;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Deletion of preferred payment method has failed"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 526
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler$4;->val$listener:Lcom/adyen/core/interfaces/DeletePreferredPaymentMethodListener;

    invoke-interface {p1}, Lcom/adyen/core/interfaces/DeletePreferredPaymentMethodListener;->onFail()V

    return-void
.end method

.method public onSuccess([B)V
    .locals 5

    const-string v0, "resultCode"

    .line 504
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    new-instance v2, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 506
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "Success"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 507
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler$4;->val$listener:Lcom/adyen/core/interfaces/DeletePreferredPaymentMethodListener;

    invoke-interface {p1}, Lcom/adyen/core/interfaces/DeletePreferredPaymentMethodListener;->onSuccess()V

    .line 509
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler$4;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-static {p1}, Lcom/adyen/core/PaymentStateHandler;->access$600(Lcom/adyen/core/PaymentStateHandler;)Ljava/util/List;

    move-result-object p1

    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler$4;->val$paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-interface {p1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 511
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler$4;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-static {p1}, Lcom/adyen/core/PaymentStateHandler;->access$700(Lcom/adyen/core/PaymentStateHandler;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;

    .line 512
    iget-object v1, p0, Lcom/adyen/core/PaymentStateHandler$4;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-static {v1}, Lcom/adyen/core/PaymentStateHandler;->access$800(Lcom/adyen/core/PaymentStateHandler;)Lcom/adyen/core/PaymentRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/adyen/core/PaymentStateHandler$4;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-static {v2}, Lcom/adyen/core/PaymentStateHandler;->access$600(Lcom/adyen/core/PaymentStateHandler;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/adyen/core/PaymentStateHandler$4;->this$0:Lcom/adyen/core/PaymentStateHandler;

    .line 513
    invoke-static {v3}, Lcom/adyen/core/PaymentStateHandler;->access$000(Lcom/adyen/core/PaymentStateHandler;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/adyen/core/PaymentStateHandler$4;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-static {v4}, Lcom/adyen/core/PaymentStateHandler;->access$900(Lcom/adyen/core/PaymentStateHandler;)Lcom/adyen/core/PaymentBroadcastReceivers;

    move-result-object v4

    invoke-virtual {v4}, Lcom/adyen/core/PaymentBroadcastReceivers;->getPaymentMethodCallback()Lcom/adyen/core/interfaces/PaymentMethodCallback;

    move-result-object v4

    .line 512
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;->onPaymentMethodSelectionRequired(Lcom/adyen/core/PaymentRequest;Ljava/util/List;Ljava/util/List;Lcom/adyen/core/interfaces/PaymentMethodCallback;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    return-void

    :catch_0
    move-exception p1

    .line 518
    invoke-static {}, Lcom/adyen/core/PaymentStateHandler;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Deletion of preferred payment method has failed"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 520
    :cond_1
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler$4;->val$listener:Lcom/adyen/core/interfaces/DeletePreferredPaymentMethodListener;

    invoke-interface {p1}, Lcom/adyen/core/interfaces/DeletePreferredPaymentMethodListener;->onFail()V

    return-void
.end method
