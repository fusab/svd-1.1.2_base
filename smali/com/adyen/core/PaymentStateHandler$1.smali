.class Lcom/adyen/core/PaymentStateHandler$1;
.super Ljava/lang/Object;
.source "PaymentStateHandler.java"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/core/PaymentStateHandler;->fetchPaymentMethods()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Ljava/util/List<",
        "Lcom/adyen/core/models/PaymentMethod;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/core/PaymentStateHandler;


# direct methods
.method constructor <init>(Lcom/adyen/core/PaymentStateHandler;)V
    .locals 0

    .line 266
    iput-object p1, p0, Lcom/adyen/core/PaymentStateHandler$1;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 266
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/adyen/core/PaymentStateHandler$1;->accept(Ljava/util/List;)V

    return-void
.end method

.method public accept(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 269
    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 270
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler$1;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-static {v0}, Lcom/adyen/core/PaymentStateHandler;->access$000(Lcom/adyen/core/PaymentStateHandler;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 271
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler$1;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-static {v0}, Lcom/adyen/core/PaymentStateHandler;->access$000(Lcom/adyen/core/PaymentStateHandler;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 272
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler$1;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-static {p1}, Lcom/adyen/core/PaymentStateHandler;->access$100(Lcom/adyen/core/PaymentStateHandler;)Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    move-result-object p1

    sget-object v0, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_METHODS_AVAILABLE:Lcom/adyen/core/internals/PaymentTrigger;

    invoke-virtual {p1, v0}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;

    return-void
.end method
