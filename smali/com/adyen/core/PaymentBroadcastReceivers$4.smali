.class Lcom/adyen/core/PaymentBroadcastReceivers$4;
.super Landroid/content/BroadcastReceiver;
.source "PaymentBroadcastReceivers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/PaymentBroadcastReceivers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/core/PaymentBroadcastReceivers;


# direct methods
.method constructor <init>(Lcom/adyen/core/PaymentBroadcastReceivers;)V
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/adyen/core/PaymentBroadcastReceivers$4;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .line 86
    invoke-static {}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$100()Ljava/lang/String;

    move-result-object p1

    const-string p2, "Payment Request Cancelled"

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object p1, p0, Lcom/adyen/core/PaymentBroadcastReceivers$4;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-static {p1}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$200(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/PaymentRequest;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->cancel()V

    return-void
.end method
