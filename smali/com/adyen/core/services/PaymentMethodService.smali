.class public interface abstract Lcom/adyen/core/services/PaymentMethodService;
.super Ljava/lang/Object;
.source "PaymentMethodService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/core/services/PaymentMethodService$PaymentDetailsListener;
    }
.end annotation


# virtual methods
.method public abstract checkAvailability(Landroid/content/Context;Lcom/adyen/core/models/PaymentMethod;Lcom/adyen/core/interfaces/PaymentMethodAvailabilityCallback;)V
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/adyen/core/models/PaymentMethod;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/adyen/core/interfaces/PaymentMethodAvailabilityCallback;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getPaymentDetails(Ljava/util/Map;Lcom/adyen/core/PaymentRequest;Lcom/adyen/core/services/PaymentMethodService$PaymentDetailsListener;)V
    .param p1    # Ljava/util/Map;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/adyen/core/PaymentRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/adyen/core/services/PaymentMethodService$PaymentDetailsListener;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/adyen/core/PaymentRequest;",
            "Lcom/adyen/core/services/PaymentMethodService$PaymentDetailsListener;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation
.end method

.method public abstract process(Landroid/content/Context;Lcom/adyen/core/PaymentRequest;Lcom/adyen/core/interfaces/PaymentRequestListener;Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;)V
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/adyen/core/PaymentRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/adyen/core/interfaces/PaymentRequestListener;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method
