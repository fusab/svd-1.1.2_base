.class Lcom/adyen/core/PaymentStateHandler;
.super Ljava/lang/Object;
.source "PaymentStateHandler.java"

# interfaces
.implements Lcom/adyen/core/interfaces/State$StateChangeListener;


# static fields
.field private static final ANDROID_PAY_TOKEN_PROVIDED:Ljava/lang/String; = "com.adyen.androidpay.ui.AndroidTokenProvided"

.field private static final COMPLETE_RESPONSE:Ljava/lang/String; = "complete"

.field private static final ERROR_RESPONSE:Ljava/lang/String; = "error"

.field private static final REDIRECT_RESPONSE:Ljava/lang/String; = "redirect"

.field private static final SDK_RETURN_URL:Ljava/lang/String; = "adyencheckout://"

.field private static final TAG:Ljava/lang/String; = "PaymentStateHandler"

.field private static final URL_JSON_KEY:Ljava/lang/String; = "url"

.field private static final VALIDATION_RESPONSE:Ljava/lang/String; = "validation"


# instance fields
.field private availablePaymentMethods:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field private merchantPaymentRequestDetailsListener:Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private merchantPaymentRequestListener:Lcom/adyen/core/interfaces/PaymentRequestListener;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private paymentBroadcastReceivers:Lcom/adyen/core/PaymentBroadcastReceivers;

.field private paymentDetails:Lcom/adyen/core/models/paymentdetails/PaymentDetails;

.field private paymentErrorThrowable:Ljava/lang/Throwable;

.field private paymentMethod:Lcom/adyen/core/models/PaymentMethod;

.field private paymentProcessorStateMachine:Lcom/adyen/core/internals/PaymentProcessorStateMachine;

.field private paymentRequest:Lcom/adyen/core/PaymentRequest;

.field private paymentRequestDetailsListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;",
            ">;"
        }
    .end annotation
.end field

.field private paymentRequestListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/adyen/core/interfaces/PaymentRequestListener;",
            ">;"
        }
    .end annotation
.end field

.field private paymentResponse:Lcom/adyen/core/models/PaymentResponse;

.field private paymentResult:Lcom/adyen/core/models/PaymentRequestResult;

.field private preferredPaymentMethods:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;"
        }
    .end annotation
.end field

.field private requiredFieldsPaymentDetails:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private responseJson:Lorg/json/JSONObject;

.field private uriCallback:Lcom/adyen/core/interfaces/UriCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/adyen/core/PaymentRequest;Lcom/adyen/core/interfaces/PaymentRequestListener;Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;)V
    .locals 1
    .param p3    # Lcom/adyen/core/interfaces/PaymentRequestListener;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequestDetailsListeners:Ljava/util/ArrayList;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequestListeners:Ljava/util/ArrayList;

    .line 460
    new-instance v0, Lcom/adyen/core/PaymentStateHandler$3;

    invoke-direct {v0, p0}, Lcom/adyen/core/PaymentStateHandler$3;-><init>(Lcom/adyen/core/PaymentStateHandler;)V

    iput-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->uriCallback:Lcom/adyen/core/interfaces/UriCallback;

    .line 90
    iput-object p1, p0, Lcom/adyen/core/PaymentStateHandler;->context:Landroid/content/Context;

    .line 91
    iput-object p2, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequest:Lcom/adyen/core/PaymentRequest;

    .line 92
    iput-object p3, p0, Lcom/adyen/core/PaymentStateHandler;->merchantPaymentRequestListener:Lcom/adyen/core/interfaces/PaymentRequestListener;

    .line 93
    iput-object p4, p0, Lcom/adyen/core/PaymentStateHandler;->merchantPaymentRequestDetailsListener:Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;

    .line 95
    new-instance v0, Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-direct {v0, p0, p2}, Lcom/adyen/core/PaymentBroadcastReceivers;-><init>(Lcom/adyen/core/PaymentStateHandler;Lcom/adyen/core/PaymentRequest;)V

    iput-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentBroadcastReceivers:Lcom/adyen/core/PaymentBroadcastReceivers;

    .line 96
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/adyen/core/PaymentStateHandler;->availablePaymentMethods:Ljava/util/List;

    .line 97
    new-instance p2, Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    invoke-direct {p2, p0}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;-><init>(Lcom/adyen/core/interfaces/State$StateChangeListener;)V

    iput-object p2, p0, Lcom/adyen/core/PaymentStateHandler;->paymentProcessorStateMachine:Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    .line 99
    iget-object p2, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequestListeners:Ljava/util/ArrayList;

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p4, :cond_0

    .line 102
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequestDetailsListeners:Ljava/util/ArrayList;

    invoke-virtual {p1, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 105
    :cond_0
    :try_start_0
    iget-object p2, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequestDetailsListeners:Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/adyen/core/ListenerFactory;->createAdyenPaymentRequestDetailsListener(Landroid/content/Context;)Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    iget-object p2, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequestListeners:Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/adyen/core/ListenerFactory;->createAdyenPaymentRequestListener(Landroid/content/Context;)Lcom/adyen/core/interfaces/PaymentRequestListener;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/adyen/core/exceptions/UIModuleNotAvailableException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 108
    invoke-virtual {p0, p1}, Lcom/adyen/core/PaymentStateHandler;->setPaymentErrorThrowable(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method static synthetic access$000(Lcom/adyen/core/PaymentStateHandler;)Ljava/util/List;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/adyen/core/PaymentStateHandler;->availablePaymentMethods:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$100(Lcom/adyen/core/PaymentStateHandler;)Lcom/adyen/core/internals/PaymentProcessorStateMachine;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentProcessorStateMachine:Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    return-object p0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .line 51
    sget-object v0, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/adyen/core/PaymentStateHandler;)Lorg/json/JSONObject;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/adyen/core/PaymentStateHandler;->responseJson:Lorg/json/JSONObject;

    return-object p0
.end method

.method static synthetic access$302(Lcom/adyen/core/PaymentStateHandler;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/adyen/core/PaymentStateHandler;->responseJson:Lorg/json/JSONObject;

    return-object p1
.end method

.method static synthetic access$402(Lcom/adyen/core/PaymentStateHandler;Lcom/adyen/core/models/PaymentRequestResult;)Lcom/adyen/core/models/PaymentRequestResult;
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/adyen/core/PaymentStateHandler;->paymentResult:Lcom/adyen/core/models/PaymentRequestResult;

    return-object p1
.end method

.method static synthetic access$502(Lcom/adyen/core/PaymentStateHandler;Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/adyen/core/PaymentStateHandler;->paymentErrorThrowable:Ljava/lang/Throwable;

    return-object p1
.end method

.method static synthetic access$600(Lcom/adyen/core/PaymentStateHandler;)Ljava/util/List;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/adyen/core/PaymentStateHandler;->preferredPaymentMethods:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$700(Lcom/adyen/core/PaymentStateHandler;)Ljava/util/ArrayList;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequestDetailsListeners:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$800(Lcom/adyen/core/PaymentStateHandler;)Lcom/adyen/core/PaymentRequest;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequest:Lcom/adyen/core/PaymentRequest;

    return-object p0
.end method

.method static synthetic access$900(Lcom/adyen/core/PaymentStateHandler;)Lcom/adyen/core/PaymentBroadcastReceivers;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentBroadcastReceivers:Lcom/adyen/core/PaymentBroadcastReceivers;

    return-object p0
.end method

.method private disableOtherRedirectHandlers()V
    .locals 3

    .line 388
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "adyen.core.utils.DISABLE_REDIRECTION_HANDLER"

    .line 389
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x20

    .line 390
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 391
    iget-object v1, p0, Lcom/adyen/core/PaymentStateHandler;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PackageName"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 392
    iget-object v1, p0, Lcom/adyen/core/PaymentStateHandler;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private fetchPaymentMethods()V
    .locals 2

    .line 259
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentResponse:Lcom/adyen/core/models/PaymentResponse;

    if-eqz v0, :cond_1

    .line 260
    invoke-virtual {v0}, Lcom/adyen/core/models/PaymentResponse;->getPreferredPaymentMethods()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->preferredPaymentMethods:Ljava/util/List;

    .line 262
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->merchantPaymentRequestDetailsListener:Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;

    if-nez v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentResponse:Lcom/adyen/core/models/PaymentResponse;

    invoke-virtual {v0}, Lcom/adyen/core/models/PaymentResponse;->getAvailablePaymentMethods()Ljava/util/List;

    move-result-object v0

    .line 264
    iget-object v1, p0, Lcom/adyen/core/PaymentStateHandler;->context:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/adyen/core/internals/ModuleAvailabilityUtil;->filterPaymentMethods(Landroid/content/Context;Ljava/util/List;)Lio/reactivex/Observable;

    move-result-object v0

    .line 266
    new-instance v1, Lcom/adyen/core/PaymentStateHandler$1;

    invoke-direct {v1, p0}, Lcom/adyen/core/PaymentStateHandler$1;-><init>(Lcom/adyen/core/PaymentStateHandler;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    goto :goto_0

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentResponse:Lcom/adyen/core/models/PaymentResponse;

    invoke-virtual {v0}, Lcom/adyen/core/models/PaymentResponse;->getAvailablePaymentMethods()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->availablePaymentMethods:Ljava/util/List;

    .line 277
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentProcessorStateMachine:Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    sget-object v1, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_METHODS_AVAILABLE:Lcom/adyen/core/internals/PaymentTrigger;

    invoke-virtual {v0, v1}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;

    :cond_1
    :goto_0
    return-void
.end method

.method private handleRedirect()V
    .locals 5

    .line 367
    sget-object v0, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v1, "handleRedirect()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    :try_start_0
    invoke-direct {p0}, Lcom/adyen/core/PaymentStateHandler;->disableOtherRedirectHandlers()V

    .line 370
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->responseJson:Lorg/json/JSONObject;

    const-string/jumbo v1, "url"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 371
    iget-object v1, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequestDetailsListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;

    .line 372
    iget-object v3, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequest:Lcom/adyen/core/PaymentRequest;

    iget-object v4, p0, Lcom/adyen/core/PaymentStateHandler;->uriCallback:Lcom/adyen/core/interfaces/UriCallback;

    invoke-interface {v2, v3, v0, v4}, Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;->onRedirectRequired(Lcom/adyen/core/PaymentRequest;Ljava/lang/String;Lcom/adyen/core/interfaces/UriCallback;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 375
    sget-object v1, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v2, "handleRedirect() exception occurred"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 376
    invoke-virtual {p0, v0}, Lcom/adyen/core/PaymentStateHandler;->setPaymentErrorThrowableAndTriggerError(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method private initiatePayment(Lcom/adyen/core/interfaces/HttpResponseCallback;)V
    .locals 5
    .param p1    # Lcom/adyen/core/interfaces/HttpResponseCallback;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 332
    sget-object v0, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v1, "initiatePayment()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "Content-Type"

    const-string v2, "application/json; charset=UTF-8"

    .line 334
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v2, "paymentData"

    .line 337
    iget-object v3, p0, Lcom/adyen/core/PaymentStateHandler;->paymentResponse:Lcom/adyen/core/models/PaymentResponse;

    invoke-virtual {v3}, Lcom/adyen/core/models/PaymentResponse;->getPaymentData()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "paymentMethodData"

    .line 338
    iget-object v3, p0, Lcom/adyen/core/PaymentStateHandler;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {v3}, Lcom/adyen/core/models/PaymentMethod;->getPaymentMethodData()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 340
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 342
    iget-object v3, p0, Lcom/adyen/core/PaymentStateHandler;->paymentDetails:Lcom/adyen/core/models/paymentdetails/PaymentDetails;

    if-eqz v3, :cond_0

    .line 343
    iget-object v2, p0, Lcom/adyen/core/PaymentStateHandler;->paymentDetails:Lcom/adyen/core/models/paymentdetails/PaymentDetails;

    invoke-static {v2}, Lcom/adyen/core/PaymentStateHandler;->paymentDetailsToJson(Lcom/adyen/core/models/paymentdetails/PaymentDetails;)Lorg/json/JSONObject;

    move-result-object v2

    goto :goto_0

    .line 344
    :cond_0
    iget-object v3, p0, Lcom/adyen/core/PaymentStateHandler;->requiredFieldsPaymentDetails:Ljava/util/Map;

    if-eqz v3, :cond_1

    .line 345
    iget-object v2, p0, Lcom/adyen/core/PaymentStateHandler;->requiredFieldsPaymentDetails:Ljava/util/Map;

    invoke-static {v2}, Lcom/adyen/core/utils/Util;->mapToJson(Ljava/util/Map;)Lorg/json/JSONObject;

    move-result-object v2

    .line 348
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/adyen/core/PaymentStateHandler;->sdkHandlesUI()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 349
    iget-object v3, p0, Lcom/adyen/core/PaymentStateHandler;->paymentDetails:Lcom/adyen/core/models/paymentdetails/PaymentDetails;

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/adyen/core/PaymentStateHandler;->requiredFieldsPaymentDetails:Ljava/util/Map;

    if-nez v3, :cond_2

    .line 350
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 352
    :cond_2
    sget-object v3, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v4, "Return url is going to be overridden by SDK"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "overrideReturnUrl"

    const-string v4, "adyencheckout://"

    .line 353
    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_3
    if-eqz v2, :cond_4

    const-string v3, "paymentDetails"

    .line 356
    invoke-virtual {v1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 358
    :cond_4
    iget-object v2, p0, Lcom/adyen/core/PaymentStateHandler;->paymentResponse:Lcom/adyen/core/models/PaymentResponse;

    invoke-virtual {v2}, Lcom/adyen/core/models/PaymentResponse;->getInitiationURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1, p1}, Lcom/adyen/core/utils/AsyncHttpClient;->post(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Lcom/adyen/core/interfaces/HttpResponseCallback;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 361
    sget-object v0, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v1, "initiatePayment() error"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 362
    invoke-virtual {p0, p1}, Lcom/adyen/core/PaymentStateHandler;->setPaymentErrorThrowableAndTriggerError(Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method

.method private notifyPaymentError()V
    .locals 5

    .line 236
    sget-object v0, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v1, "Notifying the payment error to the client app"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequestListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/core/interfaces/PaymentRequestListener;

    .line 238
    iget-object v2, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequest:Lcom/adyen/core/PaymentRequest;

    new-instance v3, Lcom/adyen/core/models/PaymentRequestResult;

    iget-object v4, p0, Lcom/adyen/core/PaymentStateHandler;->paymentErrorThrowable:Ljava/lang/Throwable;

    invoke-direct {v3, v4}, Lcom/adyen/core/models/PaymentRequestResult;-><init>(Ljava/lang/Throwable;)V

    invoke-interface {v1, v2, v3}, Lcom/adyen/core/interfaces/PaymentRequestListener;->onPaymentResult(Lcom/adyen/core/PaymentRequest;Lcom/adyen/core/models/PaymentRequestResult;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private notifyPaymentResult()V
    .locals 4

    .line 228
    sget-object v0, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v1, "Notifying the payment result to the merchant application"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequestListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/core/interfaces/PaymentRequestListener;

    .line 230
    iget-object v2, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequest:Lcom/adyen/core/PaymentRequest;

    iget-object v3, p0, Lcom/adyen/core/PaymentStateHandler;->paymentResult:Lcom/adyen/core/models/PaymentRequestResult;

    invoke-interface {v1, v2, v3}, Lcom/adyen/core/interfaces/PaymentRequestListener;->onPaymentResult(Lcom/adyen/core/PaymentRequest;Lcom/adyen/core/models/PaymentRequestResult;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static paymentDetailsToJson(Lcom/adyen/core/models/paymentdetails/PaymentDetails;)Lorg/json/JSONObject;
    .locals 6
    .param p0    # Lcom/adyen/core/models/paymentdetails/PaymentDetails;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 536
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 537
    invoke-virtual {p0}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->getInputDetails()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 538
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getInputDetails()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getInputDetails()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 539
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 540
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getInputDetails()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 541
    invoke-virtual {v4}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 543
    :cond_0
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 545
    :cond_1
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private processPayment()V
    .locals 2

    .line 283
    sget-object v0, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v1, "processPayment()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    invoke-virtual {p0}, Lcom/adyen/core/PaymentStateHandler;->sdkHandlesUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    sget-object v0, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v1, "Checkout SDK will display an animation while processing."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 288
    :cond_0
    sget-object v0, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v1, "The merchant application will handle UI while Checkout SDK is processing payment."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    :goto_0
    new-instance v0, Lcom/adyen/core/PaymentStateHandler$2;

    invoke-direct {v0, p0}, Lcom/adyen/core/PaymentStateHandler$2;-><init>(Lcom/adyen/core/PaymentStateHandler;)V

    invoke-direct {p0, v0}, Lcom/adyen/core/PaymentStateHandler;->initiatePayment(Lcom/adyen/core/interfaces/HttpResponseCallback;)V

    return-void
.end method

.method private requestPaymentData()V
    .locals 5

    .line 186
    sget-object v0, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v1, "requestPaymentData()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->context:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/adyen/core/PaymentStateHandler;->sdkHandlesUI()Z

    move-result v1

    invoke-static {v0, p0, v1}, Lcom/adyen/core/DeviceTokenGenerator;->getToken(Landroid/content/Context;Lcom/adyen/core/PaymentStateHandler;Z)Ljava/lang/String;

    move-result-object v0

    .line 190
    iget-object v1, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequestListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adyen/core/interfaces/PaymentRequestListener;

    .line 191
    iget-object v3, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequest:Lcom/adyen/core/PaymentRequest;

    iget-object v4, p0, Lcom/adyen/core/PaymentStateHandler;->paymentBroadcastReceivers:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-virtual {v4}, Lcom/adyen/core/PaymentBroadcastReceivers;->getPaymentDataCallback()Lcom/adyen/core/interfaces/PaymentDataCallback;

    move-result-object v4

    invoke-interface {v2, v3, v0, v4}, Lcom/adyen/core/interfaces/PaymentRequestListener;->onPaymentDataRequested(Lcom/adyen/core/PaymentRequest;Ljava/lang/String;Lcom/adyen/core/interfaces/PaymentDataCallback;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private requestPaymentMethodDetails()V
    .locals 5

    .line 196
    sget-object v0, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v1, "requestPaymentMethodDetails()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.adyen.androidpay.ui.AndroidTokenProvided"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 200
    iget-object v1, p0, Lcom/adyen/core/PaymentStateHandler;->context:Landroid/content/Context;

    invoke-static {v1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/adyen/core/PaymentStateHandler;->paymentBroadcastReceivers:Lcom/adyen/core/PaymentBroadcastReceivers;

    .line 201
    invoke-virtual {v2}, Lcom/adyen/core/PaymentBroadcastReceivers;->getAndroidPayInfoListener()Landroid/content/BroadcastReceiver;

    move-result-object v2

    .line 200
    invoke-virtual {v1, v2, v0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 203
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequestDetailsListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;

    .line 204
    iget-object v2, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequest:Lcom/adyen/core/PaymentRequest;

    iget-object v3, p0, Lcom/adyen/core/PaymentStateHandler;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {v3}, Lcom/adyen/core/models/PaymentMethod;->getInputDetails()Ljava/util/Collection;

    move-result-object v3

    iget-object v4, p0, Lcom/adyen/core/PaymentStateHandler;->paymentBroadcastReceivers:Lcom/adyen/core/PaymentBroadcastReceivers;

    .line 205
    invoke-virtual {v4}, Lcom/adyen/core/PaymentBroadcastReceivers;->getPaymentDetailsCallback()Lcom/adyen/core/interfaces/PaymentDetailsCallback;

    move-result-object v4

    .line 204
    invoke-interface {v1, v2, v3, v4}, Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;->onPaymentDetailsRequired(Lcom/adyen/core/PaymentRequest;Ljava/util/Collection;Lcom/adyen/core/interfaces/PaymentDetailsCallback;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private requestPaymentMethodSelection()V
    .locals 6

    .line 210
    sget-object v0, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v1, "requestPaymentMethodSelection()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequestDetailsListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;

    .line 213
    iget-object v2, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequest:Lcom/adyen/core/PaymentRequest;

    iget-object v3, p0, Lcom/adyen/core/PaymentStateHandler;->preferredPaymentMethods:Ljava/util/List;

    iget-object v4, p0, Lcom/adyen/core/PaymentStateHandler;->availablePaymentMethods:Ljava/util/List;

    iget-object v5, p0, Lcom/adyen/core/PaymentStateHandler;->paymentBroadcastReceivers:Lcom/adyen/core/PaymentBroadcastReceivers;

    .line 214
    invoke-virtual {v5}, Lcom/adyen/core/PaymentBroadcastReceivers;->getPaymentMethodCallback()Lcom/adyen/core/interfaces/PaymentMethodCallback;

    move-result-object v5

    .line 213
    invoke-interface {v1, v2, v3, v4, v5}, Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;->onPaymentMethodSelectionRequired(Lcom/adyen/core/PaymentRequest;Ljava/util/List;Ljava/util/List;Lcom/adyen/core/interfaces/PaymentMethodCallback;)V

    goto :goto_0

    .line 217
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.adyen.core.ui.PaymentMethodSelected"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 218
    iget-object v1, p0, Lcom/adyen/core/PaymentStateHandler;->context:Landroid/content/Context;

    invoke-static {v1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/adyen/core/PaymentStateHandler;->paymentBroadcastReceivers:Lcom/adyen/core/PaymentBroadcastReceivers;

    .line 219
    invoke-virtual {v2}, Lcom/adyen/core/PaymentBroadcastReceivers;->getPaymentMethodSelectionReceiver()Landroid/content/BroadcastReceiver;

    move-result-object v2

    .line 218
    invoke-virtual {v1, v2, v0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 222
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.adyen.core.ui.PaymentDetailsProvided"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 223
    iget-object v1, p0, Lcom/adyen/core/PaymentStateHandler;->context:Landroid/content/Context;

    invoke-static {v1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/adyen/core/PaymentStateHandler;->paymentBroadcastReceivers:Lcom/adyen/core/PaymentBroadcastReceivers;

    .line 224
    invoke-virtual {v2}, Lcom/adyen/core/PaymentBroadcastReceivers;->getPaymentDetailsReceiver()Landroid/content/BroadcastReceiver;

    move-result-object v2

    .line 223
    invoke-virtual {v1, v2, v0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    return-void
.end method

.method private unregisterBroadcastReceivers()V
    .locals 3

    .line 244
    :try_start_0
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->context:Landroid/content/Context;

    invoke-static {v0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/adyen/core/PaymentStateHandler;->paymentBroadcastReceivers:Lcom/adyen/core/PaymentBroadcastReceivers;

    .line 245
    invoke-virtual {v1}, Lcom/adyen/core/PaymentBroadcastReceivers;->getPaymentMethodSelectionReceiver()Landroid/content/BroadcastReceiver;

    move-result-object v1

    .line 244
    invoke-virtual {v0, v1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 246
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->context:Landroid/content/Context;

    invoke-static {v0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/adyen/core/PaymentStateHandler;->paymentBroadcastReceivers:Lcom/adyen/core/PaymentBroadcastReceivers;

    .line 247
    invoke-virtual {v1}, Lcom/adyen/core/PaymentBroadcastReceivers;->getAndroidPayInfoListener()Landroid/content/BroadcastReceiver;

    move-result-object v1

    .line 246
    invoke-virtual {v0, v1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 248
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->context:Landroid/content/Context;

    invoke-static {v0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/adyen/core/PaymentStateHandler;->paymentBroadcastReceivers:Lcom/adyen/core/PaymentBroadcastReceivers;

    .line 249
    invoke-virtual {v1}, Lcom/adyen/core/PaymentBroadcastReceivers;->getPaymentRequestCancellationReceiver()Landroid/content/BroadcastReceiver;

    move-result-object v1

    .line 248
    invoke-virtual {v0, v1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 250
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->context:Landroid/content/Context;

    invoke-static {v0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/adyen/core/PaymentStateHandler;->paymentBroadcastReceivers:Lcom/adyen/core/PaymentBroadcastReceivers;

    .line 251
    invoke-virtual {v1}, Lcom/adyen/core/PaymentBroadcastReceivers;->getPaymentDetailsReceiver()Landroid/content/BroadcastReceiver;

    move-result-object v1

    .line 250
    invoke-virtual {v0, v1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 254
    sget-object v1, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v2, "Accepted exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method


# virtual methods
.method clearPaymentMethod()V
    .locals 1

    const/4 v0, 0x0

    .line 444
    iput-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    return-void
.end method

.method deletePreferredPaymentMethod(Lcom/adyen/core/models/PaymentMethod;Lcom/adyen/core/interfaces/DeletePreferredPaymentMethodListener;)V
    .locals 4

    .line 478
    sget-object v0, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v1, "deletePreferredPaymentMethod()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    .line 480
    sget-object p1, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string p2, "paymentMethod cannot be null."

    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    if-nez p2, :cond_1

    .line 484
    sget-object p1, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string p2, "listener cannot be null."

    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    .line 487
    :cond_1
    invoke-virtual {p1}, Lcom/adyen/core/models/PaymentMethod;->isOneClick()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->preferredPaymentMethods:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_1

    .line 492
    :cond_2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "Content-Type"

    const-string v2, "application/json; charset=UTF-8"

    .line 493
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v2, "paymentData"

    .line 496
    iget-object v3, p0, Lcom/adyen/core/PaymentStateHandler;->paymentResponse:Lcom/adyen/core/models/PaymentResponse;

    invoke-virtual {v3}, Lcom/adyen/core/models/PaymentResponse;->getPaymentData()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "paymentMethodData"

    .line 497
    invoke-virtual {p1}, Lcom/adyen/core/models/PaymentMethod;->getPaymentMethodData()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 499
    iget-object v2, p0, Lcom/adyen/core/PaymentStateHandler;->paymentResponse:Lcom/adyen/core/models/PaymentResponse;

    invoke-virtual {v2}, Lcom/adyen/core/models/PaymentResponse;->getDisableRecurringDetailUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/adyen/core/PaymentStateHandler$4;

    invoke-direct {v3, p0, p2, p1}, Lcom/adyen/core/PaymentStateHandler$4;-><init>(Lcom/adyen/core/PaymentStateHandler;Lcom/adyen/core/interfaces/DeletePreferredPaymentMethodListener;Lcom/adyen/core/models/PaymentMethod;)V

    invoke-static {v2, v0, v1, v3}, Lcom/adyen/core/utils/AsyncHttpClient;->post(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Lcom/adyen/core/interfaces/HttpResponseCallback;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 530
    sget-object v0, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v1, "Deletion of preferred payment method has failed"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 531
    invoke-interface {p2}, Lcom/adyen/core/interfaces/DeletePreferredPaymentMethodListener;->onFail()V

    :goto_0
    return-void

    .line 488
    :cond_3
    :goto_1
    invoke-interface {p2}, Lcom/adyen/core/interfaces/DeletePreferredPaymentMethodListener;->onFail()V

    return-void
.end method

.method getAmount()Lcom/adyen/core/models/Amount;
    .locals 1

    .line 412
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentResponse:Lcom/adyen/core/models/PaymentResponse;

    invoke-virtual {v0}, Lcom/adyen/core/models/PaymentResponse;->getAmount()Lcom/adyen/core/models/Amount;

    move-result-object v0

    return-object v0
.end method

.method getGenerationTime()Ljava/lang/String;
    .locals 1

    .line 408
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentResponse:Lcom/adyen/core/models/PaymentResponse;

    invoke-virtual {v0}, Lcom/adyen/core/models/PaymentResponse;->getGenerationTime()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getPaymentBroadcastReceivers()Lcom/adyen/core/PaymentBroadcastReceivers;
    .locals 1

    .line 400
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentBroadcastReceivers:Lcom/adyen/core/PaymentBroadcastReceivers;

    return-object v0
.end method

.method getPaymentMethod()Lcom/adyen/core/models/PaymentMethod;
    .locals 1

    .line 420
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    return-object v0
.end method

.method getPaymentProcessorStateMachine()Lcom/adyen/core/internals/PaymentProcessorStateMachine;
    .locals 1

    .line 396
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentProcessorStateMachine:Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    return-object v0
.end method

.method getPaymentRequestDetailsListener()Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;
    .locals 2

    .line 428
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequestDetailsListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 432
    :cond_0
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequestDetailsListeners:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method getPaymentRequestListener()Lcom/adyen/core/interfaces/PaymentRequestListener;
    .locals 1

    .line 424
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->merchantPaymentRequestListener:Lcom/adyen/core/interfaces/PaymentRequestListener;

    return-object v0
.end method

.method getPublicKey()Ljava/lang/String;
    .locals 1

    .line 404
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentResponse:Lcom/adyen/core/models/PaymentResponse;

    invoke-virtual {v0}, Lcom/adyen/core/models/PaymentResponse;->getPublicKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getShopperReference()Ljava/lang/String;
    .locals 1

    .line 416
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentResponse:Lcom/adyen/core/models/PaymentResponse;

    invoke-virtual {v0}, Lcom/adyen/core/models/PaymentResponse;->getShopperReference()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method hasPaymentRequestDetailsListener()Z
    .locals 1

    .line 436
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->paymentRequestDetailsListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onStateChanged(Lcom/adyen/core/interfaces/State;)V
    .locals 3

    .line 116
    sget-object v0, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStateChanged: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    move-object v0, p1

    check-cast v0, Lcom/adyen/core/internals/PaymentRequestState;

    .line 118
    sget-object v1, Lcom/adyen/core/PaymentStateHandler$5;->$SwitchMap$com$adyen$core$internals$PaymentRequestState:[I

    invoke-virtual {v0}, Lcom/adyen/core/internals/PaymentRequestState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 162
    sget-object v0, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Internal error - payment request state machine failure."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/adyen/core/PaymentStateHandler;->paymentErrorThrowable:Ljava/lang/Throwable;

    .line 165
    invoke-direct {p0}, Lcom/adyen/core/PaymentStateHandler;->unregisterBroadcastReceivers()V

    .line 166
    invoke-direct {p0}, Lcom/adyen/core/PaymentStateHandler;->notifyPaymentError()V

    goto/16 :goto_0

    .line 155
    :pswitch_0
    sget-object p1, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v0, "Payment cancelled."

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    new-instance p1, Lcom/adyen/core/models/PaymentRequestResult;

    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Cancelled"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-direct {p1, v0}, Lcom/adyen/core/models/PaymentRequestResult;-><init>(Ljava/lang/Throwable;)V

    iput-object p1, p0, Lcom/adyen/core/PaymentStateHandler;->paymentResult:Lcom/adyen/core/models/PaymentRequestResult;

    .line 157
    invoke-direct {p0}, Lcom/adyen/core/PaymentStateHandler;->notifyPaymentResult()V

    .line 158
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler;->context:Landroid/content/Context;

    invoke-static {p1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object p1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.adyen.core.ui.finish"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 159
    invoke-direct {p0}, Lcom/adyen/core/PaymentStateHandler;->unregisterBroadcastReceivers()V

    goto :goto_0

    .line 150
    :pswitch_1
    sget-object p1, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v0, "Payment aborted."

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-direct {p0}, Lcom/adyen/core/PaymentStateHandler;->unregisterBroadcastReceivers()V

    .line 152
    invoke-direct {p0}, Lcom/adyen/core/PaymentStateHandler;->notifyPaymentError()V

    goto :goto_0

    .line 145
    :pswitch_2
    sget-object p1, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v0, "Payment processed."

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-direct {p0}, Lcom/adyen/core/PaymentStateHandler;->unregisterBroadcastReceivers()V

    .line 147
    invoke-direct {p0}, Lcom/adyen/core/PaymentStateHandler;->notifyPaymentResult()V

    goto :goto_0

    .line 141
    :pswitch_3
    sget-object p1, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v0, "Waiting for redirection."

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    invoke-direct {p0}, Lcom/adyen/core/PaymentStateHandler;->handleRedirect()V

    goto :goto_0

    .line 137
    :pswitch_4
    sget-object p1, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v0, "Processing payment."

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    invoke-direct {p0}, Lcom/adyen/core/PaymentStateHandler;->processPayment()V

    goto :goto_0

    .line 132
    :pswitch_5
    sget-object p1, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v0, "Waiting for payment details (The selected payment method requires additional information)"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    invoke-direct {p0}, Lcom/adyen/core/PaymentStateHandler;->requestPaymentMethodDetails()V

    goto :goto_0

    .line 128
    :pswitch_6
    sget-object p1, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v0, "Waiting for user to select payment method."

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    invoke-direct {p0}, Lcom/adyen/core/PaymentStateHandler;->requestPaymentMethodSelection()V

    goto :goto_0

    .line 124
    :pswitch_7
    sget-object p1, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v0, "Fetching and filtering the available payment methods"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    invoke-direct {p0}, Lcom/adyen/core/PaymentStateHandler;->fetchPaymentMethods()V

    goto :goto_0

    .line 120
    :pswitch_8
    sget-object p1, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v0, "Waiting for client to provide payment data."

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    invoke-direct {p0}, Lcom/adyen/core/PaymentStateHandler;->requestPaymentData()V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onStateNotChanged(Lcom/adyen/core/interfaces/State;)V
    .locals 3

    .line 172
    sget-object v0, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStateNotChanged: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    check-cast p1, Lcom/adyen/core/internals/PaymentRequestState;

    .line 174
    sget-object v0, Lcom/adyen/core/PaymentStateHandler$5;->$SwitchMap$com$adyen$core$internals$PaymentRequestState:[I

    invoke-virtual {p1}, Lcom/adyen/core/internals/PaymentRequestState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    .line 181
    sget-object p1, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v0, "No action will be taken for this state."

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 176
    :cond_0
    sget-object p1, Lcom/adyen/core/PaymentStateHandler;->TAG:Ljava/lang/String;

    const-string v0, "Waiting for payment details (The selected payment method requires additional information)"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    invoke-direct {p0}, Lcom/adyen/core/PaymentStateHandler;->requestPaymentMethodDetails()V

    :goto_0
    return-void
.end method

.method sdkHandlesUI()Z
    .locals 1

    .line 384
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler;->merchantPaymentRequestDetailsListener:Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method setPaymentDetails(Lcom/adyen/core/models/paymentdetails/PaymentDetails;)V
    .locals 0

    .line 470
    iput-object p1, p0, Lcom/adyen/core/PaymentStateHandler;->paymentDetails:Lcom/adyen/core/models/paymentdetails/PaymentDetails;

    return-void
.end method

.method setPaymentDetails(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 474
    iput-object p1, p0, Lcom/adyen/core/PaymentStateHandler;->requiredFieldsPaymentDetails:Ljava/util/Map;

    return-void
.end method

.method setPaymentErrorThrowable(Ljava/lang/Throwable;)V
    .locals 0

    .line 452
    iput-object p1, p0, Lcom/adyen/core/PaymentStateHandler;->paymentErrorThrowable:Ljava/lang/Throwable;

    return-void
.end method

.method setPaymentErrorThrowableAndTriggerError(Ljava/lang/Throwable;)V
    .locals 1

    .line 456
    iput-object p1, p0, Lcom/adyen/core/PaymentStateHandler;->paymentErrorThrowable:Ljava/lang/Throwable;

    .line 457
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler;->paymentProcessorStateMachine:Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    sget-object v0, Lcom/adyen/core/internals/PaymentTrigger;->ERROR_OCCURRED:Lcom/adyen/core/internals/PaymentTrigger;

    invoke-virtual {p1, v0}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;

    return-void
.end method

.method setPaymentMethod(Lcom/adyen/core/models/PaymentMethod;)V
    .locals 0
    .param p1    # Lcom/adyen/core/models/PaymentMethod;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 440
    iput-object p1, p0, Lcom/adyen/core/PaymentStateHandler;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    return-void
.end method

.method setPaymentResponse(Lcom/adyen/core/models/PaymentResponse;)V
    .locals 0

    .line 448
    iput-object p1, p0, Lcom/adyen/core/PaymentStateHandler;->paymentResponse:Lcom/adyen/core/models/PaymentResponse;

    return-void
.end method
