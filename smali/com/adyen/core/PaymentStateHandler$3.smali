.class Lcom/adyen/core/PaymentStateHandler$3;
.super Ljava/lang/Object;
.source "PaymentStateHandler.java"

# interfaces
.implements Lcom/adyen/core/interfaces/UriCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/PaymentStateHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/core/PaymentStateHandler;


# direct methods
.method constructor <init>(Lcom/adyen/core/PaymentStateHandler;)V
    .locals 0

    .line 460
    iput-object p1, p0, Lcom/adyen/core/PaymentStateHandler$3;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public completionWithUri(Landroid/net/Uri;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 463
    invoke-static {}, Lcom/adyen/core/PaymentStateHandler;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "completionWithUri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler$3;->this$0:Lcom/adyen/core/PaymentStateHandler;

    new-instance v1, Lcom/adyen/core/models/PaymentRequestResult;

    new-instance v2, Lcom/adyen/core/models/Payment;

    invoke-direct {v2, p1}, Lcom/adyen/core/models/Payment;-><init>(Landroid/net/Uri;)V

    invoke-direct {v1, v2}, Lcom/adyen/core/models/PaymentRequestResult;-><init>(Lcom/adyen/core/models/Payment;)V

    invoke-static {v0, v1}, Lcom/adyen/core/PaymentStateHandler;->access$402(Lcom/adyen/core/PaymentStateHandler;Lcom/adyen/core/models/PaymentRequestResult;)Lcom/adyen/core/models/PaymentRequestResult;

    .line 465
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler$3;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-static {p1}, Lcom/adyen/core/PaymentStateHandler;->access$100(Lcom/adyen/core/PaymentStateHandler;)Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    move-result-object p1

    sget-object v0, Lcom/adyen/core/internals/PaymentTrigger;->RETURN_URI_RECEIVED:Lcom/adyen/core/internals/PaymentTrigger;

    invoke-virtual {p1, v0}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;

    return-void
.end method
