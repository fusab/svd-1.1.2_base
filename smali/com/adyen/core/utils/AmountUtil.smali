.class public final Lcom/adyen/core/utils/AmountUtil;
.super Ljava/lang/Object;
.source "AmountUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AmountUtil"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static format(JILjava/util/Locale;)Ljava/lang/String;
    .locals 11

    if-lez p2, :cond_3

    const-wide/16 v0, 0x0

    cmp-long v2, p0, v0

    if-gez v2, :cond_0

    const-string v0, "-"

    goto :goto_0

    :cond_0
    const-string v0, ""

    .line 111
    :goto_0
    invoke-static {p0, p1}, Ljava/lang/Math;->abs(J)J

    move-result-wide p0

    const-wide/16 v1, 0x1

    const/4 v3, 0x0

    move-wide v4, v1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, p2, :cond_1

    const-wide/16 v6, 0xa

    mul-long v4, v4, v6

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x2

    const/4 v2, 0x3

    const-string v6, "d"

    const/4 v7, 0x1

    if-eqz p3, :cond_2

    .line 117
    invoke-static {p3}, Ljava/text/DecimalFormatSymbols;->getInstance(Ljava/util/Locale;)Ljava/text/DecimalFormatSymbols;

    move-result-object v8

    .line 118
    invoke-static {p3}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object p3

    div-long v9, p0, v4

    invoke-virtual {p3, v9, v10}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object p3

    .line 119
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "%s%s%s%0"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v3

    aput-object p3, v6, v7

    invoke-virtual {v8}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result p3

    invoke-static {p3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object p3

    aput-object p3, v6, v1

    rem-long/2addr p0, v4

    .line 120
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    aput-object p0, v6, v2

    .line 119
    invoke-static {p2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_2

    .line 122
    :cond_2
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "%s%d.%0"

    invoke-virtual {p3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    new-array p3, v2, [Ljava/lang/Object;

    aput-object v0, p3, v3

    div-long v2, p0, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, p3, v7

    rem-long/2addr p0, v4

    .line 123
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    aput-object p0, p3, v1

    .line 122
    invoke-static {p2, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_2

    :cond_3
    if-eqz p3, :cond_4

    .line 127
    invoke-static {p3}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object p2

    invoke-virtual {p2, p0, p1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object p0

    goto :goto_2

    .line 129
    :cond_4
    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    :goto_2
    return-object p0
.end method

.method public static format(Lcom/adyen/core/models/Amount;Z)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/adyen/core/models/Amount;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    .line 74
    invoke-static {p0, p1, v0}, Lcom/adyen/core/utils/AmountUtil;->format(Lcom/adyen/core/models/Amount;ZLjava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static format(Lcom/adyen/core/models/Amount;ZLjava/util/Locale;)Ljava/lang/String;
    .locals 3
    .param p0    # Lcom/adyen/core/models/Amount;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 89
    invoke-virtual {p0}, Lcom/adyen/core/models/Amount;->getValue()J

    move-result-wide v0

    .line 90
    invoke-virtual {p0}, Lcom/adyen/core/models/Amount;->getCurrency()Ljava/lang/String;

    move-result-object p0

    .line 91
    invoke-static {p0}, Lcom/adyen/core/utils/AmountUtil;->getExponent(Ljava/lang/String;)I

    move-result v2

    .line 92
    invoke-static {v0, v1, v2, p2}, Lcom/adyen/core/utils/AmountUtil;->format(JILjava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    if-eqz p1, :cond_1

    .line 94
    invoke-static {p0}, Lcom/adyen/core/utils/AmountUtil;->getCurrencySymbol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 95
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p0

    const/4 v0, 0x1

    if-le p0, v0, :cond_0

    const-string p0, " "

    .line 99
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    :cond_0
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    return-object p2
.end method

.method private static getCurrencySymbol(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 139
    :try_start_0
    invoke-static {p0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {v0}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method private static getExponent(Ljava/lang/String;)I
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "ISK"

    .line 149
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x2

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string v0, "CLP"

    .line 152
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    const-string v0, "MXP"

    .line 155
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    return v1

    :cond_2
    const-string v0, "MRO"

    .line 158
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 p0, 0x1

    return p0

    :cond_3
    const-string v0, "IDR"

    .line 161
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_4

    return v2

    :cond_4
    const-string v0, "VND"

    .line 164
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    return v2

    :cond_5
    const-string v0, "UGX"

    .line 167
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    return v2

    :cond_6
    const-string v0, "CVE"

    .line 170
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    return v2

    :cond_7
    const-string v0, "ZMW"

    .line 173
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    return v1

    :cond_8
    const-string v0, "GHC"

    .line 176
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    return v2

    :cond_9
    const-string v0, "BYR"

    .line 179
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    return v2

    :cond_a
    const-string v0, "BYN"

    .line 182
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    return v1

    :cond_b
    const-string v0, "RSD"

    .line 185
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    return v1

    :cond_c
    const/4 v0, 0x0

    .line 192
    :try_start_0
    invoke-static {p0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 194
    sget-object v1, Lcom/adyen/core/utils/AmountUtil;->TAG:Ljava/lang/String;

    const-string v3, "Currency is incorrect: "

    invoke-static {v1, v3, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    if-eqz v0, :cond_e

    .line 198
    invoke-virtual {v0}, Ljava/util/Currency;->getDefaultFractionDigits()I

    move-result p0

    const/4 v0, -0x1

    if-ne p0, v0, :cond_d

    goto :goto_1

    :cond_d
    move v2, p0

    :cond_e
    :goto_1
    return v2
.end method

.method public static isValidCurrencyCode(Ljava/lang/String;)Z
    .locals 3

    .line 32
    invoke-static {p0}, Lcom/adyen/core/utils/StringUtils;->isEmptyOrNull(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string v0, "BYR"

    .line 38
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    if-nez v0, :cond_2

    const-string v0, "BYN"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 44
    :cond_1
    :try_start_0
    invoke-static {p0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return v2

    :catch_0
    return v1

    :cond_2
    :goto_0
    return v2
.end method

.method public static parseMajorAmount(Ljava/lang/String;Ljava/lang/String;)J
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    move-object/from16 v0, p1

    .line 216
    invoke-static/range {p0 .. p0}, Lcom/adyen/core/utils/AmountUtil;->getExponent(Ljava/lang/String;)I

    move-result v1

    const/16 v2, 0x2c

    .line 218
    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    const/16 v4, 0x2e

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    const-string v6, ""

    if-ge v3, v5, :cond_0

    const-string v3, ","

    .line 220
    invoke-virtual {v0, v3, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_0
    move-object v3, v0

    .line 223
    :goto_0
    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    invoke-virtual {v3, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    if-ge v5, v7, :cond_1

    const-string v5, "."

    .line 225
    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 228
    :cond_1
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    .line 237
    array-length v5, v3

    const/4 v6, 0x0

    if-eqz v5, :cond_f

    .line 240
    aget-char v5, v3, v6

    const/16 v7, 0x2d

    const-wide/16 v8, 0x0

    if-ne v5, v7, :cond_2

    move-wide v11, v8

    const/4 v5, 0x1

    const/4 v13, 0x1

    goto :goto_1

    :cond_2
    move-wide v11, v8

    const/4 v5, 0x0

    const/4 v13, 0x0

    .line 244
    :goto_1
    array-length v14, v3

    const/16 v15, 0x39

    const-wide/16 v16, 0xa

    const/16 v10, 0x30

    if-ge v5, v14, :cond_4

    .line 245
    aget-char v14, v3, v5

    if-lt v14, v10, :cond_3

    aget-char v14, v3, v5

    if-gt v14, v15, :cond_3

    mul-long v11, v11, v16

    .line 247
    aget-char v14, v3, v5

    sub-int/2addr v14, v10

    int-to-long v14, v14

    add-long/2addr v11, v14

    goto :goto_2

    .line 249
    :cond_3
    aget-char v14, v3, v5

    invoke-static {v14}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v14

    if-eqz v14, :cond_4

    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 255
    :cond_4
    array-length v14, v3

    if-ge v5, v14, :cond_9

    aget-char v14, v3, v5

    if-eq v14, v4, :cond_5

    aget-char v4, v3, v5

    if-ne v4, v2, :cond_9

    :cond_5
    add-int/lit8 v5, v5, 0x1

    move-wide/from16 v18, v8

    const/4 v2, 0x0

    .line 258
    :goto_3
    array-length v4, v3

    if-ge v5, v4, :cond_7

    .line 259
    aget-char v4, v3, v5

    if-lt v4, v10, :cond_6

    aget-char v4, v3, v5

    if-gt v4, v15, :cond_6

    mul-long v18, v18, v16

    .line 261
    aget-char v4, v3, v5

    sub-int/2addr v4, v10

    move-wide/from16 v20, v11

    int-to-long v10, v4

    add-long v18, v18, v10

    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_6
    move-wide/from16 v20, v11

    .line 264
    aget-char v4, v3, v5

    invoke-static {v4}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v4

    if-eqz v4, :cond_8

    add-int/lit8 v5, v5, 0x1

    :goto_4
    move-wide/from16 v11, v20

    const/16 v10, 0x30

    goto :goto_3

    :cond_7
    move-wide/from16 v20, v11

    :cond_8
    move v4, v2

    const/4 v2, 0x1

    goto :goto_5

    :cond_9
    move-wide/from16 v20, v11

    move-wide/from16 v18, v8

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 273
    :goto_5
    array-length v10, v3

    if-ge v5, v10, :cond_a

    aget-char v3, v3, v5

    if-ne v3, v7, :cond_a

    const/4 v13, 0x1

    :cond_a
    if-eqz v2, :cond_c

    if-eq v4, v1, :cond_c

    cmp-long v2, v18, v8

    if-eqz v2, :cond_c

    if-le v4, v1, :cond_b

    goto :goto_6

    .line 279
    :cond_b
    new-instance v2, Ljava/text/ParseException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Number of minor currency digits in amount ("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ") does not match currency definition ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "). Amount was "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v6}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v2

    :cond_c
    :goto_6
    const-wide/16 v2, 0x1

    :goto_7
    if-ge v6, v1, :cond_d

    mul-long v2, v2, v16

    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    .line 286
    :cond_d
    invoke-static {v2, v3}, Ljava/lang/Long;->signum(J)I

    mul-long v2, v2, v20

    add-long v18, v18, v2

    if-eqz v13, :cond_e

    const/4 v10, -0x1

    goto :goto_8

    :cond_e
    const/4 v10, 0x1

    :goto_8
    int-to-long v0, v10

    mul-long v18, v18, v0

    return-wide v18

    .line 238
    :cond_f
    new-instance v0, Ljava/text/ParseException;

    const-string v1, "Empty string is not an amount"

    invoke-direct {v0, v1, v6}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0
.end method

.method public static toString(Lcom/adyen/core/models/Amount;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/adyen/core/models/Amount;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x1

    .line 62
    invoke-static {p0, v0}, Lcom/adyen/core/utils/AmountUtil;->format(Lcom/adyen/core/models/Amount;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
