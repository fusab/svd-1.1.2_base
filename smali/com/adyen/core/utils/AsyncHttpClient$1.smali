.class final Lcom/adyen/core/utils/AsyncHttpClient$1;
.super Ljava/lang/Object;
.source "AsyncHttpClient.java"

# interfaces
.implements Lio/reactivex/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/core/utils/AsyncHttpClient;->post(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Lcom/adyen/core/interfaces/HttpResponseCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/Observer<",
        "[B>;"
    }
.end annotation


# instance fields
.field final synthetic val$httpResponseCallback:Lcom/adyen/core/interfaces/HttpResponseCallback;


# direct methods
.method constructor <init>(Lcom/adyen/core/interfaces/HttpResponseCallback;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/adyen/core/utils/AsyncHttpClient$1;->val$httpResponseCallback:Lcom/adyen/core/interfaces/HttpResponseCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 0

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/adyen/core/utils/AsyncHttpClient$1;->val$httpResponseCallback:Lcom/adyen/core/interfaces/HttpResponseCallback;

    invoke-interface {v0, p1}, Lcom/adyen/core/interfaces/HttpResponseCallback;->onFailure(Ljava/lang/Throwable;)V

    return-void
.end method

.method public bridge synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 55
    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/adyen/core/utils/AsyncHttpClient$1;->onNext([B)V

    return-void
.end method

.method public onNext([B)V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/adyen/core/utils/AsyncHttpClient$1;->val$httpResponseCallback:Lcom/adyen/core/interfaces/HttpResponseCallback;

    invoke-interface {v0, p1}, Lcom/adyen/core/interfaces/HttpResponseCallback;->onSuccess([B)V

    return-void
.end method

.method public onSubscribe(Lio/reactivex/disposables/Disposable;)V
    .locals 0
    .param p1    # Lio/reactivex/disposables/Disposable;
        .annotation build Lio/reactivex/annotations/NonNull;
        .end annotation
    .end param

    return-void
.end method
