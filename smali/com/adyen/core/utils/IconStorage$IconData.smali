.class final Lcom/adyen/core/utils/IconStorage$IconData;
.super Ljava/lang/Object;
.source "IconStorage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/utils/IconStorage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "IconData"
.end annotation


# static fields
.field private static final DAYS_UNTIL_LOGOS_WILL_REFRESH:I = 0x1e

.field private static final KEY_ENCODED_IMAGE:Ljava/lang/String; = "encoded_image"

.field private static final KEY_LAST_MODIFIED:Ljava/lang/String; = "last_modified"


# instance fields
.field private encodedImage:Ljava/lang/String;

.field private lastModified:J


# direct methods
.method private constructor <init>(JLjava/lang/String;)V
    .locals 0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-wide p1, p0, Lcom/adyen/core/utils/IconStorage$IconData;->lastModified:J

    .line 81
    iput-object p3, p0, Lcom/adyen/core/utils/IconStorage$IconData;->encodedImage:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(JLjava/lang/String;Lcom/adyen/core/utils/IconStorage$1;)V
    .locals 0

    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/adyen/core/utils/IconStorage$IconData;-><init>(JLjava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "last_modified"

    .line 75
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/adyen/core/utils/IconStorage$IconData;->lastModified:J

    const-string p1, "encoded_image"

    .line 76
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/adyen/core/utils/IconStorage$IconData;->encodedImage:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/adyen/core/utils/IconStorage$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 64
    invoke-direct {p0, p1}, Lcom/adyen/core/utils/IconStorage$IconData;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/adyen/core/utils/IconStorage$IconData;)Z
    .locals 0

    .line 64
    invoke-direct {p0}, Lcom/adyen/core/utils/IconStorage$IconData;->needsRefreshing()Z

    move-result p0

    return p0
.end method

.method static synthetic access$300(Lcom/adyen/core/utils/IconStorage$IconData;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 64
    invoke-direct {p0}, Lcom/adyen/core/utils/IconStorage$IconData;->serialize()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private needsRefreshing()Z
    .locals 6

    .line 97
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    .line 98
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/adyen/core/utils/IconStorage$IconData;->lastModified:J

    sub-long/2addr v2, v4

    cmp-long v4, v2, v0

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private serialize()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 85
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 86
    iget-object v1, p0, Lcom/adyen/core/utils/IconStorage$IconData;->encodedImage:Ljava/lang/String;

    const-string v2, "encoded_image"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 87
    iget-wide v1, p0, Lcom/adyen/core/utils/IconStorage$IconData;->lastModified:J

    const-string v3, "last_modified"

    invoke-virtual {v0, v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 88
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method getBitmap()Landroid/graphics/Bitmap;
    .locals 3

    .line 92
    iget-object v0, p0, Lcom/adyen/core/utils/IconStorage$IconData;->encodedImage:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 93
    array-length v2, v0

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
