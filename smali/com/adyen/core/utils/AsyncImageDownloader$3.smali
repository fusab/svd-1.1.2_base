.class final Lcom/adyen/core/utils/AsyncImageDownloader$3;
.super Ljava/lang/Object;
.source "AsyncImageDownloader.java"

# interfaces
.implements Lio/reactivex/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/core/utils/AsyncImageDownloader;->downloadImage(Landroid/content/Context;Lcom/adyen/core/utils/AsyncImageDownloader$ImageListener;Ljava/lang/String;Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/Observer<",
        "Landroidx/core/util/Pair<",
        "Landroid/graphics/Bitmap;",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic val$imageListener:Lcom/adyen/core/utils/AsyncImageDownloader$ImageListener;


# direct methods
.method constructor <init>(Lcom/adyen/core/utils/AsyncImageDownloader$ImageListener;)V
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/adyen/core/utils/AsyncImageDownloader$3;->val$imageListener:Lcom/adyen/core/utils/AsyncImageDownloader$ImageListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 0

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 0

    return-void
.end method

.method public onNext(Landroidx/core/util/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/core/util/Pair<",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 153
    iget-object v0, p0, Lcom/adyen/core/utils/AsyncImageDownloader$3;->val$imageListener:Lcom/adyen/core/utils/AsyncImageDownloader$ImageListener;

    iget-object v1, p1, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    iget-object p1, p1, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/adyen/core/utils/AsyncImageDownloader$ImageListener;->onImage(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 134
    check-cast p1, Landroidx/core/util/Pair;

    invoke-virtual {p0, p1}, Lcom/adyen/core/utils/AsyncImageDownloader$3;->onNext(Landroidx/core/util/Pair;)V

    return-void
.end method

.method public onSubscribe(Lio/reactivex/disposables/Disposable;)V
    .locals 0
    .param p1    # Lio/reactivex/disposables/Disposable;
        .annotation build Lio/reactivex/annotations/NonNull;
        .end annotation
    .end param

    return-void
.end method
