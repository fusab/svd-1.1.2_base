.class final Lcom/adyen/core/utils/AsyncImageDownloader$2;
.super Ljava/lang/Object;
.source "AsyncImageDownloader.java"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/core/utils/AsyncImageDownloader;->downloadImage(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOnSubscribe<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$fallbackImage:Landroid/graphics/Bitmap;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/adyen/core/utils/AsyncImageDownloader$2;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/adyen/core/utils/AsyncImageDownloader$2;->val$url:Ljava/lang/String;

    iput-object p3, p0, Lcom/adyen/core/utils/AsyncImageDownloader$2;->val$fallbackImage:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 3
    .param p1    # Lio/reactivex/ObservableEmitter;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .line 69
    invoke-interface {p1}, Lio/reactivex/ObservableEmitter;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/adyen/core/utils/AsyncImageDownloader$2;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/adyen/core/utils/AsyncImageDownloader$2;->val$url:Ljava/lang/String;

    iget-object v2, p0, Lcom/adyen/core/utils/AsyncImageDownloader$2;->val$fallbackImage:Landroid/graphics/Bitmap;

    invoke-static {v0, v1, v2}, Lcom/adyen/core/utils/AsyncImageDownloader;->access$000(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 74
    invoke-interface {p1, v0}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    .line 76
    :cond_1
    invoke-interface {p1}, Lio/reactivex/ObservableEmitter;->onComplete()V

    return-void
.end method
