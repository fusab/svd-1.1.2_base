.class final Lcom/adyen/core/utils/AsyncImageDownloader$4;
.super Ljava/lang/Object;
.source "AsyncImageDownloader.java"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/core/utils/AsyncImageDownloader;->downloadImage(Landroid/content/Context;Lcom/adyen/core/utils/AsyncImageDownloader$ImageListener;Ljava/lang/String;Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOnSubscribe<",
        "Landroidx/core/util/Pair<",
        "Landroid/graphics/Bitmap;",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$fallbackImage:Landroid/graphics/Bitmap;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/adyen/core/utils/AsyncImageDownloader$4;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/adyen/core/utils/AsyncImageDownloader$4;->val$url:Ljava/lang/String;

    iput-object p3, p0, Lcom/adyen/core/utils/AsyncImageDownloader$4;->val$fallbackImage:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 3
    .param p1    # Lio/reactivex/ObservableEmitter;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "Landroidx/core/util/Pair<",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 121
    invoke-interface {p1}, Lio/reactivex/ObservableEmitter;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/adyen/core/utils/AsyncImageDownloader$4;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/adyen/core/utils/AsyncImageDownloader$4;->val$url:Ljava/lang/String;

    iget-object v2, p0, Lcom/adyen/core/utils/AsyncImageDownloader$4;->val$fallbackImage:Landroid/graphics/Bitmap;

    invoke-static {v0, v1, v2}, Lcom/adyen/core/utils/AsyncImageDownloader;->access$000(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 126
    new-instance v1, Landroidx/core/util/Pair;

    iget-object v2, p0, Lcom/adyen/core/utils/AsyncImageDownloader$4;->val$url:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Landroidx/core/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p1, v1}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    .line 128
    :cond_1
    invoke-interface {p1}, Lio/reactivex/ObservableEmitter;->onComplete()V

    return-void
.end method
