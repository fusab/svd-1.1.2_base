.class final Lcom/adyen/core/utils/AsyncHttpClient$2;
.super Ljava/lang/Object;
.source "AsyncHttpClient.java"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/core/utils/AsyncHttpClient;->post(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Lcom/adyen/core/interfaces/HttpResponseCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOnSubscribe<",
        "[B>;"
    }
.end annotation


# instance fields
.field final synthetic val$data:Ljava/lang/String;

.field final synthetic val$headers:Ljava/util/Map;

.field final synthetic val$httpClient:Lcom/adyen/core/internals/HttpClient;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/adyen/core/internals/HttpClient;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/adyen/core/utils/AsyncHttpClient$2;->val$httpClient:Lcom/adyen/core/internals/HttpClient;

    iput-object p2, p0, Lcom/adyen/core/utils/AsyncHttpClient$2;->val$url:Ljava/lang/String;

    iput-object p3, p0, Lcom/adyen/core/utils/AsyncHttpClient$2;->val$headers:Ljava/util/Map;

    iput-object p4, p0, Lcom/adyen/core/utils/AsyncHttpClient$2;->val$data:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 5
    .param p1    # Lio/reactivex/ObservableEmitter;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "[B>;)V"
        }
    .end annotation

    .line 36
    invoke-interface {p1}, Lio/reactivex/ObservableEmitter;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 41
    :try_start_0
    iget-object v1, p0, Lcom/adyen/core/utils/AsyncHttpClient$2;->val$httpClient:Lcom/adyen/core/internals/HttpClient;

    iget-object v2, p0, Lcom/adyen/core/utils/AsyncHttpClient$2;->val$url:Ljava/lang/String;

    iget-object v3, p0, Lcom/adyen/core/utils/AsyncHttpClient$2;->val$headers:Ljava/util/Map;

    iget-object v4, p0, Lcom/adyen/core/utils/AsyncHttpClient$2;->val$data:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/adyen/core/internals/HttpClient;->post(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)[B

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 44
    invoke-static {}, Lcom/adyen/core/utils/AsyncHttpClient;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Post failed"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 45
    invoke-interface {p1, v1}, Lio/reactivex/ObservableEmitter;->onError(Ljava/lang/Throwable;)V

    :goto_0
    if-eqz v0, :cond_1

    .line 48
    invoke-interface {p1, v0}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    .line 50
    :cond_1
    invoke-interface {p1}, Lio/reactivex/ObservableEmitter;->onComplete()V

    return-void
.end method
