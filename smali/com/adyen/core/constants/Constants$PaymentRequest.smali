.class public Lcom/adyen/core/constants/Constants$PaymentRequest;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/constants/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PaymentRequest"
.end annotation


# static fields
.field public static final ADYEN_UI_FINALIZE_INTENT:Ljava/lang/String; = "com.adyen.core.ui.finish"

.field public static final PAYMENT_DETAILS:Ljava/lang/String; = "PaymentDetails"

.field public static final PAYMENT_DETAILS_PROVIDED_INTENT:Ljava/lang/String; = "com.adyen.core.ui.PaymentDetailsProvided"

.field public static final PAYMENT_METHOD_SELECTED_INTENT:Ljava/lang/String; = "com.adyen.core.ui.PaymentMethodSelected"

.field public static final PAYMENT_REQUEST_CANCELLED_INTENT:Ljava/lang/String; = "com.adyen.core.ui.PaymentRequestCancelled"

.field public static final REDIRECT_HANDLED_INTENT:Ljava/lang/String; = "com.adyen.core.RedirectHandled"

.field public static final REDIRECT_PROBLEM_INTENT:Ljava/lang/String; = "com.adyen.core.RedirectProblem"

.field public static final REDIRECT_RETURN_URI_KEY:Ljava/lang/String; = "returnUri"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
