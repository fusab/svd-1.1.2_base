.class public Lcom/adyen/core/constants/Constants$DataKeys;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/constants/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DataKeys"
.end annotation


# static fields
.field public static final AMOUNT:Ljava/lang/String; = "amount"

.field public static final CVC_FIELD_STATUS:Ljava/lang/String; = "cvc_field_status"

.field public static final GENERATION_TIME:Ljava/lang/String; = "generation_time"

.field public static final PAYMENT_CARD_SCAN_ENABLED:Ljava/lang/String; = "payment_card_scan_enabled"

.field public static final PAYMENT_METHOD:Ljava/lang/String; = "PaymentMethod"

.field public static final PUBLIC_KEY:Ljava/lang/String; = "public_key"

.field public static final SHOPPER_REFERENCE:Ljava/lang/String; = "shopper_reference"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
