.class Lcom/adyen/core/PaymentBroadcastReceivers$5;
.super Landroid/content/BroadcastReceiver;
.source "PaymentBroadcastReceivers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/PaymentBroadcastReceivers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/core/PaymentBroadcastReceivers;


# direct methods
.method constructor <init>(Lcom/adyen/core/PaymentBroadcastReceivers;)V
    .locals 0

    .line 91
    iput-object p1, p0, Lcom/adyen/core/PaymentBroadcastReceivers$5;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    const-string p1, "PaymentMethod"

    .line 94
    invoke-virtual {p2, p1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/adyen/core/models/PaymentMethod;

    .line 95
    invoke-static {}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$100()Ljava/lang/String;

    move-result-object p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Payment Method Selected: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/adyen/core/models/PaymentMethod;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iget-object p2, p0, Lcom/adyen/core/PaymentBroadcastReceivers$5;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-static {p2}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$300(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/interfaces/PaymentMethodCallback;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/adyen/core/interfaces/PaymentMethodCallback;->completionWithPaymentMethod(Lcom/adyen/core/models/PaymentMethod;)V

    return-void
.end method
