.class public interface abstract Lcom/adyen/core/interfaces/PaymentMethodCallback;
.super Ljava/lang/Object;
.source "PaymentMethodCallback.java"

# interfaces
.implements Ljava/io/Serializable;


# virtual methods
.method public abstract completionWithPaymentMethod(Lcom/adyen/core/models/PaymentMethod;)V
    .param p1    # Lcom/adyen/core/models/PaymentMethod;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method
