.class public interface abstract Lcom/adyen/core/interfaces/State$StateChangeListener;
.super Ljava/lang/Object;
.source "State.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/interfaces/State;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "StateChangeListener"
.end annotation


# virtual methods
.method public abstract onStateChanged(Lcom/adyen/core/interfaces/State;)V
.end method

.method public abstract onStateNotChanged(Lcom/adyen/core/interfaces/State;)V
.end method
