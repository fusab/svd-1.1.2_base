.class public interface abstract Lcom/adyen/core/interfaces/State;
.super Ljava/lang/Object;
.source "State.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/core/interfaces/State$StateChangeListener;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "State"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method


# virtual methods
.method public abstract onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;
.end method
