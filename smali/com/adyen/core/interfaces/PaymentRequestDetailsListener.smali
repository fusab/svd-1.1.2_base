.class public interface abstract Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;
.super Ljava/lang/Object;
.source "PaymentRequestDetailsListener.java"


# virtual methods
.method public abstract onPaymentDetailsRequired(Lcom/adyen/core/PaymentRequest;Ljava/util/Collection;Lcom/adyen/core/interfaces/PaymentDetailsCallback;)V
    .param p1    # Lcom/adyen/core/PaymentRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Collection;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/adyen/core/interfaces/PaymentDetailsCallback;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/adyen/core/PaymentRequest;",
            "Ljava/util/Collection<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail;",
            ">;",
            "Lcom/adyen/core/interfaces/PaymentDetailsCallback;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onPaymentMethodSelectionRequired(Lcom/adyen/core/PaymentRequest;Ljava/util/List;Ljava/util/List;Lcom/adyen/core/interfaces/PaymentMethodCallback;)V
    .param p1    # Lcom/adyen/core/PaymentRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/adyen/core/interfaces/PaymentMethodCallback;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/adyen/core/PaymentRequest;",
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;",
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;",
            "Lcom/adyen/core/interfaces/PaymentMethodCallback;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onRedirectRequired(Lcom/adyen/core/PaymentRequest;Ljava/lang/String;Lcom/adyen/core/interfaces/UriCallback;)V
    .param p1    # Lcom/adyen/core/PaymentRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/adyen/core/interfaces/UriCallback;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method
