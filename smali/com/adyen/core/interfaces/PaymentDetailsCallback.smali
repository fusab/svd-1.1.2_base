.class public interface abstract Lcom/adyen/core/interfaces/PaymentDetailsCallback;
.super Ljava/lang/Object;
.source "PaymentDetailsCallback.java"


# virtual methods
.method public abstract completionWithPaymentDetails(Lcom/adyen/core/models/paymentdetails/PaymentDetails;)V
.end method

.method public abstract completionWithPaymentDetails(Ljava/util/Map;)V
    .param p1    # Ljava/util/Map;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method
