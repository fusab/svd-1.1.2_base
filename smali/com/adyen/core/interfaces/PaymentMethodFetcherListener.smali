.class public interface abstract Lcom/adyen/core/interfaces/PaymentMethodFetcherListener;
.super Ljava/lang/Object;
.source "PaymentMethodFetcherListener.java"


# virtual methods
.method public abstract onPaymentMethodsFetched(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;)V"
        }
    .end annotation
.end method
