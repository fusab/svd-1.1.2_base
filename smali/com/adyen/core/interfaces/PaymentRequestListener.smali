.class public interface abstract Lcom/adyen/core/interfaces/PaymentRequestListener;
.super Ljava/lang/Object;
.source "PaymentRequestListener.java"


# virtual methods
.method public abstract onPaymentDataRequested(Lcom/adyen/core/PaymentRequest;Ljava/lang/String;Lcom/adyen/core/interfaces/PaymentDataCallback;)V
    .param p1    # Lcom/adyen/core/PaymentRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/adyen/core/interfaces/PaymentDataCallback;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onPaymentResult(Lcom/adyen/core/PaymentRequest;Lcom/adyen/core/models/PaymentRequestResult;)V
    .param p1    # Lcom/adyen/core/PaymentRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/adyen/core/models/PaymentRequestResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method
