.class public final Lcom/adyen/core/models/PaymentMethod;
.super Ljava/lang/Object;
.source "PaymentMethod.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/core/models/PaymentMethod$Configuration;,
        Lcom/adyen/core/models/PaymentMethod$Card;,
        Lcom/adyen/core/models/PaymentMethod$Type;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x23ea3dc2090ecd34L


# instance fields
.field private card:Lcom/adyen/core/models/PaymentMethod$Card;

.field private configuration:Lcom/adyen/core/models/PaymentMethod$Configuration;

.field private inputDetails:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail;",
            ">;"
        }
    .end annotation
.end field

.field private isOneClick:Z

.field private logoUrl:Ljava/lang/String;

.field private memberPaymentMethods:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;"
        }
    .end annotation
.end field

.field private name:Ljava/lang/String;

.field private paymentMethodData:Ljava/lang/String;

.field private type:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static createContainerPaymentMethod(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/adyen/core/models/PaymentMethod;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 122
    new-instance v0, Lcom/adyen/core/models/PaymentMethod;

    invoke-direct {v0}, Lcom/adyen/core/models/PaymentMethod;-><init>()V

    const-string v1, "group"

    .line 124
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "type"

    .line 125
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/adyen/core/models/PaymentMethod;->type:Ljava/lang/String;

    const-string v3, "name"

    .line 126
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/adyen/core/models/PaymentMethod;->name:Ljava/lang/String;

    .line 127
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".png"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/adyen/core/models/PaymentMethod;->logoUrl:Ljava/lang/String;

    const-string p1, "paymentMethodData"

    .line 128
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/adyen/core/models/PaymentMethod;->paymentMethodData:Ljava/lang/String;

    const-string p1, "inputDetails"

    .line 130
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 132
    invoke-static {p0}, Lcom/adyen/core/models/PaymentMethod;->parseInputDetails(Lorg/json/JSONArray;)Ljava/util/Collection;

    move-result-object p0

    iput-object p0, v0, Lcom/adyen/core/models/PaymentMethod;->inputDetails:Ljava/util/Collection;

    :cond_0
    return-object v0
.end method

.method static createPaymentMethod(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/adyen/core/models/PaymentMethod;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 71
    invoke-static {p0, p1, v0}, Lcom/adyen/core/models/PaymentMethod;->createPaymentMethod(Lorg/json/JSONObject;Ljava/lang/String;Z)Lcom/adyen/core/models/PaymentMethod;

    move-result-object p0

    return-object p0
.end method

.method static createPaymentMethod(Lorg/json/JSONObject;Ljava/lang/String;Z)Lcom/adyen/core/models/PaymentMethod;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 76
    new-instance v0, Lcom/adyen/core/models/PaymentMethod;

    invoke-direct {v0}, Lcom/adyen/core/models/PaymentMethod;-><init>()V

    .line 78
    iput-boolean p2, v0, Lcom/adyen/core/models/PaymentMethod;->isOneClick:Z

    const-string p2, "type"

    .line 80
    invoke-virtual {p0, p2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Lcom/adyen/core/models/PaymentMethod;->type:Ljava/lang/String;

    const-string p2, "name"

    .line 81
    invoke-virtual {p0, p2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Lcom/adyen/core/models/PaymentMethod;->name:Ljava/lang/String;

    const-string p2, "paymentMethodData"

    .line 82
    invoke-virtual {p0, p2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Lcom/adyen/core/models/PaymentMethod;->paymentMethodData:Ljava/lang/String;

    .line 83
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/adyen/core/models/PaymentMethod;->getType()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".png"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/adyen/core/models/PaymentMethod;->logoUrl:Ljava/lang/String;

    const-string p1, "inputDetails"

    .line 85
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 87
    invoke-static {p1}, Lcom/adyen/core/models/PaymentMethod;->parseInputDetails(Lorg/json/JSONArray;)Ljava/util/Collection;

    move-result-object p1

    iput-object p1, v0, Lcom/adyen/core/models/PaymentMethod;->inputDetails:Ljava/util/Collection;

    :cond_0
    const-string p1, "card"

    .line 90
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result p2

    const/4 v1, 0x0

    if-nez p2, :cond_1

    .line 91
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    .line 92
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\u2022\u2022\u2022\u2022 "

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "number"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Lcom/adyen/core/models/PaymentMethod;->name:Ljava/lang/String;

    .line 93
    new-instance p2, Lcom/adyen/core/models/PaymentMethod$Card;

    invoke-direct {p2, p1, v1}, Lcom/adyen/core/models/PaymentMethod$Card;-><init>(Lorg/json/JSONObject;Lcom/adyen/core/models/PaymentMethod$1;)V

    iput-object p2, v0, Lcom/adyen/core/models/PaymentMethod;->card:Lcom/adyen/core/models/PaymentMethod$Card;

    .line 95
    :cond_1
    new-instance p1, Lcom/adyen/core/models/PaymentMethod$Configuration;

    invoke-direct {p1, v1}, Lcom/adyen/core/models/PaymentMethod$Configuration;-><init>(Lcom/adyen/core/models/PaymentMethod$1;)V

    const-string p2, "configuration"

    .line 96
    invoke-virtual {p0, p2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 97
    invoke-virtual {p0, p2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0

    const-string p2, "merchantIdentifier"

    .line 98
    invoke-virtual {p0, p2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/adyen/core/models/PaymentMethod$Configuration;->access$202(Lcom/adyen/core/models/PaymentMethod$Configuration;Ljava/lang/String;)Ljava/lang/String;

    const-string p2, "merchantName"

    .line 99
    invoke-virtual {p0, p2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/adyen/core/models/PaymentMethod$Configuration;->access$302(Lcom/adyen/core/models/PaymentMethod$Configuration;Ljava/lang/String;)Ljava/lang/String;

    const-string p2, "publicKey"

    .line 100
    invoke-virtual {p0, p2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string v1, "\\r\\n"

    const-string v2, ""

    invoke-virtual {p2, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/adyen/core/models/PaymentMethod$Configuration;->access$402(Lcom/adyen/core/models/PaymentMethod$Configuration;Ljava/lang/String;)Ljava/lang/String;

    const-string p2, "environment"

    .line 101
    invoke-virtual {p0, p2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p1, p0}, Lcom/adyen/core/models/PaymentMethod$Configuration;->access$502(Lcom/adyen/core/models/PaymentMethod$Configuration;Ljava/lang/String;)Ljava/lang/String;

    .line 104
    :cond_2
    iget-object p0, v0, Lcom/adyen/core/models/PaymentMethod;->inputDetails:Ljava/util/Collection;

    if-eqz p0, :cond_4

    .line 105
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 106
    invoke-virtual {p2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getConfiguration()Ljava/util/Map;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 107
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_3

    const-string p0, "cvcOptional"

    .line 108
    invoke-interface {p2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    invoke-static {p1, p0}, Lcom/adyen/core/models/PaymentMethod$Configuration;->access$602(Lcom/adyen/core/models/PaymentMethod$Configuration;Ljava/lang/String;)Ljava/lang/String;

    const-string p0, "noCVC"

    .line 109
    invoke-interface {p2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    invoke-static {p1, p0}, Lcom/adyen/core/models/PaymentMethod$Configuration;->access$702(Lcom/adyen/core/models/PaymentMethod$Configuration;Ljava/lang/String;)Ljava/lang/String;

    .line 115
    :cond_4
    iput-object p1, v0, Lcom/adyen/core/models/PaymentMethod;->configuration:Lcom/adyen/core/models/PaymentMethod$Configuration;

    return-object v0
.end method

.method private static parseInputDetails(Lorg/json/JSONArray;)Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/Collection<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 140
    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 141
    invoke-virtual {p0, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 144
    invoke-static {v2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->fromJson(Lorg/json/JSONObject;)Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 145
    invoke-static {v2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->fromJson(Lorg/json/JSONObject;)Lcom/adyen/core/models/paymentdetails/InputDetail;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method


# virtual methods
.method addMember(Lcom/adyen/core/models/PaymentMethod;)V
    .locals 1

    .line 254
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod;->memberPaymentMethods:Ljava/util/List;

    if-nez v0, :cond_0

    .line 255
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/adyen/core/models/PaymentMethod;->memberPaymentMethods:Ljava/util/List;

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod;->memberPaymentMethods:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 349
    instance-of v0, p1, Lcom/adyen/core/models/PaymentMethod;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/adyen/core/models/PaymentMethod;

    .line 350
    invoke-virtual {p1}, Lcom/adyen/core/models/PaymentMethod;->getPaymentMethodData()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod;->paymentMethodData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public getConfiguration()Lcom/adyen/core/models/PaymentMethod$Configuration;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 250
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod;->configuration:Lcom/adyen/core/models/PaymentMethod$Configuration;

    return-object v0
.end method

.method public getInputDetails()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail;",
            ">;"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod;->inputDetails:Ljava/util/Collection;

    return-object v0
.end method

.method public getLogoUrl()Ljava/lang/String;
    .locals 1

    .line 200
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod;->logoUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getMemberPaymentMethods()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;"
        }
    .end annotation

    .line 209
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod;->memberPaymentMethods:Ljava/util/List;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPaymentMethodData()Ljava/lang/String;
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod;->paymentMethodData:Ljava/lang/String;

    return-object v0
.end method

.method public getPaymentModule()Lcom/adyen/core/models/PaymentModule;
    .locals 6
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 158
    invoke-static {}, Lcom/adyen/core/models/PaymentModule;->values()[Lcom/adyen/core/models/PaymentModule;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 159
    invoke-virtual {v3}, Lcom/adyen/core/models/PaymentModule;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/adyen/core/models/PaymentMethod;->type:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRequiredFieldsMap()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 364
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 365
    iget-object v1, p0, Lcom/adyen/core/models/PaymentMethod;->inputDetails:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 366
    invoke-virtual {v2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getSavedCard()Lcom/adyen/core/models/PaymentMethod$Card;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 172
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod;->card:Lcom/adyen/core/models/PaymentMethod$Card;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod;->type:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .line 355
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod;->paymentMethodData:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isOneClick()Z
    .locals 1

    .line 242
    iget-boolean v0, p0, Lcom/adyen/core/models/PaymentMethod;->isOneClick:Z

    return v0
.end method

.method public isRedirectMethod()Z
    .locals 2

    .line 218
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod;->type:Ljava/lang/String;

    const-string v1, "paypal"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod;->inputDetails:Ljava/util/Collection;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Lcom/adyen/core/models/PaymentMethod;->isOneClick:Z

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public requiresInput()Z
    .locals 2

    .line 226
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod;->inputDetails:Ljava/util/Collection;

    if-eqz v0, :cond_1

    .line 227
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 228
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->isOptional()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method
