.class public final enum Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;
.super Ljava/lang/Enum;
.source "CreditCardPaymentDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AddressKey"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

.field public static final enum city:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

.field public static final enum country:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

.field public static final enum houseNumberOrName:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

.field public static final enum postalCode:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

.field public static final enum stateOrProvince:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

.field public static final enum street:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 19
    new-instance v0, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    const/4 v1, 0x0

    const-string v2, "street"

    invoke-direct {v0, v2, v1}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->street:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    .line 20
    new-instance v0, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    const/4 v2, 0x1

    const-string v3, "houseNumberOrName"

    invoke-direct {v0, v3, v2}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->houseNumberOrName:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    .line 21
    new-instance v0, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    const/4 v3, 0x2

    const-string v4, "city"

    invoke-direct {v0, v4, v3}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->city:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    .line 22
    new-instance v0, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    const/4 v4, 0x3

    const-string v5, "country"

    invoke-direct {v0, v5, v4}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->country:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    .line 23
    new-instance v0, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    const/4 v5, 0x4

    const-string v6, "postalCode"

    invoke-direct {v0, v6, v5}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->postalCode:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    .line 24
    new-instance v0, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    const/4 v6, 0x5

    const-string v7, "stateOrProvince"

    invoke-direct {v0, v7, v6}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->stateOrProvince:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    const/4 v0, 0x6

    .line 18
    new-array v0, v0, [Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    sget-object v7, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->street:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    aput-object v7, v0, v1

    sget-object v1, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->houseNumberOrName:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->city:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    aput-object v1, v0, v3

    sget-object v1, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->country:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    aput-object v1, v0, v4

    sget-object v1, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->postalCode:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    aput-object v1, v0, v5

    sget-object v1, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->stateOrProvince:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    aput-object v1, v0, v6

    sput-object v0, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->$VALUES:[Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;
    .locals 1

    .line 18
    const-class v0, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    return-object p0
.end method

.method public static values()[Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;
    .locals 1

    .line 18
    sget-object v0, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->$VALUES:[Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    invoke-virtual {v0}, [Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    return-object v0
.end method
