.class public Lcom/adyen/core/models/paymentdetails/CVCOnlyPaymentDetails;
.super Lcom/adyen/core/models/paymentdetails/PaymentDetails;
.source "CVCOnlyPaymentDetails.java"


# static fields
.field public static final CARD_DETAILS_CVC:Ljava/lang/String; = "cardDetails.cvc"


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail;",
            ">;)V"
        }
    .end annotation

    .line 13
    invoke-direct {p0, p1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;-><init>(Ljava/util/Collection;)V

    return-void
.end method


# virtual methods
.method public fillCvc(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "cardDetails.cvc"

    .line 17
    invoke-super {p0, v0, p1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->fill(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method
