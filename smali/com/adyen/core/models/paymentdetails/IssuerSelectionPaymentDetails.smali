.class public Lcom/adyen/core/models/paymentdetails/IssuerSelectionPaymentDetails;
.super Lcom/adyen/core/models/paymentdetails/PaymentDetails;
.source "IssuerSelectionPaymentDetails.java"


# static fields
.field public static final ISSUER:Ljava/lang/String; = "issuer"


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail;",
            ">;)V"
        }
    .end annotation

    .line 13
    invoke-direct {p0, p1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;-><init>(Ljava/util/Collection;)V

    return-void
.end method


# virtual methods
.method public fillIssuer(Lcom/adyen/core/models/paymentdetails/InputDetail$Item;)Z
    .locals 0

    .line 27
    invoke-virtual {p1}, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/adyen/core/models/paymentdetails/IssuerSelectionPaymentDetails;->fillIssuer(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public fillIssuer(Ljava/lang/String;)Z
    .locals 4

    .line 17
    invoke-virtual {p0}, Lcom/adyen/core/models/paymentdetails/IssuerSelectionPaymentDetails;->getInputDetails()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 18
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "idealIssuer"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 19
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "issuer"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 20
    :cond_1
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0, p1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->fill(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method
