.class public Lcom/adyen/core/models/paymentdetails/IdealPaymentDetails;
.super Lcom/adyen/core/models/paymentdetails/PaymentDetails;
.source "IdealPaymentDetails.java"


# static fields
.field public static final IDEAL_ISSUER:Ljava/lang/String; = "idealIssuer"


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail;",
            ">;)V"
        }
    .end annotation

    .line 13
    invoke-direct {p0, p1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;-><init>(Ljava/util/Collection;)V

    return-void
.end method


# virtual methods
.method public fillIssuer(Lcom/adyen/core/models/paymentdetails/InputDetail$Item;)Z
    .locals 1

    .line 21
    invoke-virtual {p1}, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;->getId()Ljava/lang/String;

    move-result-object p1

    const-string v0, "idealIssuer"

    invoke-super {p0, v0, p1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->fill(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public fillIssuer(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "idealIssuer"

    .line 17
    invoke-super {p0, v0, p1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->fill(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method
