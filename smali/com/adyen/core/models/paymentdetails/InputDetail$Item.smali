.class public final Lcom/adyen/core/models/paymentdetails/InputDetail$Item;
.super Ljava/lang/Object;
.source "InputDetail.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/models/paymentdetails/InputDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Item"
.end annotation


# instance fields
.field private id:Ljava/lang/String;

.field private imageUrl:Ljava/lang/String;

.field private name:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static fromJson(Lorg/json/JSONObject;)Lcom/adyen/core/models/paymentdetails/InputDetail$Item;
    .locals 2
    .param p0    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 171
    new-instance v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;

    invoke-direct {v0}, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;-><init>()V

    const-string v1, "id"

    .line 172
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;->id:Ljava/lang/String;

    const-string v1, "imageUrl"

    .line 173
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;->imageUrl:Ljava/lang/String;

    const-string v1, "name"

    .line 174
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;->name:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;->imageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;->name:Ljava/lang/String;

    return-object v0
.end method
