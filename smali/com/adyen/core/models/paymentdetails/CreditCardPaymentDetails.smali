.class public Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails;
.super Lcom/adyen/core/models/paymentdetails/PaymentDetails;
.source "CreditCardPaymentDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;
    }
.end annotation


# static fields
.field public static final ADDITIONAL_DATA_CARD:Ljava/lang/String; = "additionalData.card.encrypted.json"

.field public static final BILLING_ADDRESS:Ljava/lang/String; = "billingAddress"

.field public static final CARD_HOLDER_NAME_REQUIRED:Ljava/lang/String; = "cardHolderNameRequired"

.field public static final INSTALLMENTS:Ljava/lang/String; = "installments"

.field public static final STORE_DETAILS:Ljava/lang/String; = "storeDetails"


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;-><init>(Ljava/util/Collection;)V

    return-void
.end method


# virtual methods
.method public fillCardToken(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "additionalData.card.encrypted.json"

    .line 32
    invoke-super {p0, v0, p1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->fill(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public fillNumberOfInstallments(S)Z
    .locals 1

    .line 40
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "installments"

    invoke-super {p0, v0, p1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->fill(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public fillStoreDetails(Z)Z
    .locals 1

    const-string v0, "storeDetails"

    .line 36
    invoke-super {p0, v0, p1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->fill(Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method public fillbillingAddressCity(Ljava/lang/String;)Z
    .locals 5

    .line 70
    invoke-virtual {p0}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails;->getInputDetails()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 71
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "billingAddress"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 72
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getInputDetails()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 73
    invoke-virtual {v2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->city:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    invoke-virtual {v4}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 74
    invoke-virtual {v2, p1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->fill(Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public fillbillingAddressCountry(Ljava/lang/String;)Z
    .locals 5

    .line 83
    invoke-virtual {p0}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails;->getInputDetails()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 84
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "billingAddress"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 85
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getInputDetails()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 86
    invoke-virtual {v2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->country:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    invoke-virtual {v4}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 87
    invoke-virtual {v2, p1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->fill(Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public fillbillingAddressHouseNumberOrName(Ljava/lang/String;)Z
    .locals 5

    .line 57
    invoke-virtual {p0}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails;->getInputDetails()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 58
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "billingAddress"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 59
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getInputDetails()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 60
    invoke-virtual {v2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->houseNumberOrName:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    invoke-virtual {v4}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 61
    invoke-virtual {v2, p1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->fill(Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public fillbillingAddressPostalCode(Ljava/lang/String;)Z
    .locals 5

    .line 96
    invoke-virtual {p0}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails;->getInputDetails()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 97
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "billingAddress"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 98
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getInputDetails()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 99
    invoke-virtual {v2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->postalCode:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    invoke-virtual {v4}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 100
    invoke-virtual {v2, p1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->fill(Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public fillbillingAddressStateOrProvince(Ljava/lang/String;)Z
    .locals 5

    .line 109
    invoke-virtual {p0}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails;->getInputDetails()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 110
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "billingAddress"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 111
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getInputDetails()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 112
    invoke-virtual {v2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->stateOrProvince:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    invoke-virtual {v4}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 113
    invoke-virtual {v2, p1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->fill(Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public fillbillingAddressStreet(Ljava/lang/String;)Z
    .locals 5

    .line 44
    invoke-virtual {p0}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails;->getInputDetails()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 45
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "billingAddress"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 46
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getInputDetails()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 47
    invoke-virtual {v2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->street:Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;

    invoke-virtual {v4}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails$AddressKey;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 48
    invoke-virtual {v2, p1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->fill(Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method
