.class public Lcom/adyen/core/models/paymentdetails/SepaDirectDebitPaymentDetails;
.super Lcom/adyen/core/models/paymentdetails/PaymentDetails;
.source "SepaDirectDebitPaymentDetails.java"


# static fields
.field private static final SEPA_IBAN_NUMBER:Ljava/lang/String; = "sepa.ibanNumber"

.field private static final SEPA_IBAN_OWNER:Ljava/lang/String; = "sepa.ownerName"


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0, p1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;-><init>(Ljava/util/Collection;)V

    return-void
.end method


# virtual methods
.method public fillIban(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "sepa.ibanNumber"

    .line 19
    invoke-super {p0, v0, p1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->fill(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public fillOwner(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "sepa.ownerName"

    .line 23
    invoke-super {p0, v0, p1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->fill(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method
