.class public final Lcom/adyen/core/models/paymentdetails/InputDetail;
.super Ljava/lang/Object;
.source "InputDetail.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/core/models/paymentdetails/InputDetail$Item;,
        Lcom/adyen/core/models/paymentdetails/InputDetail$Type;
    }
.end annotation


# instance fields
.field private configuration:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private inputDetails:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail;",
            ">;"
        }
    .end annotation
.end field

.field private items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail$Item;",
            ">;"
        }
    .end annotation
.end field

.field private key:Ljava/lang/String;

.field private optional:Z

.field private type:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

.field private value:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 23
    iput-boolean v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->optional:Z

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->items:Ljava/util/ArrayList;

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->configuration:Ljava/util/Map;

    return-void
.end method

.method private addInputDetail(Lcom/adyen/core/models/paymentdetails/InputDetail;)V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->inputDetails:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->inputDetails:Ljava/util/ArrayList;

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->inputDetails:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static fromJson(Lorg/json/JSONObject;)Lcom/adyen/core/models/paymentdetails/InputDetail;
    .locals 7
    .param p0    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 62
    new-instance v0, Lcom/adyen/core/models/paymentdetails/InputDetail;

    invoke-direct {v0}, Lcom/adyen/core/models/paymentdetails/InputDetail;-><init>()V

    const-string v1, "key"

    .line 63
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/adyen/core/models/paymentdetails/InputDetail;->key:Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "optional"

    .line 64
    invoke-virtual {p0, v2, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v0, Lcom/adyen/core/models/paymentdetails/InputDetail;->optional:Z

    const-string v2, "type"

    .line 65
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->fromString(Ljava/lang/String;)Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    move-result-object v2

    iput-object v2, v0, Lcom/adyen/core/models/paymentdetails/InputDetail;->type:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    const-string/jumbo v2, "value"

    .line 66
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/adyen/core/models/paymentdetails/InputDetail;->value:Ljava/lang/String;

    const-string v2, "configuration"

    .line 67
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 69
    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 70
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 71
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 72
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 73
    iget-object v6, v0, Lcom/adyen/core/models/paymentdetails/InputDetail;->configuration:Ljava/util/Map;

    invoke-interface {v6, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 77
    :cond_0
    iget-object v2, v0, Lcom/adyen/core/models/paymentdetails/InputDetail;->type:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    sget-object v3, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->Select:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    if-ne v2, v3, :cond_1

    const-string v2, "items"

    .line 78
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    const/4 v3, 0x0

    .line 79
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 80
    iget-object v4, v0, Lcom/adyen/core/models/paymentdetails/InputDetail;->items:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    invoke-static {v5}, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;->fromJson(Lorg/json/JSONObject;)Lcom/adyen/core/models/paymentdetails/InputDetail$Item;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    const-string v2, "inputDetails"

    .line 83
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 84
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p0

    .line 85
    :goto_2
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 86
    invoke-virtual {p0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->fromJson(Lorg/json/JSONObject;)Lcom/adyen/core/models/paymentdetails/InputDetail;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->addInputDetail(Lcom/adyen/core/models/paymentdetails/InputDetail;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    return-object v0
.end method


# virtual methods
.method public fill(Ljava/lang/String;)Z
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->value:Ljava/lang/String;

    const/4 p1, 0x1

    return p1
.end method

.method public fill(Z)Z
    .locals 2

    .line 40
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->type:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    sget-object v1, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->Boolean:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    if-ne v0, v1, :cond_0

    .line 41
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->value:Ljava/lang/String;

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public getConfiguration()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 124
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->configuration:Ljava/util/Map;

    return-object v0
.end method

.method public getInputDetails()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail;",
            ">;"
        }
    .end annotation

    .line 100
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->inputDetails:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getItems()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail$Item;",
            ">;"
        }
    .end annotation

    .line 116
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->items:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->key:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/adyen/core/models/paymentdetails/InputDetail$Type;
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->type:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->value:Ljava/lang/String;

    return-object v0
.end method

.method public isFilled()Z
    .locals 3

    .line 49
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->inputDetails:Ljava/util/ArrayList;

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 50
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->inputDetails:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 51
    invoke-virtual {v2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->isFilled()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_1
    return v1

    .line 57
    :cond_2
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->value:Ljava/lang/String;

    invoke-static {v0}, Lcom/adyen/core/utils/StringUtils;->isEmptyOrNull(Ljava/lang/String;)Z

    move-result v0

    xor-int/2addr v0, v1

    return v0
.end method

.method public isOptional()Z
    .locals 1

    .line 112
    iget-boolean v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail;->optional:Z

    return v0
.end method
