.class public Lcom/adyen/core/models/paymentdetails/QiwiWalletPaymentDetails;
.super Lcom/adyen/core/models/paymentdetails/PaymentDetails;
.source "QiwiWalletPaymentDetails.java"


# static fields
.field private static final QIWI_TELEPHONE_NUMBER:Ljava/lang/String; = "qiwiwallet.telephoneNumber"

.field private static final QIWI_TELEPHONE_NUMBER_PREFIX:Ljava/lang/String; = "qiwiwallet.telephoneNumberPrefix"


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0, p1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;-><init>(Ljava/util/Collection;)V

    return-void
.end method


# virtual methods
.method public fillTelephoneNumber(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    const-string v0, "qiwiwallet.telephoneNumber"

    .line 19
    invoke-super {p0, v0, p2}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->fill(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    const-string p2, "qiwiwallet.telephoneNumberPrefix"

    .line 20
    invoke-super {p0, p2, p1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->fill(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
