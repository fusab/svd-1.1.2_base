.class public final enum Lcom/adyen/core/models/paymentdetails/InputDetail$Type;
.super Ljava/lang/Enum;
.source "InputDetail.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/models/paymentdetails/InputDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/adyen/core/models/paymentdetails/InputDetail$Type;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

.field public static final enum Address:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

.field public static final enum AndroidPayToken:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

.field public static final enum ApplePayToken:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

.field public static final enum Boolean:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

.field public static final enum CardToken:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

.field public static final enum Cvc:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

.field public static final enum Iban:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

.field public static final enum SamsungPayToken:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

.field public static final enum Select:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

.field public static final enum Text:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

.field public static final enum Unknown:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;


# instance fields
.field private apiField:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 128
    new-instance v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    const/4 v1, 0x0

    const-string v2, "Text"

    const-string v3, "text"

    invoke-direct {v0, v2, v1, v3}, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->Text:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    .line 129
    new-instance v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    const/4 v2, 0x1

    const-string v3, "CardToken"

    const-string v4, "cardToken"

    invoke-direct {v0, v3, v2, v4}, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->CardToken:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    .line 130
    new-instance v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    const/4 v3, 0x2

    const-string v4, "Iban"

    const-string v5, "iban"

    invoke-direct {v0, v4, v3, v5}, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->Iban:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    .line 131
    new-instance v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    const/4 v4, 0x3

    const-string v5, "Select"

    const-string v6, "select"

    invoke-direct {v0, v5, v4, v6}, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->Select:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    .line 132
    new-instance v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    const/4 v5, 0x4

    const-string v6, "Boolean"

    const-string v7, "boolean"

    invoke-direct {v0, v6, v5, v7}, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->Boolean:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    .line 133
    new-instance v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    const/4 v6, 0x5

    const-string v7, "ApplePayToken"

    const-string v8, "applePayToken"

    invoke-direct {v0, v7, v6, v8}, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->ApplePayToken:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    .line 134
    new-instance v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    const/4 v7, 0x6

    const-string v8, "AndroidPayToken"

    const-string v9, "androidPayToken"

    invoke-direct {v0, v8, v7, v9}, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->AndroidPayToken:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    .line 135
    new-instance v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    const/4 v8, 0x7

    const-string v9, "SamsungPayToken"

    const-string v10, "samsungPayToken"

    invoke-direct {v0, v9, v8, v10}, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->SamsungPayToken:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    .line 136
    new-instance v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    const/16 v9, 0x8

    const-string v10, "Cvc"

    const-string v11, "cvc"

    invoke-direct {v0, v10, v9, v11}, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->Cvc:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    .line 137
    new-instance v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    const/16 v10, 0x9

    const-string v11, "Address"

    const-string v12, "address"

    invoke-direct {v0, v11, v10, v12}, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->Address:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    .line 138
    new-instance v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    const/16 v11, 0xa

    const-string v12, "Unknown"

    invoke-direct {v0, v12, v11, v12}, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->Unknown:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    const/16 v0, 0xb

    .line 127
    new-array v0, v0, [Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    sget-object v12, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->Text:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    aput-object v12, v0, v1

    sget-object v1, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->CardToken:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->Iban:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->Select:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->Boolean:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->ApplePayToken:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->AndroidPayToken:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->SamsungPayToken:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    aput-object v1, v0, v8

    sget-object v1, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->Cvc:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    aput-object v1, v0, v9

    sget-object v1, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->Address:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    aput-object v1, v0, v10

    sget-object v1, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->Unknown:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    aput-object v1, v0, v11

    sput-object v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->$VALUES:[Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 142
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 143
    iput-object p3, p0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->apiField:Ljava/lang/String;

    return-void
.end method

.method static fromString(Ljava/lang/String;)Lcom/adyen/core/models/paymentdetails/InputDetail$Type;
    .locals 5
    .param p0    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 151
    invoke-static {}, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->values()[Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 152
    invoke-virtual {v3}, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->getApiField()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 156
    :cond_1
    sget-object p0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->Unknown:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/adyen/core/models/paymentdetails/InputDetail$Type;
    .locals 1

    .line 127
    const-class v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    return-object p0
.end method

.method public static values()[Lcom/adyen/core/models/paymentdetails/InputDetail$Type;
    .locals 1

    .line 127
    sget-object v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->$VALUES:[Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    invoke-virtual {v0}, [Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    return-object v0
.end method


# virtual methods
.method public getApiField()Ljava/lang/String;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->apiField:Ljava/lang/String;

    return-object v0
.end method
