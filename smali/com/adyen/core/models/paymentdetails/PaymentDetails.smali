.class public Lcom/adyen/core/models/paymentdetails/PaymentDetails;
.super Ljava/lang/Object;
.source "PaymentDetails.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private inputDetails:Ljava/util/Map;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/adyen/core/models/paymentdetails/InputDetail;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->inputDetails:Ljava/util/Map;

    .line 30
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 31
    iget-object v1, p0, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->inputDetails:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public fill(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->inputDetails:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/adyen/core/models/paymentdetails/InputDetail;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 63
    :cond_0
    invoke-virtual {p1, p2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->fill(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public fill(Ljava/lang/String;Z)Z
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->inputDetails:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/adyen/core/models/paymentdetails/InputDetail;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 77
    :cond_0
    invoke-virtual {p1, p2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->fill(Z)Z

    move-result p1

    return p1
.end method

.method public getInputDetail(Ljava/lang/String;)Lcom/adyen/core/models/paymentdetails/InputDetail;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 94
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->inputDetails:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/adyen/core/models/paymentdetails/InputDetail;

    return-object p1
.end method

.method public getInputDetails()Ljava/util/Collection;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail;",
            ">;"
        }
    .end annotation

    .line 90
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->inputDetails:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public hasKey(Ljava/lang/String;)Z
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->inputDetails:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public isEmpty()Z
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->inputDetails:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isFilled()Z
    .locals 3

    .line 81
    iget-object v0, p0, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->inputDetails:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 82
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->isFilled()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->isOptional()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_1
    const/4 v0, 0x1

    return v0
.end method
