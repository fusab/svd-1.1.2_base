.class public Lcom/adyen/core/models/Payment;
.super Ljava/lang/Object;
.source "Payment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/core/models/Payment$PaymentStatus;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Payment"


# instance fields
.field private payload:Ljava/lang/String;

.field private paymentStatus:Lcom/adyen/core/models/Payment$PaymentStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    sget-object v0, Lcom/adyen/core/models/Payment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "resultCode"

    .line 76
    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/adyen/core/models/Payment$PaymentStatus;->getPaymentStatus(Ljava/lang/String;)Lcom/adyen/core/models/Payment$PaymentStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/models/Payment;->paymentStatus:Lcom/adyen/core/models/Payment$PaymentStatus;

    const-string v0, "payload"

    .line 77
    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/adyen/core/models/Payment;->payload:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 1
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    :try_start_0
    const-string v0, "resultCode"

    .line 60
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/adyen/core/models/Payment$PaymentStatus;->getPaymentStatus(Ljava/lang/String;)Lcom/adyen/core/models/Payment$PaymentStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/models/Payment;->paymentStatus:Lcom/adyen/core/models/Payment$PaymentStatus;

    const-string v0, "payload"

    .line 61
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/adyen/core/models/Payment;->payload:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 63
    :catch_0
    sget-object p1, Lcom/adyen/core/models/Payment;->TAG:Ljava/lang/String;

    const-string v0, "Payment result code cannot be resolved."

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    sget-object p1, Lcom/adyen/core/models/Payment$PaymentStatus;->ERROR:Lcom/adyen/core/models/Payment$PaymentStatus;

    iput-object p1, p0, Lcom/adyen/core/models/Payment;->paymentStatus:Lcom/adyen/core/models/Payment$PaymentStatus;

    :goto_0
    return-void
.end method


# virtual methods
.method public getPayload()Ljava/lang/String;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/adyen/core/models/Payment;->payload:Ljava/lang/String;

    return-object v0
.end method

.method public getPaymentStatus()Lcom/adyen/core/models/Payment$PaymentStatus;
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/adyen/core/models/Payment;->paymentStatus:Lcom/adyen/core/models/Payment$PaymentStatus;

    return-object v0
.end method
