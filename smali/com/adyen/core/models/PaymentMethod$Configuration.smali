.class public final Lcom/adyen/core/models/PaymentMethod$Configuration;
.super Ljava/lang/Object;
.source "PaymentMethod.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/models/PaymentMethod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Configuration"
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x30eda02db7873ae5L


# instance fields
.field private cvcOptional:Ljava/lang/String;

.field private environment:Ljava/lang/String;

.field private merchantId:Ljava/lang/String;

.field private merchantName:Ljava/lang/String;

.field private noCVC:Ljava/lang/String;

.field private publicKey:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/adyen/core/models/PaymentMethod$1;)V
    .locals 0

    .line 310
    invoke-direct {p0}, Lcom/adyen/core/models/PaymentMethod$Configuration;-><init>()V

    return-void
.end method

.method static synthetic access$202(Lcom/adyen/core/models/PaymentMethod$Configuration;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/adyen/core/models/PaymentMethod$Configuration;->merchantId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/adyen/core/models/PaymentMethod$Configuration;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/adyen/core/models/PaymentMethod$Configuration;->merchantName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$402(Lcom/adyen/core/models/PaymentMethod$Configuration;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/adyen/core/models/PaymentMethod$Configuration;->publicKey:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/adyen/core/models/PaymentMethod$Configuration;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/adyen/core/models/PaymentMethod$Configuration;->environment:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lcom/adyen/core/models/PaymentMethod$Configuration;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/adyen/core/models/PaymentMethod$Configuration;->cvcOptional:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/adyen/core/models/PaymentMethod$Configuration;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/adyen/core/models/PaymentMethod$Configuration;->noCVC:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public getCvcOptional()Ljava/lang/String;
    .locals 1

    .line 335
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod$Configuration;->cvcOptional:Ljava/lang/String;

    return-object v0
.end method

.method public getEnvironment()Ljava/lang/String;
    .locals 1

    .line 343
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod$Configuration;->environment:Ljava/lang/String;

    return-object v0
.end method

.method public getMerchantId()Ljava/lang/String;
    .locals 1

    .line 323
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod$Configuration;->merchantId:Ljava/lang/String;

    return-object v0
.end method

.method public getMerchantName()Ljava/lang/String;
    .locals 1

    .line 327
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod$Configuration;->merchantName:Ljava/lang/String;

    return-object v0
.end method

.method public getNoCVC()Ljava/lang/String;
    .locals 1

    .line 339
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod$Configuration;->noCVC:Ljava/lang/String;

    return-object v0
.end method

.method public getPublicKey()Ljava/lang/String;
    .locals 1

    .line 331
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod$Configuration;->publicKey:Ljava/lang/String;

    return-object v0
.end method
