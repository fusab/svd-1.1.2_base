.class public Lcom/adyen/core/models/Amount;
.super Ljava/lang/Object;
.source "Amount.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x10c083edc20e93c8L


# instance fields
.field private currency:Ljava/lang/String;

.field private value:J


# direct methods
.method public constructor <init>(JLjava/lang/String;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-wide p1, p0, Lcom/adyen/core/models/Amount;->value:J

    .line 26
    iput-object p3, p0, Lcom/adyen/core/models/Amount;->currency:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCurrency()Ljava/lang/String;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/adyen/core/models/Amount;->currency:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()J
    .locals 2

    .line 42
    iget-wide v0, p0, Lcom/adyen/core/models/Amount;->value:J

    return-wide v0
.end method
