.class public Lcom/adyen/core/models/PaymentRequestResult;
.super Ljava/lang/Object;
.source "PaymentRequestResult.java"


# instance fields
.field private error:Ljava/lang/Throwable;

.field private payment:Lcom/adyen/core/models/Payment;


# direct methods
.method public constructor <init>(Lcom/adyen/core/models/Payment;)V
    .locals 0
    .param p1    # Lcom/adyen/core/models/Payment;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/adyen/core/models/PaymentRequestResult;->payment:Lcom/adyen/core/models/Payment;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/adyen/core/models/PaymentRequestResult;->error:Ljava/lang/Throwable;

    return-void
.end method


# virtual methods
.method public getError()Ljava/lang/Throwable;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/adyen/core/models/PaymentRequestResult;->error:Ljava/lang/Throwable;

    return-object v0
.end method

.method public getPayment()Lcom/adyen/core/models/Payment;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/adyen/core/models/PaymentRequestResult;->payment:Lcom/adyen/core/models/Payment;

    return-object v0
.end method

.method public isProcessed()Z
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/adyen/core/models/PaymentRequestResult;->error:Ljava/lang/Throwable;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
