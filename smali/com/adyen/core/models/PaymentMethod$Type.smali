.class public Lcom/adyen/core/models/PaymentMethod$Type;
.super Ljava/lang/Object;
.source "PaymentMethod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/models/PaymentMethod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Type"
.end annotation


# static fields
.field public static final BCMC:Ljava/lang/String; = "bcmc"

.field public static final CARD:Ljava/lang/String; = "card"

.field public static final IDEAL:Ljava/lang/String; = "ideal"

.field public static final PAYPAL:Ljava/lang/String; = "paypal"

.field public static final QIWI_WALLET:Ljava/lang/String; = "qiwiwallet"

.field public static final SEPA_DIRECT_DEBIT:Ljava/lang/String; = "sepadirectdebit"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
