.class public abstract enum Lcom/adyen/core/models/PaymentModule;
.super Ljava/lang/Enum;
.source "PaymentModule.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/adyen/core/models/PaymentModule;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/adyen/core/models/PaymentModule;

.field public static final enum androidpay:Lcom/adyen/core/models/PaymentModule;

.field public static final enum applepay:Lcom/adyen/core/models/PaymentModule;

.field public static final enum samsungpay:Lcom/adyen/core/models/PaymentModule;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 11
    new-instance v0, Lcom/adyen/core/models/PaymentModule$1;

    const/4 v1, 0x0

    const-string v2, "androidpay"

    invoke-direct {v0, v2, v1}, Lcom/adyen/core/models/PaymentModule$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/models/PaymentModule;->androidpay:Lcom/adyen/core/models/PaymentModule;

    .line 25
    new-instance v0, Lcom/adyen/core/models/PaymentModule$2;

    const/4 v2, 0x1

    const-string v3, "applepay"

    invoke-direct {v0, v3, v2}, Lcom/adyen/core/models/PaymentModule$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/models/PaymentModule;->applepay:Lcom/adyen/core/models/PaymentModule;

    .line 39
    new-instance v0, Lcom/adyen/core/models/PaymentModule$3;

    const/4 v3, 0x2

    const-string v4, "samsungpay"

    invoke-direct {v0, v4, v3}, Lcom/adyen/core/models/PaymentModule$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/models/PaymentModule;->samsungpay:Lcom/adyen/core/models/PaymentModule;

    const/4 v0, 0x3

    .line 9
    new-array v0, v0, [Lcom/adyen/core/models/PaymentModule;

    sget-object v4, Lcom/adyen/core/models/PaymentModule;->androidpay:Lcom/adyen/core/models/PaymentModule;

    aput-object v4, v0, v1

    sget-object v1, Lcom/adyen/core/models/PaymentModule;->applepay:Lcom/adyen/core/models/PaymentModule;

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/core/models/PaymentModule;->samsungpay:Lcom/adyen/core/models/PaymentModule;

    aput-object v1, v0, v3

    sput-object v0, Lcom/adyen/core/models/PaymentModule;->$VALUES:[Lcom/adyen/core/models/PaymentModule;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/adyen/core/models/PaymentModule$1;)V
    .locals 0

    .line 9
    invoke-direct {p0, p1, p2}, Lcom/adyen/core/models/PaymentModule;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/adyen/core/models/PaymentModule;
    .locals 1

    .line 9
    const-class v0, Lcom/adyen/core/models/PaymentModule;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/adyen/core/models/PaymentModule;

    return-object p0
.end method

.method public static values()[Lcom/adyen/core/models/PaymentModule;
    .locals 1

    .line 9
    sget-object v0, Lcom/adyen/core/models/PaymentModule;->$VALUES:[Lcom/adyen/core/models/PaymentModule;

    invoke-virtual {v0}, [Lcom/adyen/core/models/PaymentModule;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/adyen/core/models/PaymentModule;

    return-object v0
.end method


# virtual methods
.method public abstract getInputFieldsServiceName()Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract getServiceName()Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method
