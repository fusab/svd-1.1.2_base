.class public Lcom/adyen/core/models/Issuer;
.super Ljava/lang/Object;
.source "Issuer.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x656e7bd0cbda5bdaL


# instance fields
.field private issuerId:Ljava/lang/String;

.field private issuerLogoUrl:Ljava/lang/String;

.field private issuerName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lorg/json/JSONObject;)V
    .locals 1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "id"

    .line 20
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/models/Issuer;->issuerId:Ljava/lang/String;

    const-string v0, "imageUrl"

    .line 21
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/models/Issuer;->issuerLogoUrl:Ljava/lang/String;

    const-string v0, "name"

    .line 22
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/adyen/core/models/Issuer;->issuerName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getIssuerId()Ljava/lang/String;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/adyen/core/models/Issuer;->issuerId:Ljava/lang/String;

    return-object v0
.end method

.method public getIssuerLogoUrl()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/adyen/core/models/Issuer;->issuerLogoUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getIssuerName()Ljava/lang/String;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/adyen/core/models/Issuer;->issuerName:Ljava/lang/String;

    return-object v0
.end method
