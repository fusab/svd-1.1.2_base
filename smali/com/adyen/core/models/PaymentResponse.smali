.class public Lcom/adyen/core/models/PaymentResponse;
.super Ljava/lang/Object;
.source "PaymentResponse.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "PaymentResponse"


# instance fields
.field private amount:Lcom/adyen/core/models/Amount;

.field private countryCode:Ljava/lang/String;

.field private disableRecurringDetailUrl:Ljava/lang/String;

.field private generationTime:Ljava/lang/String;

.field private initiationUrl:Ljava/lang/String;

.field private logoBaseUrl:Ljava/lang/String;

.field private origin:Ljava/lang/String;

.field private paymentData:Ljava/lang/String;

.field private paymentMethods:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;"
        }
    .end annotation
.end field

.field private preferredPaymentMethods:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;"
        }
    .end annotation
.end field

.field private publicKey:Ljava/lang/String;

.field private publicKeyToken:Ljava/lang/String;

.field private reference:Ljava/lang/String;

.field private sessionValidity:Ljava/lang/String;

.field private shopperReference:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->paymentMethods:Ljava/util/List;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->preferredPaymentMethods:Ljava/util/List;

    return-void
.end method

.method public constructor <init>([B)V
    .locals 6
    .param p1    # [B
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->paymentMethods:Ljava/util/List;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->preferredPaymentMethods:Ljava/util/List;

    .line 46
    invoke-direct {p0, p1}, Lcom/adyen/core/models/PaymentResponse;->getJSONResponse([B)Lorg/json/JSONObject;

    move-result-object p1

    const-string v0, "generationtime"

    .line 49
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->generationTime:Ljava/lang/String;

    const-string v0, "initiationUrl"

    .line 50
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->initiationUrl:Ljava/lang/String;

    const-string v0, "paymentData"

    .line 51
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->paymentData:Ljava/lang/String;

    const-string v0, "logoBaseUrl"

    .line 52
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->logoBaseUrl:Ljava/lang/String;

    const-string v0, "origin"

    .line 53
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->origin:Ljava/lang/String;

    const-string v0, "publicKeyToken"

    .line 54
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->publicKeyToken:Ljava/lang/String;

    const-string v0, "payment"

    .line 56
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "countryCode"

    .line 57
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/adyen/core/models/PaymentResponse;->countryCode:Ljava/lang/String;

    const-string v1, "reference"

    .line 58
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/adyen/core/models/PaymentResponse;->reference:Ljava/lang/String;

    const-string v1, "sessionValidity"

    .line 59
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/adyen/core/models/PaymentResponse;->sessionValidity:Ljava/lang/String;

    const-string v1, "amount"

    .line 61
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 62
    new-instance v2, Lcom/adyen/core/models/Amount;

    const-string/jumbo v3, "value"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    const-string v5, "currency"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v4, v1}, Lcom/adyen/core/models/Amount;-><init>(JLjava/lang/String;)V

    iput-object v2, p0, Lcom/adyen/core/models/PaymentResponse;->amount:Lcom/adyen/core/models/Amount;

    const-string v1, "publicKey"

    .line 65
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/adyen/core/models/PaymentResponse;->publicKey:Ljava/lang/String;

    const-string v1, "shopperReference"

    .line 66
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->shopperReference:Ljava/lang/String;

    const-string v0, "paymentMethods"

    .line 69
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/adyen/core/models/PaymentResponse;->parsePaymentMethods(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->paymentMethods:Ljava/util/List;

    const-string v0, "recurringDetails"

    .line 70
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/adyen/core/models/PaymentResponse;->parsePreferredPaymentMethods(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->preferredPaymentMethods:Ljava/util/List;

    const-string v0, "disableRecurringDetailUrl"

    .line 72
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/adyen/core/models/PaymentResponse;->disableRecurringDetailUrl:Ljava/lang/String;

    return-void
.end method

.method private getJSONResponse([B)Lorg/json/JSONObject;
    .locals 3
    .param p1    # [B
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 177
    new-instance v0, Lorg/json/JSONObject;

    new-instance v1, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private parsePaymentMethods(Lorg/json/JSONArray;)Ljava/util/List;
    .locals 8
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 137
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_5

    .line 138
    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "group"

    .line 139
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 140
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "type"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 142
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/adyen/core/models/PaymentMethod;

    .line 143
    invoke-virtual {v6}, Lcom/adyen/core/models/PaymentMethod;->getType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 146
    invoke-virtual {p0}, Lcom/adyen/core/models/PaymentResponse;->getLogoBaseURL()Ljava/lang/String;

    move-result-object v4

    .line 145
    invoke-static {v3, v4}, Lcom/adyen/core/models/PaymentMethod;->createPaymentMethod(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/adyen/core/models/PaymentMethod;

    move-result-object v4

    invoke-virtual {v6, v4}, Lcom/adyen/core/models/PaymentMethod;->addMember(Lcom/adyen/core/models/PaymentMethod;)V

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    if-nez v4, :cond_4

    .line 152
    invoke-virtual {p0}, Lcom/adyen/core/models/PaymentResponse;->getLogoBaseURL()Ljava/lang/String;

    move-result-object v4

    .line 151
    invoke-static {v3, v4}, Lcom/adyen/core/models/PaymentMethod;->createContainerPaymentMethod(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/adyen/core/models/PaymentMethod;

    move-result-object v4

    .line 154
    invoke-virtual {p0}, Lcom/adyen/core/models/PaymentResponse;->getLogoBaseURL()Ljava/lang/String;

    move-result-object v5

    .line 153
    invoke-static {v3, v5}, Lcom/adyen/core/models/PaymentMethod;->createPaymentMethod(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/adyen/core/models/PaymentMethod;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/adyen/core/models/PaymentMethod;->addMember(Lcom/adyen/core/models/PaymentMethod;)V

    .line 155
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 158
    :cond_3
    invoke-virtual {p0}, Lcom/adyen/core/models/PaymentResponse;->getLogoBaseURL()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/adyen/core/models/PaymentMethod;->createPaymentMethod(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/adyen/core/models/PaymentMethod;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    return-object v0
.end method

.method private parsePreferredPaymentMethods(Lorg/json/JSONArray;)Ljava/util/List;
    .locals 5
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    .line 170
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 171
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {p0}, Lcom/adyen/core/models/PaymentResponse;->getLogoBaseURL()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/adyen/core/models/PaymentMethod;->createPaymentMethod(Lorg/json/JSONObject;Ljava/lang/String;Z)Lcom/adyen/core/models/PaymentMethod;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public getAmount()Lcom/adyen/core/models/Amount;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->amount:Lcom/adyen/core/models/Amount;

    return-object v0
.end method

.method public getAvailablePaymentMethods()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;"
        }
    .end annotation

    .line 124
    iget-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->paymentMethods:Ljava/util/List;

    return-object v0
.end method

.method public getCountryCode()Ljava/lang/String;
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->countryCode:Ljava/lang/String;

    return-object v0
.end method

.method public getDisableRecurringDetailUrl()Ljava/lang/String;
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->disableRecurringDetailUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getGenerationTime()Ljava/lang/String;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->generationTime:Ljava/lang/String;

    return-object v0
.end method

.method public getInitiationURL()Ljava/lang/String;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->initiationUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getLogoBaseURL()Ljava/lang/String;
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->logoBaseUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getOrigin()Ljava/lang/String;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->origin:Ljava/lang/String;

    return-object v0
.end method

.method public getPaymentData()Ljava/lang/String;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->paymentData:Ljava/lang/String;

    return-object v0
.end method

.method public getPreferredPaymentMethods()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;"
        }
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->preferredPaymentMethods:Ljava/util/List;

    return-object v0
.end method

.method public getPublicKey()Ljava/lang/String;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->publicKey:Ljava/lang/String;

    return-object v0
.end method

.method public getPublicKeyToken()Ljava/lang/String;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->publicKeyToken:Ljava/lang/String;

    return-object v0
.end method

.method public getReference()Ljava/lang/String;
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->reference:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionValidity()Ljava/lang/String;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->sessionValidity:Ljava/lang/String;

    return-object v0
.end method

.method public getShopperReference()Ljava/lang/String;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/adyen/core/models/PaymentResponse;->shopperReference:Ljava/lang/String;

    return-object v0
.end method
