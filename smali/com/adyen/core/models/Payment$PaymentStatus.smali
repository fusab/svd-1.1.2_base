.class public final enum Lcom/adyen/core/models/Payment$PaymentStatus;
.super Ljava/lang/Enum;
.source "Payment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/models/Payment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PaymentStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/adyen/core/models/Payment$PaymentStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/adyen/core/models/Payment$PaymentStatus;

.field public static final enum AUTHORISED:Lcom/adyen/core/models/Payment$PaymentStatus;

.field public static final enum CANCELLED:Lcom/adyen/core/models/Payment$PaymentStatus;

.field public static final enum ERROR:Lcom/adyen/core/models/Payment$PaymentStatus;

.field public static final enum RECEIVED:Lcom/adyen/core/models/Payment$PaymentStatus;

.field public static final enum REFUSED:Lcom/adyen/core/models/Payment$PaymentStatus;


# instance fields
.field private status:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 27
    new-instance v0, Lcom/adyen/core/models/Payment$PaymentStatus;

    const/4 v1, 0x0

    const-string v2, "RECEIVED"

    const-string v3, "Received"

    invoke-direct {v0, v2, v1, v3}, Lcom/adyen/core/models/Payment$PaymentStatus;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/adyen/core/models/Payment$PaymentStatus;->RECEIVED:Lcom/adyen/core/models/Payment$PaymentStatus;

    .line 28
    new-instance v0, Lcom/adyen/core/models/Payment$PaymentStatus;

    const/4 v2, 0x1

    const-string v3, "AUTHORISED"

    const-string v4, "Authorised"

    invoke-direct {v0, v3, v2, v4}, Lcom/adyen/core/models/Payment$PaymentStatus;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/adyen/core/models/Payment$PaymentStatus;->AUTHORISED:Lcom/adyen/core/models/Payment$PaymentStatus;

    .line 29
    new-instance v0, Lcom/adyen/core/models/Payment$PaymentStatus;

    const/4 v3, 0x2

    const-string v4, "ERROR"

    const-string v5, "Error"

    invoke-direct {v0, v4, v3, v5}, Lcom/adyen/core/models/Payment$PaymentStatus;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/adyen/core/models/Payment$PaymentStatus;->ERROR:Lcom/adyen/core/models/Payment$PaymentStatus;

    .line 30
    new-instance v0, Lcom/adyen/core/models/Payment$PaymentStatus;

    const/4 v4, 0x3

    const-string v5, "REFUSED"

    const-string v6, "Refused"

    invoke-direct {v0, v5, v4, v6}, Lcom/adyen/core/models/Payment$PaymentStatus;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/adyen/core/models/Payment$PaymentStatus;->REFUSED:Lcom/adyen/core/models/Payment$PaymentStatus;

    .line 31
    new-instance v0, Lcom/adyen/core/models/Payment$PaymentStatus;

    const/4 v5, 0x4

    const-string v6, "CANCELLED"

    const-string v7, "Cancelled"

    invoke-direct {v0, v6, v5, v7}, Lcom/adyen/core/models/Payment$PaymentStatus;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/adyen/core/models/Payment$PaymentStatus;->CANCELLED:Lcom/adyen/core/models/Payment$PaymentStatus;

    const/4 v0, 0x5

    .line 26
    new-array v0, v0, [Lcom/adyen/core/models/Payment$PaymentStatus;

    sget-object v6, Lcom/adyen/core/models/Payment$PaymentStatus;->RECEIVED:Lcom/adyen/core/models/Payment$PaymentStatus;

    aput-object v6, v0, v1

    sget-object v1, Lcom/adyen/core/models/Payment$PaymentStatus;->AUTHORISED:Lcom/adyen/core/models/Payment$PaymentStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/core/models/Payment$PaymentStatus;->ERROR:Lcom/adyen/core/models/Payment$PaymentStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/adyen/core/models/Payment$PaymentStatus;->REFUSED:Lcom/adyen/core/models/Payment$PaymentStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/adyen/core/models/Payment$PaymentStatus;->CANCELLED:Lcom/adyen/core/models/Payment$PaymentStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/adyen/core/models/Payment$PaymentStatus;->$VALUES:[Lcom/adyen/core/models/Payment$PaymentStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 36
    iput-object p3, p0, Lcom/adyen/core/models/Payment$PaymentStatus;->status:Ljava/lang/String;

    return-void
.end method

.method static getPaymentStatus(Ljava/lang/String;)Lcom/adyen/core/models/Payment$PaymentStatus;
    .locals 5

    .line 40
    invoke-static {}, Lcom/adyen/core/models/Payment$PaymentStatus;->values()[Lcom/adyen/core/models/Payment$PaymentStatus;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 41
    iget-object v4, v3, Lcom/adyen/core/models/Payment$PaymentStatus;->status:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/adyen/core/models/Payment$PaymentStatus;
    .locals 1

    .line 26
    const-class v0, Lcom/adyen/core/models/Payment$PaymentStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/adyen/core/models/Payment$PaymentStatus;

    return-object p0
.end method

.method public static values()[Lcom/adyen/core/models/Payment$PaymentStatus;
    .locals 1

    .line 26
    sget-object v0, Lcom/adyen/core/models/Payment$PaymentStatus;->$VALUES:[Lcom/adyen/core/models/Payment$PaymentStatus;

    invoke-virtual {v0}, [Lcom/adyen/core/models/Payment$PaymentStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/adyen/core/models/Payment$PaymentStatus;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/adyen/core/models/Payment$PaymentStatus;->status:Ljava/lang/String;

    return-object v0
.end method
