.class public final Lcom/adyen/core/models/PaymentMethod$Card;
.super Ljava/lang/Object;
.source "PaymentMethod.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/models/PaymentMethod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Card"
.end annotation


# instance fields
.field private expiryMonth:Ljava/lang/String;

.field private expiryYear:Ljava/lang/String;

.field private holderName:Ljava/lang/String;

.field private number:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lorg/json/JSONObject;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "expiryMonth"

    .line 270
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/models/PaymentMethod$Card;->expiryMonth:Ljava/lang/String;

    const-string v0, "expiryYear"

    .line 271
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/models/PaymentMethod$Card;->expiryYear:Ljava/lang/String;

    const-string v0, "number"

    .line 272
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/core/models/PaymentMethod$Card;->number:Ljava/lang/String;

    const-string v0, "holderName"

    .line 274
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/adyen/core/models/PaymentMethod$Card;->holderName:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lorg/json/JSONObject;Lcom/adyen/core/models/PaymentMethod$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 263
    invoke-direct {p0, p1}, Lcom/adyen/core/models/PaymentMethod$Card;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public getExpiryMonth()Ljava/lang/String;
    .locals 1

    .line 282
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod$Card;->expiryMonth:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiryYear()Ljava/lang/String;
    .locals 1

    .line 289
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod$Card;->expiryYear:Ljava/lang/String;

    return-object v0
.end method

.method public getHolderName()Ljava/lang/String;
    .locals 1

    .line 296
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod$Card;->holderName:Ljava/lang/String;

    return-object v0
.end method

.method public getNumber()Ljava/lang/String;
    .locals 1

    .line 303
    iget-object v0, p0, Lcom/adyen/core/models/PaymentMethod$Card;->number:Ljava/lang/String;

    return-object v0
.end method
