.class Lcom/adyen/core/PaymentStateHandler$2;
.super Ljava/lang/Object;
.source "PaymentStateHandler.java"

# interfaces
.implements Lcom/adyen/core/interfaces/HttpResponseCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/core/PaymentStateHandler;->processPayment()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/core/PaymentStateHandler;


# direct methods
.method constructor <init>(Lcom/adyen/core/PaymentStateHandler;)V
    .locals 0

    .line 290
    iput-object p1, p0, Lcom/adyen/core/PaymentStateHandler$2;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .line 321
    invoke-static {}, Lcom/adyen/core/PaymentStateHandler;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "processPayment(): onFailure"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler$2;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-virtual {v0, p1}, Lcom/adyen/core/PaymentStateHandler;->setPaymentErrorThrowableAndTriggerError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onSuccess([B)V
    .locals 4
    .param p1    # [B
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 293
    invoke-static {}, Lcom/adyen/core/PaymentStateHandler;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "processPayment(): onSuccess"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    :try_start_0
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler$2;->this$0:Lcom/adyen/core/PaymentStateHandler;

    new-instance v1, Lorg/json/JSONObject;

    new-instance v2, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/adyen/core/PaymentStateHandler;->access$302(Lcom/adyen/core/PaymentStateHandler;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    .line 296
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler$2;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-static {p1}, Lcom/adyen/core/PaymentStateHandler;->access$300(Lcom/adyen/core/PaymentStateHandler;)Lorg/json/JSONObject;

    move-result-object p1

    const-string v0, "type"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "redirect"

    .line 297
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler$2;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-static {p1}, Lcom/adyen/core/PaymentStateHandler;->access$100(Lcom/adyen/core/PaymentStateHandler;)Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    move-result-object p1

    sget-object v0, Lcom/adyen/core/internals/PaymentTrigger;->REDIRECTION_REQUIRED:Lcom/adyen/core/internals/PaymentTrigger;

    invoke-virtual {p1, v0}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;

    goto/16 :goto_1

    :cond_0
    const-string v0, "complete"

    .line 299
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 300
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler$2;->this$0:Lcom/adyen/core/PaymentStateHandler;

    new-instance v0, Lcom/adyen/core/models/PaymentRequestResult;

    new-instance v1, Lcom/adyen/core/models/Payment;

    iget-object v2, p0, Lcom/adyen/core/PaymentStateHandler$2;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-static {v2}, Lcom/adyen/core/PaymentStateHandler;->access$300(Lcom/adyen/core/PaymentStateHandler;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/adyen/core/models/Payment;-><init>(Lorg/json/JSONObject;)V

    invoke-direct {v0, v1}, Lcom/adyen/core/models/PaymentRequestResult;-><init>(Lcom/adyen/core/models/Payment;)V

    invoke-static {p1, v0}, Lcom/adyen/core/PaymentStateHandler;->access$402(Lcom/adyen/core/PaymentStateHandler;Lcom/adyen/core/models/PaymentRequestResult;)Lcom/adyen/core/models/PaymentRequestResult;

    .line 301
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler$2;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-static {p1}, Lcom/adyen/core/PaymentStateHandler;->access$100(Lcom/adyen/core/PaymentStateHandler;)Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    move-result-object p1

    sget-object v0, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_RESULT_RECEIVED:Lcom/adyen/core/internals/PaymentTrigger;

    invoke-virtual {p1, v0}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;

    goto/16 :goto_1

    :cond_1
    const-string v0, "error"

    .line 302
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "validation"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    .line 308
    :cond_2
    invoke-static {}, Lcom/adyen/core/PaymentStateHandler;->access$200()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown response type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/adyen/core/PaymentStateHandler$2;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-static {v1}, Lcom/adyen/core/PaymentStateHandler;->access$300(Lcom/adyen/core/PaymentStateHandler;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler$2;->this$0:Lcom/adyen/core/PaymentStateHandler;

    new-instance v0, Lcom/adyen/core/exceptions/PostResponseFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown response type. Response must be redirect or complete."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/adyen/core/PaymentStateHandler$2;->this$0:Lcom/adyen/core/PaymentStateHandler;

    .line 310
    invoke-static {v2}, Lcom/adyen/core/PaymentStateHandler;->access$300(Lcom/adyen/core/PaymentStateHandler;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/adyen/core/exceptions/PostResponseFormatException;-><init>(Ljava/lang/String;)V

    .line 309
    invoke-static {p1, v0}, Lcom/adyen/core/PaymentStateHandler;->access$502(Lcom/adyen/core/PaymentStateHandler;Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 311
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler$2;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-static {p1}, Lcom/adyen/core/PaymentStateHandler;->access$100(Lcom/adyen/core/PaymentStateHandler;)Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    move-result-object p1

    sget-object v0, Lcom/adyen/core/internals/PaymentTrigger;->ERROR_OCCURRED:Lcom/adyen/core/internals/PaymentTrigger;

    invoke-virtual {p1, v0}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;

    goto :goto_1

    .line 303
    :cond_3
    :goto_0
    invoke-static {}, Lcom/adyen/core/PaymentStateHandler;->access$200()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Payment failed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/adyen/core/PaymentStateHandler$2;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-static {v1}, Lcom/adyen/core/PaymentStateHandler;->access$300(Lcom/adyen/core/PaymentStateHandler;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler$2;->this$0:Lcom/adyen/core/PaymentStateHandler;

    new-instance v0, Lcom/adyen/core/exceptions/PostResponseFormatException;

    iget-object v1, p0, Lcom/adyen/core/PaymentStateHandler$2;->this$0:Lcom/adyen/core/PaymentStateHandler;

    .line 305
    invoke-static {v1}, Lcom/adyen/core/PaymentStateHandler;->access$300(Lcom/adyen/core/PaymentStateHandler;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "errorMessage"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/adyen/core/exceptions/PostResponseFormatException;-><init>(Ljava/lang/String;)V

    .line 304
    invoke-static {p1, v0}, Lcom/adyen/core/PaymentStateHandler;->access$502(Lcom/adyen/core/PaymentStateHandler;Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 306
    iget-object p1, p0, Lcom/adyen/core/PaymentStateHandler$2;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-static {p1}, Lcom/adyen/core/PaymentStateHandler;->access$100(Lcom/adyen/core/PaymentStateHandler;)Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    move-result-object p1

    sget-object v0, Lcom/adyen/core/internals/PaymentTrigger;->ERROR_OCCURRED:Lcom/adyen/core/internals/PaymentTrigger;

    invoke-virtual {p1, v0}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 314
    invoke-static {}, Lcom/adyen/core/PaymentStateHandler;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "processPayment(): JSONException occurred"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 315
    iget-object v0, p0, Lcom/adyen/core/PaymentStateHandler$2;->this$0:Lcom/adyen/core/PaymentStateHandler;

    invoke-virtual {v0, p1}, Lcom/adyen/core/PaymentStateHandler;->setPaymentErrorThrowableAndTriggerError(Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method
