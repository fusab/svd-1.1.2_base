.class Lcom/adyen/core/PaymentBroadcastReceivers$2;
.super Ljava/lang/Object;
.source "PaymentBroadcastReceivers.java"

# interfaces
.implements Lcom/adyen/core/interfaces/PaymentDataCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/PaymentBroadcastReceivers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/core/PaymentBroadcastReceivers;


# direct methods
.method constructor <init>(Lcom/adyen/core/PaymentBroadcastReceivers;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/adyen/core/PaymentBroadcastReceivers$2;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public completionWithPaymentData([B)V
    .locals 3
    .param p1    # [B
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 57
    :try_start_0
    iget-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers$2;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-static {v0}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$000(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/PaymentStateHandler;

    move-result-object v0

    new-instance v1, Lcom/adyen/core/models/PaymentResponse;

    invoke-direct {v1, p1}, Lcom/adyen/core/models/PaymentResponse;-><init>([B)V

    invoke-virtual {v0, v1}, Lcom/adyen/core/PaymentStateHandler;->setPaymentResponse(Lcom/adyen/core/models/PaymentResponse;)V

    .line 58
    iget-object p1, p0, Lcom/adyen/core/PaymentBroadcastReceivers$2;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-static {p1}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$000(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/PaymentStateHandler;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adyen/core/PaymentStateHandler;->getPaymentProcessorStateMachine()Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    move-result-object p1

    sget-object v0, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_DATA_PROVIDED:Lcom/adyen/core/internals/PaymentTrigger;

    invoke-virtual {p1, v0}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 61
    invoke-static {}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Provided payment data response is invalid"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 62
    iget-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers$2;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-static {v0}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$000(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/PaymentStateHandler;

    move-result-object v0

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2, v1, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v2}, Lcom/adyen/core/PaymentStateHandler;->setPaymentErrorThrowableAndTriggerError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
