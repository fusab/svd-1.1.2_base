.class public final enum Lcom/adyen/core/internals/PaymentTrigger;
.super Ljava/lang/Enum;
.source "PaymentTrigger.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/adyen/core/internals/PaymentTrigger;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/adyen/core/internals/PaymentTrigger;

.field public static final enum ERROR_OCCURRED:Lcom/adyen/core/internals/PaymentTrigger;

.field public static final enum PAYMENT_CANCELLED:Lcom/adyen/core/internals/PaymentTrigger;

.field public static final enum PAYMENT_DATA_PROVIDED:Lcom/adyen/core/internals/PaymentTrigger;

.field public static final enum PAYMENT_DETAILS_NOT_REQUIRED:Lcom/adyen/core/internals/PaymentTrigger;

.field public static final enum PAYMENT_DETAILS_PROVIDED:Lcom/adyen/core/internals/PaymentTrigger;

.field public static final enum PAYMENT_DETAILS_REQUIRED:Lcom/adyen/core/internals/PaymentTrigger;

.field public static final enum PAYMENT_METHODS_AVAILABLE:Lcom/adyen/core/internals/PaymentTrigger;

.field public static final enum PAYMENT_REQUESTED:Lcom/adyen/core/internals/PaymentTrigger;

.field public static final enum PAYMENT_RESULT_RECEIVED:Lcom/adyen/core/internals/PaymentTrigger;

.field public static final enum PAYMENT_SELECTION_CANCELLED:Lcom/adyen/core/internals/PaymentTrigger;

.field public static final enum REDIRECTION_REQUIRED:Lcom/adyen/core/internals/PaymentTrigger;

.field public static final enum RETURN_URI_RECEIVED:Lcom/adyen/core/internals/PaymentTrigger;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .line 8
    new-instance v0, Lcom/adyen/core/internals/PaymentTrigger;

    const/4 v1, 0x0

    const-string v2, "PAYMENT_REQUESTED"

    invoke-direct {v0, v2, v1}, Lcom/adyen/core/internals/PaymentTrigger;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_REQUESTED:Lcom/adyen/core/internals/PaymentTrigger;

    .line 9
    new-instance v0, Lcom/adyen/core/internals/PaymentTrigger;

    const/4 v2, 0x1

    const-string v3, "PAYMENT_DATA_PROVIDED"

    invoke-direct {v0, v3, v2}, Lcom/adyen/core/internals/PaymentTrigger;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_DATA_PROVIDED:Lcom/adyen/core/internals/PaymentTrigger;

    .line 10
    new-instance v0, Lcom/adyen/core/internals/PaymentTrigger;

    const/4 v3, 0x2

    const-string v4, "PAYMENT_METHODS_AVAILABLE"

    invoke-direct {v0, v4, v3}, Lcom/adyen/core/internals/PaymentTrigger;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_METHODS_AVAILABLE:Lcom/adyen/core/internals/PaymentTrigger;

    .line 11
    new-instance v0, Lcom/adyen/core/internals/PaymentTrigger;

    const/4 v4, 0x3

    const-string v5, "PAYMENT_DETAILS_REQUIRED"

    invoke-direct {v0, v5, v4}, Lcom/adyen/core/internals/PaymentTrigger;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_DETAILS_REQUIRED:Lcom/adyen/core/internals/PaymentTrigger;

    .line 12
    new-instance v0, Lcom/adyen/core/internals/PaymentTrigger;

    const/4 v5, 0x4

    const-string v6, "PAYMENT_DETAILS_NOT_REQUIRED"

    invoke-direct {v0, v6, v5}, Lcom/adyen/core/internals/PaymentTrigger;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_DETAILS_NOT_REQUIRED:Lcom/adyen/core/internals/PaymentTrigger;

    .line 13
    new-instance v0, Lcom/adyen/core/internals/PaymentTrigger;

    const/4 v6, 0x5

    const-string v7, "PAYMENT_SELECTION_CANCELLED"

    invoke-direct {v0, v7, v6}, Lcom/adyen/core/internals/PaymentTrigger;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_SELECTION_CANCELLED:Lcom/adyen/core/internals/PaymentTrigger;

    .line 14
    new-instance v0, Lcom/adyen/core/internals/PaymentTrigger;

    const/4 v7, 0x6

    const-string v8, "PAYMENT_DETAILS_PROVIDED"

    invoke-direct {v0, v8, v7}, Lcom/adyen/core/internals/PaymentTrigger;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_DETAILS_PROVIDED:Lcom/adyen/core/internals/PaymentTrigger;

    .line 15
    new-instance v0, Lcom/adyen/core/internals/PaymentTrigger;

    const/4 v8, 0x7

    const-string v9, "REDIRECTION_REQUIRED"

    invoke-direct {v0, v9, v8}, Lcom/adyen/core/internals/PaymentTrigger;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentTrigger;->REDIRECTION_REQUIRED:Lcom/adyen/core/internals/PaymentTrigger;

    .line 16
    new-instance v0, Lcom/adyen/core/internals/PaymentTrigger;

    const/16 v9, 0x8

    const-string v10, "RETURN_URI_RECEIVED"

    invoke-direct {v0, v10, v9}, Lcom/adyen/core/internals/PaymentTrigger;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentTrigger;->RETURN_URI_RECEIVED:Lcom/adyen/core/internals/PaymentTrigger;

    .line 17
    new-instance v0, Lcom/adyen/core/internals/PaymentTrigger;

    const/16 v10, 0x9

    const-string v11, "PAYMENT_RESULT_RECEIVED"

    invoke-direct {v0, v11, v10}, Lcom/adyen/core/internals/PaymentTrigger;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_RESULT_RECEIVED:Lcom/adyen/core/internals/PaymentTrigger;

    .line 18
    new-instance v0, Lcom/adyen/core/internals/PaymentTrigger;

    const/16 v11, 0xa

    const-string v12, "ERROR_OCCURRED"

    invoke-direct {v0, v12, v11}, Lcom/adyen/core/internals/PaymentTrigger;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentTrigger;->ERROR_OCCURRED:Lcom/adyen/core/internals/PaymentTrigger;

    .line 19
    new-instance v0, Lcom/adyen/core/internals/PaymentTrigger;

    const/16 v12, 0xb

    const-string v13, "PAYMENT_CANCELLED"

    invoke-direct {v0, v13, v12}, Lcom/adyen/core/internals/PaymentTrigger;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_CANCELLED:Lcom/adyen/core/internals/PaymentTrigger;

    const/16 v0, 0xc

    .line 7
    new-array v0, v0, [Lcom/adyen/core/internals/PaymentTrigger;

    sget-object v13, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_REQUESTED:Lcom/adyen/core/internals/PaymentTrigger;

    aput-object v13, v0, v1

    sget-object v1, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_DATA_PROVIDED:Lcom/adyen/core/internals/PaymentTrigger;

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_METHODS_AVAILABLE:Lcom/adyen/core/internals/PaymentTrigger;

    aput-object v1, v0, v3

    sget-object v1, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_DETAILS_REQUIRED:Lcom/adyen/core/internals/PaymentTrigger;

    aput-object v1, v0, v4

    sget-object v1, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_DETAILS_NOT_REQUIRED:Lcom/adyen/core/internals/PaymentTrigger;

    aput-object v1, v0, v5

    sget-object v1, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_SELECTION_CANCELLED:Lcom/adyen/core/internals/PaymentTrigger;

    aput-object v1, v0, v6

    sget-object v1, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_DETAILS_PROVIDED:Lcom/adyen/core/internals/PaymentTrigger;

    aput-object v1, v0, v7

    sget-object v1, Lcom/adyen/core/internals/PaymentTrigger;->REDIRECTION_REQUIRED:Lcom/adyen/core/internals/PaymentTrigger;

    aput-object v1, v0, v8

    sget-object v1, Lcom/adyen/core/internals/PaymentTrigger;->RETURN_URI_RECEIVED:Lcom/adyen/core/internals/PaymentTrigger;

    aput-object v1, v0, v9

    sget-object v1, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_RESULT_RECEIVED:Lcom/adyen/core/internals/PaymentTrigger;

    aput-object v1, v0, v10

    sget-object v1, Lcom/adyen/core/internals/PaymentTrigger;->ERROR_OCCURRED:Lcom/adyen/core/internals/PaymentTrigger;

    aput-object v1, v0, v11

    sget-object v1, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_CANCELLED:Lcom/adyen/core/internals/PaymentTrigger;

    aput-object v1, v0, v12

    sput-object v0, Lcom/adyen/core/internals/PaymentTrigger;->$VALUES:[Lcom/adyen/core/internals/PaymentTrigger;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/adyen/core/internals/PaymentTrigger;
    .locals 1

    .line 7
    const-class v0, Lcom/adyen/core/internals/PaymentTrigger;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/adyen/core/internals/PaymentTrigger;

    return-object p0
.end method

.method public static values()[Lcom/adyen/core/internals/PaymentTrigger;
    .locals 1

    .line 7
    sget-object v0, Lcom/adyen/core/internals/PaymentTrigger;->$VALUES:[Lcom/adyen/core/internals/PaymentTrigger;

    invoke-virtual {v0}, [Lcom/adyen/core/internals/PaymentTrigger;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/adyen/core/internals/PaymentTrigger;

    return-object v0
.end method
