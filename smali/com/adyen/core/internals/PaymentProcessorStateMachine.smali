.class public Lcom/adyen/core/internals/PaymentProcessorStateMachine;
.super Ljava/lang/Object;
.source "PaymentProcessorStateMachine.java"

# interfaces
.implements Lcom/adyen/core/interfaces/State;
.implements Lcom/adyen/core/interfaces/State$StateChangeListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "PaymentProcessorStateMachine"


# instance fields
.field private paymentProcessorState:Lcom/adyen/core/interfaces/State;

.field private stateChangeListener:Lcom/adyen/core/interfaces/State$StateChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/adyen/core/interfaces/State$StateChangeListener;)V
    .locals 2

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    sget-object v0, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->TAG:Ljava/lang/String;

    const-string v1, "PaymentProcessorStateMachine() constructed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    sget-object v0, Lcom/adyen/core/internals/PaymentRequestState;->IDLE:Lcom/adyen/core/internals/PaymentRequestState;

    iput-object v0, p0, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->paymentProcessorState:Lcom/adyen/core/interfaces/State;

    .line 20
    iput-object p1, p0, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->stateChangeListener:Lcom/adyen/core/interfaces/State$StateChangeListener;

    return-void
.end method


# virtual methods
.method public onStateChanged(Lcom/adyen/core/interfaces/State;)V
    .locals 1

    .line 25
    iput-object p1, p0, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->paymentProcessorState:Lcom/adyen/core/interfaces/State;

    .line 26
    iget-object p1, p0, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->stateChangeListener:Lcom/adyen/core/interfaces/State$StateChangeListener;

    iget-object v0, p0, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->paymentProcessorState:Lcom/adyen/core/interfaces/State;

    invoke-interface {p1, v0}, Lcom/adyen/core/interfaces/State$StateChangeListener;->onStateChanged(Lcom/adyen/core/interfaces/State;)V

    return-void
.end method

.method public onStateNotChanged(Lcom/adyen/core/interfaces/State;)V
    .locals 1

    .line 31
    iget-object p1, p0, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->stateChangeListener:Lcom/adyen/core/interfaces/State$StateChangeListener;

    iget-object v0, p0, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->paymentProcessorState:Lcom/adyen/core/interfaces/State;

    invoke-interface {p1, v0}, Lcom/adyen/core/interfaces/State$StateChangeListener;->onStateNotChanged(Lcom/adyen/core/interfaces/State;)V

    return-void
.end method

.method public onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->paymentProcessorState:Lcom/adyen/core/interfaces/State;

    invoke-interface {v0, p1}, Lcom/adyen/core/interfaces/State;->onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;

    move-result-object p1

    .line 37
    iget-object v0, p0, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->paymentProcessorState:Lcom/adyen/core/interfaces/State;

    if-eq v0, p1, :cond_0

    .line 38
    iput-object p1, p0, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->paymentProcessorState:Lcom/adyen/core/interfaces/State;

    .line 39
    invoke-virtual {p0, p1}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onStateChanged(Lcom/adyen/core/interfaces/State;)V

    goto :goto_0

    .line 41
    :cond_0
    invoke-virtual {p0, v0}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onStateNotChanged(Lcom/adyen/core/interfaces/State;)V

    :goto_0
    return-object p1
.end method
