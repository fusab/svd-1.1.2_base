.class public Lcom/adyen/core/internals/InputFieldsServiceFactory;
.super Ljava/lang/Object;
.source "InputFieldsServiceFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getService(Lcom/adyen/core/models/PaymentModule;)Lcom/adyen/core/services/InputFieldsService;
    .locals 0
    .param p1    # Lcom/adyen/core/models/PaymentModule;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;
        }
    .end annotation

    .line 13
    invoke-virtual {p1}, Lcom/adyen/core/models/PaymentModule;->getInputFieldsServiceName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/adyen/core/services/InputFieldsService;

    return-object p1
.end method
