.class public final Lcom/adyen/core/internals/ModuleAvailabilityUtil;
.super Ljava/lang/Object;
.source "ModuleAvailabilityUtil.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;Lcom/adyen/core/models/PaymentMethod;Lcom/adyen/core/interfaces/PaymentMethodAvailabilityCallback;)V
    .locals 0

    .line 25
    invoke-static {p0, p1, p2}, Lcom/adyen/core/internals/ModuleAvailabilityUtil;->isPaymentMethodAvailable(Landroid/content/Context;Lcom/adyen/core/models/PaymentMethod;Lcom/adyen/core/interfaces/PaymentMethodAvailabilityCallback;)V

    return-void
.end method

.method public static filterPaymentMethods(Landroid/content/Context;Ljava/util/List;)Lio/reactivex/Observable;
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;)",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;>;"
        }
    .end annotation

    .line 52
    invoke-static {p1}, Lio/reactivex/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1;

    invoke-direct {v0, p0}, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->concatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    .line 78
    invoke-virtual {p0}, Lio/reactivex/Observable;->toList()Lio/reactivex/Single;

    move-result-object p0

    .line 79
    invoke-virtual {p0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p0

    .line 80
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->newThread()Lio/reactivex/Scheduler;

    move-result-object p1

    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p0

    .line 81
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object p1

    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static getModulePaymentService(Lcom/adyen/core/models/PaymentModule;)Lcom/adyen/core/services/PaymentMethodService;
    .locals 1
    .param p0    # Lcom/adyen/core/models/PaymentModule;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InstantiationException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/adyen/core/internals/PaymentMethodServiceFactory;

    invoke-direct {v0}, Lcom/adyen/core/internals/PaymentMethodServiceFactory;-><init>()V

    .line 33
    :try_start_0
    invoke-virtual {v0, p0}, Lcom/adyen/core/internals/PaymentMethodServiceFactory;->getService(Lcom/adyen/core/models/PaymentModule;)Lcom/adyen/core/services/PaymentMethodService;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 42
    throw p0

    :catch_1
    move-exception p0

    .line 39
    throw p0

    :catch_2
    move-exception p0

    .line 36
    throw p0
.end method

.method private static isPaymentMethodAvailable(Landroid/content/Context;Lcom/adyen/core/models/PaymentMethod;Lcom/adyen/core/interfaces/PaymentMethodAvailabilityCallback;)V
    .locals 1

    .line 94
    invoke-virtual {p1}, Lcom/adyen/core/models/PaymentMethod;->getPaymentModule()Lcom/adyen/core/models/PaymentModule;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 97
    :try_start_0
    invoke-virtual {p1}, Lcom/adyen/core/models/PaymentMethod;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/adyen/core/models/PaymentModule;->valueOf(Ljava/lang/String;)Lcom/adyen/core/models/PaymentModule;

    move-result-object v0

    invoke-static {v0}, Lcom/adyen/core/internals/ModuleAvailabilityUtil;->getModulePaymentService(Lcom/adyen/core/models/PaymentModule;)Lcom/adyen/core/services/PaymentMethodService;

    move-result-object v0

    .line 98
    invoke-interface {v0, p0, p1, p2}, Lcom/adyen/core/services/PaymentMethodService;->checkAvailability(Landroid/content/Context;Lcom/adyen/core/models/PaymentMethod;Lcom/adyen/core/interfaces/PaymentMethodAvailabilityCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p0, 0x0

    .line 100
    invoke-interface {p2, p0}, Lcom/adyen/core/interfaces/PaymentMethodAvailabilityCallback;->onSuccess(Z)V

    goto :goto_0

    :cond_0
    const/4 p0, 0x1

    .line 103
    invoke-interface {p2, p0}, Lcom/adyen/core/interfaces/PaymentMethodAvailabilityCallback;->onSuccess(Z)V

    :goto_0
    return-void
.end method
