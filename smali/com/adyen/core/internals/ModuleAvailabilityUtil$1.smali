.class final Lcom/adyen/core/internals/ModuleAvailabilityUtil$1;
.super Ljava/lang/Object;
.source "ModuleAvailabilityUtil.java"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/core/internals/ModuleAvailabilityUtil;->filterPaymentMethods(Landroid/content/Context;Ljava/util/List;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "Lcom/adyen/core/models/PaymentMethod;",
        "Lio/reactivex/Observable<",
        "Lcom/adyen/core/models/PaymentMethod;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/adyen/core/models/PaymentMethod;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/adyen/core/models/PaymentMethod;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;"
        }
    .end annotation

    .line 56
    new-instance v0, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1;

    invoke-direct {v0, p0, p1}, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1;-><init>(Lcom/adyen/core/internals/ModuleAvailabilityUtil$1;Lcom/adyen/core/models/PaymentMethod;)V

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 53
    check-cast p1, Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {p0, p1}, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1;->apply(Lcom/adyen/core/models/PaymentMethod;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
