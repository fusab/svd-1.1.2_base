.class Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1$1;
.super Ljava/lang/Object;
.source "ModuleAvailabilityUtil.java"

# interfaces
.implements Lcom/adyen/core/interfaces/PaymentMethodAvailabilityCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1;->subscribe(Lio/reactivex/ObservableEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1;

.field final synthetic val$subscriber:Lio/reactivex/ObservableEmitter;


# direct methods
.method constructor <init>(Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1;Lio/reactivex/ObservableEmitter;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1$1;->this$1:Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1;

    iput-object p2, p0, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1$1;->val$subscriber:Lio/reactivex/ObservableEmitter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFail(Ljava/lang/Throwable;)V
    .locals 0

    .line 71
    iget-object p1, p0, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1$1;->val$subscriber:Lio/reactivex/ObservableEmitter;

    invoke-interface {p1}, Lio/reactivex/ObservableEmitter;->onComplete()V

    return-void
.end method

.method public onSuccess(Z)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1$1;->val$subscriber:Lio/reactivex/ObservableEmitter;

    invoke-interface {v0}, Lio/reactivex/ObservableEmitter;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 64
    iget-object p1, p0, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1$1;->val$subscriber:Lio/reactivex/ObservableEmitter;

    iget-object v0, p0, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1$1;->this$1:Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1;

    iget-object v0, v0, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1;->val$paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-interface {p1, v0}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    .line 66
    :cond_0
    iget-object p1, p0, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1$1;->val$subscriber:Lio/reactivex/ObservableEmitter;

    invoke-interface {p1}, Lio/reactivex/ObservableEmitter;->onComplete()V

    return-void
.end method
