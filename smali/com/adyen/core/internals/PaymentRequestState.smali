.class public abstract enum Lcom/adyen/core/internals/PaymentRequestState;
.super Ljava/lang/Enum;
.source "PaymentRequestState.java"

# interfaces
.implements Lcom/adyen/core/interfaces/State;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/adyen/core/internals/PaymentRequestState;",
        ">;",
        "Lcom/adyen/core/interfaces/State;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/adyen/core/internals/PaymentRequestState;

.field public static final enum ABORTED:Lcom/adyen/core/internals/PaymentRequestState;

.field public static final enum CANCELLED:Lcom/adyen/core/internals/PaymentRequestState;

.field public static final enum FETCHING_AND_FILTERING_PAYMENT_METHODS:Lcom/adyen/core/internals/PaymentRequestState;

.field public static final enum IDLE:Lcom/adyen/core/internals/PaymentRequestState;

.field public static final enum PROCESSED:Lcom/adyen/core/internals/PaymentRequestState;

.field public static final enum PROCESSING_PAYMENT:Lcom/adyen/core/internals/PaymentRequestState;

.field public static final enum WAITING_FOR_PAYMENT_DATA:Lcom/adyen/core/internals/PaymentRequestState;

.field public static final enum WAITING_FOR_PAYMENT_METHOD_DETAILS:Lcom/adyen/core/internals/PaymentRequestState;

.field public static final enum WAITING_FOR_PAYMENT_METHOD_SELECTION:Lcom/adyen/core/internals/PaymentRequestState;

.field public static final enum WAITING_FOR_REDIRECTION:Lcom/adyen/core/internals/PaymentRequestState;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 12
    new-instance v0, Lcom/adyen/core/internals/PaymentRequestState$1;

    const/4 v1, 0x0

    const-string v2, "IDLE"

    invoke-direct {v0, v2, v1}, Lcom/adyen/core/internals/PaymentRequestState$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentRequestState;->IDLE:Lcom/adyen/core/internals/PaymentRequestState;

    .line 30
    new-instance v0, Lcom/adyen/core/internals/PaymentRequestState$2;

    const/4 v2, 0x1

    const-string v3, "WAITING_FOR_PAYMENT_DATA"

    invoke-direct {v0, v3, v2}, Lcom/adyen/core/internals/PaymentRequestState$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentRequestState;->WAITING_FOR_PAYMENT_DATA:Lcom/adyen/core/internals/PaymentRequestState;

    .line 51
    new-instance v0, Lcom/adyen/core/internals/PaymentRequestState$3;

    const/4 v3, 0x2

    const-string v4, "FETCHING_AND_FILTERING_PAYMENT_METHODS"

    invoke-direct {v0, v4, v3}, Lcom/adyen/core/internals/PaymentRequestState$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentRequestState;->FETCHING_AND_FILTERING_PAYMENT_METHODS:Lcom/adyen/core/internals/PaymentRequestState;

    .line 72
    new-instance v0, Lcom/adyen/core/internals/PaymentRequestState$4;

    const/4 v4, 0x3

    const-string v5, "WAITING_FOR_PAYMENT_METHOD_SELECTION"

    invoke-direct {v0, v5, v4}, Lcom/adyen/core/internals/PaymentRequestState$4;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentRequestState;->WAITING_FOR_PAYMENT_METHOD_SELECTION:Lcom/adyen/core/internals/PaymentRequestState;

    .line 96
    new-instance v0, Lcom/adyen/core/internals/PaymentRequestState$5;

    const/4 v5, 0x4

    const-string v6, "WAITING_FOR_PAYMENT_METHOD_DETAILS"

    invoke-direct {v0, v6, v5}, Lcom/adyen/core/internals/PaymentRequestState$5;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentRequestState;->WAITING_FOR_PAYMENT_METHOD_DETAILS:Lcom/adyen/core/internals/PaymentRequestState;

    .line 123
    new-instance v0, Lcom/adyen/core/internals/PaymentRequestState$6;

    const/4 v6, 0x5

    const-string v7, "PROCESSING_PAYMENT"

    invoke-direct {v0, v7, v6}, Lcom/adyen/core/internals/PaymentRequestState$6;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentRequestState;->PROCESSING_PAYMENT:Lcom/adyen/core/internals/PaymentRequestState;

    .line 147
    new-instance v0, Lcom/adyen/core/internals/PaymentRequestState$7;

    const/4 v7, 0x6

    const-string v8, "WAITING_FOR_REDIRECTION"

    invoke-direct {v0, v8, v7}, Lcom/adyen/core/internals/PaymentRequestState$7;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentRequestState;->WAITING_FOR_REDIRECTION:Lcom/adyen/core/internals/PaymentRequestState;

    .line 183
    new-instance v0, Lcom/adyen/core/internals/PaymentRequestState$8;

    const/4 v8, 0x7

    const-string v9, "PROCESSED"

    invoke-direct {v0, v9, v8}, Lcom/adyen/core/internals/PaymentRequestState$8;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentRequestState;->PROCESSED:Lcom/adyen/core/internals/PaymentRequestState;

    .line 196
    new-instance v0, Lcom/adyen/core/internals/PaymentRequestState$9;

    const/16 v9, 0x8

    const-string v10, "ABORTED"

    invoke-direct {v0, v10, v9}, Lcom/adyen/core/internals/PaymentRequestState$9;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentRequestState;->ABORTED:Lcom/adyen/core/internals/PaymentRequestState;

    .line 209
    new-instance v0, Lcom/adyen/core/internals/PaymentRequestState$10;

    const/16 v10, 0x9

    const-string v11, "CANCELLED"

    invoke-direct {v0, v11, v10}, Lcom/adyen/core/internals/PaymentRequestState$10;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/core/internals/PaymentRequestState;->CANCELLED:Lcom/adyen/core/internals/PaymentRequestState;

    const/16 v0, 0xa

    .line 11
    new-array v0, v0, [Lcom/adyen/core/internals/PaymentRequestState;

    sget-object v11, Lcom/adyen/core/internals/PaymentRequestState;->IDLE:Lcom/adyen/core/internals/PaymentRequestState;

    aput-object v11, v0, v1

    sget-object v1, Lcom/adyen/core/internals/PaymentRequestState;->WAITING_FOR_PAYMENT_DATA:Lcom/adyen/core/internals/PaymentRequestState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/core/internals/PaymentRequestState;->FETCHING_AND_FILTERING_PAYMENT_METHODS:Lcom/adyen/core/internals/PaymentRequestState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/adyen/core/internals/PaymentRequestState;->WAITING_FOR_PAYMENT_METHOD_SELECTION:Lcom/adyen/core/internals/PaymentRequestState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/adyen/core/internals/PaymentRequestState;->WAITING_FOR_PAYMENT_METHOD_DETAILS:Lcom/adyen/core/internals/PaymentRequestState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/adyen/core/internals/PaymentRequestState;->PROCESSING_PAYMENT:Lcom/adyen/core/internals/PaymentRequestState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/adyen/core/internals/PaymentRequestState;->WAITING_FOR_REDIRECTION:Lcom/adyen/core/internals/PaymentRequestState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/adyen/core/internals/PaymentRequestState;->PROCESSED:Lcom/adyen/core/internals/PaymentRequestState;

    aput-object v1, v0, v8

    sget-object v1, Lcom/adyen/core/internals/PaymentRequestState;->ABORTED:Lcom/adyen/core/internals/PaymentRequestState;

    aput-object v1, v0, v9

    sget-object v1, Lcom/adyen/core/internals/PaymentRequestState;->CANCELLED:Lcom/adyen/core/internals/PaymentRequestState;

    aput-object v1, v0, v10

    sput-object v0, Lcom/adyen/core/internals/PaymentRequestState;->$VALUES:[Lcom/adyen/core/internals/PaymentRequestState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/adyen/core/internals/PaymentRequestState$1;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1, p2}, Lcom/adyen/core/internals/PaymentRequestState;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/adyen/core/internals/PaymentRequestState;
    .locals 1

    .line 11
    const-class v0, Lcom/adyen/core/internals/PaymentRequestState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/adyen/core/internals/PaymentRequestState;

    return-object p0
.end method

.method public static values()[Lcom/adyen/core/internals/PaymentRequestState;
    .locals 1

    .line 11
    sget-object v0, Lcom/adyen/core/internals/PaymentRequestState;->$VALUES:[Lcom/adyen/core/internals/PaymentRequestState;

    invoke-virtual {v0}, [Lcom/adyen/core/internals/PaymentRequestState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/adyen/core/internals/PaymentRequestState;

    return-object v0
.end method
