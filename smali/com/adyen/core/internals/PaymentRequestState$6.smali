.class final enum Lcom/adyen/core/internals/PaymentRequestState$6;
.super Lcom/adyen/core/internals/PaymentRequestState;
.source "PaymentRequestState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/internals/PaymentRequestState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    .line 123
    invoke-direct {p0, p1, p2, v0}, Lcom/adyen/core/internals/PaymentRequestState;-><init>(Ljava/lang/String;ILcom/adyen/core/internals/PaymentRequestState$1;)V

    return-void
.end method


# virtual methods
.method public onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;
    .locals 3

    .line 127
    sget-object v0, Lcom/adyen/core/internals/PaymentRequestState$11;->$SwitchMap$com$adyen$core$internals$PaymentTrigger:[I

    invoke-virtual {p1}, Lcom/adyen/core/internals/PaymentTrigger;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_1

    const/16 v1, 0xb

    if-eq v0, v1, :cond_0

    .line 141
    sget-object v0, Lcom/adyen/core/internals/PaymentRequestState$6;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/adyen/core/internals/PaymentRequestState$6;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " - Unknown trigger received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/adyen/core/internals/PaymentTrigger;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object p1, p0

    goto :goto_0

    .line 132
    :cond_0
    sget-object p1, Lcom/adyen/core/internals/PaymentRequestState$6;->WAITING_FOR_REDIRECTION:Lcom/adyen/core/internals/PaymentRequestState;

    goto :goto_0

    .line 129
    :cond_1
    sget-object p1, Lcom/adyen/core/internals/PaymentRequestState$6;->PROCESSED:Lcom/adyen/core/internals/PaymentRequestState;

    goto :goto_0

    .line 138
    :cond_2
    sget-object p1, Lcom/adyen/core/internals/PaymentRequestState$6;->CANCELLED:Lcom/adyen/core/internals/PaymentRequestState;

    goto :goto_0

    .line 135
    :cond_3
    sget-object p1, Lcom/adyen/core/internals/PaymentRequestState$6;->ABORTED:Lcom/adyen/core/internals/PaymentRequestState;

    :goto_0
    return-object p1
.end method
