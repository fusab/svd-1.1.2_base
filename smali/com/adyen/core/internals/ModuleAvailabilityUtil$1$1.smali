.class Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1;
.super Ljava/lang/Object;
.source "ModuleAvailabilityUtil.java"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/core/internals/ModuleAvailabilityUtil$1;->apply(Lcom/adyen/core/models/PaymentMethod;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOnSubscribe<",
        "Lcom/adyen/core/models/PaymentMethod;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/core/internals/ModuleAvailabilityUtil$1;

.field final synthetic val$paymentMethod:Lcom/adyen/core/models/PaymentMethod;


# direct methods
.method constructor <init>(Lcom/adyen/core/internals/ModuleAvailabilityUtil$1;Lcom/adyen/core/models/PaymentMethod;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1;->this$0:Lcom/adyen/core/internals/ModuleAvailabilityUtil$1;

    iput-object p2, p0, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1;->val$paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;)V"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1;->this$0:Lcom/adyen/core/internals/ModuleAvailabilityUtil$1;

    iget-object v0, v0, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1;->val$paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    new-instance v2, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1$1;

    invoke-direct {v2, p0, p1}, Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1$1;-><init>(Lcom/adyen/core/internals/ModuleAvailabilityUtil$1$1;Lio/reactivex/ObservableEmitter;)V

    invoke-static {v0, v1, v2}, Lcom/adyen/core/internals/ModuleAvailabilityUtil;->access$000(Landroid/content/Context;Lcom/adyen/core/models/PaymentMethod;Lcom/adyen/core/interfaces/PaymentMethodAvailabilityCallback;)V

    return-void
.end method
