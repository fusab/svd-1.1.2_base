.class Lcom/adyen/core/PaymentBroadcastReceivers$7;
.super Landroid/content/BroadcastReceiver;
.source "PaymentBroadcastReceivers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/PaymentBroadcastReceivers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/core/PaymentBroadcastReceivers;


# direct methods
.method constructor <init>(Lcom/adyen/core/PaymentBroadcastReceivers;)V
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/adyen/core/PaymentBroadcastReceivers$7;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    const-string v0, "androidpay.token"

    .line 116
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "androidpay.error"

    .line 117
    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 120
    invoke-static {}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "androidPayInfoListener failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    iget-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers$7;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-static {v0}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$000(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/PaymentStateHandler;

    move-result-object v0

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1, p2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/adyen/core/PaymentStateHandler;->setPaymentErrorThrowableAndTriggerError(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 123
    :cond_0
    new-instance p2, Lcom/adyen/core/models/paymentdetails/PaymentDetails;

    iget-object v1, p0, Lcom/adyen/core/PaymentBroadcastReceivers$7;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-static {v1}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$000(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/PaymentStateHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adyen/core/PaymentStateHandler;->getPaymentMethod()Lcom/adyen/core/models/PaymentMethod;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adyen/core/models/PaymentMethod;->getInputDetails()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {p2, v1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;-><init>(Ljava/util/Collection;)V

    if-eqz v0, :cond_2

    .line 125
    invoke-virtual {p2}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->getInputDetails()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 126
    invoke-virtual {v2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getType()Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    move-result-object v3

    sget-object v4, Lcom/adyen/core/models/paymentdetails/InputDetail$Type;->AndroidPayToken:Lcom/adyen/core/models/paymentdetails/InputDetail$Type;

    if-ne v3, v4, :cond_1

    .line 127
    invoke-virtual {v2, v0}, Lcom/adyen/core/models/paymentdetails/InputDetail;->fill(Ljava/lang/String;)Z

    goto :goto_0

    .line 131
    :cond_2
    iget-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers$7;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-static {v0}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$400(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/interfaces/PaymentDetailsCallback;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/adyen/core/interfaces/PaymentDetailsCallback;->completionWithPaymentDetails(Lcom/adyen/core/models/paymentdetails/PaymentDetails;)V

    .line 133
    :goto_1
    invoke-static {p1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
