.class Lcom/adyen/core/PaymentBroadcastReceivers;
.super Ljava/lang/Object;
.source "PaymentBroadcastReceivers.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "PaymentBroadcastReceivers"


# instance fields
.field private androidPayInfoListener:Landroid/content/BroadcastReceiver;

.field private paymentDataCallback:Lcom/adyen/core/interfaces/PaymentDataCallback;

.field private paymentDetailsCallback:Lcom/adyen/core/interfaces/PaymentDetailsCallback;

.field private paymentDetailsReceiver:Landroid/content/BroadcastReceiver;

.field private paymentMethodCallback:Lcom/adyen/core/interfaces/PaymentMethodCallback;

.field private paymentMethodSelectionReceiver:Landroid/content/BroadcastReceiver;

.field private paymentRequest:Lcom/adyen/core/PaymentRequest;

.field private paymentRequestCancellationReceiver:Landroid/content/BroadcastReceiver;

.field private paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/adyen/core/PaymentStateHandler;Lcom/adyen/core/PaymentRequest;)V
    .locals 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Lcom/adyen/core/PaymentBroadcastReceivers$1;

    invoke-direct {v0, p0}, Lcom/adyen/core/PaymentBroadcastReceivers$1;-><init>(Lcom/adyen/core/PaymentBroadcastReceivers;)V

    iput-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentMethodCallback:Lcom/adyen/core/interfaces/PaymentMethodCallback;

    .line 53
    new-instance v0, Lcom/adyen/core/PaymentBroadcastReceivers$2;

    invoke-direct {v0, p0}, Lcom/adyen/core/PaymentBroadcastReceivers$2;-><init>(Lcom/adyen/core/PaymentBroadcastReceivers;)V

    iput-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentDataCallback:Lcom/adyen/core/interfaces/PaymentDataCallback;

    .line 69
    new-instance v0, Lcom/adyen/core/PaymentBroadcastReceivers$3;

    invoke-direct {v0, p0}, Lcom/adyen/core/PaymentBroadcastReceivers$3;-><init>(Lcom/adyen/core/PaymentBroadcastReceivers;)V

    iput-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentDetailsCallback:Lcom/adyen/core/interfaces/PaymentDetailsCallback;

    .line 83
    new-instance v0, Lcom/adyen/core/PaymentBroadcastReceivers$4;

    invoke-direct {v0, p0}, Lcom/adyen/core/PaymentBroadcastReceivers$4;-><init>(Lcom/adyen/core/PaymentBroadcastReceivers;)V

    iput-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentRequestCancellationReceiver:Landroid/content/BroadcastReceiver;

    .line 91
    new-instance v0, Lcom/adyen/core/PaymentBroadcastReceivers$5;

    invoke-direct {v0, p0}, Lcom/adyen/core/PaymentBroadcastReceivers$5;-><init>(Lcom/adyen/core/PaymentBroadcastReceivers;)V

    iput-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentMethodSelectionReceiver:Landroid/content/BroadcastReceiver;

    .line 100
    new-instance v0, Lcom/adyen/core/PaymentBroadcastReceivers$6;

    invoke-direct {v0, p0}, Lcom/adyen/core/PaymentBroadcastReceivers$6;-><init>(Lcom/adyen/core/PaymentBroadcastReceivers;)V

    iput-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentDetailsReceiver:Landroid/content/BroadcastReceiver;

    .line 113
    new-instance v0, Lcom/adyen/core/PaymentBroadcastReceivers$7;

    invoke-direct {v0, p0}, Lcom/adyen/core/PaymentBroadcastReceivers$7;-><init>(Lcom/adyen/core/PaymentBroadcastReceivers;)V

    iput-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->androidPayInfoListener:Landroid/content/BroadcastReceiver;

    .line 34
    iput-object p2, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentRequest:Lcom/adyen/core/PaymentRequest;

    .line 35
    iput-object p1, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;

    return-void
.end method

.method static synthetic access$000(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/PaymentStateHandler;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;

    return-object p0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .line 26
    sget-object v0, Lcom/adyen/core/PaymentBroadcastReceivers;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/PaymentRequest;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentRequest:Lcom/adyen/core/PaymentRequest;

    return-object p0
.end method

.method static synthetic access$300(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/interfaces/PaymentMethodCallback;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentMethodCallback:Lcom/adyen/core/interfaces/PaymentMethodCallback;

    return-object p0
.end method

.method static synthetic access$400(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/interfaces/PaymentDetailsCallback;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentDetailsCallback:Lcom/adyen/core/interfaces/PaymentDetailsCallback;

    return-object p0
.end method


# virtual methods
.method getAndroidPayInfoListener()Landroid/content/BroadcastReceiver;
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->androidPayInfoListener:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method getPaymentDataCallback()Lcom/adyen/core/interfaces/PaymentDataCallback;
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentDataCallback:Lcom/adyen/core/interfaces/PaymentDataCallback;

    return-object v0
.end method

.method getPaymentDetailsCallback()Lcom/adyen/core/interfaces/PaymentDetailsCallback;
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentDetailsCallback:Lcom/adyen/core/interfaces/PaymentDetailsCallback;

    return-object v0
.end method

.method getPaymentDetailsReceiver()Landroid/content/BroadcastReceiver;
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentDetailsReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method getPaymentMethodCallback()Lcom/adyen/core/interfaces/PaymentMethodCallback;
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentMethodCallback:Lcom/adyen/core/interfaces/PaymentMethodCallback;

    return-object v0
.end method

.method getPaymentMethodSelectionReceiver()Landroid/content/BroadcastReceiver;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentMethodSelectionReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method getPaymentRequestCancellationReceiver()Landroid/content/BroadcastReceiver;
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers;->paymentRequestCancellationReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method
