.class public Lcom/adyen/core/PaymentRequest;
.super Ljava/lang/Object;
.source "PaymentRequest.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "PaymentRequest"


# instance fields
.field private context:Landroid/content/Context;

.field private paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/adyen/core/interfaces/PaymentRequestListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/adyen/core/interfaces/PaymentRequestListener;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, p1, p2, v0}, Lcom/adyen/core/PaymentRequest;-><init>(Landroid/content/Context;Lcom/adyen/core/interfaces/PaymentRequestListener;Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/adyen/core/interfaces/PaymentRequestListener;Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/adyen/core/interfaces/PaymentRequestListener;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/adyen/core/PaymentRequest;->context:Landroid/content/Context;

    .line 47
    new-instance v0, Lcom/adyen/core/PaymentStateHandler;

    invoke-direct {v0, p1, p0, p2, p3}, Lcom/adyen/core/PaymentStateHandler;-><init>(Landroid/content/Context;Lcom/adyen/core/PaymentRequest;Lcom/adyen/core/interfaces/PaymentRequestListener;Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;)V

    iput-object v0, p0, Lcom/adyen/core/PaymentRequest;->paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;

    .line 50
    iget-object p1, p0, Lcom/adyen/core/PaymentRequest;->context:Landroid/content/Context;

    invoke-static {p1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object p1

    iget-object p2, p0, Lcom/adyen/core/PaymentRequest;->paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;

    .line 51
    invoke-virtual {p2}, Lcom/adyen/core/PaymentStateHandler;->getPaymentBroadcastReceivers()Lcom/adyen/core/PaymentBroadcastReceivers;

    move-result-object p2

    invoke-virtual {p2}, Lcom/adyen/core/PaymentBroadcastReceivers;->getPaymentRequestCancellationReceiver()Landroid/content/BroadcastReceiver;

    move-result-object p2

    new-instance p3, Landroid/content/IntentFilter;

    const-string v0, "com.adyen.core.ui.PaymentRequestCancelled"

    invoke-direct {p3, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 50
    invoke-virtual {p1, p2, p3}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/adyen/core/PaymentRequest;->paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;

    invoke-virtual {v0}, Lcom/adyen/core/PaymentStateHandler;->getPaymentProcessorStateMachine()Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    move-result-object v0

    sget-object v1, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_CANCELLED:Lcom/adyen/core/internals/PaymentTrigger;

    invoke-virtual {v0, v1}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;

    return-void
.end method

.method public deletePreferredPaymentMethod(Lcom/adyen/core/models/PaymentMethod;Lcom/adyen/core/interfaces/DeletePreferredPaymentMethodListener;)V
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/adyen/core/PaymentRequest;->paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;

    invoke-virtual {v0, p1, p2}, Lcom/adyen/core/PaymentStateHandler;->deletePreferredPaymentMethod(Lcom/adyen/core/models/PaymentMethod;Lcom/adyen/core/interfaces/DeletePreferredPaymentMethodListener;)V

    return-void
.end method

.method public getAmount()Lcom/adyen/core/models/Amount;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 108
    iget-object v0, p0, Lcom/adyen/core/PaymentRequest;->paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;

    invoke-virtual {v0}, Lcom/adyen/core/PaymentStateHandler;->getAmount()Lcom/adyen/core/models/Amount;

    move-result-object v0

    return-object v0
.end method

.method public getGenerationTime()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/adyen/core/PaymentRequest;->paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;

    invoke-virtual {v0}, Lcom/adyen/core/PaymentStateHandler;->getGenerationTime()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentMethod()Lcom/adyen/core/models/PaymentMethod;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/adyen/core/PaymentRequest;->paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;

    invoke-virtual {v0}, Lcom/adyen/core/PaymentStateHandler;->getPaymentMethod()Lcom/adyen/core/models/PaymentMethod;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentRequestListener()Lcom/adyen/core/interfaces/PaymentRequestListener;
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/adyen/core/PaymentRequest;->paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;

    invoke-virtual {v0}, Lcom/adyen/core/PaymentStateHandler;->getPaymentRequestListener()Lcom/adyen/core/interfaces/PaymentRequestListener;

    move-result-object v0

    return-object v0
.end method

.method getPaymentStateHandler()Lcom/adyen/core/PaymentStateHandler;
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/adyen/core/PaymentRequest;->paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;

    return-object v0
.end method

.method public getPublicKey()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 89
    iget-object v0, p0, Lcom/adyen/core/PaymentRequest;->paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;

    invoke-virtual {v0}, Lcom/adyen/core/PaymentStateHandler;->getPublicKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShopperReference()Ljava/lang/String;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/adyen/core/PaymentRequest;->paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;

    invoke-virtual {v0}, Lcom/adyen/core/PaymentStateHandler;->getShopperReference()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public start()V
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/adyen/core/PaymentRequest;->paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;

    invoke-virtual {v0}, Lcom/adyen/core/PaymentStateHandler;->hasPaymentRequestDetailsListener()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/adyen/core/PaymentRequest;->paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;

    invoke-virtual {v0}, Lcom/adyen/core/PaymentStateHandler;->getPaymentProcessorStateMachine()Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    move-result-object v0

    sget-object v1, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_REQUESTED:Lcom/adyen/core/internals/PaymentTrigger;

    invoke-virtual {v0, v1}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;

    goto :goto_0

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/adyen/core/PaymentRequest;->paymentStateHandler:Lcom/adyen/core/PaymentStateHandler;

    invoke-virtual {v0}, Lcom/adyen/core/PaymentStateHandler;->getPaymentProcessorStateMachine()Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    move-result-object v0

    sget-object v1, Lcom/adyen/core/internals/PaymentTrigger;->ERROR_OCCURRED:Lcom/adyen/core/internals/PaymentTrigger;

    invoke-virtual {v0, v1}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;

    :goto_0
    return-void
.end method
