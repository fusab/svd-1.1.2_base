.class Lcom/adyen/core/PaymentBroadcastReceivers$3;
.super Ljava/lang/Object;
.source "PaymentBroadcastReceivers.java"

# interfaces
.implements Lcom/adyen/core/interfaces/PaymentDetailsCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/core/PaymentBroadcastReceivers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/core/PaymentBroadcastReceivers;


# direct methods
.method constructor <init>(Lcom/adyen/core/PaymentBroadcastReceivers;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/adyen/core/PaymentBroadcastReceivers$3;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public completionWithPaymentDetails(Lcom/adyen/core/models/paymentdetails/PaymentDetails;)V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers$3;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-static {v0}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$000(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/PaymentStateHandler;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/adyen/core/PaymentStateHandler;->setPaymentDetails(Lcom/adyen/core/models/paymentdetails/PaymentDetails;)V

    .line 73
    iget-object p1, p0, Lcom/adyen/core/PaymentBroadcastReceivers$3;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-static {p1}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$000(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/PaymentStateHandler;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adyen/core/PaymentStateHandler;->getPaymentProcessorStateMachine()Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    move-result-object p1

    sget-object v0, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_DETAILS_PROVIDED:Lcom/adyen/core/internals/PaymentTrigger;

    invoke-virtual {p1, v0}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;

    return-void
.end method

.method public completionWithPaymentDetails(Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/adyen/core/PaymentBroadcastReceivers$3;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-static {v0}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$000(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/PaymentStateHandler;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/adyen/core/PaymentStateHandler;->setPaymentDetails(Ljava/util/Map;)V

    .line 79
    iget-object p1, p0, Lcom/adyen/core/PaymentBroadcastReceivers$3;->this$0:Lcom/adyen/core/PaymentBroadcastReceivers;

    invoke-static {p1}, Lcom/adyen/core/PaymentBroadcastReceivers;->access$000(Lcom/adyen/core/PaymentBroadcastReceivers;)Lcom/adyen/core/PaymentStateHandler;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adyen/core/PaymentStateHandler;->getPaymentProcessorStateMachine()Lcom/adyen/core/internals/PaymentProcessorStateMachine;

    move-result-object p1

    sget-object v0, Lcom/adyen/core/internals/PaymentTrigger;->PAYMENT_DETAILS_PROVIDED:Lcom/adyen/core/internals/PaymentTrigger;

    invoke-virtual {p1, v0}, Lcom/adyen/core/internals/PaymentProcessorStateMachine;->onTrigger(Lcom/adyen/core/internals/PaymentTrigger;)Lcom/adyen/core/interfaces/State;

    return-void
.end method
