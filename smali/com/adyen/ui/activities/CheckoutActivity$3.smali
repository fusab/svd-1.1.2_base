.class Lcom/adyen/ui/activities/CheckoutActivity$3;
.super Ljava/lang/Object;
.source "CheckoutActivity.java"

# interfaces
.implements Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/activities/CheckoutActivity;->initializeFragment(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/activities/CheckoutActivity;


# direct methods
.method constructor <init>(Lcom/adyen/ui/activities/CheckoutActivity;)V
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/adyen/ui/activities/CheckoutActivity$3;->this$0:Lcom/adyen/ui/activities/CheckoutActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreditCardInfoProvided(Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails;)V
    .locals 2

    .line 204
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.adyen.core.ui.PaymentDetailsProvided"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "PaymentDetails"

    .line 205
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 206
    iget-object p1, p0, Lcom/adyen/ui/activities/CheckoutActivity$3;->this$0:Lcom/adyen/ui/activities/CheckoutActivity;

    invoke-static {p1}, Lcom/adyen/ui/activities/CheckoutActivity;->access$000(Lcom/adyen/ui/activities/CheckoutActivity;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 207
    iget-object p1, p0, Lcom/adyen/ui/activities/CheckoutActivity$3;->this$0:Lcom/adyen/ui/activities/CheckoutActivity;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/adyen/ui/activities/CheckoutActivity;->access$102(Lcom/adyen/ui/activities/CheckoutActivity;Z)Z

    return-void
.end method
