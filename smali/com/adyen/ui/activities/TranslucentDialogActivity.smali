.class public Lcom/adyen/ui/activities/TranslucentDialogActivity;
.super Landroid/app/Activity;
.source "TranslucentDialogActivity.java"


# instance fields
.field private cvcDialog:Lcom/adyen/ui/views/CVCDialog;

.field private uiFinalizationReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 82
    new-instance v0, Lcom/adyen/ui/activities/TranslucentDialogActivity$2;

    invoke-direct {v0, p0}, Lcom/adyen/ui/activities/TranslucentDialogActivity$2;-><init>(Lcom/adyen/ui/activities/TranslucentDialogActivity;)V

    iput-object v0, p0, Lcom/adyen/ui/activities/TranslucentDialogActivity;->uiFinalizationReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private setupDialog(Landroid/content/Intent;)V
    .locals 3

    .line 47
    iget-object v0, p0, Lcom/adyen/ui/activities/TranslucentDialogActivity;->cvcDialog:Lcom/adyen/ui/views/CVCDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/adyen/ui/views/CVCDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/adyen/ui/activities/TranslucentDialogActivity;->cvcDialog:Lcom/adyen/ui/views/CVCDialog;

    invoke-virtual {v0}, Lcom/adyen/ui/views/CVCDialog;->dismiss()V

    :cond_0
    const-string v0, "amount"

    .line 50
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/adyen/core/models/Amount;

    const-string v1, "PaymentMethod"

    .line 51
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/adyen/core/models/PaymentMethod;

    .line 52
    new-instance v1, Lcom/adyen/ui/views/CVCDialog;

    new-instance v2, Lcom/adyen/ui/activities/TranslucentDialogActivity$1;

    invoke-direct {v2, p0}, Lcom/adyen/ui/activities/TranslucentDialogActivity$1;-><init>(Lcom/adyen/ui/activities/TranslucentDialogActivity;)V

    invoke-direct {v1, p0, v0, p1, v2}, Lcom/adyen/ui/views/CVCDialog;-><init>(Landroid/app/Activity;Lcom/adyen/core/models/Amount;Lcom/adyen/core/models/PaymentMethod;Lcom/adyen/ui/views/CVCDialog$CVCDialogListener;)V

    iput-object v1, p0, Lcom/adyen/ui/activities/TranslucentDialogActivity;->cvcDialog:Lcom/adyen/ui/views/CVCDialog;

    .line 58
    iget-object p1, p0, Lcom/adyen/ui/activities/TranslucentDialogActivity;->cvcDialog:Lcom/adyen/ui/views/CVCDialog;

    invoke-virtual {p1}, Lcom/adyen/ui/views/CVCDialog;->show()V

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 1

    .line 76
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 77
    iget-object v0, p0, Lcom/adyen/ui/activities/TranslucentDialogActivity;->cvcDialog:Lcom/adyen/ui/views/CVCDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/adyen/ui/views/CVCDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/adyen/ui/activities/TranslucentDialogActivity;->cvcDialog:Lcom/adyen/ui/views/CVCDialog;

    invoke-virtual {v0}, Lcom/adyen/ui/views/CVCDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .line 63
    invoke-virtual {p0}, Lcom/adyen/ui/activities/TranslucentDialogActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 30
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    invoke-virtual {p0}, Lcom/adyen/ui/activities/TranslucentDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/adyen/ui/activities/TranslucentDialogActivity;->setupDialog(Landroid/content/Intent;)V

    .line 32
    invoke-virtual {p0}, Lcom/adyen/ui/activities/TranslucentDialogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object p1

    iget-object v0, p0, Lcom/adyen/ui/activities/TranslucentDialogActivity;->uiFinalizationReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.adyen.core.ui.finish"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .line 68
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 69
    iget-object v0, p0, Lcom/adyen/ui/activities/TranslucentDialogActivity;->cvcDialog:Lcom/adyen/ui/views/CVCDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/adyen/ui/views/CVCDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/adyen/ui/activities/TranslucentDialogActivity;->cvcDialog:Lcom/adyen/ui/views/CVCDialog;

    invoke-virtual {v0}, Lcom/adyen/ui/views/CVCDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .line 43
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    return-void
.end method

.method protected onResume()V
    .locals 0

    .line 38
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method
