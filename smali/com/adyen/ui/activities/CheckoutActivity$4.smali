.class Lcom/adyen/ui/activities/CheckoutActivity$4;
.super Ljava/lang/Object;
.source "CheckoutActivity.java"

# interfaces
.implements Lcom/adyen/ui/fragments/IssuerSelectionFragment$IssuerSelectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/activities/CheckoutActivity;->initializeFragment(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/activities/CheckoutActivity;

.field final synthetic val$paymentMethod:Lcom/adyen/core/models/PaymentMethod;


# direct methods
.method constructor <init>(Lcom/adyen/ui/activities/CheckoutActivity;Lcom/adyen/core/models/PaymentMethod;)V
    .locals 0

    .line 218
    iput-object p1, p0, Lcom/adyen/ui/activities/CheckoutActivity$4;->this$0:Lcom/adyen/ui/activities/CheckoutActivity;

    iput-object p2, p0, Lcom/adyen/ui/activities/CheckoutActivity$4;->val$paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onIssuerSelected(Ljava/lang/String;)V
    .locals 2

    .line 221
    new-instance v0, Lcom/adyen/core/models/paymentdetails/IssuerSelectionPaymentDetails;

    iget-object v1, p0, Lcom/adyen/ui/activities/CheckoutActivity$4;->val$paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {v1}, Lcom/adyen/core/models/PaymentMethod;->getInputDetails()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/adyen/core/models/paymentdetails/IssuerSelectionPaymentDetails;-><init>(Ljava/util/Collection;)V

    .line 222
    invoke-virtual {v0, p1}, Lcom/adyen/core/models/paymentdetails/IssuerSelectionPaymentDetails;->fillIssuer(Ljava/lang/String;)Z

    .line 223
    new-instance p1, Landroid/content/Intent;

    const-string v1, "com.adyen.core.ui.PaymentDetailsProvided"

    invoke-direct {p1, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "PaymentDetails"

    .line 224
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 225
    iget-object v0, p0, Lcom/adyen/ui/activities/CheckoutActivity$4;->this$0:Lcom/adyen/ui/activities/CheckoutActivity;

    invoke-static {v0}, Lcom/adyen/ui/activities/CheckoutActivity;->access$000(Lcom/adyen/ui/activities/CheckoutActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    return-void
.end method
