.class public Lcom/adyen/ui/activities/CheckoutActivity;
.super Landroidx/fragment/app/FragmentActivity;
.source "CheckoutActivity.java"


# static fields
.field public static final AMOUNT:Ljava/lang/String; = "amount"

.field public static final CARD_HOLDER_NAME_REQUIRED:Ljava/lang/String; = "card_holder_name_required"

.field public static final CREDIT_CARD_FRAGMENT:I = 0x1

.field public static final FRAGMENT:Ljava/lang/String; = "fragment"

.field public static final GIROPAY_FRAGMENT:I = 0x4

.field public static final ISSUER_SELECTION_FRAGMENT:I = 0x2

.field public static final LOADING_SCREEN_FRAGMENT:I = 0xb

.field public static final ONE_CLICK:Ljava/lang/String; = "oneClick"

.field public static final PAYMENT_METHOD:Ljava/lang/String; = "PaymentMethod"

.field public static final PAYMENT_METHODS:Ljava/lang/String; = "filteredPaymentMethods"

.field public static final PAYMENT_METHOD_SELECTION_FRAGMENT:I = 0x0

.field public static final PREFERED_PAYMENT_METHODS:Ljava/lang/String; = "preferredPaymentMethods"

.field public static final QIWI_WALLET_FRAGMENT:I = 0x5

.field public static final SEPA_DIRECT_DEBIT_FRAGMENT:I = 0x3

.field private static final TAG:Ljava/lang/String; = "CheckoutActivity"

.field private static final TAG_CREDIT_CARD_FRAGMENT:Ljava/lang/String; = "CREDIT_CARD_FRAGMENT"


# instance fields
.field private backButtonDisabled:Z

.field private context:Landroid/content/Context;

.field currentFragment:I

.field private uiFinalizationIntent:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 52
    invoke-direct {p0}, Landroidx/fragment/app/FragmentActivity;-><init>()V

    const/4 v0, 0x0

    .line 75
    iput-boolean v0, p0, Lcom/adyen/ui/activities/CheckoutActivity;->backButtonDisabled:Z

    .line 146
    new-instance v0, Lcom/adyen/ui/activities/CheckoutActivity$1;

    invoke-direct {v0, p0}, Lcom/adyen/ui/activities/CheckoutActivity$1;-><init>(Lcom/adyen/ui/activities/CheckoutActivity;)V

    iput-object v0, p0, Lcom/adyen/ui/activities/CheckoutActivity;->uiFinalizationIntent:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/adyen/ui/activities/CheckoutActivity;)Landroid/content/Context;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/adyen/ui/activities/CheckoutActivity;->context:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$102(Lcom/adyen/ui/activities/CheckoutActivity;Z)Z
    .locals 0

    .line 52
    iput-boolean p1, p0, Lcom/adyen/ui/activities/CheckoutActivity;->backButtonDisabled:Z

    return p1
.end method

.method private hideKeyboard()V
    .locals 2

    .line 331
    invoke-virtual {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 334
    :cond_0
    invoke-virtual {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    return-void
.end method

.method private initializeFragment(Landroid/content/Intent;)V
    .locals 5

    .line 155
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    .line 157
    invoke-virtual {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->finish()V

    return-void

    :cond_0
    const/4 v1, -0x1

    const-string v2, "fragment"

    .line 160
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 161
    iput v1, p0, Lcom/adyen/ui/activities/CheckoutActivity;->currentFragment:I

    if-eqz v1, :cond_7

    const/4 v2, 0x1

    const-string v3, "amount"

    const-string v4, "PaymentMethod"

    if-eq v1, v2, :cond_6

    const/4 v2, 0x2

    if-eq v1, v2, :cond_5

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    const/4 v2, 0x4

    if-eq v1, v2, :cond_3

    const/4 v2, 0x5

    if-eq v1, v2, :cond_2

    const/16 p1, 0xb

    if-ne v1, p1, :cond_1

    .line 287
    new-instance p1, Lcom/adyen/ui/fragments/LoadingScreenFragment;

    invoke-direct {p1}, Lcom/adyen/ui/fragments/LoadingScreenFragment;-><init>()V

    .line 288
    invoke-direct {p0, p1}, Lcom/adyen/ui/activities/CheckoutActivity;->replaceFragment(Landroidx/fragment/app/Fragment;)V

    goto/16 :goto_0

    .line 292
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unknown fragment selected"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 255
    :cond_2
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/adyen/core/models/PaymentMethod;

    .line 256
    invoke-virtual {v0}, Lcom/adyen/core/models/PaymentMethod;->getInputDetails()Ljava/util/Collection;

    .line 257
    new-instance v1, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;

    invoke-direct {v1}, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;-><init>()V

    .line 258
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/adyen/core/models/Amount;

    invoke-virtual {v1, p1}, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;->setAmount(Lcom/adyen/core/models/Amount;)Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;

    move-result-object p1

    .line 259
    invoke-virtual {p1, v0}, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;->setPaymentMethod(Lcom/adyen/core/models/PaymentMethod;)Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;

    move-result-object p1

    new-instance v1, Lcom/adyen/ui/activities/CheckoutActivity$6;

    invoke-direct {v1, p0, v0}, Lcom/adyen/ui/activities/CheckoutActivity$6;-><init>(Lcom/adyen/ui/activities/CheckoutActivity;Lcom/adyen/core/models/PaymentMethod;)V

    .line 260
    invoke-virtual {p1, v1}, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;->setQiwiWalletPaymentDetailsListener(Lcom/adyen/ui/fragments/QiwiWalletFragment$QiwiWalletPaymentDetailsListener;)Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;

    move-result-object p1

    .line 271
    invoke-virtual {p1}, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;->build()Lcom/adyen/ui/fragments/QiwiWalletFragment;

    move-result-object p1

    .line 272
    invoke-direct {p0, p1}, Lcom/adyen/ui/activities/CheckoutActivity;->replaceFragment(Landroidx/fragment/app/Fragment;)V

    goto/16 :goto_0

    .line 276
    :cond_3
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/adyen/core/models/PaymentMethod;

    .line 277
    new-instance v1, Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-direct {v1}, Lcom/adyen/ui/fragments/GiropayFragment;-><init>()V

    .line 279
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 280
    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 281
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 282
    invoke-virtual {v1, v2}, Lcom/adyen/ui/fragments/GiropayFragment;->setArguments(Landroid/os/Bundle;)V

    .line 283
    invoke-direct {p0, v1}, Lcom/adyen/ui/activities/CheckoutActivity;->replaceFragment(Landroidx/fragment/app/Fragment;)V

    goto/16 :goto_0

    .line 233
    :cond_4
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/adyen/core/models/PaymentMethod;

    .line 235
    new-instance v1, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;

    invoke-direct {v1}, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;-><init>()V

    .line 236
    invoke-virtual {v1, v0}, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;->setPaymentMethod(Lcom/adyen/core/models/PaymentMethod;)Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;

    move-result-object v1

    .line 237
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/adyen/core/models/Amount;

    invoke-virtual {v1, p1}, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;->setAmount(Lcom/adyen/core/models/Amount;)Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;

    move-result-object p1

    new-instance v1, Lcom/adyen/ui/activities/CheckoutActivity$5;

    invoke-direct {v1, p0, v0}, Lcom/adyen/ui/activities/CheckoutActivity$5;-><init>(Lcom/adyen/ui/activities/CheckoutActivity;Lcom/adyen/core/models/PaymentMethod;)V

    .line 238
    invoke-virtual {p1, v1}, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;->setSEPADirectDebitPaymentDetailsListener(Lcom/adyen/ui/fragments/SepaDirectDebitFragment$SEPADirectDebitPaymentDetailsListener;)Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;

    move-result-object p1

    .line 250
    invoke-virtual {p1}, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;->build()Lcom/adyen/ui/fragments/SepaDirectDebitFragment;

    move-result-object p1

    .line 251
    invoke-direct {p0, p1}, Lcom/adyen/ui/activities/CheckoutActivity;->replaceFragment(Landroidx/fragment/app/Fragment;)V

    goto/16 :goto_0

    .line 215
    :cond_5
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/adyen/core/models/PaymentMethod;

    .line 216
    new-instance v0, Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;

    invoke-direct {v0}, Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;-><init>()V

    .line 217
    invoke-virtual {v0, p1}, Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;->setPaymentMethod(Lcom/adyen/core/models/PaymentMethod;)Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;

    move-result-object v0

    new-instance v1, Lcom/adyen/ui/activities/CheckoutActivity$4;

    invoke-direct {v1, p0, p1}, Lcom/adyen/ui/activities/CheckoutActivity$4;-><init>(Lcom/adyen/ui/activities/CheckoutActivity;Lcom/adyen/core/models/PaymentMethod;)V

    .line 218
    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;->setIssuerSelectionListener(Lcom/adyen/ui/fragments/IssuerSelectionFragment$IssuerSelectionListener;)Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;

    move-result-object p1

    .line 228
    invoke-virtual {p1}, Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;->build()Lcom/adyen/ui/fragments/IssuerSelectionFragment;

    move-result-object p1

    .line 229
    invoke-direct {p0, p1}, Lcom/adyen/ui/activities/CheckoutActivity;->replaceFragment(Landroidx/fragment/app/Fragment;)V

    goto/16 :goto_0

    .line 191
    :cond_6
    new-instance v0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;

    invoke-direct {v0}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;-><init>()V

    .line 192
    invoke-virtual {p1, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->setPaymentMethod(Lcom/adyen/core/models/PaymentMethod;)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;

    move-result-object v0

    const-string v1, "public_key"

    .line 193
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->setPublicKey(Ljava/lang/String;)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;

    move-result-object v0

    const-string v1, "generation_time"

    .line 194
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->setGenerationtime(Ljava/lang/String;)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;

    move-result-object v0

    .line 195
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/adyen/core/models/Amount;

    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->setAmount(Lcom/adyen/core/models/Amount;)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;

    move-result-object v0

    const-string v1, "shopper_reference"

    .line 196
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->setShopperReference(Ljava/lang/String;)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;

    move-result-object v0

    const-string v1, "cvc_field_status"

    .line 198
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->valueOf(Ljava/lang/String;)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    move-result-object v1

    .line 197
    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->setCVCFieldStatus(Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "payment_card_scan_enabled"

    .line 200
    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->setPaymentCardScanEnabled(Z)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;

    move-result-object p1

    new-instance v0, Lcom/adyen/ui/activities/CheckoutActivity$3;

    invoke-direct {v0, p0}, Lcom/adyen/ui/activities/CheckoutActivity$3;-><init>(Lcom/adyen/ui/activities/CheckoutActivity;)V

    .line 201
    invoke-virtual {p1, v0}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->setCreditCardInfoListener(Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;

    move-result-object p1

    .line 210
    invoke-virtual {p1}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->build()Lcom/adyen/ui/fragments/CreditCardFragment;

    move-result-object p1

    const-string v0, "CREDIT_CARD_FRAGMENT"

    .line 211
    invoke-direct {p0, p1, v0}, Lcom/adyen/ui/activities/CheckoutActivity;->replaceFragment(Landroidx/fragment/app/Fragment;Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    const-string p1, "preferredPaymentMethods"

    .line 165
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Ljava/util/ArrayList;

    const-string v1, "filteredPaymentMethods"

    .line 166
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 168
    new-instance v1, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;

    invoke-direct {v1}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;-><init>()V

    .line 170
    invoke-virtual {v1, v0}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->setPaymentMethods(Ljava/util/List;)Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;

    move-result-object v0

    .line 171
    invoke-virtual {v0, p1}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->setPreferredPaymentMethods(Ljava/util/List;)Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;

    move-result-object p1

    new-instance v0, Lcom/adyen/ui/activities/CheckoutActivity$2;

    invoke-direct {v0, p0}, Lcom/adyen/ui/activities/CheckoutActivity$2;-><init>(Lcom/adyen/ui/activities/CheckoutActivity;)V

    .line 172
    invoke-virtual {p1, v0}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->setPaymentMethodSelectionListener(Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$PaymentMethodSelectionListener;)Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;

    move-result-object p1

    .line 185
    invoke-virtual {p1}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->build()Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;

    move-result-object p1

    .line 186
    invoke-direct {p0, p1}, Lcom/adyen/ui/activities/CheckoutActivity;->replaceFragment(Landroidx/fragment/app/Fragment;)V

    .line 187
    invoke-direct {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->hideKeyboard()V

    :goto_0
    return-void
.end method

.method private replaceFragment(Landroidx/fragment/app/Fragment;)V
    .locals 1

    const/4 v0, 0x0

    .line 339
    invoke-direct {p0, p1, v0}, Lcom/adyen/ui/activities/CheckoutActivity;->replaceFragment(Landroidx/fragment/app/Fragment;Ljava/lang/String;)V

    return-void
.end method

.method private replaceFragment(Landroidx/fragment/app/Fragment;Ljava/lang/String;)V
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 343
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 344
    invoke-virtual {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    .line 345
    invoke-virtual {v1, v0, v2}, Landroidx/fragment/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 349
    sget-object v2, Lcom/adyen/ui/activities/CheckoutActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Starting fragment: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x1020002

    .line 351
    invoke-virtual {v1, v2, p1, p2}, Landroidx/fragment/app/FragmentTransaction;->replace(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    .line 352
    invoke-virtual {v1, v0}, Landroidx/fragment/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    .line 353
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 355
    :cond_0
    sget-object p1, Lcom/adyen/ui/activities/CheckoutActivity;->TAG:Ljava/lang/String;

    const-string p2, "Fragment popped back"

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method private setupActionBar()V
    .locals 2

    .line 298
    invoke-virtual {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 300
    sget v1, Lcom/adyen/ui/R$layout;->action_bar:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    const/16 v1, 0x10

    .line 301
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 302
    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/adyen/ui/R$id;->action_bar_back_icon:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/adyen/ui/activities/CheckoutActivity$7;

    invoke-direct {v1, p0}, Lcom/adyen/ui/activities/CheckoutActivity$7;-><init>(Lcom/adyen/ui/activities/CheckoutActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public hideActionBar()V
    .locals 1

    .line 325
    invoke-virtual {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 326
    invoke-virtual {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .line 83
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 85
    invoke-virtual {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "CREDIT_CARD_FRAGMENT"

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    .line 87
    instance-of v1, v0, Lcom/adyen/ui/fragments/CreditCardFragment;

    if-eqz v1, :cond_0

    .line 88
    invoke-virtual {v0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .line 124
    iget-boolean v0, p0, Lcom/adyen/ui/activities/CheckoutActivity;->backButtonDisabled:Z

    if-eqz v0, :cond_0

    .line 125
    sget-object v0, Lcom/adyen/ui/activities/CheckoutActivity;->TAG:Ljava/lang/String;

    const-string v1, "Going back at this step is not possible."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    .line 128
    :cond_0
    invoke-virtual {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 130
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onBackPressed()V

    goto :goto_1

    .line 132
    :cond_1
    invoke-virtual {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Landroidx/fragment/app/FragmentManager;->getBackStackEntryAt(I)Landroidx/fragment/app/FragmentManager$BackStackEntry;

    move-result-object v1

    .line 133
    invoke-interface {v1}, Landroidx/fragment/app/FragmentManager$BackStackEntry;->getName()Ljava/lang/String;

    move-result-object v1

    .line 134
    const-class v2, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-class v2, Lcom/adyen/ui/fragments/LoadingScreenFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 139
    invoke-virtual {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->finish()V

    goto :goto_1

    .line 141
    :cond_3
    invoke-virtual {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->popBackStackImmediate()Z

    goto :goto_1

    .line 135
    :cond_4
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.adyen.core.ui.PaymentRequestCancelled"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 136
    invoke-static {p0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 137
    invoke-virtual {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->finish()V

    :goto_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 95
    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 96
    sget-object p1, Lcom/adyen/ui/activities/CheckoutActivity;->TAG:Ljava/lang/String;

    const-string v0, "onCreate()"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iput-object p0, p0, Lcom/adyen/ui/activities/CheckoutActivity;->context:Landroid/content/Context;

    const/4 p1, 0x0

    .line 98
    iput-boolean p1, p0, Lcom/adyen/ui/activities/CheckoutActivity;->backButtonDisabled:Z

    .line 99
    invoke-direct {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->setupActionBar()V

    .line 100
    invoke-virtual {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    .line 102
    invoke-direct {p0, p1}, Lcom/adyen/ui/activities/CheckoutActivity;->initializeFragment(Landroid/content/Intent;)V

    .line 103
    invoke-virtual {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object p1

    iget-object v0, p0, Lcom/adyen/ui/activities/CheckoutActivity;->uiFinalizationIntent:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.adyen.core.ui.finish"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3

    .line 109
    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    const/4 v0, 0x0

    .line 110
    iput-boolean v0, p0, Lcom/adyen/ui/activities/CheckoutActivity;->backButtonDisabled:Z

    .line 111
    sget-object v1, Lcom/adyen/ui/activities/CheckoutActivity;->TAG:Ljava/lang/String;

    const-string v2, "onNewIntent()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "REDIRECT_RETURN"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    iget p1, p0, Lcom/adyen/ui/activities/CheckoutActivity;->currentFragment:I

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 114
    invoke-virtual {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    .line 115
    const-class v1, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Landroidx/fragment/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    goto :goto_0

    .line 118
    :cond_0
    invoke-direct {p0, p1}, Lcom/adyen/ui/activities/CheckoutActivity;->initializeFragment(Landroid/content/Intent;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public setActionBarTitle(I)V
    .locals 0

    .line 313
    invoke-virtual {p0, p1}, Lcom/adyen/ui/activities/CheckoutActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/adyen/ui/activities/CheckoutActivity;->setActionBarTitle(Ljava/lang/String;)V

    return-void
.end method

.method public setActionBarTitle(Ljava/lang/String;)V
    .locals 3

    .line 317
    invoke-virtual {p0}, Lcom/adyen/ui/activities/CheckoutActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 318
    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 319
    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/adyen/ui/R$id;->action_bar_title:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 320
    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    :cond_0
    return-void
.end method
