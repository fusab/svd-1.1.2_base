.class Lcom/adyen/ui/activities/CheckoutActivity$6;
.super Ljava/lang/Object;
.source "CheckoutActivity.java"

# interfaces
.implements Lcom/adyen/ui/fragments/QiwiWalletFragment$QiwiWalletPaymentDetailsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/activities/CheckoutActivity;->initializeFragment(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/activities/CheckoutActivity;

.field final synthetic val$paymentMethod:Lcom/adyen/core/models/PaymentMethod;


# direct methods
.method constructor <init>(Lcom/adyen/ui/activities/CheckoutActivity;Lcom/adyen/core/models/PaymentMethod;)V
    .locals 0

    .line 260
    iput-object p1, p0, Lcom/adyen/ui/activities/CheckoutActivity$6;->this$0:Lcom/adyen/ui/activities/CheckoutActivity;

    iput-object p2, p0, Lcom/adyen/ui/activities/CheckoutActivity$6;->val$paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPaymentDetails(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 263
    new-instance v0, Lcom/adyen/core/models/paymentdetails/QiwiWalletPaymentDetails;

    iget-object v1, p0, Lcom/adyen/ui/activities/CheckoutActivity$6;->val$paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {v1}, Lcom/adyen/core/models/PaymentMethod;->getInputDetails()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/adyen/core/models/paymentdetails/QiwiWalletPaymentDetails;-><init>(Ljava/util/Collection;)V

    .line 264
    invoke-virtual {v0, p1, p2}, Lcom/adyen/core/models/paymentdetails/QiwiWalletPaymentDetails;->fillTelephoneNumber(Ljava/lang/String;Ljava/lang/String;)Z

    .line 266
    new-instance p1, Landroid/content/Intent;

    const-string p2, "com.adyen.core.ui.PaymentDetailsProvided"

    invoke-direct {p1, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string p2, "PaymentDetails"

    .line 267
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 268
    iget-object p2, p0, Lcom/adyen/ui/activities/CheckoutActivity$6;->this$0:Lcom/adyen/ui/activities/CheckoutActivity;

    invoke-static {p2}, Lcom/adyen/ui/activities/CheckoutActivity;->access$000(Lcom/adyen/ui/activities/CheckoutActivity;)Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    return-void
.end method
