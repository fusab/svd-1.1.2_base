.class Lcom/adyen/ui/activities/CheckoutActivity$2;
.super Ljava/lang/Object;
.source "CheckoutActivity.java"

# interfaces
.implements Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$PaymentMethodSelectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/activities/CheckoutActivity;->initializeFragment(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/activities/CheckoutActivity;


# direct methods
.method constructor <init>(Lcom/adyen/ui/activities/CheckoutActivity;)V
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/adyen/ui/activities/CheckoutActivity$2;->this$0:Lcom/adyen/ui/activities/CheckoutActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPaymentMethodSelected(Lcom/adyen/core/models/PaymentMethod;)V
    .locals 3

    .line 175
    invoke-virtual {p1}, Lcom/adyen/core/models/PaymentMethod;->isRedirectMethod()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    invoke-virtual {p1}, Lcom/adyen/core/models/PaymentMethod;->isOneClick()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/adyen/core/models/PaymentMethod;->requiresInput()Z

    move-result v0

    if-nez v0, :cond_1

    .line 177
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/adyen/ui/activities/CheckoutActivity$2;->this$0:Lcom/adyen/ui/activities/CheckoutActivity;

    invoke-static {v1}, Lcom/adyen/ui/activities/CheckoutActivity;->access$000(Lcom/adyen/ui/activities/CheckoutActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/adyen/ui/activities/TranslucentLoadingScreenActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 178
    iget-object v1, p0, Lcom/adyen/ui/activities/CheckoutActivity$2;->this$0:Lcom/adyen/ui/activities/CheckoutActivity;

    invoke-static {v1}, Lcom/adyen/ui/activities/CheckoutActivity;->access$000(Lcom/adyen/ui/activities/CheckoutActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 180
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.adyen.core.ui.PaymentMethodSelected"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "PaymentMethod"

    .line 181
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 182
    iget-object p1, p0, Lcom/adyen/ui/activities/CheckoutActivity$2;->this$0:Lcom/adyen/ui/activities/CheckoutActivity;

    invoke-static {p1}, Lcom/adyen/ui/activities/CheckoutActivity;->access$000(Lcom/adyen/ui/activities/CheckoutActivity;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    return-void
.end method
