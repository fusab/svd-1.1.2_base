.class public Lcom/adyen/ui/activities/TranslucentLoadingScreenActivity;
.super Landroidx/fragment/app/FragmentActivity;
.source "TranslucentLoadingScreenActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TranslucentLoadingScreenActivity"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Landroidx/fragment/app/FragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 20
    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    new-instance p1, Lcom/adyen/ui/fragments/LoadingScreenFragment;

    invoke-direct {p1}, Lcom/adyen/ui/fragments/LoadingScreenFragment;-><init>()V

    .line 23
    invoke-virtual {p0}, Lcom/adyen/ui/activities/TranslucentLoadingScreenActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x1020002

    .line 24
    invoke-virtual {v0, v1, p1}, Landroidx/fragment/app/FragmentTransaction;->replace(ILandroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    .line 25
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    return-void
.end method
