.class public Lcom/adyen/ui/views/ExpiryDateEditText;
.super Lcom/adyen/ui/views/CheckoutEditText;
.source "ExpiryDateEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;
    }
.end annotation


# static fields
.field private static final EDIT_TEXT_MAX_LENGTH:I = 0x5

.field private static final MAX_EXPIRED_MONTHS:I = 0x3

.field private static final TWENTY:Ljava/lang/String; = "20"


# instance fields
.field private validator:Lcom/adyen/ui/utils/AdyenInputValidator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1}, Lcom/adyen/ui/views/CheckoutEditText;-><init>(Landroid/content/Context;)V

    .line 51
    invoke-direct {p0}, Lcom/adyen/ui/views/ExpiryDateEditText;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1, p2}, Lcom/adyen/ui/views/CheckoutEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    invoke-direct {p0}, Lcom/adyen/ui/views/ExpiryDateEditText;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/adyen/ui/views/CheckoutEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    invoke-direct {p0}, Lcom/adyen/ui/views/ExpiryDateEditText;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .line 66
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/adyen/ui/views/CheckoutEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 67
    invoke-direct {p0}, Lcom/adyen/ui/views/ExpiryDateEditText;->init()V

    return-void
.end method

.method static synthetic access$000(Lcom/adyen/ui/views/ExpiryDateEditText;Ljava/lang/String;)Z
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/adyen/ui/views/ExpiryDateEditText;->isInputDateValid(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$100(Lcom/adyen/ui/views/ExpiryDateEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/adyen/ui/views/ExpiryDateEditText;->validator:Lcom/adyen/ui/utils/AdyenInputValidator;

    return-object p0
.end method

.method private init()V
    .locals 3

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 30
    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/InputFilter;

    invoke-virtual {p0, v0}, Lcom/adyen/ui/views/ExpiryDateEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 32
    new-instance v0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;

    invoke-direct {v0, p0}, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;-><init>(Lcom/adyen/ui/views/ExpiryDateEditText;)V

    invoke-virtual {p0, v0}, Lcom/adyen/ui/views/ExpiryDateEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 34
    new-instance v0, Lcom/adyen/ui/views/ExpiryDateEditText$1;

    invoke-direct {v0, p0}, Lcom/adyen/ui/views/ExpiryDateEditText$1;-><init>(Lcom/adyen/ui/views/ExpiryDateEditText;)V

    invoke-virtual {p0, v0}, Lcom/adyen/ui/views/ExpiryDateEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method private isInputDateValid(Ljava/lang/String;)Z
    .locals 7

    const-string v0, "(0?[1-9]|1[0-2])/[\\d]{2}"

    .line 160
    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 161
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v2, 0x1

    .line 162
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/lit16 v3, v3, -0x7d0

    const/4 v4, 0x2

    .line 163
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/2addr v0, v2

    .line 165
    invoke-virtual {p1, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x5

    const/4 v6, 0x3

    .line 166
    invoke-virtual {p1, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    mul-int/lit8 p1, p1, 0xc

    add-int/2addr p1, v4

    mul-int/lit8 v3, v3, 0xc

    add-int/2addr v3, v0

    sub-int/2addr v3, v6

    if-lt p1, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method


# virtual methods
.method public getFullYear()Ljava/lang/String;
    .locals 4

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "20"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/adyen/ui/views/ExpiryDateEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x5

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMonth()Ljava/lang/String;
    .locals 3

    .line 76
    invoke-virtual {p0}, Lcom/adyen/ui/views/ExpiryDateEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setValidator(Lcom/adyen/ui/utils/AdyenInputValidator;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/adyen/ui/views/ExpiryDateEditText;->validator:Lcom/adyen/ui/utils/AdyenInputValidator;

    .line 72
    iget-object p1, p0, Lcom/adyen/ui/views/ExpiryDateEditText;->validator:Lcom/adyen/ui/utils/AdyenInputValidator;

    invoke-virtual {p1, p0}, Lcom/adyen/ui/utils/AdyenInputValidator;->addInputField(Landroid/view/View;)V

    return-void
.end method
