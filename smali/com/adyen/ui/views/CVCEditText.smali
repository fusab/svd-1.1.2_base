.class public Lcom/adyen/ui/views/CVCEditText;
.super Lcom/adyen/ui/views/CheckoutEditText;
.source "CVCEditText.java"


# static fields
.field public static final CVC_MAX_LENGTH:I = 0x3

.field public static final CVC_MAX_LENGTH_AMEX:I = 0x4


# instance fields
.field private maxLength:I

.field private optional:Z

.field private validator:Lcom/adyen/ui/utils/AdyenInputValidator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 90
    invoke-direct {p0, p1}, Lcom/adyen/ui/views/CheckoutEditText;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x3

    .line 25
    iput p1, p0, Lcom/adyen/ui/views/CVCEditText;->maxLength:I

    .line 91
    invoke-direct {p0}, Lcom/adyen/ui/views/CVCEditText;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 95
    invoke-direct {p0, p1, p2}, Lcom/adyen/ui/views/CheckoutEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x3

    .line 25
    iput p1, p0, Lcom/adyen/ui/views/CVCEditText;->maxLength:I

    .line 96
    invoke-direct {p0}, Lcom/adyen/ui/views/CVCEditText;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 100
    invoke-direct {p0, p1, p2, p3}, Lcom/adyen/ui/views/CheckoutEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x3

    .line 25
    iput p1, p0, Lcom/adyen/ui/views/CVCEditText;->maxLength:I

    .line 101
    invoke-direct {p0}, Lcom/adyen/ui/views/CVCEditText;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .line 106
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/adyen/ui/views/CheckoutEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 p1, 0x3

    .line 25
    iput p1, p0, Lcom/adyen/ui/views/CVCEditText;->maxLength:I

    .line 107
    invoke-direct {p0}, Lcom/adyen/ui/views/CVCEditText;->init()V

    return-void
.end method

.method static synthetic access$000(Lcom/adyen/ui/views/CVCEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/adyen/ui/views/CVCEditText;->validator:Lcom/adyen/ui/utils/AdyenInputValidator;

    return-object p0
.end method

.method private init()V
    .locals 3

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 33
    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    new-instance v1, Lcom/adyen/ui/views/CVCEditText$1;

    invoke-direct {v1, p0}, Lcom/adyen/ui/views/CVCEditText$1;-><init>(Lcom/adyen/ui/views/CVCEditText;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/InputFilter;

    invoke-virtual {p0, v0}, Lcom/adyen/ui/views/CVCEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 45
    new-instance v0, Lcom/adyen/ui/views/CVCEditText$2;

    invoke-direct {v0, p0}, Lcom/adyen/ui/views/CVCEditText$2;-><init>(Lcom/adyen/ui/views/CVCEditText;)V

    invoke-virtual {p0, v0}, Lcom/adyen/ui/views/CVCEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 71
    new-instance v0, Lcom/adyen/ui/views/CVCEditText$3;

    invoke-direct {v0, p0}, Lcom/adyen/ui/views/CVCEditText$3;-><init>(Lcom/adyen/ui/views/CVCEditText;)V

    invoke-virtual {p0, v0}, Lcom/adyen/ui/views/CVCEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method


# virtual methods
.method public getCVC()Ljava/lang/String;
    .locals 1

    .line 116
    invoke-virtual {p0}, Lcom/adyen/ui/views/CVCEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasValidInput()Z
    .locals 4

    .line 135
    invoke-virtual {p0}, Lcom/adyen/ui/views/CVCEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 136
    iget-boolean v1, p0, Lcom/adyen/ui/views/CVCEditText;->optional:Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/adyen/ui/views/CVCEditText;->maxLength:I

    if-eq v0, v1, :cond_2

    if-nez v0, :cond_1

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/adyen/ui/views/CVCEditText;->maxLength:I

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :cond_2
    :goto_0
    return v2
.end method

.method public setMaxLength(I)V
    .locals 4

    .line 120
    iput p1, p0, Lcom/adyen/ui/views/CVCEditText;->maxLength:I

    .line 121
    invoke-virtual {p0}, Lcom/adyen/ui/views/CVCEditText;->getFilters()[Landroid/text/InputFilter;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 122
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 123
    aget-object v2, p1, v1

    instance-of v2, v2, Landroid/text/InputFilter$LengthFilter;

    if-eqz v2, :cond_0

    .line 124
    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    iget v3, p0, Lcom/adyen/ui/views/CVCEditText;->maxLength:I

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, p1, v1

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 128
    :cond_1
    :goto_1
    invoke-virtual {p0, p1}, Lcom/adyen/ui/views/CVCEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 129
    invoke-virtual {p0}, Lcom/adyen/ui/views/CVCEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    iget v1, p0, Lcom/adyen/ui/views/CVCEditText;->maxLength:I

    if-le p1, v1, :cond_2

    .line 130
    invoke-virtual {p0}, Lcom/adyen/ui/views/CVCEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    iget v1, p0, Lcom/adyen/ui/views/CVCEditText;->maxLength:I

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/adyen/ui/views/CVCEditText;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    return-void
.end method

.method public setOptional(Z)V
    .locals 0

    .line 140
    iput-boolean p1, p0, Lcom/adyen/ui/views/CVCEditText;->optional:Z

    return-void
.end method

.method public setValidator(Lcom/adyen/ui/utils/AdyenInputValidator;)V
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/adyen/ui/views/CVCEditText;->validator:Lcom/adyen/ui/utils/AdyenInputValidator;

    .line 112
    iget-object p1, p0, Lcom/adyen/ui/views/CVCEditText;->validator:Lcom/adyen/ui/utils/AdyenInputValidator;

    invoke-virtual {p1, p0}, Lcom/adyen/ui/utils/AdyenInputValidator;->addInputField(Landroid/view/View;)V

    return-void
.end method
