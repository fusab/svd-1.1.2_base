.class Lcom/adyen/ui/views/IBANEditText$2;
.super Ljava/lang/Object;
.source "IBANEditText.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/views/IBANEditText;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/views/IBANEditText;


# direct methods
.method constructor <init>(Lcom/adyen/ui/views/IBANEditText;)V
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/adyen/ui/views/IBANEditText$2;->this$0:Lcom/adyen/ui/views/IBANEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .line 81
    iget-object v0, p0, Lcom/adyen/ui/views/IBANEditText$2;->this$0:Lcom/adyen/ui/views/IBANEditText;

    invoke-static {v0}, Lcom/adyen/ui/views/IBANEditText;->access$000(Lcom/adyen/ui/views/IBANEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 82
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    const-string v2, " "

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v3, 0x2

    if-le v0, v3, :cond_0

    .line 83
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/adyen/utils/Iban;->validate(Ljava/lang/String;)Z

    move-result p1

    .line 84
    iget-object v0, p0, Lcom/adyen/ui/views/IBANEditText$2;->this$0:Lcom/adyen/ui/views/IBANEditText;

    invoke-static {v0}, Lcom/adyen/ui/views/IBANEditText;->access$000(Lcom/adyen/ui/views/IBANEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;

    move-result-object v0

    iget-object v1, p0, Lcom/adyen/ui/views/IBANEditText$2;->this$0:Lcom/adyen/ui/views/IBANEditText;

    invoke-virtual {v0, v1, p1}, Lcom/adyen/ui/utils/AdyenInputValidator;->setReady(Landroid/view/View;Z)V

    goto :goto_0

    .line 86
    :cond_0
    iget-object p1, p0, Lcom/adyen/ui/views/IBANEditText$2;->this$0:Lcom/adyen/ui/views/IBANEditText;

    invoke-static {p1}, Lcom/adyen/ui/views/IBANEditText;->access$000(Lcom/adyen/ui/views/IBANEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;

    move-result-object p1

    iget-object v0, p0, Lcom/adyen/ui/views/IBANEditText$2;->this$0:Lcom/adyen/ui/views/IBANEditText;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/adyen/ui/utils/AdyenInputValidator;->setReady(Landroid/view/View;Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
