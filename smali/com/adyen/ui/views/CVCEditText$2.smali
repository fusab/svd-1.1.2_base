.class Lcom/adyen/ui/views/CVCEditText$2;
.super Ljava/lang/Object;
.source "CVCEditText.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/views/CVCEditText;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/views/CVCEditText;


# direct methods
.method constructor <init>(Lcom/adyen/ui/views/CVCEditText;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/adyen/ui/views/CVCEditText$2;->this$0:Lcom/adyen/ui/views/CVCEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 58
    iget-object p1, p0, Lcom/adyen/ui/views/CVCEditText$2;->this$0:Lcom/adyen/ui/views/CVCEditText;

    invoke-static {p1}, Lcom/adyen/ui/views/CVCEditText;->access$000(Lcom/adyen/ui/views/CVCEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 59
    iget-object p1, p0, Lcom/adyen/ui/views/CVCEditText$2;->this$0:Lcom/adyen/ui/views/CVCEditText;

    invoke-static {p1}, Lcom/adyen/ui/views/CVCEditText;->access$000(Lcom/adyen/ui/views/CVCEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;

    move-result-object p1

    iget-object v0, p0, Lcom/adyen/ui/views/CVCEditText$2;->this$0:Lcom/adyen/ui/views/CVCEditText;

    invoke-virtual {v0}, Lcom/adyen/ui/views/CVCEditText;->hasValidInput()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/adyen/ui/utils/AdyenInputValidator;->setReady(Landroid/view/View;Z)V

    .line 62
    :cond_0
    iget-object p1, p0, Lcom/adyen/ui/views/CVCEditText$2;->this$0:Lcom/adyen/ui/views/CVCEditText;

    invoke-virtual {p1}, Lcom/adyen/ui/views/CVCEditText;->hasValidInput()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 63
    iget-object p1, p0, Lcom/adyen/ui/views/CVCEditText$2;->this$0:Lcom/adyen/ui/views/CVCEditText;

    const/16 v0, 0x82

    invoke-virtual {p1, v0}, Lcom/adyen/ui/views/CVCEditText;->focusSearch(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 65
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    :cond_1
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
