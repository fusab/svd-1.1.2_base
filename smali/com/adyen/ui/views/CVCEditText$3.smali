.class Lcom/adyen/ui/views/CVCEditText$3;
.super Ljava/lang/Object;
.source "CVCEditText.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/views/CVCEditText;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/views/CVCEditText;


# direct methods
.method constructor <init>(Lcom/adyen/ui/views/CVCEditText;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/adyen/ui/views/CVCEditText$3;->this$0:Lcom/adyen/ui/views/CVCEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1

    if-eqz p2, :cond_0

    .line 75
    iget-object p1, p0, Lcom/adyen/ui/views/CVCEditText$3;->this$0:Lcom/adyen/ui/views/CVCEditText;

    invoke-virtual {p1}, Lcom/adyen/ui/views/CVCEditText;->getContext()Landroid/content/Context;

    move-result-object p2

    sget v0, Lcom/adyen/ui/R$color;->black_text:I

    invoke-static {p2, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/adyen/ui/views/CVCEditText;->setTextColor(I)V

    .line 76
    iget-object p1, p0, Lcom/adyen/ui/views/CVCEditText$3;->this$0:Lcom/adyen/ui/views/CVCEditText;

    invoke-static {p1}, Lcom/adyen/ui/views/CVCEditText;->access$000(Lcom/adyen/ui/views/CVCEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 77
    iget-object p1, p0, Lcom/adyen/ui/views/CVCEditText$3;->this$0:Lcom/adyen/ui/views/CVCEditText;

    invoke-static {p1}, Lcom/adyen/ui/views/CVCEditText;->access$000(Lcom/adyen/ui/views/CVCEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;

    move-result-object p1

    iget-object p2, p0, Lcom/adyen/ui/views/CVCEditText$3;->this$0:Lcom/adyen/ui/views/CVCEditText;

    invoke-virtual {p2}, Lcom/adyen/ui/views/CVCEditText;->hasValidInput()Z

    move-result v0

    invoke-virtual {p1, p2, v0}, Lcom/adyen/ui/utils/AdyenInputValidator;->setReady(Landroid/view/View;Z)V

    goto :goto_0

    .line 80
    :cond_0
    iget-object p1, p0, Lcom/adyen/ui/views/CVCEditText$3;->this$0:Lcom/adyen/ui/views/CVCEditText;

    invoke-virtual {p1}, Lcom/adyen/ui/views/CVCEditText;->hasValidInput()Z

    move-result p1

    if-nez p1, :cond_1

    .line 81
    iget-object p1, p0, Lcom/adyen/ui/views/CVCEditText$3;->this$0:Lcom/adyen/ui/views/CVCEditText;

    invoke-virtual {p1}, Lcom/adyen/ui/views/CVCEditText;->getContext()Landroid/content/Context;

    move-result-object p2

    sget v0, Lcom/adyen/ui/R$color;->red_invalid_input_highlight:I

    invoke-static {p2, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/adyen/ui/views/CVCEditText;->setTextColor(I)V

    :cond_1
    :goto_0
    return-void
.end method
