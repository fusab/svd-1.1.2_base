.class Lcom/adyen/ui/views/CVCDialog$4;
.super Ljava/lang/Object;
.source "CVCDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/views/CVCDialog;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/views/CVCDialog;

.field final synthetic val$cvcEditText:Lcom/adyen/ui/views/CVCEditText;


# direct methods
.method constructor <init>(Lcom/adyen/ui/views/CVCDialog;Lcom/adyen/ui/views/CVCEditText;)V
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/adyen/ui/views/CVCDialog$4;->this$0:Lcom/adyen/ui/views/CVCDialog;

    iput-object p2, p0, Lcom/adyen/ui/views/CVCDialog$4;->val$cvcEditText:Lcom/adyen/ui/views/CVCEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 2

    .line 116
    iget-object p1, p0, Lcom/adyen/ui/views/CVCDialog$4;->this$0:Lcom/adyen/ui/views/CVCDialog;

    invoke-virtual {p1}, Lcom/adyen/ui/views/CVCDialog;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/inputmethod/InputMethodManager;

    .line 118
    iget-object v0, p0, Lcom/adyen/ui/views/CVCDialog$4;->val$cvcEditText:Lcom/adyen/ui/views/CVCEditText;

    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 119
    iget-object p1, p0, Lcom/adyen/ui/views/CVCDialog$4;->val$cvcEditText:Lcom/adyen/ui/views/CVCEditText;

    invoke-virtual {p1}, Lcom/adyen/ui/views/CVCEditText;->requestFocus()Z

    return-void
.end method
