.class public Lcom/adyen/ui/views/CardHolderEditText;
.super Lcom/adyen/ui/views/CheckoutEditText;
.source "CardHolderEditText.java"


# instance fields
.field private validator:Lcom/adyen/ui/utils/AdyenInputValidator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/adyen/ui/views/CheckoutEditText;-><init>(Landroid/content/Context;)V

    .line 18
    invoke-direct {p0}, Lcom/adyen/ui/views/CardHolderEditText;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/adyen/ui/views/CheckoutEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    invoke-direct {p0}, Lcom/adyen/ui/views/CardHolderEditText;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/adyen/ui/views/CheckoutEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    invoke-direct {p0}, Lcom/adyen/ui/views/CardHolderEditText;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .line 33
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/adyen/ui/views/CheckoutEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 34
    invoke-direct {p0}, Lcom/adyen/ui/views/CardHolderEditText;->init()V

    return-void
.end method

.method static synthetic access$000(Lcom/adyen/ui/views/CardHolderEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/adyen/ui/views/CardHolderEditText;->validator:Lcom/adyen/ui/utils/AdyenInputValidator;

    return-object p0
.end method

.method private init()V
    .locals 1

    .line 38
    new-instance v0, Lcom/adyen/ui/views/CardHolderEditText$1;

    invoke-direct {v0, p0}, Lcom/adyen/ui/views/CardHolderEditText$1;-><init>(Lcom/adyen/ui/views/CardHolderEditText;)V

    invoke-virtual {p0, v0}, Lcom/adyen/ui/views/CardHolderEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method


# virtual methods
.method public setValidator(Lcom/adyen/ui/utils/AdyenInputValidator;)V
    .locals 0

    .line 59
    iput-object p1, p0, Lcom/adyen/ui/views/CardHolderEditText;->validator:Lcom/adyen/ui/utils/AdyenInputValidator;

    .line 60
    iget-object p1, p0, Lcom/adyen/ui/views/CardHolderEditText;->validator:Lcom/adyen/ui/utils/AdyenInputValidator;

    invoke-virtual {p1, p0}, Lcom/adyen/ui/utils/AdyenInputValidator;->addInputField(Landroid/view/View;)V

    return-void
.end method
