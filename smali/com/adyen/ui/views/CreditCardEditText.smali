.class public Lcom/adyen/ui/views/CreditCardEditText;
.super Lcom/adyen/ui/views/CheckoutEditText;
.source "CreditCardEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;,
        Lcom/adyen/ui/views/CreditCardEditText$CVCFieldStatusListener;
    }
.end annotation


# static fields
.field private static final CC_MAX_LENGTH:I = 0x17


# instance fields
.field private allowedCardTypes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;",
            ">;"
        }
    .end annotation
.end field

.field private baseURL:Ljava/lang/String;

.field private cvcEditText:Lcom/adyen/ui/views/CVCEditText;

.field private cvcFieldStatusListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/adyen/ui/views/CreditCardEditText$CVCFieldStatusListener;",
            ">;"
        }
    .end annotation
.end field

.field private numberOfDigits:[Ljava/lang/Integer;

.field private placeHolderIconUrl:Ljava/lang/String;

.field private validator:Lcom/adyen/ui/utils/AdyenInputValidator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lcom/adyen/ui/views/CheckoutEditText;-><init>(Landroid/content/Context;)V

    const-string p1, ""

    .line 40
    iput-object p1, p0, Lcom/adyen/ui/views/CreditCardEditText;->placeHolderIconUrl:Ljava/lang/String;

    .line 45
    new-instance p1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object p1, p0, Lcom/adyen/ui/views/CreditCardEditText;->cvcFieldStatusListeners:Ljava/util/List;

    .line 85
    invoke-direct {p0}, Lcom/adyen/ui/views/CreditCardEditText;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 89
    invoke-direct {p0, p1, p2}, Lcom/adyen/ui/views/CheckoutEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string p1, ""

    .line 40
    iput-object p1, p0, Lcom/adyen/ui/views/CreditCardEditText;->placeHolderIconUrl:Ljava/lang/String;

    .line 45
    new-instance p1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object p1, p0, Lcom/adyen/ui/views/CreditCardEditText;->cvcFieldStatusListeners:Ljava/util/List;

    .line 90
    invoke-direct {p0}, Lcom/adyen/ui/views/CreditCardEditText;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 94
    invoke-direct {p0, p1, p2, p3}, Lcom/adyen/ui/views/CheckoutEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string p1, ""

    .line 40
    iput-object p1, p0, Lcom/adyen/ui/views/CreditCardEditText;->placeHolderIconUrl:Ljava/lang/String;

    .line 45
    new-instance p1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object p1, p0, Lcom/adyen/ui/views/CreditCardEditText;->cvcFieldStatusListeners:Ljava/util/List;

    .line 95
    invoke-direct {p0}, Lcom/adyen/ui/views/CreditCardEditText;->init()V

    return-void
.end method

.method static synthetic access$100(Lcom/adyen/ui/views/CreditCardEditText;Ljava/lang/String;)Z
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/adyen/ui/views/CreditCardEditText;->isValidNr(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$200(Lcom/adyen/ui/views/CreditCardEditText;)Ljava/util/Map;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/adyen/ui/views/CreditCardEditText;->allowedCardTypes:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$300(Lcom/adyen/ui/views/CreditCardEditText;)Ljava/lang/String;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/adyen/ui/views/CreditCardEditText;->baseURL:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lcom/adyen/ui/views/CreditCardEditText;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/adyen/ui/views/CreditCardEditText;->setCCIcon(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$502(Lcom/adyen/ui/views/CreditCardEditText;[Ljava/lang/Integer;)[Ljava/lang/Integer;
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/adyen/ui/views/CreditCardEditText;->numberOfDigits:[Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$600(Lcom/adyen/ui/views/CreditCardEditText;)Lcom/adyen/ui/views/CVCEditText;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/adyen/ui/views/CreditCardEditText;->cvcEditText:Lcom/adyen/ui/views/CVCEditText;

    return-object p0
.end method

.method static synthetic access$700(Lcom/adyen/ui/views/CreditCardEditText;)Ljava/util/List;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/adyen/ui/views/CreditCardEditText;->cvcFieldStatusListeners:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$800(Lcom/adyen/ui/views/CreditCardEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/adyen/ui/views/CreditCardEditText;->validator:Lcom/adyen/ui/utils/AdyenInputValidator;

    return-object p0
.end method

.method private getLogoBaseURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "/"

    .line 244
    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const-string p1, ""

    return-object p1

    :cond_0
    const/4 v1, 0x0

    add-int/lit8 v0, v0, 0x1

    .line 248
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private init()V
    .locals 3

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 53
    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    const/16 v2, 0x17

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    new-instance v1, Lcom/adyen/ui/views/CreditCardEditText$1;

    invoke-direct {v1, p0}, Lcom/adyen/ui/views/CreditCardEditText$1;-><init>(Lcom/adyen/ui/views/CreditCardEditText;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/InputFilter;

    invoke-virtual {p0, v0}, Lcom/adyen/ui/views/CreditCardEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 66
    new-instance v0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;-><init>(Lcom/adyen/ui/views/CreditCardEditText;Lcom/adyen/ui/views/CreditCardEditText$1;)V

    invoke-virtual {p0, v0}, Lcom/adyen/ui/views/CreditCardEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 68
    new-instance v0, Lcom/adyen/ui/views/CreditCardEditText$2;

    invoke-direct {v0, p0}, Lcom/adyen/ui/views/CreditCardEditText$2;-><init>(Lcom/adyen/ui/views/CreditCardEditText;)V

    invoke-virtual {p0, v0}, Lcom/adyen/ui/views/CreditCardEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method private isValidNr(Ljava/lang/String;)Z
    .locals 10

    .line 206
    invoke-static {p1}, Lcom/adyen/utils/Luhn;->check(Ljava/lang/String;)Z

    move-result v0

    .line 208
    iget-object v1, p0, Lcom/adyen/ui/views/CreditCardEditText;->numberOfDigits:[Ljava/lang/Integer;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    .line 209
    array-length v4, v1

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    if-ge v5, v4, :cond_2

    aget-object v7, v1, v5

    const-string v8, " "

    const-string v9, ""

    .line 210
    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ne v8, v7, :cond_0

    const/4 v6, 0x1

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    :cond_2
    if-eqz v0, :cond_3

    if-eqz v6, :cond_3

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    return v2
.end method

.method private setCCIcon(Landroid/graphics/Bitmap;)V
    .locals 2

    .line 219
    invoke-virtual {p0}, Lcom/adyen/ui/views/CreditCardEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/adyen/ui/views/CreditCardEditText;->getTextSize()F

    move-result v1

    float-to-int v1, v1

    invoke-static {v0, p1, v1}, Lcom/adyen/ui/utils/IconUtil;->resizeRoundCornersAndAddBorder(Landroid/content/Context;Landroid/graphics/Bitmap;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    const/4 v0, 0x0

    .line 220
    invoke-virtual {p0, p1, v0, v0, v0}, Lcom/adyen/ui/views/CreditCardEditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public addCVCFieldStatusListener(Lcom/adyen/ui/views/CreditCardEditText$CVCFieldStatusListener;)V
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/adyen/ui/views/CreditCardEditText;->cvcFieldStatusListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getCCNumber()Ljava/lang/String;
    .locals 3

    .line 108
    invoke-virtual {p0}, Lcom/adyen/ui/views/CreditCardEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initializeLogo()V
    .locals 4

    .line 233
    iget-object v0, p0, Lcom/adyen/ui/views/CreditCardEditText;->placeHolderIconUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/adyen/core/utils/StringUtils;->isEmptyOrNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 234
    invoke-virtual {p0}, Lcom/adyen/ui/views/CreditCardEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/adyen/ui/views/CreditCardEditText$3;

    invoke-direct {v1, p0}, Lcom/adyen/ui/views/CreditCardEditText$3;-><init>(Lcom/adyen/ui/views/CreditCardEditText;)V

    .line 239
    invoke-virtual {p0}, Lcom/adyen/ui/views/CreditCardEditText;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/adyen/ui/views/CreditCardEditText;->placeHolderIconUrl:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/adyen/ui/utils/IconUtil;->addScaleFactorToIconUrl(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 234
    invoke-static {v0, v1, v2, v3}, Lcom/adyen/core/utils/AsyncImageDownloader;->downloadImage(Landroid/content/Context;Lcom/adyen/core/utils/AsyncImageDownloader$ImageListener;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method public setAllowedCardTypes(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;",
            ">;)V"
        }
    .end annotation

    .line 252
    iput-object p1, p0, Lcom/adyen/ui/views/CreditCardEditText;->allowedCardTypes:Ljava/util/Map;

    return-void
.end method

.method public setCVCEditText(Lcom/adyen/ui/views/CVCEditText;)V
    .locals 0

    .line 224
    iput-object p1, p0, Lcom/adyen/ui/views/CreditCardEditText;->cvcEditText:Lcom/adyen/ui/views/CVCEditText;

    return-void
.end method

.method public setLogoUrl(Ljava/lang/String;)V
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/adyen/ui/views/CreditCardEditText;->placeHolderIconUrl:Ljava/lang/String;

    .line 229
    invoke-direct {p0, p1}, Lcom/adyen/ui/views/CreditCardEditText;->getLogoBaseURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/adyen/ui/views/CreditCardEditText;->baseURL:Ljava/lang/String;

    return-void
.end method

.method public setValidator(Lcom/adyen/ui/utils/AdyenInputValidator;)V
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/adyen/ui/views/CreditCardEditText;->validator:Lcom/adyen/ui/utils/AdyenInputValidator;

    .line 104
    iget-object p1, p0, Lcom/adyen/ui/views/CreditCardEditText;->validator:Lcom/adyen/ui/utils/AdyenInputValidator;

    invoke-virtual {p1, p0}, Lcom/adyen/ui/utils/AdyenInputValidator;->addInputField(Landroid/view/View;)V

    return-void
.end method
