.class Lcom/adyen/ui/views/CreditCardEditText$1;
.super Ljava/lang/Object;
.source "CreditCardEditText.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/views/CreditCardEditText;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/views/CreditCardEditText;


# direct methods
.method constructor <init>(Lcom/adyen/ui/views/CreditCardEditText;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/adyen/ui/views/CreditCardEditText$1;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 0

    .line 57
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object p2

    array-length p3, p2

    const/4 p4, 0x0

    :goto_0
    if-ge p4, p3, :cond_1

    aget-char p5, p2, p4

    .line 58
    invoke-static {p5}, Ljava/lang/Character;->isDigit(C)Z

    move-result p6

    if-nez p6, :cond_0

    invoke-static {p5}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result p5

    if-nez p5, :cond_0

    const-string p1, ""

    return-object p1

    :cond_0
    add-int/lit8 p4, p4, 0x1

    goto :goto_0

    :cond_1
    return-object p1
.end method
