.class public Lcom/adyen/ui/views/IBANEditText;
.super Landroid/widget/EditText;
.source "IBANEditText.java"


# static fields
.field private static final MAX_IBAN_LENGTH:I = 0x1e

.field public static final MAX_LENGTH_EDITTEXT:I = 0x24


# instance fields
.field private validator:Lcom/adyen/ui/utils/AdyenInputValidator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 27
    invoke-direct {p0}, Lcom/adyen/ui/views/IBANEditText;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    invoke-direct {p0}, Lcom/adyen/ui/views/IBANEditText;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    invoke-direct {p0}, Lcom/adyen/ui/views/IBANEditText;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .line 42
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 43
    invoke-direct {p0}, Lcom/adyen/ui/views/IBANEditText;->init()V

    return-void
.end method

.method static synthetic access$000(Lcom/adyen/ui/views/IBANEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/adyen/ui/views/IBANEditText;->validator:Lcom/adyen/ui/utils/AdyenInputValidator;

    return-object p0
.end method

.method private init()V
    .locals 3

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 53
    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    const/16 v2, 0x24

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    new-instance v1, Landroid/text/InputFilter$AllCaps;

    invoke-direct {v1}, Landroid/text/InputFilter$AllCaps;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    new-instance v1, Lcom/adyen/ui/views/IBANEditText$1;

    invoke-direct {v1, p0}, Lcom/adyen/ui/views/IBANEditText$1;-><init>(Lcom/adyen/ui/views/IBANEditText;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/InputFilter;

    invoke-virtual {p0, v0}, Lcom/adyen/ui/views/IBANEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 68
    new-instance v0, Lcom/adyen/ui/views/IBANEditText$2;

    invoke-direct {v0, p0}, Lcom/adyen/ui/views/IBANEditText$2;-><init>(Lcom/adyen/ui/views/IBANEditText;)V

    invoke-virtual {p0, v0}, Lcom/adyen/ui/views/IBANEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method


# virtual methods
.method public getIbanNumber()Ljava/lang/String;
    .locals 3

    .line 94
    invoke-virtual {p0}, Lcom/adyen/ui/views/IBANEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setValidator(Lcom/adyen/ui/utils/AdyenInputValidator;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/adyen/ui/views/IBANEditText;->validator:Lcom/adyen/ui/utils/AdyenInputValidator;

    .line 48
    iget-object p1, p0, Lcom/adyen/ui/views/IBANEditText;->validator:Lcom/adyen/ui/utils/AdyenInputValidator;

    invoke-virtual {p1, p0}, Lcom/adyen/ui/utils/AdyenInputValidator;->addInputField(Landroid/view/View;)V

    return-void
.end method
