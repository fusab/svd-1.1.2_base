.class public Lcom/adyen/ui/views/GiroPayEditText;
.super Landroid/widget/EditText;
.source "GiroPayEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/ui/views/GiroPayEditText$OnDrawableClickListener;
    }
.end annotation


# static fields
.field private static final DRAWABLE_CLICK_PADDING:I = 0xf


# instance fields
.field private onDrawableClickListener:Lcom/adyen/ui/views/GiroPayEditText$OnDrawableClickListener;

.field private rightDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 14
    iput-object p1, p0, Lcom/adyen/ui/views/GiroPayEditText;->rightDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 14
    iput-object p1, p0, Lcom/adyen/ui/views/GiroPayEditText;->rightDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    .line 14
    iput-object p1, p0, Lcom/adyen/ui/views/GiroPayEditText;->rightDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .line 32
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 p1, 0x0

    .line 14
    iput-object p1, p0, Lcom/adyen/ui/views/GiroPayEditText;->rightDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 37
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/adyen/ui/views/GiroPayEditText;->rightDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/adyen/ui/views/GiroPayEditText;->onDrawableClickListener:Lcom/adyen/ui/views/GiroPayEditText$OnDrawableClickListener;

    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 39
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 40
    iget-object v2, p0, Lcom/adyen/ui/views/GiroPayEditText;->rightDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 42
    invoke-virtual {p0}, Lcom/adyen/ui/views/GiroPayEditText;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v3, v2

    add-int/lit8 v3, v3, -0xf

    if-lt v0, v3, :cond_0

    .line 43
    invoke-virtual {p0}, Lcom/adyen/ui/views/GiroPayEditText;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/adyen/ui/views/GiroPayEditText;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0xf

    if-gt v0, v2, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/adyen/ui/views/GiroPayEditText;->getPaddingTop()I

    move-result v0

    add-int/lit8 v0, v0, -0xf

    if-lt v1, v0, :cond_0

    .line 45
    invoke-virtual {p0}, Lcom/adyen/ui/views/GiroPayEditText;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/adyen/ui/views/GiroPayEditText;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v0, v2

    add-int/lit8 v0, v0, 0xf

    if-gt v1, v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/adyen/ui/views/GiroPayEditText;->onDrawableClickListener:Lcom/adyen/ui/views/GiroPayEditText$OnDrawableClickListener;

    invoke-interface {v0}, Lcom/adyen/ui/views/GiroPayEditText$OnDrawableClickListener;->onDrawableClick()V

    const/4 v0, 0x3

    .line 48
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    const/4 p1, 0x0

    return p1

    .line 52
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public setCancelDrawable(Landroid/graphics/drawable/Drawable;Lcom/adyen/ui/views/GiroPayEditText$OnDrawableClickListener;)V
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/adyen/ui/views/GiroPayEditText;->rightDrawable:Landroid/graphics/drawable/Drawable;

    .line 58
    iput-object p2, p0, Lcom/adyen/ui/views/GiroPayEditText;->onDrawableClickListener:Lcom/adyen/ui/views/GiroPayEditText$OnDrawableClickListener;

    const/4 p2, 0x0

    .line 59
    invoke-virtual {p0, p2, p2, p1, p2}, Lcom/adyen/ui/views/GiroPayEditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
