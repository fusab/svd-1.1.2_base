.class public Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingAnimation;
.super Lcom/adyen/ui/views/loadinganimation/Animation;
.source "ThreeDotsLoadingAnimation.java"


# static fields
.field private static final ALPHA_FULL:I = 0xff

.field private static final ALPHA_TRANSPARENT:I = 0x3c

.field private static final DURATION:I = 0x384

.field private static final ONE_THIRD_DURATION:I = 0x12c

.field private static final TWO_THIRD_DURATION:I = 0x258


# instance fields
.field private alphas:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 9
    invoke-direct {p0}, Lcom/adyen/ui/views/loadinganimation/Animation;-><init>()V

    const/4 v0, 0x3

    .line 19
    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingAnimation;->alphas:[I

    return-void

    :array_0
    .array-data 4
        0x3c
        0x3c
        0xff
    .end array-data
.end method

.method static synthetic access$000(Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingAnimation;)[I
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingAnimation;->alphas:[I

    return-object p0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 8

    .line 24
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingAnimation;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x41e00000    # 28.0f

    sub-float/2addr v0, v1

    const/high16 v1, 0x40c00000    # 6.0f

    div-float/2addr v0, v1

    .line 25
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingAnimation;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    mul-float v3, v0, v2

    const/high16 v4, 0x41600000    # 14.0f

    add-float v5, v3, v4

    sub-float/2addr v1, v5

    .line 26
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingAnimation;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v2

    const/4 v2, 0x0

    :goto_0
    const/4 v6, 0x3

    if-ge v2, v6, :cond_0

    .line 28
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v6, v2

    mul-float v7, v3, v6

    add-float/2addr v7, v1

    mul-float v6, v6, v4

    add-float/2addr v7, v6

    .line 30
    invoke-virtual {p1, v7, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 31
    iget-object v6, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingAnimation;->alphas:[I

    aget v6, v6, v2

    invoke-virtual {p2, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    const/4 v6, 0x0

    .line 32
    invoke-virtual {p1, v6, v6, v0, p2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 33
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onCreateAnimators()Ljava/util/HashMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Landroid/animation/ValueAnimator;",
            "Landroid/animation/ValueAnimator$AnimatorUpdateListener;",
            ">;"
        }
    .end annotation

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const/4 v1, 0x3

    .line 40
    new-array v2, v1, [I

    fill-array-data v2, :array_0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    .line 44
    new-array v4, v1, [I

    fill-array-data v4, :array_1

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v4

    const-wide/16 v5, 0x384

    .line 45
    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    const/4 v5, -0x1

    .line 46
    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 47
    aget v5, v2, v3

    int-to-long v5, v5

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 48
    new-instance v5, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingAnimation$1;

    invoke-direct {v5, p0, v3}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingAnimation$1;-><init>(Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingAnimation;I)V

    .line 55
    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v0

    :array_0
    .array-data 4
        0x12c
        0x258
        0x0
    .end array-data

    :array_1
    .array-data 4
        0xff
        0x3c
        0x3c
    .end array-data
.end method
