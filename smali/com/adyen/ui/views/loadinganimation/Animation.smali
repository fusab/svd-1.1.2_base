.class public abstract Lcom/adyen/ui/views/loadinganimation/Animation;
.super Landroid/graphics/drawable/Drawable;
.source "Animation.java"

# interfaces
.implements Landroid/graphics/drawable/Animatable;


# static fields
.field private static final ZERO_BOUNDS_RECT:Landroid/graphics/Rect;


# instance fields
.field private alpha:I

.field private animators:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/animation/ValueAnimator;",
            "Landroid/animation/ValueAnimator$AnimatorUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field private drawBounds:Landroid/graphics/Rect;

.field private paint:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/adyen/ui/views/loadinganimation/Animation;->ZERO_BOUNDS_RECT:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 28
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->animators:Ljava/util/HashMap;

    const/16 v0, 0xff

    .line 22
    iput v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->alpha:I

    .line 24
    sget-object v0, Lcom/adyen/ui/views/loadinganimation/Animation;->ZERO_BOUNDS_RECT:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->drawBounds:Landroid/graphics/Rect;

    .line 26
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->paint:Landroid/graphics/Paint;

    .line 29
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->paint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 30
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 31
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->paint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void
.end method

.method private animatorsIsNullOrEmpty()Z
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->animators:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private isStarted()Z
    .locals 1

    .line 111
    invoke-direct {p0}, Lcom/adyen/ui/views/loadinganimation/Animation;->animatorsIsNullOrEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->animators:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private startAnimators()V
    .locals 3

    .line 85
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->animators:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 86
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 87
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 88
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 89
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/animation/ValueAnimator;

    if-eqz v2, :cond_0

    .line 91
    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 93
    :cond_0
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->paint:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v0}, Lcom/adyen/ui/views/loadinganimation/Animation;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    return-void
.end method

.method public abstract draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
.end method

.method public getAlpha()I
    .locals 1

    .line 49
    iget v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->alpha:I

    return v0
.end method

.method public getColor()I
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public getHeight()I
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->drawBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getWidth()I
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->drawBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method

.method public isRunning()Z
    .locals 1

    .line 116
    invoke-direct {p0}, Lcom/adyen/ui/views/loadinganimation/Animation;->animatorsIsNullOrEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->animators:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 4

    .line 125
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 126
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v0, v1, v2, v3, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->drawBounds:Landroid/graphics/Rect;

    return-void
.end method

.method public abstract onCreateAnimators()Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Landroid/animation/ValueAnimator;",
            "Landroid/animation/ValueAnimator$AnimatorUpdateListener;",
            ">;"
        }
    .end annotation
.end method

.method public setAlpha(I)V
    .locals 0

    .line 44
    iput p1, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->alpha:I

    return-void
.end method

.method public setColor(I)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    return-void
.end method

.method public start()V
    .locals 1

    .line 73
    invoke-direct {p0}, Lcom/adyen/ui/views/loadinganimation/Animation;->animatorsIsNullOrEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/Animation;->onCreateAnimators()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->animators:Ljava/util/HashMap;

    .line 77
    :cond_0
    invoke-direct {p0}, Lcom/adyen/ui/views/loadinganimation/Animation;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 80
    :cond_1
    invoke-direct {p0}, Lcom/adyen/ui/views/loadinganimation/Animation;->startAnimators()V

    .line 81
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/Animation;->invalidateSelf()V

    return-void
.end method

.method public stop()V
    .locals 3

    .line 100
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/Animation;->animators:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 101
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    .line 102
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 103
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 104
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->end()V

    goto :goto_0

    :cond_1
    return-void
.end method
