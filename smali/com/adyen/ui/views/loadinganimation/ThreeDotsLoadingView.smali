.class public Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;
.super Landroid/view/View;
.source "ThreeDotsLoadingView.java"


# instance fields
.field private animation:Lcom/adyen/ui/views/loadinganimation/Animation;

.field private color:I

.field maxHeight:I

.field maxWidth:I

.field minHeight:I

.field minWidth:I

.field private shouldStartAnimationDrawable:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 29
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/16 v0, 0x30

    .line 18
    iput v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->minWidth:I

    .line 19
    iput v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->maxWidth:I

    .line 20
    iput v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->minHeight:I

    .line 21
    iput v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->maxHeight:I

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 30
    invoke-direct {p0, p1, v1, v0, v0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->init(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 v0, 0x30

    .line 18
    iput v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->minWidth:I

    .line 19
    iput v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->maxWidth:I

    .line 20
    iput v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->minHeight:I

    .line 21
    iput v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->maxHeight:I

    .line 35
    sget v0, Lcom/adyen/ui/R$style;->LoadingAnimationView:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1, v0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->init(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .line 39
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 v0, 0x30

    .line 18
    iput v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->minWidth:I

    .line 19
    iput v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->maxWidth:I

    .line 20
    iput v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->minHeight:I

    .line 21
    iput v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->maxHeight:I

    .line 40
    sget v0, Lcom/adyen/ui/R$style;->LoadingAnimationView:I

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->init(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .line 45
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/16 p4, 0x30

    .line 18
    iput p4, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->minWidth:I

    .line 19
    iput p4, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->maxWidth:I

    .line 20
    iput p4, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->minHeight:I

    .line 21
    iput p4, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->maxHeight:I

    .line 46
    sget p4, Lcom/adyen/ui/R$style;->LoadingAnimationView:I

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->init(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    .line 51
    sget-object v0, Lcom/adyen/ui/R$styleable;->ThreeDotsLoadingView:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 54
    sget p2, Lcom/adyen/ui/R$styleable;->ThreeDotsLoadingView_minWidth:I

    iget p3, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->minWidth:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->minWidth:I

    .line 55
    sget p2, Lcom/adyen/ui/R$styleable;->ThreeDotsLoadingView_maxWidth:I

    iget p3, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->maxWidth:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->maxWidth:I

    .line 56
    sget p2, Lcom/adyen/ui/R$styleable;->ThreeDotsLoadingView_minHeight:I

    iget p3, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->minHeight:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->minHeight:I

    .line 57
    sget p2, Lcom/adyen/ui/R$styleable;->ThreeDotsLoadingView_maxHeight:I

    iget p3, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->maxHeight:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->maxHeight:I

    .line 58
    sget p2, Lcom/adyen/ui/R$styleable;->ThreeDotsLoadingView_indicatorColor:I

    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getContext()Landroid/content/Context;

    move-result-object p3

    sget p4, Lcom/adyen/ui/R$color;->light_green:I

    invoke-static {p3, p4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p3

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    iput p2, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->color:I

    .line 61
    new-instance p2, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingAnimation;

    invoke-direct {p2}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingAnimation;-><init>()V

    invoke-virtual {p0, p2}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->setLoadingAnimation(Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingAnimation;)V

    .line 63
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private updateDrawableBounds(II)V
    .locals 10

    .line 150
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getPaddingRight()I

    move-result v0

    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    sub-int/2addr p1, v0

    .line 151
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    sub-int/2addr p2, v0

    .line 158
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->animation:Lcom/adyen/ui/views/loadinganimation/Animation;

    if-eqz v0, :cond_2

    .line 159
    invoke-virtual {v0}, Lcom/adyen/ui/views/loadinganimation/Animation;->getIntrinsicWidth()I

    move-result v0

    .line 160
    iget-object v1, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->animation:Lcom/adyen/ui/views/loadinganimation/Animation;

    invoke-virtual {v1}, Lcom/adyen/ui/views/loadinganimation/Animation;->getIntrinsicHeight()I

    move-result v1

    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    int-to-float v1, p1

    int-to-float v2, p2

    div-float v3, v1, v2

    sub-float v4, v0, v3

    .line 164
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-double v4, v4

    const-wide v6, 0x3e7ad7f29abcaf48L    # 1.0E-7

    const/4 v8, 0x0

    cmpl-double v9, v4, v6

    if-lez v9, :cond_1

    cmpl-float v3, v3, v0

    if-lez v3, :cond_0

    mul-float v2, v2, v0

    float-to-int v0, v2

    sub-int/2addr p1, v0

    .line 167
    div-int/lit8 p1, p1, 0x2

    add-int/2addr v0, p1

    move v8, p1

    move p1, v0

    goto :goto_0

    :cond_0
    const/high16 v2, 0x3f800000    # 1.0f

    div-float/2addr v2, v0

    mul-float v1, v1, v2

    float-to-int v0, v1

    sub-int/2addr p2, v0

    .line 171
    div-int/lit8 p2, p2, 0x2

    add-int/2addr v0, p2

    goto :goto_1

    :cond_1
    :goto_0
    move v0, p2

    const/4 p2, 0x0

    .line 175
    :goto_1
    iget-object v1, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->animation:Lcom/adyen/ui/views/loadinganimation/Animation;

    invoke-virtual {v1, v8, p2, p1, v0}, Lcom/adyen/ui/views/loadinganimation/Animation;->setBounds(IIII)V

    :cond_2
    return-void
.end method

.method private updateDrawableState()V
    .locals 2

    .line 229
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getDrawableState()[I

    move-result-object v0

    .line 230
    iget-object v1, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->animation:Lcom/adyen/ui/views/loadinganimation/Animation;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/adyen/ui/views/loadinganimation/Animation;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 231
    iget-object v1, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->animation:Lcom/adyen/ui/views/loadinganimation/Animation;

    invoke-virtual {v1, v0}, Lcom/adyen/ui/views/loadinganimation/Animation;->setState([I)Z

    :cond_0
    return-void
.end method


# virtual methods
.method drawTrack(Landroid/graphics/Canvas;)V
    .locals 3

    .line 186
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->animation:Lcom/adyen/ui/views/loadinganimation/Animation;

    if-eqz v0, :cond_0

    .line 187
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 189
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 191
    iget-object v1, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->animation:Lcom/adyen/ui/views/loadinganimation/Animation;

    invoke-virtual {v1, p1}, Lcom/adyen/ui/views/loadinganimation/Animation;->draw(Landroid/graphics/Canvas;)V

    .line 192
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 194
    iget-boolean p1, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->shouldStartAnimationDrawable:Z

    if-eqz p1, :cond_0

    .line 195
    iget-object p1, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->animation:Lcom/adyen/ui/views/loadinganimation/Animation;

    invoke-virtual {p1}, Lcom/adyen/ui/views/loadinganimation/Animation;->start()V

    const/4 p1, 0x0

    .line 196
    iput-boolean p1, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->shouldStartAnimationDrawable:Z

    :cond_0
    return-void
.end method

.method public drawableHotspotChanged(FF)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .line 238
    invoke-super {p0, p1, p2}, Landroid/view/View;->drawableHotspotChanged(FF)V

    .line 239
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->animation:Lcom/adyen/ui/views/loadinganimation/Animation;

    if-eqz v0, :cond_0

    .line 240
    invoke-virtual {v0, p1, p2}, Lcom/adyen/ui/views/loadinganimation/Animation;->setHotspot(FF)V

    :cond_0
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 0

    .line 224
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 225
    invoke-direct {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->updateDrawableState()V

    return-void
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 5

    .line 132
    invoke-virtual {p0, p1}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object p1

    .line 134
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getScrollY()I

    move-result v1

    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    .line 137
    iget v2, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v0

    iget v3, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v1

    iget v4, p1, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v0

    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr p1, v1

    invoke-virtual {p0, v2, v3, v4, p1}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->invalidate(IIII)V

    goto :goto_0

    .line 140
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .line 246
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 247
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->startAnimation()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .line 252
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->stopAnimation()V

    .line 253
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    return-void
.end method

.method protected declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    monitor-enter p0

    .line 181
    :try_start_0
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 182
    invoke-virtual {p0, p1}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->drawTrack(Landroid/graphics/Canvas;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected declared-synchronized onMeasure(II)V
    .locals 5

    monitor-enter p0

    .line 206
    :try_start_0
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->animation:Lcom/adyen/ui/views/loadinganimation/Animation;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 208
    iget v2, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->minWidth:I

    iget v3, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->maxWidth:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 209
    iget v3, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->minHeight:I

    iget v4, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->maxHeight:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 212
    :goto_0
    invoke-direct {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->updateDrawableState()V

    .line 214
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 215
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 217
    invoke-static {v2, p1, v1}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->resolveSizeAndState(III)I

    move-result p1

    .line 218
    invoke-static {v0, p2, v1}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->resolveSizeAndState(III)I

    move-result p2

    .line 219
    invoke-virtual {p0, p1, p2}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->setMeasuredDimension(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 146
    invoke-direct {p0, p1, p2}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->updateDrawableBounds(II)V

    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 0

    .line 122
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    const/16 p1, 0x8

    if-eq p2, p1, :cond_1

    const/4 p1, 0x4

    if-ne p2, p1, :cond_0

    goto :goto_0

    .line 126
    :cond_0
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->startAnimation()V

    goto :goto_1

    .line 124
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->stopAnimation()V

    :goto_1
    return-void
.end method

.method public setColor(I)V
    .locals 1

    .line 83
    iput p1, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->color:I

    .line 84
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->animation:Lcom/adyen/ui/views/loadinganimation/Animation;

    invoke-virtual {v0, p1}, Lcom/adyen/ui/views/loadinganimation/Animation;->setColor(I)V

    return-void
.end method

.method public setLoadingAnimation(Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingAnimation;)V
    .locals 2

    .line 67
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->animation:Lcom/adyen/ui/views/loadinganimation/Animation;

    if-eq v0, p1, :cond_2

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 69
    invoke-virtual {v0, v1}, Lcom/adyen/ui/views/loadinganimation/Animation;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 70
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->animation:Lcom/adyen/ui/views/loadinganimation/Animation;

    invoke-virtual {p0, v0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 73
    :cond_0
    iput-object p1, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->animation:Lcom/adyen/ui/views/loadinganimation/Animation;

    .line 74
    iget v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->color:I

    invoke-virtual {p0, v0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->setColor(I)V

    if-eqz p1, :cond_1

    .line 76
    invoke-virtual {p1, p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingAnimation;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 78
    :cond_1
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->postInvalidate()V

    :cond_2
    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getVisibility()I

    move-result v0

    if-eq v0, p1, :cond_2

    .line 111
    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    const/16 v0, 0x8

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 115
    :cond_0
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->startAnimation()V

    goto :goto_1

    .line 113
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->stopAnimation()V

    :cond_2
    :goto_1
    return-void
.end method

.method startAnimation()V
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 96
    iput-boolean v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->shouldStartAnimationDrawable:Z

    .line 97
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->postInvalidate()V

    return-void
.end method

.method stopAnimation()V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->animation:Lcom/adyen/ui/views/loadinganimation/Animation;

    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {v0}, Lcom/adyen/ui/views/loadinganimation/Animation;->stop()V

    :cond_0
    const/4 v0, 0x0

    .line 104
    iput-boolean v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->shouldStartAnimationDrawable:Z

    .line 105
    invoke-virtual {p0}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->postInvalidate()V

    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->animation:Lcom/adyen/ui/views/loadinganimation/Animation;

    if-eq p1, v0, :cond_1

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method
