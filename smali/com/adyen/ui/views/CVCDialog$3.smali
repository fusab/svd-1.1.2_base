.class Lcom/adyen/ui/views/CVCDialog$3;
.super Ljava/lang/Object;
.source "CVCDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/views/CVCDialog;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/views/CVCDialog;

.field final synthetic val$cancelButton:Landroid/widget/Button;

.field final synthetic val$checkoutButton:Landroid/widget/Button;

.field final synthetic val$cvcEditText:Lcom/adyen/ui/views/CVCEditText;


# direct methods
.method constructor <init>(Lcom/adyen/ui/views/CVCDialog;Lcom/adyen/ui/views/CVCEditText;Landroid/widget/Button;Landroid/widget/Button;)V
    .locals 0

    .line 89
    iput-object p1, p0, Lcom/adyen/ui/views/CVCDialog$3;->this$0:Lcom/adyen/ui/views/CVCDialog;

    iput-object p2, p0, Lcom/adyen/ui/views/CVCDialog$3;->val$cvcEditText:Lcom/adyen/ui/views/CVCEditText;

    iput-object p3, p0, Lcom/adyen/ui/views/CVCDialog$3;->val$checkoutButton:Landroid/widget/Button;

    iput-object p4, p0, Lcom/adyen/ui/views/CVCDialog$3;->val$cancelButton:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 92
    iget-object p1, p0, Lcom/adyen/ui/views/CVCDialog$3;->val$cvcEditText:Lcom/adyen/ui/views/CVCEditText;

    invoke-virtual {p1}, Lcom/adyen/ui/views/CVCEditText;->hasValidInput()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 95
    :cond_0
    new-instance p1, Landroid/content/Intent;

    const-string v0, "com.adyen.core.ui.PaymentDetailsProvided"

    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 96
    new-instance v0, Lcom/adyen/core/models/paymentdetails/PaymentDetails;

    iget-object v1, p0, Lcom/adyen/ui/views/CVCDialog$3;->this$0:Lcom/adyen/ui/views/CVCDialog;

    invoke-static {v1}, Lcom/adyen/ui/views/CVCDialog;->access$100(Lcom/adyen/ui/views/CVCDialog;)Lcom/adyen/core/models/PaymentMethod;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adyen/core/models/PaymentMethod;->getInputDetails()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;-><init>(Ljava/util/Collection;)V

    .line 97
    iget-object v1, p0, Lcom/adyen/ui/views/CVCDialog$3;->val$cvcEditText:Lcom/adyen/ui/views/CVCEditText;

    invoke-virtual {v1}, Lcom/adyen/ui/views/CVCEditText;->getCVC()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cardDetails.cvc"

    invoke-virtual {v0, v2, v1}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;->fill(Ljava/lang/String;Ljava/lang/String;)Z

    const-string v1, "PaymentDetails"

    .line 98
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 99
    iget-object v0, p0, Lcom/adyen/ui/views/CVCDialog$3;->this$0:Lcom/adyen/ui/views/CVCDialog;

    invoke-virtual {v0}, Lcom/adyen/ui/views/CVCDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 101
    iget-object p1, p0, Lcom/adyen/ui/views/CVCDialog$3;->val$cvcEditText:Lcom/adyen/ui/views/CVCEditText;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/adyen/ui/views/CVCEditText;->setEnabled(Z)V

    .line 102
    iget-object p1, p0, Lcom/adyen/ui/views/CVCDialog$3;->val$checkoutButton:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 103
    iget-object p1, p0, Lcom/adyen/ui/views/CVCDialog$3;->val$cancelButton:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 105
    iget-object p1, p0, Lcom/adyen/ui/views/CVCDialog$3;->this$0:Lcom/adyen/ui/views/CVCDialog;

    invoke-static {p1, v0}, Lcom/adyen/ui/views/CVCDialog;->access$202(Lcom/adyen/ui/views/CVCDialog;Z)Z

    .line 107
    iget-object p1, p0, Lcom/adyen/ui/views/CVCDialog$3;->this$0:Lcom/adyen/ui/views/CVCDialog;

    sget v1, Lcom/adyen/ui/R$id;->dialog_view:I

    invoke-virtual {p1, v1}, Lcom/adyen/ui/views/CVCDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/4 v1, 0x4

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 108
    iget-object p1, p0, Lcom/adyen/ui/views/CVCDialog$3;->this$0:Lcom/adyen/ui/views/CVCDialog;

    sget v1, Lcom/adyen/ui/R$id;->progress_bar:I

    invoke-virtual {p1, v1}, Lcom/adyen/ui/views/CVCDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
