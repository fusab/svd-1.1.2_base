.class Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;
.super Ljava/lang/Object;
.source "ExpiryDateEditText.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/ui/views/ExpiryDateEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ExpiryDateFormatWatcher"
.end annotation


# static fields
.field private static final SEPARATOR_CHAR:C = '/'


# instance fields
.field private deleted:Z

.field private final separatorString:Ljava/lang/String;

.field final synthetic this$0:Lcom/adyen/ui/views/ExpiryDateEditText;


# direct methods
.method constructor <init>(Lcom/adyen/ui/views/ExpiryDateEditText;)V
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;->this$0:Lcom/adyen/ui/views/ExpiryDateEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 p1, 0x2f

    .line 86
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;->separatorString:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 6

    .line 102
    iget-object v0, p0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;->this$0:Lcom/adyen/ui/views/ExpiryDateEditText;

    invoke-virtual {v0, p0}, Lcom/adyen/ui/views/ExpiryDateEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 104
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const-string v1, "0"

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    invoke-interface {p1, v2}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    const/16 v3, 0x31

    if-le v0, v3, :cond_0

    .line 105
    invoke-interface {p1, v2, v1}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 108
    :cond_0
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const/16 v3, 0x2f

    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    iget-boolean v0, p0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;->deleted:Z

    if-nez v0, :cond_2

    .line 109
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v5, "\\d[^\\d]"

    invoke-virtual {v0, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    invoke-interface {p1, v2, v1}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_0

    .line 111
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\d*[^\\d]\\d*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 112
    invoke-interface {p1, v3}, Landroid/text/Editable;->append(C)Landroid/text/Editable;

    .line 116
    :cond_2
    :goto_0
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 117
    invoke-interface {p1, v2}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    if-ne v2, v4, :cond_6

    if-eq v0, v3, :cond_7

    .line 121
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v0

    if-nez v0, :cond_3

    add-int/lit8 v0, v2, 0x1

    .line 122
    iget-object v1, p0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;->separatorString:Ljava/lang/String;

    invoke-interface {p1, v2, v0, v1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_1

    .line 124
    :cond_3
    iget-object v0, p0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;->separatorString:Ljava/lang/String;

    invoke-interface {p1, v2, v0}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 126
    iget-boolean v0, p0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;->deleted:Z

    if-eqz v0, :cond_7

    .line 127
    iget-object v0, p0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;->this$0:Lcom/adyen/ui/views/ExpiryDateEditText;

    invoke-virtual {v0}, Lcom/adyen/ui/views/ExpiryDateEditText;->getSelectionStart()I

    move-result v0

    .line 128
    iget-object v1, p0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;->this$0:Lcom/adyen/ui/views/ExpiryDateEditText;

    invoke-virtual {v1}, Lcom/adyen/ui/views/ExpiryDateEditText;->getSelectionEnd()I

    move-result v1

    add-int/lit8 v5, v0, -0x1

    if-ne v5, v2, :cond_4

    move v0, v5

    :cond_4
    add-int/lit8 v5, v1, -0x1

    if-ne v5, v2, :cond_5

    move v1, v5

    .line 131
    :cond_5
    iget-object v5, p0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;->this$0:Lcom/adyen/ui/views/ExpiryDateEditText;

    invoke-virtual {v5, v0, v1}, Lcom/adyen/ui/views/ExpiryDateEditText;->setSelection(II)V

    goto :goto_1

    .line 136
    :cond_6
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v0

    if-nez v0, :cond_7

    add-int/lit8 v0, v2, 0x1

    .line 137
    invoke-interface {p1, v2, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    :cond_7
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 142
    :cond_8
    iget-object v0, p0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;->this$0:Lcom/adyen/ui/views/ExpiryDateEditText;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/adyen/ui/views/ExpiryDateEditText;->access$000(Lcom/adyen/ui/views/ExpiryDateEditText;Ljava/lang/String;)Z

    move-result p1

    .line 144
    iget-object v0, p0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;->this$0:Lcom/adyen/ui/views/ExpiryDateEditText;

    invoke-static {v0}, Lcom/adyen/ui/views/ExpiryDateEditText;->access$100(Lcom/adyen/ui/views/ExpiryDateEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 145
    iget-object v0, p0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;->this$0:Lcom/adyen/ui/views/ExpiryDateEditText;

    invoke-static {v0}, Lcom/adyen/ui/views/ExpiryDateEditText;->access$100(Lcom/adyen/ui/views/ExpiryDateEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;

    move-result-object v0

    iget-object v1, p0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;->this$0:Lcom/adyen/ui/views/ExpiryDateEditText;

    invoke-virtual {v0, v1, p1}, Lcom/adyen/ui/utils/AdyenInputValidator;->setReady(Landroid/view/View;Z)V

    :cond_9
    if-eqz p1, :cond_a

    .line 149
    iget-object p1, p0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;->this$0:Lcom/adyen/ui/views/ExpiryDateEditText;

    const/16 v0, 0x42

    invoke-virtual {p1, v0}, Lcom/adyen/ui/views/ExpiryDateEditText;->focusSearch(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_a

    .line 151
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 155
    :cond_a
    iget-object p1, p0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;->this$0:Lcom/adyen/ui/views/ExpiryDateEditText;

    invoke-virtual {p1, p0}, Lcom/adyen/ui/views/ExpiryDateEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    if-nez p4, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 97
    :goto_0
    iput-boolean p1, p0, Lcom/adyen/ui/views/ExpiryDateEditText$ExpiryDateFormatWatcher;->deleted:Z

    return-void
.end method
