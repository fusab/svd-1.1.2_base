.class Lcom/adyen/ui/views/ExpiryDateEditText$1;
.super Ljava/lang/Object;
.source "ExpiryDateEditText.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/views/ExpiryDateEditText;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/views/ExpiryDateEditText;


# direct methods
.method constructor <init>(Lcom/adyen/ui/views/ExpiryDateEditText;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/adyen/ui/views/ExpiryDateEditText$1;->this$0:Lcom/adyen/ui/views/ExpiryDateEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1

    if-eqz p2, :cond_0

    .line 38
    iget-object p1, p0, Lcom/adyen/ui/views/ExpiryDateEditText$1;->this$0:Lcom/adyen/ui/views/ExpiryDateEditText;

    invoke-virtual {p1}, Lcom/adyen/ui/views/ExpiryDateEditText;->getContext()Landroid/content/Context;

    move-result-object p2

    sget v0, Lcom/adyen/ui/R$color;->black_text:I

    invoke-static {p2, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/adyen/ui/views/ExpiryDateEditText;->setTextColor(I)V

    goto :goto_0

    .line 40
    :cond_0
    iget-object p1, p0, Lcom/adyen/ui/views/ExpiryDateEditText$1;->this$0:Lcom/adyen/ui/views/ExpiryDateEditText;

    invoke-virtual {p1}, Lcom/adyen/ui/views/ExpiryDateEditText;->getText()Landroid/text/Editable;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/adyen/ui/views/ExpiryDateEditText;->access$000(Lcom/adyen/ui/views/ExpiryDateEditText;Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 41
    iget-object p1, p0, Lcom/adyen/ui/views/ExpiryDateEditText$1;->this$0:Lcom/adyen/ui/views/ExpiryDateEditText;

    invoke-virtual {p1}, Lcom/adyen/ui/views/ExpiryDateEditText;->getContext()Landroid/content/Context;

    move-result-object p2

    sget v0, Lcom/adyen/ui/R$color;->red_invalid_input_highlight:I

    invoke-static {p2, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/adyen/ui/views/ExpiryDateEditText;->setTextColor(I)V

    :cond_1
    :goto_0
    return-void
.end method
