.class Lcom/adyen/ui/views/CardHolderEditText$1;
.super Ljava/lang/Object;
.source "CardHolderEditText.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/views/CardHolderEditText;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/views/CardHolderEditText;


# direct methods
.method constructor <init>(Lcom/adyen/ui/views/CardHolderEditText;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/adyen/ui/views/CardHolderEditText$1;->this$0:Lcom/adyen/ui/views/CardHolderEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/adyen/ui/views/CardHolderEditText$1;->this$0:Lcom/adyen/ui/views/CardHolderEditText;

    invoke-static {v0}, Lcom/adyen/ui/views/CardHolderEditText;->access$000(Lcom/adyen/ui/views/CardHolderEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/adyen/ui/views/CardHolderEditText$1;->this$0:Lcom/adyen/ui/views/CardHolderEditText;

    invoke-static {v0}, Lcom/adyen/ui/views/CardHolderEditText;->access$000(Lcom/adyen/ui/views/CardHolderEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;

    move-result-object v0

    iget-object v1, p0, Lcom/adyen/ui/views/CardHolderEditText$1;->this$0:Lcom/adyen/ui/views/CardHolderEditText;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/adyen/ui/utils/AdyenInputValidator;->setReady(Landroid/view/View;Z)V

    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
