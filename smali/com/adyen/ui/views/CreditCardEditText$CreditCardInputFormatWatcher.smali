.class Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;
.super Ljava/lang/Object;
.source "CreditCardEditText.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/ui/views/CreditCardEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CreditCardInputFormatWatcher"
.end annotation


# static fields
.field private static final SPACING_CHAR:C = ' '


# instance fields
.field private deleted:Z

.field private final spacingString:Ljava/lang/String;

.field final synthetic this$0:Lcom/adyen/ui/views/CreditCardEditText;


# direct methods
.method private constructor <init>(Lcom/adyen/ui/views/CreditCardEditText;)V
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 p1, 0x20

    .line 114
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->spacingString:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/adyen/ui/views/CreditCardEditText;Lcom/adyen/ui/views/CreditCardEditText$1;)V
    .locals 0

    .line 111
    invoke-direct {p0, p1}, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;-><init>(Lcom/adyen/ui/views/CreditCardEditText;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 7

    .line 130
    iget-object v0, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-virtual {v0, p0}, Lcom/adyen/ui/views/CreditCardEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 132
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    const/4 v3, 0x4

    :goto_0
    if-ge v2, v0, :cond_6

    .line 136
    invoke-interface {p1, v2}, Landroid/text/Editable;->charAt(I)C

    move-result v4

    if-ne v2, v3, :cond_4

    const/16 v5, 0x20

    if-eq v4, v5, :cond_3

    .line 140
    invoke-static {v4}, Ljava/lang/Character;->isDigit(C)Z

    move-result v4

    if-nez v4, :cond_0

    add-int/lit8 v4, v2, 0x1

    .line 141
    iget-object v5, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->spacingString:Ljava/lang/String;

    invoke-interface {p1, v2, v4, v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_1

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->spacingString:Ljava/lang/String;

    invoke-interface {p1, v2, v0}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 144
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    .line 146
    iget-boolean v4, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->deleted:Z

    if-eqz v4, :cond_3

    .line 147
    iget-object v4, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-virtual {v4}, Lcom/adyen/ui/views/CreditCardEditText;->getSelectionStart()I

    move-result v4

    .line 148
    iget-object v5, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-virtual {v5}, Lcom/adyen/ui/views/CreditCardEditText;->getSelectionEnd()I

    move-result v5

    add-int/lit8 v6, v4, -0x1

    if-ne v6, v2, :cond_1

    move v4, v6

    :cond_1
    add-int/lit8 v6, v5, -0x1

    if-ne v6, v2, :cond_2

    move v5, v6

    .line 151
    :cond_2
    iget-object v6, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-virtual {v6, v4, v5}, Lcom/adyen/ui/views/CreditCardEditText;->setSelection(II)V

    :cond_3
    :goto_1
    add-int/lit8 v3, v3, 0x5

    goto :goto_2

    .line 158
    :cond_4
    invoke-static {v4}, Ljava/lang/Character;->isDigit(C)Z

    move-result v4

    if-nez v4, :cond_5

    add-int/lit8 v0, v2, 0x1

    .line 159
    invoke-interface {p1, v2, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 160
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    :cond_5
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 165
    :cond_6
    iget-object v0, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-virtual {v0, p0}, Lcom/adyen/ui/views/CreditCardEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const/4 v0, 0x3

    .line 169
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-static {v3}, Lcom/adyen/ui/views/CreditCardEditText;->access$200(Lcom/adyen/ui/views/CreditCardEditText;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 171
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, " "

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/adyen/utils/CardType;->detect(Ljava/lang/String;Ljava/util/List;)Lcom/adyen/utils/CardType;

    move-result-object v2

    .line 172
    sget-object v3, Lcom/adyen/utils/CardType;->amex:Lcom/adyen/utils/CardType;

    if-ne v2, v3, :cond_7

    const/4 v0, 0x4

    .line 175
    :cond_7
    sget-object v1, Lcom/adyen/utils/CardType;->UNKNOWN:Lcom/adyen/utils/CardType;

    const/4 v3, 0x0

    if-eq v2, v1, :cond_9

    .line 176
    iget-object v1, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-static {v1}, Lcom/adyen/ui/views/CreditCardEditText;->access$300(Lcom/adyen/ui/views/CreditCardEditText;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/adyen/core/utils/StringUtils;->isEmptyOrNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 177
    iget-object v1, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-virtual {v1}, Lcom/adyen/ui/views/CreditCardEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-static {v5}, Lcom/adyen/ui/views/CreditCardEditText;->access$300(Lcom/adyen/ui/views/CreditCardEditText;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v5, ".png"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/adyen/ui/utils/IconUtil;->addScaleFactorToIconUrl(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 178
    iget-object v4, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-virtual {v4}, Lcom/adyen/ui/views/CreditCardEditText;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher$1;

    invoke-direct {v5, p0}, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher$1;-><init>(Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;)V

    invoke-static {v4, v5, v1, v3}, Lcom/adyen/core/utils/AsyncImageDownloader;->downloadImage(Landroid/content/Context;Lcom/adyen/core/utils/AsyncImageDownloader$ImageListener;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 185
    :cond_8
    iget-object v1, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-virtual {v2}, Lcom/adyen/utils/CardType;->getNumberOfDigits()[Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/adyen/ui/views/CreditCardEditText;->access$502(Lcom/adyen/ui/views/CreditCardEditText;[Ljava/lang/Integer;)[Ljava/lang/Integer;

    goto :goto_3

    .line 187
    :cond_9
    iget-object v1, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-virtual {v1}, Lcom/adyen/ui/views/CreditCardEditText;->initializeLogo()V

    .line 188
    iget-object v1, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-static {v1, v3}, Lcom/adyen/ui/views/CreditCardEditText;->access$502(Lcom/adyen/ui/views/CreditCardEditText;[Ljava/lang/Integer;)[Ljava/lang/Integer;

    .line 191
    :goto_3
    iget-object v1, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-static {v1}, Lcom/adyen/ui/views/CreditCardEditText;->access$600(Lcom/adyen/ui/views/CreditCardEditText;)Lcom/adyen/ui/views/CVCEditText;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 192
    iget-object v1, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-static {v1}, Lcom/adyen/ui/views/CreditCardEditText;->access$700(Lcom/adyen/ui/views/CreditCardEditText;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/adyen/ui/views/CreditCardEditText$CVCFieldStatusListener;

    .line 193
    iget-object v4, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-static {v4}, Lcom/adyen/ui/views/CreditCardEditText;->access$200(Lcom/adyen/ui/views/CreditCardEditText;)Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v2}, Lcom/adyen/utils/CardType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    invoke-interface {v3, v4}, Lcom/adyen/ui/views/CreditCardEditText$CVCFieldStatusListener;->onCVCFieldStatusChanged(Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;)V

    goto :goto_4

    .line 195
    :cond_a
    iget-object v1, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-static {v1}, Lcom/adyen/ui/views/CreditCardEditText;->access$600(Lcom/adyen/ui/views/CreditCardEditText;)Lcom/adyen/ui/views/CVCEditText;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/adyen/ui/views/CVCEditText;->setMaxLength(I)V

    .line 198
    :cond_b
    iget-object v0, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-static {v0}, Lcom/adyen/ui/views/CreditCardEditText;->access$800(Lcom/adyen/ui/views/CreditCardEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 199
    iget-object v0, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-static {v0}, Lcom/adyen/ui/views/CreditCardEditText;->access$800(Lcom/adyen/ui/views/CreditCardEditText;)Lcom/adyen/ui/utils/AdyenInputValidator;

    move-result-object v0

    iget-object v1, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->this$0:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/adyen/ui/views/CreditCardEditText;->access$100(Lcom/adyen/ui/views/CreditCardEditText;Ljava/lang/String;)Z

    move-result p1

    invoke-virtual {v0, v1, p1}, Lcom/adyen/ui/utils/AdyenInputValidator;->setReady(Landroid/view/View;Z)V

    :cond_c
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    if-nez p4, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 125
    :goto_0
    iput-boolean p1, p0, Lcom/adyen/ui/views/CreditCardEditText$CreditCardInputFormatWatcher;->deleted:Z

    return-void
.end method
