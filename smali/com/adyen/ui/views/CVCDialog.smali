.class public Lcom/adyen/ui/views/CVCDialog;
.super Landroid/app/Dialog;
.source "CVCDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/ui/views/CVCDialog$CVCDialogListener;
    }
.end annotation


# instance fields
.field private activity:Landroid/app/Activity;

.field private amount:Lcom/adyen/core/models/Amount;

.field private backButtonEnabled:Z

.field private cvcDialogListener:Lcom/adyen/ui/views/CVCDialog$CVCDialogListener;

.field private paymentMethod:Lcom/adyen/core/models/PaymentMethod;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/adyen/core/models/Amount;Lcom/adyen/core/models/PaymentMethod;Lcom/adyen/ui/views/CVCDialog$CVCDialogListener;)V
    .locals 1

    .line 42
    sget v0, Lcom/adyen/ui/R$style;->dialogStyle:I

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    const/4 v0, 0x1

    .line 31
    iput-boolean v0, p0, Lcom/adyen/ui/views/CVCDialog;->backButtonEnabled:Z

    .line 43
    iput-object p1, p0, Lcom/adyen/ui/views/CVCDialog;->activity:Landroid/app/Activity;

    .line 44
    iput-object p2, p0, Lcom/adyen/ui/views/CVCDialog;->amount:Lcom/adyen/core/models/Amount;

    .line 45
    iput-object p3, p0, Lcom/adyen/ui/views/CVCDialog;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    .line 46
    iput-object p4, p0, Lcom/adyen/ui/views/CVCDialog;->cvcDialogListener:Lcom/adyen/ui/views/CVCDialog$CVCDialogListener;

    return-void
.end method

.method static synthetic access$000(Lcom/adyen/ui/views/CVCDialog;)Lcom/adyen/ui/views/CVCDialog$CVCDialogListener;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/adyen/ui/views/CVCDialog;->cvcDialogListener:Lcom/adyen/ui/views/CVCDialog$CVCDialogListener;

    return-object p0
.end method

.method static synthetic access$100(Lcom/adyen/ui/views/CVCDialog;)Lcom/adyen/core/models/PaymentMethod;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/adyen/ui/views/CVCDialog;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    return-object p0
.end method

.method static synthetic access$202(Lcom/adyen/ui/views/CVCDialog;Z)Z
    .locals 0

    .line 27
    iput-boolean p1, p0, Lcom/adyen/ui/views/CVCDialog;->backButtonEnabled:Z

    return p1
.end method

.method static synthetic access$300(Lcom/adyen/ui/views/CVCDialog;)Landroid/app/Activity;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/adyen/ui/views/CVCDialog;->activity:Landroid/app/Activity;

    return-object p0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .line 140
    iget-boolean v0, p0, Lcom/adyen/ui/views/CVCDialog;->backButtonEnabled:Z

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/adyen/ui/views/CVCDialog;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .line 51
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    .line 53
    invoke-virtual {p0, p1}, Lcom/adyen/ui/views/CVCDialog;->requestWindowFeature(I)Z

    .line 54
    sget v0, Lcom/adyen/ui/R$layout;->cvc_dialog:I

    invoke-virtual {p0, v0}, Lcom/adyen/ui/views/CVCDialog;->setContentView(I)V

    const/4 v0, 0x0

    .line 56
    invoke-virtual {p0, v0}, Lcom/adyen/ui/views/CVCDialog;->setCanceledOnTouchOutside(Z)V

    .line 58
    sget v1, Lcom/adyen/ui/R$id;->extended_cvc_hint_textview:I

    invoke-virtual {p0, v1}, Lcom/adyen/ui/views/CVCDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 59
    iget-object v2, p0, Lcom/adyen/ui/views/CVCDialog;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {v2}, Lcom/adyen/core/models/PaymentMethod;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    const-string/jumbo v4, "\u00a0"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 61
    invoke-virtual {p0}, Lcom/adyen/ui/views/CVCDialog;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/adyen/ui/R$string;->creditCard_oneClickVerification_message:I

    new-array v5, p1, [Ljava/lang/Object;

    aput-object v2, v5, v0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 62
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    sget v1, Lcom/adyen/ui/R$id;->adyen_credit_card_cvc:I

    invoke-virtual {p0, v1}, Lcom/adyen/ui/views/CVCDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/adyen/ui/views/CVCEditText;

    .line 66
    sget v2, Lcom/adyen/ui/R$id;->button_cancel_cvc_verification:I

    invoke-virtual {p0, v2}, Lcom/adyen/ui/views/CVCDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 68
    new-instance v3, Lcom/adyen/ui/views/CVCDialog$1;

    invoke-direct {v3, p0}, Lcom/adyen/ui/views/CVCDialog$1;-><init>(Lcom/adyen/ui/views/CVCDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    new-instance v3, Lcom/adyen/ui/utils/AdyenInputValidator;

    invoke-direct {v3}, Lcom/adyen/ui/utils/AdyenInputValidator;-><init>()V

    .line 76
    sget v4, Lcom/adyen/ui/R$id;->button_confirm_cvc_verification:I

    invoke-virtual {p0, v4}, Lcom/adyen/ui/views/CVCDialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 77
    iget-object v5, p0, Lcom/adyen/ui/views/CVCDialog;->amount:Lcom/adyen/core/models/Amount;

    iget-object v6, p0, Lcom/adyen/ui/views/CVCDialog;->activity:Landroid/app/Activity;

    invoke-static {v6}, Lcom/adyen/core/utils/StringUtils;->getLocale(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v6

    invoke-static {v5, p1, v6}, Lcom/adyen/core/utils/AmountUtil;->format(Lcom/adyen/core/models/Amount;ZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 78
    invoke-virtual {p0}, Lcom/adyen/ui/views/CVCDialog;->getContext()Landroid/content/Context;

    move-result-object v6

    sget v7, Lcom/adyen/ui/R$string;->payButton_formatted:I

    new-array p1, p1, [Ljava/lang/Object;

    aput-object v5, p1, v0

    invoke-virtual {v6, v7, p1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 79
    invoke-virtual {v4, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 80
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 81
    new-instance p1, Lcom/adyen/ui/views/CVCDialog$2;

    invoke-direct {p1, p0, v4}, Lcom/adyen/ui/views/CVCDialog$2;-><init>(Lcom/adyen/ui/views/CVCDialog;Landroid/widget/Button;)V

    invoke-virtual {v3, p1}, Lcom/adyen/ui/utils/AdyenInputValidator;->setOnReadyStateChangedListener(Lcom/adyen/ui/utils/AdyenInputValidator$OnReadyStateChangedListener;)V

    .line 87
    invoke-virtual {v1, v3}, Lcom/adyen/ui/views/CVCEditText;->setValidator(Lcom/adyen/ui/utils/AdyenInputValidator;)V

    .line 89
    new-instance p1, Lcom/adyen/ui/views/CVCDialog$3;

    invoke-direct {p1, p0, v1, v4, v2}, Lcom/adyen/ui/views/CVCDialog$3;-><init>(Lcom/adyen/ui/views/CVCDialog;Lcom/adyen/ui/views/CVCEditText;Landroid/widget/Button;Landroid/widget/Button;)V

    invoke-virtual {v4, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    new-instance p1, Lcom/adyen/ui/views/CVCDialog$4;

    invoke-direct {p1, p0, v1}, Lcom/adyen/ui/views/CVCDialog$4;-><init>(Lcom/adyen/ui/views/CVCDialog;Lcom/adyen/ui/views/CVCEditText;)V

    invoke-virtual {p0, p1}, Lcom/adyen/ui/views/CVCDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 123
    new-instance p1, Lcom/adyen/ui/views/CVCDialog$5;

    invoke-direct {p1, p0}, Lcom/adyen/ui/views/CVCDialog$5;-><init>(Lcom/adyen/ui/views/CVCDialog;)V

    invoke-virtual {p0, p1}, Lcom/adyen/ui/views/CVCDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 132
    iget-object p1, p0, Lcom/adyen/ui/views/CVCDialog;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {p1}, Lcom/adyen/core/models/PaymentMethod;->getType()Ljava/lang/String;

    move-result-object p1

    sget-object v0, Lcom/adyen/utils/CardType;->amex:Lcom/adyen/utils/CardType;

    invoke-virtual {v0}, Lcom/adyen/utils/CardType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x4

    .line 133
    invoke-virtual {v1, p1}, Lcom/adyen/ui/views/CVCEditText;->setMaxLength(I)V

    :cond_0
    return-void
.end method
