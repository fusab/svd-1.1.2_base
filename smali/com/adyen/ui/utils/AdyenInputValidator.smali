.class public Lcom/adyen/ui/utils/AdyenInputValidator;
.super Ljava/lang/Object;
.source "AdyenInputValidator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/ui/utils/AdyenInputValidator$OnReadyStateChangedListener;
    }
.end annotation


# instance fields
.field private allInputReady:Z

.field private inputFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/view/View;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private onReadyStateChangedListener:Lcom/adyen/ui/utils/AdyenInputValidator$OnReadyStateChangedListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/adyen/ui/utils/AdyenInputValidator;->inputFields:Ljava/util/HashMap;

    const/4 v0, 0x0

    .line 14
    iput-boolean v0, p0, Lcom/adyen/ui/utils/AdyenInputValidator;->allInputReady:Z

    return-void
.end method

.method static synthetic access$000(Lcom/adyen/ui/utils/AdyenInputValidator;)Lcom/adyen/ui/utils/AdyenInputValidator$OnReadyStateChangedListener;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/adyen/ui/utils/AdyenInputValidator;->onReadyStateChangedListener:Lcom/adyen/ui/utils/AdyenInputValidator$OnReadyStateChangedListener;

    return-object p0
.end method

.method private isAllInputReady()Z
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/adyen/ui/utils/AdyenInputValidator;->inputFields:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 32
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 33
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    and-int/2addr v1, v2

    goto :goto_0

    :cond_0
    return v1
.end method

.method private onReadyStateChanged(Z)V
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/adyen/ui/utils/AdyenInputValidator;->onReadyStateChangedListener:Lcom/adyen/ui/utils/AdyenInputValidator$OnReadyStateChangedListener;

    if-eqz v0, :cond_0

    .line 49
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/adyen/ui/utils/AdyenInputValidator$1;

    invoke-direct {v1, p0, p1}, Lcom/adyen/ui/utils/AdyenInputValidator$1;-><init>(Lcom/adyen/ui/utils/AdyenInputValidator;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private onReadyStateMaybeChanged()V
    .locals 2

    .line 39
    iget-boolean v0, p0, Lcom/adyen/ui/utils/AdyenInputValidator;->allInputReady:Z

    .line 40
    invoke-direct {p0}, Lcom/adyen/ui/utils/AdyenInputValidator;->isAllInputReady()Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 42
    iput-boolean v1, p0, Lcom/adyen/ui/utils/AdyenInputValidator;->allInputReady:Z

    .line 43
    invoke-direct {p0, v1}, Lcom/adyen/ui/utils/AdyenInputValidator;->onReadyStateChanged(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public addInputField(Landroid/view/View;)V
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/adyen/ui/utils/AdyenInputValidator;->inputFields:Ljava/util/HashMap;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setOnReadyStateChangedListener(Lcom/adyen/ui/utils/AdyenInputValidator$OnReadyStateChangedListener;)V
    .locals 0

    .line 59
    iput-object p1, p0, Lcom/adyen/ui/utils/AdyenInputValidator;->onReadyStateChangedListener:Lcom/adyen/ui/utils/AdyenInputValidator$OnReadyStateChangedListener;

    return-void
.end method

.method public setReady(Landroid/view/View;Z)V
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/adyen/ui/utils/AdyenInputValidator;->inputFields:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/adyen/ui/utils/AdyenInputValidator;->inputFields:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq v0, p2, :cond_0

    .line 20
    iget-object v0, p0, Lcom/adyen/ui/utils/AdyenInputValidator;->inputFields:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    invoke-direct {p0}, Lcom/adyen/ui/utils/AdyenInputValidator;->onReadyStateMaybeChanged()V

    :cond_0
    return-void
.end method
