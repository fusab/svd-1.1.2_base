.class Lcom/adyen/ui/fragments/GiropayFragment$2;
.super Ljava/lang/Object;
.source "GiropayFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/ui/fragments/GiropayFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/fragments/GiropayFragment;


# direct methods
.method constructor <init>(Lcom/adyen/ui/fragments/GiropayFragment;)V
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$2;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .line 156
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 157
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    const/4 v4, 0x3

    if-lt v0, v4, :cond_2

    .line 158
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$2;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/GiropayFragment;->access$400(Lcom/adyen/ui/fragments/GiropayFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 159
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$2;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/GiropayFragment;->access$500(Lcom/adyen/ui/fragments/GiropayFragment;)Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;

    move-result-object v0

    invoke-static {v0}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->access$600(Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;)I

    move-result v0

    if-nez v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$2;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/GiropayFragment;->access$700(Lcom/adyen/ui/fragments/GiropayFragment;)Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$2;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/GiropayFragment;->access$700(Lcom/adyen/ui/fragments/GiropayFragment;)Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->cancel(Z)Z

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$2;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    new-instance v1, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;

    const/4 v2, 0x0

    invoke-direct {v1, v0, p1, v2}, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;-><init>(Lcom/adyen/ui/fragments/GiropayFragment;Ljava/lang/String;Lcom/adyen/ui/fragments/GiropayFragment$1;)V

    invoke-static {v0, v1}, Lcom/adyen/ui/fragments/GiropayFragment;->access$702(Lcom/adyen/ui/fragments/GiropayFragment;Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;)Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;

    .line 164
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$2;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {p1}, Lcom/adyen/ui/fragments/GiropayFragment;->access$700(Lcom/adyen/ui/fragments/GiropayFragment;)Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;

    move-result-object p1

    new-array v0, v3, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$2;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/GiropayFragment;->access$500(Lcom/adyen/ui/fragments/GiropayFragment;)Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->access$900(Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;Ljava/lang/String;)V

    .line 167
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$2;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {p1}, Lcom/adyen/ui/fragments/GiropayFragment;->access$500(Lcom/adyen/ui/fragments/GiropayFragment;)Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 170
    :cond_2
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$2;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {p1}, Lcom/adyen/ui/fragments/GiropayFragment;->access$400(Lcom/adyen/ui/fragments/GiropayFragment;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 171
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$2;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {p1}, Lcom/adyen/ui/fragments/GiropayFragment;->access$1000(Lcom/adyen/ui/fragments/GiropayFragment;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 172
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$2;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {p1}, Lcom/adyen/ui/fragments/GiropayFragment;->access$700(Lcom/adyen/ui/fragments/GiropayFragment;)Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 173
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$2;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {p1}, Lcom/adyen/ui/fragments/GiropayFragment;->access$700(Lcom/adyen/ui/fragments/GiropayFragment;)Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->cancel(Z)Z

    .line 175
    :cond_3
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$2;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {p1}, Lcom/adyen/ui/fragments/GiropayFragment;->access$500(Lcom/adyen/ui/fragments/GiropayFragment;)Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;

    move-result-object p1

    invoke-static {p1}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->access$300(Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;)V

    .line 176
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$2;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {p1}, Lcom/adyen/ui/fragments/GiropayFragment;->access$500(Lcom/adyen/ui/fragments/GiropayFragment;)Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->notifyDataSetChanged()V

    :goto_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
