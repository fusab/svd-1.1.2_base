.class final Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;
.super Landroid/widget/BaseAdapter;
.source "GiropayFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/ui/fragments/GiropayFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AdyenUIGiroPayIssuersListAdapter"
.end annotation


# instance fields
.field private filteredIssuers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;",
            ">;"
        }
    .end annotation
.end field

.field private giroPayIssuers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;",
            ">;"
        }
    .end annotation
.end field

.field private layoutInflater:Landroid/view/LayoutInflater;

.field private searchStr:Ljava/lang/String;

.field final synthetic this$0:Lcom/adyen/ui/fragments/GiropayFragment;


# direct methods
.method private constructor <init>(Lcom/adyen/ui/fragments/GiropayFragment;Landroid/content/Context;)V
    .locals 0

    .line 238
    iput-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const-string p1, ""

    .line 231
    iput-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->searchStr:Ljava/lang/String;

    .line 233
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->giroPayIssuers:Ljava/util/ArrayList;

    .line 234
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->filteredIssuers:Ljava/util/ArrayList;

    const-string p1, "layout_inflater"

    .line 239
    invoke-virtual {p2, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    iput-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method synthetic constructor <init>(Lcom/adyen/ui/fragments/GiropayFragment;Landroid/content/Context;Lcom/adyen/ui/fragments/GiropayFragment$1;)V
    .locals 0

    .line 229
    invoke-direct {p0, p1, p2}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;-><init>(Lcom/adyen/ui/fragments/GiropayFragment;Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;Lorg/json/JSONArray;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 229
    invoke-direct {p0, p1, p2}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->setData(Lorg/json/JSONArray;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->clearData()V

    return-void
.end method

.method static synthetic access$600(Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;)I
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->getFullCount()I

    move-result p0

    return p0
.end method

.method static synthetic access$900(Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;Ljava/lang/String;)V
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->filter(Ljava/lang/String;)V

    return-void
.end method

.method private clearData()V
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->giroPayIssuers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 302
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->filteredIssuers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const-string v0, ""

    .line 303
    iput-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->searchStr:Ljava/lang/String;

    return-void
.end method

.method private filter(Ljava/lang/String;)V
    .locals 4

    .line 308
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->filteredIssuers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->searchStr:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_0

    goto :goto_0

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->filteredIssuers:Ljava/util/ArrayList;

    goto :goto_1

    .line 309
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->giroPayIssuers:Ljava/util/ArrayList;

    .line 313
    :goto_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 314
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;

    .line 315
    invoke-static {v2}, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;->access$100(Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 316
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 319
    :cond_3
    iput-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->searchStr:Ljava/lang/String;

    .line 320
    iput-object v1, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->filteredIssuers:Ljava/util/ArrayList;

    return-void
.end method

.method private getFullCount()I
    .locals 1

    .line 248
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->giroPayIssuers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method private setData(Lorg/json/JSONArray;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 293
    invoke-direct {p0}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->clearData()V

    const/4 v0, 0x0

    .line 294
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 295
    iget-object v1, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->giroPayIssuers:Ljava/util/ArrayList;

    new-instance v2, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;

    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;-><init>(Lorg/json/JSONObject;Lcom/adyen/ui/fragments/GiropayFragment$1;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 297
    :cond_0
    invoke-direct {p0, p2}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->filter(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 244
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->filteredIssuers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->filteredIssuers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/View;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 265
    iget-object p2, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    sget v0, Lcom/adyen/ui/R$layout;->giropay_list_item:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 267
    iget-object p3, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->filteredIssuers:Ljava/util/ArrayList;

    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;

    .line 268
    invoke-static {p1}, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;->access$100(Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;)Ljava/lang/String;

    move-result-object p3

    .line 270
    sget v0, Lcom/adyen/ui/R$id;->name_giropay_issuer:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 272
    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->searchStr:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 274
    iget-object v2, p0, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->searchStr:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v1

    .line 275
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, p3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 276
    new-instance p3, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {p3, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v4, 0x21

    invoke-virtual {v3, p3, v1, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 278
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 280
    :cond_0
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    :goto_0
    new-instance p3, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter$1;

    invoke-direct {p3, p0, p1}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter$1;-><init>(Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;)V

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p2
.end method
