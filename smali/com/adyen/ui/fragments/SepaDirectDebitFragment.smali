.class public Lcom/adyen/ui/fragments/SepaDirectDebitFragment;
.super Landroidx/fragment/app/Fragment;
.source "SepaDirectDebitFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/ui/fragments/SepaDirectDebitFragment$SEPADirectDebitPaymentDetailsListener;
    }
.end annotation


# instance fields
.field private amount:Lcom/adyen/core/models/Amount;

.field private paymentMethod:Lcom/adyen/core/models/PaymentMethod;

.field private sepaDirectDebitPaymentDetailsListener:Lcom/adyen/ui/fragments/SepaDirectDebitFragment$SEPADirectDebitPaymentDetailsListener;

.field private theme:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/adyen/ui/fragments/SepaDirectDebitFragment;)Lcom/adyen/ui/fragments/SepaDirectDebitFragment$SEPADirectDebitPaymentDetailsListener;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->sepaDirectDebitPaymentDetailsListener:Lcom/adyen/ui/fragments/SepaDirectDebitFragment$SEPADirectDebitPaymentDetailsListener;

    return-object p0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 70
    new-instance p3, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget v1, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->theme:I

    invoke-direct {p3, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 71
    invoke-virtual {p1, p3}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    .line 72
    sget p3, Lcom/adyen/ui/R$layout;->sepa_direct_debit_fragment:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 74
    new-instance p2, Lcom/adyen/ui/utils/AdyenInputValidator;

    invoke-direct {p2}, Lcom/adyen/ui/utils/AdyenInputValidator;-><init>()V

    .line 76
    sget p3, Lcom/adyen/ui/R$id;->adyen_sepa_iban_edit_text:I

    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Lcom/adyen/ui/views/IBANEditText;

    .line 77
    invoke-virtual {p3, p2}, Lcom/adyen/ui/views/IBANEditText;->setValidator(Lcom/adyen/ui/utils/AdyenInputValidator;)V

    .line 79
    sget v1, Lcom/adyen/ui/R$id;->adyen_bank_account_holder_name:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/adyen/ui/views/CardHolderEditText;

    .line 81
    invoke-virtual {v1, p2}, Lcom/adyen/ui/views/CardHolderEditText;->setValidator(Lcom/adyen/ui/utils/AdyenInputValidator;)V

    .line 83
    sget v1, Lcom/adyen/ui/R$id;->consent_direct_debit_checkbox:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/adyen/ui/views/CheckoutCheckBox;

    .line 84
    invoke-virtual {p2, v1}, Lcom/adyen/ui/utils/AdyenInputValidator;->addInputField(Landroid/view/View;)V

    .line 85
    new-instance v2, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$1;

    invoke-direct {v2, p0, p2, v1}, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$1;-><init>(Lcom/adyen/ui/fragments/SepaDirectDebitFragment;Lcom/adyen/ui/utils/AdyenInputValidator;Lcom/adyen/ui/views/CheckoutCheckBox;)V

    invoke-virtual {v1, v2}, Lcom/adyen/ui/views/CheckoutCheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 91
    sget v2, Lcom/adyen/ui/R$id;->layout_click_area_consent_checkbox:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$2;

    invoke-direct {v3, p0, v1}, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$2;-><init>(Lcom/adyen/ui/fragments/SepaDirectDebitFragment;Lcom/adyen/ui/views/CheckoutCheckBox;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    sget v1, Lcom/adyen/ui/R$id;->collect_direct_debit_data:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 101
    new-instance v2, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$3;

    invoke-direct {v2, p0, p3}, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$3;-><init>(Lcom/adyen/ui/fragments/SepaDirectDebitFragment;Lcom/adyen/ui/views/IBANEditText;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    new-instance p3, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$4;

    invoke-direct {p3, p0, v1}, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$4;-><init>(Lcom/adyen/ui/fragments/SepaDirectDebitFragment;Landroid/widget/Button;)V

    invoke-virtual {p2, p3}, Lcom/adyen/ui/utils/AdyenInputValidator;->setOnReadyStateChangedListener(Lcom/adyen/ui/utils/AdyenInputValidator$OnReadyStateChangedListener;)V

    .line 114
    sget p2, Lcom/adyen/ui/R$id;->amount_text_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 115
    iget-object p3, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->amount:Lcom/adyen/core/models/Amount;

    invoke-virtual {p0}, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/adyen/core/utils/StringUtils;->getLocale(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p3, v2, v1}, Lcom/adyen/core/utils/AmountUtil;->format(Lcom/adyen/core/models/Amount;ZLjava/util/Locale;)Ljava/lang/String;

    move-result-object p3

    .line 116
    sget v1, Lcom/adyen/ui/R$string;->payButton_formatted:I

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p3, v2, v0

    invoke-virtual {p0, v1, v2}, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    .line 117
    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    instance-of p2, p2, Lcom/adyen/ui/activities/CheckoutActivity;

    if-eqz p2, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    check-cast p2, Lcom/adyen/ui/activities/CheckoutActivity;

    iget-object p3, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {p3}, Lcom/adyen/core/models/PaymentMethod;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/adyen/ui/activities/CheckoutActivity;->setActionBarTitle(Ljava/lang/String;)V

    :cond_0
    return-object p1
.end method

.method public setArguments(Landroid/os/Bundle;)V
    .locals 1

    .line 55
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    const-string v0, "PaymentMethod"

    .line 57
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/adyen/core/models/PaymentMethod;

    iput-object v0, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    const-string v0, "amount"

    .line 59
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/adyen/core/models/Amount;

    iput-object v0, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->amount:Lcom/adyen/core/models/Amount;

    const-string v0, "theme"

    .line 61
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->theme:I

    return-void
.end method

.method setSEPADirectDebitPaymentDetailsListener(Lcom/adyen/ui/fragments/SepaDirectDebitFragment$SEPADirectDebitPaymentDetailsListener;)V
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->sepaDirectDebitPaymentDetailsListener:Lcom/adyen/ui/fragments/SepaDirectDebitFragment$SEPADirectDebitPaymentDetailsListener;

    return-void
.end method
