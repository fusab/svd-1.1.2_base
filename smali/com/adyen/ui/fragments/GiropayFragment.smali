.class public final Lcom/adyen/ui/fragments/GiropayFragment;
.super Landroidx/fragment/app/Fragment;
.source "GiropayFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;,
        Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;,
        Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;
    }
.end annotation


# static fields
.field private static final MIN_LENGTH_TO_LOOKUP:I = 0x3

.field private static final SEARCH_ENDPOINT:Ljava/lang/String; = "https://live.adyen.com/hpp/getGiroPayBankList.shtml?searchStr="


# instance fields
.field private amount:Lcom/adyen/core/models/Amount;

.field private editText:Lcom/adyen/ui/views/GiroPayEditText;

.field private exampleView:Landroid/view/View;

.field private giroPayTextWatcher:Landroid/text/TextWatcher;

.field private issuerListAdapter:Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;

.field private listView:Landroid/widget/ListView;

.field private loadingView:Landroid/view/View;

.field private lookupAsyncTask:Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;

.field private payButton:Landroid/widget/Button;

.field private paymentMethod:Lcom/adyen/core/models/PaymentMethod;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 44
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    const/4 v0, 0x0

    .line 57
    iput-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->lookupAsyncTask:Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;

    .line 143
    new-instance v0, Lcom/adyen/ui/fragments/GiropayFragment$2;

    invoke-direct {v0, p0}, Lcom/adyen/ui/fragments/GiropayFragment$2;-><init>(Lcom/adyen/ui/fragments/GiropayFragment;)V

    iput-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->giroPayTextWatcher:Landroid/text/TextWatcher;

    return-void
.end method

.method static synthetic access$1000(Lcom/adyen/ui/fragments/GiropayFragment;)Landroid/view/View;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->loadingView:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/adyen/ui/fragments/GiropayFragment;Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lcom/adyen/ui/fragments/GiropayFragment;->selectedBank(Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;)V

    return-void
.end method

.method static synthetic access$200(Lcom/adyen/ui/fragments/GiropayFragment;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/adyen/ui/fragments/GiropayFragment;->unSelectedBank()V

    return-void
.end method

.method static synthetic access$400(Lcom/adyen/ui/fragments/GiropayFragment;)Landroid/view/View;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->exampleView:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$500(Lcom/adyen/ui/fragments/GiropayFragment;)Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->issuerListAdapter:Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;

    return-object p0
.end method

.method static synthetic access$700(Lcom/adyen/ui/fragments/GiropayFragment;)Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->lookupAsyncTask:Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;

    return-object p0
.end method

.method static synthetic access$702(Lcom/adyen/ui/fragments/GiropayFragment;Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;)Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment;->lookupAsyncTask:Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;

    return-object p1
.end method

.method private selectedBank(Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;)V
    .locals 3

    .line 106
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    iget-object v1, p0, Lcom/adyen/ui/fragments/GiropayFragment;->giroPayTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/adyen/ui/views/GiroPayEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 107
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    invoke-static {p1}, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;->access$100(Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/adyen/ui/views/GiroPayEditText;->setText(Ljava/lang/CharSequence;)V

    .line 108
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    const/4 v0, 0x2

    const/high16 v1, 0x41400000    # 12.0f

    invoke-virtual {p1, v0, v1}, Lcom/adyen/ui/views/GiroPayEditText;->setTextSize(IF)V

    .line 109
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/adyen/ui/views/GiroPayEditText;->setSingleLine(Z)V

    .line 110
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    invoke-virtual {p1}, Lcom/adyen/ui/views/GiroPayEditText;->clearFocus()V

    .line 111
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    invoke-virtual {p1, v1}, Lcom/adyen/ui/views/GiroPayEditText;->setEnabled(Z)V

    .line 112
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    invoke-virtual {p0}, Lcom/adyen/ui/fragments/GiropayFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/adyen/ui/R$drawable;->clear_icon:I

    invoke-static {v1, v2}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    new-instance v2, Lcom/adyen/ui/fragments/GiropayFragment$1;

    invoke-direct {v2, p0}, Lcom/adyen/ui/fragments/GiropayFragment$1;-><init>(Lcom/adyen/ui/fragments/GiropayFragment;)V

    invoke-virtual {p1, v1, v2}, Lcom/adyen/ui/views/GiroPayEditText;->setCancelDrawable(Landroid/graphics/drawable/Drawable;Lcom/adyen/ui/views/GiroPayEditText$OnDrawableClickListener;)V

    .line 120
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment;->issuerListAdapter:Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;

    invoke-static {p1}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->access$300(Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;)V

    .line 121
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment;->issuerListAdapter:Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;

    invoke-virtual {p1}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->notifyDataSetChanged()V

    .line 123
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/GiropayFragment;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "input_method"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/inputmethod/InputMethodManager;

    .line 124
    iget-object v1, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    invoke-virtual {v1}, Lcom/adyen/ui/views/GiroPayEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 126
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment;->payButton:Landroid/widget/Button;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method private unSelectedBank()V
    .locals 4

    .line 130
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/adyen/ui/views/GiroPayEditText;->setEnabled(Z)V

    .line 131
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    const-string v2, ""

    invoke-virtual {v0, v2}, Lcom/adyen/ui/views/GiroPayEditText;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    const/4 v2, 0x2

    const/high16 v3, 0x41900000    # 18.0f

    invoke-virtual {v0, v2, v3}, Lcom/adyen/ui/views/GiroPayEditText;->setTextSize(IF)V

    .line 133
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    invoke-virtual {v0, v1}, Lcom/adyen/ui/views/GiroPayEditText;->setSingleLine(Z)V

    .line 134
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    iget-object v1, p0, Lcom/adyen/ui/fragments/GiropayFragment;->giroPayTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/adyen/ui/views/GiroPayEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 135
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    invoke-virtual {v0}, Lcom/adyen/ui/views/GiroPayEditText;->requestFocus()Z

    .line 136
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1, v1, v1}, Lcom/adyen/ui/views/GiroPayEditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 138
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->payButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 140
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->exampleView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 73
    sget p3, Lcom/adyen/ui/R$layout;->giropay_fragment:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 74
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/GiropayFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    invoke-virtual {p2}, Landroidx/fragment/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object p2

    const/16 p3, 0x20

    invoke-virtual {p2, p3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 76
    new-instance p2, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;

    invoke-virtual {p0}, Lcom/adyen/ui/fragments/GiropayFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p3

    const/4 v1, 0x0

    invoke-direct {p2, p0, p3, v1}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;-><init>(Lcom/adyen/ui/fragments/GiropayFragment;Landroid/content/Context;Lcom/adyen/ui/fragments/GiropayFragment$1;)V

    iput-object p2, p0, Lcom/adyen/ui/fragments/GiropayFragment;->issuerListAdapter:Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;

    .line 77
    sget p2, Lcom/adyen/ui/R$id;->giropay_list_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ListView;

    iput-object p2, p0, Lcom/adyen/ui/fragments/GiropayFragment;->listView:Landroid/widget/ListView;

    .line 78
    iget-object p2, p0, Lcom/adyen/ui/fragments/GiropayFragment;->listView:Landroid/widget/ListView;

    iget-object p3, p0, Lcom/adyen/ui/fragments/GiropayFragment;->issuerListAdapter:Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;

    invoke-virtual {p2, p3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 80
    sget p2, Lcom/adyen/ui/R$id;->adyen_giropay_lookup_edit_text:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/adyen/ui/views/GiroPayEditText;

    iput-object p2, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    .line 81
    iget-object p2, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    iget-object p3, p0, Lcom/adyen/ui/fragments/GiropayFragment;->giroPayTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {p2, p3}, Lcom/adyen/ui/views/GiroPayEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 83
    sget p2, Lcom/adyen/ui/R$id;->amount_text_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 84
    iget-object p3, p0, Lcom/adyen/ui/fragments/GiropayFragment;->amount:Lcom/adyen/core/models/Amount;

    invoke-virtual {p0}, Lcom/adyen/ui/fragments/GiropayFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/adyen/core/utils/StringUtils;->getLocale(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p3, v2, v1}, Lcom/adyen/core/utils/AmountUtil;->format(Lcom/adyen/core/models/Amount;ZLjava/util/Locale;)Ljava/lang/String;

    move-result-object p3

    .line 85
    sget v1, Lcom/adyen/ui/R$string;->payButton_formatted:I

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p3, v2, v0

    invoke-virtual {p0, v1, v2}, Lcom/adyen/ui/fragments/GiropayFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    .line 86
    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    sget p2, Lcom/adyen/ui/R$id;->loading_icon_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/adyen/ui/fragments/GiropayFragment;->loadingView:Landroid/view/View;

    .line 89
    sget p2, Lcom/adyen/ui/R$id;->pay_giropay_button:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    iput-object p2, p0, Lcom/adyen/ui/fragments/GiropayFragment;->payButton:Landroid/widget/Button;

    .line 90
    sget p2, Lcom/adyen/ui/R$id;->giropay_example_layout:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/adyen/ui/fragments/GiropayFragment;->exampleView:Landroid/view/View;

    .line 92
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/GiropayFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    instance-of p2, p2, Lcom/adyen/ui/activities/CheckoutActivity;

    if-eqz p2, :cond_1

    .line 93
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/GiropayFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    check-cast p2, Lcom/adyen/ui/activities/CheckoutActivity;

    iget-object p3, p0, Lcom/adyen/ui/fragments/GiropayFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/adyen/core/models/PaymentMethod;->getName()Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    :cond_0
    const-string p3, ""

    :goto_0
    invoke-virtual {p2, p3}, Lcom/adyen/ui/activities/CheckoutActivity;->setActionBarTitle(Ljava/lang/String;)V

    :cond_1
    return-object p1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 100
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    invoke-virtual {p1}, Lcom/adyen/ui/views/GiroPayEditText;->requestFocus()Z

    .line 101
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/GiropayFragment;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "input_method"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/inputmethod/InputMethodManager;

    .line 102
    iget-object p2, p0, Lcom/adyen/ui/fragments/GiropayFragment;->editText:Lcom/adyen/ui/views/GiroPayEditText;

    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    return-void
.end method

.method public setArguments(Landroid/os/Bundle;)V
    .locals 1

    .line 64
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    const-string v0, "amount"

    .line 65
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/adyen/core/models/Amount;

    iput-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment;->amount:Lcom/adyen/core/models/Amount;

    const-string v0, "PaymentMethod"

    .line 66
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/adyen/core/models/PaymentMethod;

    iput-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    return-void
.end method
