.class final Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;
.super Ljava/lang/Object;
.source "GiropayFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/ui/fragments/GiropayFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "GiroPayIssuer"
.end annotation


# instance fields
.field private bankName:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lorg/json/JSONObject;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "bankName"

    .line 330
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;->bankName:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lorg/json/JSONObject;Lcom/adyen/ui/fragments/GiropayFragment$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 324
    invoke-direct {p0, p1}, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method static synthetic access$100(Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;)Ljava/lang/String;
    .locals 0

    .line 324
    invoke-direct {p0}, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;->getBankName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private getBankName()Ljava/lang/String;
    .locals 1

    .line 336
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayIssuer;->bankName:Ljava/lang/String;

    return-object v0
.end method
