.class Lcom/adyen/ui/fragments/SepaDirectDebitFragment$1;
.super Ljava/lang/Object;
.source "SepaDirectDebitFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/fragments/SepaDirectDebitFragment;

.field final synthetic val$consentCheckbox:Lcom/adyen/ui/views/CheckoutCheckBox;

.field final synthetic val$validator:Lcom/adyen/ui/utils/AdyenInputValidator;


# direct methods
.method constructor <init>(Lcom/adyen/ui/fragments/SepaDirectDebitFragment;Lcom/adyen/ui/utils/AdyenInputValidator;Lcom/adyen/ui/views/CheckoutCheckBox;)V
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$1;->this$0:Lcom/adyen/ui/fragments/SepaDirectDebitFragment;

    iput-object p2, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$1;->val$validator:Lcom/adyen/ui/utils/AdyenInputValidator;

    iput-object p3, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$1;->val$consentCheckbox:Lcom/adyen/ui/views/CheckoutCheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .line 88
    iget-object p1, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$1;->val$validator:Lcom/adyen/ui/utils/AdyenInputValidator;

    iget-object v0, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$1;->val$consentCheckbox:Lcom/adyen/ui/views/CheckoutCheckBox;

    invoke-virtual {p1, v0, p2}, Lcom/adyen/ui/utils/AdyenInputValidator;->setReady(Landroid/view/View;Z)V

    return-void
.end method
