.class public Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;
.super Landroidx/fragment/app/Fragment;
.source "PaymentMethodSelectionFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$OnPreferredPaymentMethodClick;,
        Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$OnPaymentMethodClick;,
        Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$PaymentMethodSelectionListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "PaymentMethodSelectionFragment"


# instance fields
.field private paymentMethodSelectionListener:Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$PaymentMethodSelectionListener;

.field private paymentMethods:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;"
        }
    .end annotation
.end field

.field private preferredPaymentMethods:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;"
        }
    .end annotation
.end field

.field private preferredPaymentMethodsLayout:Landroid/view/View;

.field private theme:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 45
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->paymentMethods:Ljava/util/ArrayList;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->preferredPaymentMethods:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$000(Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;Landroid/widget/ListView;Z)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->resizeListView(Landroid/widget/ListView;Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;IZ)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->handleOnPaymentMethodClick(IZ)V

    return-void
.end method

.method private handleOnPaymentMethodClick(IZ)V
    .locals 0

    if-eqz p2, :cond_0

    .line 157
    iget-object p2, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->preferredPaymentMethods:Ljava/util/ArrayList;

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->paymentMethods:Ljava/util/ArrayList;

    :goto_0
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/adyen/core/models/PaymentMethod;

    .line 158
    iget-object p2, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->paymentMethodSelectionListener:Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$PaymentMethodSelectionListener;

    invoke-interface {p2, p1}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$PaymentMethodSelectionListener;->onPaymentMethodSelected(Lcom/adyen/core/models/PaymentMethod;)V

    return-void
.end method

.method private resizeListView(Landroid/widget/ListView;Z)V
    .locals 3

    .line 140
    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 141
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    const/4 v1, 0x0

    .line 145
    invoke-virtual {p1, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    .line 149
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 151
    invoke-virtual {p1}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz p2, :cond_1

    mul-int v0, v0, v1

    .line 152
    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    goto :goto_0

    :cond_1
    mul-int v0, v0, v1

    :goto_0
    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 153
    invoke-virtual {p1, v2}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .line 77
    sget-object p3, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->TAG:Ljava/lang/String;

    const-string v0, "onCreateView()"

    invoke-static {p3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    new-instance p3, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget v1, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->theme:I

    invoke-direct {p3, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 81
    invoke-virtual {p1, p3}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    .line 82
    sget p3, Lcom/adyen/ui/R$layout;->payment_method_selection_fragment:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 84
    new-instance p2, Lcom/adyen/ui/adapters/PaymentListAdapter;

    .line 85
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p3

    iget-object v1, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->paymentMethods:Ljava/util/ArrayList;

    invoke-direct {p2, p3, v1}, Lcom/adyen/ui/adapters/PaymentListAdapter;-><init>(Landroid/app/Activity;Ljava/util/List;)V

    const p3, 0x102000a

    .line 86
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ListView;

    .line 87
    invoke-virtual {p3, p2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 89
    new-instance p2, Lcom/adyen/ui/adapters/PaymentListAdapter;

    .line 90
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->preferredPaymentMethods:Ljava/util/ArrayList;

    invoke-direct {p2, v1, v2}, Lcom/adyen/ui/adapters/PaymentListAdapter;-><init>(Landroid/app/Activity;Ljava/util/List;)V

    .line 91
    sget v1, Lcom/adyen/ui/R$id;->preferred_payment_methods_list:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 92
    invoke-virtual {v1, p2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 94
    new-instance p2, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$OnPaymentMethodClick;

    invoke-direct {p2, p0}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$OnPaymentMethodClick;-><init>(Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;)V

    invoke-virtual {p3, p2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 95
    new-instance p2, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$OnPreferredPaymentMethodClick;

    invoke-direct {p2, p0}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$OnPreferredPaymentMethodClick;-><init>(Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;)V

    invoke-virtual {v1, p2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 97
    invoke-virtual {p3}, Landroid/widget/ListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p2

    .line 98
    new-instance v2, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$1;

    invoke-direct {v2, p0, p3}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$1;-><init>(Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;Landroid/widget/ListView;)V

    invoke-virtual {p2, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 106
    invoke-virtual {v1}, Landroid/widget/ListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p2

    .line 107
    new-instance p3, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$2;

    invoke-direct {p3, p0, v1}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$2;-><init>(Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;Landroid/widget/ListView;)V

    invoke-virtual {p2, p3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 115
    sget p2, Lcom/adyen/ui/R$id;->preferred_payment_methods_layout:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->preferredPaymentMethodsLayout:Landroid/view/View;

    .line 126
    iget-object p2, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->preferredPaymentMethods:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 127
    iget-object p2, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->preferredPaymentMethodsLayout:Landroid/view/View;

    const/16 p3, 0x8

    invoke-virtual {p2, p3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 129
    :cond_0
    iget-object p2, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->preferredPaymentMethodsLayout:Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 132
    :goto_0
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    instance-of p2, p2, Lcom/adyen/ui/activities/CheckoutActivity;

    if-eqz p2, :cond_1

    .line 133
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    check-cast p2, Lcom/adyen/ui/activities/CheckoutActivity;

    sget p3, Lcom/adyen/ui/R$string;->paymentMethods_title:I

    invoke-virtual {p2, p3}, Lcom/adyen/ui/activities/CheckoutActivity;->setActionBarTitle(I)V

    :cond_1
    return-object p1
.end method

.method public setArguments(Landroid/os/Bundle;)V
    .locals 2

    .line 63
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    const-string v0, "theme"

    .line 65
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->theme:I

    const-string v0, "filteredPaymentMethods"

    .line 67
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->paymentMethods:Ljava/util/ArrayList;

    const-string v0, "preferredPaymentMethods"

    .line 68
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 69
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->preferredPaymentMethods:Ljava/util/ArrayList;

    :cond_0
    return-void
.end method

.method setPaymentMethodSelectionListener(Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$PaymentMethodSelectionListener;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->paymentMethodSelectionListener:Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$PaymentMethodSelectionListener;

    return-void
.end method
