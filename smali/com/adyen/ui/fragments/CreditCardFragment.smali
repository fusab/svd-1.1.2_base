.class public Lcom/adyen/ui/fragments/CreditCardFragment;
.super Landroidx/fragment/app/Fragment;
.source "CreditCardFragment.java"

# interfaces
.implements Lcom/adyen/ui/views/CreditCardEditText$CVCFieldStatusListener;
.implements Lcom/adyen/cardscan/PaymentCardScanner$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "CreditCardFragment"

.field private static final TAG_LOADING_SCREEN_FRAGMENT:Ljava/lang/String;


# instance fields
.field private amount:Lcom/adyen/core/models/Amount;

.field private cardHolderEditText:Lcom/adyen/ui/views/CardHolderEditText;

.field private creditCardInfoListener:Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;

.field private creditCardNoView:Lcom/adyen/ui/views/CreditCardEditText;

.field private cvcFieldStatus:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

.field private cvcLayout:Landroid/widget/LinearLayout;

.field private cvcView:Lcom/adyen/ui/views/CVCEditText;

.field private expiryDateView:Lcom/adyen/ui/views/ExpiryDateEditText;

.field private generationTime:Ljava/lang/String;

.field private installmentsSpinner:Landroid/widget/Spinner;

.field private nameRequired:Z

.field private oneClick:Z

.field private paymentCardScanners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/adyen/cardscan/PaymentCardScanner;",
            ">;"
        }
    .end annotation
.end field

.field private paymentMethod:Lcom/adyen/core/models/PaymentMethod;

.field private publicKey:Ljava/lang/String;

.field private saveCardCheckBox:Lcom/adyen/ui/views/CheckoutCheckBox;

.field private scanCardButton:Landroid/widget/ImageButton;

.field private shopperReference:Ljava/lang/String;

.field private storeDetailsOptionAvailable:Z

.field private theme:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 71
    const-class v0, Lcom/adyen/ui/fragments/LoadingScreenFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/adyen/ui/fragments/CreditCardFragment;->TAG_LOADING_SCREEN_FRAGMENT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 101
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    .line 92
    sget-object v0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->REQUIRED:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    iput-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcFieldStatus:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    return-void
.end method

.method static synthetic access$000(Lcom/adyen/ui/fragments/CreditCardFragment;)Landroid/widget/ImageButton;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->scanCardButton:Landroid/widget/ImageButton;

    return-object p0
.end method

.method static synthetic access$100(Lcom/adyen/ui/fragments/CreditCardFragment;)Lcom/adyen/ui/views/CheckoutCheckBox;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->saveCardCheckBox:Lcom/adyen/ui/views/CheckoutCheckBox;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/adyen/ui/fragments/CreditCardFragment;)Lcom/adyen/ui/views/CardHolderEditText;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cardHolderEditText:Lcom/adyen/ui/views/CardHolderEditText;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/adyen/ui/fragments/CreditCardFragment;)Lcom/adyen/ui/views/ExpiryDateEditText;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->expiryDateView:Lcom/adyen/ui/views/ExpiryDateEditText;

    return-object p0
.end method

.method static synthetic access$200(Lcom/adyen/ui/fragments/CreditCardFragment;)Ljava/lang/String;
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/adyen/ui/fragments/CreditCardFragment;->getToken()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lcom/adyen/ui/fragments/CreditCardFragment;)Ljava/lang/String;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->shopperReference:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lcom/adyen/ui/fragments/CreditCardFragment;)Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->creditCardInfoListener:Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;

    return-object p0
.end method

.method static synthetic access$500(Lcom/adyen/ui/fragments/CreditCardFragment;)Landroid/widget/Spinner;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->installmentsSpinner:Landroid/widget/Spinner;

    return-object p0
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .line 68
    sget-object v0, Lcom/adyen/ui/fragments/CreditCardFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/adyen/ui/fragments/CreditCardFragment;)Lcom/adyen/ui/views/CVCEditText;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcView:Lcom/adyen/ui/views/CVCEditText;

    return-object p0
.end method

.method static synthetic access$800(Lcom/adyen/ui/fragments/CreditCardFragment;)Lcom/adyen/ui/views/CreditCardEditText;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->creditCardNoView:Lcom/adyen/ui/views/CreditCardEditText;

    return-object p0
.end method

.method static synthetic access$900(Lcom/adyen/ui/fragments/CreditCardFragment;)Z
    .locals 0

    .line 68
    iget-boolean p0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->nameRequired:Z

    return p0
.end method

.method private getAllowedCardTypes()Ljava/util/Map;
    .locals 5
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;",
            ">;"
        }
    .end annotation

    .line 377
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 378
    iget-object v1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {v1}, Lcom/adyen/core/models/PaymentMethod;->getMemberPaymentMethods()Ljava/util/List;

    move-result-object v1

    const-string v2, "true"

    if-eqz v1, :cond_2

    .line 380
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/adyen/core/models/PaymentMethod;

    .line 381
    invoke-virtual {v3}, Lcom/adyen/core/models/PaymentMethod;->getInputDetails()Ljava/util/Collection;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/adyen/core/models/PaymentMethod;->getConfiguration()Lcom/adyen/core/models/PaymentMethod$Configuration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/adyen/core/models/PaymentMethod$Configuration;->getNoCVC()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 382
    invoke-virtual {v3}, Lcom/adyen/core/models/PaymentMethod;->getType()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->NOCVC:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 383
    :cond_0
    invoke-virtual {v3}, Lcom/adyen/core/models/PaymentMethod;->getConfiguration()Lcom/adyen/core/models/PaymentMethod$Configuration;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/adyen/core/models/PaymentMethod;->getConfiguration()Lcom/adyen/core/models/PaymentMethod$Configuration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/adyen/core/models/PaymentMethod$Configuration;->getCvcOptional()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 384
    invoke-virtual {v3}, Lcom/adyen/core/models/PaymentMethod;->getType()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->OPTIONAL:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 386
    :cond_1
    invoke-virtual {v3}, Lcom/adyen/core/models/PaymentMethod;->getType()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->REQUIRED:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 390
    :cond_2
    iget-object v1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {v1}, Lcom/adyen/core/models/PaymentMethod;->getConfiguration()Lcom/adyen/core/models/PaymentMethod$Configuration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adyen/core/models/PaymentMethod$Configuration;->getNoCVC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 391
    iget-object v1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {v1}, Lcom/adyen/core/models/PaymentMethod;->getType()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->NOCVC:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 392
    :cond_3
    iget-object v1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {v1}, Lcom/adyen/core/models/PaymentMethod;->getConfiguration()Lcom/adyen/core/models/PaymentMethod$Configuration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adyen/core/models/PaymentMethod$Configuration;->getCvcOptional()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 393
    iget-object v1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {v1}, Lcom/adyen/core/models/PaymentMethod;->getType()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->OPTIONAL:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 395
    :cond_4
    iget-object v1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {v1}, Lcom/adyen/core/models/PaymentMethod;->getType()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->REQUIRED:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    :goto_1
    return-object v0
.end method

.method private getToken()Ljava/lang/String;
    .locals 4

    .line 420
    invoke-direct {p0}, Lcom/adyen/ui/fragments/CreditCardFragment;->inputFieldsAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 423
    :cond_0
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->publicKey:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_1

    .line 424
    sget-object v0, Lcom/adyen/ui/fragments/CreditCardFragment;->TAG:Ljava/lang/String;

    const-string v2, "Public key is not available; credit card payment cannot be handled."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    .line 427
    :cond_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 430
    :try_start_0
    iget-boolean v2, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->nameRequired:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ladyen/com/adyencse/encrypter/exception/EncrypterException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "holderName"

    if-eqz v2, :cond_2

    .line 431
    :try_start_1
    iget-object v2, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cardHolderEditText:Lcom/adyen/ui/views/CardHolderEditText;

    invoke-virtual {v2}, Lcom/adyen/ui/views/CardHolderEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    :cond_2
    const-string v2, "Checkout Shopper Placeholder"

    .line 433
    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :goto_0
    const-string v2, "number"

    .line 435
    iget-object v3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->creditCardNoView:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-virtual {v3}, Lcom/adyen/ui/views/CreditCardEditText;->getCCNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "expiryMonth"

    .line 437
    iget-object v3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->expiryDateView:Lcom/adyen/ui/views/ExpiryDateEditText;

    invoke-virtual {v3}, Lcom/adyen/ui/views/ExpiryDateEditText;->getMonth()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "expiryYear"

    .line 438
    iget-object v3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->expiryDateView:Lcom/adyen/ui/views/ExpiryDateEditText;

    invoke-virtual {v3}, Lcom/adyen/ui/views/ExpiryDateEditText;->getFullYear()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "generationtime"

    .line 439
    iget-object v3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->generationTime:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "cvc"

    .line 440
    iget-object v3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcView:Lcom/adyen/ui/views/CVCEditText;

    invoke-virtual {v3}, Lcom/adyen/ui/views/CVCEditText;->getCVC()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 441
    new-instance v2, Ladyen/com/adyencse/encrypter/ClientSideEncrypter;

    iget-object v3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->publicKey:Ljava/lang/String;

    invoke-direct {v2, v3}, Ladyen/com/adyencse/encrypter/ClientSideEncrypter;-><init>(Ljava/lang/String;)V

    .line 442
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ladyen/com/adyencse/encrypter/ClientSideEncrypter;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ladyen/com/adyencse/encrypter/exception/EncrypterException; {:try_start_1 .. :try_end_1} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 446
    sget-object v2, Lcom/adyen/ui/fragments/CreditCardFragment;->TAG:Ljava/lang/String;

    const-string v3, "EncrypterException occurred while generating token."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_1
    move-exception v0

    .line 444
    sget-object v2, Lcom/adyen/ui/fragments/CreditCardFragment;->TAG:Ljava/lang/String;

    const-string v3, "JSON Exception occurred while generating token."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    return-object v1
.end method

.method private inputFieldsAvailable()Z
    .locals 1

    .line 452
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->creditCardNoView:Lcom/adyen/ui/views/CreditCardEditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->expiryDateView:Lcom/adyen/ui/views/ExpiryDateEditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcView:Lcom/adyen/ui/views/CVCEditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cardHolderEditText:Lcom/adyen/ui/views/CardHolderEditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->saveCardCheckBox:Lcom/adyen/ui/views/CheckoutCheckBox;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .line 174
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 176
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentCardScanners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/cardscan/PaymentCardScanner;

    .line 177
    invoke-virtual {v1, p1, p2, p3}, Lcom/adyen/cardscan/PaymentCardScanner;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onCVCFieldStatusChanged(Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;)V
    .locals 2

    .line 473
    iput-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcFieldStatus:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    .line 475
    sget-object v0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->NOCVC:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    if-ne p1, v0, :cond_0

    .line 476
    iget-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcLayout:Landroid/widget/LinearLayout;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 478
    :cond_0
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 479
    sget-object v0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->OPTIONAL:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    if-ne p1, v0, :cond_1

    .line 480
    iget-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcView:Lcom/adyen/ui/views/CVCEditText;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/adyen/ui/views/CVCEditText;->setOptional(Z)V

    goto :goto_0

    .line 482
    :cond_1
    iget-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcView:Lcom/adyen/ui/views/CVCEditText;

    invoke-virtual {p1, v1}, Lcom/adyen/ui/views/CVCEditText;->setOptional(Z)V

    :goto_0
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 222
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    if-ltz v0, :cond_0

    .line 224
    iget-object v1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentCardScanners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 225
    iget-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentCardScanners:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/adyen/cardscan/PaymentCardScanner;

    .line 226
    invoke-virtual {p1}, Lcom/adyen/cardscan/PaymentCardScanner;->startScan()V

    const/4 p1, 0x1

    return p1

    .line 229
    :cond_0
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 183
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 185
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentCardScanners:Ljava/util/List;

    .line 187
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/CreditCardFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "payment_card_scan_enabled"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 188
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/CreditCardFragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/adyen/cardscan/PaymentCardScannerFactory$Loader;->getPaymentCardScannerFactory(Landroid/content/Context;)Lcom/adyen/cardscan/PaymentCardScannerFactory;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 191
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentCardScanners:Ljava/util/List;

    invoke-virtual {p0}, Lcom/adyen/ui/fragments/CreditCardFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/adyen/cardscan/PaymentCardScannerFactory;->getPaymentCardScanners(Landroid/app/Activity;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 194
    :cond_0
    iget-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentCardScanners:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/adyen/cardscan/PaymentCardScanner;

    .line 195
    invoke-virtual {v0, p0}, Lcom/adyen/cardscan/PaymentCardScanner;->setListener(Lcom/adyen/cardscan/PaymentCardScanner$Listener;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2

    .line 203
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 205
    iget-object p3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->scanCardButton:Landroid/widget/ImageButton;

    if-ne p2, p3, :cond_1

    .line 206
    iget-object p2, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentCardScanners:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const/4 p3, 0x1

    const/4 v0, 0x0

    if-ne p2, p3, :cond_0

    .line 209
    iget-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentCardScanners:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/adyen/cardscan/PaymentCardScanner;

    .line 210
    invoke-virtual {p1}, Lcom/adyen/cardscan/PaymentCardScanner;->startScan()V

    goto :goto_1

    :cond_0
    if-le p2, p3, :cond_1

    const/4 p3, 0x0

    :goto_0
    if-ge p3, p2, :cond_1

    .line 213
    iget-object v1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentCardScanners:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/cardscan/PaymentCardScanner;

    .line 214
    invoke-virtual {v1}, Lcom/adyen/cardscan/PaymentCardScanner;->getDisplayDescription()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, p3, v0, v1}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .line 238
    new-instance p3, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/adyen/ui/fragments/CreditCardFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget v1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->theme:I

    invoke-direct {p3, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 239
    invoke-virtual {p1, p3}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    .line 240
    sget p3, Lcom/adyen/ui/R$layout;->credit_card_fragment:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 242
    sget p2, Lcom/adyen/ui/R$id;->adyen_credit_card_no:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/adyen/ui/views/CreditCardEditText;

    iput-object p2, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->creditCardNoView:Lcom/adyen/ui/views/CreditCardEditText;

    .line 243
    sget p2, Lcom/adyen/ui/R$id;->scan_card_button:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ImageButton;

    iput-object p2, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->scanCardButton:Landroid/widget/ImageButton;

    .line 244
    iget-object p2, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->scanCardButton:Landroid/widget/ImageButton;

    new-instance p3, Lcom/adyen/ui/fragments/CreditCardFragment$1;

    invoke-direct {p3, p0}, Lcom/adyen/ui/fragments/CreditCardFragment$1;-><init>(Lcom/adyen/ui/fragments/CreditCardFragment;)V

    invoke-virtual {p2, p3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    iget-object p2, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentCardScanners:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const/4 p3, 0x1

    if-ne p2, p3, :cond_0

    .line 251
    iget-object p2, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->scanCardButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentCardScanners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/cardscan/PaymentCardScanner;

    invoke-virtual {v1}, Lcom/adyen/cardscan/PaymentCardScanner;->getDisplayIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 253
    :cond_0
    iget-object p2, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->scanCardButton:Landroid/widget/ImageButton;

    invoke-virtual {p0, p2}, Lcom/adyen/ui/fragments/CreditCardFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 254
    iget-object p2, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->scanCardButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentCardScanners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    const/16 v2, 0x8

    if-eqz v1, :cond_1

    const/16 v1, 0x8

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p2, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 255
    sget p2, Lcom/adyen/ui/R$id;->adyen_credit_card_exp_date:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/adyen/ui/views/ExpiryDateEditText;

    iput-object p2, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->expiryDateView:Lcom/adyen/ui/views/ExpiryDateEditText;

    .line 256
    sget p2, Lcom/adyen/ui/R$id;->adyen_credit_card_cvc:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/adyen/ui/views/CVCEditText;

    iput-object p2, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcView:Lcom/adyen/ui/views/CVCEditText;

    .line 257
    sget p2, Lcom/adyen/ui/R$id;->adyen_cvc_layout:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/LinearLayout;

    iput-object p2, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcLayout:Landroid/widget/LinearLayout;

    .line 259
    sget p2, Lcom/adyen/ui/R$id;->credit_card_holder_name:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/adyen/ui/views/CardHolderEditText;

    iput-object p2, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cardHolderEditText:Lcom/adyen/ui/views/CardHolderEditText;

    .line 260
    iget-boolean p2, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->nameRequired:Z

    if-eqz p2, :cond_2

    .line 261
    sget p2, Lcom/adyen/ui/R$id;->card_holder_name_layout:I

    .line 262
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/LinearLayout;

    .line 263
    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 266
    :cond_2
    iget-object p2, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {p2}, Lcom/adyen/core/models/PaymentMethod;->getInputDetails()Ljava/util/Collection;

    move-result-object p2

    .line 267
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 268
    invoke-virtual {v3}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v4

    const-string v5, "installments"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 269
    sget v1, Lcom/adyen/ui/R$id;->card_installments_area:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 270
    invoke-virtual {v3}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    .line 271
    sget v3, Lcom/adyen/ui/R$id;->installments_spinner:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->installmentsSpinner:Landroid/widget/Spinner;

    .line 272
    new-instance v3, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter;

    invoke-virtual {p0}, Lcom/adyen/ui/fragments/CreditCardFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter;-><init>(Landroid/app/Activity;Ljava/util/List;)V

    .line 273
    iget-object v1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->installmentsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 278
    :cond_4
    sget v1, Lcom/adyen/ui/R$id;->collectCreditCardData:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 280
    sget v3, Lcom/adyen/ui/R$id;->amount_text_view:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 281
    iget-object v4, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->amount:Lcom/adyen/core/models/Amount;

    invoke-virtual {p0}, Lcom/adyen/ui/fragments/CreditCardFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/adyen/core/utils/StringUtils;->getLocale(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v5

    invoke-static {v4, p3, v5}, Lcom/adyen/core/utils/AmountUtil;->format(Lcom/adyen/core/models/Amount;ZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 282
    sget v5, Lcom/adyen/ui/R$string;->payButton_formatted:I

    new-array v6, p3, [Ljava/lang/Object;

    aput-object v4, v6, v0

    invoke-virtual {p0, v5, v6}, Lcom/adyen/ui/fragments/CreditCardFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 283
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    new-instance v4, Lcom/adyen/ui/utils/AdyenInputValidator;

    invoke-direct {v4}, Lcom/adyen/ui/utils/AdyenInputValidator;-><init>()V

    .line 285
    new-instance v5, Lcom/adyen/ui/fragments/CreditCardFragment$2;

    invoke-direct {v5, p0, v1}, Lcom/adyen/ui/fragments/CreditCardFragment$2;-><init>(Lcom/adyen/ui/fragments/CreditCardFragment;Landroid/widget/Button;)V

    invoke-virtual {v4, v5}, Lcom/adyen/ui/utils/AdyenInputValidator;->setOnReadyStateChangedListener(Lcom/adyen/ui/utils/AdyenInputValidator$OnReadyStateChangedListener;)V

    .line 291
    iget-object v5, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->creditCardNoView:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-virtual {v5, v4}, Lcom/adyen/ui/views/CreditCardEditText;->setValidator(Lcom/adyen/ui/utils/AdyenInputValidator;)V

    .line 292
    iget-object v5, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->creditCardNoView:Lcom/adyen/ui/views/CreditCardEditText;

    iget-object v6, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcView:Lcom/adyen/ui/views/CVCEditText;

    invoke-virtual {v5, v6}, Lcom/adyen/ui/views/CreditCardEditText;->setCVCEditText(Lcom/adyen/ui/views/CVCEditText;)V

    .line 293
    iget-object v5, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->creditCardNoView:Lcom/adyen/ui/views/CreditCardEditText;

    iget-object v6, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {v6}, Lcom/adyen/core/models/PaymentMethod;->getLogoUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/adyen/ui/views/CreditCardEditText;->setLogoUrl(Ljava/lang/String;)V

    .line 294
    iget-object v5, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->creditCardNoView:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-virtual {v5}, Lcom/adyen/ui/views/CreditCardEditText;->initializeLogo()V

    .line 296
    invoke-direct {p0}, Lcom/adyen/ui/fragments/CreditCardFragment;->getAllowedCardTypes()Ljava/util/Map;

    move-result-object v5

    .line 298
    iget-object v6, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->creditCardNoView:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-virtual {v6, v5}, Lcom/adyen/ui/views/CreditCardEditText;->setAllowedCardTypes(Ljava/util/Map;)V

    .line 300
    iget-object v5, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->expiryDateView:Lcom/adyen/ui/views/ExpiryDateEditText;

    invoke-virtual {v5, v4}, Lcom/adyen/ui/views/ExpiryDateEditText;->setValidator(Lcom/adyen/ui/utils/AdyenInputValidator;)V

    .line 302
    iget-object v5, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcFieldStatus:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    sget-object v6, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->NOCVC:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    if-ne v5, v6, :cond_5

    .line 303
    iget-object v5, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcView:Lcom/adyen/ui/views/CVCEditText;

    invoke-virtual {v5, p3}, Lcom/adyen/ui/views/CVCEditText;->setOptional(Z)V

    .line 304
    iget-object p3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p3, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 306
    :cond_5
    iget-object p3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 307
    iget-object p3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->creditCardNoView:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-virtual {p3, p0}, Lcom/adyen/ui/views/CreditCardEditText;->addCVCFieldStatusListener(Lcom/adyen/ui/views/CreditCardEditText$CVCFieldStatusListener;)V

    .line 308
    iget-object p3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcView:Lcom/adyen/ui/views/CVCEditText;

    invoke-virtual {p3, v4}, Lcom/adyen/ui/views/CVCEditText;->setValidator(Lcom/adyen/ui/utils/AdyenInputValidator;)V

    .line 311
    :goto_1
    iget-boolean p3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->nameRequired:Z

    if-eqz p3, :cond_6

    .line 312
    iget-object p3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cardHolderEditText:Lcom/adyen/ui/views/CardHolderEditText;

    invoke-virtual {p3, v4}, Lcom/adyen/ui/views/CardHolderEditText;->setValidator(Lcom/adyen/ui/utils/AdyenInputValidator;)V

    .line 315
    :cond_6
    sget p3, Lcom/adyen/ui/R$id;->save_card_checkbox:I

    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Lcom/adyen/ui/views/CheckoutCheckBox;

    iput-object p3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->saveCardCheckBox:Lcom/adyen/ui/views/CheckoutCheckBox;

    .line 316
    iget-object p3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->shopperReference:Ljava/lang/String;

    invoke-static {p3}, Lcom/adyen/core/utils/StringUtils;->isEmptyOrNull(Ljava/lang/String;)Z

    move-result p3

    if-nez p3, :cond_7

    iget-boolean p3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->storeDetailsOptionAvailable:Z

    if-eqz p3, :cond_7

    .line 317
    sget p3, Lcom/adyen/ui/R$id;->layout_save_card:I

    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    invoke-virtual {p3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 318
    sget p3, Lcom/adyen/ui/R$id;->layout_click_area_save_card:I

    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    new-instance v0, Lcom/adyen/ui/fragments/CreditCardFragment$3;

    invoke-direct {v0, p0}, Lcom/adyen/ui/fragments/CreditCardFragment$3;-><init>(Lcom/adyen/ui/fragments/CreditCardFragment;)V

    invoke-virtual {p3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 326
    :cond_7
    sget p3, Lcom/adyen/ui/R$id;->layout_save_card:I

    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    invoke-virtual {p3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 329
    :goto_2
    new-instance p3, Lcom/adyen/ui/fragments/CreditCardFragment$4;

    invoke-direct {p3, p0, p2, v3, p1}, Lcom/adyen/ui/fragments/CreditCardFragment$4;-><init>(Lcom/adyen/ui/fragments/CreditCardFragment;Ljava/util/Collection;Landroid/widget/TextView;Landroid/view/View;)V

    invoke-virtual {v1, p3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 369
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/CreditCardFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    instance-of p2, p2, Lcom/adyen/ui/activities/CheckoutActivity;

    if-eqz p2, :cond_8

    .line 370
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/CreditCardFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    check-cast p2, Lcom/adyen/ui/activities/CheckoutActivity;

    sget p3, Lcom/adyen/ui/R$string;->creditCard_title:I

    invoke-virtual {p2, p3}, Lcom/adyen/ui/activities/CheckoutActivity;->setActionBarTitle(I)V

    :cond_8
    return-object p1
.end method

.method public onPause()V
    .locals 2

    .line 412
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    .line 414
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentCardScanners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/cardscan/PaymentCardScanner;

    .line 415
    invoke-virtual {v1}, Lcom/adyen/cardscan/PaymentCardScanner;->onPause()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .line 403
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 405
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentCardScanners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/cardscan/PaymentCardScanner;

    .line 406
    invoke-virtual {v1}, Lcom/adyen/cardscan/PaymentCardScanner;->onResume()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onScanCompleted(Lcom/adyen/cardscan/PaymentCardScanner;Lcom/adyen/cardscan/PaymentCard;)V
    .locals 3
    .param p1    # Lcom/adyen/cardscan/PaymentCardScanner;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/adyen/cardscan/PaymentCard;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 151
    iget-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->creditCardNoView:Lcom/adyen/ui/views/CreditCardEditText;

    invoke-virtual {p2}, Lcom/adyen/cardscan/PaymentCard;->getCardNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/adyen/ui/views/CreditCardEditText;->setText(Ljava/lang/CharSequence;)V

    .line 153
    invoke-virtual {p2}, Lcom/adyen/cardscan/PaymentCard;->getExpiryMonth()Ljava/lang/Integer;

    move-result-object p1

    .line 154
    invoke-virtual {p2}, Lcom/adyen/cardscan/PaymentCard;->getExpiryYear()Ljava/lang/Integer;

    move-result-object v0

    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    .line 157
    iget-object v1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->expiryDateView:Lcom/adyen/ui/views/ExpiryDateEditText;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "/"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/adyen/ui/views/ExpiryDateEditText;->setText(Ljava/lang/CharSequence;)V

    .line 160
    :cond_0
    iget-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcView:Lcom/adyen/ui/views/CVCEditText;

    invoke-virtual {p2}, Lcom/adyen/cardscan/PaymentCard;->getSecurityCode()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/adyen/ui/views/CVCEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onScanError(Lcom/adyen/cardscan/PaymentCardScanner;Ljava/lang/Throwable;)V
    .locals 1
    .param p1    # Lcom/adyen/cardscan/PaymentCardScanner;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-eqz p2, :cond_0

    .line 165
    invoke-virtual {p2}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 167
    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_1

    .line 168
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/CreditCardFragment;->getContext()Landroid/content/Context;

    move-result-object p2

    const/4 v0, 0x0

    invoke-static {p2, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    :cond_1
    return-void
.end method

.method public onScanStarted(Lcom/adyen/cardscan/PaymentCardScanner;)V
    .locals 0
    .param p1    # Lcom/adyen/cardscan/PaymentCardScanner;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 465
    sget p2, Lcom/adyen/ui/R$id;->adyen_credit_card_no:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/adyen/ui/views/CreditCardEditText;

    .line 466
    invoke-virtual {p1}, Lcom/adyen/ui/views/CreditCardEditText;->requestFocus()Z

    .line 467
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/CreditCardFragment;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string v0, "input_method"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/inputmethod/InputMethodManager;

    const/4 v0, 0x1

    .line 468
    invoke-virtual {p2, p1, v0}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    return-void
.end method

.method public setArguments(Landroid/os/Bundle;)V
    .locals 5

    .line 118
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    const-string v0, "oneClick"

    .line 119
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->oneClick:Z

    const-string v0, "amount"

    .line 120
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/adyen/core/models/Amount;

    iput-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->amount:Lcom/adyen/core/models/Amount;

    const-string v0, "PaymentMethod"

    .line 121
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/adyen/core/models/PaymentMethod;

    iput-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    const-string v0, "shopper_reference"

    .line 122
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->shopperReference:Ljava/lang/String;

    const-string v0, "public_key"

    .line 123
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->publicKey:Ljava/lang/String;

    const-string v0, "generation_time"

    .line 124
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->generationTime:Ljava/lang/String;

    const-string v0, "cvc_field_status"

    .line 125
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->valueOf(Ljava/lang/String;)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->cvcFieldStatus:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    .line 127
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {v0}, Lcom/adyen/core/models/PaymentMethod;->getInputDetails()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 128
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "additionalData.card.encrypted.json"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    .line 129
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getConfiguration()Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 130
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getConfiguration()Ljava/util/Map;

    move-result-object v2

    const-string v4, "cardHolderNameRequired"

    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 131
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getConfiguration()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v4, "true"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 132
    iput-boolean v3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->nameRequired:Z

    .line 135
    :cond_1
    invoke-virtual {v1}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "storeDetails"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136
    iput-boolean v3, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->storeDetailsOptionAvailable:Z

    goto :goto_0

    :cond_2
    const-string v0, "theme"

    .line 140
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->theme:I

    return-void
.end method

.method setCreditCardInfoListener(Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;)V
    .locals 0
    .param p1    # Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 113
    iput-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragment;->creditCardInfoListener:Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;

    return-void
.end method
