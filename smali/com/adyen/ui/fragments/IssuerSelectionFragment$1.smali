.class Lcom/adyen/ui/fragments/IssuerSelectionFragment$1;
.super Ljava/lang/Object;
.source "IssuerSelectionFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/fragments/IssuerSelectionFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/fragments/IssuerSelectionFragment;


# direct methods
.method constructor <init>(Lcom/adyen/ui/fragments/IssuerSelectionFragment;)V
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragment$1;->this$0:Lcom/adyen/ui/fragments/IssuerSelectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 82
    iget-object p1, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragment$1;->this$0:Lcom/adyen/ui/fragments/IssuerSelectionFragment;

    invoke-static {p1}, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->access$000(Lcom/adyen/ui/fragments/IssuerSelectionFragment;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;

    invoke-virtual {p1}, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;->getId()Ljava/lang/String;

    move-result-object p1

    .line 83
    iget-object p2, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragment$1;->this$0:Lcom/adyen/ui/fragments/IssuerSelectionFragment;

    invoke-static {p2}, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->access$100(Lcom/adyen/ui/fragments/IssuerSelectionFragment;)Lcom/adyen/ui/fragments/IssuerSelectionFragment$IssuerSelectionListener;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/adyen/ui/fragments/IssuerSelectionFragment$IssuerSelectionListener;->onIssuerSelected(Ljava/lang/String;)V

    return-void
.end method
