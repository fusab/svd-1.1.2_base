.class final Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;
.super Landroid/os/AsyncTask;
.source "GiropayFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/ui/fragments/GiropayFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "GiroPayLookUpASyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private searchStr:Ljava/lang/String;

.field final synthetic this$0:Lcom/adyen/ui/fragments/GiropayFragment;


# direct methods
.method private constructor <init>(Lcom/adyen/ui/fragments/GiropayFragment;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/adyen/ui/fragments/GiropayFragment;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 185
    iput-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const-string p1, ""

    .line 183
    iput-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->searchStr:Ljava/lang/String;

    .line 186
    iput-object p2, p0, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->searchStr:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/adyen/ui/fragments/GiropayFragment;Ljava/lang/String;Lcom/adyen/ui/fragments/GiropayFragment$1;)V
    .locals 0

    .line 181
    invoke-direct {p0, p1, p2}, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;-><init>(Lcom/adyen/ui/fragments/GiropayFragment;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 181
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 5

    .line 199
    :try_start_0
    new-instance p1, Ljava/lang/String;

    new-instance v0, Lcom/adyen/core/internals/HttpClient;

    invoke-direct {v0}, Lcom/adyen/core/internals/HttpClient;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://live.adyen.com/hpp/getGiroPayBankList.shtml?searchStr="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->searchStr:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/adyen/core/internals/HttpClient;->get(Ljava/lang/String;Ljava/util/Map;)[B

    move-result-object v0

    const-string v1, "UTF-8"

    .line 200
    invoke-static {v1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 202
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    const-string p1, ""

    return-object p1
.end method

.method protected onCancelled()V
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/GiropayFragment;->access$1000(Lcom/adyen/ui/fragments/GiropayFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/GiropayFragment;->access$1000(Lcom/adyen/ui/fragments/GiropayFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 181
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 4

    .line 209
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/GiropayFragment;->access$1000(Lcom/adyen/ui/fragments/GiropayFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/GiropayFragment;->access$1000(Lcom/adyen/ui/fragments/GiropayFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 213
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/GiropayFragment;->access$500(Lcom/adyen/ui/fragments/GiropayFragment;)Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;

    move-result-object v0

    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->searchStr:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->access$1100(Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;Lorg/json/JSONArray;Ljava/lang/String;)V

    .line 214
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {p1}, Lcom/adyen/ui/fragments/GiropayFragment;->access$500(Lcom/adyen/ui/fragments/GiropayFragment;)Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;

    move-result-object p1

    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->searchStr:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->access$900(Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;Ljava/lang/String;)V

    .line 215
    iget-object p1, p0, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {p1}, Lcom/adyen/ui/fragments/GiropayFragment;->access$500(Lcom/adyen/ui/fragments/GiropayFragment;)Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->notifyDataSetChanged()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 217
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 191
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/GiropayFragment;->access$500(Lcom/adyen/ui/fragments/GiropayFragment;)Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;

    move-result-object v0

    invoke-static {v0}, Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;->access$600(Lcom/adyen/ui/fragments/GiropayFragment$AdyenUIGiroPayIssuersListAdapter;)I

    move-result v0

    if-nez v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/adyen/ui/fragments/GiropayFragment$GiroPayLookUpASyncTask;->this$0:Lcom/adyen/ui/fragments/GiropayFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/GiropayFragment;->access$1000(Lcom/adyen/ui/fragments/GiropayFragment;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method
