.class Lcom/adyen/ui/fragments/SepaDirectDebitFragment$3;
.super Ljava/lang/Object;
.source "SepaDirectDebitFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/fragments/SepaDirectDebitFragment;

.field final synthetic val$ibanEditText:Lcom/adyen/ui/views/IBANEditText;


# direct methods
.method constructor <init>(Lcom/adyen/ui/fragments/SepaDirectDebitFragment;Lcom/adyen/ui/views/IBANEditText;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$3;->this$0:Lcom/adyen/ui/fragments/SepaDirectDebitFragment;

    iput-object p2, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$3;->val$ibanEditText:Lcom/adyen/ui/views/IBANEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 104
    iget-object p1, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$3;->this$0:Lcom/adyen/ui/fragments/SepaDirectDebitFragment;

    invoke-static {p1}, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->access$000(Lcom/adyen/ui/fragments/SepaDirectDebitFragment;)Lcom/adyen/ui/fragments/SepaDirectDebitFragment$SEPADirectDebitPaymentDetailsListener;

    move-result-object p1

    iget-object v0, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$3;->val$ibanEditText:Lcom/adyen/ui/views/IBANEditText;

    invoke-virtual {v0}, Lcom/adyen/ui/views/IBANEditText;->getIbanNumber()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$3;->val$ibanEditText:Lcom/adyen/ui/views/IBANEditText;

    invoke-virtual {v1}, Lcom/adyen/ui/views/IBANEditText;->getIbanNumber()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$SEPADirectDebitPaymentDetailsListener;->onPaymentDetails(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
