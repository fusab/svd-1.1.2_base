.class public Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;
.super Ljava/lang/Object;
.source "CreditCardFragmentBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;
    }
.end annotation


# instance fields
.field private amount:Lcom/adyen/core/models/Amount;

.field private creditCardInfoListener:Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;

.field private cvcFieldStatus:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

.field private generationtime:Ljava/lang/String;

.field private paymentCardScanEnabled:Z

.field private paymentMethod:Lcom/adyen/core/models/PaymentMethod;

.field private publicKey:Ljava/lang/String;

.field private shopperReference:Ljava/lang/String;

.field private theme:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    sget v0, Lcom/adyen/ui/R$style;->AdyenTheme:I

    iput v0, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->theme:I

    return-void
.end method

.method private checkParameters()V
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->amount:Lcom/adyen/core/models/Amount;

    if-eqz v0, :cond_4

    .line 126
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    if-eqz v0, :cond_3

    .line 129
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->publicKey:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 132
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->generationtime:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 135
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->creditCardInfoListener:Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;

    if-eqz v0, :cond_0

    return-void

    .line 136
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CreditCardInfoListener not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Generationtime not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PublicKey not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PaymentMethod not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Amount not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public build()Lcom/adyen/ui/fragments/CreditCardFragment;
    .locals 4

    .line 102
    invoke-direct {p0}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->checkParameters()V

    .line 104
    new-instance v0, Lcom/adyen/ui/fragments/CreditCardFragment;

    invoke-direct {v0}, Lcom/adyen/ui/fragments/CreditCardFragment;-><init>()V

    .line 105
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 106
    iget-object v2, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->amount:Lcom/adyen/core/models/Amount;

    const-string v3, "amount"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 107
    iget-object v2, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->shopperReference:Ljava/lang/String;

    const-string v3, "shopper_reference"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-object v2, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->publicKey:Ljava/lang/String;

    const-string v3, "public_key"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v2, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->generationtime:Ljava/lang/String;

    const-string v3, "generation_time"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    iget-object v2, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    const-string v3, "PaymentMethod"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 111
    iget-object v2, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->cvcFieldStatus:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    invoke-virtual {v2}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->name()Ljava/lang/String;

    move-result-object v2

    const-string v3, "cvc_field_status"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-boolean v2, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->paymentCardScanEnabled:Z

    const-string v3, "payment_card_scan_enabled"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 113
    iget v2, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->theme:I

    const-string v3, "theme"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 115
    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/CreditCardFragment;->setArguments(Landroid/os/Bundle;)V

    .line 117
    iget-object v1, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->creditCardInfoListener:Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;

    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/CreditCardFragment;->setCreditCardInfoListener(Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;)V

    return-object v0
.end method

.method public setAmount(Lcom/adyen/core/models/Amount;)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->amount:Lcom/adyen/core/models/Amount;

    return-object p0
.end method

.method public setCVCFieldStatus(Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->cvcFieldStatus:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    return-object p0
.end method

.method public setCreditCardInfoListener(Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;
    .locals 0
    .param p1    # Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 82
    iput-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->creditCardInfoListener:Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;

    return-object p0
.end method

.method public setGenerationtime(Ljava/lang/String;)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->generationtime:Ljava/lang/String;

    return-object p0
.end method

.method public setPaymentCardScanEnabled(Z)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;
    .locals 0

    .line 92
    iput-boolean p1, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->paymentCardScanEnabled:Z

    return-object p0
.end method

.method public setPaymentMethod(Lcom/adyen/core/models/PaymentMethod;)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    return-object p0
.end method

.method public setPublicKey(Ljava/lang/String;)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->publicKey:Ljava/lang/String;

    return-object p0
.end method

.method public setShopperReference(Ljava/lang/String;)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->shopperReference:Ljava/lang/String;

    return-object p0
.end method

.method public setTheme(I)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;
    .locals 0

    .line 76
    iput p1, p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;->theme:I

    return-object p0
.end method
