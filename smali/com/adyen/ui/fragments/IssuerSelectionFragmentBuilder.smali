.class public Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;
.super Ljava/lang/Object;
.source "IssuerSelectionFragmentBuilder.java"


# instance fields
.field private issuerSelectionListener:Lcom/adyen/ui/fragments/IssuerSelectionFragment$IssuerSelectionListener;

.field private paymentMethod:Lcom/adyen/core/models/PaymentMethod;

.field private theme:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget v0, Lcom/adyen/ui/R$style;->AdyenTheme:I

    iput v0, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;->theme:I

    return-void
.end method

.method private checkParameters()V
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    if-eqz v0, :cond_0

    return-void

    .line 82
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PaymentMethod not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public build()Lcom/adyen/ui/fragments/IssuerSelectionFragment;
    .locals 4

    .line 67
    invoke-direct {p0}, Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;->checkParameters()V

    .line 69
    new-instance v0, Lcom/adyen/ui/fragments/IssuerSelectionFragment;

    invoke-direct {v0}, Lcom/adyen/ui/fragments/IssuerSelectionFragment;-><init>()V

    .line 70
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 71
    iget-object v2, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    const-string v3, "PaymentMethod"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 72
    iget v2, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;->theme:I

    const-string v3, "theme"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 74
    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->setArguments(Landroid/os/Bundle;)V

    .line 75
    iget-object v1, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;->issuerSelectionListener:Lcom/adyen/ui/fragments/IssuerSelectionFragment$IssuerSelectionListener;

    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->setIssuerSelectionListener(Lcom/adyen/ui/fragments/IssuerSelectionFragment$IssuerSelectionListener;)V

    return-object v0
.end method

.method public setIssuerSelectionListener(Lcom/adyen/ui/fragments/IssuerSelectionFragment$IssuerSelectionListener;)Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;->issuerSelectionListener:Lcom/adyen/ui/fragments/IssuerSelectionFragment$IssuerSelectionListener;

    return-object p0
.end method

.method public setPaymentMethod(Lcom/adyen/core/models/PaymentMethod;)Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    return-object p0
.end method

.method public setTheme(I)Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;
    .locals 0

    .line 47
    iput p1, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragmentBuilder;->theme:I

    return-object p0
.end method
