.class public Lcom/adyen/ui/fragments/LoadingScreenFragment;
.super Landroidx/fragment/app/Fragment;
.source "LoadingScreenFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 22
    sget p3, Lcom/adyen/ui/R$layout;->loading_screen_fragment:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 23
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/LoadingScreenFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    instance-of p2, p2, Lcom/adyen/ui/activities/CheckoutActivity;

    if-eqz p2, :cond_0

    .line 24
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/LoadingScreenFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    check-cast p2, Lcom/adyen/ui/activities/CheckoutActivity;

    invoke-virtual {p2}, Lcom/adyen/ui/activities/CheckoutActivity;->hideActionBar()V

    :cond_0
    return-object p1
.end method
