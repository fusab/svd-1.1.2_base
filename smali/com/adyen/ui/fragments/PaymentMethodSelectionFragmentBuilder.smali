.class public Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;
.super Ljava/lang/Object;
.source "PaymentMethodSelectionFragmentBuilder.java"


# instance fields
.field private paymentMethodSelectionListener:Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$PaymentMethodSelectionListener;

.field private paymentMethods:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;"
        }
    .end annotation
.end field

.field private preferredPaymentMethods:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;"
        }
    .end annotation
.end field

.field private theme:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->paymentMethods:Ljava/util/ArrayList;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->preferredPaymentMethods:Ljava/util/ArrayList;

    .line 26
    sget v0, Lcom/adyen/ui/R$style;->AdyenTheme:I

    iput v0, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->theme:I

    return-void
.end method

.method private checkParameters()V
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->paymentMethods:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->paymentMethodSelectionListener:Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$PaymentMethodSelectionListener;

    if-eqz v0, :cond_0

    return-void

    .line 97
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PaymentMethodSelectionListener not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PaymentMethods not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public build()Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;
    .locals 4

    .line 79
    invoke-direct {p0}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->checkParameters()V

    .line 81
    new-instance v0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;

    invoke-direct {v0}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;-><init>()V

    .line 82
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 83
    iget-object v2, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->paymentMethods:Ljava/util/ArrayList;

    const-string v3, "filteredPaymentMethods"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 84
    iget-object v2, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->preferredPaymentMethods:Ljava/util/ArrayList;

    const-string v3, "preferredPaymentMethods"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 85
    iget v2, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->theme:I

    const-string v3, "theme"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 87
    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->setArguments(Landroid/os/Bundle;)V

    .line 88
    iget-object v1, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->paymentMethodSelectionListener:Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$PaymentMethodSelectionListener;

    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment;->setPaymentMethodSelectionListener(Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$PaymentMethodSelectionListener;)V

    return-object v0
.end method

.method public setPaymentMethodSelectionListener(Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$PaymentMethodSelectionListener;)Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->paymentMethodSelectionListener:Lcom/adyen/ui/fragments/PaymentMethodSelectionFragment$PaymentMethodSelectionListener;

    return-object p0
.end method

.method public setPaymentMethods(Ljava/util/List;)Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;)",
            "Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->paymentMethods:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 41
    iget-object v0, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->paymentMethods:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public setPreferredPaymentMethods(Ljava/util/List;)Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;)",
            "Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->preferredPaymentMethods:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 52
    iget-object v0, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->preferredPaymentMethods:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public setTheme(I)Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;
    .locals 0

    .line 63
    iput p1, p0, Lcom/adyen/ui/fragments/PaymentMethodSelectionFragmentBuilder;->theme:I

    return-object p0
.end method
