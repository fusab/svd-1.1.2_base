.class public Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;
.super Ljava/lang/Object;
.source "QiwiWalletFragmentBuilder.java"


# instance fields
.field private amount:Lcom/adyen/core/models/Amount;

.field private paymentMethod:Lcom/adyen/core/models/PaymentMethod;

.field private qiwiWalletPaymentDetailsListener:Lcom/adyen/ui/fragments/QiwiWalletFragment$QiwiWalletPaymentDetailsListener;

.field private theme:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    sget v0, Lcom/adyen/ui/R$style;->AdyenTheme:I

    iput v0, p0, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;->theme:I

    return-void
.end method

.method private checkParameters()V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;->amount:Lcom/adyen/core/models/Amount;

    if-eqz v0, :cond_2

    .line 84
    iget-object v0, p0, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;->qiwiWalletPaymentDetailsListener:Lcom/adyen/ui/fragments/QiwiWalletFragment$QiwiWalletPaymentDetailsListener;

    if-eqz v0, :cond_0

    return-void

    .line 89
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "QiwiWalletPaymentDataListener not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PaymentMethod not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Amount not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public build()Lcom/adyen/ui/fragments/QiwiWalletFragment;
    .locals 4

    .line 65
    invoke-direct {p0}, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;->checkParameters()V

    .line 67
    new-instance v0, Lcom/adyen/ui/fragments/QiwiWalletFragment;

    invoke-direct {v0}, Lcom/adyen/ui/fragments/QiwiWalletFragment;-><init>()V

    .line 68
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 69
    iget-object v2, p0, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    const-string v3, "PaymentMethod"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 70
    iget-object v2, p0, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;->amount:Lcom/adyen/core/models/Amount;

    const-string v3, "amount"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 71
    iget v2, p0, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;->theme:I

    const-string v3, "theme"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 73
    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/QiwiWalletFragment;->setArguments(Landroid/os/Bundle;)V

    .line 74
    iget-object v1, p0, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;->qiwiWalletPaymentDetailsListener:Lcom/adyen/ui/fragments/QiwiWalletFragment$QiwiWalletPaymentDetailsListener;

    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/QiwiWalletFragment;->setQiwiWalletPaymentDetailsListener(Lcom/adyen/ui/fragments/QiwiWalletFragment$QiwiWalletPaymentDetailsListener;)V

    return-object v0
.end method

.method public setAmount(Lcom/adyen/core/models/Amount;)Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;->amount:Lcom/adyen/core/models/Amount;

    return-object p0
.end method

.method public setPaymentMethod(Lcom/adyen/core/models/PaymentMethod;)Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    return-object p0
.end method

.method public setQiwiWalletPaymentDetailsListener(Lcom/adyen/ui/fragments/QiwiWalletFragment$QiwiWalletPaymentDetailsListener;)Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;->qiwiWalletPaymentDetailsListener:Lcom/adyen/ui/fragments/QiwiWalletFragment$QiwiWalletPaymentDetailsListener;

    return-object p0
.end method

.method public setTheme(I)Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;
    .locals 0

    .line 50
    iput p1, p0, Lcom/adyen/ui/fragments/QiwiWalletFragmentBuilder;->theme:I

    return-object p0
.end method
