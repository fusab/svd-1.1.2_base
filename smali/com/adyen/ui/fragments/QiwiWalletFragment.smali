.class public Lcom/adyen/ui/fragments/QiwiWalletFragment;
.super Landroidx/fragment/app/Fragment;
.source "QiwiWalletFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/ui/fragments/QiwiWalletFragment$QiwiWalletPaymentDetailsListener;
    }
.end annotation


# instance fields
.field private amount:Lcom/adyen/core/models/Amount;

.field private paymentMethod:Lcom/adyen/core/models/PaymentMethod;

.field private qiwiWalletPaymentDetailsListener:Lcom/adyen/ui/fragments/QiwiWalletFragment$QiwiWalletPaymentDetailsListener;

.field private theme:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/adyen/ui/fragments/QiwiWalletFragment;)Lcom/adyen/ui/fragments/QiwiWalletFragment$QiwiWalletPaymentDetailsListener;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/adyen/ui/fragments/QiwiWalletFragment;->qiwiWalletPaymentDetailsListener:Lcom/adyen/ui/fragments/QiwiWalletFragment$QiwiWalletPaymentDetailsListener;

    return-object p0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 69
    new-instance p3, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/adyen/ui/fragments/QiwiWalletFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget v1, p0, Lcom/adyen/ui/fragments/QiwiWalletFragment;->theme:I

    invoke-direct {p3, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 70
    invoke-virtual {p1, p3}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    .line 71
    sget p3, Lcom/adyen/ui/R$layout;->qiwi_wallet_fragment:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 74
    sget p2, Lcom/adyen/ui/R$id;->telephone_number_edit_text:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/EditText;

    .line 76
    sget p3, Lcom/adyen/ui/R$id;->country_code_spinner:I

    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/Spinner;

    .line 78
    iget-object v1, p0, Lcom/adyen/ui/fragments/QiwiWalletFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {v1}, Lcom/adyen/core/models/PaymentMethod;->getInputDetails()Ljava/util/Collection;

    move-result-object v1

    .line 79
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 80
    invoke-virtual {v2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v3

    const-string v4, "qiwiwallet.telephoneNumberPrefix"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 81
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 82
    invoke-virtual {v2}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getItems()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;

    .line 83
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ")"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 85
    :cond_1
    new-instance v2, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/adyen/ui/fragments/QiwiWalletFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v4

    const v5, 0x1090011

    invoke-direct {v2, v4, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 86
    invoke-virtual {p3, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto :goto_0

    .line 92
    :cond_2
    sget v1, Lcom/adyen/ui/R$id;->collect_direct_debit_data:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 93
    new-instance v2, Lcom/adyen/ui/fragments/QiwiWalletFragment$1;

    invoke-direct {v2, p0, p3, p2}, Lcom/adyen/ui/fragments/QiwiWalletFragment$1;-><init>(Lcom/adyen/ui/fragments/QiwiWalletFragment;Landroid/widget/Spinner;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 p2, 0x1

    .line 101
    invoke-virtual {v1, p2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 103
    sget p3, Lcom/adyen/ui/R$id;->amount_text_view:I

    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 104
    iget-object v1, p0, Lcom/adyen/ui/fragments/QiwiWalletFragment;->amount:Lcom/adyen/core/models/Amount;

    invoke-virtual {p0}, Lcom/adyen/ui/fragments/QiwiWalletFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/adyen/core/utils/StringUtils;->getLocale(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v2

    invoke-static {v1, p2, v2}, Lcom/adyen/core/utils/AmountUtil;->format(Lcom/adyen/core/models/Amount;ZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 105
    sget v2, Lcom/adyen/ui/R$string;->payButton_formatted:I

    new-array p2, p2, [Ljava/lang/Object;

    aput-object v1, p2, v0

    invoke-virtual {p0, v2, p2}, Lcom/adyen/ui/fragments/QiwiWalletFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 106
    invoke-virtual {p3, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/QiwiWalletFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    instance-of p2, p2, Lcom/adyen/ui/activities/CheckoutActivity;

    if-eqz p2, :cond_3

    .line 109
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/QiwiWalletFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    check-cast p2, Lcom/adyen/ui/activities/CheckoutActivity;

    iget-object p3, p0, Lcom/adyen/ui/fragments/QiwiWalletFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {p3}, Lcom/adyen/core/models/PaymentMethod;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/adyen/ui/activities/CheckoutActivity;->setActionBarTitle(Ljava/lang/String;)V

    :cond_3
    return-object p1
.end method

.method public setArguments(Landroid/os/Bundle;)V
    .locals 1

    .line 55
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    const-string v0, "amount"

    .line 56
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/adyen/core/models/Amount;

    iput-object v0, p0, Lcom/adyen/ui/fragments/QiwiWalletFragment;->amount:Lcom/adyen/core/models/Amount;

    const-string v0, "PaymentMethod"

    .line 58
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/adyen/core/models/PaymentMethod;

    iput-object v0, p0, Lcom/adyen/ui/fragments/QiwiWalletFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    const-string v0, "theme"

    .line 60
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/adyen/ui/fragments/QiwiWalletFragment;->theme:I

    return-void
.end method

.method setQiwiWalletPaymentDetailsListener(Lcom/adyen/ui/fragments/QiwiWalletFragment$QiwiWalletPaymentDetailsListener;)V
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/adyen/ui/fragments/QiwiWalletFragment;->qiwiWalletPaymentDetailsListener:Lcom/adyen/ui/fragments/QiwiWalletFragment$QiwiWalletPaymentDetailsListener;

    return-void
.end method
