.class public final enum Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;
.super Ljava/lang/Enum;
.source "CreditCardFragmentBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/ui/fragments/CreditCardFragmentBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CvcFieldStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

.field public static final enum NOCVC:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

.field public static final enum OPTIONAL:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

.field public static final enum REQUIRED:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 20
    new-instance v0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    const/4 v1, 0x0

    const-string v2, "REQUIRED"

    invoke-direct {v0, v2, v1}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->REQUIRED:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    .line 21
    new-instance v0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    const/4 v2, 0x1

    const-string v3, "OPTIONAL"

    invoke-direct {v0, v3, v2}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->OPTIONAL:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    .line 22
    new-instance v0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    const/4 v3, 0x2

    const-string v4, "NOCVC"

    invoke-direct {v0, v4, v3}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->NOCVC:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    const/4 v0, 0x3

    .line 19
    new-array v0, v0, [Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    sget-object v4, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->REQUIRED:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    aput-object v4, v0, v1

    sget-object v1, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->OPTIONAL:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->NOCVC:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->$VALUES:[Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;
    .locals 1

    .line 19
    const-class v0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    return-object p0
.end method

.method public static values()[Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;
    .locals 1

    .line 19
    sget-object v0, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->$VALUES:[Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    invoke-virtual {v0}, [Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    return-object v0
.end method
