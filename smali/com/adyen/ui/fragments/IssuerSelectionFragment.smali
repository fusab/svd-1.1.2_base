.class public Lcom/adyen/ui/fragments/IssuerSelectionFragment;
.super Landroidx/fragment/app/Fragment;
.source "IssuerSelectionFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/ui/fragments/IssuerSelectionFragment$IssuerSelectionListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "IssuerSelectionFragment"


# instance fields
.field private issuerSelectionListener:Lcom/adyen/ui/fragments/IssuerSelectionFragment$IssuerSelectionListener;

.field private issuers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail$Item;",
            ">;"
        }
    .end annotation
.end field

.field private paymentMethod:Lcom/adyen/core/models/PaymentMethod;

.field private theme:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 40
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    .line 32
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->issuers:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/adyen/ui/fragments/IssuerSelectionFragment;)Ljava/util/List;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->issuers:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$100(Lcom/adyen/ui/fragments/IssuerSelectionFragment;)Lcom/adyen/ui/fragments/IssuerSelectionFragment$IssuerSelectionListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->issuerSelectionListener:Lcom/adyen/ui/fragments/IssuerSelectionFragment$IssuerSelectionListener;

    return-object p0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .line 63
    sget-object p3, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->TAG:Ljava/lang/String;

    const-string v0, "onCreateView()"

    invoke-static {p3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    new-instance p3, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget v1, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->theme:I

    invoke-direct {p3, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 66
    invoke-virtual {p1, p3}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    .line 67
    sget p3, Lcom/adyen/ui/R$layout;->issuer_selection_fragment:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 69
    iget-object p2, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {p2}, Lcom/adyen/core/models/PaymentMethod;->getInputDetails()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/adyen/core/models/paymentdetails/InputDetail;

    .line 70
    invoke-virtual {p3}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "idealIssuer"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p3}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "issuer"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    :cond_1
    invoke-virtual {p3}, Lcom/adyen/core/models/paymentdetails/InputDetail;->getItems()Ljava/util/ArrayList;

    move-result-object p2

    iput-object p2, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->issuers:Ljava/util/List;

    .line 76
    :cond_2
    new-instance p2, Lcom/adyen/ui/adapters/IssuerListAdapter;

    invoke-virtual {p0}, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p3

    iget-object v0, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->issuers:Ljava/util/List;

    invoke-direct {p2, p3, v0}, Lcom/adyen/ui/adapters/IssuerListAdapter;-><init>(Landroid/app/Activity;Ljava/util/List;)V

    .line 77
    sget p3, Lcom/adyen/ui/R$id;->issuer_methods_list:I

    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ListView;

    .line 78
    invoke-virtual {p3, p2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 79
    new-instance p2, Lcom/adyen/ui/fragments/IssuerSelectionFragment$1;

    invoke-direct {p2, p0}, Lcom/adyen/ui/fragments/IssuerSelectionFragment$1;-><init>(Lcom/adyen/ui/fragments/IssuerSelectionFragment;)V

    invoke-virtual {p3, p2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 87
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    instance-of p2, p2, Lcom/adyen/ui/activities/CheckoutActivity;

    if-eqz p2, :cond_3

    .line 88
    invoke-virtual {p0}, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    check-cast p2, Lcom/adyen/ui/activities/CheckoutActivity;

    iget-object p3, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    invoke-virtual {p3}, Lcom/adyen/core/models/PaymentMethod;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/adyen/ui/activities/CheckoutActivity;->setActionBarTitle(Ljava/lang/String;)V

    :cond_3
    return-object p1
.end method

.method public setArguments(Landroid/os/Bundle;)V
    .locals 1

    .line 54
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    const-string v0, "PaymentMethod"

    .line 55
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/adyen/core/models/PaymentMethod;

    iput-object v0, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    const-string v0, "theme"

    .line 57
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->theme:I

    return-void
.end method

.method setIssuerSelectionListener(Lcom/adyen/ui/fragments/IssuerSelectionFragment$IssuerSelectionListener;)V
    .locals 0

    .line 94
    iput-object p1, p0, Lcom/adyen/ui/fragments/IssuerSelectionFragment;->issuerSelectionListener:Lcom/adyen/ui/fragments/IssuerSelectionFragment$IssuerSelectionListener;

    return-void
.end method
