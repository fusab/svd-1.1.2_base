.class Lcom/adyen/ui/fragments/SepaDirectDebitFragment$2;
.super Ljava/lang/Object;
.source "SepaDirectDebitFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/fragments/SepaDirectDebitFragment;

.field final synthetic val$consentCheckbox:Lcom/adyen/ui/views/CheckoutCheckBox;


# direct methods
.method constructor <init>(Lcom/adyen/ui/fragments/SepaDirectDebitFragment;Lcom/adyen/ui/views/CheckoutCheckBox;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$2;->this$0:Lcom/adyen/ui/fragments/SepaDirectDebitFragment;

    iput-object p2, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$2;->val$consentCheckbox:Lcom/adyen/ui/views/CheckoutCheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0

    .line 95
    iget-object p1, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$2;->val$consentCheckbox:Lcom/adyen/ui/views/CheckoutCheckBox;

    invoke-virtual {p1}, Lcom/adyen/ui/views/CheckoutCheckBox;->forceRippleAnimation()V

    .line 96
    iget-object p1, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment$2;->val$consentCheckbox:Lcom/adyen/ui/views/CheckoutCheckBox;

    invoke-virtual {p1}, Lcom/adyen/ui/views/CheckoutCheckBox;->toggle()V

    return-void
.end method
