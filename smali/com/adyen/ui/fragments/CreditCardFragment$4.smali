.class Lcom/adyen/ui/fragments/CreditCardFragment$4;
.super Ljava/lang/Object;
.source "CreditCardFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/fragments/CreditCardFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/fragments/CreditCardFragment;

.field final synthetic val$checkoutTextView:Landroid/widget/TextView;

.field final synthetic val$fragmentView:Landroid/view/View;

.field final synthetic val$inputDetails:Ljava/util/Collection;


# direct methods
.method constructor <init>(Lcom/adyen/ui/fragments/CreditCardFragment;Ljava/util/Collection;Landroid/widget/TextView;Landroid/view/View;)V
    .locals 0

    .line 329
    iput-object p1, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->this$0:Lcom/adyen/ui/fragments/CreditCardFragment;

    iput-object p2, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->val$inputDetails:Ljava/util/Collection;

    iput-object p3, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->val$checkoutTextView:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->val$fragmentView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .line 332
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->this$0:Lcom/adyen/ui/fragments/CreditCardFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/CreditCardFragment;->access$200(Lcom/adyen/ui/fragments/CreditCardFragment;)Ljava/lang/String;

    move-result-object v0

    .line 334
    iget-object v1, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->this$0:Lcom/adyen/ui/fragments/CreditCardFragment;

    invoke-static {v1}, Lcom/adyen/ui/fragments/CreditCardFragment;->access$300(Lcom/adyen/ui/fragments/CreditCardFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/adyen/core/utils/StringUtils;->isEmptyOrNull(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->this$0:Lcom/adyen/ui/fragments/CreditCardFragment;

    .line 335
    invoke-static {v1}, Lcom/adyen/ui/fragments/CreditCardFragment;->access$100(Lcom/adyen/ui/fragments/CreditCardFragment;)Lcom/adyen/ui/views/CheckoutCheckBox;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adyen/ui/views/CheckoutCheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 337
    :goto_0
    iget-object v3, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->this$0:Lcom/adyen/ui/fragments/CreditCardFragment;

    invoke-static {v3}, Lcom/adyen/ui/fragments/CreditCardFragment;->access$400(Lcom/adyen/ui/fragments/CreditCardFragment;)Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 338
    new-instance v3, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails;

    iget-object v4, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->val$inputDetails:Ljava/util/Collection;

    invoke-direct {v3, v4}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails;-><init>(Ljava/util/Collection;)V

    .line 339
    invoke-virtual {v3, v0}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails;->fillCardToken(Ljava/lang/String;)Z

    .line 340
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->this$0:Lcom/adyen/ui/fragments/CreditCardFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/CreditCardFragment;->access$500(Lcom/adyen/ui/fragments/CreditCardFragment;)Landroid/widget/Spinner;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 341
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->this$0:Lcom/adyen/ui/fragments/CreditCardFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/CreditCardFragment;->access$500(Lcom/adyen/ui/fragments/CreditCardFragment;)Landroid/widget/Spinner;

    move-result-object v0

    .line 342
    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;

    invoke-virtual {v0}, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;->getId()Ljava/lang/String;

    move-result-object v0

    .line 341
    invoke-static {v0}, Ljava/lang/Short;->valueOf(Ljava/lang/String;)Ljava/lang/Short;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v0

    invoke-virtual {v3, v0}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails;->fillNumberOfInstallments(S)Z

    .line 344
    :cond_1
    invoke-virtual {v3, v1}, Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails;->fillStoreDetails(Z)Z

    .line 345
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->this$0:Lcom/adyen/ui/fragments/CreditCardFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/CreditCardFragment;->access$400(Lcom/adyen/ui/fragments/CreditCardFragment;)Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/adyen/ui/fragments/CreditCardFragment$CreditCardInfoListener;->onCreditCardInfoProvided(Lcom/adyen/core/models/paymentdetails/CreditCardPaymentDetails;)V

    goto :goto_1

    .line 347
    :cond_2
    invoke-static {}, Lcom/adyen/ui/fragments/CreditCardFragment;->access$600()Ljava/lang/String;

    move-result-object v0

    const-string v1, "No listener provided."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    :goto_1
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->val$checkoutTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 351
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->val$fragmentView:Landroid/view/View;

    sget v1, Lcom/adyen/ui/R$id;->processing_progress_bar:I

    .line 352
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;

    .line 353
    invoke-virtual {v0, v2}, Lcom/adyen/ui/views/loadinganimation/ThreeDotsLoadingView;->setVisibility(I)V

    .line 355
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->this$0:Lcom/adyen/ui/fragments/CreditCardFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/CreditCardFragment;->access$700(Lcom/adyen/ui/fragments/CreditCardFragment;)Lcom/adyen/ui/views/CVCEditText;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/adyen/ui/views/CVCEditText;->setEnabled(Z)V

    .line 356
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->this$0:Lcom/adyen/ui/fragments/CreditCardFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/CreditCardFragment;->access$800(Lcom/adyen/ui/fragments/CreditCardFragment;)Lcom/adyen/ui/views/CreditCardEditText;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/adyen/ui/views/CreditCardEditText;->setEnabled(Z)V

    .line 357
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->this$0:Lcom/adyen/ui/fragments/CreditCardFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/CreditCardFragment;->access$900(Lcom/adyen/ui/fragments/CreditCardFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 358
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->this$0:Lcom/adyen/ui/fragments/CreditCardFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/CreditCardFragment;->access$1000(Lcom/adyen/ui/fragments/CreditCardFragment;)Lcom/adyen/ui/views/CardHolderEditText;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/adyen/ui/views/CardHolderEditText;->setEnabled(Z)V

    .line 360
    :cond_3
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->this$0:Lcom/adyen/ui/fragments/CreditCardFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/CreditCardFragment;->access$1100(Lcom/adyen/ui/fragments/CreditCardFragment;)Lcom/adyen/ui/views/ExpiryDateEditText;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/adyen/ui/views/ExpiryDateEditText;->setEnabled(Z)V

    .line 362
    iget-object v0, p0, Lcom/adyen/ui/fragments/CreditCardFragment$4;->this$0:Lcom/adyen/ui/fragments/CreditCardFragment;

    invoke-virtual {v0}, Lcom/adyen/ui/fragments/CreditCardFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 364
    invoke-virtual {p1}, Landroid/view/View;->getApplicationWindowToken()Landroid/os/IBinder;

    move-result-object p1

    invoke-virtual {v0, p1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method
