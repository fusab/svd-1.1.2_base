.class public Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;
.super Ljava/lang/Object;
.source "SepaDirectDebitFragmentBuilder.java"


# instance fields
.field private amount:Lcom/adyen/core/models/Amount;

.field private paymentMethod:Lcom/adyen/core/models/PaymentMethod;

.field private sepaDirectDebitPaymentDetailsListener:Lcom/adyen/ui/fragments/SepaDirectDebitFragment$SEPADirectDebitPaymentDetailsListener;

.field private theme:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    sget v0, Lcom/adyen/ui/R$style;->AdyenTheme:I

    iput v0, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;->theme:I

    return-void
.end method

.method private checkParameters()V
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;->amount:Lcom/adyen/core/models/Amount;

    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;->sepaDirectDebitPaymentDetailsListener:Lcom/adyen/ui/fragments/SepaDirectDebitFragment$SEPADirectDebitPaymentDetailsListener;

    if-eqz v0, :cond_0

    return-void

    .line 85
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SepaDirectDebitPaymentDetailsListener not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Amount not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public build()Lcom/adyen/ui/fragments/SepaDirectDebitFragment;
    .locals 4

    .line 66
    invoke-direct {p0}, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;->checkParameters()V

    .line 68
    new-instance v0, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;

    invoke-direct {v0}, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;-><init>()V

    .line 69
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 70
    iget-object v2, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    const-string v3, "PaymentMethod"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 71
    iget-object v2, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;->amount:Lcom/adyen/core/models/Amount;

    const-string v3, "amount"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 72
    iget v2, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;->theme:I

    const-string v3, "theme"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 74
    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->setArguments(Landroid/os/Bundle;)V

    .line 75
    iget-object v1, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;->sepaDirectDebitPaymentDetailsListener:Lcom/adyen/ui/fragments/SepaDirectDebitFragment$SEPADirectDebitPaymentDetailsListener;

    invoke-virtual {v0, v1}, Lcom/adyen/ui/fragments/SepaDirectDebitFragment;->setSEPADirectDebitPaymentDetailsListener(Lcom/adyen/ui/fragments/SepaDirectDebitFragment$SEPADirectDebitPaymentDetailsListener;)V

    return-object v0
.end method

.method public setAmount(Lcom/adyen/core/models/Amount;)Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;->amount:Lcom/adyen/core/models/Amount;

    return-object p0
.end method

.method public setPaymentMethod(Lcom/adyen/core/models/PaymentMethod;)Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;->paymentMethod:Lcom/adyen/core/models/PaymentMethod;

    return-object p0
.end method

.method public setSEPADirectDebitPaymentDetailsListener(Lcom/adyen/ui/fragments/SepaDirectDebitFragment$SEPADirectDebitPaymentDetailsListener;)Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;->sepaDirectDebitPaymentDetailsListener:Lcom/adyen/ui/fragments/SepaDirectDebitFragment$SEPADirectDebitPaymentDetailsListener;

    return-object p0
.end method

.method public setTheme(I)Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;
    .locals 0

    .line 50
    iput p1, p0, Lcom/adyen/ui/fragments/SepaDirectDebitFragmentBuilder;->theme:I

    return-object p0
.end method
