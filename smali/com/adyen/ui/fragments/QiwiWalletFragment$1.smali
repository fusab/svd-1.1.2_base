.class Lcom/adyen/ui/fragments/QiwiWalletFragment$1;
.super Ljava/lang/Object;
.source "QiwiWalletFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/fragments/QiwiWalletFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/fragments/QiwiWalletFragment;

.field final synthetic val$countryCode:Landroid/widget/Spinner;

.field final synthetic val$telephoneNumber:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/adyen/ui/fragments/QiwiWalletFragment;Landroid/widget/Spinner;Landroid/widget/EditText;)V
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/adyen/ui/fragments/QiwiWalletFragment$1;->this$0:Lcom/adyen/ui/fragments/QiwiWalletFragment;

    iput-object p2, p0, Lcom/adyen/ui/fragments/QiwiWalletFragment$1;->val$countryCode:Landroid/widget/Spinner;

    iput-object p3, p0, Lcom/adyen/ui/fragments/QiwiWalletFragment$1;->val$telephoneNumber:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 96
    iget-object p1, p0, Lcom/adyen/ui/fragments/QiwiWalletFragment$1;->val$countryCode:Landroid/widget/Spinner;

    invoke-virtual {p1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "+"

    .line 97
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const-string v1, ")"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 98
    iget-object v0, p0, Lcom/adyen/ui/fragments/QiwiWalletFragment$1;->this$0:Lcom/adyen/ui/fragments/QiwiWalletFragment;

    invoke-static {v0}, Lcom/adyen/ui/fragments/QiwiWalletFragment;->access$000(Lcom/adyen/ui/fragments/QiwiWalletFragment;)Lcom/adyen/ui/fragments/QiwiWalletFragment$QiwiWalletPaymentDetailsListener;

    move-result-object v0

    iget-object v1, p0, Lcom/adyen/ui/fragments/QiwiWalletFragment$1;->val$telephoneNumber:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/adyen/ui/fragments/QiwiWalletFragment$QiwiWalletPaymentDetailsListener;->onPaymentDetails(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
