.class public final Lcom/adyen/ui/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/ui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f100000

.field public static final abc_action_bar_up_description:I = 0x7f100001

.field public static final abc_action_menu_overflow_description:I = 0x7f100002

.field public static final abc_action_mode_done:I = 0x7f100003

.field public static final abc_activity_chooser_view_see_all:I = 0x7f100004

.field public static final abc_activitychooserview_choose_application:I = 0x7f100005

.field public static final abc_capital_off:I = 0x7f100006

.field public static final abc_capital_on:I = 0x7f100007

.field public static final abc_font_family_body_1_material:I = 0x7f100008

.field public static final abc_font_family_body_2_material:I = 0x7f100009

.field public static final abc_font_family_button_material:I = 0x7f10000a

.field public static final abc_font_family_caption_material:I = 0x7f10000b

.field public static final abc_font_family_display_1_material:I = 0x7f10000c

.field public static final abc_font_family_display_2_material:I = 0x7f10000d

.field public static final abc_font_family_display_3_material:I = 0x7f10000e

.field public static final abc_font_family_display_4_material:I = 0x7f10000f

.field public static final abc_font_family_headline_material:I = 0x7f100010

.field public static final abc_font_family_menu_material:I = 0x7f100011

.field public static final abc_font_family_subhead_material:I = 0x7f100012

.field public static final abc_font_family_title_material:I = 0x7f100013

.field public static final abc_search_hint:I = 0x7f10001e

.field public static final abc_searchview_description_clear:I = 0x7f10001f

.field public static final abc_searchview_description_query:I = 0x7f100020

.field public static final abc_searchview_description_search:I = 0x7f100021

.field public static final abc_searchview_description_submit:I = 0x7f100022

.field public static final abc_searchview_description_voice:I = 0x7f100023

.field public static final abc_shareactionprovider_share_with:I = 0x7f100024

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f100025

.field public static final abc_toolbar_collapse_description:I = 0x7f100026

.field public static final cancelButton:I = 0x7f10002e

.field public static final country_code:I = 0x7f100075

.field public static final creditCard_cvcField_placeholder:I = 0x7f100076

.field public static final creditCard_cvcField_title:I = 0x7f100077

.field public static final creditCard_expiryDateField_invalid:I = 0x7f100078

.field public static final creditCard_expiryDateField_month:I = 0x7f100079

.field public static final creditCard_expiryDateField_month_placeholder:I = 0x7f10007a

.field public static final creditCard_expiryDateField_placeholder:I = 0x7f10007b

.field public static final creditCard_expiryDateField_title:I = 0x7f10007c

.field public static final creditCard_expiryDateField_year:I = 0x7f10007d

.field public static final creditCard_expiryDateField_year_placeholder:I = 0x7f10007e

.field public static final creditCard_holderName:I = 0x7f10007f

.field public static final creditCard_holderName_placeholder:I = 0x7f100080

.field public static final creditCard_installmentsField:I = 0x7f100081

.field public static final creditCard_numberField_invalid:I = 0x7f100082

.field public static final creditCard_numberField_placeholder:I = 0x7f100083

.field public static final creditCard_numberField_title:I = 0x7f100084

.field public static final creditCard_oneClickVerification_invalidInput_message:I = 0x7f100085

.field public static final creditCard_oneClickVerification_invalidInput_title:I = 0x7f100086

.field public static final creditCard_oneClickVerification_message:I = 0x7f100087

.field public static final creditCard_oneClickVerification_title:I = 0x7f100088

.field public static final creditCard_storeDetailsButton:I = 0x7f100089

.field public static final creditCard_success:I = 0x7f10008a

.field public static final creditCard_title:I = 0x7f10008b

.field public static final dismissButton:I = 0x7f10009a

.field public static final error_message_cannotConnectToHost:I = 0x7f10009f

.field public static final error_message_cannotConnectToInternet:I = 0x7f1000a0

.field public static final error_message_unknown:I = 0x7f1000a1

.field public static final error_retryButton:I = 0x7f1000a2

.field public static final error_subtitle_payment:I = 0x7f1000a3

.field public static final error_subtitle_redirect:I = 0x7f1000a4

.field public static final error_subtitle_refused:I = 0x7f1000a5

.field public static final error_title:I = 0x7f1000a6

.field public static final example:I = 0x7f1000a9

.field public static final giropay_minimumLength:I = 0x7f1000d2

.field public static final giropay_noResults:I = 0x7f1000d3

.field public static final giropay_searchField_placeholder:I = 0x7f1000d4

.field public static final hint_telehone_number:I = 0x7f1000db

.field public static final holderName:I = 0x7f1000dc

.field public static final idealIssuer_selectField_placeholder:I = 0x7f1000dd

.field public static final idealIssuer_selectField_title:I = 0x7f1000de

.field public static final oneClick_confirmationAlert_title:I = 0x7f1000ed

.field public static final payButton:I = 0x7f1000f3

.field public static final payButton_formatted:I = 0x7f1000f4

.field public static final paymentMethods_moreMethodsButton:I = 0x7f1000f5

.field public static final paymentMethods_otherMethods:I = 0x7f1000f6

.field public static final paymentMethods_storedMethods:I = 0x7f1000f7

.field public static final paymentMethods_title:I = 0x7f1000f8

.field public static final payment_processing:I = 0x7f1000f9

.field public static final payment_redirecting:I = 0x7f1000fa

.field public static final redirect_cannotOpenApp_appNotInstalledMessage:I = 0x7f10010b

.field public static final redirect_cannotOpenApp_title:I = 0x7f10010c

.field public static final search_menu_title:I = 0x7f10010f

.field public static final sepaDirectDebit_consentButton:I = 0x7f100110

.field public static final sepaDirectDebit_ibanField_invalid:I = 0x7f100111

.field public static final sepaDirectDebit_ibanField_placeholder:I = 0x7f100112

.field public static final sepaDirectDebit_ibanField_title:I = 0x7f100113

.field public static final sepaDirectDebit_nameField_placeholder:I = 0x7f100114

.field public static final sepaDirectDebit_nameField_title:I = 0x7f100115

.field public static final sepa_ibanNumber:I = 0x7f100116

.field public static final sepa_ownerName:I = 0x7f100117

.field public static final status_bar_notification_info_overflow:I = 0x7f100118

.field public static final storeDetails:I = 0x7f100119

.field public static final telephone_number:I = 0x7f10011a


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 839
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
