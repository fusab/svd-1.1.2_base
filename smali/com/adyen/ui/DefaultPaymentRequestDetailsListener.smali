.class public Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;
.super Ljava/lang/Object;
.source "DefaultPaymentRequestDetailsListener.java"

# interfaces
.implements Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "DefaultPaymentRequestDetailsListener"


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .line 54
    sget-object v0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onPaymentDetailsRequired(Lcom/adyen/core/PaymentRequest;Ljava/util/Collection;Lcom/adyen/core/interfaces/PaymentDetailsCallback;)V
    .locals 10
    .param p1    # Lcom/adyen/core/PaymentRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Collection;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/adyen/core/interfaces/PaymentDetailsCallback;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/adyen/core/PaymentRequest;",
            "Ljava/util/Collection<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail;",
            ">;",
            "Lcom/adyen/core/interfaces/PaymentDetailsCallback;",
            ")V"
        }
    .end annotation

    const-string v0, "idealIssuer"

    .line 126
    invoke-static {p2, v0}, Lcom/adyen/core/models/paymentdetails/InputDetailsUtil;->containsKey(Ljava/util/Collection;Ljava/lang/String;)Z

    move-result v0

    const-string v1, "fragment"

    const-string v2, "PaymentMethod"

    if-nez v0, :cond_9

    const-string v0, "issuer"

    .line 127
    invoke-static {p2, v0}, Lcom/adyen/core/models/paymentdetails/InputDetailsUtil;->containsKey(Ljava/util/Collection;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_2

    .line 134
    :cond_0
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getPaymentMethod()Lcom/adyen/core/models/PaymentMethod;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adyen/core/models/PaymentMethod;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v3, "sepadirectdebit"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v3, "amount"

    if-eqz v0, :cond_1

    .line 135
    new-instance p2, Landroid/content/Intent;

    iget-object p3, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    const-class v0, Lcom/adyen/ui/activities/CheckoutActivity;

    invoke-direct {p2, p3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 136
    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    .line 137
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getPaymentMethod()Lcom/adyen/core/models/PaymentMethod;

    move-result-object v0

    invoke-virtual {p3, v2, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 138
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getAmount()Lcom/adyen/core/models/Amount;

    move-result-object p1

    invoke-virtual {p3, v3, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const/4 p1, 0x3

    .line 139
    invoke-virtual {p3, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 140
    invoke-virtual {p2, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 141
    iget-object p1, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_3

    .line 142
    :cond_1
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getPaymentMethod()Lcom/adyen/core/models/PaymentMethod;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adyen/core/models/PaymentMethod;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v4, "card"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v4, "cvc_field_status"

    const-string v5, "generation_time"

    const-string v6, "public_key"

    const-string v7, "shopper_reference"

    const/4 v8, 0x1

    if-eqz v0, :cond_2

    .line 143
    new-instance p2, Landroid/content/Intent;

    iget-object p3, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    const-class v0, Lcom/adyen/ui/activities/CheckoutActivity;

    invoke-direct {p2, p3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 144
    invoke-virtual {p2, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 145
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getPaymentMethod()Lcom/adyen/core/models/PaymentMethod;

    move-result-object p3

    invoke-virtual {p2, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 146
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getAmount()Lcom/adyen/core/models/Amount;

    move-result-object p3

    invoke-virtual {p2, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 147
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getShopperReference()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, v7, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getPublicKey()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, v6, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 149
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getGenerationTime()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 150
    sget-object p1, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->REQUIRED:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    invoke-virtual {p1}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "payment_card_scan_enabled"

    .line 151
    invoke-virtual {p2, p1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 152
    iget-object p1, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_3

    .line 153
    :cond_2
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getPaymentMethod()Lcom/adyen/core/models/PaymentMethod;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adyen/core/models/PaymentMethod;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v9, "bcmc"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 154
    new-instance p2, Landroid/content/Intent;

    iget-object p3, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    const-class v0, Lcom/adyen/ui/activities/CheckoutActivity;

    invoke-direct {p2, p3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 155
    invoke-virtual {p2, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 156
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getPaymentMethod()Lcom/adyen/core/models/PaymentMethod;

    move-result-object p3

    invoke-virtual {p2, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 157
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getAmount()Lcom/adyen/core/models/Amount;

    move-result-object p3

    invoke-virtual {p2, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 158
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getShopperReference()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, v7, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getPublicKey()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, v6, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getGenerationTime()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 161
    sget-object p1, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->NOCVC:Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;

    invoke-virtual {p1}, Lcom/adyen/ui/fragments/CreditCardFragmentBuilder$CvcFieldStatus;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 162
    iget-object p1, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_3

    .line 163
    :cond_3
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getPaymentMethod()Lcom/adyen/core/models/PaymentMethod;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adyen/core/models/PaymentMethod;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v4, "qiwiwallet"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 164
    new-instance p2, Landroid/content/Intent;

    iget-object p3, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    const-class v0, Lcom/adyen/ui/activities/CheckoutActivity;

    invoke-direct {p2, p3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 165
    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    .line 166
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getPaymentMethod()Lcom/adyen/core/models/PaymentMethod;

    move-result-object v0

    invoke-virtual {p3, v2, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 167
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getAmount()Lcom/adyen/core/models/Amount;

    move-result-object p1

    invoke-virtual {p3, v3, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const/4 p1, 0x5

    .line 168
    invoke-virtual {p3, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 169
    invoke-virtual {p2, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 170
    iget-object p1, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_3

    .line 171
    :cond_4
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getPaymentMethod()Lcom/adyen/core/models/PaymentMethod;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adyen/core/models/PaymentMethod;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "paypal"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 173
    new-instance p1, Lcom/adyen/core/models/paymentdetails/PaymentDetails;

    invoke-direct {p1, p2}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;-><init>(Ljava/util/Collection;)V

    invoke-interface {p3, p1}, Lcom/adyen/core/interfaces/PaymentDetailsCallback;->completionWithPaymentDetails(Lcom/adyen/core/models/paymentdetails/PaymentDetails;)V

    goto/16 :goto_3

    :cond_5
    const-string v0, "cardDetails.cvc"

    .line 174
    invoke-static {p2, v0}, Lcom/adyen/core/models/paymentdetails/InputDetailsUtil;->containsKey(Ljava/util/Collection;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 175
    new-instance p2, Landroid/content/Intent;

    iget-object p3, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    const-class v0, Lcom/adyen/ui/activities/TranslucentDialogActivity;

    invoke-direct {p2, p3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 176
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getAmount()Lcom/adyen/core/models/Amount;

    move-result-object p3

    invoke-virtual {p2, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 177
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getPaymentMethod()Lcom/adyen/core/models/PaymentMethod;

    move-result-object p1

    invoke-virtual {p2, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/high16 p1, 0x10000

    .line 178
    invoke-virtual {p2, p1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 179
    iget-object p1, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_3

    .line 180
    :cond_6
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getPaymentMethod()Lcom/adyen/core/models/PaymentMethod;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adyen/core/models/PaymentMethod;->getPaymentModule()Lcom/adyen/core/models/PaymentModule;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 p2, 0x0

    .line 184
    :try_start_0
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getPaymentMethod()Lcom/adyen/core/models/PaymentMethod;

    move-result-object p3

    invoke-virtual {p3}, Lcom/adyen/core/models/PaymentMethod;->getPaymentModule()Lcom/adyen/core/models/PaymentModule;

    move-result-object p3

    .line 183
    invoke-static {p3}, Lcom/adyen/core/internals/ModuleAvailabilityUtil;->getModulePaymentService(Lcom/adyen/core/models/PaymentModule;)Lcom/adyen/core/services/PaymentMethodService;

    move-result-object p3
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p3

    .line 192
    sget-object v0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->TAG:Ljava/lang/String;

    const-string v1, "requestPaymentMethodDetails(): Null pointer exception: "

    invoke-static {v0, v1, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception p3

    .line 190
    sget-object v0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->TAG:Ljava/lang/String;

    const-string v1, "requestPaymentMethodDetails(): InstantiationException occurred"

    invoke-static {v0, v1, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_2
    move-exception p3

    .line 188
    sget-object v0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->TAG:Ljava/lang/String;

    const-string v1, "requestPaymentMethodDetails(): IllegalAccessException occurred"

    invoke-static {v0, v1, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_3
    move-exception p3

    .line 186
    sget-object v0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->TAG:Ljava/lang/String;

    const-string v1, "requestPaymentMethodDetails(): Payment module not found."

    invoke-static {v0, v1, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    move-object p3, p2

    :goto_1
    if-nez p3, :cond_7

    .line 196
    iget-object p1, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    const-string p2, "Payment method not supported."

    invoke-static {p1, p2, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_3

    .line 199
    :cond_7
    iget-object v0, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getPaymentRequestListener()Lcom/adyen/core/interfaces/PaymentRequestListener;

    move-result-object v1

    invoke-interface {p3, v0, p1, v1, p2}, Lcom/adyen/core/services/PaymentMethodService;->process(Landroid/content/Context;Lcom/adyen/core/PaymentRequest;Lcom/adyen/core/interfaces/PaymentRequestListener;Lcom/adyen/core/interfaces/PaymentRequestDetailsListener;)V

    goto :goto_3

    .line 203
    :cond_8
    new-instance p1, Lcom/adyen/core/models/paymentdetails/PaymentDetails;

    invoke-direct {p1, p2}, Lcom/adyen/core/models/paymentdetails/PaymentDetails;-><init>(Ljava/util/Collection;)V

    invoke-interface {p3, p1}, Lcom/adyen/core/interfaces/PaymentDetailsCallback;->completionWithPaymentDetails(Lcom/adyen/core/models/paymentdetails/PaymentDetails;)V

    goto :goto_3

    .line 128
    :cond_9
    :goto_2
    new-instance p2, Landroid/content/Intent;

    iget-object p3, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    const-class v0, Lcom/adyen/ui/activities/CheckoutActivity;

    invoke-direct {p2, p3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 129
    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    .line 130
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getPaymentMethod()Lcom/adyen/core/models/PaymentMethod;

    move-result-object p1

    invoke-virtual {p3, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const/4 p1, 0x2

    .line 131
    invoke-virtual {p3, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 132
    invoke-virtual {p2, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 133
    iget-object p1, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :goto_3
    return-void
.end method

.method public onPaymentMethodSelectionRequired(Lcom/adyen/core/PaymentRequest;Ljava/util/List;Ljava/util/List;Lcom/adyen/core/interfaces/PaymentMethodCallback;)V
    .locals 2
    .param p1    # Lcom/adyen/core/PaymentRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/adyen/core/interfaces/PaymentMethodCallback;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/adyen/core/PaymentRequest;",
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;",
            "Ljava/util/List<",
            "Lcom/adyen/core/models/PaymentMethod;",
            ">;",
            "Lcom/adyen/core/interfaces/PaymentMethodCallback;",
            ")V"
        }
    .end annotation

    .line 70
    new-instance p4, Landroid/content/Intent;

    iget-object v0, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/adyen/ui/activities/CheckoutActivity;

    invoke-direct {p4, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 71
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 72
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 73
    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 74
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 75
    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const-string p3, "preferredPaymentMethods"

    .line 77
    invoke-virtual {v0, p3, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string p3, "filteredPaymentMethods"

    .line 78
    invoke-virtual {v0, p3, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string p2, "fragment"

    const/4 p3, 0x0

    .line 79
    invoke-virtual {v0, p2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 80
    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->getAmount()Lcom/adyen/core/models/Amount;

    move-result-object p1

    const-string p2, "amount"

    invoke-virtual {v0, p2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 82
    invoke-virtual {p4, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 84
    iget-object p1, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    invoke-virtual {p1, p4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public onRedirectRequired(Lcom/adyen/core/PaymentRequest;Ljava/lang/String;Lcom/adyen/core/interfaces/UriCallback;)V
    .locals 3
    .param p1    # Lcom/adyen/core/PaymentRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/adyen/core/interfaces/UriCallback;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 91
    sget-object v0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->TAG:Ljava/lang/String;

    const-string v1, "Checkout SDK will handle redirection"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.adyen.core.RedirectHandled"

    .line 93
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.adyen.core.RedirectProblem"

    .line 94
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 95
    iget-object v1, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    invoke-static {v1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object v1

    new-instance v2, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener$1;

    invoke-direct {v2, p0, p3, p1}, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener$1;-><init>(Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;Lcom/adyen/core/interfaces/UriCallback;Lcom/adyen/core/PaymentRequest;)V

    invoke-virtual {v1, v2, v0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 112
    iget-object p1, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    .line 113
    new-instance p3, Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    const-class v1, Lcom/adyen/ui/activities/RedirectHandlerActivity;

    invoke-direct {p3, v0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v0, 0x1

    invoke-virtual {p1, p3, v0, v0}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 116
    new-instance p1, Landroid/content/Intent;

    iget-object p3, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    const-class v0, Lcom/adyen/ui/activities/RedirectHandlerActivity;

    invoke-direct {p1, p3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo p3, "url"

    .line 117
    invoke-virtual {p1, p3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    iget-object p2, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->context:Landroid/content/Context;

    invoke-virtual {p2, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
