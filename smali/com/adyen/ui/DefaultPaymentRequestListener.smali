.class public Lcom/adyen/ui/DefaultPaymentRequestListener;
.super Ljava/lang/Object;
.source "DefaultPaymentRequestListener.java"

# interfaces
.implements Lcom/adyen/core/interfaces/PaymentRequestListener;


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/adyen/ui/DefaultPaymentRequestListener;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public onPaymentDataRequested(Lcom/adyen/core/PaymentRequest;Ljava/lang/String;Lcom/adyen/core/interfaces/PaymentDataCallback;)V
    .locals 1
    .param p1    # Lcom/adyen/core/PaymentRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/adyen/core/interfaces/PaymentDataCallback;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 30
    new-instance p1, Landroid/content/Intent;

    iget-object p2, p0, Lcom/adyen/ui/DefaultPaymentRequestListener;->context:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    const-class p3, Lcom/adyen/ui/activities/CheckoutActivity;

    invoke-direct {p1, p2, p3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 31
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    const-string p3, "fragment"

    const/16 v0, 0xb

    .line 32
    invoke-virtual {p2, p3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 33
    invoke-virtual {p1, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 34
    iget-object p2, p0, Lcom/adyen/ui/DefaultPaymentRequestListener;->context:Landroid/content/Context;

    invoke-virtual {p2, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public onPaymentResult(Lcom/adyen/core/PaymentRequest;Lcom/adyen/core/models/PaymentRequestResult;)V
    .locals 0
    .param p1    # Lcom/adyen/core/PaymentRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/adyen/core/models/PaymentRequestResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 39
    new-instance p1, Landroid/content/Intent;

    const-string p2, "com.adyen.core.ui.finish"

    invoke-direct {p1, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 40
    iget-object p2, p0, Lcom/adyen/ui/DefaultPaymentRequestListener;->context:Landroid/content/Context;

    invoke-static {p2}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    return-void
.end method
