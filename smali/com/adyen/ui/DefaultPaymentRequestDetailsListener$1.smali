.class Lcom/adyen/ui/DefaultPaymentRequestDetailsListener$1;
.super Landroid/content/BroadcastReceiver;
.source "DefaultPaymentRequestDetailsListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->onRedirectRequired(Lcom/adyen/core/PaymentRequest;Ljava/lang/String;Lcom/adyen/core/interfaces/UriCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;

.field final synthetic val$paymentRequest:Lcom/adyen/core/PaymentRequest;

.field final synthetic val$uriCallback:Lcom/adyen/core/interfaces/UriCallback;


# direct methods
.method constructor <init>(Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;Lcom/adyen/core/interfaces/UriCallback;Lcom/adyen/core/PaymentRequest;)V
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener$1;->this$0:Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;

    iput-object p2, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener$1;->val$uriCallback:Lcom/adyen/core/interfaces/UriCallback;

    iput-object p3, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener$1;->val$paymentRequest:Lcom/adyen/core/PaymentRequest;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .line 98
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.adyen.core.RedirectHandled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p2

    const-string v0, "returnUri"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/net/Uri;

    .line 100
    invoke-static {}, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RedirectHandled(redirection handled by Checkout SDK): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    iget-object v0, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener$1;->val$uriCallback:Lcom/adyen/core/interfaces/UriCallback;

    invoke-interface {v0, p2}, Lcom/adyen/core/interfaces/UriCallback;->completionWithUri(Landroid/net/Uri;)V

    .line 102
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p2

    .line 103
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/adyen/ui/activities/RedirectHandlerActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {p2, v0, v1, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 105
    invoke-static {p1}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroidx/localbroadcastmanager/content/LocalBroadcastManager;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroidx/localbroadcastmanager/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0

    .line 107
    :cond_0
    iget-object p1, p0, Lcom/adyen/ui/DefaultPaymentRequestDetailsListener$1;->val$paymentRequest:Lcom/adyen/core/PaymentRequest;

    invoke-virtual {p1}, Lcom/adyen/core/PaymentRequest;->cancel()V

    :goto_0
    return-void
.end method
