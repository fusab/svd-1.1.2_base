.class Lcom/adyen/ui/adapters/PaymentListAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "PaymentListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/ui/adapters/PaymentListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolder"
.end annotation


# instance fields
.field private imageView:Landroid/widget/ImageView;

.field private paymentMethodNameView:Landroid/widget/TextView;

.field private url:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/adyen/ui/adapters/PaymentListAdapter$1;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/adyen/ui/adapters/PaymentListAdapter$ViewHolder;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/adyen/ui/adapters/PaymentListAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/adyen/ui/adapters/PaymentListAdapter$ViewHolder;->paymentMethodNameView:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$102(Lcom/adyen/ui/adapters/PaymentListAdapter$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/adyen/ui/adapters/PaymentListAdapter$ViewHolder;->paymentMethodNameView:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$200(Lcom/adyen/ui/adapters/PaymentListAdapter$ViewHolder;)Landroid/widget/ImageView;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/adyen/ui/adapters/PaymentListAdapter$ViewHolder;->imageView:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic access$202(Lcom/adyen/ui/adapters/PaymentListAdapter$ViewHolder;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/adyen/ui/adapters/PaymentListAdapter$ViewHolder;->imageView:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$300(Lcom/adyen/ui/adapters/PaymentListAdapter$ViewHolder;)Ljava/lang/String;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/adyen/ui/adapters/PaymentListAdapter$ViewHolder;->url:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$302(Lcom/adyen/ui/adapters/PaymentListAdapter$ViewHolder;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/adyen/ui/adapters/PaymentListAdapter$ViewHolder;->url:Ljava/lang/String;

    return-object p1
.end method
