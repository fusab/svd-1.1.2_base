.class public Lcom/adyen/ui/adapters/InstallmentOptionsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "InstallmentOptionsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter<",
        "Lcom/adyen/core/models/paymentdetails/InputDetail$Item;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/app/Activity;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private final installmentOptions:Ljava/util/List;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail$Item;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/util/List;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/List<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail$Item;",
            ">;)V"
        }
    .end annotation

    const v0, 0x1090003

    .line 33
    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 34
    iput-object p1, p0, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter;->context:Landroid/app/Activity;

    .line 35
    iput-object p2, p0, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter;->installmentOptions:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    .line 70
    invoke-virtual {p0, p1, p2, p3}, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getItem(I)Lcom/adyen/core/models/paymentdetails/InputDetail$Item;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter;->installmentOptions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;

    return-object p1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 21
    invoke-virtual {p0, p1}, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter;->getItem(I)Lcom/adyen/core/models/paymentdetails/InputDetail$Item;

    move-result-object p1

    return-object p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p2    # Landroid/view/View;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    if-nez p2, :cond_0

    .line 49
    new-instance p2, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$ViewHolder;

    const/4 v0, 0x0

    invoke-direct {p2, v0}, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$ViewHolder;-><init>(Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$1;)V

    .line 50
    iget-object v0, p0, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter;->context:Landroid/app/Activity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x1090003

    const/4 v2, 0x0

    .line 52
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    const v0, 0x1020014

    .line 54
    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p2, v0}, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$ViewHolder;->access$102(Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 55
    invoke-virtual {p3, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 57
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$ViewHolder;

    move-object v3, p3

    move-object p3, p2

    move-object p2, v3

    :goto_0
    if-eqz p2, :cond_1

    .line 61
    invoke-static {p2}, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$ViewHolder;->access$100(Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 62
    invoke-static {p2}, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$ViewHolder;->access$100(Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object p2

    iget-object v0, p0, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter;->installmentOptions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;

    invoke-virtual {p1}, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-object p3
.end method
