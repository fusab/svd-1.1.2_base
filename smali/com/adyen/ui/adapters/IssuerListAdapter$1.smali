.class Lcom/adyen/ui/adapters/IssuerListAdapter$1;
.super Ljava/lang/Object;
.source "IssuerListAdapter.java"

# interfaces
.implements Lcom/adyen/core/utils/AsyncImageDownloader$ImageListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adyen/ui/adapters/IssuerListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adyen/ui/adapters/IssuerListAdapter;

.field final synthetic val$viewHolder:Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;


# direct methods
.method constructor <init>(Lcom/adyen/ui/adapters/IssuerListAdapter;Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;)V
    .locals 0

    .line 78
    iput-object p1, p0, Lcom/adyen/ui/adapters/IssuerListAdapter$1;->this$0:Lcom/adyen/ui/adapters/IssuerListAdapter;

    iput-object p2, p0, Lcom/adyen/ui/adapters/IssuerListAdapter$1;->val$viewHolder:Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onImage(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/adyen/ui/adapters/IssuerListAdapter$1;->val$viewHolder:Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;

    invoke-static {v0}, Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;->access$300(Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 82
    iget-object p2, p0, Lcom/adyen/ui/adapters/IssuerListAdapter$1;->val$viewHolder:Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;

    invoke-static {p2}, Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;->access$200(Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method
