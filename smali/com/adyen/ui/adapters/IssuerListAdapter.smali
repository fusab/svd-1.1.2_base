.class public Lcom/adyen/ui/adapters/IssuerListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "IssuerListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter<",
        "Lcom/adyen/core/models/paymentdetails/InputDetail$Item;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "IssuerListAdapter"


# instance fields
.field private final context:Landroid/app/Activity;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private final issuers:Ljava/util/List;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail$Item;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/util/List;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/List<",
            "Lcom/adyen/core/models/paymentdetails/InputDetail$Item;",
            ">;)V"
        }
    .end annotation

    .line 43
    sget v0, Lcom/adyen/ui/R$layout;->payment_method_list:I

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 44
    sget-object v0, Lcom/adyen/ui/adapters/IssuerListAdapter;->TAG:Ljava/lang/String;

    const-string v1, "IssuerListAdapter()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    iput-object p1, p0, Lcom/adyen/ui/adapters/IssuerListAdapter;->context:Landroid/app/Activity;

    .line 46
    iput-object p2, p0, Lcom/adyen/ui/adapters/IssuerListAdapter;->issuers:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getItem(I)Lcom/adyen/core/models/paymentdetails/InputDetail$Item;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/adyen/ui/adapters/IssuerListAdapter;->issuers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;

    return-object p1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 27
    invoke-virtual {p0, p1}, Lcom/adyen/ui/adapters/IssuerListAdapter;->getItem(I)Lcom/adyen/core/models/paymentdetails/InputDetail$Item;

    move-result-object p1

    return-object p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/View;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const/4 v0, 0x0

    if-nez p2, :cond_0

    .line 60
    new-instance p2, Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;

    invoke-direct {p2, v0}, Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;-><init>(Lcom/adyen/ui/adapters/IssuerListAdapter$1;)V

    .line 61
    iget-object v1, p0, Lcom/adyen/ui/adapters/IssuerListAdapter;->context:Landroid/app/Activity;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 63
    sget v2, Lcom/adyen/ui/R$layout;->payment_method_list:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    .line 65
    sget v1, Lcom/adyen/ui/R$id;->paymentMethodName:I

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {p2, v1}, Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;->access$102(Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 66
    sget v1, Lcom/adyen/ui/R$id;->paymentMethodLogo:I

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-static {p2, v1}, Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;->access$202(Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 67
    invoke-virtual {p3, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 69
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;

    move-object v4, p3

    move-object p3, p2

    move-object p2, v4

    :goto_0
    if-eqz p2, :cond_1

    .line 73
    invoke-static {p2}, Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;->access$100(Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {p2}, Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;->access$200(Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 74
    invoke-static {p2}, Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;->access$100(Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/adyen/ui/adapters/IssuerListAdapter;->issuers:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;

    invoke-virtual {v2}, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v1, p0, Lcom/adyen/ui/adapters/IssuerListAdapter;->context:Landroid/app/Activity;

    iget-object v2, p0, Lcom/adyen/ui/adapters/IssuerListAdapter;->issuers:Ljava/util/List;

    .line 76
    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;

    invoke-virtual {p1}, Lcom/adyen/core/models/paymentdetails/InputDetail$Item;->getImageUrl()Ljava/lang/String;

    move-result-object p1

    .line 75
    invoke-static {v1, p1}, Lcom/adyen/ui/utils/IconUtil;->addScaleFactorToIconUrl(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 77
    invoke-static {p2, p1}, Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;->access$302(Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;Ljava/lang/String;)Ljava/lang/String;

    .line 78
    invoke-virtual {p0}, Lcom/adyen/ui/adapters/IssuerListAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/adyen/ui/adapters/IssuerListAdapter$1;

    invoke-direct {v2, p0, p2}, Lcom/adyen/ui/adapters/IssuerListAdapter$1;-><init>(Lcom/adyen/ui/adapters/IssuerListAdapter;Lcom/adyen/ui/adapters/IssuerListAdapter$ViewHolder;)V

    invoke-static {v1, v2, p1, v0}, Lcom/adyen/core/utils/AsyncImageDownloader;->downloadImage(Landroid/content/Context;Lcom/adyen/core/utils/AsyncImageDownloader$ImageListener;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    :cond_1
    return-object p3
.end method
