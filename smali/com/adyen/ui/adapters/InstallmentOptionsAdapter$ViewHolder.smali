.class Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "InstallmentOptionsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/ui/adapters/InstallmentOptionsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolder"
.end annotation


# instance fields
.field private installmentOption:Landroid/widget/TextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$1;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$ViewHolder;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$ViewHolder;->installmentOption:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$102(Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/adyen/ui/adapters/InstallmentOptionsAdapter$ViewHolder;->installmentOption:Landroid/widget/TextView;

    return-object p1
.end method
