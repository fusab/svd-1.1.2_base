.class public final enum Lcom/adyen/utils/CardType;
.super Ljava/lang/Enum;
.source "CardType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/adyen/utils/CardType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/adyen/utils/CardType;

.field public static final enum UNKNOWN:Lcom/adyen/utils/CardType;

.field public static final enum amex:Lcom/adyen/utils/CardType;

.field public static final enum bcmc:Lcom/adyen/utils/CardType;

.field public static final enum bijcard:Lcom/adyen/utils/CardType;

.field public static final enum cartebancaire:Lcom/adyen/utils/CardType;

.field public static final enum codensa:Lcom/adyen/utils/CardType;

.field public static final enum cup:Lcom/adyen/utils/CardType;

.field public static final enum dankort:Lcom/adyen/utils/CardType;

.field public static final enum diners:Lcom/adyen/utils/CardType;

.field public static final enum discover:Lcom/adyen/utils/CardType;

.field public static final enum elo:Lcom/adyen/utils/CardType;

.field public static final enum hiper:Lcom/adyen/utils/CardType;

.field public static final enum hipercard:Lcom/adyen/utils/CardType;

.field public static final enum jcb:Lcom/adyen/utils/CardType;

.field public static final enum karenmillen:Lcom/adyen/utils/CardType;

.field public static final enum maestro:Lcom/adyen/utils/CardType;

.field public static final enum maestrouk:Lcom/adyen/utils/CardType;

.field public static final enum mc:Lcom/adyen/utils/CardType;

.field public static final enum mcalphabankbonus:Lcom/adyen/utils/CardType;

.field public static final enum mir:Lcom/adyen/utils/CardType;

.field public static final enum oasis:Lcom/adyen/utils/CardType;

.field public static final enum solo:Lcom/adyen/utils/CardType;

.field public static final enum uatp:Lcom/adyen/utils/CardType;

.field public static final enum visa:Lcom/adyen/utils/CardType;

.field public static final enum visaalphabankbonus:Lcom/adyen/utils/CardType;

.field public static final enum visadankort:Lcom/adyen/utils/CardType;

.field public static final enum warehouse:Lcom/adyen/utils/CardType;


# instance fields
.field private numberOfDigits:[Ljava/lang/Integer;

.field private pattern:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 11
    new-instance v0, Lcom/adyen/utils/CardType;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Integer;

    const/16 v3, 0xf

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v2, v5

    const-string v6, "amex"

    const-string v7, "^3[47][0-9]{0,13}$"

    invoke-direct {v0, v6, v5, v7, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->amex:Lcom/adyen/utils/CardType;

    .line 12
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    const/16 v6, 0xe

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v2, v5

    const-string v8, "diners"

    const-string v9, "^(36)[0-9]{0,12}$"

    invoke-direct {v0, v8, v1, v9, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->diners:Lcom/adyen/utils/CardType;

    .line 13
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    const/16 v8, 0x10

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v2, v5

    const/4 v10, 0x2

    const-string v11, "discover"

    const-string v12, "^(6011[0-9]{0,12}|(644|645|646|647|648|649)[0-9]{0,13}|65[0-9]{0,14})$"

    invoke-direct {v0, v11, v10, v12, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->discover:Lcom/adyen/utils/CardType;

    .line 14
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    const/4 v11, 0x3

    const-string v12, "elo"

    const-string v13, "^((((506699)|(506770)|(506771)|(506772)|(506773)|(506774)|(506775)|(506776)|(506777)|(506778)|(401178)|(438935)|(451416)|(457631)|(457632)|(504175)|(627780)|(636368)|(636297))[0-9]{0,10})|((50676)|(50675)|(50674)|(50673)|(50672)|(50671)|(50670))[0-9]{0,11})$"

    .line 16
    invoke-direct {v0, v12, v11, v13, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->elo:Lcom/adyen/utils/CardType;

    .line 17
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    const/4 v12, 0x4

    const-string v13, "hipercard"

    const-string v14, "^(606282)[0-9]{0,10}$"

    invoke-direct {v0, v13, v12, v14, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->hipercard:Lcom/adyen/utils/CardType;

    .line 18
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v10, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    const/16 v13, 0x13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v2, v1

    const/4 v15, 0x5

    const-string v13, "jcb"

    const-string v8, "^(352[8,9]{1}[0-9]{0,15}|35[4-8]{1}[0-9]{0,16})$"

    invoke-direct {v0, v13, v15, v8, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->jcb:Lcom/adyen/utils/CardType;

    .line 19
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    const/4 v8, 0x6

    const-string v13, "bijcard"

    const-string v3, "^(5100081)[0-9]{0,9}$"

    invoke-direct {v0, v13, v8, v3, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->bijcard:Lcom/adyen/utils/CardType;

    .line 20
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v11, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    const/16 v3, 0x12

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v2, v1

    aput-object v14, v2, v10

    const/4 v3, 0x7

    const-string v6, "maestrouk"

    const-string v15, "^(6759)[0-9]{0,15}$"

    invoke-direct {v0, v6, v3, v15, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->maestrouk:Lcom/adyen/utils/CardType;

    .line 21
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v11, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    aput-object v13, v2, v1

    aput-object v14, v2, v10

    const-string v6, "solo"

    const/16 v15, 0x8

    const-string v3, "^(6767)[0-9]{0,15}$"

    invoke-direct {v0, v6, v15, v3, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->solo:Lcom/adyen/utils/CardType;

    .line 22
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v12, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    const/16 v3, 0x11

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v1

    aput-object v13, v2, v10

    aput-object v14, v2, v11

    const-string v15, "bcmc"

    const/16 v3, 0x9

    const-string v12, "^((6703)[0-9]{0,15}|(479658|606005)[0-9]{0,13})$"

    invoke-direct {v0, v15, v3, v12, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->bcmc:Lcom/adyen/utils/CardType;

    .line 23
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    const-string v3, "dankort"

    const/16 v12, 0xa

    const-string v15, "^(5019)[0-9]{0,12}$"

    invoke-direct {v0, v3, v12, v15, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->dankort:Lcom/adyen/utils/CardType;

    .line 24
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v4, v2, v5

    const-string v3, "uatp"

    const/16 v12, 0xb

    const-string v15, "^1[0-9]{0,14}$"

    invoke-direct {v0, v3, v12, v15, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->uatp:Lcom/adyen/utils/CardType;

    .line 25
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v8, [Ljava/lang/Integer;

    aput-object v7, v2, v5

    aput-object v4, v2, v1

    aput-object v9, v2, v10

    aput-object v6, v2, v11

    const/4 v3, 0x4

    aput-object v13, v2, v3

    const/4 v3, 0x5

    aput-object v14, v2, v3

    const-string v3, "cup"

    const/16 v4, 0xc

    const-string v7, "^(62)[0-9]{0,17}$"

    invoke-direct {v0, v3, v4, v7, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->cup:Lcom/adyen/utils/CardType;

    .line 26
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    const/16 v3, 0xd

    const-string v4, "codensa"

    const-string v7, "^(590712)[0-9]{0,10}$"

    invoke-direct {v0, v4, v3, v7, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->codensa:Lcom/adyen/utils/CardType;

    .line 27
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    const-string/jumbo v4, "visaalphabankbonus"

    const-string v7, "^(450903)[0-9]{0,10}$"

    const/16 v12, 0xe

    invoke-direct {v0, v4, v12, v7, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->visaalphabankbonus:Lcom/adyen/utils/CardType;

    .line 28
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    const-string/jumbo v4, "visadankort"

    const-string v7, "^(4571)[0-9]{0,12}$"

    const/16 v12, 0xf

    invoke-direct {v0, v4, v12, v7, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->visadankort:Lcom/adyen/utils/CardType;

    .line 29
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    const-string v4, "mcalphabankbonus"

    const-string v7, "^(510099)[0-9]{0,10}$"

    const/16 v12, 0x10

    invoke-direct {v0, v4, v12, v7, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->mcalphabankbonus:Lcom/adyen/utils/CardType;

    .line 30
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    const-string v4, "hiper"

    const-string v7, "^(637095|637599|637609|637612)[0-9]{0,10}$"

    const/16 v12, 0x11

    invoke-direct {v0, v4, v12, v7, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->hiper:Lcom/adyen/utils/CardType;

    .line 31
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    const-string v4, "oasis"

    const-string v7, "^(982616)[0-9]{0,10}$"

    const/16 v12, 0x12

    invoke-direct {v0, v4, v12, v7, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->oasis:Lcom/adyen/utils/CardType;

    .line 32
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    const-string v4, "karenmillen"

    const-string v7, "^(98261465)[0-9]{0,8}$"

    const/16 v12, 0x13

    invoke-direct {v0, v4, v12, v7, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->karenmillen:Lcom/adyen/utils/CardType;

    .line 33
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    const-string/jumbo v4, "warehouse"

    const/16 v7, 0x14

    const-string v12, "^(982633)[0-9]{0,10}$"

    invoke-direct {v0, v4, v7, v12, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->warehouse:Lcom/adyen/utils/CardType;

    .line 34
    new-instance v0, Lcom/adyen/utils/CardType;

    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/Integer;

    aput-object v9, v4, v5

    aput-object v6, v4, v1

    aput-object v13, v4, v10

    aput-object v14, v4, v11

    const-string v2, "mir"

    const/16 v7, 0x15

    const-string v12, "^(220)[0-9]{0,16}$"

    invoke-direct {v0, v2, v7, v12, v4}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->mir:Lcom/adyen/utils/CardType;

    .line 35
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v10, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v5

    aput-object v9, v2, v1

    const-string/jumbo v4, "visa"

    const/16 v7, 0x16

    const-string v12, "^4[0-9]{0,16}$"

    invoke-direct {v0, v4, v7, v12, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->visa:Lcom/adyen/utils/CardType;

    .line 36
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    const-string v4, "mc"

    const/16 v7, 0x17

    const-string v12, "^(5[1-5][0-9]{0,14}|2[2-7][0-9]{0,14})$"

    invoke-direct {v0, v4, v7, v12, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->mc:Lcom/adyen/utils/CardType;

    .line 37
    new-instance v0, Lcom/adyen/utils/CardType;

    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/Integer;

    aput-object v9, v4, v5

    aput-object v6, v4, v1

    aput-object v13, v4, v10

    aput-object v14, v4, v11

    const-string v2, "maestro"

    const/16 v6, 0x18

    const-string v7, "^(5[6-8][0-9]{0,17}|6[0-9]{0,18})$"

    invoke-direct {v0, v2, v6, v7, v4}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->maestro:Lcom/adyen/utils/CardType;

    .line 38
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v9, v2, v5

    const-string v4, "cartebancaire"

    const/16 v6, 0x19

    const-string v7, "^[4-6][0-9]{0,15}$"

    invoke-direct {v0, v4, v6, v7, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->cartebancaire:Lcom/adyen/utils/CardType;

    .line 39
    new-instance v0, Lcom/adyen/utils/CardType;

    new-array v2, v1, [Ljava/lang/Integer;

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v5

    const-string v4, "UNKNOWN"

    const/16 v6, 0x1a

    const-string v7, ""

    invoke-direct {v0, v4, v6, v7, v2}, Lcom/adyen/utils/CardType;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V

    sput-object v0, Lcom/adyen/utils/CardType;->UNKNOWN:Lcom/adyen/utils/CardType;

    const/16 v0, 0x1b

    .line 9
    new-array v0, v0, [Lcom/adyen/utils/CardType;

    sget-object v2, Lcom/adyen/utils/CardType;->amex:Lcom/adyen/utils/CardType;

    aput-object v2, v0, v5

    sget-object v2, Lcom/adyen/utils/CardType;->diners:Lcom/adyen/utils/CardType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/adyen/utils/CardType;->discover:Lcom/adyen/utils/CardType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/adyen/utils/CardType;->elo:Lcom/adyen/utils/CardType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/adyen/utils/CardType;->hipercard:Lcom/adyen/utils/CardType;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->jcb:Lcom/adyen/utils/CardType;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->bijcard:Lcom/adyen/utils/CardType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/adyen/utils/CardType;->maestrouk:Lcom/adyen/utils/CardType;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->solo:Lcom/adyen/utils/CardType;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->bcmc:Lcom/adyen/utils/CardType;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->dankort:Lcom/adyen/utils/CardType;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->uatp:Lcom/adyen/utils/CardType;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->cup:Lcom/adyen/utils/CardType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->codensa:Lcom/adyen/utils/CardType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/adyen/utils/CardType;->visaalphabankbonus:Lcom/adyen/utils/CardType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->visadankort:Lcom/adyen/utils/CardType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->mcalphabankbonus:Lcom/adyen/utils/CardType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->hiper:Lcom/adyen/utils/CardType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->oasis:Lcom/adyen/utils/CardType;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->karenmillen:Lcom/adyen/utils/CardType;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->warehouse:Lcom/adyen/utils/CardType;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->mir:Lcom/adyen/utils/CardType;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->visa:Lcom/adyen/utils/CardType;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->mc:Lcom/adyen/utils/CardType;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->maestro:Lcom/adyen/utils/CardType;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->cartebancaire:Lcom/adyen/utils/CardType;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/CardType;->UNKNOWN:Lcom/adyen/utils/CardType;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sput-object v0, Lcom/adyen/utils/CardType;->$VALUES:[Lcom/adyen/utils/CardType;

    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/Integer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    invoke-static {p3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p1

    iput-object p1, p0, Lcom/adyen/utils/CardType;->pattern:Ljava/util/regex/Pattern;

    .line 46
    iput-object p4, p0, Lcom/adyen/utils/CardType;->numberOfDigits:[Ljava/lang/Integer;

    return-void
.end method

.method public static detect(Ljava/lang/String;Ljava/util/List;)Lcom/adyen/utils/CardType;
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/adyen/utils/CardType;"
        }
    .end annotation

    .line 50
    invoke-static {}, Lcom/adyen/utils/CardType;->values()[Lcom/adyen/utils/CardType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    .line 51
    iget-object v4, v3, Lcom/adyen/utils/CardType;->pattern:Ljava/util/regex/Pattern;

    if-nez v4, :cond_0

    goto :goto_1

    .line 55
    :cond_0
    invoke-virtual {v4, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/adyen/utils/CardType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    return-object v3

    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 59
    :cond_2
    sget-object p0, Lcom/adyen/utils/CardType;->UNKNOWN:Lcom/adyen/utils/CardType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/adyen/utils/CardType;
    .locals 1

    .line 9
    const-class v0, Lcom/adyen/utils/CardType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/adyen/utils/CardType;

    return-object p0
.end method

.method public static values()[Lcom/adyen/utils/CardType;
    .locals 1

    .line 9
    sget-object v0, Lcom/adyen/utils/CardType;->$VALUES:[Lcom/adyen/utils/CardType;

    invoke-virtual {v0}, [Lcom/adyen/utils/CardType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/adyen/utils/CardType;

    return-object v0
.end method


# virtual methods
.method public getNumberOfDigits()[Ljava/lang/Integer;
    .locals 4

    .line 63
    iget-object v0, p0, Lcom/adyen/utils/CardType;->numberOfDigits:[Ljava/lang/Integer;

    array-length v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 64
    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v1
.end method
