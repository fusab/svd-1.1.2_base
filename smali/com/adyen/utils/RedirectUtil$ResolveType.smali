.class public final enum Lcom/adyen/utils/RedirectUtil$ResolveType;
.super Ljava/lang/Enum;
.source "RedirectUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/utils/RedirectUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ResolveType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/adyen/utils/RedirectUtil$ResolveType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/adyen/utils/RedirectUtil$ResolveType;

.field public static final enum APPLICATION:Lcom/adyen/utils/RedirectUtil$ResolveType;

.field public static final enum DEFAULT_BROWSER:Lcom/adyen/utils/RedirectUtil$ResolveType;

.field public static final enum SYSTEM_COMPONENT:Lcom/adyen/utils/RedirectUtil$ResolveType;

.field public static final enum UNKNOWN:Lcom/adyen/utils/RedirectUtil$ResolveType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 59
    new-instance v0, Lcom/adyen/utils/RedirectUtil$ResolveType;

    const/4 v1, 0x0

    const-string v2, "SYSTEM_COMPONENT"

    invoke-direct {v0, v2, v1}, Lcom/adyen/utils/RedirectUtil$ResolveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/utils/RedirectUtil$ResolveType;->SYSTEM_COMPONENT:Lcom/adyen/utils/RedirectUtil$ResolveType;

    .line 63
    new-instance v0, Lcom/adyen/utils/RedirectUtil$ResolveType;

    const/4 v2, 0x1

    const-string v3, "DEFAULT_BROWSER"

    invoke-direct {v0, v3, v2}, Lcom/adyen/utils/RedirectUtil$ResolveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/utils/RedirectUtil$ResolveType;->DEFAULT_BROWSER:Lcom/adyen/utils/RedirectUtil$ResolveType;

    .line 67
    new-instance v0, Lcom/adyen/utils/RedirectUtil$ResolveType;

    const/4 v3, 0x2

    const-string v4, "APPLICATION"

    invoke-direct {v0, v4, v3}, Lcom/adyen/utils/RedirectUtil$ResolveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/utils/RedirectUtil$ResolveType;->APPLICATION:Lcom/adyen/utils/RedirectUtil$ResolveType;

    .line 71
    new-instance v0, Lcom/adyen/utils/RedirectUtil$ResolveType;

    const/4 v4, 0x3

    const-string v5, "UNKNOWN"

    invoke-direct {v0, v5, v4}, Lcom/adyen/utils/RedirectUtil$ResolveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/adyen/utils/RedirectUtil$ResolveType;->UNKNOWN:Lcom/adyen/utils/RedirectUtil$ResolveType;

    const/4 v0, 0x4

    .line 55
    new-array v0, v0, [Lcom/adyen/utils/RedirectUtil$ResolveType;

    sget-object v5, Lcom/adyen/utils/RedirectUtil$ResolveType;->SYSTEM_COMPONENT:Lcom/adyen/utils/RedirectUtil$ResolveType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/adyen/utils/RedirectUtil$ResolveType;->DEFAULT_BROWSER:Lcom/adyen/utils/RedirectUtil$ResolveType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/adyen/utils/RedirectUtil$ResolveType;->APPLICATION:Lcom/adyen/utils/RedirectUtil$ResolveType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/adyen/utils/RedirectUtil$ResolveType;->UNKNOWN:Lcom/adyen/utils/RedirectUtil$ResolveType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/adyen/utils/RedirectUtil$ResolveType;->$VALUES:[Lcom/adyen/utils/RedirectUtil$ResolveType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/adyen/utils/RedirectUtil$ResolveType;
    .locals 1

    .line 55
    const-class v0, Lcom/adyen/utils/RedirectUtil$ResolveType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/adyen/utils/RedirectUtil$ResolveType;

    return-object p0
.end method

.method public static values()[Lcom/adyen/utils/RedirectUtil$ResolveType;
    .locals 1

    .line 55
    sget-object v0, Lcom/adyen/utils/RedirectUtil$ResolveType;->$VALUES:[Lcom/adyen/utils/RedirectUtil$ResolveType;

    invoke-virtual {v0}, [Lcom/adyen/utils/RedirectUtil$ResolveType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/adyen/utils/RedirectUtil$ResolveType;

    return-object v0
.end method
