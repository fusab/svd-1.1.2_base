.class public final Lcom/adyen/utils/RedirectUtil;
.super Ljava/lang/Object;
.source "RedirectUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adyen/utils/RedirectUtil$ResolveResult;,
        Lcom/adyen/utils/RedirectUtil$ResolveType;
    }
.end annotation


# static fields
.field private static final SYSTEM_PACKAGE_NAME:Ljava/lang/String; = "android"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static determineResolveResult(Landroid/content/Context;Landroid/net/Uri;)Lcom/adyen/utils/RedirectUtil$ResolveResult;
    .locals 3
    .param p0    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Landroid/net/Uri;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 26
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 27
    new-instance p1, Landroid/content/Intent;

    const-string v2, "http://"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {p1, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 29
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    const/4 v1, 0x0

    .line 30
    invoke-virtual {p0, v0, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    const/high16 v1, 0x10000

    .line 31
    invoke-virtual {p0, p1, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object p0

    const/4 p1, 0x0

    if-eqz v0, :cond_0

    .line 32
    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v1, :cond_0

    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v1, p1

    :goto_0
    if-eqz p0, :cond_1

    .line 33
    iget-object v2, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v2, :cond_1

    iget-object p0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object p0, p0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object p0, p1

    :goto_1
    if-eqz v1, :cond_4

    const-string v2, "android"

    .line 36
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 37
    new-instance p0, Lcom/adyen/utils/RedirectUtil$ResolveResult;

    sget-object v1, Lcom/adyen/utils/RedirectUtil$ResolveType;->SYSTEM_COMPONENT:Lcom/adyen/utils/RedirectUtil$ResolveType;

    invoke-direct {p0, v1, v0, p1}, Lcom/adyen/utils/RedirectUtil$ResolveResult;-><init>(Lcom/adyen/utils/RedirectUtil$ResolveType;Landroid/content/pm/ResolveInfo;Lcom/adyen/utils/RedirectUtil$1;)V

    return-object p0

    .line 38
    :cond_2
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 39
    new-instance p0, Lcom/adyen/utils/RedirectUtil$ResolveResult;

    sget-object v1, Lcom/adyen/utils/RedirectUtil$ResolveType;->DEFAULT_BROWSER:Lcom/adyen/utils/RedirectUtil$ResolveType;

    invoke-direct {p0, v1, v0, p1}, Lcom/adyen/utils/RedirectUtil$ResolveResult;-><init>(Lcom/adyen/utils/RedirectUtil$ResolveType;Landroid/content/pm/ResolveInfo;Lcom/adyen/utils/RedirectUtil$1;)V

    return-object p0

    .line 41
    :cond_3
    new-instance p0, Lcom/adyen/utils/RedirectUtil$ResolveResult;

    sget-object v1, Lcom/adyen/utils/RedirectUtil$ResolveType;->APPLICATION:Lcom/adyen/utils/RedirectUtil$ResolveType;

    invoke-direct {p0, v1, v0, p1}, Lcom/adyen/utils/RedirectUtil$ResolveResult;-><init>(Lcom/adyen/utils/RedirectUtil$ResolveType;Landroid/content/pm/ResolveInfo;Lcom/adyen/utils/RedirectUtil$1;)V

    return-object p0

    .line 45
    :cond_4
    new-instance p0, Lcom/adyen/utils/RedirectUtil$ResolveResult;

    sget-object v0, Lcom/adyen/utils/RedirectUtil$ResolveType;->UNKNOWN:Lcom/adyen/utils/RedirectUtil$ResolveType;

    invoke-direct {p0, v0, p1, p1}, Lcom/adyen/utils/RedirectUtil$ResolveResult;-><init>(Lcom/adyen/utils/RedirectUtil$ResolveType;Landroid/content/pm/ResolveInfo;Lcom/adyen/utils/RedirectUtil$1;)V

    return-object p0
.end method
