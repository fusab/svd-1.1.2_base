.class public final Lcom/adyen/utils/RedirectUtil$ResolveResult;
.super Ljava/lang/Object;
.source "RedirectUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adyen/utils/RedirectUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ResolveResult"
.end annotation


# instance fields
.field private mResolveInfo:Landroid/content/pm/ResolveInfo;

.field private mResolveType:Lcom/adyen/utils/RedirectUtil$ResolveType;


# direct methods
.method private constructor <init>(Lcom/adyen/utils/RedirectUtil$ResolveType;Landroid/content/pm/ResolveInfo;)V
    .locals 0
    .param p1    # Lcom/adyen/utils/RedirectUtil$ResolveType;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/content/pm/ResolveInfo;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object p1, p0, Lcom/adyen/utils/RedirectUtil$ResolveResult;->mResolveType:Lcom/adyen/utils/RedirectUtil$ResolveType;

    .line 84
    iput-object p2, p0, Lcom/adyen/utils/RedirectUtil$ResolveResult;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    return-void
.end method

.method synthetic constructor <init>(Lcom/adyen/utils/RedirectUtil$ResolveType;Landroid/content/pm/ResolveInfo;Lcom/adyen/utils/RedirectUtil$1;)V
    .locals 0

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/adyen/utils/RedirectUtil$ResolveResult;-><init>(Lcom/adyen/utils/RedirectUtil$ResolveType;Landroid/content/pm/ResolveInfo;)V

    return-void
.end method


# virtual methods
.method public getResolveInfo()Landroid/content/pm/ResolveInfo;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 100
    iget-object v0, p0, Lcom/adyen/utils/RedirectUtil$ResolveResult;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    return-object v0
.end method

.method public getResolveType()Lcom/adyen/utils/RedirectUtil$ResolveType;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/adyen/utils/RedirectUtil$ResolveResult;->mResolveType:Lcom/adyen/utils/RedirectUtil$ResolveType;

    return-object v0
.end method
