.class public final Lcom/google/android/exoplayer2/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final ad_marker_color:I = 0x7f040025

.field public static final ad_marker_width:I = 0x7f040026

.field public static final auto_show:I = 0x7f040038

.field public static final bar_height:I = 0x7f040040

.field public static final buffered_color:I = 0x7f040056

.field public static final controller_layout_id:I = 0x7f0400c3

.field public static final default_artwork:I = 0x7f0400ed

.field public static final fastforward_increment:I = 0x7f04011a

.field public static final font:I = 0x7f04011d

.field public static final fontProviderAuthority:I = 0x7f04011f

.field public static final fontProviderCerts:I = 0x7f040120

.field public static final fontProviderFetchStrategy:I = 0x7f040121

.field public static final fontProviderFetchTimeout:I = 0x7f040122

.field public static final fontProviderPackage:I = 0x7f040123

.field public static final fontProviderQuery:I = 0x7f040124

.field public static final fontStyle:I = 0x7f040125

.field public static final fontWeight:I = 0x7f040127

.field public static final hide_during_ads:I = 0x7f040135

.field public static final hide_on_touch:I = 0x7f040136

.field public static final keep_content_on_player_reset:I = 0x7f04015a

.field public static final played_ad_marker_color:I = 0x7f0401b4

.field public static final played_color:I = 0x7f0401b5

.field public static final player_layout_id:I = 0x7f0401b6

.field public static final repeat_toggle_modes:I = 0x7f0401c8

.field public static final resize_mode:I = 0x7f0401c9

.field public static final rewind_increment:I = 0x7f0401cd

.field public static final scrubber_color:I = 0x7f0401e2

.field public static final scrubber_disabled_size:I = 0x7f0401e3

.field public static final scrubber_dragged_size:I = 0x7f0401e4

.field public static final scrubber_drawable:I = 0x7f0401e5

.field public static final scrubber_enabled_size:I = 0x7f0401e6

.field public static final show_buffering:I = 0x7f0401f2

.field public static final show_shuffle_button:I = 0x7f0401f3

.field public static final show_timeout:I = 0x7f0401f4

.field public static final shutter_background_color:I = 0x7f0401f5

.field public static final surface_type:I = 0x7f040212

.field public static final touch_target_height:I = 0x7f040268

.field public static final unplayed_color:I = 0x7f040275

.field public static final use_artwork:I = 0x7f040278

.field public static final use_controller:I = 0x7f040279


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
