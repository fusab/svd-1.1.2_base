.class Lcom/airbnb/lottie/parser/GradientStrokeParser;
.super Ljava/lang/Object;
.source "GradientStrokeParser.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static parse(Landroid/util/JsonReader;Lcom/airbnb/lottie/LottieComposition;)Lcom/airbnb/lottie/model/content/GradientStroke;
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 36
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    .line 38
    :goto_0
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1d

    .line 39
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->hashCode()I

    move-result v13

    const-string v15, "o"

    const-string v0, "g"

    const-string v14, "d"

    move-object/from16 v17, v11

    const/16 v18, -0x1

    const/16 v11, 0x64

    if-eq v13, v11, :cond_9

    const/16 v11, 0x65

    if-eq v13, v11, :cond_8

    const/16 v11, 0x67

    if-eq v13, v11, :cond_7

    const/16 v11, 0x6f

    if-eq v13, v11, :cond_6

    const/16 v11, 0x77

    if-eq v13, v11, :cond_5

    const/16 v11, 0xd77

    if-eq v13, v11, :cond_4

    const/16 v11, 0xd7e

    if-eq v13, v11, :cond_3

    const/16 v11, 0xdbf

    if-eq v13, v11, :cond_2

    const/16 v11, 0x73

    if-eq v13, v11, :cond_1

    const/16 v11, 0x74

    if-eq v13, v11, :cond_0

    goto/16 :goto_1

    :cond_0
    const-string v11, "t"

    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    const/4 v11, 0x3

    goto :goto_2

    :cond_1
    const-string v11, "s"

    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    const/4 v11, 0x4

    goto :goto_2

    :cond_2
    const-string v11, "nm"

    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    const/4 v11, 0x0

    goto :goto_2

    :cond_3
    const-string v11, "lj"

    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    const/16 v11, 0x8

    goto :goto_2

    :cond_4
    const-string v11, "lc"

    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    const/4 v11, 0x7

    goto :goto_2

    :cond_5
    const-string/jumbo v11, "w"

    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    const/4 v11, 0x6

    goto :goto_2

    :cond_6
    invoke-virtual {v12, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    const/4 v11, 0x2

    goto :goto_2

    :cond_7
    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    const/4 v11, 0x1

    goto :goto_2

    :cond_8
    const-string v11, "e"

    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    const/4 v11, 0x5

    goto :goto_2

    :cond_9
    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    const/16 v11, 0x9

    goto :goto_2

    :cond_a
    :goto_1
    const/4 v11, -0x1

    :goto_2
    packed-switch v11, :pswitch_data_0

    move-object/from16 v12, p1

    move-object/from16 v20, v8

    move-object/from16 v19, v9

    move-object/from16 v8, p0

    .line 114
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->skipValue()V

    :goto_3
    move-object/from16 v11, v17

    goto/16 :goto_11

    .line 82
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->beginArray()V

    move-object/from16 v11, v17

    .line 83
    :goto_4
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_14

    .line 86
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->beginObject()V

    const/4 v12, 0x0

    const/4 v13, 0x0

    .line 87
    :goto_5
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_10

    move-object/from16 v16, v11

    .line 88
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v19, v9

    invoke-virtual {v11}, Ljava/lang/String;->hashCode()I

    move-result v9

    move-object/from16 v20, v8

    const/16 v8, 0x6e

    if-eq v9, v8, :cond_c

    const/16 v8, 0x76

    if-eq v9, v8, :cond_b

    goto :goto_6

    :cond_b
    const-string/jumbo v8, "v"

    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    const/4 v8, 0x1

    goto :goto_7

    :cond_c
    const-string v8, "n"

    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    const/4 v8, 0x0

    goto :goto_7

    :cond_d
    :goto_6
    const/4 v8, -0x1

    :goto_7
    if-eqz v8, :cond_f

    const/4 v9, 0x1

    if-eq v8, v9, :cond_e

    .line 96
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_8

    .line 93
    :cond_e
    invoke-static/range {p0 .. p1}, Lcom/airbnb/lottie/parser/AnimatableValueParser;->parseFloat(Landroid/util/JsonReader;Lcom/airbnb/lottie/LottieComposition;)Lcom/airbnb/lottie/model/animatable/AnimatableFloatValue;

    move-result-object v8

    move-object v13, v8

    goto :goto_8

    .line 90
    :cond_f
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v8

    move-object v12, v8

    :goto_8
    move-object/from16 v11, v16

    move-object/from16 v9, v19

    move-object/from16 v8, v20

    goto :goto_5

    :cond_10
    move-object/from16 v20, v8

    move-object/from16 v19, v9

    move-object/from16 v16, v11

    .line 99
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->endObject()V

    .line 101
    invoke-virtual {v12, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_11

    move-object v11, v13

    goto :goto_9

    .line 103
    :cond_11
    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_12

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_13

    .line 104
    :cond_12
    invoke-interface {v10, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_13
    move-object/from16 v11, v16

    :goto_9
    move-object/from16 v9, v19

    move-object/from16 v8, v20

    goto :goto_4

    :cond_14
    move-object/from16 v20, v8

    move-object/from16 v19, v9

    move-object/from16 v16, v11

    .line 107
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->endArray()V

    .line 108
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    const/4 v8, 0x1

    if-ne v0, v8, :cond_15

    const/4 v11, 0x0

    .line 110
    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_15
    move-object/from16 v8, p0

    move-object/from16 v12, p1

    move-object/from16 v11, v16

    goto/16 :goto_10

    :pswitch_1
    move-object/from16 v20, v8

    const/4 v8, 0x1

    .line 79
    invoke-static {}, Lcom/airbnb/lottie/model/content/ShapeStroke$LineJoinType;->values()[Lcom/airbnb/lottie/model/content/ShapeStroke$LineJoinType;

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->nextInt()I

    move-result v9

    sub-int/2addr v9, v8

    aget-object v9, v0, v9

    goto :goto_b

    :pswitch_2
    move-object/from16 v19, v9

    const/4 v8, 0x1

    .line 76
    invoke-static {}, Lcom/airbnb/lottie/model/content/ShapeStroke$LineCapType;->values()[Lcom/airbnb/lottie/model/content/ShapeStroke$LineCapType;

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->nextInt()I

    move-result v9

    sub-int/2addr v9, v8

    aget-object v8, v0, v9

    move-object/from16 v12, p1

    move-object/from16 v20, v8

    move-object/from16 v11, v17

    move-object/from16 v9, v19

    move-object/from16 v8, p0

    goto/16 :goto_11

    :pswitch_3
    move-object/from16 v20, v8

    move-object/from16 v19, v9

    .line 73
    invoke-static/range {p0 .. p1}, Lcom/airbnb/lottie/parser/AnimatableValueParser;->parseFloat(Landroid/util/JsonReader;Lcom/airbnb/lottie/LottieComposition;)Lcom/airbnb/lottie/model/animatable/AnimatableFloatValue;

    move-result-object v7

    goto :goto_b

    :pswitch_4
    move-object/from16 v20, v8

    move-object/from16 v19, v9

    .line 70
    invoke-static/range {p0 .. p1}, Lcom/airbnb/lottie/parser/AnimatableValueParser;->parsePoint(Landroid/util/JsonReader;Lcom/airbnb/lottie/LottieComposition;)Lcom/airbnb/lottie/model/animatable/AnimatablePointValue;

    move-result-object v6

    goto :goto_b

    :pswitch_5
    move-object/from16 v20, v8

    move-object/from16 v19, v9

    .line 67
    invoke-static/range {p0 .. p1}, Lcom/airbnb/lottie/parser/AnimatableValueParser;->parsePoint(Landroid/util/JsonReader;Lcom/airbnb/lottie/LottieComposition;)Lcom/airbnb/lottie/model/animatable/AnimatablePointValue;

    move-result-object v5

    goto :goto_b

    :pswitch_6
    move-object/from16 v20, v8

    move-object/from16 v19, v9

    const/4 v8, 0x1

    .line 64
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->nextInt()I

    move-result v0

    if-ne v0, v8, :cond_16

    sget-object v0, Lcom/airbnb/lottie/model/content/GradientType;->Linear:Lcom/airbnb/lottie/model/content/GradientType;

    goto :goto_a

    :cond_16
    sget-object v0, Lcom/airbnb/lottie/model/content/GradientType;->Radial:Lcom/airbnb/lottie/model/content/GradientType;

    :goto_a
    move-object v2, v0

    move-object/from16 v8, p0

    move-object/from16 v12, p1

    goto/16 :goto_f

    :pswitch_7
    move-object/from16 v20, v8

    move-object/from16 v19, v9

    .line 61
    invoke-static/range {p0 .. p1}, Lcom/airbnb/lottie/parser/AnimatableValueParser;->parseInteger(Landroid/util/JsonReader;Lcom/airbnb/lottie/LottieComposition;)Lcom/airbnb/lottie/model/animatable/AnimatableIntegerValue;

    move-result-object v4

    :goto_b
    move-object/from16 v8, p0

    move-object/from16 v12, p1

    goto/16 :goto_3

    :pswitch_8
    move-object/from16 v20, v8

    move-object/from16 v19, v9

    const/4 v11, 0x0

    .line 45
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->beginObject()V

    const/4 v0, -0x1

    .line 46
    :goto_c
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1c

    .line 47
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    const/16 v12, 0x6b

    if-eq v9, v12, :cond_18

    const/16 v12, 0x70

    if-eq v9, v12, :cond_17

    goto :goto_d

    :cond_17
    const-string v9, "p"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_19

    const/4 v8, 0x0

    goto :goto_e

    :cond_18
    const-string v9, "k"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_19

    const/4 v8, 0x1

    goto :goto_e

    :cond_19
    :goto_d
    const/4 v8, -0x1

    :goto_e
    if-eqz v8, :cond_1b

    const/4 v9, 0x1

    if-eq v8, v9, :cond_1a

    .line 55
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->skipValue()V

    move-object/from16 v8, p0

    move-object/from16 v12, p1

    goto :goto_c

    :cond_1a
    move-object/from16 v8, p0

    move-object/from16 v12, p1

    .line 52
    invoke-static {v8, v12, v0}, Lcom/airbnb/lottie/parser/AnimatableValueParser;->parseGradientColor(Landroid/util/JsonReader;Lcom/airbnb/lottie/LottieComposition;I)Lcom/airbnb/lottie/model/animatable/AnimatableGradientColorValue;

    move-result-object v3

    goto :goto_c

    :cond_1b
    const/4 v9, 0x1

    move-object/from16 v8, p0

    move-object/from16 v12, p1

    .line 49
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->nextInt()I

    move-result v0

    goto :goto_c

    :cond_1c
    move-object/from16 v8, p0

    move-object/from16 v12, p1

    .line 58
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->endObject()V

    :goto_f
    move-object/from16 v11, v17

    :goto_10
    move-object/from16 v9, v19

    goto :goto_11

    :pswitch_9
    move-object/from16 v12, p1

    move-object/from16 v20, v8

    move-object/from16 v19, v9

    move-object/from16 v8, p0

    .line 41
    invoke-virtual/range {p0 .. p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3

    :goto_11
    move-object/from16 v8, v20

    goto/16 :goto_0

    :cond_1d
    move-object/from16 v20, v8

    move-object/from16 v19, v9

    move-object/from16 v17, v11

    .line 118
    new-instance v12, Lcom/airbnb/lottie/model/content/GradientStroke;

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/airbnb/lottie/model/content/GradientStroke;-><init>(Ljava/lang/String;Lcom/airbnb/lottie/model/content/GradientType;Lcom/airbnb/lottie/model/animatable/AnimatableGradientColorValue;Lcom/airbnb/lottie/model/animatable/AnimatableIntegerValue;Lcom/airbnb/lottie/model/animatable/AnimatablePointValue;Lcom/airbnb/lottie/model/animatable/AnimatablePointValue;Lcom/airbnb/lottie/model/animatable/AnimatableFloatValue;Lcom/airbnb/lottie/model/content/ShapeStroke$LineCapType;Lcom/airbnb/lottie/model/content/ShapeStroke$LineJoinType;Ljava/util/List;Lcom/airbnb/lottie/model/animatable/AnimatableFloatValue;)V

    return-object v12

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
