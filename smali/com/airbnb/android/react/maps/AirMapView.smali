.class public Lcom/airbnb/android/react/maps/AirMapView;
.super Lcom/google/android/gms/maps/MapView;
.source "AirMapView.java"

# interfaces
.implements Lcom/google/android/gms/maps/GoogleMap$InfoWindowAdapter;
.implements Lcom/google/android/gms/maps/GoogleMap$OnMarkerDragListener;
.implements Lcom/google/android/gms/maps/OnMapReadyCallback;
.implements Lcom/google/android/gms/maps/GoogleMap$OnPoiClickListener;
.implements Lcom/google/android/gms/maps/GoogleMap$OnIndoorStateChangeListener;


# static fields
.field private static final PERMISSIONS:[Ljava/lang/String;


# instance fields
.field private attacherGroup:Lcom/airbnb/android/react/maps/ViewAttacherGroup;

.field private final baseMapPadding:I

.field private boundsToMove:Lcom/google/android/gms/maps/model/LatLngBounds;

.field private cacheEnabled:Z

.field private cacheImageView:Landroid/widget/ImageView;

.field private cameraLastIdleBounds:Lcom/google/android/gms/maps/model/LatLngBounds;

.field private cameraMoveReason:I

.field private cameraToSet:Lcom/google/android/gms/maps/CameraUpdate;

.field private final context:Lcom/facebook/react/uimanager/ThemedReactContext;

.field private destroyed:Z

.field private final eventDispatcher:Lcom/facebook/react/uimanager/events/EventDispatcher;

.field private final features:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/airbnb/android/react/maps/AirMapFeature;",
            ">;"
        }
    .end annotation
.end field

.field private final gestureDetector:Landroidx/core/view/GestureDetectorCompat;

.field private handlePanDrag:Z

.field private initialCameraSet:Z

.field private initialRegionSet:Z

.field private isMapLoaded:Ljava/lang/Boolean;

.field private kmlLayer:Lcom/google/maps/android/data/kml/KmlLayer;

.field private lifecycleListener:Lcom/facebook/react/bridge/LifecycleEventListener;

.field private loadingBackgroundColor:Ljava/lang/Integer;

.field private loadingIndicatorColor:Ljava/lang/Integer;

.field private final manager:Lcom/airbnb/android/react/maps/AirMapManager;

.field public map:Lcom/google/android/gms/maps/GoogleMap;

.field private mapLoadingLayout:Landroid/widget/RelativeLayout;

.field private mapLoadingProgressBar:Landroid/widget/ProgressBar;

.field private final markerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/google/android/gms/maps/model/Marker;",
            "Lcom/airbnb/android/react/maps/AirMapMarker;",
            ">;"
        }
    .end annotation
.end field

.field private moveOnMarkerPress:Z

.field private paused:Z

.field private final polygonMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/google/android/gms/maps/model/Polygon;",
            "Lcom/airbnb/android/react/maps/AirMapPolygon;",
            ">;"
        }
    .end annotation
.end field

.field private final polylineMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/google/android/gms/maps/model/Polyline;",
            "Lcom/airbnb/android/react/maps/AirMapPolyline;",
            ">;"
        }
    .end annotation
.end field

.field private showUserLocation:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "android.permission.ACCESS_FINE_LOCATION"

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    .line 96
    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/airbnb/android/react/maps/AirMapView;->PERMISSIONS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/react/uimanager/ThemedReactContext;Lcom/facebook/react/bridge/ReactApplicationContext;Lcom/airbnb/android/react/maps/AirMapManager;Lcom/google/android/gms/maps/GoogleMapOptions;)V
    .locals 1

    .line 147
    invoke-static {p1, p2}, Lcom/airbnb/android/react/maps/AirMapView;->getNonBuggyContext(Lcom/facebook/react/uimanager/ThemedReactContext;Lcom/facebook/react/bridge/ReactApplicationContext;)Landroid/content/Context;

    move-result-object p2

    invoke-direct {p0, p2, p4}, Lcom/google/android/gms/maps/MapView;-><init>(Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V

    const/4 p2, 0x0

    .line 80
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p4

    iput-object p4, p0, Lcom/airbnb/android/react/maps/AirMapView;->isMapLoaded:Ljava/lang/Boolean;

    const/4 p4, 0x0

    .line 81
    iput-object p4, p0, Lcom/airbnb/android/react/maps/AirMapView;->loadingBackgroundColor:Ljava/lang/Integer;

    .line 82
    iput-object p4, p0, Lcom/airbnb/android/react/maps/AirMapView;->loadingIndicatorColor:Ljava/lang/Integer;

    const/16 v0, 0x32

    .line 83
    iput v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->baseMapPadding:I

    .line 87
    iput-boolean p2, p0, Lcom/airbnb/android/react/maps/AirMapView;->showUserLocation:Z

    .line 88
    iput-boolean p2, p0, Lcom/airbnb/android/react/maps/AirMapView;->handlePanDrag:Z

    const/4 v0, 0x1

    .line 89
    iput-boolean v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->moveOnMarkerPress:Z

    .line 90
    iput-boolean p2, p0, Lcom/airbnb/android/react/maps/AirMapView;->cacheEnabled:Z

    .line 91
    iput-boolean p2, p0, Lcom/airbnb/android/react/maps/AirMapView;->initialRegionSet:Z

    .line 92
    iput-boolean p2, p0, Lcom/airbnb/android/react/maps/AirMapView;->initialCameraSet:Z

    .line 94
    iput p2, p0, Lcom/airbnb/android/react/maps/AirMapView;->cameraMoveReason:I

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->features:Ljava/util/List;

    .line 100
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->markerMap:Ljava/util/Map;

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->polylineMap:Ljava/util/Map;

    .line 102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->polygonMap:Ljava/util/Map;

    .line 106
    iput-boolean p2, p0, Lcom/airbnb/android/react/maps/AirMapView;->paused:Z

    .line 107
    iput-boolean p2, p0, Lcom/airbnb/android/react/maps/AirMapView;->destroyed:Z

    .line 149
    iput-object p3, p0, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    .line 150
    iput-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    .line 152
    invoke-super {p0, p4}, Lcom/google/android/gms/maps/MapView;->onCreate(Landroid/os/Bundle;)V

    .line 154
    invoke-super {p0}, Lcom/google/android/gms/maps/MapView;->onResume()V

    .line 155
    invoke-super {p0, p0}, Lcom/google/android/gms/maps/MapView;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V

    .line 159
    new-instance p3, Landroidx/core/view/GestureDetectorCompat;

    new-instance p4, Lcom/airbnb/android/react/maps/AirMapView$1;

    invoke-direct {p4, p0}, Lcom/airbnb/android/react/maps/AirMapView$1;-><init>(Lcom/airbnb/android/react/maps/AirMapView;)V

    invoke-direct {p3, p1, p4}, Landroidx/core/view/GestureDetectorCompat;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object p3, p0, Lcom/airbnb/android/react/maps/AirMapView;->gestureDetector:Landroidx/core/view/GestureDetectorCompat;

    .line 172
    new-instance p3, Lcom/airbnb/android/react/maps/AirMapView$2;

    invoke-direct {p3, p0}, Lcom/airbnb/android/react/maps/AirMapView$2;-><init>(Lcom/airbnb/android/react/maps/AirMapView;)V

    invoke-virtual {p0, p3}, Lcom/airbnb/android/react/maps/AirMapView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 181
    const-class p3, Lcom/facebook/react/uimanager/UIManagerModule;

    invoke-virtual {p1, p3}, Lcom/facebook/react/uimanager/ThemedReactContext;->getNativeModule(Ljava/lang/Class;)Lcom/facebook/react/bridge/NativeModule;

    move-result-object p1

    check-cast p1, Lcom/facebook/react/uimanager/UIManagerModule;

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/UIManagerModule;->getEventDispatcher()Lcom/facebook/react/uimanager/events/EventDispatcher;

    move-result-object p1

    iput-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->eventDispatcher:Lcom/facebook/react/uimanager/events/EventDispatcher;

    .line 185
    new-instance p1, Lcom/airbnb/android/react/maps/ViewAttacherGroup;

    iget-object p3, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    invoke-direct {p1, p3}, Lcom/airbnb/android/react/maps/ViewAttacherGroup;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->attacherGroup:Lcom/airbnb/android/react/maps/ViewAttacherGroup;

    .line 186
    new-instance p1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {p1, p2, p2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 187
    iput p2, p1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 188
    iput p2, p1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    const p2, 0x5f5e0ff

    .line 189
    iput p2, p1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 190
    iput p2, p1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 191
    iget-object p2, p0, Lcom/airbnb/android/react/maps/AirMapView;->attacherGroup:Lcom/airbnb/android/react/maps/ViewAttacherGroup;

    invoke-virtual {p2, p1}, Lcom/airbnb/android/react/maps/ViewAttacherGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 192
    iget-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->attacherGroup:Lcom/airbnb/android/react/maps/ViewAttacherGroup;

    invoke-virtual {p0, p1}, Lcom/airbnb/android/react/maps/AirMapView;->addView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$000(Lcom/airbnb/android/react/maps/AirMapView;)Z
    .locals 0

    .line 73
    iget-boolean p0, p0, Lcom/airbnb/android/react/maps/AirMapView;->handlePanDrag:Z

    return p0
.end method

.method static synthetic access$100(Lcom/airbnb/android/react/maps/AirMapView;)Z
    .locals 0

    .line 73
    iget-boolean p0, p0, Lcom/airbnb/android/react/maps/AirMapView;->paused:Z

    return p0
.end method

.method static synthetic access$1000(Lcom/airbnb/android/react/maps/AirMapView;)Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/airbnb/android/react/maps/AirMapView;->cameraLastIdleBounds:Lcom/google/android/gms/maps/model/LatLngBounds;

    return-object p0
.end method

.method static synthetic access$1002(Lcom/airbnb/android/react/maps/AirMapView;Lcom/google/android/gms/maps/model/LatLngBounds;)Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->cameraLastIdleBounds:Lcom/google/android/gms/maps/model/LatLngBounds;

    return-object p1
.end method

.method static synthetic access$102(Lcom/airbnb/android/react/maps/AirMapView;Z)Z
    .locals 0

    .line 73
    iput-boolean p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->paused:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/airbnb/android/react/maps/AirMapView;)Lcom/facebook/react/uimanager/events/EventDispatcher;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/airbnb/android/react/maps/AirMapView;->eventDispatcher:Lcom/facebook/react/uimanager/events/EventDispatcher;

    return-object p0
.end method

.method static synthetic access$1202(Lcom/airbnb/android/react/maps/AirMapView;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->isMapLoaded:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/airbnb/android/react/maps/AirMapView;)Z
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/airbnb/android/react/maps/AirMapView;->hasPermissions()Z

    move-result p0

    return p0
.end method

.method static synthetic access$1400(Lcom/airbnb/android/react/maps/AirMapView;)Z
    .locals 0

    .line 73
    iget-boolean p0, p0, Lcom/airbnb/android/react/maps/AirMapView;->showUserLocation:Z

    return p0
.end method

.method static synthetic access$1500(Lcom/airbnb/android/react/maps/AirMapView;)Z
    .locals 0

    .line 73
    iget-boolean p0, p0, Lcom/airbnb/android/react/maps/AirMapView;->destroyed:Z

    return p0
.end method

.method static synthetic access$200(Lcom/airbnb/android/react/maps/AirMapView;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/airbnb/android/react/maps/AirMapView;->cacheView()V

    return-void
.end method

.method static synthetic access$300(Lcom/airbnb/android/react/maps/AirMapView;)Lcom/facebook/react/uimanager/ThemedReactContext;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    return-object p0
.end method

.method static synthetic access$400(Lcom/airbnb/android/react/maps/AirMapView;)Lcom/airbnb/android/react/maps/AirMapManager;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    return-object p0
.end method

.method static synthetic access$500(Lcom/airbnb/android/react/maps/AirMapView;Lcom/google/android/gms/maps/model/Marker;)Lcom/airbnb/android/react/maps/AirMapMarker;
    .locals 0

    .line 73
    invoke-direct {p0, p1}, Lcom/airbnb/android/react/maps/AirMapView;->getMarkerMap(Lcom/google/android/gms/maps/model/Marker;)Lcom/airbnb/android/react/maps/AirMapMarker;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$600(Lcom/airbnb/android/react/maps/AirMapView;)Z
    .locals 0

    .line 73
    iget-boolean p0, p0, Lcom/airbnb/android/react/maps/AirMapView;->moveOnMarkerPress:Z

    return p0
.end method

.method static synthetic access$700(Lcom/airbnb/android/react/maps/AirMapView;)Ljava/util/Map;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/airbnb/android/react/maps/AirMapView;->polygonMap:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$800(Lcom/airbnb/android/react/maps/AirMapView;)Ljava/util/Map;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/airbnb/android/react/maps/AirMapView;->polylineMap:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$900(Lcom/airbnb/android/react/maps/AirMapView;)I
    .locals 0

    .line 73
    iget p0, p0, Lcom/airbnb/android/react/maps/AirMapView;->cameraMoveReason:I

    return p0
.end method

.method static synthetic access$902(Lcom/airbnb/android/react/maps/AirMapView;I)I
    .locals 0

    .line 73
    iput p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->cameraMoveReason:I

    return p1
.end method

.method private cacheView()V
    .locals 4

    .line 1031
    iget-boolean v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->cacheEnabled:Z

    if-eqz v0, :cond_0

    .line 1032
    invoke-direct {p0}, Lcom/airbnb/android/react/maps/AirMapView;->getCacheImageView()Landroid/widget/ImageView;

    move-result-object v0

    .line 1033
    invoke-direct {p0}, Lcom/airbnb/android/react/maps/AirMapView;->getMapLoadingLayoutView()Landroid/widget/RelativeLayout;

    move-result-object v1

    const/4 v2, 0x4

    .line 1034
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v2, 0x0

    .line 1035
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1036
    iget-object v2, p0, Lcom/airbnb/android/react/maps/AirMapView;->isMapLoaded:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1037
    iget-object v2, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v3, Lcom/airbnb/android/react/maps/AirMapView$15;

    invoke-direct {v3, p0, v0, v1}, Lcom/airbnb/android/react/maps/AirMapView$15;-><init>(Lcom/airbnb/android/react/maps/AirMapView;Landroid/widget/ImageView;Landroid/widget/RelativeLayout;)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/GoogleMap;->snapshot(Lcom/google/android/gms/maps/GoogleMap$SnapshotReadyCallback;)V

    goto :goto_0

    .line 1046
    :cond_0
    invoke-direct {p0}, Lcom/airbnb/android/react/maps/AirMapView;->removeCacheImageView()V

    .line 1047
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->isMapLoaded:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1048
    invoke-direct {p0}, Lcom/airbnb/android/react/maps/AirMapView;->removeMapLoadingLayoutView()V

    :cond_1
    :goto_0
    return-void
.end method

.method private static contextHasBug(Landroid/content/Context;)Z
    .locals 1

    if-eqz p0, :cond_1

    .line 115
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 116
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private getCacheImageView()Landroid/widget/ImageView;
    .locals 3

    .line 998
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->cacheImageView:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 999
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/airbnb/android/react/maps/AirMapView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->cacheImageView:Landroid/widget/ImageView;

    .line 1000
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->cacheImageView:Landroid/widget/ImageView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/airbnb/android/react/maps/AirMapView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1003
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->cacheImageView:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1005
    :cond_0
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->cacheImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method private getMapLoadingLayoutView()Landroid/widget/RelativeLayout;
    .locals 3

    .line 979
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingLayout:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 980
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/airbnb/android/react/maps/AirMapView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingLayout:Landroid/widget/RelativeLayout;

    .line 981
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingLayout:Landroid/widget/RelativeLayout;

    const v1, -0x333334

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 982
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/airbnb/android/react/maps/AirMapView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 986
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xd

    .line 988
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 989
    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingLayout:Landroid/widget/RelativeLayout;

    invoke-direct {p0}, Lcom/airbnb/android/react/maps/AirMapView;->getMapLoadingProgressBar()Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 991
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 993
    :cond_0
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->loadingBackgroundColor:Ljava/lang/Integer;

    invoke-virtual {p0, v0}, Lcom/airbnb/android/react/maps/AirMapView;->setLoadingBackgroundColor(Ljava/lang/Integer;)V

    .line 994
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private getMapLoadingProgressBar()Landroid/widget/ProgressBar;
    .locals 2

    .line 968
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingProgressBar:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    .line 969
    new-instance v0, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/airbnb/android/react/maps/AirMapView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingProgressBar:Landroid/widget/ProgressBar;

    .line 970
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 972
    :cond_0
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->loadingIndicatorColor:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 973
    invoke-virtual {p0, v0}, Lcom/airbnb/android/react/maps/AirMapView;->setLoadingIndicatorColor(Ljava/lang/Integer;)V

    .line 975
    :cond_1
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private getMarkerMap(Lcom/google/android/gms/maps/model/Marker;)Lcom/airbnb/android/react/maps/AirMapMarker;
    .locals 5

    .line 1230
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->markerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/airbnb/android/react/maps/AirMapMarker;

    if-eqz v0, :cond_0

    return-object v0

    .line 1236
    :cond_0
    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->markerMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1237
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/maps/model/Marker;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/model/Marker;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/maps/model/LatLng;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1238
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/maps/model/Marker;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/model/Marker;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1239
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/airbnb/android/react/maps/AirMapMarker;

    :cond_2
    return-object v0
.end method

.method private static getNonBuggyContext(Lcom/facebook/react/uimanager/ThemedReactContext;Lcom/facebook/react/bridge/ReactApplicationContext;)Landroid/content/Context;
    .locals 1

    .line 129
    invoke-virtual {p1}, Lcom/facebook/react/bridge/ReactApplicationContext;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/airbnb/android/react/maps/AirMapView;->contextHasBug(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    invoke-virtual {p1}, Lcom/facebook/react/bridge/ReactApplicationContext;->getCurrentActivity()Landroid/app/Activity;

    move-result-object p0

    goto :goto_0

    .line 131
    :cond_0
    invoke-static {p0}, Lcom/airbnb/android/react/maps/AirMapView;->contextHasBug(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 133
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ThemedReactContext;->getCurrentActivity()Landroid/app/Activity;

    move-result-object p1

    invoke-static {p1}, Lcom/airbnb/android/react/maps/AirMapView;->contextHasBug(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 134
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ThemedReactContext;->getCurrentActivity()Landroid/app/Activity;

    move-result-object p0

    goto :goto_0

    .line 135
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ThemedReactContext;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/airbnb/android/react/maps/AirMapView;->contextHasBug(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 136
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ThemedReactContext;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    :cond_2
    :goto_0
    return-object p0
.end method

.method private hasPermissions()Z
    .locals 4

    .line 399
    invoke-virtual {p0}, Lcom/airbnb/android/react/maps/AirMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/airbnb/android/react/maps/AirMapView;->PERMISSIONS:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Landroidx/core/content/PermissionChecker;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 400
    invoke-virtual {p0}, Lcom/airbnb/android/react/maps/AirMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v3, Lcom/airbnb/android/react/maps/AirMapView;->PERMISSIONS:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-static {v0, v3}, Landroidx/core/content/PermissionChecker;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method private removeCacheImageView()V
    .locals 2

    .line 1009
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->cacheImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1010
    invoke-virtual {v0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->cacheImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    .line 1011
    iput-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->cacheImageView:Landroid/widget/ImageView;

    :cond_0
    return-void
.end method

.method private removeMapLoadingLayoutView()V
    .locals 2

    .line 1023
    invoke-direct {p0}, Lcom/airbnb/android/react/maps/AirMapView;->removeMapLoadingProgressBar()V

    .line 1024
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1025
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    .line 1026
    iput-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingLayout:Landroid/widget/RelativeLayout;

    :cond_0
    return-void
.end method

.method private removeMapLoadingProgressBar()V
    .locals 2

    .line 1016
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 1017
    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    .line 1018
    iput-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingProgressBar:Landroid/widget/ProgressBar;

    :cond_0
    return-void
.end method


# virtual methods
.method public addFeature(Landroid/view/View;I)V
    .locals 2

    .line 574
    instance-of v0, p1, Lcom/airbnb/android/react/maps/AirMapMarker;

    if-eqz v0, :cond_1

    .line 575
    check-cast p1, Lcom/airbnb/android/react/maps/AirMapMarker;

    .line 576
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p1, v0}, Lcom/airbnb/android/react/maps/AirMapMarker;->addToMap(Lcom/google/android/gms/maps/GoogleMap;)V

    .line 577
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->features:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 580
    invoke-virtual {p1}, Lcom/airbnb/android/react/maps/AirMapMarker;->getVisibility()I

    move-result p2

    const/4 v0, 0x4

    .line 581
    invoke-virtual {p1, v0}, Lcom/airbnb/android/react/maps/AirMapMarker;->setVisibility(I)V

    .line 585
    invoke-virtual {p1}, Lcom/airbnb/android/react/maps/AirMapMarker;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 587
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 591
    :cond_0
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->attacherGroup:Lcom/airbnb/android/react/maps/ViewAttacherGroup;

    invoke-virtual {v0, p1}, Lcom/airbnb/android/react/maps/ViewAttacherGroup;->addView(Landroid/view/View;)V

    .line 596
    invoke-virtual {p1, p2}, Lcom/airbnb/android/react/maps/AirMapMarker;->setVisibility(I)V

    .line 598
    invoke-virtual {p1}, Lcom/airbnb/android/react/maps/AirMapMarker;->getFeature()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/google/android/gms/maps/model/Marker;

    .line 599
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->markerMap:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 600
    :cond_1
    instance-of v0, p1, Lcom/airbnb/android/react/maps/AirMapPolyline;

    if-eqz v0, :cond_2

    .line 601
    check-cast p1, Lcom/airbnb/android/react/maps/AirMapPolyline;

    .line 602
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p1, v0}, Lcom/airbnb/android/react/maps/AirMapPolyline;->addToMap(Lcom/google/android/gms/maps/GoogleMap;)V

    .line 603
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->features:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 604
    invoke-virtual {p1}, Lcom/airbnb/android/react/maps/AirMapPolyline;->getFeature()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/google/android/gms/maps/model/Polyline;

    .line 605
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->polylineMap:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 606
    :cond_2
    instance-of v0, p1, Lcom/airbnb/android/react/maps/AirMapPolygon;

    if-eqz v0, :cond_3

    .line 607
    check-cast p1, Lcom/airbnb/android/react/maps/AirMapPolygon;

    .line 608
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p1, v0}, Lcom/airbnb/android/react/maps/AirMapPolygon;->addToMap(Lcom/google/android/gms/maps/GoogleMap;)V

    .line 609
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->features:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 610
    invoke-virtual {p1}, Lcom/airbnb/android/react/maps/AirMapPolygon;->getFeature()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/google/android/gms/maps/model/Polygon;

    .line 611
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->polygonMap:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 612
    :cond_3
    instance-of v0, p1, Lcom/airbnb/android/react/maps/AirMapCircle;

    if-eqz v0, :cond_4

    .line 613
    check-cast p1, Lcom/airbnb/android/react/maps/AirMapCircle;

    .line 614
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p1, v0}, Lcom/airbnb/android/react/maps/AirMapCircle;->addToMap(Lcom/google/android/gms/maps/GoogleMap;)V

    .line 615
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->features:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 616
    :cond_4
    instance-of v0, p1, Lcom/airbnb/android/react/maps/AirMapUrlTile;

    if-eqz v0, :cond_5

    .line 617
    check-cast p1, Lcom/airbnb/android/react/maps/AirMapUrlTile;

    .line 618
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p1, v0}, Lcom/airbnb/android/react/maps/AirMapUrlTile;->addToMap(Lcom/google/android/gms/maps/GoogleMap;)V

    .line 619
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->features:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 620
    :cond_5
    instance-of v0, p1, Lcom/airbnb/android/react/maps/AirMapWMSTile;

    if-eqz v0, :cond_6

    .line 621
    check-cast p1, Lcom/airbnb/android/react/maps/AirMapWMSTile;

    .line 622
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p1, v0}, Lcom/airbnb/android/react/maps/AirMapWMSTile;->addToMap(Lcom/google/android/gms/maps/GoogleMap;)V

    .line 623
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->features:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 624
    :cond_6
    instance-of v0, p1, Lcom/airbnb/android/react/maps/AirMapLocalTile;

    if-eqz v0, :cond_7

    .line 625
    check-cast p1, Lcom/airbnb/android/react/maps/AirMapLocalTile;

    .line 626
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p1, v0}, Lcom/airbnb/android/react/maps/AirMapLocalTile;->addToMap(Lcom/google/android/gms/maps/GoogleMap;)V

    .line 627
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->features:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 628
    :cond_7
    instance-of v0, p1, Lcom/airbnb/android/react/maps/AirMapOverlay;

    if-eqz v0, :cond_8

    .line 629
    check-cast p1, Lcom/airbnb/android/react/maps/AirMapOverlay;

    .line 630
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p1, v0}, Lcom/airbnb/android/react/maps/AirMapOverlay;->addToMap(Lcom/google/android/gms/maps/GoogleMap;)V

    .line 631
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->features:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 632
    :cond_8
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_9

    .line 633
    check-cast p1, Landroid/view/ViewGroup;

    const/4 v0, 0x0

    .line 634
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_a

    .line 635
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lcom/airbnb/android/react/maps/AirMapView;->addFeature(Landroid/view/View;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 638
    :cond_9
    invoke-virtual {p0, p1, p2}, Lcom/airbnb/android/react/maps/AirMapView;->addView(Landroid/view/View;I)V

    :cond_a
    :goto_1
    return-void
.end method

.method public animateToBearing(FI)V
    .locals 2

    .line 754
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v0, :cond_0

    return-void

    .line 755
    :cond_0
    new-instance v1, Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;-><init>(Lcom/google/android/gms/maps/model/CameraPosition;)V

    .line 756
    invoke-virtual {v1, p1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->bearing(F)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object p1

    .line 757
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->build()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object p1

    .line 758
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-static {p1}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newCameraPosition(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;ILcom/google/android/gms/maps/GoogleMap$CancelableCallback;)V

    return-void
.end method

.method public animateToCamera(Lcom/facebook/react/bridge/ReadableMap;I)V
    .locals 6

    .line 703
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v0, :cond_0

    return-void

    .line 704
    :cond_0
    new-instance v1, Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;-><init>(Lcom/google/android/gms/maps/model/CameraPosition;)V

    const-string/jumbo v0, "zoom"

    .line 705
    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 706
    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-float v0, v2

    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->zoom(F)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    :cond_1
    const-string v0, "heading"

    .line 708
    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 709
    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-float v0, v2

    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->bearing(F)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    :cond_2
    const-string v0, "pitch"

    .line 711
    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 712
    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-float v0, v2

    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->tilt(F)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    :cond_3
    const-string v0, "center"

    .line 714
    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 715
    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/ReadableMap;->getMap(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableMap;

    move-result-object p1

    .line 716
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    const-string v2, "latitude"

    invoke-interface {p1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    const-string v4, "longitude"

    invoke-interface {p1, v4}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->target(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    .line 719
    :cond_4
    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->build()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object p1

    invoke-static {p1}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newCameraPosition(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object p1

    if-gtz p2, :cond_5

    .line 722
    iget-object p2, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p2, p1}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    goto :goto_0

    .line 725
    :cond_5
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;ILcom/google/android/gms/maps/GoogleMap$CancelableCallback;)V

    :goto_0
    return-void
.end method

.method public animateToCoordinate(Lcom/google/android/gms/maps/model/LatLng;I)V
    .locals 2

    .line 762
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v0, :cond_0

    return-void

    .line 763
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLng(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;ILcom/google/android/gms/maps/GoogleMap$CancelableCallback;)V

    return-void
.end method

.method public animateToNavigation(Lcom/google/android/gms/maps/model/LatLng;FFI)V
    .locals 2

    .line 730
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v0, :cond_0

    return-void

    .line 731
    :cond_0
    new-instance v1, Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;-><init>(Lcom/google/android/gms/maps/model/CameraPosition;)V

    .line 732
    invoke-virtual {v1, p2}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->bearing(F)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object p2

    .line 733
    invoke-virtual {p2, p3}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->tilt(F)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object p2

    .line 734
    invoke-virtual {p2, p1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->target(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object p1

    .line 735
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->build()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object p1

    .line 736
    iget-object p2, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-static {p1}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newCameraPosition(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object p1

    const/4 p3, 0x0

    invoke-virtual {p2, p1, p4, p3}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;ILcom/google/android/gms/maps/GoogleMap$CancelableCallback;)V

    return-void
.end method

.method public animateToRegion(Lcom/google/android/gms/maps/model/LatLngBounds;I)V
    .locals 2

    .line 740
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    .line 741
    invoke-static {p1, v1}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngBounds(Lcom/google/android/gms/maps/model/LatLngBounds;I)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;ILcom/google/android/gms/maps/GoogleMap$CancelableCallback;)V

    return-void
.end method

.method public animateToViewingAngle(FI)V
    .locals 2

    .line 745
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v0, :cond_0

    return-void

    .line 747
    :cond_0
    new-instance v1, Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;-><init>(Lcom/google/android/gms/maps/model/CameraPosition;)V

    .line 748
    invoke-virtual {v1, p1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->tilt(F)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object p1

    .line 749
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->build()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object p1

    .line 750
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-static {p1}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newCameraPosition(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;ILcom/google/android/gms/maps/GoogleMap$CancelableCallback;)V

    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 909
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->gestureDetector:Landroidx/core/view/GestureDetectorCompat;

    invoke-virtual {v0, p1}, Landroidx/core/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 911
    invoke-static {p1}, Landroidx/core/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    if-eq v0, v2, :cond_0

    goto :goto_0

    .line 920
    :cond_0
    invoke-virtual {p0}, Lcom/airbnb/android/react/maps/AirMapView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 915
    :cond_1
    invoke-virtual {p0}, Lcom/airbnb/android/react/maps/AirMapView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v3, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v3, :cond_2

    .line 916
    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/maps/UiSettings;->isScrollGesturesEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v1, 0x1

    .line 915
    :cond_2
    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 923
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/gms/maps/MapView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    return v2
.end method

.method public declared-synchronized doDestroy()V
    .locals 3

    monitor-enter p0

    .line 408
    :try_start_0
    iget-boolean v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->destroyed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 409
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 411
    :try_start_1
    iput-boolean v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->destroyed:Z

    .line 413
    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->lifecycleListener:Lcom/facebook/react/bridge/LifecycleEventListener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    if-eqz v1, :cond_1

    .line 414
    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    iget-object v2, p0, Lcom/airbnb/android/react/maps/AirMapView;->lifecycleListener:Lcom/facebook/react/bridge/LifecycleEventListener;

    invoke-virtual {v1, v2}, Lcom/facebook/react/uimanager/ThemedReactContext;->removeLifecycleEventListener(Lcom/facebook/react/bridge/LifecycleEventListener;)V

    const/4 v1, 0x0

    .line 415
    iput-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->lifecycleListener:Lcom/facebook/react/bridge/LifecycleEventListener;

    .line 417
    :cond_1
    iget-boolean v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->paused:Z

    if-nez v1, :cond_2

    .line 418
    invoke-virtual {p0}, Lcom/airbnb/android/react/maps/AirMapView;->onPause()V

    .line 419
    iput-boolean v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->paused:Z

    .line 421
    :cond_2
    invoke-virtual {p0}, Lcom/airbnb/android/react/maps/AirMapView;->onDestroy()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 422
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public enableMapLoading(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 517
    iget-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->isMapLoaded:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    .line 518
    invoke-direct {p0}, Lcom/airbnb/android/react/maps/AirMapView;->getMapLoadingLayoutView()Landroid/widget/RelativeLayout;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public fitToCoordinates(Lcom/facebook/react/bridge/ReadableArray;Lcom/facebook/react/bridge/ReadableMap;Z)V
    .locals 8

    .line 836
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v0, :cond_0

    return-void

    .line 838
    :cond_0
    new-instance v0, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 840
    :goto_0
    invoke-interface {p1}, Lcom/facebook/react/bridge/ReadableArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 841
    invoke-interface {p1, v2}, Lcom/facebook/react/bridge/ReadableArray;->getMap(I)Lcom/facebook/react/bridge/ReadableMap;

    move-result-object v3

    const-string v4, "latitude"

    .line 842
    invoke-interface {v3, v4}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    const-string v5, "longitude"

    .line 843
    invoke-interface {v3, v5}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    .line 844
    new-instance v5, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    invoke-direct {v5, v6, v7, v3, v4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 847
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->build()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object p1

    const/16 v0, 0x32

    .line 848
    invoke-static {p1, v0}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngBounds(Lcom/google/android/gms/maps/model/LatLngBounds;I)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object p1

    if-eqz p2, :cond_2

    .line 851
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    const-string v2, "left"

    invoke-interface {p2, v2}, Lcom/facebook/react/bridge/ReadableMap;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v3, "top"

    invoke-interface {p2, v3}, Lcom/facebook/react/bridge/ReadableMap;->getInt(Ljava/lang/String;)I

    move-result v3

    const-string v4, "right"

    .line 852
    invoke-interface {p2, v4}, Lcom/facebook/react/bridge/ReadableMap;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v5, "bottom"

    invoke-interface {p2, v5}, Lcom/facebook/react/bridge/ReadableMap;->getInt(Ljava/lang/String;)I

    move-result p2

    .line 851
    invoke-virtual {v0, v2, v3, v4, p2}, Lcom/google/android/gms/maps/GoogleMap;->setPadding(IIII)V

    :cond_2
    if-eqz p3, :cond_3

    .line 856
    iget-object p2, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p2, p1}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    goto :goto_1

    .line 858
    :cond_3
    iget-object p2, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p2, p1}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 860
    :goto_1
    iget-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p1, v1, v1, v1, v1}, Lcom/google/android/gms/maps/GoogleMap;->setPadding(IIII)V

    return-void
.end method

.method public fitToElements(Z)V
    .locals 5

    .line 767
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v0, :cond_0

    return-void

    .line 769
    :cond_0
    new-instance v0, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;-><init>()V

    const/4 v1, 0x0

    .line 773
    iget-object v2, p0, Lcom/airbnb/android/react/maps/AirMapView;->features:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/airbnb/android/react/maps/AirMapFeature;

    .line 774
    instance-of v4, v3, Lcom/airbnb/android/react/maps/AirMapMarker;

    if-eqz v4, :cond_1

    .line 775
    invoke-virtual {v3}, Lcom/airbnb/android/react/maps/AirMapFeature;->getFeature()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/maps/model/Marker;

    .line 776
    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/Marker;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_4

    .line 782
    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->build()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    const/16 v1, 0x32

    .line 783
    invoke-static {v0, v1}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngBounds(Lcom/google/android/gms/maps/model/LatLngBounds;I)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v0

    if-eqz p1, :cond_3

    .line 785
    iget-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    goto :goto_1

    .line 787
    :cond_3
    iget-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    :cond_4
    :goto_1
    return-void
.end method

.method public fitToSuppliedMarkers(Lcom/facebook/react/bridge/ReadableArray;Lcom/facebook/react/bridge/ReadableMap;Z)V
    .locals 5

    .line 793
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v0, :cond_0

    return-void

    .line 795
    :cond_0
    new-instance v0, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;-><init>()V

    .line 797
    invoke-interface {p1}, Lcom/facebook/react/bridge/ReadableArray;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 798
    :goto_0
    invoke-interface {p1}, Lcom/facebook/react/bridge/ReadableArray;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 799
    invoke-interface {p1, v3}, Lcom/facebook/react/bridge/ReadableArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 804
    :cond_1
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 806
    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->features:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/airbnb/android/react/maps/AirMapFeature;

    .line 807
    instance-of v4, v3, Lcom/airbnb/android/react/maps/AirMapMarker;

    if-eqz v4, :cond_2

    .line 808
    move-object v4, v3

    check-cast v4, Lcom/airbnb/android/react/maps/AirMapMarker;

    invoke-virtual {v4}, Lcom/airbnb/android/react/maps/AirMapMarker;->getIdentifier()Ljava/lang/String;

    move-result-object v4

    .line 809
    invoke-virtual {v3}, Lcom/airbnb/android/react/maps/AirMapFeature;->getFeature()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/maps/model/Marker;

    .line 810
    invoke-interface {p1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 811
    invoke-virtual {v3}, Lcom/google/android/gms/maps/model/Marker;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    if-eqz v2, :cond_6

    .line 818
    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->build()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object p1

    const/16 v0, 0x32

    .line 819
    invoke-static {p1, v0}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngBounds(Lcom/google/android/gms/maps/model/LatLngBounds;I)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object p1

    if-eqz p2, :cond_4

    .line 822
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    const-string v1, "left"

    invoke-interface {p2, v1}, Lcom/facebook/react/bridge/ReadableMap;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "top"

    invoke-interface {p2, v2}, Lcom/facebook/react/bridge/ReadableMap;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v3, "right"

    .line 823
    invoke-interface {p2, v3}, Lcom/facebook/react/bridge/ReadableMap;->getInt(Ljava/lang/String;)I

    move-result v3

    const-string v4, "bottom"

    invoke-interface {p2, v4}, Lcom/facebook/react/bridge/ReadableMap;->getInt(Ljava/lang/String;)I

    move-result p2

    .line 822
    invoke-virtual {v0, v1, v2, v3, p2}, Lcom/google/android/gms/maps/GoogleMap;->setPadding(IIII)V

    :cond_4
    if-eqz p3, :cond_5

    .line 827
    iget-object p2, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p2, p1}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    goto :goto_2

    .line 829
    :cond_5
    iget-object p2, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p2, p1}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    :cond_6
    :goto_2
    return-void
.end method

.method public getFeatureAt(I)Landroid/view/View;
    .locals 1

    .line 647
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->features:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public getFeatureCount()I
    .locals 1

    .line 643
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->features:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getInfoContents(Lcom/google/android/gms/maps/model/Marker;)Landroid/view/View;
    .locals 0

    .line 903
    invoke-direct {p0, p1}, Lcom/airbnb/android/react/maps/AirMapView;->getMarkerMap(Lcom/google/android/gms/maps/model/Marker;)Lcom/airbnb/android/react/maps/AirMapMarker;

    move-result-object p1

    .line 904
    invoke-virtual {p1}, Lcom/airbnb/android/react/maps/AirMapMarker;->getInfoContents()Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getInfoWindow(Lcom/google/android/gms/maps/model/Marker;)Landroid/view/View;
    .locals 0

    .line 897
    invoke-direct {p0, p1}, Lcom/airbnb/android/react/maps/AirMapView;->getMarkerMap(Lcom/google/android/gms/maps/model/Marker;)Lcom/airbnb/android/react/maps/AirMapMarker;

    move-result-object p1

    .line 898
    invoke-virtual {p1}, Lcom/airbnb/android/react/maps/AirMapMarker;->getCallout()Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getMapBoundaries()[[D
    .locals 8

    .line 865
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getProjection()Lcom/google/android/gms/maps/Projection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/Projection;->getVisibleRegion()Lcom/google/android/gms/maps/model/VisibleRegion;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/maps/model/VisibleRegion;->latLngBounds:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 866
    iget-object v1, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->northeast:Lcom/google/android/gms/maps/model/LatLng;

    .line 867
    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->southwest:Lcom/google/android/gms/maps/model/LatLng;

    const/4 v2, 0x2

    .line 869
    new-array v3, v2, [[D

    new-array v4, v2, [D

    iget-wide v5, v1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    const/4 v7, 0x0

    aput-wide v5, v4, v7

    iget-wide v5, v1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    const/4 v1, 0x1

    aput-wide v5, v4, v1

    aput-object v4, v3, v7

    new-array v2, v2, [D

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    aput-wide v4, v2, v7

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    aput-wide v4, v2, v1

    aput-object v2, v3, v1

    return-object v3
.end method

.method public makeClickEventData(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/react/bridge/WritableMap;
    .locals 5

    .line 659
    new-instance v0, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v0}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 661
    new-instance v1, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v1}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 662
    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    const-string v4, "latitude"

    invoke-interface {v1, v4, v2, v3}, Lcom/facebook/react/bridge/WritableMap;->putDouble(Ljava/lang/String;D)V

    .line 663
    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    const-string v4, "longitude"

    invoke-interface {v1, v4, v2, v3}, Lcom/facebook/react/bridge/WritableMap;->putDouble(Ljava/lang/String;D)V

    const-string v2, "coordinate"

    .line 664
    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putMap(Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    .line 666
    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/GoogleMap;->getProjection()Lcom/google/android/gms/maps/Projection;

    move-result-object v1

    .line 667
    invoke-virtual {v1, p1}, Lcom/google/android/gms/maps/Projection;->toScreenLocation(Lcom/google/android/gms/maps/model/LatLng;)Landroid/graphics/Point;

    move-result-object p1

    .line 669
    new-instance v1, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v1}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 670
    iget v2, p1, Landroid/graphics/Point;->x:I

    int-to-double v2, v2

    const-string/jumbo v4, "x"

    invoke-interface {v1, v4, v2, v3}, Lcom/facebook/react/bridge/WritableMap;->putDouble(Ljava/lang/String;D)V

    .line 671
    iget p1, p1, Landroid/graphics/Point;->y:I

    int-to-double v2, p1

    const-string/jumbo p1, "y"

    invoke-interface {v1, p1, v2, v3}, Lcom/facebook/react/bridge/WritableMap;->putDouble(Ljava/lang/String;D)V

    const-string p1, "position"

    .line 672
    invoke-interface {v0, p1, v1}, Lcom/facebook/react/bridge/WritableMap;->putMap(Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    return-object v0
.end method

.method public onIndoorBuildingFocused()V
    .locals 13

    .line 1158
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getFocusedBuilding()Lcom/google/android/gms/maps/model/IndoorBuilding;

    move-result-object v0

    const-string v1, "onIndoorBuildingFocused"

    const-string v2, "IndoorBuilding"

    const-string v3, "underground"

    const-string v4, "activeLevelIndex"

    const-string v5, "levels"

    const/4 v6, 0x0

    if-eqz v0, :cond_1

    .line 1160
    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/IndoorBuilding;->getLevels()Ljava/util/List;

    move-result-object v7

    .line 1162
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createArray()Lcom/facebook/react/bridge/WritableArray;

    move-result-object v8

    .line 1163
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/gms/maps/model/IndoorLevel;

    .line 1164
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v10

    const-string v11, "index"

    .line 1165
    invoke-interface {v10, v11, v6}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    .line 1166
    invoke-virtual {v9}, Lcom/google/android/gms/maps/model/IndoorLevel;->getName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "name"

    invoke-interface {v10, v12, v11}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1167
    invoke-virtual {v9}, Lcom/google/android/gms/maps/model/IndoorLevel;->getShortName()Ljava/lang/String;

    move-result-object v9

    const-string v11, "shortName"

    invoke-interface {v10, v11, v9}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1168
    invoke-interface {v8, v10}, Lcom/facebook/react/bridge/WritableArray;->pushMap(Lcom/facebook/react/bridge/WritableMap;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1171
    :cond_0
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v6

    .line 1172
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v7

    .line 1173
    invoke-interface {v7, v5, v8}, Lcom/facebook/react/bridge/WritableMap;->putArray(Ljava/lang/String;Lcom/facebook/react/bridge/WritableArray;)V

    .line 1174
    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/IndoorBuilding;->getActiveLevelIndex()I

    move-result v5

    invoke-interface {v7, v4, v5}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    .line 1175
    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/IndoorBuilding;->isUnderground()Z

    move-result v0

    invoke-interface {v7, v3, v0}, Lcom/facebook/react/bridge/WritableMap;->putBoolean(Ljava/lang/String;Z)V

    .line 1177
    invoke-interface {v6, v2, v7}, Lcom/facebook/react/bridge/WritableMap;->putMap(Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    .line 1179
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    iget-object v2, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    invoke-virtual {v0, v2, p0, v1, v6}, Lcom/airbnb/android/react/maps/AirMapManager;->pushEvent(Lcom/facebook/react/uimanager/ThemedReactContext;Landroid/view/View;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    goto :goto_1

    .line 1181
    :cond_1
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v0

    .line 1182
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createArray()Lcom/facebook/react/bridge/WritableArray;

    move-result-object v7

    .line 1183
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v8

    .line 1184
    invoke-interface {v8, v5, v7}, Lcom/facebook/react/bridge/WritableMap;->putArray(Ljava/lang/String;Lcom/facebook/react/bridge/WritableArray;)V

    .line 1185
    invoke-interface {v8, v4, v6}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    .line 1186
    invoke-interface {v8, v3, v6}, Lcom/facebook/react/bridge/WritableMap;->putBoolean(Ljava/lang/String;Z)V

    .line 1188
    invoke-interface {v0, v2, v8}, Lcom/facebook/react/bridge/WritableMap;->putMap(Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    .line 1190
    iget-object v2, p0, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    iget-object v3, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    invoke-virtual {v2, v3, p0, v1, v0}, Lcom/airbnb/android/react/maps/AirMapManager;->pushEvent(Lcom/facebook/react/uimanager/ThemedReactContext;Landroid/view/View;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    :goto_1
    return-void
.end method

.method public onIndoorLevelActivated(Lcom/google/android/gms/maps/model/IndoorBuilding;)V
    .locals 4

    if-nez p1, :cond_0

    return-void

    .line 1199
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/IndoorBuilding;->getActiveLevelIndex()I

    move-result v0

    if-ltz v0, :cond_2

    .line 1200
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/IndoorBuilding;->getLevels()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    goto :goto_0

    .line 1203
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/IndoorBuilding;->getLevels()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/maps/model/IndoorLevel;

    .line 1205
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v1

    .line 1206
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v2

    const-string v3, "activeLevelIndex"

    .line 1208
    invoke-interface {v2, v3, v0}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    .line 1209
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/IndoorLevel;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v3, "name"

    invoke-interface {v2, v3, v0}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1210
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/IndoorLevel;->getShortName()Ljava/lang/String;

    move-result-object p1

    const-string v0, "shortName"

    invoke-interface {v2, v0, p1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "IndoorLevel"

    .line 1212
    invoke-interface {v1, p1, v2}, Lcom/facebook/react/bridge/WritableMap;->putMap(Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    .line 1214
    iget-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    const-string v2, "onIndoorLevelActivated"

    invoke-virtual {p1, v0, p0, v2, v1}, Lcom/airbnb/android/react/maps/AirMapManager;->pushEvent(Lcom/facebook/react/uimanager/ThemedReactContext;Landroid/view/View;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
    .locals 4

    .line 197
    iget-boolean v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->destroyed:Z

    if-eqz v0, :cond_0

    return-void

    .line 200
    :cond_0
    iput-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    .line 201
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/maps/GoogleMap;->setInfoWindowAdapter(Lcom/google/android/gms/maps/GoogleMap$InfoWindowAdapter;)V

    .line 202
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/maps/GoogleMap;->setOnMarkerDragListener(Lcom/google/android/gms/maps/GoogleMap$OnMarkerDragListener;)V

    .line 203
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/maps/GoogleMap;->setOnPoiClickListener(Lcom/google/android/gms/maps/GoogleMap$OnPoiClickListener;)V

    .line 204
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/maps/GoogleMap;->setOnIndoorStateChangeListener(Lcom/google/android/gms/maps/GoogleMap$OnIndoorStateChangeListener;)V

    .line 206
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    new-instance v2, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v2}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    const-string v3, "onMapReady"

    invoke-virtual {v0, v1, p0, v3, v2}, Lcom/airbnb/android/react/maps/AirMapManager;->pushEvent(Lcom/facebook/react/uimanager/ThemedReactContext;Landroid/view/View;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    .line 210
    new-instance v0, Lcom/airbnb/android/react/maps/AirMapView$3;

    invoke-direct {v0, p0, p0}, Lcom/airbnb/android/react/maps/AirMapView$3;-><init>(Lcom/airbnb/android/react/maps/AirMapView;Lcom/airbnb/android/react/maps/AirMapView;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->setOnMyLocationChangeListener(Lcom/google/android/gms/maps/GoogleMap$OnMyLocationChangeListener;)V

    .line 232
    new-instance v0, Lcom/airbnb/android/react/maps/AirMapView$4;

    invoke-direct {v0, p0, p0}, Lcom/airbnb/android/react/maps/AirMapView$4;-><init>(Lcom/airbnb/android/react/maps/AirMapView;Lcom/airbnb/android/react/maps/AirMapView;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->setOnMarkerClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMarkerClickListener;)V

    .line 260
    new-instance v0, Lcom/airbnb/android/react/maps/AirMapView$5;

    invoke-direct {v0, p0}, Lcom/airbnb/android/react/maps/AirMapView$5;-><init>(Lcom/airbnb/android/react/maps/AirMapView;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->setOnPolygonClickListener(Lcom/google/android/gms/maps/GoogleMap$OnPolygonClickListener;)V

    .line 269
    new-instance v0, Lcom/airbnb/android/react/maps/AirMapView$6;

    invoke-direct {v0, p0}, Lcom/airbnb/android/react/maps/AirMapView$6;-><init>(Lcom/airbnb/android/react/maps/AirMapView;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->setOnPolylineClickListener(Lcom/google/android/gms/maps/GoogleMap$OnPolylineClickListener;)V

    .line 278
    new-instance v0, Lcom/airbnb/android/react/maps/AirMapView$7;

    invoke-direct {v0, p0, p0}, Lcom/airbnb/android/react/maps/AirMapView$7;-><init>(Lcom/airbnb/android/react/maps/AirMapView;Lcom/airbnb/android/react/maps/AirMapView;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->setOnInfoWindowClickListener(Lcom/google/android/gms/maps/GoogleMap$OnInfoWindowClickListener;)V

    .line 299
    new-instance v0, Lcom/airbnb/android/react/maps/AirMapView$8;

    invoke-direct {v0, p0, p0}, Lcom/airbnb/android/react/maps/AirMapView$8;-><init>(Lcom/airbnb/android/react/maps/AirMapView;Lcom/airbnb/android/react/maps/AirMapView;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->setOnMapClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;)V

    .line 308
    new-instance v0, Lcom/airbnb/android/react/maps/AirMapView$9;

    invoke-direct {v0, p0, p0}, Lcom/airbnb/android/react/maps/AirMapView$9;-><init>(Lcom/airbnb/android/react/maps/AirMapView;Lcom/airbnb/android/react/maps/AirMapView;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->setOnMapLongClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMapLongClickListener;)V

    .line 317
    new-instance v0, Lcom/airbnb/android/react/maps/AirMapView$10;

    invoke-direct {v0, p0}, Lcom/airbnb/android/react/maps/AirMapView$10;-><init>(Lcom/airbnb/android/react/maps/AirMapView;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->setOnCameraMoveStartedListener(Lcom/google/android/gms/maps/GoogleMap$OnCameraMoveStartedListener;)V

    .line 324
    new-instance v0, Lcom/airbnb/android/react/maps/AirMapView$11;

    invoke-direct {v0, p0, p1}, Lcom/airbnb/android/react/maps/AirMapView$11;-><init>(Lcom/airbnb/android/react/maps/AirMapView;Lcom/google/android/gms/maps/GoogleMap;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->setOnCameraMoveListener(Lcom/google/android/gms/maps/GoogleMap$OnCameraMoveListener;)V

    .line 333
    new-instance v0, Lcom/airbnb/android/react/maps/AirMapView$12;

    invoke-direct {v0, p0, p1}, Lcom/airbnb/android/react/maps/AirMapView$12;-><init>(Lcom/airbnb/android/react/maps/AirMapView;Lcom/google/android/gms/maps/GoogleMap;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->setOnCameraIdleListener(Lcom/google/android/gms/maps/GoogleMap$OnCameraIdleListener;)V

    .line 346
    new-instance v0, Lcom/airbnb/android/react/maps/AirMapView$13;

    invoke-direct {v0, p0}, Lcom/airbnb/android/react/maps/AirMapView$13;-><init>(Lcom/airbnb/android/react/maps/AirMapView;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->setOnMapLoadedCallback(Lcom/google/android/gms/maps/GoogleMap$OnMapLoadedCallback;)V

    .line 360
    new-instance v0, Lcom/airbnb/android/react/maps/AirMapView$14;

    invoke-direct {v0, p0, p1}, Lcom/airbnb/android/react/maps/AirMapView$14;-><init>(Lcom/airbnb/android/react/maps/AirMapView;Lcom/google/android/gms/maps/GoogleMap;)V

    iput-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->lifecycleListener:Lcom/facebook/react/bridge/LifecycleEventListener;

    .line 395
    iget-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->lifecycleListener:Lcom/facebook/react/bridge/LifecycleEventListener;

    invoke-virtual {p1, v0}, Lcom/facebook/react/uimanager/ThemedReactContext;->addLifecycleEventListener(Lcom/facebook/react/bridge/LifecycleEventListener;)V

    return-void
.end method

.method public onMarkerDrag(Lcom/google/android/gms/maps/model/Marker;)V
    .locals 4

    .line 939
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/airbnb/android/react/maps/AirMapView;->makeClickEventData(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object v0

    .line 940
    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    iget-object v2, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    const-string v3, "onMarkerDrag"

    invoke-virtual {v1, v2, p0, v3, v0}, Lcom/airbnb/android/react/maps/AirMapManager;->pushEvent(Lcom/facebook/react/uimanager/ThemedReactContext;Landroid/view/View;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    .line 942
    invoke-direct {p0, p1}, Lcom/airbnb/android/react/maps/AirMapView;->getMarkerMap(Lcom/google/android/gms/maps/model/Marker;)Lcom/airbnb/android/react/maps/AirMapMarker;

    move-result-object v0

    .line 943
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/airbnb/android/react/maps/AirMapView;->makeClickEventData(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object p1

    .line 944
    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    iget-object v2, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    const-string v3, "onDrag"

    invoke-virtual {v1, v2, v0, v3, p1}, Lcom/airbnb/android/react/maps/AirMapManager;->pushEvent(Lcom/facebook/react/uimanager/ThemedReactContext;Landroid/view/View;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    return-void
.end method

.method public onMarkerDragEnd(Lcom/google/android/gms/maps/model/Marker;)V
    .locals 4

    .line 949
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/airbnb/android/react/maps/AirMapView;->makeClickEventData(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object v0

    .line 950
    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    iget-object v2, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    const-string v3, "onMarkerDragEnd"

    invoke-virtual {v1, v2, p0, v3, v0}, Lcom/airbnb/android/react/maps/AirMapManager;->pushEvent(Lcom/facebook/react/uimanager/ThemedReactContext;Landroid/view/View;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    .line 952
    invoke-direct {p0, p1}, Lcom/airbnb/android/react/maps/AirMapView;->getMarkerMap(Lcom/google/android/gms/maps/model/Marker;)Lcom/airbnb/android/react/maps/AirMapMarker;

    move-result-object v0

    .line 953
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/airbnb/android/react/maps/AirMapView;->makeClickEventData(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object p1

    .line 954
    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    iget-object v2, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    const-string v3, "onDragEnd"

    invoke-virtual {v1, v2, v0, v3, p1}, Lcom/airbnb/android/react/maps/AirMapManager;->pushEvent(Lcom/facebook/react/uimanager/ThemedReactContext;Landroid/view/View;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    return-void
.end method

.method public onMarkerDragStart(Lcom/google/android/gms/maps/model/Marker;)V
    .locals 4

    .line 929
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/airbnb/android/react/maps/AirMapView;->makeClickEventData(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object v0

    .line 930
    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    iget-object v2, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    const-string v3, "onMarkerDragStart"

    invoke-virtual {v1, v2, p0, v3, v0}, Lcom/airbnb/android/react/maps/AirMapManager;->pushEvent(Lcom/facebook/react/uimanager/ThemedReactContext;Landroid/view/View;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    .line 932
    invoke-direct {p0, p1}, Lcom/airbnb/android/react/maps/AirMapView;->getMarkerMap(Lcom/google/android/gms/maps/model/Marker;)Lcom/airbnb/android/react/maps/AirMapMarker;

    move-result-object v0

    .line 933
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/airbnb/android/react/maps/AirMapView;->makeClickEventData(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object p1

    .line 934
    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    iget-object v2, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    const-string v3, "onDragStart"

    invoke-virtual {v1, v2, v0, v3, p1}, Lcom/airbnb/android/react/maps/AirMapManager;->pushEvent(Lcom/facebook/react/uimanager/ThemedReactContext;Landroid/view/View;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    return-void
.end method

.method public onPanDrag(Landroid/view/MotionEvent;)V
    .locals 3

    .line 1054
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    float-to-int p1, p1

    invoke-direct {v0, v1, p1}, Landroid/graphics/Point;-><init>(II)V

    .line 1055
    iget-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/GoogleMap;->getProjection()Lcom/google/android/gms/maps/Projection;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/Projection;->fromScreenLocation(Landroid/graphics/Point;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object p1

    .line 1056
    invoke-virtual {p0, p1}, Lcom/airbnb/android/react/maps/AirMapView;->makeClickEventData(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object p1

    .line 1057
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    const-string v2, "onPanDrag"

    invoke-virtual {v0, v1, p0, v2, p1}, Lcom/airbnb/android/react/maps/AirMapManager;->pushEvent(Lcom/facebook/react/uimanager/ThemedReactContext;Landroid/view/View;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    return-void
.end method

.method public onPoiClick(Lcom/google/android/gms/maps/model/PointOfInterest;)V
    .locals 3

    .line 959
    iget-object v0, p1, Lcom/google/android/gms/maps/model/PointOfInterest;->latLng:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p0, v0}, Lcom/airbnb/android/react/maps/AirMapView;->makeClickEventData(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object v0

    .line 961
    iget-object v1, p1, Lcom/google/android/gms/maps/model/PointOfInterest;->placeId:Ljava/lang/String;

    const-string v2, "placeId"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    iget-object p1, p1, Lcom/google/android/gms/maps/model/PointOfInterest;->name:Ljava/lang/String;

    const-string v1, "name"

    invoke-interface {v0, v1, p1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 964
    iget-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    const-string v2, "onPoiClick"

    invoke-virtual {p1, v1, p0, v2, v0}, Lcom/airbnb/android/react/maps/AirMapManager;->pushEvent(Lcom/facebook/react/uimanager/ThemedReactContext;Landroid/view/View;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    return-void
.end method

.method public removeFeatureAt(I)V
    .locals 2

    .line 651
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->features:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/airbnb/android/react/maps/AirMapFeature;

    .line 652
    instance-of v0, p1, Lcom/airbnb/android/react/maps/AirMapMarker;

    if-eqz v0, :cond_0

    .line 653
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->markerMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/airbnb/android/react/maps/AirMapFeature;->getFeature()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 655
    :cond_0
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p1, v0}, Lcom/airbnb/android/react/maps/AirMapFeature;->removeFromMap(Lcom/google/android/gms/maps/GoogleMap;)V

    return-void
.end method

.method public setCacheEnabled(Z)V
    .locals 0

    .line 512
    iput-boolean p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->cacheEnabled:Z

    .line 513
    invoke-direct {p0}, Lcom/airbnb/android/react/maps/AirMapView;->cacheView()V

    return-void
.end method

.method public setCamera(Lcom/facebook/react/bridge/ReadableMap;)V
    .locals 6

    if-nez p1, :cond_0

    return-void

    .line 465
    :cond_0
    new-instance v0, Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;-><init>()V

    const-string v1, "center"

    .line 467
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableMap;->getMap(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableMap;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "longitude"

    .line 469
    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const-string v3, "latitude"

    .line 470
    invoke-interface {v1, v3}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 471
    new-instance v3, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-direct {v3, v4, v5, v1, v2}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v3}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->target(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    :cond_1
    const-string v1, "pitch"

    .line 474
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->tilt(F)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    const-string v1, "heading"

    .line 475
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->bearing(F)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    const-string/jumbo v1, "zoom"

    .line 476
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableMap;->getInt(Ljava/lang/String;)I

    move-result p1

    int-to-float p1, p1

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->zoom(F)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    .line 478
    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->build()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object p1

    invoke-static {p1}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newCameraPosition(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object p1

    .line 480
    invoke-super {p0}, Lcom/google/android/gms/maps/MapView;->getHeight()I

    move-result v0

    if-lez v0, :cond_3

    invoke-super {p0}, Lcom/google/android/gms/maps/MapView;->getWidth()I

    move-result v0

    if-gtz v0, :cond_2

    goto :goto_0

    .line 486
    :cond_2
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    const/4 p1, 0x0

    .line 487
    iput-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->cameraToSet:Lcom/google/android/gms/maps/CameraUpdate;

    goto :goto_1

    .line 484
    :cond_3
    :goto_0
    iput-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->cameraToSet:Lcom/google/android/gms/maps/CameraUpdate;

    :goto_1
    return-void
.end method

.method public setHandlePanDrag(Z)V
    .locals 0

    .line 568
    iput-boolean p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->handlePanDrag:Z

    return-void
.end method

.method public setIndoorActiveLevelIndex(I)V
    .locals 2

    .line 1218
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getFocusedBuilding()Lcom/google/android/gms/maps/model/IndoorBuilding;

    move-result-object v0

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    .line 1220
    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/IndoorBuilding;->getLevels()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 1221
    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/IndoorBuilding;->getLevels()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/maps/model/IndoorLevel;

    if-eqz p1, :cond_0

    .line 1223
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/IndoorLevel;->activate()V

    :cond_0
    return-void
.end method

.method public setInitialCamera(Lcom/facebook/react/bridge/ReadableMap;)V
    .locals 1

    .line 432
    iget-boolean v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->initialCameraSet:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 433
    invoke-virtual {p0, p1}, Lcom/airbnb/android/react/maps/AirMapView;->setCamera(Lcom/facebook/react/bridge/ReadableMap;)V

    const/4 p1, 0x1

    .line 434
    iput-boolean p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->initialCameraSet:Z

    :cond_0
    return-void
.end method

.method public setInitialRegion(Lcom/facebook/react/bridge/ReadableMap;)V
    .locals 1

    .line 425
    iget-boolean v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->initialRegionSet:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 426
    invoke-virtual {p0, p1}, Lcom/airbnb/android/react/maps/AirMapView;->setRegion(Lcom/facebook/react/bridge/ReadableMap;)V

    const/4 p1, 0x1

    .line 427
    iput-boolean p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->initialRegionSet:Z

    :cond_0
    return-void
.end method

.method public setKmlSrc(Ljava/lang/String;)V
    .locals 17

    move-object/from16 v1, p0

    const-string v0, "name"

    const-string v2, "description"

    .line 1062
    :try_start_0
    new-instance v3, Lcom/airbnb/android/react/maps/FileUtil;

    iget-object v4, v1, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    invoke-direct {v3, v4}, Lcom/airbnb/android/react/maps/FileUtil;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v3, v5}, Lcom/airbnb/android/react/maps/FileUtil;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/AsyncTask;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/InputStream;

    if-nez v3, :cond_0

    return-void

    .line 1068
    :cond_0
    new-instance v5, Lcom/google/maps/android/data/kml/KmlLayer;

    iget-object v7, v1, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    iget-object v8, v1, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    invoke-direct {v5, v7, v3, v8}, Lcom/google/maps/android/data/kml/KmlLayer;-><init>(Lcom/google/android/gms/maps/GoogleMap;Ljava/io/InputStream;Landroid/content/Context;)V

    iput-object v5, v1, Lcom/airbnb/android/react/maps/AirMapView;->kmlLayer:Lcom/google/maps/android/data/kml/KmlLayer;

    .line 1069
    iget-object v3, v1, Lcom/airbnb/android/react/maps/AirMapView;->kmlLayer:Lcom/google/maps/android/data/kml/KmlLayer;

    invoke-virtual {v3}, Lcom/google/maps/android/data/kml/KmlLayer;->addLayerToMap()V

    .line 1071
    new-instance v3, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v3}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 1072
    new-instance v5, Lcom/facebook/react/bridge/WritableNativeArray;

    invoke-direct {v5}, Lcom/facebook/react/bridge/WritableNativeArray;-><init>()V

    .line 1074
    iget-object v7, v1, Lcom/airbnb/android/react/maps/AirMapView;->kmlLayer:Lcom/google/maps/android/data/kml/KmlLayer;

    invoke-virtual {v7}, Lcom/google/maps/android/data/kml/KmlLayer;->getContainers()Ljava/lang/Iterable;

    move-result-object v7
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v8, "onKmlReady"

    if-nez v7, :cond_1

    .line 1075
    :try_start_1
    iget-object v0, v1, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    iget-object v2, v1, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    invoke-virtual {v0, v2, v1, v8, v3}, Lcom/airbnb/android/react/maps/AirMapManager;->pushEvent(Lcom/facebook/react/uimanager/ThemedReactContext;Landroid/view/View;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    return-void

    .line 1080
    :cond_1
    iget-object v7, v1, Lcom/airbnb/android/react/maps/AirMapView;->kmlLayer:Lcom/google/maps/android/data/kml/KmlLayer;

    invoke-virtual {v7}, Lcom/google/maps/android/data/kml/KmlLayer;->getContainers()Ljava/lang/Iterable;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/maps/android/data/kml/KmlContainer;

    if-eqz v7, :cond_a

    .line 1081
    invoke-virtual {v7}, Lcom/google/maps/android/data/kml/KmlContainer;->getContainers()Ljava/lang/Iterable;

    move-result-object v9

    if-nez v9, :cond_2

    goto/16 :goto_4

    .line 1087
    :cond_2
    invoke-virtual {v7}, Lcom/google/maps/android/data/kml/KmlContainer;->getContainers()Ljava/lang/Iterable;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1088
    invoke-virtual {v7}, Lcom/google/maps/android/data/kml/KmlContainer;->getContainers()Ljava/lang/Iterable;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/maps/android/data/kml/KmlContainer;

    .line 1091
    :cond_3
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 1092
    invoke-virtual {v7}, Lcom/google/maps/android/data/kml/KmlContainer;->getPlacemarks()Ljava/lang/Iterable;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/maps/android/data/kml/KmlPlacemark;

    .line 1093
    new-instance v11, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v11}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    .line 1095
    invoke-virtual {v10}, Lcom/google/maps/android/data/kml/KmlPlacemark;->getInlineStyle()Lcom/google/maps/android/data/kml/KmlStyle;

    move-result-object v12

    if-eqz v12, :cond_4

    .line 1096
    invoke-virtual {v10}, Lcom/google/maps/android/data/kml/KmlPlacemark;->getMarkerOptions()Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v11

    goto :goto_1

    .line 1098
    :cond_4
    invoke-static {}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->defaultMarker()Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/google/android/gms/maps/model/MarkerOptions;->icon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 1101
    :goto_1
    invoke-virtual {v10}, Lcom/google/maps/android/data/kml/KmlPlacemark;->getGeometry()Lcom/google/maps/android/data/Geometry;

    move-result-object v12

    invoke-interface {v12}, Lcom/google/maps/android/data/Geometry;->getGeometryObject()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/gms/maps/model/LatLng;

    .line 1105
    invoke-virtual {v10, v0}, Lcom/google/maps/android/data/kml/KmlPlacemark;->hasProperty(Ljava/lang/String;)Z

    move-result v13
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0

    const-string v14, ""

    if-eqz v13, :cond_5

    .line 1106
    :try_start_2
    invoke-virtual {v10, v0}, Lcom/google/maps/android/data/kml/KmlPlacemark;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    goto :goto_2

    :cond_5
    move-object v13, v14

    .line 1109
    :goto_2
    invoke-virtual {v10, v2}, Lcom/google/maps/android/data/kml/KmlPlacemark;->hasProperty(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 1110
    invoke-virtual {v10, v2}, Lcom/google/maps/android/data/kml/KmlPlacemark;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 1113
    :cond_6
    invoke-virtual {v11, v12}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 1114
    invoke-virtual {v11, v13}, Lcom/google/android/gms/maps/model/MarkerOptions;->title(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 1115
    invoke-virtual {v11, v14}, Lcom/google/android/gms/maps/model/MarkerOptions;->snippet(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 1117
    new-instance v15, Lcom/airbnb/android/react/maps/AirMapMarker;

    iget-object v4, v1, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    move-object/from16 v16, v0

    iget-object v0, v1, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    invoke-virtual {v0}, Lcom/airbnb/android/react/maps/AirMapManager;->getMarkerManager()Lcom/airbnb/android/react/maps/AirMapMarkerManager;

    move-result-object v0

    invoke-direct {v15, v4, v11, v0}, Lcom/airbnb/android/react/maps/AirMapMarker;-><init>(Landroid/content/Context;Lcom/google/android/gms/maps/model/MarkerOptions;Lcom/airbnb/android/react/maps/AirMapMarkerManager;)V

    .line 1119
    invoke-virtual {v10}, Lcom/google/maps/android/data/kml/KmlPlacemark;->getInlineStyle()Lcom/google/maps/android/data/kml/KmlStyle;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1120
    invoke-virtual {v10}, Lcom/google/maps/android/data/kml/KmlPlacemark;->getInlineStyle()Lcom/google/maps/android/data/kml/KmlStyle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/android/data/kml/KmlStyle;->getIconUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1121
    invoke-virtual {v10}, Lcom/google/maps/android/data/kml/KmlPlacemark;->getInlineStyle()Lcom/google/maps/android/data/kml/KmlStyle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/android/data/kml/KmlStyle;->getIconUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v15, v0}, Lcom/airbnb/android/react/maps/AirMapMarker;->setImage(Ljava/lang/String;)V

    goto :goto_3

    .line 1122
    :cond_7
    invoke-virtual {v10}, Lcom/google/maps/android/data/kml/KmlPlacemark;->getStyleId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/maps/android/data/kml/KmlContainer;->getStyle(Ljava/lang/String;)Lcom/google/maps/android/data/kml/KmlStyle;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1123
    invoke-virtual {v10}, Lcom/google/maps/android/data/kml/KmlPlacemark;->getStyleId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/maps/android/data/kml/KmlContainer;->getStyle(Ljava/lang/String;)Lcom/google/maps/android/data/kml/KmlStyle;

    move-result-object v0

    .line 1124
    invoke-virtual {v0}, Lcom/google/maps/android/data/kml/KmlStyle;->getIconUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v15, v0}, Lcom/airbnb/android/react/maps/AirMapMarker;->setImage(Ljava/lang/String;)V

    .line 1127
    :cond_8
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " - "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1129
    invoke-virtual {v15, v0}, Lcom/airbnb/android/react/maps/AirMapMarker;->setIdentifier(Ljava/lang/String;)V

    .line 1131
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v10, 0x1

    add-int/2addr v4, v10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v1, v15, v6}, Lcom/airbnb/android/react/maps/AirMapView;->addFeature(Landroid/view/View;I)V

    .line 1133
    invoke-virtual {v1, v12}, Lcom/airbnb/android/react/maps/AirMapView;->makeClickEventData(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object v6

    const-string v11, "id"

    .line 1134
    invoke-interface {v6, v11, v0}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "title"

    .line 1135
    invoke-interface {v6, v0, v13}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1136
    invoke-interface {v6, v2, v14}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1138
    invoke-interface {v5, v6}, Lcom/facebook/react/bridge/WritableArray;->pushMap(Lcom/facebook/react/bridge/WritableMap;)V

    move-object v6, v4

    move-object/from16 v0, v16

    const/4 v4, 0x1

    goto/16 :goto_0

    :cond_9
    const-string v0, "markers"

    .line 1141
    invoke-interface {v3, v0, v5}, Lcom/facebook/react/bridge/WritableMap;->putArray(Ljava/lang/String;Lcom/facebook/react/bridge/WritableArray;)V

    .line 1143
    iget-object v0, v1, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    iget-object v2, v1, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    invoke-virtual {v0, v2, v1, v8, v3}, Lcom/airbnb/android/react/maps/AirMapManager;->pushEvent(Lcom/facebook/react/uimanager/ThemedReactContext;Landroid/view/View;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    goto :goto_5

    .line 1082
    :cond_a
    :goto_4
    iget-object v0, v1, Lcom/airbnb/android/react/maps/AirMapView;->manager:Lcom/airbnb/android/react/maps/AirMapManager;

    iget-object v2, v1, Lcom/airbnb/android/react/maps/AirMapView;->context:Lcom/facebook/react/uimanager/ThemedReactContext;

    invoke-virtual {v0, v2, v1, v8, v3}, Lcom/airbnb/android/react/maps/AirMapManager;->pushEvent(Lcom/facebook/react/uimanager/ThemedReactContext;Landroid/view/View;Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 1152
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->printStackTrace()V

    goto :goto_5

    :catch_1
    move-exception v0

    .line 1150
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_5

    :catch_2
    move-exception v0

    .line 1148
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    :catch_3
    move-exception v0

    .line 1146
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    :goto_5
    return-void
.end method

.method public setLoadingBackgroundColor(Ljava/lang/Integer;)V
    .locals 1

    .line 527
    iput-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->loadingBackgroundColor:Ljava/lang/Integer;

    .line 529
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    const/4 p1, -0x1

    .line 531
    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    goto :goto_0

    .line 533
    :cond_0
    iget-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->loadingBackgroundColor:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public setLoadingIndicatorColor(Ljava/lang/Integer;)V
    .locals 3

    .line 539
    iput-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->loadingIndicatorColor:Ljava/lang/Integer;

    .line 540
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_4

    if-nez p1, :cond_0

    const-string v0, "#606060"

    .line 543
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, p1

    .line 546
    :goto_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    .line 547
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 548
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 549
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    .line 551
    iget-object v2, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setProgressTintList(Landroid/content/res/ColorStateList;)V

    .line 552
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setSecondaryProgressTintList(Landroid/content/res/ColorStateList;)V

    .line 553
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setIndeterminateTintList(Landroid/content/res/ColorStateList;)V

    goto :goto_1

    .line 555
    :cond_1
    sget-object p1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    .line 556
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xa

    if-gt v1, v2, :cond_2

    .line 557
    sget-object p1, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    .line 559
    :cond_2
    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 560
    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 561
    :cond_3
    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 562
    iget-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->mapLoadingProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    :cond_4
    :goto_1
    return-void
.end method

.method public setMapBoundaries(Lcom/facebook/react/bridge/ReadableMap;Lcom/facebook/react/bridge/ReadableMap;)V
    .locals 9

    .line 876
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v0, :cond_0

    return-void

    .line 878
    :cond_0
    new-instance v0, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;-><init>()V

    const-string v1, "latitude"

    .line 880
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const-string v3, "longitude"

    .line 881
    invoke-interface {p1, v3}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    .line 882
    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    .line 884
    invoke-interface {p2, v1}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    .line 885
    invoke-interface {p2, v3}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p2

    .line 886
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p1

    invoke-direct {v1, v2, v3, p1, p2}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    .line 888
    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->build()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object p1

    .line 890
    iget-object p2, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {p2, p1}, Lcom/google/android/gms/maps/GoogleMap;->setLatLngBoundsForCameraTarget(Lcom/google/android/gms/maps/model/LatLngBounds;)V

    return-void
.end method

.method public setMoveOnMarkerPress(Z)V
    .locals 0

    .line 523
    iput-boolean p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->moveOnMarkerPress:Z

    return-void
.end method

.method public setRegion(Lcom/facebook/react/bridge/ReadableMap;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const-string v2, "longitude"

    .line 441
    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const-string v3, "latitude"

    .line 442
    invoke-interface {v1, v3}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    const-string v4, "longitudeDelta"

    .line 443
    invoke-interface {v1, v4}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    const-string v5, "latitudeDelta"

    .line 444
    invoke-interface {v1, v5}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 445
    new-instance v5, Lcom/google/android/gms/maps/model/LatLngBounds;

    new-instance v6, Lcom/google/android/gms/maps/model/LatLng;

    .line 446
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    const-wide/high16 v11, 0x4000000000000000L    # 2.0

    div-double/2addr v9, v11

    sub-double/2addr v7, v9

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v13

    div-double/2addr v13, v11

    sub-double/2addr v9, v13

    invoke-direct {v6, v7, v8, v9, v10}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    new-instance v7, Lcom/google/android/gms/maps/model/LatLng;

    .line 447
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v13

    div-double/2addr v13, v11

    add-double/2addr v8, v13

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v13

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v15

    div-double/2addr v15, v11

    add-double/2addr v13, v15

    invoke-direct {v7, v8, v9, v13, v14}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLngBounds;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    .line 449
    invoke-super/range {p0 .. p0}, Lcom/google/android/gms/maps/MapView;->getHeight()I

    move-result v1

    if-lez v1, :cond_2

    invoke-super/range {p0 .. p0}, Lcom/google/android/gms/maps/MapView;->getWidth()I

    move-result v1

    if-gtz v1, :cond_1

    goto :goto_0

    .line 457
    :cond_1
    iget-object v1, v0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    const/4 v2, 0x0

    invoke-static {v5, v2}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngBounds(Lcom/google/android/gms/maps/model/LatLngBounds;I)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    const/4 v1, 0x0

    .line 458
    iput-object v1, v0, Lcom/airbnb/android/react/maps/AirMapView;->boundsToMove:Lcom/google/android/gms/maps/model/LatLngBounds;

    goto :goto_1

    .line 454
    :cond_2
    :goto_0
    iget-object v1, v0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-direct {v4, v6, v7, v2, v3}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    const/high16 v2, 0x41200000    # 10.0f

    invoke-static {v4, v2}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngZoom(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 455
    iput-object v5, v0, Lcom/airbnb/android/react/maps/AirMapView;->boundsToMove:Lcom/google/android/gms/maps/model/LatLngBounds;

    :goto_1
    return-void
.end method

.method public setShowsMyLocationButton(Z)V
    .locals 1

    .line 500
    invoke-direct {p0}, Lcom/airbnb/android/react/maps/AirMapView;->hasPermissions()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 501
    :cond_0
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/UiSettings;->setMyLocationButtonEnabled(Z)V

    :cond_1
    return-void
.end method

.method public setShowsUserLocation(Z)V
    .locals 1

    .line 492
    iput-boolean p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->showUserLocation:Z

    .line 493
    invoke-direct {p0}, Lcom/airbnb/android/react/maps/AirMapView;->hasPermissions()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/GoogleMap;->setMyLocationEnabled(Z)V

    :cond_0
    return-void
.end method

.method public setToolbarEnabled(Z)V
    .locals 1

    .line 506
    invoke-direct {p0}, Lcom/airbnb/android/react/maps/AirMapView;->hasPermissions()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/UiSettings;->setMapToolbarEnabled(Z)V

    :cond_1
    return-void
.end method

.method public updateExtraData(Ljava/lang/Object;)V
    .locals 5

    .line 680
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->boundsToMove:Lcom/google/android/gms/maps/model/LatLngBounds;

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    .line 681
    check-cast p1, Ljava/util/HashMap;

    const-string/jumbo v0, "width"

    .line 682
    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v0

    :goto_0
    const-string v2, "height"

    .line 683
    invoke-virtual {p1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_1

    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->intValue()I

    move-result p1

    :goto_1
    if-lez v0, :cond_3

    if-gtz p1, :cond_2

    goto :goto_2

    .line 690
    :cond_2
    iget-object v2, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    iget-object v4, p0, Lcom/airbnb/android/react/maps/AirMapView;->boundsToMove:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-static {v4, v0, p1, v3}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngBounds(Lcom/google/android/gms/maps/model/LatLngBounds;III)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    goto :goto_3

    .line 688
    :cond_3
    :goto_2
    iget-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->boundsToMove:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-static {v0, v3}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngBounds(Lcom/google/android/gms/maps/model/LatLngBounds;I)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 693
    :goto_3
    iput-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->boundsToMove:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 694
    iput-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->cameraToSet:Lcom/google/android/gms/maps/CameraUpdate;

    goto :goto_4

    .line 696
    :cond_4
    iget-object p1, p0, Lcom/airbnb/android/react/maps/AirMapView;->cameraToSet:Lcom/google/android/gms/maps/CameraUpdate;

    if-eqz p1, :cond_5

    .line 697
    iget-object v0, p0, Lcom/airbnb/android/react/maps/AirMapView;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 698
    iput-object v1, p0, Lcom/airbnb/android/react/maps/AirMapView;->cameraToSet:Lcom/google/android/gms/maps/CameraUpdate;

    :cond_5
    :goto_4
    return-void
.end method
