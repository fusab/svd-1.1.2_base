.class Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule$DispatchingProgressListener;
.super Ljava/lang/Object;
.source "FastImageOkHttpProgressGlideModule.java"

# interfaces
.implements Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule$ResponseProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DispatchingProgressListener"
.end annotation


# instance fields
.field private final LISTENERS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/dylanvann/fastimage/FastImageProgressListener;",
            ">;"
        }
    .end annotation
.end field

.field private final PROGRESSES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final handler:Landroid/os/Handler;


# direct methods
.method constructor <init>()V
    .locals 2

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule$DispatchingProgressListener;->LISTENERS:Ljava/util/Map;

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule$DispatchingProgressListener;->PROGRESSES:Ljava/util/Map;

    .line 88
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule$DispatchingProgressListener;->handler:Landroid/os/Handler;

    return-void
.end method

.method private needsDispatch(Ljava/lang/String;JJF)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    cmpl-float v1, p6, v1

    if-eqz v1, :cond_3

    const-wide/16 v1, 0x0

    cmp-long v3, p2, v1

    if-eqz v3, :cond_3

    cmp-long v1, p4, p2

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    const/high16 v1, 0x42c80000    # 100.0f

    long-to-float p2, p2

    mul-float p2, p2, v1

    long-to-float p3, p4

    div-float/2addr p2, p3

    div-float/2addr p2, p6

    float-to-long p2, p2

    .line 125
    iget-object p4, p0, Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule$DispatchingProgressListener;->PROGRESSES:Ljava/util/Map;

    invoke-interface {p4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/lang/Long;

    if-eqz p4, :cond_2

    .line 126
    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide p4

    cmp-long p6, p2, p4

    if-eqz p6, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return p1

    .line 127
    :cond_2
    :goto_0
    iget-object p4, p0, Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule$DispatchingProgressListener;->PROGRESSES:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {p4, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    :goto_1
    return v0
.end method


# virtual methods
.method expect(Ljava/lang/String;Lcom/dylanvann/fastimage/FastImageProgressListener;)V
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule$DispatchingProgressListener;->LISTENERS:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method forget(Ljava/lang/String;)V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule$DispatchingProgressListener;->LISTENERS:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    iget-object v0, p0, Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule$DispatchingProgressListener;->PROGRESSES:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public update(Ljava/lang/String;JJ)V
    .locals 12

    move-object v8, p0

    .line 102
    iget-object v0, v8, Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule$DispatchingProgressListener;->LISTENERS:Ljava/util/Map;

    move-object v7, p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/dylanvann/fastimage/FastImageProgressListener;

    if-nez v9, :cond_0

    return-void

    :cond_0
    cmp-long v0, p4, p2

    if-gtz v0, :cond_1

    .line 107
    invoke-virtual {p0, p1}, Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule$DispatchingProgressListener;->forget(Ljava/lang/String;)V

    .line 109
    :cond_1
    invoke-interface {v9}, Lcom/dylanvann/fastimage/FastImageProgressListener;->getGranularityPercentage()F

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide/from16 v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule$DispatchingProgressListener;->needsDispatch(Ljava/lang/String;JJF)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 110
    iget-object v10, v8, Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule$DispatchingProgressListener;->handler:Landroid/os/Handler;

    new-instance v11, Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule$DispatchingProgressListener$1;

    move-object v0, v11

    move-object v1, p0

    move-object v2, v9

    move-object v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule$DispatchingProgressListener$1;-><init>(Lcom/dylanvann/fastimage/FastImageOkHttpProgressGlideModule$DispatchingProgressListener;Lcom/dylanvann/fastimage/FastImageProgressListener;Ljava/lang/String;JJ)V

    invoke-virtual {v10, v11}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_2
    return-void
.end method
