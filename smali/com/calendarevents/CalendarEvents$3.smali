.class Lcom/calendarevents/CalendarEvents$3;
.super Ljava/lang/Object;
.source "CalendarEvents.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/calendarevents/CalendarEvents;->saveEvent(Ljava/lang/String;Lcom/facebook/react/bridge/ReadableMap;Lcom/facebook/react/bridge/ReadableMap;Lcom/facebook/react/bridge/Promise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/calendarevents/CalendarEvents;

.field final synthetic val$details:Lcom/facebook/react/bridge/ReadableMap;

.field final synthetic val$options:Lcom/facebook/react/bridge/ReadableMap;

.field final synthetic val$promise:Lcom/facebook/react/bridge/Promise;

.field final synthetic val$title:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/calendarevents/CalendarEvents;Ljava/lang/String;Lcom/facebook/react/bridge/ReadableMap;Lcom/facebook/react/bridge/ReadableMap;Lcom/facebook/react/bridge/Promise;)V
    .locals 0

    .line 1220
    iput-object p1, p0, Lcom/calendarevents/CalendarEvents$3;->this$0:Lcom/calendarevents/CalendarEvents;

    iput-object p2, p0, Lcom/calendarevents/CalendarEvents$3;->val$title:Ljava/lang/String;

    iput-object p3, p0, Lcom/calendarevents/CalendarEvents$3;->val$details:Lcom/facebook/react/bridge/ReadableMap;

    iput-object p4, p0, Lcom/calendarevents/CalendarEvents$3;->val$options:Lcom/facebook/react/bridge/ReadableMap;

    iput-object p5, p0, Lcom/calendarevents/CalendarEvents$3;->val$promise:Lcom/facebook/react/bridge/Promise;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const-string v0, "add event error"

    .line 1225
    :try_start_0
    iget-object v1, p0, Lcom/calendarevents/CalendarEvents$3;->this$0:Lcom/calendarevents/CalendarEvents;

    iget-object v2, p0, Lcom/calendarevents/CalendarEvents$3;->val$title:Ljava/lang/String;

    iget-object v3, p0, Lcom/calendarevents/CalendarEvents$3;->val$details:Lcom/facebook/react/bridge/ReadableMap;

    iget-object v4, p0, Lcom/calendarevents/CalendarEvents$3;->val$options:Lcom/facebook/react/bridge/ReadableMap;

    invoke-static {v1, v2, v3, v4}, Lcom/calendarevents/CalendarEvents;->access$200(Lcom/calendarevents/CalendarEvents;Ljava/lang/String;Lcom/facebook/react/bridge/ReadableMap;Lcom/facebook/react/bridge/ReadableMap;)I

    move-result v1

    const/4 v2, -0x1

    if-le v1, v2, :cond_0

    .line 1227
    iget-object v2, p0, Lcom/calendarevents/CalendarEvents$3;->val$promise:Lcom/facebook/react/bridge/Promise;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    goto :goto_0

    .line 1229
    :cond_0
    iget-object v1, p0, Lcom/calendarevents/CalendarEvents$3;->val$promise:Lcom/facebook/react/bridge/Promise;

    const-string v2, "Unable to save event"

    invoke-interface {v1, v0, v2}, Lcom/facebook/react/bridge/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 1232
    iget-object v2, p0, Lcom/calendarevents/CalendarEvents$3;->val$promise:Lcom/facebook/react/bridge/Promise;

    invoke-virtual {v1}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Lcom/facebook/react/bridge/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method
