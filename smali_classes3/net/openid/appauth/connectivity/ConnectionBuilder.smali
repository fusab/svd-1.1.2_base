.class public interface abstract Lnet/openid/appauth/connectivity/ConnectionBuilder;
.super Ljava/lang/Object;
.source "ConnectionBuilder.java"


# virtual methods
.method public abstract openConnection(Landroid/net/Uri;)Ljava/net/HttpURLConnection;
    .param p1    # Landroid/net/Uri;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
