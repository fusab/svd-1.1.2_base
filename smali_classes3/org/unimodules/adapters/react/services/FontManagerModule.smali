.class public Lorg/unimodules/adapters/react/services/FontManagerModule;
.super Ljava/lang/Object;
.source "FontManagerModule.java"

# interfaces
.implements Lorg/unimodules/interfaces/font/FontManager;
.implements Lorg/unimodules/core/interfaces/InternalModule;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getExportedInterfaces()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation

    .line 16
    const-class v0, Lorg/unimodules/interfaces/font/FontManager;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic onCreate(Lorg/unimodules/core/ModuleRegistry;)V
    .locals 0

    invoke-static {p0, p1}, Lorg/unimodules/core/interfaces/RegistryLifecycleListener$-CC;->$default$onCreate(Lorg/unimodules/core/interfaces/RegistryLifecycleListener;Lorg/unimodules/core/ModuleRegistry;)V

    return-void
.end method

.method public synthetic onDestroy()V
    .locals 0

    invoke-static {p0}, Lorg/unimodules/core/interfaces/RegistryLifecycleListener$-CC;->$default$onDestroy(Lorg/unimodules/core/interfaces/RegistryLifecycleListener;)V

    return-void
.end method

.method public setTypeface(Ljava/lang/String;ILandroid/graphics/Typeface;)V
    .locals 1

    .line 21
    invoke-static {}, Lcom/facebook/react/views/text/ReactFontManager;->getInstance()Lcom/facebook/react/views/text/ReactFontManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/react/views/text/ReactFontManager;->setTypeface(Ljava/lang/String;ILandroid/graphics/Typeface;)V

    return-void
.end method
