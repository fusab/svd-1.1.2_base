.class Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$5;
.super Ljava/lang/Object;
.source "UIManagerModuleWrapper.java"

# interfaces
.implements Lcom/facebook/react/modules/core/PermissionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;->requestPermissions([Ljava/lang/String;ILorg/unimodules/interfaces/permissions/PermissionsListener;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;

.field final synthetic val$listener:Lorg/unimodules/interfaces/permissions/PermissionsListener;

.field final synthetic val$requestCode:I


# direct methods
.method constructor <init>(Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;ILorg/unimodules/interfaces/permissions/PermissionsListener;)V
    .locals 0

    .line 219
    iput-object p1, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$5;->this$0:Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;

    iput p2, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$5;->val$requestCode:I

    iput-object p3, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$5;->val$listener:Lorg/unimodules/interfaces/permissions/PermissionsListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)Z
    .locals 1

    .line 222
    iget v0, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$5;->val$requestCode:I

    if-ne v0, p1, :cond_0

    .line 223
    iget-object p1, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$5;->val$listener:Lorg/unimodules/interfaces/permissions/PermissionsListener;

    invoke-interface {p1, p2, p3}, Lorg/unimodules/interfaces/permissions/PermissionsListener;->onPermissionResult([Ljava/lang/String;[I)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
