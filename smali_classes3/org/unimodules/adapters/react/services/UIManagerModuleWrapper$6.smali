.class Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$6;
.super Lcom/facebook/imagepipeline/datasource/BaseBitmapDataSubscriber;
.source "UIManagerModuleWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;->loadImageForDisplayFromURL(Ljava/lang/String;Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;

.field final synthetic val$resultListener:Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;


# direct methods
.method constructor <init>(Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;)V
    .locals 0

    .line 249
    iput-object p1, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$6;->this$0:Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;

    iput-object p2, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$6;->val$resultListener:Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;

    invoke-direct {p0}, Lcom/facebook/imagepipeline/datasource/BaseBitmapDataSubscriber;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailureImpl(Lcom/facebook/datasource/DataSource;)V
    .locals 1

    .line 261
    iget-object v0, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$6;->val$resultListener:Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;

    invoke-interface {p1}, Lcom/facebook/datasource/DataSource;->getFailureCause()Ljava/lang/Throwable;

    move-result-object p1

    invoke-interface {v0, p1}, Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;->onFailure(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onNewResultImpl(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    if-nez p1, :cond_0

    .line 253
    iget-object p1, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$6;->val$resultListener:Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;

    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Loaded bitmap is null"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;->onFailure(Ljava/lang/Throwable;)V

    return-void

    .line 256
    :cond_0
    iget-object v0, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$6;->val$resultListener:Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;

    invoke-interface {v0, p1}, Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;->onSuccess(Landroid/graphics/Bitmap;)V

    return-void
.end method
