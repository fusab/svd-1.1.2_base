.class Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$4;
.super Ljava/lang/Object;
.source "UIManagerModuleWrapper.java"

# interfaces
.implements Lcom/facebook/react/bridge/ActivityEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;->registerActivityEventListener(Lorg/unimodules/core/interfaces/ActivityEventListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;

.field final synthetic val$weakListener:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;Ljava/lang/ref/WeakReference;)V
    .locals 0

    .line 187
    iput-object p1, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$4;->this$0:Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;

    iput-object p2, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$4;->val$weakListener:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)V
    .locals 1

    .line 190
    iget-object v0, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$4;->val$weakListener:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/ActivityEventListener;

    if-eqz v0, :cond_0

    .line 192
    invoke-interface {v0, p1, p2, p3, p4}, Lorg/unimodules/core/interfaces/ActivityEventListener;->onActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .line 198
    iget-object v0, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$4;->val$weakListener:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/ActivityEventListener;

    if-eqz v0, :cond_0

    .line 200
    invoke-interface {v0, p1}, Lorg/unimodules/core/interfaces/ActivityEventListener;->onNewIntent(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
