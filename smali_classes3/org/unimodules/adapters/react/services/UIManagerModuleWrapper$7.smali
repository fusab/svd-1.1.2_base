.class Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$7;
.super Lcom/bumptech/glide/request/target/SimpleTarget;
.source "UIManagerModuleWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;->loadImageForManipulationFromURL(Ljava/lang/String;Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/bumptech/glide/request/target/SimpleTarget<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;

.field final synthetic val$resultListener:Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;


# direct methods
.method constructor <init>(Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;)V
    .locals 0

    .line 274
    iput-object p1, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$7;->this$0:Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;

    iput-object p2, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$7;->val$resultListener:Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;

    invoke-direct {p0}, Lcom/bumptech/glide/request/target/SimpleTarget;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadFailed(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .line 282
    iget-object p1, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$7;->val$resultListener:Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;

    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Loading bitmap failed"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;->onFailure(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onResourceReady(Landroid/graphics/Bitmap;Lcom/bumptech/glide/request/transition/Transition;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bumptech/glide/request/transition/Transition;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Lcom/bumptech/glide/request/transition/Transition<",
            "-",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .line 277
    iget-object p2, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$7;->val$resultListener:Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;

    invoke-interface {p2, p1}, Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;->onSuccess(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public bridge synthetic onResourceReady(Ljava/lang/Object;Lcom/bumptech/glide/request/transition/Transition;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bumptech/glide/request/transition/Transition;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .line 274
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$7;->onResourceReady(Landroid/graphics/Bitmap;Lcom/bumptech/glide/request/transition/Transition;)V

    return-void
.end method
