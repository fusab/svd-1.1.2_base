.class Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$3;
.super Ljava/lang/Object;
.source "UIManagerModuleWrapper.java"

# interfaces
.implements Lcom/facebook/react/bridge/LifecycleEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;->registerLifecycleEventListener(Lorg/unimodules/core/interfaces/LifecycleEventListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;

.field final synthetic val$weakListener:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;Ljava/lang/ref/WeakReference;)V
    .locals 0

    .line 149
    iput-object p1, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$3;->this$0:Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;

    iput-object p2, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$3;->val$weakListener:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHostDestroy()V
    .locals 1

    .line 168
    iget-object v0, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$3;->val$weakListener:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/LifecycleEventListener;

    if-eqz v0, :cond_0

    .line 170
    invoke-interface {v0}, Lorg/unimodules/core/interfaces/LifecycleEventListener;->onHostDestroy()V

    :cond_0
    return-void
.end method

.method public onHostPause()V
    .locals 1

    .line 160
    iget-object v0, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$3;->val$weakListener:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/LifecycleEventListener;

    if-eqz v0, :cond_0

    .line 162
    invoke-interface {v0}, Lorg/unimodules/core/interfaces/LifecycleEventListener;->onHostPause()V

    :cond_0
    return-void
.end method

.method public onHostResume()V
    .locals 1

    .line 152
    iget-object v0, p0, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper$3;->val$weakListener:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/LifecycleEventListener;

    if-eqz v0, :cond_0

    .line 154
    invoke-interface {v0}, Lorg/unimodules/core/interfaces/LifecycleEventListener;->onHostResume()V

    :cond_0
    return-void
.end method
