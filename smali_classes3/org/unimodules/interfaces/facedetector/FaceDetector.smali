.class public interface abstract Lorg/unimodules/interfaces/facedetector/FaceDetector;
.super Ljava/lang/Object;
.source "FaceDetector.java"


# virtual methods
.method public abstract detectFaces(Landroid/net/Uri;Lorg/unimodules/interfaces/facedetector/FacesDetectionCompleted;Lorg/unimodules/interfaces/facedetector/FaceDetectionError;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract detectFaces([BIIIZDDLorg/unimodules/interfaces/facedetector/FacesDetectionCompleted;Lorg/unimodules/interfaces/facedetector/FaceDetectionError;Lorg/unimodules/interfaces/facedetector/FaceDetectionSkipped;)V
.end method

.method public abstract release()V
.end method

.method public abstract setSettings(Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation
.end method
