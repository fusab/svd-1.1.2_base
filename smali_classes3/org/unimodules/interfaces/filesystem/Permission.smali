.class public final enum Lorg/unimodules/interfaces/filesystem/Permission;
.super Ljava/lang/Enum;
.source "Permission.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/unimodules/interfaces/filesystem/Permission;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/unimodules/interfaces/filesystem/Permission;

.field public static final enum READ:Lorg/unimodules/interfaces/filesystem/Permission;

.field public static final enum WRITE:Lorg/unimodules/interfaces/filesystem/Permission;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 4
    new-instance v0, Lorg/unimodules/interfaces/filesystem/Permission;

    const/4 v1, 0x0

    const-string v2, "READ"

    invoke-direct {v0, v2, v1}, Lorg/unimodules/interfaces/filesystem/Permission;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/unimodules/interfaces/filesystem/Permission;->READ:Lorg/unimodules/interfaces/filesystem/Permission;

    new-instance v0, Lorg/unimodules/interfaces/filesystem/Permission;

    const/4 v2, 0x1

    const-string v3, "WRITE"

    invoke-direct {v0, v3, v2}, Lorg/unimodules/interfaces/filesystem/Permission;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/unimodules/interfaces/filesystem/Permission;->WRITE:Lorg/unimodules/interfaces/filesystem/Permission;

    const/4 v0, 0x2

    .line 3
    new-array v0, v0, [Lorg/unimodules/interfaces/filesystem/Permission;

    sget-object v3, Lorg/unimodules/interfaces/filesystem/Permission;->READ:Lorg/unimodules/interfaces/filesystem/Permission;

    aput-object v3, v0, v1

    sget-object v1, Lorg/unimodules/interfaces/filesystem/Permission;->WRITE:Lorg/unimodules/interfaces/filesystem/Permission;

    aput-object v1, v0, v2

    sput-object v0, Lorg/unimodules/interfaces/filesystem/Permission;->$VALUES:[Lorg/unimodules/interfaces/filesystem/Permission;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/unimodules/interfaces/filesystem/Permission;
    .locals 1

    .line 3
    const-class v0, Lorg/unimodules/interfaces/filesystem/Permission;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/unimodules/interfaces/filesystem/Permission;

    return-object p0
.end method

.method public static values()[Lorg/unimodules/interfaces/filesystem/Permission;
    .locals 1

    .line 3
    sget-object v0, Lorg/unimodules/interfaces/filesystem/Permission;->$VALUES:[Lorg/unimodules/interfaces/filesystem/Permission;

    invoke-virtual {v0}, [Lorg/unimodules/interfaces/filesystem/Permission;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/unimodules/interfaces/filesystem/Permission;

    return-object v0
.end method
