.class public interface abstract Lorg/unimodules/interfaces/sensors/SensorServiceSubscription;
.super Ljava/lang/Object;
.source "SensorServiceSubscription.java"


# virtual methods
.method public abstract getUpdateInterval()Ljava/lang/Long;
.end method

.method public abstract isEnabled()Z
.end method

.method public abstract release()V
.end method

.method public abstract setUpdateInterval(J)V
.end method

.method public abstract start()V
.end method

.method public abstract stop()V
.end method
