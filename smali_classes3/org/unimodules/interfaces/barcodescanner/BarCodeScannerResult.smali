.class public Lorg/unimodules/interfaces/barcodescanner/BarCodeScannerResult;
.super Ljava/lang/Object;
.source "BarCodeScannerResult.java"


# instance fields
.field private mCornerPoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mReferenceImageHeight:I

.field private mReferenceImageWidth:I

.field private mType:I

.field private mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/util/List;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;II)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, Lorg/unimodules/interfaces/barcodescanner/BarCodeScannerResult;->mType:I

    .line 17
    iput-object p2, p0, Lorg/unimodules/interfaces/barcodescanner/BarCodeScannerResult;->mValue:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lorg/unimodules/interfaces/barcodescanner/BarCodeScannerResult;->mCornerPoints:Ljava/util/List;

    .line 19
    iput p4, p0, Lorg/unimodules/interfaces/barcodescanner/BarCodeScannerResult;->mReferenceImageHeight:I

    .line 20
    iput p5, p0, Lorg/unimodules/interfaces/barcodescanner/BarCodeScannerResult;->mReferenceImageWidth:I

    return-void
.end method


# virtual methods
.method public getCornerPoints()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lorg/unimodules/interfaces/barcodescanner/BarCodeScannerResult;->mCornerPoints:Ljava/util/List;

    return-object v0
.end method

.method public getReferenceImageHeight()I
    .locals 1

    .line 38
    iget v0, p0, Lorg/unimodules/interfaces/barcodescanner/BarCodeScannerResult;->mReferenceImageHeight:I

    return v0
.end method

.method public getReferenceImageWidth()I
    .locals 1

    .line 46
    iget v0, p0, Lorg/unimodules/interfaces/barcodescanner/BarCodeScannerResult;->mReferenceImageWidth:I

    return v0
.end method

.method public getType()I
    .locals 1

    .line 24
    iget v0, p0, Lorg/unimodules/interfaces/barcodescanner/BarCodeScannerResult;->mType:I

    return v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lorg/unimodules/interfaces/barcodescanner/BarCodeScannerResult;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public setCornerPoints(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 34
    iput-object p1, p0, Lorg/unimodules/interfaces/barcodescanner/BarCodeScannerResult;->mCornerPoints:Ljava/util/List;

    return-void
.end method

.method public setReferenceImageHeight(I)V
    .locals 0

    .line 42
    iput p1, p0, Lorg/unimodules/interfaces/barcodescanner/BarCodeScannerResult;->mReferenceImageHeight:I

    return-void
.end method

.method public setReferenceImageWidth(I)V
    .locals 0

    .line 50
    iput p1, p0, Lorg/unimodules/interfaces/barcodescanner/BarCodeScannerResult;->mReferenceImageWidth:I

    return-void
.end method
