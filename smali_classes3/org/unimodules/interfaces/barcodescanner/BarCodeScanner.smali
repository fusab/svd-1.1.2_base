.class public interface abstract Lorg/unimodules/interfaces/barcodescanner/BarCodeScanner;
.super Ljava/lang/Object;
.source "BarCodeScanner.java"


# virtual methods
.method public abstract scan([BIII)Lorg/unimodules/interfaces/barcodescanner/BarCodeScannerResult;
.end method

.method public abstract scanMultiple(Landroid/graphics/Bitmap;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            ")",
            "Ljava/util/List<",
            "Lorg/unimodules/interfaces/barcodescanner/BarCodeScannerResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setSettings(Lorg/unimodules/interfaces/barcodescanner/BarCodeScannerSettings;)V
.end method
