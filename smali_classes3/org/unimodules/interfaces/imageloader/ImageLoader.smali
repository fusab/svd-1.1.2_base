.class public interface abstract Lorg/unimodules/interfaces/imageloader/ImageLoader;
.super Ljava/lang/Object;
.source "ImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;
    }
.end annotation


# virtual methods
.method public abstract loadImageForDisplayFromURL(Ljava/lang/String;Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;)V
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract loadImageForManipulationFromURL(Ljava/lang/String;Lorg/unimodules/interfaces/imageloader/ImageLoader$ResultListener;)V
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method
