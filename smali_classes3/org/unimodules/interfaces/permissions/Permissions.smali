.class public interface abstract Lorg/unimodules/interfaces/permissions/Permissions;
.super Ljava/lang/Object;
.source "Permissions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/unimodules/interfaces/permissions/Permissions$PermissionsRequestListener;,
        Lorg/unimodules/interfaces/permissions/Permissions$PermissionRequestListener;
    }
.end annotation


# virtual methods
.method public abstract askForPermission(Ljava/lang/String;Lorg/unimodules/interfaces/permissions/Permissions$PermissionRequestListener;)V
.end method

.method public abstract askForPermissions([Ljava/lang/String;Lorg/unimodules/interfaces/permissions/Permissions$PermissionsRequestListener;)V
.end method

.method public abstract getPermission(Ljava/lang/String;)I
.end method

.method public abstract getPermissions([Ljava/lang/String;)[I
.end method

.method public abstract hasPermissions([Ljava/lang/String;)Z
.end method
