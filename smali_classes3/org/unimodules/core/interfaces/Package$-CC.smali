.class public final synthetic Lorg/unimodules/core/interfaces/Package$-CC;
.super Ljava/lang/Object;
.source "Package.java"


# direct methods
.method public static $default$createExportedModules(Lorg/unimodules/core/interfaces/Package;Landroid/content/Context;)Ljava/util/List;
    .locals 0
    .param p0, "_this"    # Lorg/unimodules/core/interfaces/Package;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "+",
            "Lorg/unimodules/core/ExportedModule;",
            ">;"
        }
    .end annotation

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public static $default$createInternalModules(Lorg/unimodules/core/interfaces/Package;Landroid/content/Context;)Ljava/util/List;
    .locals 0
    .param p0, "_this"    # Lorg/unimodules/core/interfaces/Package;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "+",
            "Lorg/unimodules/core/interfaces/InternalModule;",
            ">;"
        }
    .end annotation

    .line 14
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public static $default$createSingletonModules(Lorg/unimodules/core/interfaces/Package;Landroid/content/Context;)Ljava/util/List;
    .locals 0
    .param p0, "_this"    # Lorg/unimodules/core/interfaces/Package;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "+",
            "Lorg/unimodules/core/interfaces/SingletonModule;",
            ">;"
        }
    .end annotation

    .line 32
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public static $default$createViewManagers(Lorg/unimodules/core/interfaces/Package;Landroid/content/Context;)Ljava/util/List;
    .locals 0
    .param p0, "_this"    # Lorg/unimodules/core/interfaces/Package;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "+",
            "Lorg/unimodules/core/ViewManager;",
            ">;"
        }
    .end annotation

    .line 28
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
