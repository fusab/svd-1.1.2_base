.class public interface abstract Lorg/unimodules/core/interfaces/Package;
.super Ljava/lang/Object;
.source "Package.java"


# virtual methods
.method public abstract createExportedModules(Landroid/content/Context;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "+",
            "Lorg/unimodules/core/ExportedModule;",
            ">;"
        }
    .end annotation
.end method

.method public abstract createInternalModules(Landroid/content/Context;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "+",
            "Lorg/unimodules/core/interfaces/InternalModule;",
            ">;"
        }
    .end annotation
.end method

.method public abstract createSingletonModules(Landroid/content/Context;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "+",
            "Lorg/unimodules/core/interfaces/SingletonModule;",
            ">;"
        }
    .end annotation
.end method

.method public abstract createViewManagers(Landroid/content/Context;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "+",
            "Lorg/unimodules/core/ViewManager;",
            ">;"
        }
    .end annotation
.end method
