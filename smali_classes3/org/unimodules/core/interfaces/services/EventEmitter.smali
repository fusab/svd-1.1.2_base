.class public interface abstract Lorg/unimodules/core/interfaces/services/EventEmitter;
.super Ljava/lang/Object;
.source "EventEmitter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/unimodules/core/interfaces/services/EventEmitter$BaseEvent;,
        Lorg/unimodules/core/interfaces/services/EventEmitter$Event;
    }
.end annotation


# virtual methods
.method public abstract emit(ILjava/lang/String;Landroid/os/Bundle;)V
.end method

.method public abstract emit(ILorg/unimodules/core/interfaces/services/EventEmitter$Event;)V
.end method

.method public abstract emit(Ljava/lang/String;Landroid/os/Bundle;)V
.end method
