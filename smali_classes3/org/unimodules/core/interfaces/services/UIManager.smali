.class public interface abstract Lorg/unimodules/core/interfaces/services/UIManager;
.super Ljava/lang/Object;
.source "UIManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/unimodules/core/interfaces/services/UIManager$GroupUIBlock;,
        Lorg/unimodules/core/interfaces/services/UIManager$ViewHolder;,
        Lorg/unimodules/core/interfaces/services/UIManager$UIBlock;
    }
.end annotation


# virtual methods
.method public abstract addUIBlock(ILorg/unimodules/core/interfaces/services/UIManager$UIBlock;Ljava/lang/Class;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Lorg/unimodules/core/interfaces/services/UIManager$UIBlock<",
            "TT;>;",
            "Ljava/lang/Class<",
            "TT;>;)V"
        }
    .end annotation
.end method

.method public abstract addUIBlock(Lorg/unimodules/core/interfaces/services/UIManager$GroupUIBlock;)V
.end method

.method public abstract registerActivityEventListener(Lorg/unimodules/core/interfaces/ActivityEventListener;)V
.end method

.method public abstract registerLifecycleEventListener(Lorg/unimodules/core/interfaces/LifecycleEventListener;)V
.end method

.method public abstract runOnClientCodeQueueThread(Ljava/lang/Runnable;)V
.end method

.method public abstract runOnUiQueueThread(Ljava/lang/Runnable;)V
.end method

.method public abstract unregisterActivityEventListener(Lorg/unimodules/core/interfaces/ActivityEventListener;)V
.end method

.method public abstract unregisterLifecycleEventListener(Lorg/unimodules/core/interfaces/LifecycleEventListener;)V
.end method
