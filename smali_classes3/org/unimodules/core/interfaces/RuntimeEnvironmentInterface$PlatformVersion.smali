.class public interface abstract Lorg/unimodules/core/interfaces/RuntimeEnvironmentInterface$PlatformVersion;
.super Ljava/lang/Object;
.source "RuntimeEnvironmentInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/unimodules/core/interfaces/RuntimeEnvironmentInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PlatformVersion"
.end annotation


# virtual methods
.method public abstract major()I
.end method

.method public abstract minor()I
.end method

.method public abstract patch()I
.end method

.method public abstract prerelease()Ljava/lang/String;
.end method
