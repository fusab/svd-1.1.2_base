.class public final enum Lorg/unimodules/core/ViewManager$ViewManagerType;
.super Ljava/lang/Enum;
.source "ViewManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/unimodules/core/ViewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ViewManagerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/unimodules/core/ViewManager$ViewManagerType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/unimodules/core/ViewManager$ViewManagerType;

.field public static final enum GROUP:Lorg/unimodules/core/ViewManager$ViewManagerType;

.field public static final enum SIMPLE:Lorg/unimodules/core/ViewManager$ViewManagerType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 33
    new-instance v0, Lorg/unimodules/core/ViewManager$ViewManagerType;

    const/4 v1, 0x0

    const-string v2, "SIMPLE"

    invoke-direct {v0, v2, v1}, Lorg/unimodules/core/ViewManager$ViewManagerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/unimodules/core/ViewManager$ViewManagerType;->SIMPLE:Lorg/unimodules/core/ViewManager$ViewManagerType;

    .line 34
    new-instance v0, Lorg/unimodules/core/ViewManager$ViewManagerType;

    const/4 v2, 0x1

    const-string v3, "GROUP"

    invoke-direct {v0, v3, v2}, Lorg/unimodules/core/ViewManager$ViewManagerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/unimodules/core/ViewManager$ViewManagerType;->GROUP:Lorg/unimodules/core/ViewManager$ViewManagerType;

    const/4 v0, 0x2

    .line 32
    new-array v0, v0, [Lorg/unimodules/core/ViewManager$ViewManagerType;

    sget-object v3, Lorg/unimodules/core/ViewManager$ViewManagerType;->SIMPLE:Lorg/unimodules/core/ViewManager$ViewManagerType;

    aput-object v3, v0, v1

    sget-object v1, Lorg/unimodules/core/ViewManager$ViewManagerType;->GROUP:Lorg/unimodules/core/ViewManager$ViewManagerType;

    aput-object v1, v0, v2

    sput-object v0, Lorg/unimodules/core/ViewManager$ViewManagerType;->$VALUES:[Lorg/unimodules/core/ViewManager$ViewManagerType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/unimodules/core/ViewManager$ViewManagerType;
    .locals 1

    .line 32
    const-class v0, Lorg/unimodules/core/ViewManager$ViewManagerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/unimodules/core/ViewManager$ViewManagerType;

    return-object p0
.end method

.method public static values()[Lorg/unimodules/core/ViewManager$ViewManagerType;
    .locals 1

    .line 32
    sget-object v0, Lorg/unimodules/core/ViewManager$ViewManagerType;->$VALUES:[Lorg/unimodules/core/ViewManager$ViewManagerType;

    invoke-virtual {v0}, [Lorg/unimodules/core/ViewManager$ViewManagerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/unimodules/core/ViewManager$ViewManagerType;

    return-object v0
.end method
