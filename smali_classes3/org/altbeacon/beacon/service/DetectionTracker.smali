.class public Lorg/altbeacon/beacon/service/DetectionTracker;
.super Ljava/lang/Object;
.source "DetectionTracker.java"


# static fields
.field private static sDetectionTracker:Lorg/altbeacon/beacon/service/DetectionTracker;


# instance fields
.field private mLastDetectionTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 10
    iput-wide v0, p0, Lorg/altbeacon/beacon/service/DetectionTracker;->mLastDetectionTime:J

    return-void
.end method

.method public static declared-synchronized getInstance()Lorg/altbeacon/beacon/service/DetectionTracker;
    .locals 2

    const-class v0, Lorg/altbeacon/beacon/service/DetectionTracker;

    monitor-enter v0

    .line 15
    :try_start_0
    sget-object v1, Lorg/altbeacon/beacon/service/DetectionTracker;->sDetectionTracker:Lorg/altbeacon/beacon/service/DetectionTracker;

    if-nez v1, :cond_0

    .line 16
    new-instance v1, Lorg/altbeacon/beacon/service/DetectionTracker;

    invoke-direct {v1}, Lorg/altbeacon/beacon/service/DetectionTracker;-><init>()V

    sput-object v1, Lorg/altbeacon/beacon/service/DetectionTracker;->sDetectionTracker:Lorg/altbeacon/beacon/service/DetectionTracker;

    .line 18
    :cond_0
    sget-object v1, Lorg/altbeacon/beacon/service/DetectionTracker;->sDetectionTracker:Lorg/altbeacon/beacon/service/DetectionTracker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public getLastDetectionTime()J
    .locals 2

    .line 21
    iget-wide v0, p0, Lorg/altbeacon/beacon/service/DetectionTracker;->mLastDetectionTime:J

    return-wide v0
.end method

.method public recordDetection()V
    .locals 2

    .line 24
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/altbeacon/beacon/service/DetectionTracker;->mLastDetectionTime:J

    return-void
.end method
