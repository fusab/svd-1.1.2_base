.class public Lorg/altbeacon/beacon/service/Callback;
.super Ljava/lang/Object;
.source "Callback.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final TAG:Ljava/lang/String; = "Callback"


# instance fields
.field private transient intent:Landroid/content/Intent;

.field private intentPackageName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lorg/altbeacon/beacon/service/Callback;->intentPackageName:Ljava/lang/String;

    .line 43
    invoke-direct {p0}, Lorg/altbeacon/beacon/service/Callback;->initializeIntent()V

    return-void
.end method

.method private initializeIntent()V
    .locals 4

    .line 47
    iget-object v0, p0, Lorg/altbeacon/beacon/service/Callback;->intentPackageName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 48
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lorg/altbeacon/beacon/service/Callback;->intent:Landroid/content/Intent;

    .line 49
    iget-object v0, p0, Lorg/altbeacon/beacon/service/Callback;->intent:Landroid/content/Intent;

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lorg/altbeacon/beacon/service/Callback;->intentPackageName:Ljava/lang/String;

    const-string v3, "org.altbeacon.beacon.BeaconIntentProcessor"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 81
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 82
    invoke-direct {p0}, Lorg/altbeacon/beacon/service/Callback;->initializeIntent()V

    return-void
.end method


# virtual methods
.method public call(Landroid/content/Context;Ljava/lang/String;Landroid/os/Parcelable;)Z
    .locals 4

    .line 66
    iget-object v0, p0, Lorg/altbeacon/beacon/service/Callback;->intent:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 67
    invoke-direct {p0}, Lorg/altbeacon/beacon/service/Callback;->initializeIntent()V

    .line 69
    :cond_0
    iget-object v0, p0, Lorg/altbeacon/beacon/service/Callback;->intent:Landroid/content/Intent;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    .line 70
    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    aput-object v0, v3, v1

    const-string v0, "Callback"

    const-string v1, "attempting callback via intent: %s"

    invoke-static {v0, v1, v3}, Lorg/altbeacon/beacon/logging/LogManager;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    iget-object v0, p0, Lorg/altbeacon/beacon/service/Callback;->intent:Landroid/content/Intent;

    invoke-virtual {v0, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 72
    iget-object p2, p0, Lorg/altbeacon/beacon/service/Callback;->intent:Landroid/content/Intent;

    invoke-virtual {p1, p2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return v2

    :cond_1
    return v1
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    .line 54
    iget-object v0, p0, Lorg/altbeacon/beacon/service/Callback;->intent:Landroid/content/Intent;

    return-object v0
.end method
