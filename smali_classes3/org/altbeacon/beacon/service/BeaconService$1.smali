.class Lorg/altbeacon/beacon/service/BeaconService$1;
.super Ljava/lang/Object;
.source "BeaconService.java"

# interfaces
.implements Lorg/altbeacon/beacon/service/scanner/CycledLeScanCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/altbeacon/beacon/service/BeaconService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/altbeacon/beacon/service/BeaconService;


# direct methods
.method constructor <init>(Lorg/altbeacon/beacon/service/BeaconService;)V
    .locals 0

    .line 346
    iput-object p1, p0, Lorg/altbeacon/beacon/service/BeaconService$1;->this$0:Lorg/altbeacon/beacon/service/BeaconService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCycleEnd()V
    .locals 5

    .line 363
    iget-object v0, p0, Lorg/altbeacon/beacon/service/BeaconService$1;->this$0:Lorg/altbeacon/beacon/service/BeaconService;

    invoke-static {v0}, Lorg/altbeacon/beacon/service/BeaconService;->access$200(Lorg/altbeacon/beacon/service/BeaconService;)Lorg/altbeacon/beacon/service/MonitoringStatus;

    move-result-object v0

    invoke-virtual {v0}, Lorg/altbeacon/beacon/service/MonitoringStatus;->updateNewlyOutside()V

    .line 364
    iget-object v0, p0, Lorg/altbeacon/beacon/service/BeaconService$1;->this$0:Lorg/altbeacon/beacon/service/BeaconService;

    invoke-static {v0}, Lorg/altbeacon/beacon/service/BeaconService;->access$300(Lorg/altbeacon/beacon/service/BeaconService;)V

    .line 366
    iget-object v0, p0, Lorg/altbeacon/beacon/service/BeaconService$1;->this$0:Lorg/altbeacon/beacon/service/BeaconService;

    invoke-static {v0}, Lorg/altbeacon/beacon/service/BeaconService;->access$400(Lorg/altbeacon/beacon/service/BeaconService;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "BeaconService"

    if-eqz v0, :cond_1

    .line 369
    new-array v0, v1, [Ljava/lang/Object;

    const-string v3, "Simulated scan data is deprecated and will be removed in a future release. Please use the new BeaconSimulator interface instead."

    invoke-static {v2, v3, v0}, Lorg/altbeacon/beacon/logging/LogManager;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 371
    iget-object v0, p0, Lorg/altbeacon/beacon/service/BeaconService$1;->this$0:Lorg/altbeacon/beacon/service/BeaconService;

    invoke-virtual {v0}, Lorg/altbeacon/beacon/service/BeaconService;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v3, v3, 0x2

    iput v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    if-eqz v3, :cond_0

    .line 372
    iget-object v0, p0, Lorg/altbeacon/beacon/service/BeaconService$1;->this$0:Lorg/altbeacon/beacon/service/BeaconService;

    invoke-static {v0}, Lorg/altbeacon/beacon/service/BeaconService;->access$400(Lorg/altbeacon/beacon/service/BeaconService;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/altbeacon/beacon/Beacon;

    .line 373
    iget-object v4, p0, Lorg/altbeacon/beacon/service/BeaconService$1;->this$0:Lorg/altbeacon/beacon/service/BeaconService;

    invoke-static {v4, v3}, Lorg/altbeacon/beacon/service/BeaconService;->access$500(Lorg/altbeacon/beacon/service/BeaconService;Lorg/altbeacon/beacon/Beacon;)V

    goto :goto_0

    .line 376
    :cond_0
    new-array v0, v1, [Ljava/lang/Object;

    const-string v3, "Simulated scan data provided, but ignored because we are not running in debug mode.  Please remove simulated scan data for production."

    invoke-static {v2, v3, v0}, Lorg/altbeacon/beacon/logging/LogManager;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 379
    :cond_1
    invoke-static {}, Lorg/altbeacon/beacon/BeaconManager;->getBeaconSimulator()Lorg/altbeacon/beacon/simulator/BeaconSimulator;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 382
    invoke-static {}, Lorg/altbeacon/beacon/BeaconManager;->getBeaconSimulator()Lorg/altbeacon/beacon/simulator/BeaconSimulator;

    move-result-object v0

    invoke-interface {v0}, Lorg/altbeacon/beacon/simulator/BeaconSimulator;->getBeacons()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 383
    iget-object v0, p0, Lorg/altbeacon/beacon/service/BeaconService$1;->this$0:Lorg/altbeacon/beacon/service/BeaconService;

    invoke-virtual {v0}, Lorg/altbeacon/beacon/service/BeaconService;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v3, v3, 0x2

    iput v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    if-eqz v3, :cond_2

    .line 384
    invoke-static {}, Lorg/altbeacon/beacon/BeaconManager;->getBeaconSimulator()Lorg/altbeacon/beacon/simulator/BeaconSimulator;

    move-result-object v0

    invoke-interface {v0}, Lorg/altbeacon/beacon/simulator/BeaconSimulator;->getBeacons()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/altbeacon/beacon/Beacon;

    .line 385
    iget-object v2, p0, Lorg/altbeacon/beacon/service/BeaconService$1;->this$0:Lorg/altbeacon/beacon/service/BeaconService;

    invoke-static {v2, v1}, Lorg/altbeacon/beacon/service/BeaconService;->access$500(Lorg/altbeacon/beacon/service/BeaconService;Lorg/altbeacon/beacon/Beacon;)V

    goto :goto_1

    .line 388
    :cond_2
    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "Beacon simulations provided, but ignored because we are not running in debug mode.  Please remove beacon simulations for production."

    invoke-static {v2, v1, v0}, Lorg/altbeacon/beacon/logging/LogManager;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 391
    :cond_3
    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "getBeacons is returning null. No simulated beacons to report."

    invoke-static {v2, v1, v0}, Lorg/altbeacon/beacon/logging/LogManager;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    :goto_2
    return-void
.end method

.method public onLeScan(Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .line 351
    iget-object v0, p0, Lorg/altbeacon/beacon/service/BeaconService$1;->this$0:Lorg/altbeacon/beacon/service/BeaconService;

    invoke-static {v0}, Lorg/altbeacon/beacon/service/BeaconService;->access$000(Lorg/altbeacon/beacon/service/BeaconService;)Lorg/altbeacon/beacon/BeaconManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/altbeacon/beacon/BeaconManager;->getNonBeaconLeScanCallback()Lorg/altbeacon/beacon/service/scanner/NonBeaconLeScanCallback;

    move-result-object v0

    const/4 v1, 0x0

    .line 354
    :try_start_0
    new-instance v2, Lorg/altbeacon/beacon/service/BeaconService$ScanProcessor;

    iget-object v3, p0, Lorg/altbeacon/beacon/service/BeaconService$1;->this$0:Lorg/altbeacon/beacon/service/BeaconService;

    invoke-direct {v2, v3, v0}, Lorg/altbeacon/beacon/service/BeaconService$ScanProcessor;-><init>(Lorg/altbeacon/beacon/service/BeaconService;Lorg/altbeacon/beacon/service/scanner/NonBeaconLeScanCallback;)V

    iget-object v0, p0, Lorg/altbeacon/beacon/service/BeaconService$1;->this$0:Lorg/altbeacon/beacon/service/BeaconService;

    invoke-static {v0}, Lorg/altbeacon/beacon/service/BeaconService;->access$100(Lorg/altbeacon/beacon/service/BeaconService;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [Lorg/altbeacon/beacon/service/BeaconService$ScanData;

    new-instance v4, Lorg/altbeacon/beacon/service/BeaconService$ScanData;

    iget-object v5, p0, Lorg/altbeacon/beacon/service/BeaconService$1;->this$0:Lorg/altbeacon/beacon/service/BeaconService;

    invoke-direct {v4, v5, p1, p2, p3}, Lorg/altbeacon/beacon/service/BeaconService$ScanData;-><init>(Lorg/altbeacon/beacon/service/BeaconService;Landroid/bluetooth/BluetoothDevice;I[B)V

    aput-object v4, v3, v1

    invoke-virtual {v2, v0, v3}, Lorg/altbeacon/beacon/service/BeaconService$ScanProcessor;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 357
    :catch_0
    new-array p1, v1, [Ljava/lang/Object;

    const-string p2, "BeaconService"

    const-string p3, "Ignoring scan result because we cannot keep up."

    invoke-static {p2, p3, p1}, Lorg/altbeacon/beacon/logging/LogManager;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
