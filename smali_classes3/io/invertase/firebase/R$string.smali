.class public final Lio/invertase/firebase/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/invertase/firebase/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f100000

.field public static final abc_action_bar_up_description:I = 0x7f100001

.field public static final abc_action_menu_overflow_description:I = 0x7f100002

.field public static final abc_action_mode_done:I = 0x7f100003

.field public static final abc_activity_chooser_view_see_all:I = 0x7f100004

.field public static final abc_activitychooserview_choose_application:I = 0x7f100005

.field public static final abc_capital_off:I = 0x7f100006

.field public static final abc_capital_on:I = 0x7f100007

.field public static final abc_font_family_body_1_material:I = 0x7f100008

.field public static final abc_font_family_body_2_material:I = 0x7f100009

.field public static final abc_font_family_button_material:I = 0x7f10000a

.field public static final abc_font_family_caption_material:I = 0x7f10000b

.field public static final abc_font_family_display_1_material:I = 0x7f10000c

.field public static final abc_font_family_display_2_material:I = 0x7f10000d

.field public static final abc_font_family_display_3_material:I = 0x7f10000e

.field public static final abc_font_family_display_4_material:I = 0x7f10000f

.field public static final abc_font_family_headline_material:I = 0x7f100010

.field public static final abc_font_family_menu_material:I = 0x7f100011

.field public static final abc_font_family_subhead_material:I = 0x7f100012

.field public static final abc_font_family_title_material:I = 0x7f100013

.field public static final abc_menu_alt_shortcut_label:I = 0x7f100014

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f100015

.field public static final abc_menu_delete_shortcut_label:I = 0x7f100016

.field public static final abc_menu_enter_shortcut_label:I = 0x7f100017

.field public static final abc_menu_function_shortcut_label:I = 0x7f100018

.field public static final abc_menu_meta_shortcut_label:I = 0x7f100019

.field public static final abc_menu_shift_shortcut_label:I = 0x7f10001a

.field public static final abc_menu_space_shortcut_label:I = 0x7f10001b

.field public static final abc_menu_sym_shortcut_label:I = 0x7f10001c

.field public static final abc_prepend_shortcut_label:I = 0x7f10001d

.field public static final abc_search_hint:I = 0x7f10001e

.field public static final abc_searchview_description_clear:I = 0x7f10001f

.field public static final abc_searchview_description_query:I = 0x7f100020

.field public static final abc_searchview_description_search:I = 0x7f100021

.field public static final abc_searchview_description_submit:I = 0x7f100022

.field public static final abc_searchview_description_voice:I = 0x7f100023

.field public static final abc_shareactionprovider_share_with:I = 0x7f100024

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f100025

.field public static final abc_toolbar_collapse_description:I = 0x7f100026

.field public static final adjustable_description:I = 0x7f100027

.field public static final catalyst_copy_button:I = 0x7f100030

.field public static final catalyst_debugjs:I = 0x7f100031

.field public static final catalyst_debugjs_nuclide:I = 0x7f100032

.field public static final catalyst_debugjs_nuclide_failure:I = 0x7f100033

.field public static final catalyst_debugjs_off:I = 0x7f100034

.field public static final catalyst_dismiss_button:I = 0x7f100035

.field public static final catalyst_element_inspector:I = 0x7f100036

.field public static final catalyst_heap_capture:I = 0x7f100037

.field public static final catalyst_hot_module_replacement:I = 0x7f100038

.field public static final catalyst_hot_module_replacement_off:I = 0x7f100039

.field public static final catalyst_jsload_error:I = 0x7f10003a

.field public static final catalyst_live_reload:I = 0x7f10003b

.field public static final catalyst_live_reload_off:I = 0x7f10003c

.field public static final catalyst_loading_from_url:I = 0x7f10003d

.field public static final catalyst_perf_monitor:I = 0x7f10003e

.field public static final catalyst_perf_monitor_off:I = 0x7f10003f

.field public static final catalyst_poke_sampling_profiler:I = 0x7f100040

.field public static final catalyst_reload_button:I = 0x7f100041

.field public static final catalyst_reloadjs:I = 0x7f100042

.field public static final catalyst_remotedbg_error:I = 0x7f100043

.field public static final catalyst_remotedbg_message:I = 0x7f100044

.field public static final catalyst_report_button:I = 0x7f100045

.field public static final catalyst_settings:I = 0x7f100046

.field public static final catalyst_settings_title:I = 0x7f100047

.field public static final header_description:I = 0x7f1000d9

.field public static final image_button_description:I = 0x7f1000df

.field public static final image_description:I = 0x7f1000e0

.field public static final link_description:I = 0x7f1000ea

.field public static final search_description:I = 0x7f10010e

.field public static final search_menu_title:I = 0x7f10010f

.field public static final status_bar_notification_info_overflow:I = 0x7f100118


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 869
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
