.class final Lio/nlopez/smartlocation/rx/ObservableFactory$4;
.super Ljava/lang/Object;
.source "ObservableFactory.java"

# interfaces
.implements Lrx/Observable$OnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/nlopez/smartlocation/rx/ObservableFactory;->from(Lio/nlopez/smartlocation/SmartLocation$ActivityRecognitionControl;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observable$OnSubscribe<",
        "Lcom/google/android/gms/location/DetectedActivity;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$activityControl:Lio/nlopez/smartlocation/SmartLocation$ActivityRecognitionControl;


# direct methods
.method constructor <init>(Lio/nlopez/smartlocation/SmartLocation$ActivityRecognitionControl;)V
    .locals 0

    .line 68
    iput-object p1, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$4;->val$activityControl:Lio/nlopez/smartlocation/SmartLocation$ActivityRecognitionControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 68
    check-cast p1, Lrx/Subscriber;

    invoke-virtual {p0, p1}, Lio/nlopez/smartlocation/rx/ObservableFactory$4;->call(Lrx/Subscriber;)V

    return-void
.end method

.method public call(Lrx/Subscriber;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-",
            "Lcom/google/android/gms/location/DetectedActivity;",
            ">;)V"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$4;->val$activityControl:Lio/nlopez/smartlocation/SmartLocation$ActivityRecognitionControl;

    new-instance v1, Lio/nlopez/smartlocation/rx/ObservableFactory$4$1;

    invoke-direct {v1, p0, p1}, Lio/nlopez/smartlocation/rx/ObservableFactory$4$1;-><init>(Lio/nlopez/smartlocation/rx/ObservableFactory$4;Lrx/Subscriber;)V

    invoke-virtual {v0, v1}, Lio/nlopez/smartlocation/SmartLocation$ActivityRecognitionControl;->start(Lio/nlopez/smartlocation/OnActivityUpdatedListener;)V

    return-void
.end method
