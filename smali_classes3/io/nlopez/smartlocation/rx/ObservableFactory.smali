.class public Lio/nlopez/smartlocation/rx/ObservableFactory;
.super Ljava/lang/Object;
.source "ObservableFactory.java"


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This should not be instantiated"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static from(Lio/nlopez/smartlocation/SmartLocation$ActivityRecognitionControl;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/nlopez/smartlocation/SmartLocation$ActivityRecognitionControl;",
            ")",
            "Lrx/Observable<",
            "Lcom/google/android/gms/location/DetectedActivity;",
            ">;"
        }
    .end annotation

    .line 68
    new-instance v0, Lio/nlopez/smartlocation/rx/ObservableFactory$4;

    invoke-direct {v0, p0}, Lio/nlopez/smartlocation/rx/ObservableFactory$4;-><init>(Lio/nlopez/smartlocation/SmartLocation$ActivityRecognitionControl;)V

    invoke-static {v0}, Lrx/Observable;->create(Lrx/Observable$OnSubscribe;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lio/nlopez/smartlocation/rx/ObservableFactory$3;

    invoke-direct {v1, p0}, Lio/nlopez/smartlocation/rx/ObservableFactory$3;-><init>(Lio/nlopez/smartlocation/SmartLocation$ActivityRecognitionControl;)V

    .line 79
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnUnsubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static from(Lio/nlopez/smartlocation/SmartLocation$GeofencingControl;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/nlopez/smartlocation/SmartLocation$GeofencingControl;",
            ")",
            "Lrx/Observable<",
            "Lio/nlopez/smartlocation/geofencing/utils/TransitionGeofence;",
            ">;"
        }
    .end annotation

    .line 95
    new-instance v0, Lio/nlopez/smartlocation/rx/ObservableFactory$6;

    invoke-direct {v0, p0}, Lio/nlopez/smartlocation/rx/ObservableFactory$6;-><init>(Lio/nlopez/smartlocation/SmartLocation$GeofencingControl;)V

    invoke-static {v0}, Lrx/Observable;->create(Lrx/Observable$OnSubscribe;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lio/nlopez/smartlocation/rx/ObservableFactory$5;

    invoke-direct {v1, p0}, Lio/nlopez/smartlocation/rx/ObservableFactory$5;-><init>(Lio/nlopez/smartlocation/SmartLocation$GeofencingControl;)V

    .line 105
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnUnsubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static from(Lio/nlopez/smartlocation/SmartLocation$LocationControl;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/nlopez/smartlocation/SmartLocation$LocationControl;",
            ")",
            "Lrx/Observable<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation

    .line 40
    new-instance v0, Lio/nlopez/smartlocation/rx/ObservableFactory$2;

    invoke-direct {v0, p0}, Lio/nlopez/smartlocation/rx/ObservableFactory$2;-><init>(Lio/nlopez/smartlocation/SmartLocation$LocationControl;)V

    invoke-static {v0}, Lrx/Observable;->create(Lrx/Observable$OnSubscribe;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lio/nlopez/smartlocation/rx/ObservableFactory$1;

    invoke-direct {v1, p0}, Lio/nlopez/smartlocation/rx/ObservableFactory$1;-><init>(Lio/nlopez/smartlocation/SmartLocation$LocationControl;)V

    .line 51
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnUnsubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static fromAddress(Landroid/content/Context;Ljava/lang/String;I)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lio/nlopez/smartlocation/geocoding/utils/LocationAddress;",
            ">;>;"
        }
    .end annotation

    .line 123
    new-instance v0, Lio/nlopez/smartlocation/rx/ObservableFactory$7;

    invoke-direct {v0, p0, p1, p2}, Lio/nlopez/smartlocation/rx/ObservableFactory$7;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-static {v0}, Lrx/Observable;->create(Lrx/Observable$OnSubscribe;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static fromLocation(Landroid/content/Context;Landroid/location/Location;I)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/location/Location;",
            "I)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Landroid/location/Address;",
            ">;>;"
        }
    .end annotation

    .line 148
    new-instance v0, Lio/nlopez/smartlocation/rx/ObservableFactory$8;

    invoke-direct {v0, p0, p1, p2}, Lio/nlopez/smartlocation/rx/ObservableFactory$8;-><init>(Landroid/content/Context;Landroid/location/Location;I)V

    invoke-static {v0}, Lrx/Observable;->create(Lrx/Observable$OnSubscribe;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method
