.class final Lio/nlopez/smartlocation/rx/ObservableFactory$7;
.super Ljava/lang/Object;
.source "ObservableFactory.java"

# interfaces
.implements Lrx/Observable$OnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/nlopez/smartlocation/rx/ObservableFactory;->fromAddress(Landroid/content/Context;Ljava/lang/String;I)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observable$OnSubscribe<",
        "Ljava/util/List<",
        "Lio/nlopez/smartlocation/geocoding/utils/LocationAddress;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic val$address:Ljava/lang/String;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$maxResults:I


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 0

    .line 123
    iput-object p1, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$7;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$7;->val$address:Ljava/lang/String;

    iput p3, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$7;->val$maxResults:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 123
    check-cast p1, Lrx/Subscriber;

    invoke-virtual {p0, p1}, Lio/nlopez/smartlocation/rx/ObservableFactory$7;->call(Lrx/Subscriber;)V

    return-void
.end method

.method public call(Lrx/Subscriber;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-",
            "Ljava/util/List<",
            "Lio/nlopez/smartlocation/geocoding/utils/LocationAddress;",
            ">;>;)V"
        }
    .end annotation

    .line 126
    iget-object v0, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$7;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lio/nlopez/smartlocation/SmartLocation;->with(Landroid/content/Context;)Lio/nlopez/smartlocation/SmartLocation;

    move-result-object v0

    invoke-virtual {v0}, Lio/nlopez/smartlocation/SmartLocation;->geocoding()Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;

    move-result-object v0

    iget-object v1, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$7;->val$address:Ljava/lang/String;

    iget v2, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$7;->val$maxResults:I

    invoke-virtual {v0, v1, v2}, Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;->add(Ljava/lang/String;I)Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;

    move-result-object v0

    new-instance v1, Lio/nlopez/smartlocation/rx/ObservableFactory$7$1;

    invoke-direct {v1, p0, p1}, Lio/nlopez/smartlocation/rx/ObservableFactory$7$1;-><init>(Lio/nlopez/smartlocation/rx/ObservableFactory$7;Lrx/Subscriber;)V

    invoke-virtual {v0, v1}, Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;->start(Lio/nlopez/smartlocation/OnGeocodingListener;)V

    return-void
.end method
