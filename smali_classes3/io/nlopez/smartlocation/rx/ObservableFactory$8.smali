.class final Lio/nlopez/smartlocation/rx/ObservableFactory$8;
.super Ljava/lang/Object;
.source "ObservableFactory.java"

# interfaces
.implements Lrx/Observable$OnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/nlopez/smartlocation/rx/ObservableFactory;->fromLocation(Landroid/content/Context;Landroid/location/Location;I)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observable$OnSubscribe<",
        "Ljava/util/List<",
        "Landroid/location/Address;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$location:Landroid/location/Location;

.field final synthetic val$maxResults:I


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/location/Location;I)V
    .locals 0

    .line 148
    iput-object p1, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$8;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$8;->val$location:Landroid/location/Location;

    iput p3, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$8;->val$maxResults:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 148
    check-cast p1, Lrx/Subscriber;

    invoke-virtual {p0, p1}, Lio/nlopez/smartlocation/rx/ObservableFactory$8;->call(Lrx/Subscriber;)V

    return-void
.end method

.method public call(Lrx/Subscriber;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-",
            "Ljava/util/List<",
            "Landroid/location/Address;",
            ">;>;)V"
        }
    .end annotation

    .line 151
    iget-object v0, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$8;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lio/nlopez/smartlocation/SmartLocation;->with(Landroid/content/Context;)Lio/nlopez/smartlocation/SmartLocation;

    move-result-object v0

    invoke-virtual {v0}, Lio/nlopez/smartlocation/SmartLocation;->geocoding()Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;

    move-result-object v0

    iget-object v1, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$8;->val$location:Landroid/location/Location;

    iget v2, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$8;->val$maxResults:I

    invoke-virtual {v0, v1, v2}, Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;->add(Landroid/location/Location;I)Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;

    move-result-object v0

    new-instance v1, Lio/nlopez/smartlocation/rx/ObservableFactory$8$1;

    invoke-direct {v1, p0, p1}, Lio/nlopez/smartlocation/rx/ObservableFactory$8$1;-><init>(Lio/nlopez/smartlocation/rx/ObservableFactory$8;Lrx/Subscriber;)V

    invoke-virtual {v0, v1}, Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;->start(Lio/nlopez/smartlocation/OnReverseGeocodingListener;)V

    return-void
.end method
