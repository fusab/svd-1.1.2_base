.class final Lio/nlopez/smartlocation/rx/ObservableFactory$2;
.super Ljava/lang/Object;
.source "ObservableFactory.java"

# interfaces
.implements Lrx/Observable$OnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/nlopez/smartlocation/rx/ObservableFactory;->from(Lio/nlopez/smartlocation/SmartLocation$LocationControl;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observable$OnSubscribe<",
        "Landroid/location/Location;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$locationControl:Lio/nlopez/smartlocation/SmartLocation$LocationControl;


# direct methods
.method constructor <init>(Lio/nlopez/smartlocation/SmartLocation$LocationControl;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$2;->val$locationControl:Lio/nlopez/smartlocation/SmartLocation$LocationControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 40
    check-cast p1, Lrx/Subscriber;

    invoke-virtual {p0, p1}, Lio/nlopez/smartlocation/rx/ObservableFactory$2;->call(Lrx/Subscriber;)V

    return-void
.end method

.method public call(Lrx/Subscriber;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-",
            "Landroid/location/Location;",
            ">;)V"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$2;->val$locationControl:Lio/nlopez/smartlocation/SmartLocation$LocationControl;

    new-instance v1, Lio/nlopez/smartlocation/rx/ObservableFactory$2$1;

    invoke-direct {v1, p0, p1}, Lio/nlopez/smartlocation/rx/ObservableFactory$2$1;-><init>(Lio/nlopez/smartlocation/rx/ObservableFactory$2;Lrx/Subscriber;)V

    invoke-virtual {v0, v1}, Lio/nlopez/smartlocation/SmartLocation$LocationControl;->start(Lio/nlopez/smartlocation/OnLocationUpdatedListener;)V

    return-void
.end method
