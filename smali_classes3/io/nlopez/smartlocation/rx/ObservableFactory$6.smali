.class final Lio/nlopez/smartlocation/rx/ObservableFactory$6;
.super Ljava/lang/Object;
.source "ObservableFactory.java"

# interfaces
.implements Lrx/Observable$OnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/nlopez/smartlocation/rx/ObservableFactory;->from(Lio/nlopez/smartlocation/SmartLocation$GeofencingControl;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observable$OnSubscribe<",
        "Lio/nlopez/smartlocation/geofencing/utils/TransitionGeofence;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$geofencingControl:Lio/nlopez/smartlocation/SmartLocation$GeofencingControl;


# direct methods
.method constructor <init>(Lio/nlopez/smartlocation/SmartLocation$GeofencingControl;)V
    .locals 0

    .line 95
    iput-object p1, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$6;->val$geofencingControl:Lio/nlopez/smartlocation/SmartLocation$GeofencingControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 95
    check-cast p1, Lrx/Subscriber;

    invoke-virtual {p0, p1}, Lio/nlopez/smartlocation/rx/ObservableFactory$6;->call(Lrx/Subscriber;)V

    return-void
.end method

.method public call(Lrx/Subscriber;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-",
            "Lio/nlopez/smartlocation/geofencing/utils/TransitionGeofence;",
            ">;)V"
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lio/nlopez/smartlocation/rx/ObservableFactory$6;->val$geofencingControl:Lio/nlopez/smartlocation/SmartLocation$GeofencingControl;

    new-instance v1, Lio/nlopez/smartlocation/rx/ObservableFactory$6$1;

    invoke-direct {v1, p0, p1}, Lio/nlopez/smartlocation/rx/ObservableFactory$6$1;-><init>(Lio/nlopez/smartlocation/rx/ObservableFactory$6;Lrx/Subscriber;)V

    invoke-virtual {v0, v1}, Lio/nlopez/smartlocation/SmartLocation$GeofencingControl;->start(Lio/nlopez/smartlocation/OnGeofencingTransitionListener;)V

    return-void
.end method
