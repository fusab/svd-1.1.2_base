.class public Lversioned/host/exp/exponent/ExponentDevBundleDownloadListener;
.super Ljava/lang/Object;
.source "ExponentDevBundleDownloadListener.java"

# interfaces
.implements Lcom/facebook/react/devsupport/interfaces/DevBundleDownloadListener;


# instance fields
.field private mListener:Lhost/exp/exponent/experience/DevBundleDownloadProgressListener;


# direct methods
.method public constructor <init>(Lhost/exp/exponent/experience/DevBundleDownloadProgressListener;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lversioned/host/exp/exponent/ExponentDevBundleDownloadListener;->mListener:Lhost/exp/exponent/experience/DevBundleDownloadProgressListener;

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Exception;)V
    .locals 1

    .line 33
    iget-object v0, p0, Lversioned/host/exp/exponent/ExponentDevBundleDownloadListener;->mListener:Lhost/exp/exponent/experience/DevBundleDownloadProgressListener;

    invoke-interface {v0, p1}, Lhost/exp/exponent/experience/DevBundleDownloadProgressListener;->onFailure(Ljava/lang/Exception;)V

    return-void
.end method

.method public onProgress(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .line 28
    iget-object v0, p0, Lversioned/host/exp/exponent/ExponentDevBundleDownloadListener;->mListener:Lhost/exp/exponent/experience/DevBundleDownloadProgressListener;

    invoke-interface {v0, p1, p2, p3}, Lhost/exp/exponent/experience/DevBundleDownloadProgressListener;->onProgress(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method public onSuccess(Lcom/facebook/react/bridge/NativeDeltaClient;)V
    .locals 0
    .param p1    # Lcom/facebook/react/bridge/NativeDeltaClient;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .line 23
    iget-object p1, p0, Lversioned/host/exp/exponent/ExponentDevBundleDownloadListener;->mListener:Lhost/exp/exponent/experience/DevBundleDownloadProgressListener;

    invoke-interface {p1}, Lhost/exp/exponent/experience/DevBundleDownloadProgressListener;->onSuccess()V

    return-void
.end method
