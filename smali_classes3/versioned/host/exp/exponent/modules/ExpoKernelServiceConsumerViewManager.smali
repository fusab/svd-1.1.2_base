.class public abstract Lversioned/host/exp/exponent/modules/ExpoKernelServiceConsumerViewManager;
.super Lcom/facebook/react/uimanager/SimpleViewManager;
.source "ExpoKernelServiceConsumerViewManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        ">",
        "Lcom/facebook/react/uimanager/SimpleViewManager<",
        "TT;>;"
    }
.end annotation


# instance fields
.field protected final experienceId:Lhost/exp/exponent/kernel/ExperienceId;

.field protected mKernelServiceRegistry:Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lhost/exp/exponent/kernel/ExperienceId;)V
    .locals 1

    .line 20
    invoke-direct {p0}, Lcom/facebook/react/uimanager/SimpleViewManager;-><init>()V

    .line 21
    iput-object p1, p0, Lversioned/host/exp/exponent/modules/ExpoKernelServiceConsumerViewManager;->experienceId:Lhost/exp/exponent/kernel/ExperienceId;

    .line 22
    invoke-static {}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->getInstance()Lhost/exp/exponent/di/NativeModuleDepsProvider;

    move-result-object p1

    const-class v0, Lversioned/host/exp/exponent/modules/ExpoKernelServiceConsumerViewManager;

    invoke-virtual {p1, v0, p0}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->inject(Ljava/lang/Class;Ljava/lang/Object;)V

    return-void
.end method
