.class final enum Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;
.super Ljava/lang/Enum;
.source "RNSharedElementTypes.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

.field public static final enum AUTO:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

.field public static final enum CENTER_BOTTOM:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

.field public static final enum CENTER_CENTER:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

.field public static final enum CENTER_TOP:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

.field public static final enum LEFT_BOTTOM:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

.field public static final enum LEFT_CENTER:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

.field public static final enum LEFT_TOP:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

.field public static final enum RIGHT_BOTTOM:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

.field public static final enum RIGHT_CENTER:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

.field public static final enum RIGHT_TOP:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 26
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    const/4 v1, 0x0

    const-string v2, "AUTO"

    invoke-direct {v0, v2, v1, v1}, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->AUTO:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    .line 27
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    const/4 v2, 0x1

    const-string v3, "LEFT_TOP"

    invoke-direct {v0, v3, v2, v2}, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->LEFT_TOP:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    .line 28
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    const/4 v3, 0x2

    const-string v4, "LEFT_CENTER"

    invoke-direct {v0, v4, v3, v3}, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->LEFT_CENTER:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    .line 29
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    const/4 v4, 0x3

    const-string v5, "LEFT_BOTTOM"

    invoke-direct {v0, v5, v4, v4}, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->LEFT_BOTTOM:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    .line 30
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    const/4 v5, 0x4

    const-string v6, "RIGHT_TOP"

    invoke-direct {v0, v6, v5, v5}, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->RIGHT_TOP:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    .line 31
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    const/4 v6, 0x5

    const-string v7, "RIGHT_CENTER"

    invoke-direct {v0, v7, v6, v6}, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->RIGHT_CENTER:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    .line 32
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    const/4 v7, 0x6

    const-string v8, "RIGHT_BOTTOM"

    invoke-direct {v0, v8, v7, v7}, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->RIGHT_BOTTOM:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    .line 33
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    const/4 v8, 0x7

    const-string v9, "CENTER_TOP"

    invoke-direct {v0, v9, v8, v8}, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->CENTER_TOP:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    .line 34
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    const/16 v9, 0x8

    const-string v10, "CENTER_CENTER"

    invoke-direct {v0, v10, v9, v9}, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->CENTER_CENTER:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    .line 35
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    const/16 v10, 0x9

    const-string v11, "CENTER_BOTTOM"

    invoke-direct {v0, v11, v10, v10}, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->CENTER_BOTTOM:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    const/16 v0, 0xa

    .line 25
    new-array v0, v0, [Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    sget-object v11, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->AUTO:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    aput-object v11, v0, v1

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->LEFT_TOP:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    aput-object v1, v0, v2

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->LEFT_CENTER:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    aput-object v1, v0, v3

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->LEFT_BOTTOM:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    aput-object v1, v0, v4

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->RIGHT_TOP:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    aput-object v1, v0, v5

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->RIGHT_CENTER:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    aput-object v1, v0, v6

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->RIGHT_BOTTOM:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    aput-object v1, v0, v7

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->CENTER_TOP:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    aput-object v1, v0, v8

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->CENTER_CENTER:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    aput-object v1, v0, v9

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->CENTER_BOTTOM:Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    aput-object v1, v0, v10

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->$VALUES:[Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;
    .locals 1

    .line 25
    const-class v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    return-object p0
.end method

.method public static values()[Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;
    .locals 1

    .line 25
    sget-object v0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->$VALUES:[Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    invoke-virtual {v0}, [Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 39
    iget v0, p0, Lversioned/host/exp/exponent/modules/api/components/sharedelement/RNSharedElementAlign;->value:I

    return v0
.end method
