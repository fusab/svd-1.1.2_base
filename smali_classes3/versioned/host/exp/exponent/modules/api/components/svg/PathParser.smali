.class Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;
.super Ljava/lang/Object;
.source "PathParser.java"


# static fields
.field private static i:I

.field private static l:I

.field private static mPath:Landroid/graphics/Path;

.field private static mPenDown:Z

.field private static mPenDownX:F

.field private static mPenDownY:F

.field private static mPenX:F

.field private static mPenY:F

.field private static mPivotX:F

.field private static mPivotY:F

.field static mScale:F

.field private static s:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static arc(FFFZZFF)V
    .locals 8

    .line 270
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    add-float v6, p5, v0

    sget p5, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    add-float v7, p6, p5

    move v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v1 .. v7}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->arcTo(FFFZZFF)V

    return-void
.end method

.method private static arcTo(FFFZZFF)V
    .locals 22

    move/from16 v0, p3

    move/from16 v6, p4

    .line 275
    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    .line 276
    sget v2, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    const/4 v3, 0x0

    cmpl-float v4, p1, v3

    if-nez v4, :cond_1

    cmpl-float v4, p0, v3

    if-nez v4, :cond_0

    sub-float v4, p6, v2

    goto :goto_0

    :cond_0
    move/from16 v4, p0

    goto :goto_0

    :cond_1
    move/from16 v4, p1

    .line 278
    :goto_0
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v5, p0, v3

    if-nez v5, :cond_2

    sub-float v5, p5, v1

    goto :goto_1

    :cond_2
    move/from16 v5, p0

    .line 279
    :goto_1
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpl-float v7, v5, v3

    if-eqz v7, :cond_b

    cmpl-float v7, v4, v3

    if-eqz v7, :cond_b

    cmpl-float v7, p5, v1

    if-nez v7, :cond_3

    cmpl-float v7, p6, v2

    if-nez v7, :cond_3

    goto/16 :goto_6

    :cond_3
    move/from16 v7, p2

    float-to-double v7, v7

    .line 286
    invoke-static {v7, v8}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v7

    double-to-float v7, v7

    float-to-double v8, v7

    .line 287
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    double-to-float v10, v10

    .line 288
    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v8, v8

    sub-float v9, p5, v1

    sub-float v11, p6, v2

    mul-float v12, v10, v9

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    mul-float v14, v8, v11

    div-float/2addr v14, v13

    add-float/2addr v12, v14

    neg-float v14, v8

    mul-float v15, v14, v9

    div-float/2addr v15, v13

    mul-float v16, v10, v11

    div-float v16, v16, v13

    add-float v15, v15, v16

    mul-float v16, v5, v5

    mul-float v17, v16, v4

    mul-float v17, v17, v4

    mul-float v18, v4, v4

    mul-float v18, v18, v12

    mul-float v18, v18, v12

    mul-float v16, v16, v15

    mul-float v16, v16, v15

    sub-float v19, v17, v16

    sub-float v19, v19, v18

    cmpg-float v20, v19, v3

    if-gez v20, :cond_4

    const/high16 v12, 0x3f800000    # 1.0f

    div-float v19, v19, v17

    sub-float v12, v12, v19

    move/from16 v17, v14

    float-to-double v13, v12

    .line 301
    invoke-static {v13, v14}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    double-to-float v12, v12

    mul-float v5, v5, v12

    mul-float v4, v4, v12

    const/high16 v12, 0x40000000    # 2.0f

    div-float v13, v9, v12

    div-float v12, v11, v12

    move/from16 v21, v13

    move v13, v12

    move/from16 v12, v21

    goto :goto_2

    :cond_4
    move/from16 v17, v14

    add-float v16, v16, v18

    div-float v13, v19, v16

    float-to-double v13, v13

    .line 307
    invoke-static {v13, v14}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v13

    double-to-float v13, v13

    if-ne v0, v6, :cond_5

    neg-float v13, v13

    :cond_5
    neg-float v14, v13

    mul-float v14, v14, v15

    mul-float v14, v14, v5

    div-float/2addr v14, v4

    mul-float v13, v13, v12

    mul-float v13, v13, v4

    div-float/2addr v13, v5

    mul-float v12, v10, v14

    mul-float v15, v8, v13

    sub-float/2addr v12, v15

    const/high16 v15, 0x40000000    # 2.0f

    div-float v16, v9, v15

    add-float v12, v12, v16

    mul-float v14, v14, v8

    mul-float v13, v13, v10

    add-float/2addr v14, v13

    div-float v13, v11, v15

    add-float/2addr v13, v14

    :goto_2
    div-float v14, v10, v5

    div-float/2addr v8, v5

    div-float v15, v17, v4

    div-float/2addr v10, v4

    neg-float v3, v12

    mul-float v17, v15, v3

    neg-float v6, v13

    mul-float v18, v10, v6

    add-float v0, v17, v18

    move/from16 p1, v4

    move/from16 p0, v5

    float-to-double v4, v0

    mul-float v3, v3, v14

    mul-float v6, v6, v8

    add-float/2addr v3, v6

    move/from16 v17, v7

    float-to-double v6, v3

    .line 325
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v3

    double-to-float v4, v3

    sub-float v0, v9, v12

    mul-float v15, v15, v0

    sub-float v3, v11, v13

    mul-float v10, v10, v3

    add-float/2addr v15, v10

    float-to-double v5, v15

    mul-float v14, v14, v0

    mul-float v8, v8, v3

    add-float/2addr v14, v8

    float-to-double v7, v14

    .line 326
    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v5

    double-to-float v5, v5

    add-float v0, v12, v1

    add-float v3, v13, v2

    add-float/2addr v9, v1

    add-float/2addr v11, v2

    .line 333
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->setPenDown()V

    .line 335
    sput v9, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotX:F

    sput v9, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    .line 336
    sput v11, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotY:F

    sput v11, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    cmpl-float v1, p0, p1

    if-nez v1, :cond_a

    const/4 v1, 0x0

    cmpl-float v1, v17, v1

    if-eqz v1, :cond_6

    goto :goto_4

    :cond_6
    float-to-double v1, v4

    .line 342
    invoke-static {v1, v2}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v1

    double-to-float v1, v1

    float-to-double v4, v5

    .line 343
    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v2, v4

    sub-float v2, v1, v2

    const/high16 v4, 0x43b40000    # 360.0f

    rem-float/2addr v2, v4

    .line 344
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v5, 0x43340000    # 180.0f

    if-eqz p3, :cond_7

    cmpg-float v5, v2, v5

    if-gez v5, :cond_8

    goto :goto_3

    :cond_7
    cmpl-float v5, v2, v5

    if-lez v5, :cond_8

    :goto_3
    sub-float v2, v4, v2

    :cond_8
    if-nez p4, :cond_9

    neg-float v2, v2

    .line 360
    :cond_9
    new-instance v4, Landroid/graphics/RectF;

    sub-float v5, v0, p0

    sget v6, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mScale:F

    mul-float v5, v5, v6

    sub-float v7, v3, p0

    mul-float v7, v7, v6

    add-float v0, v0, p0

    mul-float v0, v0, v6

    add-float v3, v3, p0

    mul-float v3, v3, v6

    invoke-direct {v4, v5, v7, v0, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 366
    sget-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0, v4, v1, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    goto :goto_5

    :cond_a
    :goto_4
    move v1, v3

    move/from16 v2, p0

    move/from16 v3, p1

    move/from16 v6, p4

    move/from16 v7, v17

    .line 339
    invoke-static/range {v0 .. v7}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->arcToBezier(FFFFFFZF)V

    :goto_5
    return-void

    .line 282
    :cond_b
    :goto_6
    invoke-static/range {p5 .. p6}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->lineTo(FF)V

    return-void
.end method

.method private static arcToBezier(FFFFFFZF)V
    .locals 24

    move/from16 v0, p4

    move/from16 v1, p7

    float-to-double v1, v1

    .line 381
    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    double-to-float v3, v3

    .line 382
    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float v2, v3, p2

    neg-float v4, v1

    mul-float v4, v4, p3

    mul-float v1, v1, p2

    mul-float v3, v3, p3

    sub-float v5, p5, v0

    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    const/4 v8, 0x0

    cmpg-float v9, v5, v8

    if-gez v9, :cond_0

    if-eqz p6, :cond_0

    float-to-double v8, v5

    add-double/2addr v8, v6

    :goto_0
    double-to-float v5, v8

    goto :goto_1

    :cond_0
    cmpl-float v8, v5, v8

    if-lez v8, :cond_1

    if-nez p6, :cond_1

    float-to-double v8, v5

    sub-double/2addr v8, v6

    goto :goto_0

    :cond_1
    :goto_1
    float-to-double v6, v5

    const-wide v8, 0x3ff921fb54442d18L    # 1.5707963267948966

    div-double/2addr v6, v8

    .line 396
    invoke-static {v6, v7}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->round(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v6, v6

    int-to-float v7, v6

    div-float/2addr v5, v7

    const-wide v7, 0x3ff5555555555555L    # 1.3333333333333333

    const/high16 v9, 0x40800000    # 4.0f

    div-float v9, v5, v9

    float-to-double v9, v9

    .line 399
    invoke-static {v9, v10}, Ljava/lang/Math;->tan(D)D

    move-result-wide v9

    mul-double v9, v9, v7

    double-to-float v7, v9

    float-to-double v8, v0

    .line 401
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    double-to-float v10, v10

    .line 402
    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v8, v8

    const/4 v9, 0x0

    :goto_2
    if-ge v9, v6, :cond_2

    mul-float v11, v7, v8

    sub-float v11, v10, v11

    mul-float v10, v10, v7

    add-float/2addr v8, v10

    add-float/2addr v0, v5

    float-to-double v12, v0

    .line 409
    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    double-to-float v10, v14

    .line 410
    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    double-to-float v12, v12

    mul-float v13, v7, v12

    add-float/2addr v13, v10

    mul-float v14, v7, v10

    sub-float v14, v12, v14

    mul-float v15, v2, v11

    add-float v15, p0, v15

    mul-float v16, v4, v8

    add-float v15, v15, v16

    mul-float v11, v11, v1

    add-float v11, p1, v11

    mul-float v8, v8, v3

    add-float/2addr v11, v8

    mul-float v8, v2, v13

    add-float v8, p0, v8

    mul-float v16, v4, v14

    add-float v8, v8, v16

    mul-float v13, v13, v1

    add-float v13, p1, v13

    mul-float v14, v14, v3

    add-float/2addr v13, v14

    mul-float v14, v2, v10

    add-float v14, p0, v14

    mul-float v16, v4, v12

    add-float v14, v14, v16

    mul-float v16, v1, v10

    add-float v16, p1, v16

    mul-float v17, v3, v12

    add-float v16, v16, v17

    .line 422
    sget-object v17, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPath:Landroid/graphics/Path;

    sget v18, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mScale:F

    mul-float v15, v15, v18

    mul-float v19, v11, v18

    mul-float v20, v8, v18

    mul-float v21, v13, v18

    mul-float v22, v14, v18

    mul-float v23, v16, v18

    move/from16 v18, v15

    invoke-virtual/range {v17 .. v23}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    add-int/lit8 v9, v9, 0x1

    move v8, v12

    goto :goto_2

    :cond_2
    return-void
.end method

.method private static close()V
    .locals 1

    .line 371
    sget-boolean v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenDown:Z

    if-eqz v0, :cond_0

    .line 372
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenDownX:F

    sput v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    .line 373
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenDownY:F

    sput v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    const/4 v0, 0x0

    .line 374
    sput-boolean v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenDown:Z

    .line 375
    sget-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    :cond_0
    return-void
.end method

.method private static cubicTo(FFFFFF)V
    .locals 7

    .line 218
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->setPenDown()V

    .line 219
    sput p4, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    .line 220
    sput p5, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    .line 221
    sget-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPath:Landroid/graphics/Path;

    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mScale:F

    mul-float p0, p0, v1

    mul-float v2, p1, v1

    mul-float v3, p2, v1

    mul-float v4, p3, v1

    mul-float v5, p4, v1

    mul-float v6, p5, v1

    move v1, p0

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    return-void
.end method

.method private static curve(FFFFFF)V
    .locals 2

    .line 207
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    add-float/2addr p0, v0

    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    add-float/2addr p1, v1

    add-float/2addr p2, v0

    add-float/2addr p3, v1

    add-float/2addr p4, v0

    add-float/2addr p5, v1

    invoke-static/range {p0 .. p5}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->curveTo(FFFFFF)V

    return-void
.end method

.method private static curveTo(FFFFFF)V
    .locals 0

    .line 212
    sput p2, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotX:F

    .line 213
    sput p3, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotY:F

    .line 214
    invoke-static/range {p0 .. p5}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->cubicTo(FFFFFF)V

    return-void
.end method

.method private static is_absolute(C)Z
    .locals 0

    .line 475
    invoke-static {p0}, Ljava/lang/Character;->isUpperCase(C)Z

    move-result p0

    return p0
.end method

.method private static is_cmd(C)Z
    .locals 0

    sparse-switch p0, :sswitch_data_0

    const/4 p0, 0x0

    return p0

    :sswitch_0
    const/4 p0, 0x1

    return p0

    nop

    :sswitch_data_0
    .sparse-switch
        0x41 -> :sswitch_0
        0x43 -> :sswitch_0
        0x48 -> :sswitch_0
        0x4c -> :sswitch_0
        0x4d -> :sswitch_0
        0x51 -> :sswitch_0
        0x53 -> :sswitch_0
        0x54 -> :sswitch_0
        0x56 -> :sswitch_0
        0x5a -> :sswitch_0
        0x61 -> :sswitch_0
        0x63 -> :sswitch_0
        0x68 -> :sswitch_0
        0x6c -> :sswitch_0
        0x6d -> :sswitch_0
        0x71 -> :sswitch_0
        0x73 -> :sswitch_0
        0x74 -> :sswitch_0
        0x76 -> :sswitch_0
        0x7a -> :sswitch_0
    .end sparse-switch
.end method

.method private static is_number_start(C)Z
    .locals 1

    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-le p0, v0, :cond_2

    :cond_0
    const/16 v0, 0x2e

    if-eq p0, v0, :cond_2

    const/16 v0, 0x2d

    if-eq p0, v0, :cond_2

    const/16 v0, 0x2b

    if-ne p0, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static line(FF)V
    .locals 1

    .line 195
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    add-float/2addr p0, v0

    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    add-float/2addr p1, v0

    invoke-static {p0, p1}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->lineTo(FF)V

    return-void
.end method

.method private static lineTo(FF)V
    .locals 2

    .line 200
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->setPenDown()V

    .line 201
    sput p0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    sput p0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotX:F

    .line 202
    sput p1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    sput p1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotY:F

    .line 203
    sget-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPath:Landroid/graphics/Path;

    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mScale:F

    mul-float p0, p0, v1

    mul-float p1, p1, v1

    invoke-virtual {v0, p0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    return-void
.end method

.method private static move(FF)V
    .locals 1

    .line 184
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    add-float/2addr p0, v0

    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    add-float/2addr p1, v0

    invoke-static {p0, p1}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->moveTo(FF)V

    return-void
.end method

.method private static moveTo(FF)V
    .locals 2

    .line 189
    sput p0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    sput p0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotX:F

    sput p0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenDownX:F

    .line 190
    sput p1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    sput p1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotY:F

    sput p1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenDownY:F

    .line 191
    sget-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPath:Landroid/graphics/Path;

    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mScale:F

    mul-float p0, p0, v1

    mul-float p1, p1, v1

    invoke-virtual {v0, p0, p1}, Landroid/graphics/Path;->moveTo(FF)V

    return-void
.end method

.method static parse(Ljava/lang/String;)Landroid/graphics/Path;
    .locals 23

    .line 24
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPath:Landroid/graphics/Path;

    .line 25
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->l:I

    .line 26
    sput-object p0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->s:Ljava/lang/String;

    const/4 v0, 0x0

    .line 27
    sput v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    const/4 v1, 0x0

    .line 29
    sput v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    .line 30
    sput v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    .line 31
    sput v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotX:F

    .line 32
    sput v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotY:F

    .line 33
    sput v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenDownX:F

    .line 34
    sput v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenDownY:F

    .line 35
    sput-boolean v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenDown:Z

    const/16 v2, 0x20

    const/16 v3, 0x20

    .line 37
    :cond_0
    :goto_0
    sget v4, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    sget v5, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->l:I

    if-ge v4, v5, :cond_c

    .line 38
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->skip_spaces()V

    .line 40
    sget v4, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    sget v5, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->l:I

    if-lt v4, v5, :cond_1

    goto/16 :goto_5

    :cond_1
    const/4 v4, 0x1

    if-eq v3, v2, :cond_2

    const/4 v5, 0x1

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    .line 45
    :goto_1
    sget-object v6, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->s:Ljava/lang/String;

    sget v7, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x6d

    const/16 v8, 0x4d

    const-string v9, "UnexpectedData"

    if-nez v5, :cond_4

    if-eq v6, v8, :cond_4

    if-ne v6, v7, :cond_3

    goto :goto_2

    .line 49
    :cond_3
    new-instance v0, Ljava/lang/Error;

    invoke-direct {v0, v9}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_4
    :goto_2
    invoke-static {v6}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->is_cmd(C)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 58
    sget v3, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    add-int/2addr v3, v4

    sput v3, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    move v3, v6

    :cond_5
    const/4 v4, 0x0

    goto :goto_3

    .line 59
    :cond_6
    invoke-static {v6}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->is_number_start(C)Z

    move-result v6

    if-eqz v6, :cond_b

    if-eqz v5, :cond_b

    const/16 v5, 0x5a

    if-eq v3, v5, :cond_a

    const/16 v5, 0x7a

    if-eq v3, v5, :cond_a

    if-eq v3, v8, :cond_7

    if-ne v3, v7, :cond_5

    .line 70
    :cond_7
    invoke-static {v3}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->is_absolute(C)Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x4c

    goto :goto_3

    :cond_8
    const/16 v3, 0x6c

    .line 83
    :goto_3
    invoke-static {v3}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->is_absolute(C)Z

    move-result v5

    sparse-switch v3, :sswitch_data_0

    .line 163
    new-instance v0, Ljava/lang/Error;

    invoke-direct {v0, v9}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :sswitch_0
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v6

    invoke-static {v1, v6}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->line(FF)V

    goto/16 :goto_4

    .line 142
    :sswitch_1
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v6

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v9

    invoke-static {v6, v9}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->smoothQuadraticBezierCurve(FF)V

    goto/16 :goto_4

    .line 126
    :sswitch_2
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v6

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v9

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v10

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v11

    invoke-static {v6, v9, v10, v11}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->smoothCurve(FFFF)V

    goto/16 :goto_4

    .line 134
    :sswitch_3
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v6

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v9

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v10

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v11

    invoke-static {v6, v9, v10, v11}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->quadraticBezierCurve(FFFF)V

    goto/16 :goto_4

    .line 86
    :sswitch_4
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v6

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v9

    invoke-static {v6, v9}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->move(FF)V

    goto/16 :goto_4

    .line 94
    :sswitch_5
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v6

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v9

    invoke-static {v6, v9}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->line(FF)V

    goto/16 :goto_4

    .line 102
    :sswitch_6
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v6

    invoke-static {v6, v1}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->line(FF)V

    goto/16 :goto_4

    .line 118
    :sswitch_7
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v9

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v10

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v11

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v12

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v13

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v14

    invoke-static/range {v9 .. v14}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->curve(FFFFFF)V

    goto/16 :goto_4

    .line 150
    :sswitch_8
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v15

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v16

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v17

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_flag()Z

    move-result v18

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_flag()Z

    move-result v19

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v20

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v21

    invoke-static/range {v15 .. v21}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->arc(FFFZZFF)V

    goto/16 :goto_4

    .line 159
    :sswitch_9
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->close()V

    goto/16 :goto_4

    .line 114
    :sswitch_a
    sget v6, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v9

    invoke-static {v6, v9}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->lineTo(FF)V

    goto/16 :goto_4

    .line 146
    :sswitch_b
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v6

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v9

    invoke-static {v6, v9}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->smoothQuadraticBezierCurveTo(FF)V

    goto/16 :goto_4

    .line 130
    :sswitch_c
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v6

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v9

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v10

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v11

    invoke-static {v6, v9, v10, v11}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->smoothCurveTo(FFFF)V

    goto :goto_4

    .line 138
    :sswitch_d
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v6

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v9

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v10

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v11

    invoke-static {v6, v9, v10, v11}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->quadraticBezierCurveTo(FFFF)V

    goto :goto_4

    .line 90
    :sswitch_e
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v6

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v9

    invoke-static {v6, v9}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->moveTo(FF)V

    goto :goto_4

    .line 98
    :sswitch_f
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v6

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v9

    invoke-static {v6, v9}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->lineTo(FF)V

    goto :goto_4

    .line 106
    :sswitch_10
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v6

    sget v9, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    invoke-static {v6, v9}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->lineTo(FF)V

    goto :goto_4

    .line 122
    :sswitch_11
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v10

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v11

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v12

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v13

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v14

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v15

    invoke-static/range {v10 .. v15}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->curveTo(FFFFFF)V

    goto :goto_4

    .line 154
    :sswitch_12
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v16

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v17

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v18

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_flag()Z

    move-result v19

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_flag()Z

    move-result v20

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v21

    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_number()F

    move-result v22

    invoke-static/range {v16 .. v22}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->arcTo(FFFZZFF)V

    :goto_4
    if-eqz v4, :cond_0

    if-eqz v5, :cond_9

    const/16 v3, 0x4d

    goto/16 :goto_0

    :cond_9
    const/16 v3, 0x6d

    goto/16 :goto_0

    .line 62
    :cond_a
    new-instance v0, Ljava/lang/Error;

    invoke-direct {v0, v9}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_b
    new-instance v0, Ljava/lang/Error;

    invoke-direct {v0, v9}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :cond_c
    :goto_5
    sget-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPath:Landroid/graphics/Path;

    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x41 -> :sswitch_12
        0x43 -> :sswitch_11
        0x48 -> :sswitch_10
        0x4c -> :sswitch_f
        0x4d -> :sswitch_e
        0x51 -> :sswitch_d
        0x53 -> :sswitch_c
        0x54 -> :sswitch_b
        0x56 -> :sswitch_a
        0x5a -> :sswitch_9
        0x61 -> :sswitch_8
        0x63 -> :sswitch_7
        0x68 -> :sswitch_6
        0x6c -> :sswitch_5
        0x6d -> :sswitch_4
        0x71 -> :sswitch_3
        0x73 -> :sswitch_2
        0x74 -> :sswitch_1
        0x76 -> :sswitch_0
        0x7a -> :sswitch_9
    .end sparse-switch
.end method

.method private static parse_flag()Z
    .locals 5

    .line 481
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->skip_spaces()V

    .line 483
    sget-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->s:Ljava/lang/String;

    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x30

    const/16 v2, 0x31

    if-eq v0, v1, :cond_1

    if-ne v0, v2, :cond_0

    goto :goto_0

    .line 495
    :cond_0
    new-instance v0, Ljava/lang/Error;

    const-string v1, "UnexpectedData"

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    .line 487
    :cond_1
    :goto_0
    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    const/4 v3, 0x1

    add-int/2addr v1, v3

    sput v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    .line 488
    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    sget v4, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->l:I

    if-ge v1, v4, :cond_2

    sget-object v4, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->s:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v4, 0x2c

    if-ne v1, v4, :cond_2

    .line 489
    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    add-int/2addr v1, v3

    sput v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    .line 491
    :cond_2
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->skip_spaces()V

    if-ne v0, v2, :cond_3

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    :goto_1
    return v3
.end method

.method private static parse_list_number()F
    .locals 2

    .line 502
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->l:I

    if-eq v0, v1, :cond_0

    .line 506
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_number()F

    move-result v0

    .line 507
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->skip_spaces()V

    .line 508
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->parse_list_separator()V

    return v0

    .line 503
    :cond_0
    new-instance v0, Ljava/lang/Error;

    const-string v1, "UnexpectedEnd"

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static parse_list_separator()V
    .locals 2

    .line 580
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->l:I

    if-ge v0, v1, :cond_0

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->s:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2c

    if-ne v0, v1, :cond_0

    .line 581
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    :cond_0
    return-void
.end method

.method private static parse_number()F
    .locals 10

    .line 515
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->skip_spaces()V

    .line 517
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->l:I

    const-string v2, "InvalidNumber"

    if-eq v0, v1, :cond_c

    .line 523
    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->s:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v3, 0x2b

    const/16 v4, 0x2d

    if-eq v1, v4, :cond_0

    if-ne v1, v3, :cond_1

    .line 527
    :cond_0
    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    .line 528
    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->s:Ljava/lang/String;

    sget v5, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    invoke-virtual {v1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v1

    :cond_1
    const/16 v5, 0x39

    const/16 v6, 0x2e

    const/16 v7, 0x30

    if-lt v1, v7, :cond_2

    if-gt v1, v5, :cond_2

    .line 533
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->skip_digits()V

    .line 534
    sget v8, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    sget v9, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->l:I

    if-ge v8, v9, :cond_3

    .line 535
    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->s:Ljava/lang/String;

    invoke-virtual {v1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v1

    goto :goto_0

    :cond_2
    if-ne v1, v6, :cond_b

    :cond_3
    :goto_0
    if-ne v1, v6, :cond_4

    .line 543
    sget v6, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    add-int/lit8 v6, v6, 0x1

    sput v6, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    .line 544
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->skip_digits()V

    .line 545
    sget v6, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    sget v8, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->l:I

    if-ge v6, v8, :cond_4

    .line 546
    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->s:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    :cond_4
    const/16 v6, 0x65

    if-eq v1, v6, :cond_5

    const/16 v6, 0x45

    if-ne v1, v6, :cond_9

    .line 550
    :cond_5
    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    add-int/lit8 v6, v1, 0x1

    sget v8, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->l:I

    if-ge v6, v8, :cond_9

    .line 551
    sget-object v6, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->s:Ljava/lang/String;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v6, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v6, 0x6d

    if-eq v1, v6, :cond_9

    const/16 v6, 0x78

    if-eq v1, v6, :cond_9

    .line 554
    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    .line 555
    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->s:Ljava/lang/String;

    sget v6, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    invoke-virtual {v1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-eq v1, v3, :cond_8

    if-ne v1, v4, :cond_6

    goto :goto_1

    :cond_6
    if-lt v1, v7, :cond_7

    if-gt v1, v5, :cond_7

    .line 561
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->skip_digits()V

    goto :goto_2

    .line 563
    :cond_7
    new-instance v0, Ljava/lang/Error;

    invoke-direct {v0, v2}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    .line 558
    :cond_8
    :goto_1
    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    .line 559
    invoke-static {}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->skip_digits()V

    .line 568
    :cond_9
    :goto_2
    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->s:Ljava/lang/String;

    sget v3, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    invoke-virtual {v1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 569
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 572
    invoke-static {v0}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v1

    if-nez v1, :cond_a

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_a

    return v0

    .line 573
    :cond_a
    new-instance v0, Ljava/lang/Error;

    invoke-direct {v0, v2}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    .line 538
    :cond_b
    new-instance v0, Ljava/lang/Error;

    invoke-direct {v0, v2}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    .line 518
    :cond_c
    new-instance v0, Ljava/lang/Error;

    invoke-direct {v0, v2}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static quadraticBezierCurve(FFFF)V
    .locals 2

    .line 240
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    add-float/2addr p0, v0

    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    add-float/2addr p1, v1

    add-float/2addr p2, v0

    add-float/2addr p3, v1

    invoke-static {p0, p1, p2, p3}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->quadraticBezierCurveTo(FFFF)V

    return-void
.end method

.method private static quadraticBezierCurveTo(FFFF)V
    .locals 9

    .line 245
    sput p0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotX:F

    .line 246
    sput p1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotY:F

    const/high16 v0, 0x40000000    # 2.0f

    mul-float p0, p0, v0

    add-float v1, p2, p0

    const/high16 v2, 0x40400000    # 3.0f

    div-float v5, v1, v2

    mul-float p1, p1, v0

    add-float v0, p3, p1

    div-float v6, v0, v2

    .line 251
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    add-float/2addr v0, p0

    div-float v3, v0, v2

    .line 252
    sget p0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    add-float/2addr p0, p1

    div-float v4, p0, v2

    move v7, p2

    move v8, p3

    .line 253
    invoke-static/range {v3 .. v8}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->cubicTo(FFFFFF)V

    return-void
.end method

.method private static round(D)D
    .locals 4

    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    .line 435
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    mul-double p0, p0, v0

    .line 436
    invoke-static {p0, p1}, Ljava/lang/Math;->round(D)J

    move-result-wide p0

    long-to-double p0, p0

    div-double/2addr p0, v0

    return-wide p0
.end method

.method private static setPenDown()V
    .locals 1

    .line 427
    sget-boolean v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenDown:Z

    if-nez v0, :cond_0

    .line 428
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    sput v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenDownX:F

    .line 429
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    sput v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenDownY:F

    const/4 v0, 0x1

    .line 430
    sput-boolean v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenDown:Z

    :cond_0
    return-void
.end method

.method private static skip_digits()V
    .locals 2

    .line 586
    :goto_0
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->l:I

    if-ge v0, v1, :cond_0

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->s:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static skip_spaces()V
    .locals 2

    .line 440
    :goto_0
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->l:I

    if-ge v0, v1, :cond_0

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->s:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->i:I

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static smoothCurve(FFFF)V
    .locals 2

    .line 225
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    add-float/2addr p0, v0

    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    add-float/2addr p1, v1

    add-float/2addr p2, v0

    add-float/2addr p3, v1

    invoke-static {p0, p1, p2, p3}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->smoothCurveTo(FFFF)V

    return-void
.end method

.method private static smoothCurveTo(FFFF)V
    .locals 9

    .line 232
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    const/high16 v1, 0x40000000    # 2.0f

    mul-float v0, v0, v1

    sget v2, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotX:F

    sub-float v3, v0, v2

    .line 233
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    mul-float v0, v0, v1

    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotY:F

    sub-float v4, v0, v1

    .line 234
    sput p0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotX:F

    .line 235
    sput p1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotY:F

    move v5, p0

    move v6, p1

    move v7, p2

    move v8, p3

    .line 236
    invoke-static/range {v3 .. v8}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->cubicTo(FFFFFF)V

    return-void
.end method

.method private static smoothQuadraticBezierCurve(FF)V
    .locals 1

    .line 257
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    add-float/2addr p0, v0

    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    add-float/2addr p1, v0

    invoke-static {p0, p1}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->smoothQuadraticBezierCurveTo(FF)V

    return-void
.end method

.method private static smoothQuadraticBezierCurveTo(FF)V
    .locals 3

    .line 264
    sget v0, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenX:F

    const/high16 v1, 0x40000000    # 2.0f

    mul-float v0, v0, v1

    sget v2, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotX:F

    sub-float/2addr v0, v2

    .line 265
    sget v2, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPenY:F

    mul-float v2, v2, v1

    sget v1, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->mPivotY:F

    sub-float/2addr v2, v1

    .line 266
    invoke-static {v0, v2, p0, p1}, Lversioned/host/exp/exponent/modules/api/components/svg/PathParser;->quadraticBezierCurveTo(FFFF)V

    return-void
.end method
