.class final enum Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;
.super Ljava/lang/Enum;
.source "RenderableViewManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "SVGClass"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGCircle:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGClipPath:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGDefs:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGEllipse:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGGroup:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGImage:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGLine:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGLinearGradient:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGMask:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGPath:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGPattern:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGRadialGradient:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGRect:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGSymbol:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGTSpan:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGText:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGTextPath:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

.field public static final enum RNSVGUse:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 55
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/4 v1, 0x0

    const-string v2, "RNSVGGroup"

    invoke-direct {v0, v2, v1}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGGroup:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    .line 56
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/4 v2, 0x1

    const-string v3, "RNSVGPath"

    invoke-direct {v0, v3, v2}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGPath:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    .line 57
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/4 v3, 0x2

    const-string v4, "RNSVGText"

    invoke-direct {v0, v4, v3}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGText:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    .line 58
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/4 v4, 0x3

    const-string v5, "RNSVGTSpan"

    invoke-direct {v0, v5, v4}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGTSpan:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    .line 59
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/4 v5, 0x4

    const-string v6, "RNSVGTextPath"

    invoke-direct {v0, v6, v5}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGTextPath:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    .line 60
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/4 v6, 0x5

    const-string v7, "RNSVGImage"

    invoke-direct {v0, v7, v6}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGImage:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    .line 61
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/4 v7, 0x6

    const-string v8, "RNSVGCircle"

    invoke-direct {v0, v8, v7}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGCircle:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    .line 62
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/4 v8, 0x7

    const-string v9, "RNSVGEllipse"

    invoke-direct {v0, v9, v8}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGEllipse:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    .line 63
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/16 v9, 0x8

    const-string v10, "RNSVGLine"

    invoke-direct {v0, v10, v9}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGLine:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    .line 64
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/16 v10, 0x9

    const-string v11, "RNSVGRect"

    invoke-direct {v0, v11, v10}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGRect:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    .line 65
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/16 v11, 0xa

    const-string v12, "RNSVGClipPath"

    invoke-direct {v0, v12, v11}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGClipPath:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    .line 66
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/16 v12, 0xb

    const-string v13, "RNSVGDefs"

    invoke-direct {v0, v13, v12}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGDefs:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    .line 67
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/16 v13, 0xc

    const-string v14, "RNSVGUse"

    invoke-direct {v0, v14, v13}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGUse:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    .line 68
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/16 v14, 0xd

    const-string v15, "RNSVGSymbol"

    invoke-direct {v0, v15, v14}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGSymbol:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    .line 69
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/16 v15, 0xe

    const-string v14, "RNSVGLinearGradient"

    invoke-direct {v0, v14, v15}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGLinearGradient:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    .line 70
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const-string v14, "RNSVGRadialGradient"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGRadialGradient:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    .line 71
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const-string v14, "RNSVGPattern"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGPattern:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    .line 72
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const-string v14, "RNSVGMask"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15}, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGMask:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/16 v0, 0x12

    .line 54
    new-array v0, v0, [Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    sget-object v14, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGGroup:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    aput-object v14, v0, v1

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGPath:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    aput-object v1, v0, v2

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGText:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    aput-object v1, v0, v3

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGTSpan:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    aput-object v1, v0, v4

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGTextPath:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    aput-object v1, v0, v5

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGImage:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    aput-object v1, v0, v6

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGCircle:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    aput-object v1, v0, v7

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGEllipse:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    aput-object v1, v0, v8

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGLine:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    aput-object v1, v0, v9

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGRect:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    aput-object v1, v0, v10

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGClipPath:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    aput-object v1, v0, v11

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGDefs:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    aput-object v1, v0, v12

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGUse:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    aput-object v1, v0, v13

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGSymbol:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGLinearGradient:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGRadialGradient:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGPattern:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->RNSVGMask:Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->$VALUES:[Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;
    .locals 1

    .line 54
    const-class v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    return-object p0
.end method

.method public static values()[Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;
    .locals 1

    .line 54
    sget-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->$VALUES:[Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    invoke-virtual {v0}, [Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lversioned/host/exp/exponent/modules/api/components/svg/RenderableViewManager$SVGClass;

    return-object v0
.end method
