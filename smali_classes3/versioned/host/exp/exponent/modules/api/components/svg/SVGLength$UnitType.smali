.class public final enum Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;
.super Ljava/lang/Enum;
.source "SVGLength.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UnitType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

.field public static final enum CM:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

.field public static final enum EMS:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

.field public static final enum EXS:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

.field public static final enum IN:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

.field public static final enum MM:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

.field public static final enum NUMBER:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

.field public static final enum PC:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

.field public static final enum PERCENTAGE:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

.field public static final enum PT:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

.field public static final enum PX:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

.field public static final enum UNKNOWN:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 11
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1}, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->UNKNOWN:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    .line 12
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    const/4 v2, 0x1

    const-string v3, "NUMBER"

    invoke-direct {v0, v3, v2}, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->NUMBER:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    .line 13
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    const/4 v3, 0x2

    const-string v4, "PERCENTAGE"

    invoke-direct {v0, v4, v3}, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->PERCENTAGE:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    .line 14
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    const/4 v4, 0x3

    const-string v5, "EMS"

    invoke-direct {v0, v5, v4}, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->EMS:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    .line 15
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    const/4 v5, 0x4

    const-string v6, "EXS"

    invoke-direct {v0, v6, v5}, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->EXS:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    .line 16
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    const/4 v6, 0x5

    const-string v7, "PX"

    invoke-direct {v0, v7, v6}, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->PX:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    .line 17
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    const/4 v7, 0x6

    const-string v8, "CM"

    invoke-direct {v0, v8, v7}, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->CM:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    .line 18
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    const/4 v8, 0x7

    const-string v9, "MM"

    invoke-direct {v0, v9, v8}, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->MM:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    .line 19
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    const/16 v9, 0x8

    const-string v10, "IN"

    invoke-direct {v0, v10, v9}, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->IN:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    .line 20
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    const/16 v10, 0x9

    const-string v11, "PT"

    invoke-direct {v0, v11, v10}, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->PT:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    .line 21
    new-instance v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    const/16 v11, 0xa

    const-string v12, "PC"

    invoke-direct {v0, v12, v11}, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->PC:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    const/16 v0, 0xb

    .line 10
    new-array v0, v0, [Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    sget-object v12, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->UNKNOWN:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    aput-object v12, v0, v1

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->NUMBER:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    aput-object v1, v0, v2

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->PERCENTAGE:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    aput-object v1, v0, v3

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->EMS:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    aput-object v1, v0, v4

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->EXS:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    aput-object v1, v0, v5

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->PX:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    aput-object v1, v0, v6

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->CM:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    aput-object v1, v0, v7

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->MM:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    aput-object v1, v0, v8

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->IN:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    aput-object v1, v0, v9

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->PT:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    aput-object v1, v0, v10

    sget-object v1, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->PC:Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    aput-object v1, v0, v11

    sput-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->$VALUES:[Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;
    .locals 1

    .line 10
    const-class v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    return-object p0
.end method

.method public static values()[Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;
    .locals 1

    .line 10
    sget-object v0, Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->$VALUES:[Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    invoke-virtual {v0}, [Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lversioned/host/exp/exponent/modules/api/components/svg/SVGLength$UnitType;

    return-object v0
.end method
