.class public Lversioned/host/exp/exponent/modules/api/ErrorRecoveryModule;
.super Lversioned/host/exp/exponent/modules/ExpoBaseModule;
.source "ErrorRecoveryModule.java"


# direct methods
.method public constructor <init>(Lcom/facebook/react/bridge/ReactApplicationContext;Lhost/exp/exponent/kernel/ExperienceId;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Lversioned/host/exp/exponent/modules/ExpoBaseModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;Lhost/exp/exponent/kernel/ExperienceId;)V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "ExponentErrorRecovery"

    return-object v0
.end method

.method public setRecoveryProps(Lcom/facebook/react/bridge/ReadableMap;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 28
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/api/ErrorRecoveryModule;->experienceId:Lhost/exp/exponent/kernel/ExperienceId;

    invoke-static {v0}, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->getInstance(Lhost/exp/exponent/kernel/ExperienceId;)Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;

    move-result-object v0

    invoke-static {p1}, Lversioned/host/exp/exponent/ReadableObjectUtils;->readableToJson(Lcom/facebook/react/bridge/ReadableMap;)Lorg/json/JSONObject;

    move-result-object p1

    invoke-virtual {v0, p1}, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->setRecoveryProps(Lorg/json/JSONObject;)V

    return-void
.end method
