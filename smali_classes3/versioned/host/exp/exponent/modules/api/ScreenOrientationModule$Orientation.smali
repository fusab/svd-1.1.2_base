.class public final enum Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;
.super Ljava/lang/Enum;
.source "ScreenOrientationModule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Orientation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

.field public static final enum LANDSCAPE:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

.field public static final enum LANDSCAPE_LEFT:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

.field public static final enum LANDSCAPE_RIGHT:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

.field public static final enum PORTRAIT:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

.field public static final enum PORTRAIT_DOWN:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

.field public static final enum PORTRAIT_UP:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

.field public static final enum UNKNOWN:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 215
    new-instance v0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    const/4 v1, 0x0

    const-string v2, "PORTRAIT"

    invoke-direct {v0, v2, v1}, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;->PORTRAIT:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    .line 216
    new-instance v0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    const/4 v2, 0x1

    const-string v3, "PORTRAIT_UP"

    invoke-direct {v0, v3, v2}, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;->PORTRAIT_UP:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    .line 217
    new-instance v0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    const/4 v3, 0x2

    const-string v4, "PORTRAIT_DOWN"

    invoke-direct {v0, v4, v3}, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;->PORTRAIT_DOWN:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    .line 218
    new-instance v0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    const/4 v4, 0x3

    const-string v5, "LANDSCAPE"

    invoke-direct {v0, v5, v4}, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;->LANDSCAPE:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    .line 219
    new-instance v0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    const/4 v5, 0x4

    const-string v6, "LANDSCAPE_LEFT"

    invoke-direct {v0, v6, v5}, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;->LANDSCAPE_LEFT:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    .line 220
    new-instance v0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    const/4 v6, 0x5

    const-string v7, "LANDSCAPE_RIGHT"

    invoke-direct {v0, v7, v6}, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;->LANDSCAPE_RIGHT:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    .line 221
    new-instance v0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    const/4 v7, 0x6

    const-string v8, "UNKNOWN"

    invoke-direct {v0, v8, v7}, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;->UNKNOWN:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    const/4 v0, 0x7

    .line 214
    new-array v0, v0, [Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    sget-object v8, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;->PORTRAIT:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    aput-object v8, v0, v1

    sget-object v1, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;->PORTRAIT_UP:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    aput-object v1, v0, v2

    sget-object v1, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;->PORTRAIT_DOWN:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    aput-object v1, v0, v3

    sget-object v1, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;->LANDSCAPE:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    aput-object v1, v0, v4

    sget-object v1, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;->LANDSCAPE_LEFT:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    aput-object v1, v0, v5

    sget-object v1, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;->LANDSCAPE_RIGHT:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    aput-object v1, v0, v6

    sget-object v1, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;->UNKNOWN:Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    aput-object v1, v0, v7

    sput-object v0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;->$VALUES:[Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 214
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;
    .locals 1

    .line 214
    const-class v0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    return-object p0
.end method

.method public static values()[Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;
    .locals 1

    .line 214
    sget-object v0, Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;->$VALUES:[Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    invoke-virtual {v0}, [Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lversioned/host/exp/exponent/modules/api/ScreenOrientationModule$Orientation;

    return-object v0
.end method
