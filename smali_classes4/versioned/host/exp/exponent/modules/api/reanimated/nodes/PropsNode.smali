.class public Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;
.super Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;
.source "PropsNode.java"

# interfaces
.implements Lversioned/host/exp/exponent/modules/api/reanimated/nodes/FinalNode;


# instance fields
.field private mConnectedViewTag:I

.field private final mDiffMap:Lcom/facebook/react/uimanager/ReactStylesDiffMap;

.field private final mMapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mPropMap:Lcom/facebook/react/bridge/JavaOnlyMap;

.field private final mUIImplementation:Lcom/facebook/react/uimanager/UIImplementation;


# direct methods
.method public constructor <init>(ILcom/facebook/react/bridge/ReadableMap;Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;Lcom/facebook/react/uimanager/UIImplementation;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2, p3}, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;-><init>(ILcom/facebook/react/bridge/ReadableMap;Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;)V

    const/4 p1, -0x1

    .line 23
    iput p1, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mConnectedViewTag:I

    const-string p1, "props"

    .line 34
    invoke-interface {p2, p1}, Lcom/facebook/react/bridge/ReadableMap;->getMap(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableMap;

    move-result-object p1

    invoke-static {p1}, Lversioned/host/exp/exponent/modules/api/reanimated/Utils;->processMapping(Lcom/facebook/react/bridge/ReadableMap;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mMapping:Ljava/util/Map;

    .line 35
    iput-object p4, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mUIImplementation:Lcom/facebook/react/uimanager/UIImplementation;

    .line 36
    new-instance p1, Lcom/facebook/react/bridge/JavaOnlyMap;

    invoke-direct {p1}, Lcom/facebook/react/bridge/JavaOnlyMap;-><init>()V

    iput-object p1, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mPropMap:Lcom/facebook/react/bridge/JavaOnlyMap;

    .line 37
    new-instance p1, Lcom/facebook/react/uimanager/ReactStylesDiffMap;

    iget-object p2, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mPropMap:Lcom/facebook/react/bridge/JavaOnlyMap;

    invoke-direct {p1, p2}, Lcom/facebook/react/uimanager/ReactStylesDiffMap;-><init>(Lcom/facebook/react/bridge/ReadableMap;)V

    iput-object p1, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mDiffMap:Lcom/facebook/react/uimanager/ReactStylesDiffMap;

    return-void
.end method


# virtual methods
.method public connectToView(I)V
    .locals 0

    .line 41
    iput p1, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mConnectedViewTag:I

    .line 42
    invoke-virtual {p0}, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->dangerouslyRescheduleEvaluate()V

    return-void
.end method

.method public disconnectFromView(I)V
    .locals 0

    const/4 p1, -0x1

    .line 46
    iput p1, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mConnectedViewTag:I

    return-void
.end method

.method protected evaluate()Ljava/lang/Double;
    .locals 14

    .line 54
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v0

    .line 55
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v1

    .line 57
    iget-object v2, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mMapping:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 58
    iget-object v7, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mNodesManager:Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    const-class v9, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;

    invoke-virtual {v7, v8, v9}, Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;->findNodeById(ILjava/lang/Class;)Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;

    move-result-object v7

    .line 59
    instance-of v8, v7, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/StyleNode;

    const/4 v9, 0x1

    if-eqz v8, :cond_6

    .line 60
    invoke-virtual {v7}, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->value()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/react/bridge/WritableMap;

    .line 61
    invoke-interface {v6}, Lcom/facebook/react/bridge/WritableMap;->keySetIterator()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;

    move-result-object v7

    .line 62
    :goto_1
    invoke-interface {v7}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->hasNextKey()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 63
    invoke-interface {v7}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->nextKey()Ljava/lang/String;

    move-result-object v8

    .line 65
    iget-object v10, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mNodesManager:Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;

    iget-object v10, v10, Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;->uiProps:Ljava/util/Set;

    invoke-interface {v10, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 67
    iget-object v3, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mPropMap:Lcom/facebook/react/bridge/JavaOnlyMap;

    const/4 v10, 0x1

    goto :goto_2

    .line 68
    :cond_1
    iget-object v10, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mNodesManager:Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;

    iget-object v10, v10, Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;->nativeProps:Ljava/util/Set;

    invoke-interface {v10, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    move v10, v3

    const/4 v4, 0x1

    move-object v3, v1

    goto :goto_2

    :cond_2
    move v10, v3

    const/4 v5, 0x1

    move-object v3, v0

    .line 75
    :goto_2
    invoke-interface {v6, v8}, Lcom/facebook/react/bridge/WritableMap;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v11

    .line 76
    sget-object v12, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode$1;->$SwitchMap$com$facebook$react$bridge$ReadableType:[I

    invoke-virtual {v11}, Lcom/facebook/react/bridge/ReadableType;->ordinal()I

    move-result v13

    aget v12, v12, v13

    if-eq v12, v9, :cond_5

    const/4 v13, 0x2

    if-eq v12, v13, :cond_4

    const/4 v13, 0x3

    if-ne v12, v13, :cond_3

    .line 84
    invoke-interface {v6, v8}, Lcom/facebook/react/bridge/WritableMap;->getArray(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableArray;

    move-result-object v11

    check-cast v11, Lcom/facebook/react/bridge/WritableArray;

    invoke-interface {v3, v8, v11}, Lcom/facebook/react/bridge/WritableMap;->putArray(Ljava/lang/String;Lcom/facebook/react/bridge/WritableArray;)V

    goto :goto_3

    .line 87
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_4
    invoke-interface {v6, v8}, Lcom/facebook/react/bridge/WritableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v3, v8, v11}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 78
    :cond_5
    invoke-interface {v6, v8}, Lcom/facebook/react/bridge/WritableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v11

    invoke-interface {v3, v8, v11, v12}, Lcom/facebook/react/bridge/WritableMap;->putDouble(Ljava/lang/String;D)V

    :goto_3
    move v3, v10

    goto :goto_1

    .line 91
    :cond_6
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 92
    iget-object v8, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mNodesManager:Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;

    iget-object v8, v8, Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;->uiProps:Ljava/util/Set;

    invoke-interface {v8, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 94
    iget-object v3, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mPropMap:Lcom/facebook/react/bridge/JavaOnlyMap;

    invoke-virtual {v7}, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->doubleValue()Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    invoke-virtual {v3, v6, v7, v8}, Lcom/facebook/react/bridge/JavaOnlyMap;->putDouble(Ljava/lang/String;D)V

    const/4 v3, 0x1

    goto/16 :goto_0

    .line 97
    :cond_7
    invoke-virtual {v7}, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->doubleValue()Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    invoke-interface {v1, v6, v7, v8}, Lcom/facebook/react/bridge/WritableMap;->putDouble(Ljava/lang/String;D)V

    const/4 v4, 0x1

    goto/16 :goto_0

    .line 102
    :cond_8
    iget v2, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mConnectedViewTag:I

    const/4 v6, -0x1

    if-eq v2, v6, :cond_b

    if-eqz v3, :cond_9

    .line 104
    iget-object v3, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mUIImplementation:Lcom/facebook/react/uimanager/UIImplementation;

    iget-object v6, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mDiffMap:Lcom/facebook/react/uimanager/ReactStylesDiffMap;

    invoke-virtual {v3, v2, v6}, Lcom/facebook/react/uimanager/UIImplementation;->synchronouslyUpdateViewOnUIThread(ILcom/facebook/react/uimanager/ReactStylesDiffMap;)V

    :cond_9
    if-eqz v4, :cond_a

    .line 109
    iget-object v2, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mNodesManager:Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;

    iget v3, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mConnectedViewTag:I

    invoke-virtual {v2, v3, v1}, Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;->enqueueUpdateViewOnNativeThread(ILcom/facebook/react/bridge/WritableMap;)V

    :cond_a
    if-eqz v5, :cond_b

    .line 112
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v1

    .line 113
    iget v2, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mConnectedViewTag:I

    const-string v3, "viewTag"

    invoke-interface {v1, v3, v2}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    const-string v2, "props"

    .line 114
    invoke-interface {v1, v2, v0}, Lcom/facebook/react/bridge/WritableMap;->putMap(Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    .line 115
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mNodesManager:Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;

    const-string v2, "onReanimatedPropsChange"

    invoke-virtual {v0, v2, v1}, Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;->sendEvent(Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    .line 119
    :cond_b
    sget-object v0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->ZERO:Ljava/lang/Double;

    return-object v0
.end method

.method protected bridge synthetic evaluate()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->evaluate()Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public update()V
    .locals 2

    .line 127
    iget v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->mConnectedViewTag:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    return-void

    .line 132
    :cond_0
    invoke-virtual {p0}, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/PropsNode;->value()Ljava/lang/Object;

    return-void
.end method
