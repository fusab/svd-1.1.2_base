.class public abstract Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;
.super Ljava/lang/Object;
.source "Node.java"


# static fields
.field public static final ONE:Ljava/lang/Double;

.field public static final ZERO:Ljava/lang/Double;


# instance fields
.field private mChildren:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private mLastLoopID:J

.field private mMemoizedValue:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field protected final mNodeID:I

.field protected final mNodesManager:Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;

.field private final mUpdateContext:Lversioned/host/exp/exponent/modules/api/reanimated/UpdateContext;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, 0x0

    .line 18
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->ZERO:Ljava/lang/Double;

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 19
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->ONE:Ljava/lang/Double;

    return-void
.end method

.method public constructor <init>(ILcom/facebook/react/bridge/ReadableMap;Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;)V
    .locals 2
    .param p2    # Lcom/facebook/react/bridge/ReadableMap;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    .line 26
    iput-wide v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mLastLoopID:J

    .line 31
    iput p1, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mNodeID:I

    .line 32
    iput-object p3, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mNodesManager:Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;

    .line 33
    iget-object p1, p3, Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;->updateContext:Lversioned/host/exp/exponent/modules/api/reanimated/UpdateContext;

    iput-object p1, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mUpdateContext:Lversioned/host/exp/exponent/modules/api/reanimated/UpdateContext;

    return-void
.end method

.method private static findAndUpdateNodes(Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;Ljava/util/Set;Ljava/util/Stack;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;",
            "Ljava/util/Set<",
            "Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;",
            ">;",
            "Ljava/util/Stack<",
            "Lversioned/host/exp/exponent/modules/api/reanimated/nodes/FinalNode;",
            ">;)V"
        }
    .end annotation

    .line 96
    invoke-interface {p1, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 99
    :cond_0
    invoke-interface {p1, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 102
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mChildren:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 104
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;

    .line 105
    invoke-static {v1, p1, p2}, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->findAndUpdateNodes(Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;Ljava/util/Set;Ljava/util/Stack;)V

    goto :goto_0

    .line 108
    :cond_1
    instance-of p1, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/FinalNode;

    if-eqz p1, :cond_2

    .line 109
    check-cast p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/FinalNode;

    invoke-virtual {p2, p0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-void
.end method

.method public static runUpdates(Lversioned/host/exp/exponent/modules/api/reanimated/UpdateContext;)V
    .locals 5

    .line 114
    invoke-static {}, Lcom/facebook/react/bridge/UiThreadUtil;->assertOnUiThread()V

    .line 115
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/UpdateContext;->updatedNodes:Ljava/util/ArrayList;

    .line 116
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 117
    new-instance v2, Ljava/util/Stack;

    invoke-direct {v2}, Ljava/util/Stack;-><init>()V

    const/4 v3, 0x0

    .line 118
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 119
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;

    invoke-static {v4, v1, v2}, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->findAndUpdateNodes(Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;Ljava/util/Set;Ljava/util/Stack;)V

    .line 120
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_0

    .line 121
    :goto_1
    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 122
    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/FinalNode;

    invoke-interface {v4}, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/FinalNode;->update()V

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 126
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 127
    iget-wide v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/UpdateContext;->updateLoopID:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/UpdateContext;->updateLoopID:J

    return-void
.end method


# virtual methods
.method public addChild(Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;)V
    .locals 1

    .line 66
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mChildren:Ljava/util/List;

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mChildren:Ljava/util/List;

    .line 69
    :cond_0
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mChildren:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    invoke-virtual {p0}, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->dangerouslyRescheduleEvaluate()V

    return-void
.end method

.method protected final dangerouslyRescheduleEvaluate()V
    .locals 2

    const-wide/16 v0, -0x1

    .line 86
    iput-wide v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mLastLoopID:J

    .line 87
    invoke-virtual {p0}, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->markUpdated()V

    return-void
.end method

.method public final doubleValue()Ljava/lang/Double;
    .locals 3

    .line 52
    invoke-virtual {p0}, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->value()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 54
    sget-object v0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->ZERO:Ljava/lang/Double;

    return-object v0

    .line 55
    :cond_0
    instance-of v1, v0, Ljava/lang/Double;

    if-eqz v1, :cond_1

    .line 56
    check-cast v0, Ljava/lang/Double;

    return-object v0

    .line 57
    :cond_1
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_2

    .line 58
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0

    .line 59
    :cond_2
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 60
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->ONE:Ljava/lang/Double;

    goto :goto_0

    :cond_3
    sget-object v0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->ZERO:Ljava/lang/Double;

    :goto_0
    return-object v0

    .line 62
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Value of node "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " cannot be cast to a number"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected abstract evaluate()Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method

.method protected final forceUpdateMemoizedValue(Ljava/lang/Object;)V
    .locals 0

    .line 91
    iput-object p1, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mMemoizedValue:Ljava/lang/Object;

    .line 92
    invoke-virtual {p0}, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->markUpdated()V

    return-void
.end method

.method protected markUpdated()V
    .locals 1

    .line 80
    invoke-static {}, Lcom/facebook/react/bridge/UiThreadUtil;->assertOnUiThread()V

    .line 81
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mUpdateContext:Lversioned/host/exp/exponent/modules/api/reanimated/UpdateContext;

    iget-object v0, v0, Lversioned/host/exp/exponent/modules/api/reanimated/UpdateContext;->updatedNodes:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mNodesManager:Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;

    invoke-virtual {v0}, Lversioned/host/exp/exponent/modules/api/reanimated/NodesManager;->postRunUpdatesAfterAnimation()V

    return-void
.end method

.method public removeChild(Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;)V
    .locals 1

    .line 74
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mChildren:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 75
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final value()Ljava/lang/Object;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .line 39
    iget-wide v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mLastLoopID:J

    iget-object v2, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mUpdateContext:Lversioned/host/exp/exponent/modules/api/reanimated/UpdateContext;

    iget-wide v2, v2, Lversioned/host/exp/exponent/modules/api/reanimated/UpdateContext;->updateLoopID:J

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    .line 40
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mUpdateContext:Lversioned/host/exp/exponent/modules/api/reanimated/UpdateContext;

    iget-wide v0, v0, Lversioned/host/exp/exponent/modules/api/reanimated/UpdateContext;->updateLoopID:J

    iput-wide v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mLastLoopID:J

    .line 41
    invoke-virtual {p0}, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->evaluate()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mMemoizedValue:Ljava/lang/Object;

    return-object v0

    .line 43
    :cond_0
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/api/reanimated/nodes/Node;->mMemoizedValue:Ljava/lang/Object;

    return-object v0
.end method
