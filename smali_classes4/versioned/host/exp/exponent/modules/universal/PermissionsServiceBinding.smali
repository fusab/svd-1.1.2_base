.class Lversioned/host/exp/exponent/modules/universal/PermissionsServiceBinding;
.super Lexpo/modules/permissions/PermissionsService;
.source "PermissionsServiceBinding.java"


# instance fields
.field private mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

.field protected mKernelServiceRegistry:Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Lhost/exp/exponent/kernel/ExperienceId;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lexpo/modules/permissions/PermissionsService;-><init>(Landroid/content/Context;)V

    .line 23
    iput-object p2, p0, Lversioned/host/exp/exponent/modules/universal/PermissionsServiceBinding;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    .line 24
    invoke-static {}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->getInstance()Lhost/exp/exponent/di/NativeModuleDepsProvider;

    move-result-object p1

    const-class p2, Lversioned/host/exp/exponent/modules/universal/PermissionsServiceBinding;

    invoke-virtual {p1, p2, p0}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->inject(Ljava/lang/Class;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getPermission(Ljava/lang/String;)I
    .locals 3

    .line 31
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x0

    const/16 v2, 0x17

    if-lt v0, v2, :cond_0

    .line 32
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/universal/PermissionsServiceBinding;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Landroidx/core/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 35
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/universal/PermissionsServiceBinding;->mKernelServiceRegistry:Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;

    .line 36
    invoke-virtual {v0}, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->getPermissionsKernelService()Lhost/exp/exponent/kernel/services/PermissionsKernelService;

    move-result-object v0

    iget-object v2, p0, Lversioned/host/exp/exponent/modules/universal/PermissionsServiceBinding;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    invoke-virtual {v0, p1, v2}, Lhost/exp/exponent/kernel/services/PermissionsKernelService;->hasGrantedPermissions(Ljava/lang/String;Lhost/exp/exponent/kernel/ExperienceId;)Z

    move-result p1

    if-eqz p1, :cond_1

    return v1

    :cond_1
    const/4 p1, -0x1

    return p1
.end method
