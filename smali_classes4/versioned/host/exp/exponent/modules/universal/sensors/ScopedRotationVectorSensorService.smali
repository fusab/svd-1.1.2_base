.class public Lversioned/host/exp/exponent/modules/universal/sensors/ScopedRotationVectorSensorService;
.super Lversioned/host/exp/exponent/modules/universal/sensors/BaseSensorService;
.source "ScopedRotationVectorSensorService.java"

# interfaces
.implements Lorg/unimodules/core/interfaces/InternalModule;
.implements Lorg/unimodules/interfaces/sensors/services/RotationVectorSensorService;


# direct methods
.method public constructor <init>(Lhost/exp/exponent/kernel/ExperienceId;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lversioned/host/exp/exponent/modules/universal/sensors/BaseSensorService;-><init>(Lhost/exp/exponent/kernel/ExperienceId;)V

    return-void
.end method


# virtual methods
.method public getExportedInterfaces()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation

    .line 23
    const-class v0, Lorg/unimodules/interfaces/sensors/services/RotationVectorSensorService;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getSensorKernelService()Lhost/exp/exponent/kernel/services/sensors/SubscribableSensorKernelService;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lversioned/host/exp/exponent/modules/universal/sensors/ScopedRotationVectorSensorService;->getKernelServiceRegistry()Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;

    move-result-object v0

    invoke-virtual {v0}, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->getRotationVectorSensorKernelService()Lhost/exp/exponent/kernel/services/sensors/RotationVectorSensorKernelService;

    move-result-object v0

    return-object v0
.end method

.method public synthetic onCreate(Lorg/unimodules/core/ModuleRegistry;)V
    .locals 0

    invoke-static {p0, p1}, Lorg/unimodules/core/interfaces/RegistryLifecycleListener$-CC;->$default$onCreate(Lorg/unimodules/core/interfaces/RegistryLifecycleListener;Lorg/unimodules/core/ModuleRegistry;)V

    return-void
.end method

.method public synthetic onDestroy()V
    .locals 0

    invoke-static {p0}, Lorg/unimodules/core/interfaces/RegistryLifecycleListener$-CC;->$default$onDestroy(Lorg/unimodules/core/interfaces/RegistryLifecycleListener;)V

    return-void
.end method
