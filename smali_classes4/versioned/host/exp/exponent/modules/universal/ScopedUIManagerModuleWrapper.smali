.class public Lversioned/host/exp/exponent/modules/universal/ScopedUIManagerModuleWrapper;
.super Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;
.source "ScopedUIManagerModuleWrapper.java"


# instance fields
.field private final mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

.field private final mExperienceName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/react/bridge/ReactContext;Lhost/exp/exponent/kernel/ExperienceId;Ljava/lang/String;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lorg/unimodules/adapters/react/services/UIManagerModuleWrapper;-><init>(Lcom/facebook/react/bridge/ReactContext;)V

    .line 21
    iput-object p2, p0, Lversioned/host/exp/exponent/modules/universal/ScopedUIManagerModuleWrapper;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    .line 22
    iput-object p3, p0, Lversioned/host/exp/exponent/modules/universal/ScopedUIManagerModuleWrapper;->mExperienceName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(II)[I
    .locals 0

    .line 15
    invoke-static {p0, p1}, Lversioned/host/exp/exponent/modules/universal/ScopedUIManagerModuleWrapper;->arrayFilled(II)[I

    move-result-object p0

    return-object p0
.end method

.method private static arrayFilled(II)[I
    .locals 2

    .line 51
    new-array v0, p1, [I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    .line 53
    aput p0, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public registerActivityEventListener(Lorg/unimodules/core/interfaces/ActivityEventListener;)V
    .locals 2

    .line 42
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    new-instance v1, Lversioned/host/exp/exponent/modules/universal/ScopedUIManagerModuleWrapper$2;

    invoke-direct {v1, p0, p1}, Lversioned/host/exp/exponent/modules/universal/ScopedUIManagerModuleWrapper$2;-><init>(Lversioned/host/exp/exponent/modules/universal/ScopedUIManagerModuleWrapper;Lorg/unimodules/core/interfaces/ActivityEventListener;)V

    invoke-virtual {v0, v1}, Lhost/exp/expoview/Exponent;->addActivityResultListener(Lhost/exp/exponent/ActivityResultListener;)V

    return-void
.end method

.method public requestPermissions([Ljava/lang/String;ILorg/unimodules/interfaces/permissions/PermissionsListener;)Z
    .locals 2

    .line 27
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object p2

    new-instance v0, Lversioned/host/exp/exponent/modules/universal/ScopedUIManagerModuleWrapper$1;

    invoke-direct {v0, p0, p3, p1}, Lversioned/host/exp/exponent/modules/universal/ScopedUIManagerModuleWrapper$1;-><init>(Lversioned/host/exp/exponent/modules/universal/ScopedUIManagerModuleWrapper;Lorg/unimodules/interfaces/permissions/PermissionsListener;[Ljava/lang/String;)V

    iget-object p3, p0, Lversioned/host/exp/exponent/modules/universal/ScopedUIManagerModuleWrapper;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    iget-object v1, p0, Lversioned/host/exp/exponent/modules/universal/ScopedUIManagerModuleWrapper;->mExperienceName:Ljava/lang/String;

    invoke-virtual {p2, v0, p1, p3, v1}, Lhost/exp/expoview/Exponent;->requestPermissions(Lhost/exp/expoview/Exponent$PermissionsListener;[Ljava/lang/String;Lhost/exp/exponent/kernel/ExperienceId;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method
