.class public interface abstract Lexpo/modules/av/AVManagerInterface;
.super Ljava/lang/Object;
.source "AVManagerInterface.java"


# virtual methods
.method public abstract abandonAudioFocusIfUnused()V
.end method

.method public abstract acquireAudioFocus()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lexpo/modules/av/AudioFocusNotAcquiredException;
        }
    .end annotation
.end method

.method public abstract getAudioRecordingStatus(Lorg/unimodules/core/Promise;)V
.end method

.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract getModuleRegistry()Lorg/unimodules/core/ModuleRegistry;
.end method

.method public abstract getStatusForSound(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)V
.end method

.method public abstract getStatusForVideo(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)V
.end method

.method public abstract getVolumeForDuckAndFocus(ZF)F
.end method

.method public abstract loadForSound(Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
.end method

.method public abstract loadForVideo(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
.end method

.method public abstract pauseAudioRecording(Lorg/unimodules/core/Promise;)V
.end method

.method public abstract prepareAudioRecorder(Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
.end method

.method public abstract registerVideoViewForAudioLifecycle(Lexpo/modules/av/video/VideoView;)V
.end method

.method public abstract replaySound(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
.end method

.method public abstract replayVideo(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
.end method

.method public abstract setAudioIsEnabled(Ljava/lang/Boolean;)V
.end method

.method public abstract setAudioMode(Lorg/unimodules/core/arguments/ReadableArguments;)V
.end method

.method public abstract setStatusForSound(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
.end method

.method public abstract setStatusForVideo(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
.end method

.method public abstract startAudioRecording(Lorg/unimodules/core/Promise;)V
.end method

.method public abstract stopAudioRecording(Lorg/unimodules/core/Promise;)V
.end method

.method public abstract unloadAudioRecorder(Lorg/unimodules/core/Promise;)V
.end method

.method public abstract unloadForSound(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)V
.end method

.method public abstract unloadForVideo(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)V
.end method

.method public abstract unregisterVideoViewForAudioLifecycle(Lexpo/modules/av/video/VideoView;)V
.end method
