.class public Lexpo/modules/av/AVManager;
.super Ljava/lang/Object;
.source "AVManager.java"

# interfaces
.implements Lorg/unimodules/core/interfaces/LifecycleEventListener;
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;
.implements Landroid/media/MediaRecorder$OnInfoListener;
.implements Lexpo/modules/av/AVManagerInterface;
.implements Lorg/unimodules/core/interfaces/InternalModule;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lexpo/modules/av/AVManager$VideoViewCallback;,
        Lexpo/modules/av/AVManager$AudioInterruptionMode;
    }
.end annotation


# static fields
.field private static final AUDIO_MODE_INTERRUPTION_MODE_KEY:Ljava/lang/String; = "interruptionModeAndroid"

.field private static final AUDIO_MODE_PLAY_THROUGH_EARPIECE:Ljava/lang/String; = "playThroughEarpieceAndroid"

.field private static final AUDIO_MODE_SHOULD_DUCK_KEY:Ljava/lang/String; = "shouldDuckAndroid"

.field private static final AUDIO_MODE_STAYS_ACTIVE_IN_BACKGROUND:Ljava/lang/String; = "staysActiveInBackground"

.field private static final RECORDING_OPTIONS_KEY:Ljava/lang/String; = "android"

.field private static final RECORDING_OPTION_AUDIO_ENCODER_KEY:Ljava/lang/String; = "audioEncoder"

.field private static final RECORDING_OPTION_BIT_RATE_KEY:Ljava/lang/String; = "bitRate"

.field private static final RECORDING_OPTION_EXTENSION_KEY:Ljava/lang/String; = "extension"

.field private static final RECORDING_OPTION_MAX_FILE_SIZE_KEY:Ljava/lang/String; = "maxFileSize"

.field private static final RECORDING_OPTION_NUMBER_OF_CHANNELS_KEY:Ljava/lang/String; = "numberOfChannels"

.field private static final RECORDING_OPTION_OUTPUT_FORMAT_KEY:Ljava/lang/String; = "outputFormat"

.field private static final RECORDING_OPTION_SAMPLE_RATE_KEY:Ljava/lang/String; = "sampleRate"


# instance fields
.field private mAcquiredAudioFocus:Z

.field private mAppIsPaused:Z

.field private mAudioInterruptionMode:Lexpo/modules/av/AVManager$AudioInterruptionMode;

.field private final mAudioManager:Landroid/media/AudioManager;

.field private mAudioRecorder:Landroid/media/MediaRecorder;

.field private mAudioRecorderDurationAlreadyRecorded:J

.field private mAudioRecorderIsPaused:Z

.field private mAudioRecorderIsRecording:Z

.field private mAudioRecorderUptimeOfLastStartResume:J

.field private mAudioRecordingFilePath:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mEnabled:Z

.field private mIsDuckingAudio:Z

.field private mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

.field private final mNoisyAudioStreamReceiver:Landroid/content/BroadcastReceiver;

.field private mShouldDuckAudio:Z

.field private mShouldRouteThroughEarpiece:Z

.field private final mSoundMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lexpo/modules/av/player/PlayerData;",
            ">;"
        }
    .end annotation
.end field

.field private mSoundMapKeyCount:I

.field private mStaysActiveInBackground:Z

.field private final mVideoViewSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lexpo/modules/av/video/VideoView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 59
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mShouldRouteThroughEarpiece:Z

    const/4 v1, 0x1

    .line 68
    iput-boolean v1, p0, Lexpo/modules/av/AVManager;->mEnabled:Z

    .line 72
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mAcquiredAudioFocus:Z

    .line 74
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mAppIsPaused:Z

    .line 76
    sget-object v2, Lexpo/modules/av/AVManager$AudioInterruptionMode;->DUCK_OTHERS:Lexpo/modules/av/AVManager$AudioInterruptionMode;

    iput-object v2, p0, Lexpo/modules/av/AVManager;->mAudioInterruptionMode:Lexpo/modules/av/AVManager$AudioInterruptionMode;

    .line 77
    iput-boolean v1, p0, Lexpo/modules/av/AVManager;->mShouldDuckAudio:Z

    .line 78
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mIsDuckingAudio:Z

    .line 79
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mStaysActiveInBackground:Z

    .line 81
    iput v0, p0, Lexpo/modules/av/AVManager;->mSoundMapKeyCount:I

    .line 83
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lexpo/modules/av/AVManager;->mSoundMap:Ljava/util/Map;

    .line 84
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lexpo/modules/av/AVManager;->mVideoViewSet:Ljava/util/Set;

    const/4 v1, 0x0

    .line 86
    iput-object v1, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    .line 87
    iput-object v1, p0, Lexpo/modules/av/AVManager;->mAudioRecordingFilePath:Ljava/lang/String;

    const-wide/16 v1, 0x0

    .line 88
    iput-wide v1, p0, Lexpo/modules/av/AVManager;->mAudioRecorderUptimeOfLastStartResume:J

    .line 89
    iput-wide v1, p0, Lexpo/modules/av/AVManager;->mAudioRecorderDurationAlreadyRecorded:J

    .line 90
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorderIsRecording:Z

    .line 91
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorderIsPaused:Z

    .line 96
    iput-object p1, p0, Lexpo/modules/av/AVManager;->mContext:Landroid/content/Context;

    const-string v0, "audio"

    .line 98
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/media/AudioManager;

    iput-object p1, p0, Lexpo/modules/av/AVManager;->mAudioManager:Landroid/media/AudioManager;

    .line 101
    new-instance p1, Lexpo/modules/av/AVManager$1;

    invoke-direct {p1, p0}, Lexpo/modules/av/AVManager$1;-><init>(Lexpo/modules/av/AVManager;)V

    iput-object p1, p0, Lexpo/modules/av/AVManager;->mNoisyAudioStreamReceiver:Landroid/content/BroadcastReceiver;

    .line 109
    iget-object p1, p0, Lexpo/modules/av/AVManager;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lexpo/modules/av/AVManager;->mNoisyAudioStreamReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.media.AUDIO_BECOMING_NOISY"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private abandonAudioFocus()V
    .locals 3

    .line 268
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->getAllRegisteredAudioEventHandlers()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lexpo/modules/av/AudioEventHandler;

    .line 269
    invoke-interface {v1}, Lexpo/modules/av/AudioEventHandler;->requiresAudioFocus()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 270
    invoke-interface {v1}, Lexpo/modules/av/AudioEventHandler;->pauseImmediately()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 273
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mAcquiredAudioFocus:Z

    .line 274
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    return-void
.end method

.method static synthetic access$000(Lexpo/modules/av/AVManager;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->abandonAudioFocus()V

    return-void
.end method

.method static synthetic access$100(Lexpo/modules/av/AVManager;Ljava/lang/Integer;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lexpo/modules/av/AVManager;->removeSoundForKey(Ljava/lang/Integer;)V

    return-void
.end method

.method static synthetic access$200(Lexpo/modules/av/AVManager;)Ljava/util/Map;
    .locals 0

    .line 44
    iget-object p0, p0, Lexpo/modules/av/AVManager;->mSoundMap:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$300(Lexpo/modules/av/AVManager;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Lexpo/modules/av/AVManager;->sendEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method private checkAudioRecorderExistsOrReject(Lorg/unimodules/core/Promise;)Z
    .locals 2

    .line 507
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "E_AUDIO_NORECORDER"

    const-string v1, "Recorder does not exist."

    .line 508
    invoke-virtual {p1, v0, v1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    :cond_0
    iget-object p1, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private static ensureDirExists(Ljava/io/File;)Ljava/io/File;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 711
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 712
    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t create directory \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, "\'"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return-object p0
.end method

.method private getAllRegisteredAudioEventHandlers()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lexpo/modules/av/AudioEventHandler;",
            ">;"
        }
    .end annotation

    .line 209
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 210
    iget-object v1, p0, Lexpo/modules/av/AVManager;->mVideoViewSet:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 211
    iget-object v1, p0, Lexpo/modules/av/AVManager;->mSoundMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method private getAudioRecorderDurationMillis()J
    .locals 7

    .line 514
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    const-wide/16 v1, 0x0

    if-nez v0, :cond_0

    return-wide v1

    .line 517
    :cond_0
    iget-wide v3, p0, Lexpo/modules/av/AVManager;->mAudioRecorderDurationAlreadyRecorded:J

    .line 518
    iget-boolean v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorderIsRecording:Z

    if-eqz v0, :cond_1

    iget-wide v5, p0, Lexpo/modules/av/AVManager;->mAudioRecorderUptimeOfLastStartResume:J

    cmp-long v0, v5, v1

    if-lez v0, :cond_1

    .line 519
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v5, p0, Lexpo/modules/av/AVManager;->mAudioRecorderUptimeOfLastStartResume:J

    sub-long/2addr v0, v5

    add-long/2addr v3, v0

    :cond_1
    return-wide v3
.end method

.method private getAudioRecorderStatus()Landroid/os/Bundle;
    .locals 3

    .line 525
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 526
    iget-object v1, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    const-string v2, "canRecord"

    .line 527
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 528
    iget-boolean v1, p0, Lexpo/modules/av/AVManager;->mAudioRecorderIsRecording:Z

    const-string v2, "isRecording"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 529
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->getAudioRecorderDurationMillis()J

    move-result-wide v1

    long-to-int v2, v1

    const-string v1, "durationMillis"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-object v0
.end method

.method private isMissingAudioRecordingPermissions()Z
    .locals 2

    .line 502
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    const-class v1, Lorg/unimodules/interfaces/permissions/Permissions;

    invoke-virtual {v0, v1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/interfaces/permissions/Permissions;

    const-string v1, "android.permission.RECORD_AUDIO"

    invoke-interface {v0, v1}, Lorg/unimodules/interfaces/permissions/Permissions;->getPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private removeAudioRecorder()V
    .locals 2

    .line 535
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 537
    :try_start_0
    invoke-virtual {v0}, Landroid/media/MediaRecorder;->stop()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 542
    :catch_0
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    .line 543
    iput-object v1, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    .line 546
    :cond_0
    iput-object v1, p0, Lexpo/modules/av/AVManager;->mAudioRecordingFilePath:Ljava/lang/String;

    const/4 v0, 0x0

    .line 547
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorderIsRecording:Z

    .line 548
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorderIsPaused:Z

    const-wide/16 v0, 0x0

    .line 549
    iput-wide v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorderDurationAlreadyRecorded:J

    .line 550
    iput-wide v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorderUptimeOfLastStartResume:J

    return-void
.end method

.method private removeSoundForKey(Ljava/lang/Integer;)V
    .locals 1

    .line 347
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mSoundMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lexpo/modules/av/player/PlayerData;

    if-eqz p1, :cond_0

    .line 349
    invoke-virtual {p1}, Lexpo/modules/av/player/PlayerData;->release()V

    .line 350
    invoke-virtual {p0}, Lexpo/modules/av/AVManager;->abandonAudioFocusIfUnused()V

    :cond_0
    return-void
.end method

.method private sendEvent(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    .line 140
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    if-eqz v0, :cond_0

    .line 141
    const-class v1, Lorg/unimodules/core/interfaces/services/EventEmitter;

    invoke-virtual {v0, v1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/services/EventEmitter;

    if-eqz v0, :cond_0

    .line 143
    invoke-interface {v0, p1, p2}, Lorg/unimodules/core/interfaces/services/EventEmitter;->emit(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method private tryGetSoundForKey(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)Lexpo/modules/av/player/PlayerData;
    .locals 2

    .line 339
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mSoundMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lexpo/modules/av/player/PlayerData;

    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    const-string v0, "E_AUDIO_NOPLAYER"

    const-string v1, "Player does not exist."

    .line 341
    invoke-virtual {p2, v0, v1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object p1
.end method

.method private tryRunWithVideoView(Ljava/lang/Integer;Lexpo/modules/av/AVManager$VideoViewCallback;Lorg/unimodules/core/Promise;)V
    .locals 2

    .line 429
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    if-eqz v0, :cond_0

    .line 430
    const-class v1, Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-virtual {v0, v1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/services/UIManager;

    if-eqz v0, :cond_0

    .line 432
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    new-instance v1, Lexpo/modules/av/AVManager$5;

    invoke-direct {v1, p0, p2, p3}, Lexpo/modules/av/AVManager$5;-><init>(Lexpo/modules/av/AVManager;Lexpo/modules/av/AVManager$VideoViewCallback;Lorg/unimodules/core/Promise;)V

    const-class p2, Lexpo/modules/av/video/VideoViewWrapper;

    invoke-interface {v0, p1, v1, p2}, Lorg/unimodules/core/interfaces/services/UIManager;->addUIBlock(ILorg/unimodules/core/interfaces/services/UIManager$UIBlock;Ljava/lang/Class;)V

    :cond_0
    return-void
.end method

.method private updateDuckStatusForAllPlayersPlaying()V
    .locals 2

    .line 292
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->getAllRegisteredAudioEventHandlers()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lexpo/modules/av/AudioEventHandler;

    .line 293
    invoke-interface {v1}, Lexpo/modules/av/AudioEventHandler;->updateVolumeMuteAndDuck()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updatePlaySoundThroughEarpiece(Z)V
    .locals 2

    .line 298
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mAudioManager:Landroid/media/AudioManager;

    if-eqz p1, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 299
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mAudioManager:Landroid/media/AudioManager;

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    return-void
.end method


# virtual methods
.method public abandonAudioFocusIfUnused()V
    .locals 2

    .line 278
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->getAllRegisteredAudioEventHandlers()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lexpo/modules/av/AudioEventHandler;

    .line 279
    invoke-interface {v1}, Lexpo/modules/av/AudioEventHandler;->requiresAudioFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 283
    :cond_1
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->abandonAudioFocus()V

    return-void
.end method

.method public acquireAudioFocus()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lexpo/modules/av/AudioFocusNotAcquiredException;
        }
    .end annotation

    .line 245
    iget-boolean v0, p0, Lexpo/modules/av/AVManager;->mEnabled:Z

    if-eqz v0, :cond_6

    .line 249
    iget-boolean v0, p0, Lexpo/modules/av/AVManager;->mAppIsPaused:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lexpo/modules/av/AVManager;->mStaysActiveInBackground:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 250
    :cond_0
    new-instance v0, Lexpo/modules/av/AudioFocusNotAcquiredException;

    const-string v1, "This experience is currently in the background, so audio focus could not be acquired."

    invoke-direct {v0, v1}, Lexpo/modules/av/AudioFocusNotAcquiredException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lexpo/modules/av/AVManager;->mAcquiredAudioFocus:Z

    if-eqz v0, :cond_2

    return-void

    .line 257
    :cond_2
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mAudioInterruptionMode:Lexpo/modules/av/AVManager$AudioInterruptionMode;

    sget-object v1, Lexpo/modules/av/AVManager$AudioInterruptionMode;->DO_NOT_MIX:Lexpo/modules/av/AVManager$AudioInterruptionMode;

    const/4 v2, 0x3

    const/4 v3, 0x1

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x3

    .line 260
    :goto_1
    iget-object v1, p0, Lexpo/modules/av/AVManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, p0, v2, v0}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    if-ne v0, v3, :cond_4

    goto :goto_2

    :cond_4
    const/4 v3, 0x0

    .line 261
    :goto_2
    iput-boolean v3, p0, Lexpo/modules/av/AVManager;->mAcquiredAudioFocus:Z

    .line 262
    iget-boolean v0, p0, Lexpo/modules/av/AVManager;->mAcquiredAudioFocus:Z

    if-eqz v0, :cond_5

    return-void

    .line 263
    :cond_5
    new-instance v0, Lexpo/modules/av/AudioFocusNotAcquiredException;

    const-string v1, "Audio focus could not be acquired from the OS at this time."

    invoke-direct {v0, v1}, Lexpo/modules/av/AudioFocusNotAcquiredException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 246
    :cond_6
    new-instance v0, Lexpo/modules/av/AudioFocusNotAcquiredException;

    const-string v1, "Expo Audio is disabled, so audio focus could not be acquired."

    invoke-direct {v0, v1}, Lexpo/modules/av/AudioFocusNotAcquiredException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getAudioRecordingStatus(Lorg/unimodules/core/Promise;)V
    .locals 1

    .line 697
    invoke-direct {p0, p1}, Lexpo/modules/av/AVManager;->checkAudioRecorderExistsOrReject(Lorg/unimodules/core/Promise;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 698
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->getAudioRecorderStatus()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .line 131
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getExportedInterfaces()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation

    .line 136
    const-class v0, Lexpo/modules/av/AVManagerInterface;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getModuleRegistry()Lorg/unimodules/core/ModuleRegistry;
    .locals 1

    .line 115
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    return-object v0
.end method

.method public getStatusForSound(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)V
    .locals 0

    .line 415
    invoke-direct {p0, p1, p2}, Lexpo/modules/av/AVManager;->tryGetSoundForKey(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)Lexpo/modules/av/player/PlayerData;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 417
    invoke-virtual {p1}, Lexpo/modules/av/player/PlayerData;->getStatus()Landroid/os/Bundle;

    move-result-object p1

    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public getStatusForVideo(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)V
    .locals 1

    .line 489
    new-instance v0, Lexpo/modules/av/AVManager$10;

    invoke-direct {v0, p0, p2}, Lexpo/modules/av/AVManager$10;-><init>(Lexpo/modules/av/AVManager;Lorg/unimodules/core/Promise;)V

    invoke-direct {p0, p1, v0, p2}, Lexpo/modules/av/AVManager;->tryRunWithVideoView(Ljava/lang/Integer;Lexpo/modules/av/AVManager$VideoViewCallback;Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public getVolumeForDuckAndFocus(ZF)F
    .locals 1

    .line 288
    iget-boolean v0, p0, Lexpo/modules/av/AVManager;->mAcquiredAudioFocus:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    iget-boolean p1, p0, Lexpo/modules/av/AVManager;->mIsDuckingAudio:Z

    if-eqz p1, :cond_2

    const/high16 p1, 0x40000000    # 2.0f

    div-float/2addr p2, p1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p2, 0x0

    :cond_2
    :goto_1
    return p2
.end method

.method public loadForSound(Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
    .locals 3

    .line 356
    iget v0, p0, Lexpo/modules/av/AVManager;->mSoundMapKeyCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lexpo/modules/av/AVManager;->mSoundMapKeyCount:I

    .line 357
    iget-object v1, p0, Lexpo/modules/av/AVManager;->mContext:Landroid/content/Context;

    invoke-interface {p2}, Lorg/unimodules/core/arguments/ReadableArguments;->toBundle()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {p0, v1, p1, v2}, Lexpo/modules/av/player/PlayerData;->createUnloadedPlayerData(Lexpo/modules/av/AVManagerInterface;Landroid/content/Context;Lorg/unimodules/core/arguments/ReadableArguments;Landroid/os/Bundle;)Lexpo/modules/av/player/PlayerData;

    move-result-object p1

    .line 358
    new-instance v1, Lexpo/modules/av/AVManager$2;

    invoke-direct {v1, p0, v0}, Lexpo/modules/av/AVManager$2;-><init>(Lexpo/modules/av/AVManager;I)V

    invoke-virtual {p1, v1}, Lexpo/modules/av/player/PlayerData;->setErrorListener(Lexpo/modules/av/player/PlayerData$ErrorListener;)V

    .line 364
    iget-object v1, p0, Lexpo/modules/av/AVManager;->mSoundMap:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    invoke-interface {p2}, Lorg/unimodules/core/arguments/ReadableArguments;->toBundle()Landroid/os/Bundle;

    move-result-object p2

    new-instance v1, Lexpo/modules/av/AVManager$3;

    invoke-direct {v1, p0, p3, v0}, Lexpo/modules/av/AVManager$3;-><init>(Lexpo/modules/av/AVManager;Lorg/unimodules/core/Promise;I)V

    invoke-virtual {p1, p2, v1}, Lexpo/modules/av/player/PlayerData;->load(Landroid/os/Bundle;Lexpo/modules/av/player/PlayerData$LoadCompletionListener;)V

    .line 378
    new-instance p2, Lexpo/modules/av/AVManager$4;

    invoke-direct {p2, p0, v0}, Lexpo/modules/av/AVManager$4;-><init>(Lexpo/modules/av/AVManager;I)V

    invoke-virtual {p1, p2}, Lexpo/modules/av/player/PlayerData;->setStatusUpdateListener(Lexpo/modules/av/player/PlayerData$StatusUpdateListener;)V

    return-void
.end method

.method public loadForVideo(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
    .locals 1

    .line 449
    new-instance v0, Lexpo/modules/av/AVManager$6;

    invoke-direct {v0, p0, p2, p3, p4}, Lexpo/modules/av/AVManager$6;-><init>(Lexpo/modules/av/AVManager;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V

    invoke-direct {p0, p1, v0, p4}, Lexpo/modules/av/AVManager;->tryRunWithVideoView(Ljava/lang/Integer;Lexpo/modules/av/AVManager$VideoViewCallback;Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public onAudioFocusChange(I)V
    .locals 3

    const/4 v0, -0x3

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, -0x2

    if-eq p1, v0, :cond_2

    const/4 v0, -0x1

    if-eq p1, v0, :cond_2

    if-eq p1, v2, :cond_0

    goto :goto_2

    .line 234
    :cond_0
    iput-boolean v1, p0, Lexpo/modules/av/AVManager;->mIsDuckingAudio:Z

    .line 235
    iput-boolean v2, p0, Lexpo/modules/av/AVManager;->mAcquiredAudioFocus:Z

    .line 236
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->getAllRegisteredAudioEventHandlers()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexpo/modules/av/AudioEventHandler;

    .line 237
    invoke-interface {v0}, Lexpo/modules/av/AudioEventHandler;->handleAudioFocusGained()V

    goto :goto_0

    .line 219
    :cond_1
    iget-boolean p1, p0, Lexpo/modules/av/AVManager;->mShouldDuckAudio:Z

    if-eqz p1, :cond_2

    .line 220
    iput-boolean v2, p0, Lexpo/modules/av/AVManager;->mIsDuckingAudio:Z

    .line 221
    iput-boolean v2, p0, Lexpo/modules/av/AVManager;->mAcquiredAudioFocus:Z

    .line 222
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->updateDuckStatusForAllPlayersPlaying()V

    goto :goto_2

    .line 227
    :cond_2
    iput-boolean v1, p0, Lexpo/modules/av/AVManager;->mIsDuckingAudio:Z

    .line 228
    iput-boolean v1, p0, Lexpo/modules/av/AVManager;->mAcquiredAudioFocus:Z

    .line 229
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->getAllRegisteredAudioEventHandlers()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexpo/modules/av/AudioEventHandler;

    .line 230
    invoke-interface {v0}, Lexpo/modules/av/AudioEventHandler;->handleAudioFocusInterruptionBegan()V

    goto :goto_1

    :cond_3
    :goto_2
    return-void
.end method

.method public onCreate(Lorg/unimodules/core/ModuleRegistry;)V
    .locals 2

    .line 120
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    if-eqz v0, :cond_0

    .line 121
    const-class v1, Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-virtual {v0, v1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-interface {v0, p0}, Lorg/unimodules/core/interfaces/services/UIManager;->unregisterLifecycleEventListener(Lorg/unimodules/core/interfaces/LifecycleEventListener;)V

    .line 123
    :cond_0
    iput-object p1, p0, Lexpo/modules/av/AVManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    .line 124
    iget-object p1, p0, Lexpo/modules/av/AVManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    if-eqz p1, :cond_1

    .line 125
    const-class v0, Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-virtual {p1, v0}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-interface {p1, p0}, Lorg/unimodules/core/interfaces/services/UIManager;->registerLifecycleEventListener(Lorg/unimodules/core/interfaces/LifecycleEventListener;)V

    :cond_1
    return-void
.end method

.method public synthetic onDestroy()V
    .locals 0

    invoke-static {p0}, Lorg/unimodules/core/interfaces/RegistryLifecycleListener$-CC;->$default$onDestroy(Lorg/unimodules/core/interfaces/RegistryLifecycleListener;)V

    return-void
.end method

.method public onHostDestroy()V
    .locals 2

    .line 184
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lexpo/modules/av/AVManager;->mNoisyAudioStreamReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 185
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mSoundMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 186
    invoke-direct {p0, v1}, Lexpo/modules/av/AVManager;->removeSoundForKey(Ljava/lang/Integer;)V

    goto :goto_0

    .line 188
    :cond_0
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mVideoViewSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lexpo/modules/av/video/VideoView;

    .line 189
    invoke-virtual {v1}, Lexpo/modules/av/video/VideoView;->unloadPlayerAndMediaController()V

    goto :goto_1

    .line 192
    :cond_1
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->removeAudioRecorder()V

    .line 193
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->abandonAudioFocus()V

    return-void
.end method

.method public onHostPause()V
    .locals 2

    .line 167
    iget-boolean v0, p0, Lexpo/modules/av/AVManager;->mAppIsPaused:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 168
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mAppIsPaused:Z

    .line 169
    iget-boolean v0, p0, Lexpo/modules/av/AVManager;->mStaysActiveInBackground:Z

    if-nez v0, :cond_1

    .line 170
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->getAllRegisteredAudioEventHandlers()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lexpo/modules/av/AudioEventHandler;

    .line 171
    invoke-interface {v1}, Lexpo/modules/av/AudioEventHandler;->onPause()V

    goto :goto_0

    .line 173
    :cond_0
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->abandonAudioFocus()V

    .line 175
    iget-boolean v0, p0, Lexpo/modules/av/AVManager;->mShouldRouteThroughEarpiece:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 176
    invoke-direct {p0, v0}, Lexpo/modules/av/AVManager;->updatePlaySoundThroughEarpiece(Z)V

    :cond_1
    return-void
.end method

.method public onHostResume()V
    .locals 2

    .line 152
    iget-boolean v0, p0, Lexpo/modules/av/AVManager;->mAppIsPaused:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 153
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mAppIsPaused:Z

    .line 154
    iget-boolean v0, p0, Lexpo/modules/av/AVManager;->mStaysActiveInBackground:Z

    if-nez v0, :cond_1

    .line 155
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->getAllRegisteredAudioEventHandlers()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lexpo/modules/av/AudioEventHandler;

    .line 156
    invoke-interface {v1}, Lexpo/modules/av/AudioEventHandler;->onResume()V

    goto :goto_0

    .line 158
    :cond_0
    iget-boolean v0, p0, Lexpo/modules/av/AVManager;->mShouldRouteThroughEarpiece:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 159
    invoke-direct {p0, v0}, Lexpo/modules/av/AVManager;->updatePlaySoundThroughEarpiece(Z)V

    :cond_1
    return-void
.end method

.method public onInfo(Landroid/media/MediaRecorder;II)V
    .locals 0

    const/16 p1, 0x321

    if-eq p2, p1, :cond_0

    goto :goto_0

    .line 557
    :cond_0
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->removeAudioRecorder()V

    .line 558
    iget-object p1, p0, Lexpo/modules/av/AVManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    if-eqz p1, :cond_1

    .line 559
    const-class p2, Lorg/unimodules/core/interfaces/services/EventEmitter;

    invoke-virtual {p1, p2}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/unimodules/core/interfaces/services/EventEmitter;

    if-eqz p1, :cond_1

    .line 561
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    const-string p3, "Expo.Recording.recorderUnloaded"

    invoke-interface {p1, p3, p2}, Lorg/unimodules/core/interfaces/services/EventEmitter;->emit(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public pauseAudioRecording(Lorg/unimodules/core/Promise;)V
    .locals 3

    .line 656
    invoke-direct {p0, p1}, Lexpo/modules/av/AVManager;->checkAudioRecorderExistsOrReject(Lorg/unimodules/core/Promise;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 657
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-ge v0, v1, :cond_0

    const-string v0, "E_AUDIO_VERSIONINCOMPATIBLE"

    const-string v1, "Pausing an audio recording is unsupported on Android devices running SDK < 24."

    .line 658
    invoke-virtual {p1, v0, v1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 662
    :cond_0
    :try_start_0
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->pause()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 668
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->getAudioRecorderDurationMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorderDurationAlreadyRecorded:J

    const/4 v0, 0x0

    .line 669
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorderIsRecording:Z

    const/4 v0, 0x1

    .line 670
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorderIsPaused:Z

    .line 672
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->getAudioRecorderStatus()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "E_AUDIO_RECORDINGPAUSE"

    const-string v2, "Pause encountered an error: recording not paused"

    .line 664
    invoke-virtual {p1, v1, v2, v0}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public prepareAudioRecorder(Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
    .locals 4

    .line 571
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->isMissingAudioRecordingPermissions()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, "E_MISSING_PERMISSION"

    const-string v0, "Missing audio recording permissions."

    .line 572
    invoke-virtual {p2, p1, v0}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 576
    :cond_0
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->removeAudioRecorder()V

    const-string v0, "android"

    .line 578
    invoke-interface {p1, v0}, Lorg/unimodules/core/arguments/ReadableArguments;->getArguments(Ljava/lang/String;)Lorg/unimodules/core/arguments/ReadableArguments;

    move-result-object p1

    .line 580
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "recording-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "extension"

    .line 581
    invoke-interface {p1, v1}, Lorg/unimodules/core/arguments/ReadableArguments;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 583
    :try_start_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lexpo/modules/av/AVManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "Audio"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 584
    invoke-static {v1}, Lexpo/modules/av/AVManager;->ensureDirExists(Ljava/io/File;)Ljava/io/File;

    .line 585
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexpo/modules/av/AVManager;->mAudioRecordingFilePath:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 591
    :catch_0
    new-instance v0, Landroid/media/MediaRecorder;

    invoke-direct {v0}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    .line 592
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 594
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    const-string v1, "outputFormat"

    invoke-interface {p1, v1}, Lorg/unimodules/core/arguments/ReadableArguments;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    .line 595
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    const-string v1, "audioEncoder"

    invoke-interface {p1, v1}, Lorg/unimodules/core/arguments/ReadableArguments;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    const-string v0, "sampleRate"

    .line 596
    invoke-interface {p1, v0}, Lorg/unimodules/core/arguments/ReadableArguments;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 597
    iget-object v1, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    invoke-interface {p1, v0}, Lorg/unimodules/core/arguments/ReadableArguments;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/media/MediaRecorder;->setAudioSamplingRate(I)V

    :cond_1
    const-string v0, "numberOfChannels"

    .line 599
    invoke-interface {p1, v0}, Lorg/unimodules/core/arguments/ReadableArguments;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 600
    iget-object v1, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    invoke-interface {p1, v0}, Lorg/unimodules/core/arguments/ReadableArguments;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/media/MediaRecorder;->setAudioChannels(I)V

    :cond_2
    const-string v0, "bitRate"

    .line 602
    invoke-interface {p1, v0}, Lorg/unimodules/core/arguments/ReadableArguments;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 603
    iget-object v1, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    invoke-interface {p1, v0}, Lorg/unimodules/core/arguments/ReadableArguments;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/media/MediaRecorder;->setAudioEncodingBitRate(I)V

    .line 606
    :cond_3
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    iget-object v1, p0, Lexpo/modules/av/AVManager;->mAudioRecordingFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    const-string v0, "maxFileSize"

    .line 608
    invoke-interface {p1, v0}, Lorg/unimodules/core/arguments/ReadableArguments;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 609
    iget-object v1, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    invoke-interface {p1, v0}, Lorg/unimodules/core/arguments/ReadableArguments;->getInt(Ljava/lang/String;)I

    move-result p1

    int-to-long v2, p1

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaRecorder;->setMaxFileSize(J)V

    .line 610
    iget-object p1, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {p1, p0}, Landroid/media/MediaRecorder;->setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    .line 614
    :cond_4
    :try_start_1
    iget-object p1, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {p1}, Landroid/media/MediaRecorder;->prepare()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 621
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 622
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lexpo/modules/av/AVManager;->mAudioRecordingFilePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "uri"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->getAudioRecorderStatus()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "status"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 624
    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void

    :catch_1
    move-exception p1

    const-string v0, "E_AUDIO_RECORDERNOTCREATED"

    const-string v1, "Prepare encountered an error: recorder not prepared"

    .line 616
    invoke-virtual {p2, v0, v1, p1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 617
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->removeAudioRecorder()V

    return-void
.end method

.method public registerVideoViewForAudioLifecycle(Lexpo/modules/av/video/VideoView;)V
    .locals 1

    .line 200
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mVideoViewSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public replaySound(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
    .locals 0

    .line 407
    invoke-direct {p0, p1, p3}, Lexpo/modules/av/AVManager;->tryGetSoundForKey(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)Lexpo/modules/av/player/PlayerData;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 409
    invoke-interface {p2}, Lorg/unimodules/core/arguments/ReadableArguments;->toBundle()Landroid/os/Bundle;

    move-result-object p2

    invoke-virtual {p1, p2, p3}, Lexpo/modules/av/player/PlayerData;->setStatus(Landroid/os/Bundle;Lorg/unimodules/core/Promise;)V

    :cond_0
    return-void
.end method

.method public replayVideo(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
    .locals 1

    .line 479
    new-instance v0, Lexpo/modules/av/AVManager$9;

    invoke-direct {v0, p0, p2, p3}, Lexpo/modules/av/AVManager$9;-><init>(Lexpo/modules/av/AVManager;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V

    invoke-direct {p0, p1, v0, p3}, Lexpo/modules/av/AVManager;->tryRunWithVideoView(Ljava/lang/Integer;Lexpo/modules/av/AVManager$VideoViewCallback;Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public setAudioIsEnabled(Ljava/lang/Boolean;)V
    .locals 1

    .line 304
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mEnabled:Z

    .line 305
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    .line 306
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->abandonAudioFocus()V

    :cond_0
    return-void
.end method

.method public setAudioMode(Lorg/unimodules/core/arguments/ReadableArguments;)V
    .locals 2

    const-string v0, "shouldDuckAndroid"

    .line 312
    invoke-interface {p1, v0}, Lorg/unimodules/core/arguments/ReadableArguments;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mShouldDuckAudio:Z

    .line 313
    iget-boolean v0, p0, Lexpo/modules/av/AVManager;->mShouldDuckAudio:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 314
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mIsDuckingAudio:Z

    .line 315
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->updateDuckStatusForAllPlayersPlaying()V

    :cond_0
    const-string v0, "playThroughEarpieceAndroid"

    .line 318
    invoke-interface {p1, v0}, Lorg/unimodules/core/arguments/ReadableArguments;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 319
    invoke-interface {p1, v0}, Lorg/unimodules/core/arguments/ReadableArguments;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mShouldRouteThroughEarpiece:Z

    .line 320
    iget-boolean v0, p0, Lexpo/modules/av/AVManager;->mShouldRouteThroughEarpiece:Z

    invoke-direct {p0, v0}, Lexpo/modules/av/AVManager;->updatePlaySoundThroughEarpiece(Z)V

    :cond_1
    const-string v0, "interruptionModeAndroid"

    .line 323
    invoke-interface {p1, v0}, Lorg/unimodules/core/arguments/ReadableArguments;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    goto :goto_0

    .line 326
    :cond_2
    sget-object v0, Lexpo/modules/av/AVManager$AudioInterruptionMode;->DO_NOT_MIX:Lexpo/modules/av/AVManager$AudioInterruptionMode;

    iput-object v0, p0, Lexpo/modules/av/AVManager;->mAudioInterruptionMode:Lexpo/modules/av/AVManager$AudioInterruptionMode;

    .line 329
    :goto_0
    sget-object v0, Lexpo/modules/av/AVManager$AudioInterruptionMode;->DUCK_OTHERS:Lexpo/modules/av/AVManager$AudioInterruptionMode;

    iput-object v0, p0, Lexpo/modules/av/AVManager;->mAudioInterruptionMode:Lexpo/modules/av/AVManager$AudioInterruptionMode;

    const-string v0, "staysActiveInBackground"

    .line 332
    invoke-interface {p1, v0}, Lorg/unimodules/core/arguments/ReadableArguments;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lexpo/modules/av/AVManager;->mStaysActiveInBackground:Z

    return-void
.end method

.method public setStatusForSound(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
    .locals 0

    .line 399
    invoke-direct {p0, p1, p3}, Lexpo/modules/av/AVManager;->tryGetSoundForKey(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)Lexpo/modules/av/player/PlayerData;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 401
    invoke-interface {p2}, Lorg/unimodules/core/arguments/ReadableArguments;->toBundle()Landroid/os/Bundle;

    move-result-object p2

    invoke-virtual {p1, p2, p3}, Lexpo/modules/av/player/PlayerData;->setStatus(Landroid/os/Bundle;Lorg/unimodules/core/Promise;)V

    :cond_0
    return-void
.end method

.method public setStatusForVideo(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
    .locals 1

    .line 469
    new-instance v0, Lexpo/modules/av/AVManager$8;

    invoke-direct {v0, p0, p2, p3}, Lexpo/modules/av/AVManager$8;-><init>(Lexpo/modules/av/AVManager;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V

    invoke-direct {p0, p1, v0, p3}, Lexpo/modules/av/AVManager;->tryRunWithVideoView(Ljava/lang/Integer;Lexpo/modules/av/AVManager$VideoViewCallback;Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public startAudioRecording(Lorg/unimodules/core/Promise;)V
    .locals 3

    .line 629
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->isMissingAudioRecordingPermissions()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "E_MISSING_PERMISSION"

    const-string v1, "Missing audio recording permissions."

    .line 630
    invoke-virtual {p1, v0, v1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 634
    :cond_0
    invoke-direct {p0, p1}, Lexpo/modules/av/AVManager;->checkAudioRecorderExistsOrReject(Lorg/unimodules/core/Promise;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 636
    :try_start_0
    iget-boolean v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorderIsPaused:Z

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_1

    .line 637
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->resume()V

    goto :goto_0

    .line 639
    :cond_1
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 646
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorderUptimeOfLastStartResume:J

    const/4 v0, 0x1

    .line 647
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorderIsRecording:Z

    const/4 v0, 0x0

    .line 648
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorderIsPaused:Z

    .line 650
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->getAudioRecorderStatus()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "E_AUDIO_RECORDING"

    const-string v2, "Start encountered an error: recording not started"

    .line 642
    invoke-virtual {p1, v1, v2, v0}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    :goto_1
    return-void
.end method

.method public stopAudioRecording(Lorg/unimodules/core/Promise;)V
    .locals 3

    .line 679
    invoke-direct {p0, p1}, Lexpo/modules/av/AVManager;->checkAudioRecorderExistsOrReject(Lorg/unimodules/core/Promise;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 681
    :try_start_0
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->stop()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 687
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->getAudioRecorderDurationMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorderDurationAlreadyRecorded:J

    const/4 v0, 0x0

    .line 688
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorderIsRecording:Z

    .line 689
    iput-boolean v0, p0, Lexpo/modules/av/AVManager;->mAudioRecorderIsPaused:Z

    .line 691
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->getAudioRecorderStatus()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "E_AUDIO_RECORDINGSTOP"

    const-string v2, "Stop encountered an error: recording not stopped"

    .line 683
    invoke-virtual {p1, v1, v2, v0}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method

.method public unloadAudioRecorder(Lorg/unimodules/core/Promise;)V
    .locals 1

    .line 704
    invoke-direct {p0, p1}, Lexpo/modules/av/AVManager;->checkAudioRecorderExistsOrReject(Lorg/unimodules/core/Promise;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 705
    invoke-direct {p0}, Lexpo/modules/av/AVManager;->removeAudioRecorder()V

    const/4 v0, 0x0

    .line 706
    invoke-virtual {p1, v0}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public unloadForSound(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)V
    .locals 1

    .line 391
    invoke-direct {p0, p1, p2}, Lexpo/modules/av/AVManager;->tryGetSoundForKey(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)Lexpo/modules/av/player/PlayerData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 392
    invoke-direct {p0, p1}, Lexpo/modules/av/AVManager;->removeSoundForKey(Ljava/lang/Integer;)V

    .line 393
    invoke-static {}, Lexpo/modules/av/player/PlayerData;->getUnloadedStatus()Landroid/os/Bundle;

    move-result-object p1

    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public unloadForVideo(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)V
    .locals 1

    .line 459
    new-instance v0, Lexpo/modules/av/AVManager$7;

    invoke-direct {v0, p0, p2}, Lexpo/modules/av/AVManager$7;-><init>(Lexpo/modules/av/AVManager;Lorg/unimodules/core/Promise;)V

    invoke-direct {p0, p1, v0, p2}, Lexpo/modules/av/AVManager;->tryRunWithVideoView(Ljava/lang/Integer;Lexpo/modules/av/AVManager$VideoViewCallback;Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public unregisterVideoViewForAudioLifecycle(Lexpo/modules/av/video/VideoView;)V
    .locals 1

    .line 205
    iget-object v0, p0, Lexpo/modules/av/AVManager;->mVideoViewSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method
