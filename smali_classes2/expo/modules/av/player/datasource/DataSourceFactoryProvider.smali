.class public interface abstract Lexpo/modules/av/player/datasource/DataSourceFactoryProvider;
.super Ljava/lang/Object;
.source "DataSourceFactoryProvider.java"


# virtual methods
.method public abstract createFactory(Landroid/content/Context;Lorg/unimodules/core/ModuleRegistry;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/exoplayer2/upstream/DataSource$Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lorg/unimodules/core/ModuleRegistry;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/android/exoplayer2/upstream/DataSource$Factory;"
        }
    .end annotation
.end method
