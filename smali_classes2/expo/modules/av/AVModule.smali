.class public Lexpo/modules/av/AVModule;
.super Lorg/unimodules/core/ExportedModule;
.source "AVModule.java"


# instance fields
.field private mAVManager:Lexpo/modules/av/AVManagerInterface;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1}, Lorg/unimodules/core/ExportedModule;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public getAudioRecordingStatus(Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 112
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1}, Lexpo/modules/av/AVManagerInterface;->getAudioRecordingStatus(Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "ExponentAV"

    return-object v0
.end method

.method public getStatusForSound(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 62
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1, p2}, Lexpo/modules/av/AVManagerInterface;->getStatusForSound(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public getStatusForVideo(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 87
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1, p2}, Lexpo/modules/av/AVManagerInterface;->getStatusForVideo(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public loadForSound(Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 42
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1, p2, p3}, Lexpo/modules/av/AVManagerInterface;->loadForSound(Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public loadForVideo(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 67
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lexpo/modules/av/AVManagerInterface;->loadForVideo(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public onCreate(Lorg/unimodules/core/ModuleRegistry;)V
    .locals 1

    .line 25
    const-class v0, Lexpo/modules/av/AVManagerInterface;

    invoke-virtual {p1, v0}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lexpo/modules/av/AVManagerInterface;

    iput-object p1, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    return-void
.end method

.method public pauseAudioRecording(Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 102
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1}, Lexpo/modules/av/AVManagerInterface;->pauseAudioRecording(Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public prepareAudioRecorder(Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 92
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1, p2}, Lexpo/modules/av/AVManagerInterface;->prepareAudioRecorder(Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public replaySound(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 57
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1, p2, p3}, Lexpo/modules/av/AVManagerInterface;->replaySound(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public replayVideo(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 82
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1, p2, p3}, Lexpo/modules/av/AVManagerInterface;->replayVideo(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public setAudioIsEnabled(Ljava/lang/Boolean;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 30
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1}, Lexpo/modules/av/AVManagerInterface;->setAudioIsEnabled(Ljava/lang/Boolean;)V

    const/4 p1, 0x0

    .line 31
    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public setAudioMode(Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 36
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1}, Lexpo/modules/av/AVManagerInterface;->setAudioMode(Lorg/unimodules/core/arguments/ReadableArguments;)V

    const/4 p1, 0x0

    .line 37
    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public setStatusForSound(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 52
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1, p2, p3}, Lexpo/modules/av/AVManagerInterface;->setStatusForSound(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public setStatusForVideo(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 77
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1, p2, p3}, Lexpo/modules/av/AVManagerInterface;->setStatusForVideo(Ljava/lang/Integer;Lorg/unimodules/core/arguments/ReadableArguments;Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public startAudioRecording(Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 97
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1}, Lexpo/modules/av/AVManagerInterface;->startAudioRecording(Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public stopAudioRecording(Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 107
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1}, Lexpo/modules/av/AVManagerInterface;->stopAudioRecording(Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public unloadAudioRecorder(Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 117
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1}, Lexpo/modules/av/AVManagerInterface;->unloadAudioRecorder(Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public unloadForSound(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 47
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1, p2}, Lexpo/modules/av/AVManagerInterface;->unloadForSound(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public unloadForVideo(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 72
    iget-object v0, p0, Lexpo/modules/av/AVModule;->mAVManager:Lexpo/modules/av/AVManagerInterface;

    invoke-interface {v0, p1, p2}, Lexpo/modules/av/AVManagerInterface;->unloadForVideo(Ljava/lang/Integer;Lorg/unimodules/core/Promise;)V

    return-void
.end method
