.class public final Lexpo/modules/av/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lexpo/modules/av/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action0:I = 0x7f09000b

.field public static final action_bar:I = 0x7f09000c

.field public static final action_bar_activity_content:I = 0x7f09000d

.field public static final action_bar_container:I = 0x7f09000f

.field public static final action_bar_root:I = 0x7f090010

.field public static final action_bar_spinner:I = 0x7f090011

.field public static final action_bar_subtitle:I = 0x7f090012

.field public static final action_bar_title:I = 0x7f090013

.field public static final action_container:I = 0x7f090014

.field public static final action_context_bar:I = 0x7f090015

.field public static final action_divider:I = 0x7f090016

.field public static final action_image:I = 0x7f090017

.field public static final action_menu_divider:I = 0x7f090018

.field public static final action_menu_presenter:I = 0x7f090019

.field public static final action_mode_bar:I = 0x7f09001a

.field public static final action_mode_bar_stub:I = 0x7f09001b

.field public static final action_mode_close_button:I = 0x7f09001c

.field public static final action_text:I = 0x7f09001d

.field public static final actions:I = 0x7f09001e

.field public static final activity_chooser_view_content:I = 0x7f09001f

.field public static final alertTitle:I = 0x7f09002d

.field public static final always:I = 0x7f09002f

.field public static final async:I = 0x7f090038

.field public static final beginning:I = 0x7f09003c

.field public static final blocking:I = 0x7f09003d

.field public static final buttonPanel:I = 0x7f090047

.field public static final cancel_action:I = 0x7f09004e

.field public static final center:I = 0x7f090055

.field public static final centerBottom:I = 0x7f090056

.field public static final centerBottomCrop:I = 0x7f090057

.field public static final centerCrop:I = 0x7f090058

.field public static final centerInside:I = 0x7f090059

.field public static final centerTop:I = 0x7f09005a

.field public static final centerTopCrop:I = 0x7f09005b

.field public static final checkbox:I = 0x7f09005e

.field public static final chronometer:I = 0x7f09005f

.field public static final collapseActionView:I = 0x7f090064

.field public static final contentPanel:I = 0x7f090078

.field public static final current_time_text:I = 0x7f090087

.field public static final custom:I = 0x7f090088

.field public static final customPanel:I = 0x7f090089

.field public static final decor_content_parent:I = 0x7f09008c

.field public static final default_activity_button:I = 0x7f09008d

.field public static final disableHome:I = 0x7f090094

.field public static final edit_query:I = 0x7f090099

.field public static final end:I = 0x7f09009a

.field public static final endInside:I = 0x7f09009b

.field public static final end_padder:I = 0x7f09009c

.field public static final end_time_text:I = 0x7f09009d

.field public static final exo_artwork:I = 0x7f0900a7

.field public static final exo_buffering:I = 0x7f0900a8

.field public static final exo_content_frame:I = 0x7f0900a9

.field public static final exo_controller:I = 0x7f0900aa

.field public static final exo_controller_placeholder:I = 0x7f0900ab

.field public static final exo_duration:I = 0x7f0900ac

.field public static final exo_error_message:I = 0x7f0900ad

.field public static final exo_ffwd:I = 0x7f0900ae

.field public static final exo_next:I = 0x7f0900af

.field public static final exo_overlay:I = 0x7f0900b0

.field public static final exo_pause:I = 0x7f0900b1

.field public static final exo_play:I = 0x7f0900b2

.field public static final exo_position:I = 0x7f0900b3

.field public static final exo_prev:I = 0x7f0900b4

.field public static final exo_progress:I = 0x7f0900b5

.field public static final exo_repeat_toggle:I = 0x7f0900b6

.field public static final exo_rew:I = 0x7f0900b7

.field public static final exo_shuffle:I = 0x7f0900b8

.field public static final exo_shutter:I = 0x7f0900b9

.field public static final exo_subtitles:I = 0x7f0900ba

.field public static final exo_track_selection_view:I = 0x7f0900bb

.field public static final expand_activities_button:I = 0x7f0900bc

.field public static final expanded_menu:I = 0x7f0900bd

.field public static final fast_forward_button:I = 0x7f0900c1

.field public static final fill:I = 0x7f0900c2

.field public static final fit:I = 0x7f0900c6

.field public static final fitCenter:I = 0x7f0900c8

.field public static final fitEnd:I = 0x7f0900c9

.field public static final fitStart:I = 0x7f0900ca

.field public static final fitXY:I = 0x7f0900cb

.field public static final fixed_height:I = 0x7f0900cd

.field public static final fixed_width:I = 0x7f0900ce

.field public static final forever:I = 0x7f0900d0

.field public static final fullscreen_mode_button:I = 0x7f0900d2

.field public static final home:I = 0x7f0900df

.field public static final homeAsUp:I = 0x7f0900e0

.field public static final icon:I = 0x7f0900e5

.field public static final icon_group:I = 0x7f0900e6

.field public static final ifRoom:I = 0x7f0900e8

.field public static final image:I = 0x7f0900e9

.field public static final info:I = 0x7f0900eb

.field public static final italic:I = 0x7f0900f1

.field public static final leftBottom:I = 0x7f0900fc

.field public static final leftBottomCrop:I = 0x7f0900fd

.field public static final leftCenter:I = 0x7f0900fe

.field public static final leftCenterCrop:I = 0x7f0900ff

.field public static final leftTop:I = 0x7f090100

.field public static final leftTopCrop:I = 0x7f090101

.field public static final line1:I = 0x7f090103

.field public static final line3:I = 0x7f090104

.field public static final listMode:I = 0x7f090106

.field public static final list_item:I = 0x7f090107

.field public static final media_actions:I = 0x7f090110

.field public static final middle:I = 0x7f090113

.field public static final multiply:I = 0x7f090118

.field public static final never:I = 0x7f09011b

.field public static final none:I = 0x7f09011d

.field public static final normal:I = 0x7f09011e

.field public static final notification_background:I = 0x7f09011f

.field public static final notification_main_column:I = 0x7f090120

.field public static final notification_main_column_container:I = 0x7f090121

.field public static final parentPanel:I = 0x7f09012b

.field public static final play_button:I = 0x7f090132

.field public static final progress_circular:I = 0x7f090139

.field public static final progress_horizontal:I = 0x7f09013a

.field public static final radio:I = 0x7f09013c

.field public static final rewind_button:I = 0x7f090143

.field public static final rightBottom:I = 0x7f090145

.field public static final rightBottomCrop:I = 0x7f090146

.field public static final rightCenter:I = 0x7f090147

.field public static final rightCenterCrop:I = 0x7f090148

.field public static final rightTop:I = 0x7f090149

.field public static final rightTopCrop:I = 0x7f09014a

.field public static final right_icon:I = 0x7f09014b

.field public static final right_side:I = 0x7f09014c

.field public static final screen:I = 0x7f09015f

.field public static final scrollView:I = 0x7f090163

.field public static final search_badge:I = 0x7f090166

.field public static final search_bar:I = 0x7f090167

.field public static final search_button:I = 0x7f090168

.field public static final search_close_btn:I = 0x7f090169

.field public static final search_edit_frame:I = 0x7f09016a

.field public static final search_go_btn:I = 0x7f09016b

.field public static final search_mag_icon:I = 0x7f09016c

.field public static final search_plate:I = 0x7f09016d

.field public static final search_src_text:I = 0x7f09016e

.field public static final search_voice_btn:I = 0x7f09016f

.field public static final seek_bar:I = 0x7f090170

.field public static final select_dialog_listview:I = 0x7f090171

.field public static final shortcut:I = 0x7f090175

.field public static final showCustom:I = 0x7f090176

.field public static final showHome:I = 0x7f090177

.field public static final showTitle:I = 0x7f090178

.field public static final skip_next_button:I = 0x7f090179

.field public static final skip_previous_button:I = 0x7f09017a

.field public static final spherical_view:I = 0x7f090183

.field public static final split_action_bar:I = 0x7f090184

.field public static final src_atop:I = 0x7f090185

.field public static final src_in:I = 0x7f090186

.field public static final src_over:I = 0x7f090187

.field public static final startInside:I = 0x7f09018a

.field public static final status_bar_latest_event_content:I = 0x7f09018c

.field public static final submit_area:I = 0x7f090192

.field public static final surface_view:I = 0x7f090193

.field public static final tabMode:I = 0x7f090194

.field public static final tag_transition_group:I = 0x7f090195

.field public static final tag_unhandled_key_event_manager:I = 0x7f090196

.field public static final tag_unhandled_key_listeners:I = 0x7f090197

.field public static final text:I = 0x7f09019c

.field public static final text2:I = 0x7f09019d

.field public static final textSpacerNoButtons:I = 0x7f09019e

.field public static final texture_view:I = 0x7f0901a6

.field public static final time:I = 0x7f0901a7

.field public static final title:I = 0x7f0901a8

.field public static final title_template:I = 0x7f0901aa

.field public static final topPanel:I = 0x7f0901ae

.field public static final up:I = 0x7f0901b8

.field public static final useLogo:I = 0x7f0901b9

.field public static final when_playing:I = 0x7f0901c1

.field public static final withText:I = 0x7f0901c4

.field public static final wrap_content:I = 0x7f0901c5

.field public static final zoom:I = 0x7f0901c6


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
