.class public Lexpo/modules/av/video/VideoManager;
.super Lorg/unimodules/core/ExportedModule;
.source "VideoManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lexpo/modules/av/video/VideoManager$VideoViewCallback;
    }
.end annotation


# static fields
.field private static final NAME:Ljava/lang/String; = "ExpoVideoManager"


# instance fields
.field private mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lorg/unimodules/core/ExportedModule;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private tryRunWithVideoView(Ljava/lang/Integer;Lexpo/modules/av/video/VideoManager$VideoViewCallback;Lorg/unimodules/core/Promise;)V
    .locals 2

    .line 101
    iget-object v0, p0, Lexpo/modules/av/video/VideoManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    const-class v1, Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-virtual {v0, v1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    new-instance v1, Lexpo/modules/av/video/VideoManager$2;

    invoke-direct {v1, p0, p2, p3}, Lexpo/modules/av/video/VideoManager$2;-><init>(Lexpo/modules/av/video/VideoManager;Lexpo/modules/av/video/VideoManager$VideoViewCallback;Lorg/unimodules/core/Promise;)V

    const-class p2, Lexpo/modules/av/video/VideoViewWrapper;

    invoke-interface {v0, p1, v1, p2}, Lorg/unimodules/core/interfaces/services/UIManager;->addUIBlock(ILorg/unimodules/core/interfaces/services/UIManager$UIBlock;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public getConstants()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 34
    sget-object v1, Lcom/yqritc/scalablevideoview/ScalableType;->LEFT_TOP:Lcom/yqritc/scalablevideoview/ScalableType;

    invoke-virtual {v1}, Lcom/yqritc/scalablevideoview/ScalableType;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ScaleNone"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v1, Lcom/yqritc/scalablevideoview/ScalableType;->FIT_XY:Lcom/yqritc/scalablevideoview/ScalableType;

    invoke-virtual {v1}, Lcom/yqritc/scalablevideoview/ScalableType;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ScaleToFill"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v1, Lcom/yqritc/scalablevideoview/ScalableType;->FIT_CENTER:Lcom/yqritc/scalablevideoview/ScalableType;

    invoke-virtual {v1}, Lcom/yqritc/scalablevideoview/ScalableType;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ScaleAspectFit"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v1, Lcom/yqritc/scalablevideoview/ScalableType;->CENTER_CROP:Lcom/yqritc/scalablevideoview/ScalableType;

    invoke-virtual {v1}, Lcom/yqritc/scalablevideoview/ScalableType;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ScaleAspectFill"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "ExpoVideoManager"

    return-object v0
.end method

.method public onCreate(Lorg/unimodules/core/ModuleRegistry;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lexpo/modules/av/video/VideoManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    return-void
.end method

.method public setFullscreen(Ljava/lang/Integer;Ljava/lang/Boolean;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 50
    new-instance v0, Lexpo/modules/av/video/VideoManager$1;

    invoke-direct {v0, p0, p3, p2}, Lexpo/modules/av/video/VideoManager$1;-><init>(Lexpo/modules/av/video/VideoManager;Lorg/unimodules/core/Promise;Ljava/lang/Boolean;)V

    invoke-direct {p0, p1, v0, p3}, Lexpo/modules/av/video/VideoManager;->tryRunWithVideoView(Ljava/lang/Integer;Lexpo/modules/av/video/VideoManager$VideoViewCallback;Lorg/unimodules/core/Promise;)V

    return-void
.end method
