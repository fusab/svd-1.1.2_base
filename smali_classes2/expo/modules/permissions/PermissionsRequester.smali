.class public Lexpo/modules/permissions/PermissionsRequester;
.super Ljava/lang/Object;
.source "PermissionsRequester.java"


# static fields
.field private static final PERMISSIONS_REQUEST:I = 0xd


# instance fields
.field private mPermissionManager:Lorg/unimodules/interfaces/permissions/PermissionsManager;


# direct methods
.method constructor <init>(Lorg/unimodules/core/ModuleRegistry;)V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-class v0, Lorg/unimodules/interfaces/permissions/PermissionsManager;

    invoke-virtual {p1, v0}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/unimodules/interfaces/permissions/PermissionsManager;

    iput-object p1, p0, Lexpo/modules/permissions/PermissionsRequester;->mPermissionManager:Lorg/unimodules/interfaces/permissions/PermissionsManager;

    return-void
.end method


# virtual methods
.method askForPermissions([Ljava/lang/String;Lorg/unimodules/interfaces/permissions/PermissionsListener;)Z
    .locals 2

    .line 17
    iget-object v0, p0, Lexpo/modules/permissions/PermissionsRequester;->mPermissionManager:Lorg/unimodules/interfaces/permissions/PermissionsManager;

    if-eqz v0, :cond_0

    const/16 v1, 0xd

    .line 18
    invoke-interface {v0, p1, v1, p2}, Lorg/unimodules/interfaces/permissions/PermissionsManager;->requestPermissions([Ljava/lang/String;ILorg/unimodules/interfaces/permissions/PermissionsListener;)Z

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
