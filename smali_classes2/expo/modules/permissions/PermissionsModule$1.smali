.class Lexpo/modules/permissions/PermissionsModule$1;
.super Ljava/lang/Object;
.source "PermissionsModule.java"

# interfaces
.implements Lorg/unimodules/interfaces/permissions/PermissionsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lexpo/modules/permissions/PermissionsModule;->askForPermissions(Ljava/util/ArrayList;Ljava/util/ArrayList;Lorg/unimodules/core/Promise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lexpo/modules/permissions/PermissionsModule;

.field final synthetic val$promise:Lorg/unimodules/core/Promise;

.field final synthetic val$requestedPermissionsTypes:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lexpo/modules/permissions/PermissionsModule;Lorg/unimodules/core/Promise;Ljava/util/ArrayList;)V
    .locals 0

    .line 170
    iput-object p1, p0, Lexpo/modules/permissions/PermissionsModule$1;->this$0:Lexpo/modules/permissions/PermissionsModule;

    iput-object p2, p0, Lexpo/modules/permissions/PermissionsModule$1;->val$promise:Lorg/unimodules/core/Promise;

    iput-object p3, p0, Lexpo/modules/permissions/PermissionsModule$1;->val$requestedPermissionsTypes:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPermissionResult([Ljava/lang/String;[I)V
    .locals 1

    .line 173
    iget-object p1, p0, Lexpo/modules/permissions/PermissionsModule$1;->val$promise:Lorg/unimodules/core/Promise;

    iget-object p2, p0, Lexpo/modules/permissions/PermissionsModule$1;->this$0:Lexpo/modules/permissions/PermissionsModule;

    iget-object v0, p0, Lexpo/modules/permissions/PermissionsModule$1;->val$requestedPermissionsTypes:Ljava/util/ArrayList;

    invoke-static {p2, v0}, Lexpo/modules/permissions/PermissionsModule;->access$000(Lexpo/modules/permissions/PermissionsModule;Ljava/util/ArrayList;)Landroid/os/Bundle;

    move-result-object p2

    invoke-virtual {p1, p2}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method
