.class public Lexpo/modules/permissions/PermissionsService;
.super Ljava/lang/Object;
.source "PermissionsService.java"

# interfaces
.implements Lorg/unimodules/core/interfaces/InternalModule;
.implements Lorg/unimodules/interfaces/permissions/Permissions;


# instance fields
.field protected mContext:Landroid/content/Context;

.field private mPermissionsRequester:Lexpo/modules/permissions/PermissionsRequester;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lexpo/modules/permissions/PermissionsService;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public askForPermission(Ljava/lang/String;Lorg/unimodules/interfaces/permissions/Permissions$PermissionRequestListener;)V
    .locals 2

    const/4 v0, 0x1

    .line 67
    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    new-instance p1, Lexpo/modules/permissions/PermissionsService$2;

    invoke-direct {p1, p0, p2}, Lexpo/modules/permissions/PermissionsService$2;-><init>(Lexpo/modules/permissions/PermissionsService;Lorg/unimodules/interfaces/permissions/Permissions$PermissionRequestListener;)V

    invoke-virtual {p0, v0, p1}, Lexpo/modules/permissions/PermissionsService;->askForPermissions([Ljava/lang/String;Lorg/unimodules/interfaces/permissions/Permissions$PermissionsRequestListener;)V

    return-void
.end method

.method public askForPermissions([Ljava/lang/String;Lorg/unimodules/interfaces/permissions/Permissions$PermissionsRequestListener;)V
    .locals 2

    .line 50
    iget-object v0, p0, Lexpo/modules/permissions/PermissionsService;->mPermissionsRequester:Lexpo/modules/permissions/PermissionsRequester;

    new-instance v1, Lexpo/modules/permissions/PermissionsService$1;

    invoke-direct {v1, p0, p2}, Lexpo/modules/permissions/PermissionsService$1;-><init>(Lexpo/modules/permissions/PermissionsService;Lorg/unimodules/interfaces/permissions/Permissions$PermissionsRequestListener;)V

    invoke-virtual {v0, p1, v1}, Lexpo/modules/permissions/PermissionsRequester;->askForPermissions([Ljava/lang/String;Lorg/unimodules/interfaces/permissions/PermissionsListener;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    array-length p1, p1

    new-array p1, p1, [I

    const/4 v0, -0x1

    .line 60
    invoke-static {p1, v0}, Ljava/util/Arrays;->fill([II)V

    .line 61
    invoke-interface {p2, p1}, Lorg/unimodules/interfaces/permissions/Permissions$PermissionsRequestListener;->onPermissionsResult([I)V

    :cond_0
    return-void
.end method

.method public getExportedInterfaces()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation

    .line 26
    const-class v0, Lorg/unimodules/interfaces/permissions/Permissions;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPermission(Ljava/lang/String;)I
    .locals 1

    .line 45
    iget-object v0, p0, Lexpo/modules/permissions/PermissionsService;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Landroidx/core/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getPermissions([Ljava/lang/String;)[I
    .locals 3

    .line 36
    array-length v0, p1

    new-array v0, v0, [I

    const/4 v1, 0x0

    .line 37
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    .line 38
    aget-object v2, p1, v1

    invoke-virtual {p0, v2}, Lexpo/modules/permissions/PermissionsService;->getPermission(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public hasPermissions([Ljava/lang/String;)Z
    .locals 2

    .line 78
    invoke-virtual {p0, p1}, Lexpo/modules/permissions/PermissionsService;->getPermissions([Ljava/lang/String;)[I

    move-result-object v0

    .line 79
    array-length p1, p1

    new-array p1, p1, [I

    const/4 v1, 0x0

    .line 80
    invoke-static {p1, v1}, Ljava/util/Arrays;->fill([II)V

    .line 81
    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result p1

    return p1
.end method

.method public onCreate(Lorg/unimodules/core/ModuleRegistry;)V
    .locals 1

    .line 31
    new-instance v0, Lexpo/modules/permissions/PermissionsRequester;

    invoke-direct {v0, p1}, Lexpo/modules/permissions/PermissionsRequester;-><init>(Lorg/unimodules/core/ModuleRegistry;)V

    iput-object v0, p0, Lexpo/modules/permissions/PermissionsService;->mPermissionsRequester:Lexpo/modules/permissions/PermissionsRequester;

    return-void
.end method

.method public synthetic onDestroy()V
    .locals 0

    invoke-static {p0}, Lorg/unimodules/core/interfaces/RegistryLifecycleListener$-CC;->$default$onDestroy(Lorg/unimodules/core/interfaces/RegistryLifecycleListener;)V

    return-void
.end method
