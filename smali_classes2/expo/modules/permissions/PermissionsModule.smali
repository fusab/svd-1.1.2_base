.class public Lexpo/modules/permissions/PermissionsModule;
.super Lorg/unimodules/core/ExportedModule;
.source "PermissionsModule.java"

# interfaces
.implements Lorg/unimodules/core/interfaces/LifecycleEventListener;


# static fields
.field private static final DENIED_VALUE:Ljava/lang/String; = "denied"

.field private static final ERROR_TAG:Ljava/lang/String; = "E_PERMISSIONS"

.field private static final EXPIRES_KEY:Ljava/lang/String; = "expires"

.field private static final GRANTED_VALUE:Ljava/lang/String; = "granted"

.field private static PERMISSION_EXPIRES_NEVER:Ljava/lang/String; = "never"

.field private static final STATUS_KEY:Ljava/lang/String; = "status"

.field private static final UNDETERMINED_VALUE:Ljava/lang/String; = "undetermined"


# instance fields
.field private mActivityProvider:Lorg/unimodules/core/interfaces/ActivityProvider;

.field private mAskAsyncPermissionsTypesToBeAsked:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAskAsyncPromise:Lorg/unimodules/core/Promise;

.field private mAskAsyncRequestedPermissionsTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPermissions:Lorg/unimodules/interfaces/permissions/Permissions;

.field private mPermissionsAskedFor:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPermissionsRequester:Lexpo/modules/permissions/PermissionsRequester;

.field private mWritingPermissionBeingAsked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lorg/unimodules/core/ExportedModule;-><init>(Landroid/content/Context;)V

    .line 45
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lexpo/modules/permissions/PermissionsModule;->mPermissionsAskedFor:Ljava/util/Set;

    const/4 p1, 0x0

    .line 48
    iput-boolean p1, p0, Lexpo/modules/permissions/PermissionsModule;->mWritingPermissionBeingAsked:Z

    const/4 p1, 0x0

    .line 49
    iput-object p1, p0, Lexpo/modules/permissions/PermissionsModule;->mAskAsyncPromise:Lorg/unimodules/core/Promise;

    .line 50
    iput-object p1, p0, Lexpo/modules/permissions/PermissionsModule;->mAskAsyncRequestedPermissionsTypes:Ljava/util/ArrayList;

    .line 51
    iput-object p1, p0, Lexpo/modules/permissions/PermissionsModule;->mAskAsyncPermissionsTypesToBeAsked:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$000(Lexpo/modules/permissions/PermissionsModule;Ljava/util/ArrayList;)Landroid/os/Bundle;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 33
    invoke-direct {p0, p1}, Lexpo/modules/permissions/PermissionsModule;->getPermissions(Ljava/util/ArrayList;)Landroid/os/Bundle;

    move-result-object p0

    return-object p0
.end method

.method private arePermissionsGranted([Ljava/lang/String;)Z
    .locals 4

    .line 361
    iget-object v0, p0, Lexpo/modules/permissions/PermissionsModule;->mPermissions:Lorg/unimodules/interfaces/permissions/Permissions;

    if-eqz v0, :cond_3

    .line 362
    invoke-interface {v0, p1}, Lorg/unimodules/interfaces/permissions/Permissions;->getPermissions([Ljava/lang/String;)[I

    move-result-object v0

    .line 363
    array-length p1, p1

    array-length v1, v0

    const/4 v2, 0x0

    if-eq p1, v1, :cond_0

    return v2

    .line 366
    :cond_0
    array-length p1, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_2

    aget v3, v0, v1

    if-eqz v3, :cond_1

    return v2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x1

    return p1

    .line 373
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No Permissions module present."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private askForPermissions(Ljava/util/ArrayList;Ljava/util/ArrayList;Lorg/unimodules/core/Promise;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Lorg/unimodules/core/Promise;",
            ")V"
        }
    .end annotation

    .line 168
    iget-object v0, p0, Lexpo/modules/permissions/PermissionsModule;->mPermissionsRequester:Lexpo/modules/permissions/PermissionsRequester;

    .line 169
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Ljava/lang/String;

    new-instance v1, Lexpo/modules/permissions/PermissionsModule$1;

    invoke-direct {v1, p0, p3, p1}, Lexpo/modules/permissions/PermissionsModule$1;-><init>(Lexpo/modules/permissions/PermissionsModule;Lorg/unimodules/core/Promise;Ljava/util/ArrayList;)V

    .line 168
    invoke-virtual {v0, p2, v1}, Lexpo/modules/permissions/PermissionsRequester;->askForPermissions([Ljava/lang/String;Lorg/unimodules/interfaces/permissions/PermissionsListener;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "E_PERMISSIONS_UNAVAILABLE"

    const-string p2, "Permissions module is null. Are you sure all the installed Expo modules are properly linked?"

    .line 178
    invoke-virtual {p3, p1, p2}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private askForWriteSettingsPermissionFirst()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .line 408
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.action.MANAGE_WRITE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 409
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "package:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lexpo/modules/permissions/PermissionsModule;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    .line 410
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/4 v1, 0x1

    .line 411
    iput-boolean v1, p0, Lexpo/modules/permissions/PermissionsModule;->mWritingPermissionBeingAsked:Z

    .line 412
    invoke-virtual {p0}, Lexpo/modules/permissions/PermissionsModule;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private getCalendarPermissions()Landroid/os/Bundle;
    .locals 4

    const-string v0, "status"

    .line 324
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    :try_start_0
    const-string v2, "android.permission.READ_CALENDAR"

    const-string v3, "android.permission.WRITE_CALENDAR"

    .line 326
    filled-new-array {v2, v3}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lexpo/modules/permissions/PermissionsModule;->arePermissionsGranted([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "granted"

    .line 329
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 330
    :cond_0
    iget-object v2, p0, Lexpo/modules/permissions/PermissionsModule;->mPermissionsAskedFor:Ljava/util/Set;

    const-string v3, "calendar"

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "denied"

    if-eqz v2, :cond_1

    .line 331
    :try_start_1
    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 333
    :cond_1
    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    const-string v2, "undetermined"

    .line 336
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    :goto_0
    sget-object v0, Lexpo/modules/permissions/PermissionsModule;->PERMISSION_EXPIRES_NEVER:Ljava/lang/String;

    const-string v2, "expires"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method private getCameraRollPermissions()Landroid/os/Bundle;
    .locals 5

    const-string v0, "undetermined"

    const-string v1, "status"

    .line 302
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    :try_start_0
    const-string v3, "android.permission.READ_EXTERNAL_STORAGE"

    const-string v4, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 305
    filled-new-array {v3, v4}, [Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lexpo/modules/permissions/PermissionsModule;->arePermissionsGranted([Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "granted"

    .line 308
    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 309
    :cond_0
    iget-object v3, p0, Lexpo/modules/permissions/PermissionsModule;->mPermissionsAskedFor:Ljava/util/Set;

    const-string v4, "cameraRoll"

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "denied"

    .line 310
    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 312
    :cond_1
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 315
    :catch_0
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    :goto_0
    sget-object v0, Lexpo/modules/permissions/PermissionsModule;->PERMISSION_EXPIRES_NEVER:Ljava/lang/String;

    const-string v1, "expires"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method private getLocationPermissions()Landroid/os/Bundle;
    .locals 6

    const-string v0, "undetermined"

    const-string v1, "status"

    .line 230
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "none"

    :try_start_0
    const-string v4, "android.permission.ACCESS_FINE_LOCATION"

    .line 233
    invoke-direct {p0, v4}, Lexpo/modules/permissions/PermissionsModule;->isPermissionGranted(Ljava/lang/String;)Z

    move-result v4
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v5, "granted"

    if-eqz v4, :cond_0

    .line 234
    :try_start_1
    invoke-virtual {v2, v1, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "fine"

    :goto_0
    move-object v3, v0

    goto :goto_1

    :cond_0
    const-string v4, "android.permission.ACCESS_COARSE_LOCATION"

    .line 236
    invoke-direct {p0, v4}, Lexpo/modules/permissions/PermissionsModule;->isPermissionGranted(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 237
    invoke-virtual {v2, v1, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "coarse"

    goto :goto_0

    .line 239
    :cond_1
    iget-object v4, p0, Lexpo/modules/permissions/PermissionsModule;->mPermissionsAskedFor:Ljava/util/Set;

    const-string v5, "location"

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "denied"

    .line 240
    invoke-virtual {v2, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 242
    :cond_2
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 245
    :catch_0
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    :goto_1
    sget-object v0, Lexpo/modules/permissions/PermissionsModule;->PERMISSION_EXPIRES_NEVER:Ljava/lang/String;

    const-string v1, "expires"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "scope"

    .line 250
    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "android"

    .line 251
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v2
.end method

.method private getNotificationPermissions()Landroid/os/Bundle;
    .locals 3

    .line 222
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 223
    invoke-virtual {p0}, Lexpo/modules/permissions/PermissionsModule;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroidx/core/app/NotificationManagerCompat;->from(Landroid/content/Context;)Landroidx/core/app/NotificationManagerCompat;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/core/app/NotificationManagerCompat;->areNotificationsEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "granted"

    goto :goto_0

    :cond_0
    const-string v1, "denied"

    :goto_0
    const-string v2, "status"

    .line 224
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    sget-object v1, Lexpo/modules/permissions/PermissionsModule;->PERMISSION_EXPIRES_NEVER:Ljava/lang/String;

    const-string v2, "expires"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private getPermission(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 191
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v0, "location"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto/16 :goto_1

    :sswitch_1
    const-string v0, "systemBrightness"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_2
    const-string v0, "notifications"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_3
    const-string v0, "audioRecording"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_4
    const-string v0, "reminders"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xa

    goto :goto_1

    :sswitch_5
    const-string v0, "SMS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    goto :goto_1

    :sswitch_6
    const-string v0, "calendar"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    goto :goto_1

    :sswitch_7
    const-string v0, "contacts"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_8
    const-string v0, "userFacingNotifications"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_9
    const-string v0, "camera"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_a
    const-string v0, "cameraRoll"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    goto :goto_1

    :cond_0
    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 217
    new-instance v0, Ljava/lang/IllegalStateException;

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    const-string p1, "Unrecognized permission type: %s"

    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 212
    :pswitch_0
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    const-string v0, "status"

    const-string v1, "granted"

    .line 213
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    sget-object v0, Lexpo/modules/permissions/PermissionsModule;->PERMISSION_EXPIRES_NEVER:Ljava/lang/String;

    const-string v1, "expires"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1

    :pswitch_1
    const-string p1, "android.permission.READ_SMS"

    .line 210
    invoke-direct {p0, p1}, Lexpo/modules/permissions/PermissionsModule;->getSimplePermission(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    return-object p1

    .line 208
    :pswitch_2
    invoke-direct {p0}, Lexpo/modules/permissions/PermissionsModule;->getCalendarPermissions()Landroid/os/Bundle;

    move-result-object p1

    return-object p1

    .line 206
    :pswitch_3
    invoke-direct {p0}, Lexpo/modules/permissions/PermissionsModule;->getCameraRollPermissions()Landroid/os/Bundle;

    move-result-object p1

    return-object p1

    .line 204
    :pswitch_4
    invoke-direct {p0}, Lexpo/modules/permissions/PermissionsModule;->getWriteSettingsPermission()Landroid/os/Bundle;

    move-result-object p1

    return-object p1

    :pswitch_5
    const-string p1, "android.permission.RECORD_AUDIO"

    .line 202
    invoke-direct {p0, p1}, Lexpo/modules/permissions/PermissionsModule;->getSimplePermission(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    return-object p1

    :pswitch_6
    const-string p1, "android.permission.READ_CONTACTS"

    .line 200
    invoke-direct {p0, p1}, Lexpo/modules/permissions/PermissionsModule;->getSimplePermission(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    return-object p1

    :pswitch_7
    const-string p1, "android.permission.CAMERA"

    .line 198
    invoke-direct {p0, p1}, Lexpo/modules/permissions/PermissionsModule;->getSimplePermission(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    return-object p1

    .line 196
    :pswitch_8
    invoke-direct {p0}, Lexpo/modules/permissions/PermissionsModule;->getLocationPermissions()Landroid/os/Bundle;

    move-result-object p1

    return-object p1

    .line 194
    :pswitch_9
    invoke-direct {p0}, Lexpo/modules/permissions/PermissionsModule;->getNotificationPermissions()Landroid/os/Bundle;

    move-result-object p1

    return-object p1

    :sswitch_data_0
    .sparse-switch
        -0x77ef62be -> :sswitch_a
        -0x51863cdb -> :sswitch_9
        -0x247bfe9d -> :sswitch_8
        -0x21d29fad -> :sswitch_7
        -0xaa104c2 -> :sswitch_6
        0x14139 -> :sswitch_5
        0x41c14e41 -> :sswitch_4
        0x489ff2bb -> :sswitch_3
        0x4bd694e8 -> :sswitch_2
        0x6326cbe0 -> :sswitch_1
        0x714f9fb5 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getPermissions(Ljava/util/ArrayList;)Landroid/os/Bundle;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 183
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 184
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 185
    invoke-direct {p0, v1}, Lexpo/modules/permissions/PermissionsModule;->getPermission(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private getSimplePermission(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 4

    const-string v0, "undetermined"

    const-string v1, "status"

    .line 282
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 285
    :try_start_0
    invoke-direct {p0, p1}, Lexpo/modules/permissions/PermissionsModule;->isPermissionGranted(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string p1, "granted"

    .line 286
    invoke-virtual {v2, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 287
    :cond_0
    iget-object v3, p0, Lexpo/modules/permissions/PermissionsModule;->mPermissionsAskedFor:Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "denied"

    .line 288
    invoke-virtual {v2, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 290
    :cond_1
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 293
    :catch_0
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    :goto_0
    sget-object p1, Lexpo/modules/permissions/PermissionsModule;->PERMISSION_EXPIRES_NEVER:Ljava/lang/String;

    const-string v0, "expires"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method private getWriteSettingsPermission()Landroid/os/Bundle;
    .locals 5

    .line 258
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 259
    sget-object v1, Lexpo/modules/permissions/PermissionsModule;->PERMISSION_EXPIRES_NEVER:Ljava/lang/String;

    const-string v2, "expires"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const-string v2, "granted"

    const-string v3, "status"

    const/16 v4, 0x17

    if-lt v1, v4, :cond_2

    .line 262
    iget-object v1, p0, Lexpo/modules/permissions/PermissionsModule;->mActivityProvider:Lorg/unimodules/core/interfaces/ActivityProvider;

    invoke-interface {v1}, Lorg/unimodules/core/interfaces/ActivityProvider;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/provider/Settings$System;->canWrite(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 263
    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 264
    :cond_0
    iget-object v1, p0, Lexpo/modules/permissions/PermissionsModule;->mPermissionsAskedFor:Ljava/util/Set;

    const-string v2, "systemBrightness"

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "denied"

    .line 265
    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v1, "undetermined"

    .line 267
    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 270
    :cond_2
    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method private isPermissionGranted(Ljava/lang/String;)Z
    .locals 1

    .line 348
    iget-object v0, p0, Lexpo/modules/permissions/PermissionsModule;->mPermissions:Lorg/unimodules/interfaces/permissions/Permissions;

    if-eqz v0, :cond_1

    .line 349
    invoke-interface {v0, p1}, Lorg/unimodules/interfaces/permissions/Permissions;->getPermission(Ljava/lang/String;)I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1

    .line 352
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No Permissions module present."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private isPermissionPresentInManifest(Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    .line 382
    :try_start_0
    invoke-virtual {p0}, Lexpo/modules/permissions/PermissionsModule;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 383
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0x1000

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 385
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 386
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 387
    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    :cond_0
    return v0
.end method


# virtual methods
.method public askAsync(Ljava/util/ArrayList;Lorg/unimodules/core/Promise;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Lorg/unimodules/core/Promise;",
            ")V"
        }
    .end annotation

    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    const-string v0, "status"

    .line 81
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 83
    :try_start_0
    invoke-direct {p0, p1}, Lexpo/modules/permissions/PermissionsModule;->getPermissions(Ljava/util/ArrayList;)Landroid/os/Bundle;

    move-result-object v2

    .line 86
    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 87
    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    .line 88
    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 89
    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "granted"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 90
    invoke-interface {v1, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 95
    :cond_1
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 96
    invoke-virtual {p2, v2}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 105
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 106
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    :pswitch_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const-string v4, "systemBrightness"

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v5, -0x1

    .line 107
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x1

    sparse-switch v6, :sswitch_data_0

    goto/16 :goto_2

    :sswitch_0
    const-string v4, "location"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v5, 0x4

    goto :goto_2

    :sswitch_1
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v5, 0x3

    goto :goto_2

    :sswitch_2
    const-string v4, "notifications"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v5, 0x0

    goto :goto_2

    :sswitch_3
    const-string v4, "audioRecording"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v5, 0x7

    goto :goto_2

    :sswitch_4
    const-string v4, "reminders"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v5, 0x2

    goto :goto_2

    :sswitch_5
    const-string v4, "calendar"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/16 v5, 0x9

    goto :goto_2

    :sswitch_6
    const-string v4, "contacts"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v5, 0x6

    goto :goto_2

    :sswitch_7
    const-string v4, "userFacingNotifications"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v5, 0x1

    goto :goto_2

    :sswitch_8
    const-string v4, "camera"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v5, 0x5

    goto :goto_2

    :sswitch_9
    const-string v4, "cameraRoll"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/16 v5, 0x8

    :cond_4
    :goto_2
    packed-switch v5, :pswitch_data_0

    .line 142
    new-array p1, v8, [Ljava/lang/Object;

    aput-object v3, p1, v7

    const-string v0, "Cannot request permission: %s"

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "E_PERMISSIONS_UNSUPPORTED"

    invoke-virtual {p2, v0, p1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_1
    const-string v3, "android.permission.READ_CALENDAR"

    .line 138
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v3, "android.permission.WRITE_CALENDAR"

    .line 139
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :pswitch_2
    const-string v3, "android.permission.READ_EXTERNAL_STORAGE"

    .line 134
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v3, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 135
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :pswitch_3
    const-string v3, "android.permission.RECORD_AUDIO"

    .line 131
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :pswitch_4
    const-string v3, "android.permission.READ_CONTACTS"

    .line 124
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v3, "android.permission.WRITE_CONTACTS"

    .line 125
    invoke-direct {p0, v3}, Lexpo/modules/permissions/PermissionsModule;->isPermissionPresentInManifest(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 127
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :pswitch_5
    const-string v3, "android.permission.CAMERA"

    .line 121
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :pswitch_6
    const-string v3, "android.permission.ACCESS_FINE_LOCATION"

    .line 117
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v3, "android.permission.ACCESS_COARSE_LOCATION"

    .line 118
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 148
    :cond_5
    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v2, v3, :cond_7

    .line 149
    iget-object v1, p0, Lexpo/modules/permissions/PermissionsModule;->mAskAsyncPromise:Lorg/unimodules/core/Promise;

    if-eqz v1, :cond_6

    const-string p1, "E_PERMISSIONS_ASKING_IN_PROGRESS"

    const-string v0, "Different asking for permissions in progress. Await the old request and then try again."

    .line 150
    invoke-virtual {p2, p1, v0}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 153
    :cond_6
    iput-object p2, p0, Lexpo/modules/permissions/PermissionsModule;->mAskAsyncPromise:Lorg/unimodules/core/Promise;

    .line 154
    iput-object p1, p0, Lexpo/modules/permissions/PermissionsModule;->mAskAsyncRequestedPermissionsTypes:Ljava/util/ArrayList;

    .line 155
    iput-object v0, p0, Lexpo/modules/permissions/PermissionsModule;->mAskAsyncPermissionsTypesToBeAsked:Ljava/util/ArrayList;

    .line 156
    invoke-direct {p0}, Lexpo/modules/permissions/PermissionsModule;->askForWriteSettingsPermissionFirst()V

    .line 157
    iget-object p1, p0, Lexpo/modules/permissions/PermissionsModule;->mPermissionsAskedFor:Ljava/util/Set;

    invoke-interface {p1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void

    .line 161
    :cond_7
    iget-object v2, p0, Lexpo/modules/permissions/PermissionsModule;->mPermissionsAskedFor:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 162
    invoke-direct {p0, p1, v0, p2}, Lexpo/modules/permissions/PermissionsModule;->askForPermissions(Ljava/util/ArrayList;Ljava/util/ArrayList;Lorg/unimodules/core/Promise;)V

    return-void

    :catch_0
    move-exception p1

    const-string v0, "E_PERMISSIONS_UNKNOWN"

    .line 100
    invoke-virtual {p2, v0, p1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x77ef62be -> :sswitch_9
        -0x51863cdb -> :sswitch_8
        -0x247bfe9d -> :sswitch_7
        -0x21d29fad -> :sswitch_6
        -0xaa104c2 -> :sswitch_5
        0x41c14e41 -> :sswitch_4
        0x489ff2bb -> :sswitch_3
        0x4bd694e8 -> :sswitch_2
        0x6326cbe0 -> :sswitch_1
        0x714f9fb5 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public getAsync(Ljava/util/ArrayList;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Lorg/unimodules/core/Promise;",
            ")V"
        }
    .end annotation

    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 73
    :try_start_0
    invoke-direct {p0, p1}, Lexpo/modules/permissions/PermissionsModule;->getPermissions(Ljava/util/ArrayList;)Landroid/os/Bundle;

    move-result-object p1

    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "E_PERMISSIONS_UNKNOWN"

    .line 75
    invoke-virtual {p2, v0, p1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "ExpoPermissions"

    return-object v0
.end method

.method public onCreate(Lorg/unimodules/core/ModuleRegistry;)V
    .locals 1

    .line 59
    new-instance v0, Lexpo/modules/permissions/PermissionsRequester;

    invoke-direct {v0, p1}, Lexpo/modules/permissions/PermissionsRequester;-><init>(Lorg/unimodules/core/ModuleRegistry;)V

    iput-object v0, p0, Lexpo/modules/permissions/PermissionsModule;->mPermissionsRequester:Lexpo/modules/permissions/PermissionsRequester;

    .line 60
    const-class v0, Lorg/unimodules/interfaces/permissions/Permissions;

    invoke-virtual {p1, v0}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/interfaces/permissions/Permissions;

    iput-object v0, p0, Lexpo/modules/permissions/PermissionsModule;->mPermissions:Lorg/unimodules/interfaces/permissions/Permissions;

    .line 61
    const-class v0, Lorg/unimodules/core/interfaces/ActivityProvider;

    invoke-virtual {p1, v0}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/ActivityProvider;

    iput-object v0, p0, Lexpo/modules/permissions/PermissionsModule;->mActivityProvider:Lorg/unimodules/core/interfaces/ActivityProvider;

    .line 62
    const-class v0, Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-virtual {p1, v0}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-interface {p1, p0}, Lorg/unimodules/core/interfaces/services/UIManager;->registerLifecycleEventListener(Lorg/unimodules/core/interfaces/LifecycleEventListener;)V

    return-void
.end method

.method public onHostDestroy()V
    .locals 0

    return-void
.end method

.method public onHostPause()V
    .locals 0

    return-void
.end method

.method public onHostResume()V
    .locals 4

    .line 417
    iget-boolean v0, p0, Lexpo/modules/permissions/PermissionsModule;->mWritingPermissionBeingAsked:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 420
    iput-boolean v0, p0, Lexpo/modules/permissions/PermissionsModule;->mWritingPermissionBeingAsked:Z

    .line 423
    iget-object v0, p0, Lexpo/modules/permissions/PermissionsModule;->mAskAsyncPromise:Lorg/unimodules/core/Promise;

    .line 424
    iget-object v1, p0, Lexpo/modules/permissions/PermissionsModule;->mAskAsyncRequestedPermissionsTypes:Ljava/util/ArrayList;

    .line 425
    iget-object v2, p0, Lexpo/modules/permissions/PermissionsModule;->mAskAsyncPermissionsTypesToBeAsked:Ljava/util/ArrayList;

    const/4 v3, 0x0

    .line 426
    iput-object v3, p0, Lexpo/modules/permissions/PermissionsModule;->mAskAsyncPromise:Lorg/unimodules/core/Promise;

    .line 427
    iput-object v3, p0, Lexpo/modules/permissions/PermissionsModule;->mAskAsyncRequestedPermissionsTypes:Ljava/util/ArrayList;

    .line 428
    iput-object v3, p0, Lexpo/modules/permissions/PermissionsModule;->mAskAsyncPermissionsTypesToBeAsked:Ljava/util/ArrayList;

    .line 431
    invoke-direct {p0, v1, v2, v0}, Lexpo/modules/permissions/PermissionsModule;->askForPermissions(Ljava/util/ArrayList;Ljava/util/ArrayList;Lorg/unimodules/core/Promise;)V

    return-void
.end method
