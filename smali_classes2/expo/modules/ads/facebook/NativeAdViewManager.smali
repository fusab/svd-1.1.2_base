.class public Lexpo/modules/ads/facebook/NativeAdViewManager;
.super Lorg/unimodules/core/ViewManager;
.source "NativeAdViewManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/unimodules/core/ViewManager<",
        "Lexpo/modules/ads/facebook/NativeAdView;",
        ">;"
    }
.end annotation


# static fields
.field private static NAME:Ljava/lang/String; = "CTKNativeAd"


# instance fields
.field private mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Lorg/unimodules/core/ViewManager;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic createViewInstance(Landroid/content/Context;)Landroid/view/View;
    .locals 0

    .line 14
    invoke-virtual {p0, p1}, Lexpo/modules/ads/facebook/NativeAdViewManager;->createViewInstance(Landroid/content/Context;)Lexpo/modules/ads/facebook/NativeAdView;

    move-result-object p1

    return-object p1
.end method

.method public createViewInstance(Landroid/content/Context;)Lexpo/modules/ads/facebook/NativeAdView;
    .locals 2

    .line 25
    new-instance v0, Lexpo/modules/ads/facebook/NativeAdView;

    iget-object v1, p0, Lexpo/modules/ads/facebook/NativeAdViewManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    invoke-direct {v0, p1, v1}, Lexpo/modules/ads/facebook/NativeAdView;-><init>(Landroid/content/Context;Lorg/unimodules/core/ModuleRegistry;)V

    return-object v0
.end method

.method public getExportedEventNames()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "onAdLoaded"

    const-string v1, "onAdFailed"

    .line 41
    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 20
    sget-object v0, Lexpo/modules/ads/facebook/NativeAdViewManager;->NAME:Ljava/lang/String;

    return-object v0
.end method

.method public getViewManagerType()Lorg/unimodules/core/ViewManager$ViewManagerType;
    .locals 1

    .line 30
    sget-object v0, Lorg/unimodules/core/ViewManager$ViewManagerType;->GROUP:Lorg/unimodules/core/ViewManager$ViewManagerType;

    return-object v0
.end method

.method public onCreate(Lorg/unimodules/core/ModuleRegistry;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lexpo/modules/ads/facebook/NativeAdViewManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    return-void
.end method

.method public setAdsManager(Lexpo/modules/ads/facebook/NativeAdView;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoProp;
        name = "adsManager"
    .end annotation

    .line 35
    invoke-virtual {p1}, Lexpo/modules/ads/facebook/NativeAdView;->getModuleRegistry()Lorg/unimodules/core/ModuleRegistry;

    move-result-object v0

    const-class v1, Lexpo/modules/ads/facebook/NativeAdManager;

    invoke-virtual {v0, v1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexpo/modules/ads/facebook/NativeAdManager;

    invoke-virtual {v0, p2}, Lexpo/modules/ads/facebook/NativeAdManager;->getFBAdsManager(Ljava/lang/String;)Lcom/facebook/ads/NativeAdsManager;

    move-result-object p2

    .line 36
    invoke-virtual {p2}, Lcom/facebook/ads/NativeAdsManager;->nextNativeAd()Lcom/facebook/ads/NativeAd;

    move-result-object p2

    invoke-virtual {p1, p2}, Lexpo/modules/ads/facebook/NativeAdView;->setNativeAd(Lcom/facebook/ads/NativeAd;)V

    return-void
.end method
