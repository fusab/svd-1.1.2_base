.class Lexpo/modules/ads/facebook/NativeAdLayout$1;
.super Ljava/lang/Object;
.source "NativeAdLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lexpo/modules/ads/facebook/NativeAdLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lexpo/modules/ads/facebook/NativeAdLayout;


# direct methods
.method constructor <init>(Lexpo/modules/ads/facebook/NativeAdLayout;)V
    .locals 0

    .line 26
    iput-object p1, p0, Lexpo/modules/ads/facebook/NativeAdLayout$1;->this$0:Lexpo/modules/ads/facebook/NativeAdLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 29
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdLayout$1;->this$0:Lexpo/modules/ads/facebook/NativeAdLayout;

    .line 30
    invoke-virtual {v0}, Lexpo/modules/ads/facebook/NativeAdLayout;->getWidth()I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v3, p0, Lexpo/modules/ads/facebook/NativeAdLayout$1;->this$0:Lexpo/modules/ads/facebook/NativeAdLayout;

    .line 31
    invoke-virtual {v3}, Lexpo/modules/ads/facebook/NativeAdLayout;->getHeight()I

    move-result v3

    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 29
    invoke-virtual {v0, v1, v2}, Lexpo/modules/ads/facebook/NativeAdLayout;->measure(II)V

    .line 32
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdLayout$1;->this$0:Lexpo/modules/ads/facebook/NativeAdLayout;

    invoke-virtual {v0}, Lexpo/modules/ads/facebook/NativeAdLayout;->getLeft()I

    move-result v1

    iget-object v2, p0, Lexpo/modules/ads/facebook/NativeAdLayout$1;->this$0:Lexpo/modules/ads/facebook/NativeAdLayout;

    invoke-virtual {v2}, Lexpo/modules/ads/facebook/NativeAdLayout;->getTop()I

    move-result v2

    iget-object v3, p0, Lexpo/modules/ads/facebook/NativeAdLayout$1;->this$0:Lexpo/modules/ads/facebook/NativeAdLayout;

    invoke-virtual {v3}, Lexpo/modules/ads/facebook/NativeAdLayout;->getRight()I

    move-result v3

    iget-object v4, p0, Lexpo/modules/ads/facebook/NativeAdLayout$1;->this$0:Lexpo/modules/ads/facebook/NativeAdLayout;

    invoke-virtual {v4}, Lexpo/modules/ads/facebook/NativeAdLayout;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lexpo/modules/ads/facebook/NativeAdLayout;->layout(IIII)V

    return-void
.end method
