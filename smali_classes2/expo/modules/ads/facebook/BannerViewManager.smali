.class public Lexpo/modules/ads/facebook/BannerViewManager;
.super Lorg/unimodules/core/ViewManager;
.source "BannerViewManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/unimodules/core/ViewManager<",
        "Lexpo/modules/ads/facebook/BannerView;",
        ">;"
    }
.end annotation


# instance fields
.field private mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Lorg/unimodules/core/ViewManager;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic createViewInstance(Landroid/content/Context;)Landroid/view/View;
    .locals 0

    .line 14
    invoke-virtual {p0, p1}, Lexpo/modules/ads/facebook/BannerViewManager;->createViewInstance(Landroid/content/Context;)Lexpo/modules/ads/facebook/BannerView;

    move-result-object p1

    return-object p1
.end method

.method public createViewInstance(Landroid/content/Context;)Lexpo/modules/ads/facebook/BannerView;
    .locals 2

    .line 41
    new-instance v0, Lexpo/modules/ads/facebook/BannerView;

    iget-object v1, p0, Lexpo/modules/ads/facebook/BannerViewManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    invoke-direct {v0, p1, v1}, Lexpo/modules/ads/facebook/BannerView;-><init>(Landroid/content/Context;Lorg/unimodules/core/ModuleRegistry;)V

    return-object v0
.end method

.method public getExportedEventNames()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "onAdPress"

    const-string v1, "onAdError"

    const-string v2, "onLoggingImpression"

    .line 51
    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "CTKBannerView"

    return-object v0
.end method

.method public getViewManagerType()Lorg/unimodules/core/ViewManager$ViewManagerType;
    .locals 1

    .line 46
    sget-object v0, Lorg/unimodules/core/ViewManager$ViewManagerType;->SIMPLE:Lorg/unimodules/core/ViewManager$ViewManagerType;

    return-object v0
.end method

.method public onCreate(Lorg/unimodules/core/ModuleRegistry;)V
    .locals 0

    .line 61
    iput-object p1, p0, Lexpo/modules/ads/facebook/BannerViewManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    return-void
.end method

.method public setPlacementId(Lexpo/modules/ads/facebook/BannerView;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoProp;
        name = "placementId"
    .end annotation

    .line 19
    invoke-virtual {p1, p2}, Lexpo/modules/ads/facebook/BannerView;->setPlacementId(Ljava/lang/String;)V

    return-void
.end method

.method public setSize(Lexpo/modules/ads/facebook/BannerView;I)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoProp;
        name = "size"
    .end annotation

    const/16 v0, 0x5a

    if-eq p2, v0, :cond_1

    const/16 v0, 0xfa

    if-eq p2, v0, :cond_0

    .line 34
    sget-object p2, Lcom/facebook/ads/AdSize;->BANNER_HEIGHT_50:Lcom/facebook/ads/AdSize;

    goto :goto_0

    .line 30
    :cond_0
    sget-object p2, Lcom/facebook/ads/AdSize;->RECTANGLE_HEIGHT_250:Lcom/facebook/ads/AdSize;

    goto :goto_0

    .line 27
    :cond_1
    sget-object p2, Lcom/facebook/ads/AdSize;->BANNER_HEIGHT_90:Lcom/facebook/ads/AdSize;

    .line 36
    :goto_0
    invoke-virtual {p1, p2}, Lexpo/modules/ads/facebook/BannerView;->setSize(Lcom/facebook/ads/AdSize;)V

    return-void
.end method
