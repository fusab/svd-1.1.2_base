.class public Lexpo/modules/ads/facebook/AdSettingsManager;
.super Lorg/unimodules/core/ExportedModule;
.source "AdSettingsManager.java"

# interfaces
.implements Lorg/unimodules/core/interfaces/LifecycleEventListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "expo.modules.ads.facebook.AdSettingsManager"


# instance fields
.field private mIsChildDirected:Z

.field private mMediationService:Ljava/lang/String;

.field private mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

.field private mTestDeviceHashes:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUrlPrefix:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lorg/unimodules/core/ExportedModule;-><init>(Landroid/content/Context;)V

    .line 24
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lexpo/modules/ads/facebook/AdSettingsManager;->mTestDeviceHashes:Ljava/util/HashSet;

    const/4 p1, 0x0

    .line 25
    iput-boolean p1, p0, Lexpo/modules/ads/facebook/AdSettingsManager;->mIsChildDirected:Z

    const/4 p1, 0x0

    .line 26
    iput-object p1, p0, Lexpo/modules/ads/facebook/AdSettingsManager;->mMediationService:Ljava/lang/String;

    .line 27
    iput-object p1, p0, Lexpo/modules/ads/facebook/AdSettingsManager;->mUrlPrefix:Ljava/lang/String;

    return-void
.end method

.method private clearSettings()V
    .locals 1

    .line 110
    invoke-static {}, Lcom/facebook/ads/AdSettings;->clearTestDevices()V

    const/4 v0, 0x0

    .line 111
    invoke-static {v0}, Lcom/facebook/ads/AdSettings;->setIsChildDirected(Z)V

    const/4 v0, 0x0

    .line 112
    invoke-static {v0}, Lcom/facebook/ads/AdSettings;->setMediationService(Ljava/lang/String;)V

    .line 113
    invoke-static {v0}, Lcom/facebook/ads/AdSettings;->setUrlPrefix(Ljava/lang/String;)V

    return-void
.end method

.method private restoreSettings()V
    .locals 2

    .line 100
    iget-object v0, p0, Lexpo/modules/ads/facebook/AdSettingsManager;->mTestDeviceHashes:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 101
    invoke-static {v1}, Lcom/facebook/ads/AdSettings;->addTestDevice(Ljava/lang/String;)V

    goto :goto_0

    .line 104
    :cond_0
    iget-boolean v0, p0, Lexpo/modules/ads/facebook/AdSettingsManager;->mIsChildDirected:Z

    invoke-static {v0}, Lcom/facebook/ads/AdSettings;->setIsChildDirected(Z)V

    .line 105
    iget-object v0, p0, Lexpo/modules/ads/facebook/AdSettingsManager;->mMediationService:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/ads/AdSettings;->setMediationService(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lexpo/modules/ads/facebook/AdSettingsManager;->mUrlPrefix:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/ads/AdSettings;->setUrlPrefix(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public addTestDevice(Ljava/lang/String;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 60
    invoke-static {p1}, Lcom/facebook/ads/AdSettings;->addTestDevice(Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lexpo/modules/ads/facebook/AdSettingsManager;->mTestDeviceHashes:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x0

    .line 62
    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public clearTestDevices(Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 67
    invoke-static {}, Lcom/facebook/ads/AdSettings;->clearTestDevices()V

    .line 68
    iget-object v0, p0, Lexpo/modules/ads/facebook/AdSettingsManager;->mTestDeviceHashes:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    const/4 v0, 0x0

    .line 69
    invoke-virtual {p1, v0}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public getConstants()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 133
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 134
    invoke-virtual {p0}, Lexpo/modules/ads/facebook/AdSettingsManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "FBAdPrefs"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "deviceIdHash"

    const/4 v3, 0x0

    .line 135
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "currentDeviceHash"

    .line 137
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "CTKAdSettingsManager"

    return-object v0
.end method

.method public onCreate(Lorg/unimodules/core/ModuleRegistry;)V
    .locals 1

    .line 37
    iput-object p1, p0, Lexpo/modules/ads/facebook/AdSettingsManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    .line 38
    iget-object p1, p0, Lexpo/modules/ads/facebook/AdSettingsManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    const-class v0, Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-virtual {p1, v0}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/unimodules/core/interfaces/services/UIManager;

    if-eqz p1, :cond_0

    .line 40
    invoke-interface {p1, p0}, Lorg/unimodules/core/interfaces/services/UIManager;->registerLifecycleEventListener(Lorg/unimodules/core/interfaces/LifecycleEventListener;)V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .line 46
    iget-object v0, p0, Lexpo/modules/ads/facebook/AdSettingsManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    const-class v1, Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-virtual {v0, v1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/services/UIManager;

    if-eqz v0, :cond_0

    .line 48
    invoke-interface {v0, p0}, Lorg/unimodules/core/interfaces/services/UIManager;->unregisterLifecycleEventListener(Lorg/unimodules/core/interfaces/LifecycleEventListener;)V

    :cond_0
    const/4 v0, 0x0

    .line 50
    iput-object v0, p0, Lexpo/modules/ads/facebook/AdSettingsManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    return-void
.end method

.method public onHostDestroy()V
    .locals 0

    .line 128
    invoke-direct {p0}, Lexpo/modules/ads/facebook/AdSettingsManager;->clearSettings()V

    return-void
.end method

.method public onHostPause()V
    .locals 0

    .line 123
    invoke-direct {p0}, Lexpo/modules/ads/facebook/AdSettingsManager;->clearSettings()V

    return-void
.end method

.method public onHostResume()V
    .locals 0

    .line 118
    invoke-direct {p0}, Lexpo/modules/ads/facebook/AdSettingsManager;->restoreSettings()V

    return-void
.end method

.method public setIsChildDirected(ZLorg/unimodules/core/Promise;)V
    .locals 0
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 80
    invoke-static {p1}, Lcom/facebook/ads/AdSettings;->setIsChildDirected(Z)V

    .line 81
    iput-boolean p1, p0, Lexpo/modules/ads/facebook/AdSettingsManager;->mIsChildDirected:Z

    const/4 p1, 0x0

    .line 82
    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public setLogLevel(Ljava/lang/String;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 74
    sget-object p1, Lexpo/modules/ads/facebook/AdSettingsManager;->TAG:Ljava/lang/String;

    const-string v0, "This method is not supported on Android"

    invoke-static {p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    .line 75
    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public setMediationService(Ljava/lang/String;Lorg/unimodules/core/Promise;)V
    .locals 0
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 87
    invoke-static {p1}, Lcom/facebook/ads/AdSettings;->setMediationService(Ljava/lang/String;)V

    .line 88
    iput-object p1, p0, Lexpo/modules/ads/facebook/AdSettingsManager;->mMediationService:Ljava/lang/String;

    const/4 p1, 0x0

    .line 89
    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public setUrlPrefix(Ljava/lang/String;Lorg/unimodules/core/Promise;)V
    .locals 0
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 94
    invoke-static {p1}, Lcom/facebook/ads/AdSettings;->setUrlPrefix(Ljava/lang/String;)V

    .line 95
    iput-object p1, p0, Lexpo/modules/ads/facebook/AdSettingsManager;->mUrlPrefix:Ljava/lang/String;

    const/4 p1, 0x0

    .line 96
    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method
