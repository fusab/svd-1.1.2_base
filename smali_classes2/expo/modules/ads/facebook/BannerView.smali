.class public Lexpo/modules/ads/facebook/BannerView;
.super Landroid/widget/LinearLayout;
.source "BannerView.java"

# interfaces
.implements Lcom/facebook/ads/AdListener;
.implements Lorg/unimodules/core/interfaces/LifecycleEventListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# instance fields
.field private mEventEmitter:Lorg/unimodules/core/interfaces/services/EventEmitter;

.field private final mLayoutRunnable:Ljava/lang/Runnable;

.field private mPlacementId:Ljava/lang/String;

.field private mSize:Lcom/facebook/ads/AdSize;

.field private mUIManager:Lorg/unimodules/core/interfaces/services/UIManager;

.field private myAdView:Lcom/facebook/ads/AdView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lorg/unimodules/core/ModuleRegistry;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 27
    new-instance p1, Lexpo/modules/ads/facebook/BannerView$1;

    invoke-direct {p1, p0}, Lexpo/modules/ads/facebook/BannerView$1;-><init>(Lexpo/modules/ads/facebook/BannerView;)V

    iput-object p1, p0, Lexpo/modules/ads/facebook/BannerView;->mLayoutRunnable:Ljava/lang/Runnable;

    .line 39
    const-class p1, Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-virtual {p2, p1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/unimodules/core/interfaces/services/UIManager;

    iput-object p1, p0, Lexpo/modules/ads/facebook/BannerView;->mUIManager:Lorg/unimodules/core/interfaces/services/UIManager;

    .line 40
    iget-object p1, p0, Lexpo/modules/ads/facebook/BannerView;->mUIManager:Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-interface {p1, p0}, Lorg/unimodules/core/interfaces/services/UIManager;->registerLifecycleEventListener(Lorg/unimodules/core/interfaces/LifecycleEventListener;)V

    .line 41
    const-class p1, Lorg/unimodules/core/interfaces/services/EventEmitter;

    invoke-virtual {p2, p1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/unimodules/core/interfaces/services/EventEmitter;

    iput-object p1, p0, Lexpo/modules/ads/facebook/BannerView;->mEventEmitter:Lorg/unimodules/core/interfaces/services/EventEmitter;

    return-void
.end method

.method private createAdViewIfCan()V
    .locals 4

    .line 81
    iget-object v0, p0, Lexpo/modules/ads/facebook/BannerView;->myAdView:Lcom/facebook/ads/AdView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lexpo/modules/ads/facebook/BannerView;->mPlacementId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lexpo/modules/ads/facebook/BannerView;->mSize:Lcom/facebook/ads/AdSize;

    if-eqz v0, :cond_0

    .line 82
    new-instance v0, Lcom/facebook/ads/AdView;

    invoke-virtual {p0}, Lexpo/modules/ads/facebook/BannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lexpo/modules/ads/facebook/BannerView;->mPlacementId:Ljava/lang/String;

    iget-object v3, p0, Lexpo/modules/ads/facebook/BannerView;->mSize:Lcom/facebook/ads/AdSize;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/ads/AdView;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/ads/AdSize;)V

    iput-object v0, p0, Lexpo/modules/ads/facebook/BannerView;->myAdView:Lcom/facebook/ads/AdView;

    .line 83
    iget-object v0, p0, Lexpo/modules/ads/facebook/BannerView;->myAdView:Lcom/facebook/ads/AdView;

    invoke-virtual {v0, p0}, Lcom/facebook/ads/AdView;->setAdListener(Lcom/facebook/ads/AdListener;)V

    .line 84
    invoke-virtual {p0}, Lexpo/modules/ads/facebook/BannerView;->removeAllViews()V

    .line 85
    iget-object v0, p0, Lexpo/modules/ads/facebook/BannerView;->myAdView:Lcom/facebook/ads/AdView;

    invoke-virtual {p0, v0}, Lexpo/modules/ads/facebook/BannerView;->addView(Landroid/view/View;)V

    .line 86
    iget-object v0, p0, Lexpo/modules/ads/facebook/BannerView;->myAdView:Lcom/facebook/ads/AdView;

    invoke-virtual {v0}, Lcom/facebook/ads/AdView;->loadAd()V

    :cond_0
    return-void
.end method


# virtual methods
.method public onAdClicked(Lcom/facebook/ads/Ad;)V
    .locals 3

    .line 72
    iget-object p1, p0, Lexpo/modules/ads/facebook/BannerView;->mEventEmitter:Lorg/unimodules/core/interfaces/services/EventEmitter;

    invoke-virtual {p0}, Lexpo/modules/ads/facebook/BannerView;->getId()I

    move-result v0

    const-string v1, "onAdPress"

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Lorg/unimodules/core/interfaces/services/EventEmitter;->emit(ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public onAdLoaded(Lcom/facebook/ads/Ad;)V
    .locals 0

    return-void
.end method

.method public onError(Lcom/facebook/ads/Ad;Lcom/facebook/ads/AdError;)V
    .locals 2

    .line 56
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 58
    invoke-virtual {p2}, Lcom/facebook/ads/AdError;->getErrorCode()I

    move-result v0

    const-string v1, "errorCode"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 59
    invoke-virtual {p2}, Lcom/facebook/ads/AdError;->getErrorMessage()Ljava/lang/String;

    move-result-object p2

    const-string v0, "errorMessage"

    invoke-virtual {p1, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    iget-object p2, p0, Lexpo/modules/ads/facebook/BannerView;->mEventEmitter:Lorg/unimodules/core/interfaces/services/EventEmitter;

    invoke-virtual {p0}, Lexpo/modules/ads/facebook/BannerView;->getId()I

    move-result v0

    const-string v1, "onAdError"

    invoke-interface {p2, v0, v1, p1}, Lorg/unimodules/core/interfaces/services/EventEmitter;->emit(ILjava/lang/String;Landroid/os/Bundle;)V

    const/4 p1, 0x0

    .line 62
    iput-object p1, p0, Lexpo/modules/ads/facebook/BannerView;->myAdView:Lcom/facebook/ads/AdView;

    return-void
.end method

.method public onHostDestroy()V
    .locals 1

    .line 102
    iget-object v0, p0, Lexpo/modules/ads/facebook/BannerView;->myAdView:Lcom/facebook/ads/AdView;

    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {v0}, Lcom/facebook/ads/AdView;->destroy()V

    .line 105
    :cond_0
    iget-object v0, p0, Lexpo/modules/ads/facebook/BannerView;->mUIManager:Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-interface {v0, p0}, Lorg/unimodules/core/interfaces/services/UIManager;->unregisterLifecycleEventListener(Lorg/unimodules/core/interfaces/LifecycleEventListener;)V

    const/4 v0, 0x0

    .line 106
    iput-object v0, p0, Lexpo/modules/ads/facebook/BannerView;->mUIManager:Lorg/unimodules/core/interfaces/services/UIManager;

    return-void
.end method

.method public onHostPause()V
    .locals 0

    return-void
.end method

.method public onHostResume()V
    .locals 0

    return-void
.end method

.method public onLoggingImpression(Lcom/facebook/ads/Ad;)V
    .locals 3

    .line 77
    iget-object p1, p0, Lexpo/modules/ads/facebook/BannerView;->mEventEmitter:Lorg/unimodules/core/interfaces/services/EventEmitter;

    invoke-virtual {p0}, Lexpo/modules/ads/facebook/BannerView;->getId()I

    move-result v0

    const-string v1, "onLoggingImpression"

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Lorg/unimodules/core/interfaces/services/EventEmitter;->emit(ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public requestLayout()V
    .locals 1

    .line 111
    invoke-super {p0}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 116
    iget-object v0, p0, Lexpo/modules/ads/facebook/BannerView;->mLayoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lexpo/modules/ads/facebook/BannerView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public setPlacementId(Ljava/lang/String;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lexpo/modules/ads/facebook/BannerView;->mPlacementId:Ljava/lang/String;

    .line 46
    invoke-direct {p0}, Lexpo/modules/ads/facebook/BannerView;->createAdViewIfCan()V

    return-void
.end method

.method public setSize(Lcom/facebook/ads/AdSize;)V
    .locals 0

    .line 50
    iput-object p1, p0, Lexpo/modules/ads/facebook/BannerView;->mSize:Lcom/facebook/ads/AdSize;

    .line 51
    invoke-direct {p0}, Lexpo/modules/ads/facebook/BannerView;->createAdViewIfCan()V

    return-void
.end method
