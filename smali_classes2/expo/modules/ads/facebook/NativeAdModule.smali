.class public Lexpo/modules/ads/facebook/NativeAdModule;
.super Lorg/unimodules/core/ExportedModule;
.source "NativeAdModule.java"


# instance fields
.field private mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lorg/unimodules/core/ExportedModule;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public disableAutoRefresh(Ljava/lang/String;Lorg/unimodules/core/Promise;)V
    .locals 2
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 38
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdModule;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    const-class v1, Lexpo/modules/ads/facebook/NativeAdManager;

    invoke-virtual {v0, v1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexpo/modules/ads/facebook/NativeAdManager;

    invoke-virtual {v0, p1, p2}, Lexpo/modules/ads/facebook/NativeAdManager;->disableAutoRefresh(Ljava/lang/String;Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "CTKNativeAdManager"

    return-object v0
.end method

.method public init(Ljava/lang/String;ILorg/unimodules/core/Promise;)V
    .locals 2
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 33
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdModule;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    const-class v1, Lexpo/modules/ads/facebook/NativeAdManager;

    invoke-virtual {v0, v1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexpo/modules/ads/facebook/NativeAdManager;

    invoke-virtual {v0, p1, p2, p3}, Lexpo/modules/ads/facebook/NativeAdManager;->init(Ljava/lang/String;ILorg/unimodules/core/Promise;)V

    return-void
.end method

.method public onCreate(Lorg/unimodules/core/ModuleRegistry;)V
    .locals 0

    .line 22
    iput-object p1, p0, Lexpo/modules/ads/facebook/NativeAdModule;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    return-void
.end method

.method public registerViewsForInteraction(IIILjava/util/List;Lorg/unimodules/core/Promise;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/unimodules/core/Promise;",
            ")V"
        }
    .end annotation

    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 58
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdModule;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    const-class v1, Lexpo/modules/ads/facebook/NativeAdManager;

    invoke-virtual {v0, v1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lexpo/modules/ads/facebook/NativeAdManager;

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lexpo/modules/ads/facebook/NativeAdManager;->registerViewsForInteraction(IIILjava/util/List;Lorg/unimodules/core/Promise;)V

    return-void
.end method

.method public setMediaCachePolicy(Ljava/lang/String;Ljava/lang/String;Lorg/unimodules/core/Promise;)V
    .locals 0
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    const-string p1, "NativeAdManager"

    const-string p2, "This method is not supported on Android"

    .line 43
    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    .line 44
    invoke-virtual {p3, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public triggerEvent(ILorg/unimodules/core/Promise;)V
    .locals 2
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 49
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdModule;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    const-class v1, Lexpo/modules/ads/facebook/NativeAdManager;

    invoke-virtual {v0, v1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexpo/modules/ads/facebook/NativeAdManager;

    invoke-virtual {v0, p1, p2}, Lexpo/modules/ads/facebook/NativeAdManager;->triggerEvent(ILorg/unimodules/core/Promise;)V

    return-void
.end method
