.class public final Lexpo/modules/ads/facebook/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lexpo/modules/ads/facebook/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action0:I = 0x7f09000b

.field public static final action_container:I = 0x7f090014

.field public static final action_divider:I = 0x7f090016

.field public static final action_image:I = 0x7f090017

.field public static final action_text:I = 0x7f09001d

.field public static final actions:I = 0x7f09001e

.field public static final adjust_height:I = 0x7f090023

.field public static final adjust_width:I = 0x7f090024

.field public static final android_pay:I = 0x7f090032

.field public static final android_pay_dark:I = 0x7f090033

.field public static final android_pay_light:I = 0x7f090034

.field public static final android_pay_light_with_border:I = 0x7f090035

.field public static final async:I = 0x7f090038

.field public static final auto:I = 0x7f090039

.field public static final background_image_view:I = 0x7f09003b

.field public static final blocking:I = 0x7f09003d

.field public static final book_now:I = 0x7f09003e

.field public static final bottom:I = 0x7f09003f

.field public static final button:I = 0x7f090046

.field public static final buyButton:I = 0x7f09004a

.field public static final buy_now:I = 0x7f09004b

.field public static final buy_with:I = 0x7f09004c

.field public static final buy_with_google:I = 0x7f09004d

.field public static final cancel_action:I = 0x7f09004e

.field public static final center:I = 0x7f090055

.field public static final chronometer:I = 0x7f09005f

.field public static final classic:I = 0x7f090060

.field public static final dark:I = 0x7f09008b

.field public static final donate_with:I = 0x7f090096

.field public static final donate_with_google:I = 0x7f090097

.field public static final end:I = 0x7f09009a

.field public static final end_padder:I = 0x7f09009c

.field public static final forever:I = 0x7f0900d0

.field public static final google_wallet_classic:I = 0x7f0900d8

.field public static final google_wallet_grayscale:I = 0x7f0900d9

.field public static final google_wallet_monochrome:I = 0x7f0900da

.field public static final grayscale:I = 0x7f0900db

.field public static final holo_dark:I = 0x7f0900dd

.field public static final holo_light:I = 0x7f0900de

.field public static final hybrid:I = 0x7f0900e4

.field public static final icon:I = 0x7f0900e5

.field public static final icon_group:I = 0x7f0900e6

.field public static final icon_only:I = 0x7f0900e7

.field public static final info:I = 0x7f0900eb

.field public static final italic:I = 0x7f0900f1

.field public static final item_touch_helper_previous_elevation:I = 0x7f0900f2

.field public static final left:I = 0x7f0900fb

.field public static final light:I = 0x7f090102

.field public static final line1:I = 0x7f090103

.field public static final line3:I = 0x7f090104

.field public static final logo_only:I = 0x7f09010a

.field public static final match_parent:I = 0x7f09010e

.field public static final media_actions:I = 0x7f090110

.field public static final monochrome:I = 0x7f090115

.field public static final none:I = 0x7f09011d

.field public static final normal:I = 0x7f09011e

.field public static final notification_background:I = 0x7f09011f

.field public static final notification_main_column:I = 0x7f090120

.field public static final notification_main_column_container:I = 0x7f090121

.field public static final production:I = 0x7f090136

.field public static final progressBar:I = 0x7f090137

.field public static final radio:I = 0x7f09013c

.field public static final right:I = 0x7f090144

.field public static final right_icon:I = 0x7f09014b

.field public static final right_side:I = 0x7f09014c

.field public static final sandbox:I = 0x7f090157

.field public static final satellite:I = 0x7f090158

.field public static final seek_bar:I = 0x7f090170

.field public static final selectionDetails:I = 0x7f090173

.field public static final slide:I = 0x7f09017b

.field public static final standard:I = 0x7f090188

.field public static final start:I = 0x7f090189

.field public static final status_bar_latest_event_content:I = 0x7f09018c

.field public static final strict_sandbox:I = 0x7f09018f

.field public static final tag_transition_group:I = 0x7f090195

.field public static final tag_unhandled_key_event_manager:I = 0x7f090196

.field public static final tag_unhandled_key_listeners:I = 0x7f090197

.field public static final terrain:I = 0x7f09019a

.field public static final test:I = 0x7f09019b

.field public static final text:I = 0x7f09019c

.field public static final text2:I = 0x7f09019d

.field public static final time:I = 0x7f0901a7

.field public static final title:I = 0x7f0901a8

.field public static final toolbar:I = 0x7f0901ac

.field public static final top:I = 0x7f0901ad

.field public static final wide:I = 0x7f0901c2

.field public static final wrap_content:I = 0x7f0901c5


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
