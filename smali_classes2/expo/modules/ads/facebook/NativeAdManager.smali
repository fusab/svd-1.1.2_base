.class public Lexpo/modules/ads/facebook/NativeAdManager;
.super Ljava/lang/Object;
.source "NativeAdManager.java"

# interfaces
.implements Lorg/unimodules/core/interfaces/InternalModule;
.implements Lcom/facebook/ads/NativeAdsManager$Listener;


# instance fields
.field private mAdsManagers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/facebook/ads/NativeAdsManager;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lexpo/modules/ads/facebook/NativeAdManager;->mAdsManagers:Ljava/util/Map;

    .line 33
    iput-object p1, p0, Lexpo/modules/ads/facebook/NativeAdManager;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lexpo/modules/ads/facebook/NativeAdManager;)Landroid/content/Context;
    .locals 0

    .line 24
    iget-object p0, p0, Lexpo/modules/ads/facebook/NativeAdManager;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$100(Lexpo/modules/ads/facebook/NativeAdManager;)Ljava/util/Map;
    .locals 0

    .line 24
    iget-object p0, p0, Lexpo/modules/ads/facebook/NativeAdManager;->mAdsManagers:Ljava/util/Map;

    return-object p0
.end method

.method private sendAppEvent(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    .line 117
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    const-class v1, Lorg/unimodules/core/interfaces/services/EventEmitter;

    invoke-virtual {v0, v1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/services/EventEmitter;

    invoke-interface {v0, p1, p2}, Lorg/unimodules/core/interfaces/services/EventEmitter;->emit(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public disableAutoRefresh(Ljava/lang/String;Lorg/unimodules/core/Promise;)V
    .locals 1

    .line 75
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdManager;->mAdsManagers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/ads/NativeAdsManager;

    invoke-virtual {p1}, Lcom/facebook/ads/NativeAdsManager;->disableAutoRefresh()V

    const/4 p1, 0x0

    .line 76
    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public getExportedInterfaces()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation

    .line 38
    const-class v0, Lexpo/modules/ads/facebook/NativeAdManager;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFBAdsManager(Ljava/lang/String;)Lcom/facebook/ads/NativeAdsManager;
    .locals 1

    .line 107
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdManager;->mAdsManagers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/ads/NativeAdsManager;

    return-object p1
.end method

.method public init(Ljava/lang/String;ILorg/unimodules/core/Promise;)V
    .locals 2

    .line 54
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    const-class v1, Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-virtual {v0, v1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/services/UIManager;

    new-instance v1, Lexpo/modules/ads/facebook/NativeAdManager$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lexpo/modules/ads/facebook/NativeAdManager$1;-><init>(Lexpo/modules/ads/facebook/NativeAdManager;Ljava/lang/String;ILorg/unimodules/core/Promise;)V

    invoke-interface {v0, v1}, Lorg/unimodules/core/interfaces/services/UIManager;->runOnUiQueueThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onAdError(Lcom/facebook/ads/AdError;)V
    .locals 0

    return-void
.end method

.method public onAdsLoaded()V
    .locals 4

    .line 85
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 87
    iget-object v1, p0, Lexpo/modules/ads/facebook/NativeAdManager;->mAdsManagers:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 88
    iget-object v3, p0, Lexpo/modules/ads/facebook/NativeAdManager;->mAdsManagers:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/ads/NativeAdsManager;

    .line 89
    invoke-virtual {v3}, Lcom/facebook/ads/NativeAdsManager;->isLoaded()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_0
    const-string v1, "CTKNativeAdsManagersChanged"

    .line 92
    invoke-direct {p0, v1, v0}, Lexpo/modules/ads/facebook/NativeAdManager;->sendAppEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreate(Lorg/unimodules/core/ModuleRegistry;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lexpo/modules/ads/facebook/NativeAdManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    return-void
.end method

.method public synthetic onDestroy()V
    .locals 0

    invoke-static {p0}, Lorg/unimodules/core/interfaces/RegistryLifecycleListener$-CC;->$default$onDestroy(Lorg/unimodules/core/interfaces/RegistryLifecycleListener;)V

    return-void
.end method

.method public registerViewsForInteraction(IIILjava/util/List;Lorg/unimodules/core/Promise;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/unimodules/core/Promise;",
            ")V"
        }
    .end annotation

    .line 140
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    const-class v1, Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-virtual {v0, v1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/services/UIManager;

    new-instance v8, Lexpo/modules/ads/facebook/NativeAdManager$3;

    move-object v1, v8

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lexpo/modules/ads/facebook/NativeAdManager$3;-><init>(Lexpo/modules/ads/facebook/NativeAdManager;IIILjava/util/List;Lorg/unimodules/core/Promise;)V

    invoke-interface {v0, v8}, Lorg/unimodules/core/interfaces/services/UIManager;->addUIBlock(Lorg/unimodules/core/interfaces/services/UIManager$GroupUIBlock;)V

    return-void
.end method

.method public triggerEvent(ILorg/unimodules/core/Promise;)V
    .locals 2

    .line 121
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdManager;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    const-class v1, Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-virtual {v0, v1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/services/UIManager;

    new-instance v1, Lexpo/modules/ads/facebook/NativeAdManager$2;

    invoke-direct {v1, p0, p2}, Lexpo/modules/ads/facebook/NativeAdManager$2;-><init>(Lexpo/modules/ads/facebook/NativeAdManager;Lorg/unimodules/core/Promise;)V

    const-class p2, Lexpo/modules/ads/facebook/NativeAdView;

    invoke-interface {v0, p1, v1, p2}, Lorg/unimodules/core/interfaces/services/UIManager;->addUIBlock(ILorg/unimodules/core/interfaces/services/UIManager$UIBlock;Ljava/lang/Class;)V

    return-void
.end method
