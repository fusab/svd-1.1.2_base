.class public Lexpo/modules/ads/facebook/AdIconViewManager;
.super Lorg/unimodules/core/ViewManager;
.source "AdIconViewManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Lorg/unimodules/core/ViewManager;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic createViewInstance(Landroid/content/Context;)Landroid/view/View;
    .locals 0

    .line 9
    invoke-virtual {p0, p1}, Lexpo/modules/ads/facebook/AdIconViewManager;->createViewInstance(Landroid/content/Context;)Lcom/facebook/ads/AdIconView;

    move-result-object p1

    return-object p1
.end method

.method public createViewInstance(Landroid/content/Context;)Lcom/facebook/ads/AdIconView;
    .locals 1

    .line 17
    new-instance v0, Lcom/facebook/ads/AdIconView;

    invoke-direct {v0, p1}, Lcom/facebook/ads/AdIconView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "AdIconView"

    return-object v0
.end method

.method public getViewManagerType()Lorg/unimodules/core/ViewManager$ViewManagerType;
    .locals 1

    .line 22
    sget-object v0, Lorg/unimodules/core/ViewManager$ViewManagerType;->SIMPLE:Lorg/unimodules/core/ViewManager$ViewManagerType;

    return-object v0
.end method
