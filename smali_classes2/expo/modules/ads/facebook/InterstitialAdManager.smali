.class public Lexpo/modules/ads/facebook/InterstitialAdManager;
.super Lorg/unimodules/core/ExportedModule;
.source "InterstitialAdManager.java"

# interfaces
.implements Lcom/facebook/ads/InterstitialAdListener;
.implements Lorg/unimodules/core/interfaces/LifecycleEventListener;


# instance fields
.field private mActivityProvider:Lorg/unimodules/core/interfaces/ActivityProvider;

.field private mDidClick:Z

.field private mInterstitial:Lcom/facebook/ads/InterstitialAd;

.field private mPromise:Lorg/unimodules/core/Promise;

.field private mUIManager:Lorg/unimodules/core/interfaces/services/UIManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lorg/unimodules/core/ExportedModule;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 20
    iput-boolean p1, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mDidClick:Z

    return-void
.end method

.method private cleanUp()V
    .locals 2

    const/4 v0, 0x0

    .line 91
    iput-object v0, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mPromise:Lorg/unimodules/core/Promise;

    const/4 v1, 0x0

    .line 92
    iput-boolean v1, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mDidClick:Z

    .line 94
    iget-object v1, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mInterstitial:Lcom/facebook/ads/InterstitialAd;

    if-eqz v1, :cond_0

    .line 95
    invoke-virtual {v1}, Lcom/facebook/ads/InterstitialAd;->destroy()V

    .line 96
    iput-object v0, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mInterstitial:Lcom/facebook/ads/InterstitialAd;

    :cond_0
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "CTKInterstitialAdManager"

    return-object v0
.end method

.method public onAdClicked(Lcom/facebook/ads/Ad;)V
    .locals 0

    const/4 p1, 0x1

    .line 72
    iput-boolean p1, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mDidClick:Z

    return-void
.end method

.method public onAdLoaded(Lcom/facebook/ads/Ad;)V
    .locals 1

    .line 65
    iget-object v0, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mInterstitial:Lcom/facebook/ads/InterstitialAd;

    if-ne p1, v0, :cond_0

    .line 66
    invoke-virtual {v0}, Lcom/facebook/ads/InterstitialAd;->show()Z

    :cond_0
    return-void
.end method

.method public onCreate(Lorg/unimodules/core/ModuleRegistry;)V
    .locals 1

    .line 31
    iget-object v0, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mUIManager:Lorg/unimodules/core/interfaces/services/UIManager;

    if-eqz v0, :cond_0

    .line 32
    invoke-interface {v0, p0}, Lorg/unimodules/core/interfaces/services/UIManager;->unregisterLifecycleEventListener(Lorg/unimodules/core/interfaces/LifecycleEventListener;)V

    .line 34
    :cond_0
    const-class v0, Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-virtual {p1, v0}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/services/UIManager;

    iput-object v0, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mUIManager:Lorg/unimodules/core/interfaces/services/UIManager;

    .line 35
    const-class v0, Lorg/unimodules/core/interfaces/ActivityProvider;

    invoke-virtual {p1, v0}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/unimodules/core/interfaces/ActivityProvider;

    iput-object p1, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mActivityProvider:Lorg/unimodules/core/interfaces/ActivityProvider;

    .line 36
    iget-object p1, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mUIManager:Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-interface {p1, p0}, Lorg/unimodules/core/interfaces/services/UIManager;->registerLifecycleEventListener(Lorg/unimodules/core/interfaces/LifecycleEventListener;)V

    return-void
.end method

.method public onError(Lcom/facebook/ads/Ad;Lcom/facebook/ads/AdError;)V
    .locals 1

    .line 59
    iget-object p1, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mPromise:Lorg/unimodules/core/Promise;

    invoke-virtual {p2}, Lcom/facebook/ads/AdError;->getErrorMessage()Ljava/lang/String;

    move-result-object p2

    const-string v0, "E_FAILED_TO_LOAD"

    invoke-virtual {p1, v0, p2}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0}, Lexpo/modules/ads/facebook/InterstitialAdManager;->cleanUp()V

    return-void
.end method

.method public onHostDestroy()V
    .locals 1

    .line 112
    invoke-direct {p0}, Lexpo/modules/ads/facebook/InterstitialAdManager;->cleanUp()V

    .line 113
    iget-object v0, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mUIManager:Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-interface {v0, p0}, Lorg/unimodules/core/interfaces/services/UIManager;->unregisterLifecycleEventListener(Lorg/unimodules/core/interfaces/LifecycleEventListener;)V

    const/4 v0, 0x0

    .line 114
    iput-object v0, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mUIManager:Lorg/unimodules/core/interfaces/services/UIManager;

    return-void
.end method

.method public onHostPause()V
    .locals 0

    return-void
.end method

.method public onHostResume()V
    .locals 0

    return-void
.end method

.method public onInterstitialDismissed(Lcom/facebook/ads/Ad;)V
    .locals 1

    .line 77
    iget-object p1, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mPromise:Lorg/unimodules/core/Promise;

    iget-boolean v0, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mDidClick:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    .line 78
    invoke-direct {p0}, Lexpo/modules/ads/facebook/InterstitialAdManager;->cleanUp()V

    return-void
.end method

.method public onInterstitialDisplayed(Lcom/facebook/ads/Ad;)V
    .locals 0

    return-void
.end method

.method public onLoggingImpression(Lcom/facebook/ads/Ad;)V
    .locals 0

    return-void
.end method

.method public showAd(Ljava/lang/String;Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 41
    iget-object v0, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mPromise:Lorg/unimodules/core/Promise;

    if-eqz v0, :cond_0

    const-string p1, "E_FAILED_TO_SHOW"

    const-string v0, "Only one `showAd` can be called at once"

    .line 42
    invoke-virtual {p2, p1, v0}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 46
    :cond_0
    iput-object p2, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mPromise:Lorg/unimodules/core/Promise;

    .line 47
    new-instance p2, Lcom/facebook/ads/InterstitialAd;

    iget-object v0, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mActivityProvider:Lorg/unimodules/core/interfaces/ActivityProvider;

    invoke-interface {v0}, Lorg/unimodules/core/interfaces/ActivityProvider;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Lcom/facebook/ads/InterstitialAd;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object p2, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mInterstitial:Lcom/facebook/ads/InterstitialAd;

    .line 48
    iget-object p1, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mInterstitial:Lcom/facebook/ads/InterstitialAd;

    invoke-virtual {p1, p0}, Lcom/facebook/ads/InterstitialAd;->setAdListener(Lcom/facebook/ads/InterstitialAdListener;)V

    .line 49
    iget-object p1, p0, Lexpo/modules/ads/facebook/InterstitialAdManager;->mInterstitial:Lcom/facebook/ads/InterstitialAd;

    invoke-virtual {p1}, Lcom/facebook/ads/InterstitialAd;->loadAd()V

    return-void
.end method
