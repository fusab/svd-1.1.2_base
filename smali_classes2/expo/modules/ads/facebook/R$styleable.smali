.class public final Lexpo/modules/ads/facebook/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lexpo/modules/ads/facebook/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final ColorStateListItem:[I

.field public static final ColorStateListItem_alpha:I = 0x2

.field public static final ColorStateListItem_android_alpha:I = 0x1

.field public static final ColorStateListItem_android_color:I = 0x0

.field public static final CoordinatorLayout:[I

.field public static final CoordinatorLayout_Layout:[I

.field public static final CoordinatorLayout_Layout_android_layout_gravity:I = 0x0

.field public static final CoordinatorLayout_Layout_layout_anchor:I = 0x1

.field public static final CoordinatorLayout_Layout_layout_anchorGravity:I = 0x2

.field public static final CoordinatorLayout_Layout_layout_behavior:I = 0x3

.field public static final CoordinatorLayout_Layout_layout_dodgeInsetEdges:I = 0x4

.field public static final CoordinatorLayout_Layout_layout_insetEdge:I = 0x5

.field public static final CoordinatorLayout_Layout_layout_keyline:I = 0x6

.field public static final CoordinatorLayout_keylines:I = 0x0

.field public static final CoordinatorLayout_statusBarBackground:I = 0x1

.field public static final CustomWalletTheme:[I

.field public static final CustomWalletTheme_customThemeStyle:I = 0x0

.field public static final CustomWalletTheme_toolbarTextColorStyle:I = 0x1

.field public static final CustomWalletTheme_windowTransitionStyle:I = 0x2

.field public static final FontFamily:[I

.field public static final FontFamilyFont:[I

.field public static final FontFamilyFont_android_font:I = 0x0

.field public static final FontFamilyFont_android_fontStyle:I = 0x2

.field public static final FontFamilyFont_android_fontVariationSettings:I = 0x4

.field public static final FontFamilyFont_android_fontWeight:I = 0x1

.field public static final FontFamilyFont_android_ttcIndex:I = 0x3

.field public static final FontFamilyFont_font:I = 0x5

.field public static final FontFamilyFont_fontStyle:I = 0x6

.field public static final FontFamilyFont_fontVariationSettings:I = 0x7

.field public static final FontFamilyFont_fontWeight:I = 0x8

.field public static final FontFamilyFont_ttcIndex:I = 0x9

.field public static final FontFamily_fontProviderAuthority:I = 0x0

.field public static final FontFamily_fontProviderCerts:I = 0x1

.field public static final FontFamily_fontProviderFetchStrategy:I = 0x2

.field public static final FontFamily_fontProviderFetchTimeout:I = 0x3

.field public static final FontFamily_fontProviderPackage:I = 0x4

.field public static final FontFamily_fontProviderQuery:I = 0x5

.field public static final GradientColor:[I

.field public static final GradientColorItem:[I

.field public static final GradientColorItem_android_color:I = 0x0

.field public static final GradientColorItem_android_offset:I = 0x1

.field public static final GradientColor_android_centerColor:I = 0x7

.field public static final GradientColor_android_centerX:I = 0x3

.field public static final GradientColor_android_centerY:I = 0x4

.field public static final GradientColor_android_endColor:I = 0x1

.field public static final GradientColor_android_endX:I = 0xa

.field public static final GradientColor_android_endY:I = 0xb

.field public static final GradientColor_android_gradientRadius:I = 0x5

.field public static final GradientColor_android_startColor:I = 0x0

.field public static final GradientColor_android_startX:I = 0x8

.field public static final GradientColor_android_startY:I = 0x9

.field public static final GradientColor_android_tileMode:I = 0x6

.field public static final GradientColor_android_type:I = 0x2

.field public static final LoadingImageView:[I

.field public static final LoadingImageView_circleCrop:I = 0x0

.field public static final LoadingImageView_imageAspectRatio:I = 0x1

.field public static final LoadingImageView_imageAspectRatioAdjust:I = 0x2

.field public static final MapAttrs:[I

.field public static final MapAttrs_ambientEnabled:I = 0x0

.field public static final MapAttrs_cameraBearing:I = 0x1

.field public static final MapAttrs_cameraMaxZoomPreference:I = 0x2

.field public static final MapAttrs_cameraMinZoomPreference:I = 0x3

.field public static final MapAttrs_cameraTargetLat:I = 0x4

.field public static final MapAttrs_cameraTargetLng:I = 0x5

.field public static final MapAttrs_cameraTilt:I = 0x6

.field public static final MapAttrs_cameraZoom:I = 0x7

.field public static final MapAttrs_latLngBoundsNorthEastLatitude:I = 0x8

.field public static final MapAttrs_latLngBoundsNorthEastLongitude:I = 0x9

.field public static final MapAttrs_latLngBoundsSouthWestLatitude:I = 0xa

.field public static final MapAttrs_latLngBoundsSouthWestLongitude:I = 0xb

.field public static final MapAttrs_liteMode:I = 0xc

.field public static final MapAttrs_mapType:I = 0xd

.field public static final MapAttrs_uiCompass:I = 0xe

.field public static final MapAttrs_uiMapToolbar:I = 0xf

.field public static final MapAttrs_uiRotateGestures:I = 0x10

.field public static final MapAttrs_uiScrollGestures:I = 0x11

.field public static final MapAttrs_uiScrollGesturesDuringRotateOrZoom:I = 0x12

.field public static final MapAttrs_uiTiltGestures:I = 0x13

.field public static final MapAttrs_uiZoomControls:I = 0x14

.field public static final MapAttrs_uiZoomGestures:I = 0x15

.field public static final MapAttrs_useViewLifecycle:I = 0x16

.field public static final MapAttrs_zOrderOnTop:I = 0x17

.field public static final RecyclerView:[I

.field public static final RecyclerView_android_descendantFocusability:I = 0x1

.field public static final RecyclerView_android_orientation:I = 0x0

.field public static final RecyclerView_fastScrollEnabled:I = 0x2

.field public static final RecyclerView_fastScrollHorizontalThumbDrawable:I = 0x3

.field public static final RecyclerView_fastScrollHorizontalTrackDrawable:I = 0x4

.field public static final RecyclerView_fastScrollVerticalThumbDrawable:I = 0x5

.field public static final RecyclerView_fastScrollVerticalTrackDrawable:I = 0x6

.field public static final RecyclerView_layoutManager:I = 0x7

.field public static final RecyclerView_reverseLayout:I = 0x8

.field public static final RecyclerView_spanCount:I = 0x9

.field public static final RecyclerView_stackFromEnd:I = 0xa

.field public static final SignInButton:[I

.field public static final SignInButton_buttonSize:I = 0x0

.field public static final SignInButton_colorScheme:I = 0x1

.field public static final SignInButton_scopeUris:I = 0x2

.field public static final WalletFragmentOptions:[I

.field public static final WalletFragmentOptions_appTheme:I = 0x0

.field public static final WalletFragmentOptions_environment:I = 0x1

.field public static final WalletFragmentOptions_fragmentMode:I = 0x2

.field public static final WalletFragmentOptions_fragmentStyle:I = 0x3

.field public static final WalletFragmentStyle:[I

.field public static final WalletFragmentStyle_buyButtonAppearance:I = 0x0

.field public static final WalletFragmentStyle_buyButtonHeight:I = 0x1

.field public static final WalletFragmentStyle_buyButtonText:I = 0x2

.field public static final WalletFragmentStyle_buyButtonWidth:I = 0x3

.field public static final WalletFragmentStyle_maskedWalletDetailsBackground:I = 0x4

.field public static final WalletFragmentStyle_maskedWalletDetailsButtonBackground:I = 0x5

.field public static final WalletFragmentStyle_maskedWalletDetailsButtonTextAppearance:I = 0x6

.field public static final WalletFragmentStyle_maskedWalletDetailsHeaderTextAppearance:I = 0x7

.field public static final WalletFragmentStyle_maskedWalletDetailsLogoImageType:I = 0x8

.field public static final WalletFragmentStyle_maskedWalletDetailsLogoTextColor:I = 0x9

.field public static final WalletFragmentStyle_maskedWalletDetailsTextAppearance:I = 0xa


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    .line 370
    new-array v1, v0, [I

    fill-array-data v1, :array_0

    sput-object v1, Lexpo/modules/ads/facebook/R$styleable;->ColorStateListItem:[I

    const/4 v1, 0x2

    .line 374
    new-array v2, v1, [I

    fill-array-data v2, :array_1

    sput-object v2, Lexpo/modules/ads/facebook/R$styleable;->CoordinatorLayout:[I

    const/4 v2, 0x7

    .line 377
    new-array v2, v2, [I

    fill-array-data v2, :array_2

    sput-object v2, Lexpo/modules/ads/facebook/R$styleable;->CoordinatorLayout_Layout:[I

    .line 385
    new-array v2, v0, [I

    fill-array-data v2, :array_3

    sput-object v2, Lexpo/modules/ads/facebook/R$styleable;->CustomWalletTheme:[I

    const/4 v2, 0x6

    .line 389
    new-array v2, v2, [I

    fill-array-data v2, :array_4

    sput-object v2, Lexpo/modules/ads/facebook/R$styleable;->FontFamily:[I

    const/16 v2, 0xa

    .line 396
    new-array v2, v2, [I

    fill-array-data v2, :array_5

    sput-object v2, Lexpo/modules/ads/facebook/R$styleable;->FontFamilyFont:[I

    const/16 v2, 0xc

    .line 407
    new-array v2, v2, [I

    fill-array-data v2, :array_6

    sput-object v2, Lexpo/modules/ads/facebook/R$styleable;->GradientColor:[I

    .line 420
    new-array v1, v1, [I

    fill-array-data v1, :array_7

    sput-object v1, Lexpo/modules/ads/facebook/R$styleable;->GradientColorItem:[I

    .line 423
    new-array v1, v0, [I

    fill-array-data v1, :array_8

    sput-object v1, Lexpo/modules/ads/facebook/R$styleable;->LoadingImageView:[I

    const/16 v1, 0x18

    .line 427
    new-array v1, v1, [I

    fill-array-data v1, :array_9

    sput-object v1, Lexpo/modules/ads/facebook/R$styleable;->MapAttrs:[I

    const/16 v1, 0xb

    .line 452
    new-array v2, v1, [I

    fill-array-data v2, :array_a

    sput-object v2, Lexpo/modules/ads/facebook/R$styleable;->RecyclerView:[I

    .line 464
    new-array v0, v0, [I

    fill-array-data v0, :array_b

    sput-object v0, Lexpo/modules/ads/facebook/R$styleable;->SignInButton:[I

    const/4 v0, 0x4

    .line 468
    new-array v0, v0, [I

    fill-array-data v0, :array_c

    sput-object v0, Lexpo/modules/ads/facebook/R$styleable;->WalletFragmentOptions:[I

    .line 473
    new-array v0, v1, [I

    fill-array-data v0, :array_d

    sput-object v0, Lexpo/modules/ads/facebook/R$styleable;->WalletFragmentStyle:[I

    return-void

    :array_0
    .array-data 4
        0x10101a5
        0x101031f
        0x7f04002c
    .end array-data

    :array_1
    .array-data 4
        0x7f04015b
        0x7f040207
    .end array-data

    :array_2
    .array-data 4
        0x10100b3
        0x7f040164
        0x7f040165
        0x7f040166
        0x7f040169
        0x7f04016a
        0x7f04016b
    .end array-data

    :array_3
    .array-data 4
        0x7f0400eb
        0x7f040264
        0x7f040287
    .end array-data

    :array_4
    .array-data 4
        0x7f04011f
        0x7f040120
        0x7f040121
        0x7f040122
        0x7f040123
        0x7f040124
    .end array-data

    :array_5
    .array-data 4
        0x1010532
        0x1010533
        0x101053f
        0x101056f
        0x1010570
        0x7f04011d
        0x7f040125
        0x7f040126
        0x7f040127
        0x7f04026c
    .end array-data

    :array_6
    .array-data 4
        0x101019d
        0x101019e
        0x10101a1
        0x10101a2
        0x10101a3
        0x10101a4
        0x1010201
        0x101020b
        0x1010510
        0x1010511
        0x1010512
        0x1010513
    .end array-data

    :array_7
    .array-data 4
        0x10101a5
        0x1010514
    .end array-data

    :array_8
    .array-data 4
        0x7f04008e
        0x7f040146
        0x7f040147
    .end array-data

    :array_9
    .array-data 4
        0x7f04002e
        0x7f040068
        0x7f040069
        0x7f04006a
        0x7f04006b
        0x7f04006c
        0x7f04006d
        0x7f04006e
        0x7f04015e
        0x7f04015f
        0x7f040160
        0x7f040161
        0x7f04017c
        0x7f04018b
        0x7f04026d
        0x7f04026e
        0x7f04026f
        0x7f040270
        0x7f040271
        0x7f040272
        0x7f040273
        0x7f040274
        0x7f040277
        0x7f040288
    .end array-data

    :array_a
    .array-data 4
        0x10100c4
        0x10100f1
        0x7f040115
        0x7f040116
        0x7f040117
        0x7f040118
        0x7f040119
        0x7f040163
        0x7f0401cc
        0x7f0401fb
        0x7f040201
    .end array-data

    :array_b
    .array-data 4
        0x7f04005f
        0x7f0400a5
        0x7f0401de
    .end array-data

    :array_c
    .array-data 4
        0x7f04002f
        0x7f040100
        0x7f040129
        0x7f04012a
    .end array-data

    :array_d
    .array-data 4
        0x7f040064
        0x7f040065
        0x7f040066
        0x7f040067
        0x7f04018c
        0x7f04018d
        0x7f04018e
        0x7f04018f
        0x7f040190
        0x7f040191
        0x7f040192
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
