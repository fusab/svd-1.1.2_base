.class Lexpo/modules/ads/facebook/NativeAdManager$2;
.super Ljava/lang/Object;
.source "NativeAdManager.java"

# interfaces
.implements Lorg/unimodules/core/interfaces/services/UIManager$UIBlock;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lexpo/modules/ads/facebook/NativeAdManager;->triggerEvent(ILorg/unimodules/core/Promise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/unimodules/core/interfaces/services/UIManager$UIBlock<",
        "Lexpo/modules/ads/facebook/NativeAdView;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lexpo/modules/ads/facebook/NativeAdManager;

.field final synthetic val$promise:Lorg/unimodules/core/Promise;


# direct methods
.method constructor <init>(Lexpo/modules/ads/facebook/NativeAdManager;Lorg/unimodules/core/Promise;)V
    .locals 0

    .line 121
    iput-object p1, p0, Lexpo/modules/ads/facebook/NativeAdManager$2;->this$0:Lexpo/modules/ads/facebook/NativeAdManager;

    iput-object p2, p0, Lexpo/modules/ads/facebook/NativeAdManager$2;->val$promise:Lorg/unimodules/core/Promise;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public reject(Ljava/lang/Throwable;)V
    .locals 2

    .line 130
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdManager$2;->val$promise:Lorg/unimodules/core/Promise;

    const-string v1, "E_NO_NATIVE_AD_VIEW"

    invoke-virtual {v0, v1, p1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public resolve(Lexpo/modules/ads/facebook/NativeAdView;)V
    .locals 1

    .line 124
    invoke-virtual {p1}, Lexpo/modules/ads/facebook/NativeAdView;->triggerClick()V

    .line 125
    iget-object p1, p0, Lexpo/modules/ads/facebook/NativeAdManager$2;->val$promise:Lorg/unimodules/core/Promise;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic resolve(Ljava/lang/Object;)V
    .locals 0

    .line 121
    check-cast p1, Lexpo/modules/ads/facebook/NativeAdView;

    invoke-virtual {p0, p1}, Lexpo/modules/ads/facebook/NativeAdManager$2;->resolve(Lexpo/modules/ads/facebook/NativeAdView;)V

    return-void
.end method
