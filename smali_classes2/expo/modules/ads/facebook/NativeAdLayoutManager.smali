.class public Lexpo/modules/ads/facebook/NativeAdLayoutManager;
.super Lorg/unimodules/core/ViewManager;
.source "NativeAdLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/unimodules/core/ViewManager<",
        "Lexpo/modules/ads/facebook/NativeAdLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static NAME:Ljava/lang/String; = "NativeAdLayout"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Lorg/unimodules/core/ViewManager;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic createViewInstance(Landroid/content/Context;)Landroid/view/View;
    .locals 0

    .line 7
    invoke-virtual {p0, p1}, Lexpo/modules/ads/facebook/NativeAdLayoutManager;->createViewInstance(Landroid/content/Context;)Lexpo/modules/ads/facebook/NativeAdLayout;

    move-result-object p1

    return-object p1
.end method

.method public createViewInstance(Landroid/content/Context;)Lexpo/modules/ads/facebook/NativeAdLayout;
    .locals 1

    .line 17
    new-instance v0, Lexpo/modules/ads/facebook/NativeAdLayout;

    invoke-direct {v0, p1}, Lexpo/modules/ads/facebook/NativeAdLayout;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 12
    sget-object v0, Lexpo/modules/ads/facebook/NativeAdLayoutManager;->NAME:Ljava/lang/String;

    return-object v0
.end method

.method public getViewManagerType()Lorg/unimodules/core/ViewManager$ViewManagerType;
    .locals 1

    .line 22
    sget-object v0, Lorg/unimodules/core/ViewManager$ViewManagerType;->GROUP:Lorg/unimodules/core/ViewManager$ViewManagerType;

    return-object v0
.end method
