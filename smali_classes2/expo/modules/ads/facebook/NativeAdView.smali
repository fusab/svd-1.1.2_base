.class public Lexpo/modules/ads/facebook/NativeAdView;
.super Landroid/view/ViewGroup;
.source "NativeAdView.java"


# instance fields
.field private mEventEmitter:Lorg/unimodules/core/interfaces/services/EventEmitter;

.field private mMediaView:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/facebook/ads/MediaView;",
            ">;"
        }
    .end annotation
.end field

.field private mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

.field private mNativeAd:Lcom/facebook/ads/NativeAd;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lorg/unimodules/core/ModuleRegistry;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 38
    iput-object p2, p0, Lexpo/modules/ads/facebook/NativeAdView;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    .line 39
    const-class p1, Lorg/unimodules/core/interfaces/services/EventEmitter;

    invoke-virtual {p2, p1}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/unimodules/core/interfaces/services/EventEmitter;

    iput-object p1, p0, Lexpo/modules/ads/facebook/NativeAdView;->mEventEmitter:Lorg/unimodules/core/interfaces/services/EventEmitter;

    return-void
.end method


# virtual methods
.method public getModuleRegistry()Lorg/unimodules/core/ModuleRegistry;
    .locals 1

    .line 43
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdView;->mModuleRegistry:Lorg/unimodules/core/ModuleRegistry;

    return-object v0
.end method

.method public getNativeAd()Lcom/facebook/ads/NativeAd;
    .locals 1

    .line 79
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdView;->mNativeAd:Lcom/facebook/ads/NativeAd;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    return-void
.end method

.method public registerViewsForInteraction(Lcom/facebook/ads/MediaView;Lcom/facebook/ads/AdIconView;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ads/MediaView;",
            "Lcom/facebook/ads/AdIconView;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .line 83
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lexpo/modules/ads/facebook/NativeAdView;->mMediaView:Ljava/lang/ref/WeakReference;

    .line 85
    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdView;->mNativeAd:Lcom/facebook/ads/NativeAd;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/facebook/ads/NativeAd;->registerViewForInteraction(Landroid/view/View;Lcom/facebook/ads/MediaView;Lcom/facebook/ads/MediaView;Ljava/util/List;)V

    return-void
.end method

.method public setNativeAd(Lcom/facebook/ads/NativeAd;)V
    .locals 4

    .line 53
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdView;->mNativeAd:Lcom/facebook/ads/NativeAd;

    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {v0}, Lcom/facebook/ads/NativeAd;->unregisterView()V

    .line 57
    :cond_0
    iput-object p1, p0, Lexpo/modules/ads/facebook/NativeAdView;->mNativeAd:Lcom/facebook/ads/NativeAd;

    const-string v0, "onAdLoaded"

    if-nez p1, :cond_1

    .line 60
    iget-object p1, p0, Lexpo/modules/ads/facebook/NativeAdView;->mEventEmitter:Lorg/unimodules/core/interfaces/services/EventEmitter;

    invoke-virtual {p0}, Lexpo/modules/ads/facebook/NativeAdView;->getId()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {p1, v1, v0, v2}, Lorg/unimodules/core/interfaces/services/EventEmitter;->emit(ILjava/lang/String;Landroid/os/Bundle;)V

    return-void

    .line 64
    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 65
    invoke-virtual {p1}, Lcom/facebook/ads/NativeAd;->getAdHeadline()Ljava/lang/String;

    move-result-object v2

    const-string v3, "headline"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-virtual {p1}, Lcom/facebook/ads/NativeAd;->getAdLinkDescription()Ljava/lang/String;

    move-result-object v2

    const-string v3, "linkDescription"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-virtual {p1}, Lcom/facebook/ads/NativeAd;->getAdvertiserName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "advertiserName"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p1}, Lcom/facebook/ads/NativeAd;->getAdSocialContext()Ljava/lang/String;

    move-result-object v2

    const-string v3, "socialContext"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p1}, Lcom/facebook/ads/NativeAd;->getAdCallToAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "callToActionText"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    invoke-virtual {p1}, Lcom/facebook/ads/NativeAd;->getAdBodyText()Ljava/lang/String;

    move-result-object v2

    const-string v3, "bodyText"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-virtual {p1}, Lcom/facebook/ads/NativeAd;->getAdTranslation()Ljava/lang/String;

    move-result-object v2

    const-string v3, "adTranslation"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p1}, Lcom/facebook/ads/NativeAd;->getPromotedTranslation()Ljava/lang/String;

    move-result-object v2

    const-string v3, "promotedTranslation"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-virtual {p1}, Lcom/facebook/ads/NativeAd;->getSponsoredTranslation()Ljava/lang/String;

    move-result-object p1

    const-string v2, "sponsoredTranslation"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iget-object p1, p0, Lexpo/modules/ads/facebook/NativeAdView;->mEventEmitter:Lorg/unimodules/core/interfaces/services/EventEmitter;

    invoke-virtual {p0}, Lexpo/modules/ads/facebook/NativeAdView;->getId()I

    move-result v2

    invoke-interface {p1, v2, v0, v1}, Lorg/unimodules/core/interfaces/services/EventEmitter;->emit(ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public triggerClick()V
    .locals 1

    .line 90
    iget-object v0, p0, Lexpo/modules/ads/facebook/NativeAdView;->mMediaView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ads/MediaView;

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {v0}, Lcom/facebook/ads/MediaView;->performClick()Z

    :cond_0
    return-void
.end method
