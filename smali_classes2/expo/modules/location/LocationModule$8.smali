.class Lexpo/modules/location/LocationModule$8;
.super Ljava/lang/Object;
.source "LocationModule.java"

# interfaces
.implements Lexpo/modules/location/LocationActivityResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lexpo/modules/location/LocationModule;->enableNetworkProviderAsync(Lorg/unimodules/core/Promise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lexpo/modules/location/LocationModule;

.field final synthetic val$promise:Lorg/unimodules/core/Promise;


# direct methods
.method constructor <init>(Lexpo/modules/location/LocationModule;Lorg/unimodules/core/Promise;)V
    .locals 0

    .line 385
    iput-object p1, p0, Lexpo/modules/location/LocationModule$8;->this$0:Lexpo/modules/location/LocationModule;

    iput-object p2, p0, Lexpo/modules/location/LocationModule$8;->val$promise:Lorg/unimodules/core/Promise;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(I)V
    .locals 1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 389
    iget-object p1, p0, Lexpo/modules/location/LocationModule$8;->val$promise:Lorg/unimodules/core/Promise;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    goto :goto_0

    .line 391
    :cond_0
    iget-object p1, p0, Lexpo/modules/location/LocationModule$8;->val$promise:Lorg/unimodules/core/Promise;

    new-instance v0, Lexpo/modules/location/exceptions/LocationSettingsUnsatisfiedException;

    invoke-direct {v0}, Lexpo/modules/location/exceptions/LocationSettingsUnsatisfiedException;-><init>()V

    invoke-virtual {p1, v0}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
