.class public Lexpo/modules/location/LocationModule;
.super Lorg/unimodules/core/ExportedModule;
.source "LocationModule.java"

# interfaces
.implements Lorg/unimodules/core/interfaces/LifecycleEventListener;
.implements Landroid/hardware/SensorEventListener;
.implements Lorg/unimodules/core/interfaces/ActivityEventListener;


# static fields
.field public static final ACCURACY_BALANCED:I = 0x3

.field public static final ACCURACY_BEST_FOR_NAVIGATION:I = 0x6

.field public static final ACCURACY_HIGH:I = 0x4

.field public static final ACCURACY_HIGHEST:I = 0x5

.field public static final ACCURACY_LOW:I = 0x2

.field public static final ACCURACY_LOWEST:I = 0x1

.field private static final CHECK_SETTINGS_REQUEST_CODE:I = 0x2a

.field private static final DEGREE_DELTA:D = 0.0355

.field public static final GEOFENCING_EVENT_ENTER:I = 0x1

.field public static final GEOFENCING_EVENT_EXIT:I = 0x2

.field public static final GEOFENCING_REGION_STATE_INSIDE:I = 0x1

.field public static final GEOFENCING_REGION_STATE_OUTSIDE:I = 0x2

.field public static final GEOFENCING_REGION_STATE_UNKNOWN:I = 0x0

.field private static final HEADING_EVENT_NAME:Ljava/lang/String; = "Expo.headingChanged"

.field private static final LOCATION_EVENT_NAME:Ljava/lang/String; = "Expo.locationChanged"

.field private static final SHOW_USER_SETTINGS_DIALOG_KEY:Ljava/lang/String; = "mayShowUserSettingsDialog"

.field private static final TAG:Ljava/lang/String; = "LocationModule"

.field private static final TIME_DELTA:F = 50.0f


# instance fields
.field private mAccuracy:I

.field private mActivityProvider:Lorg/unimodules/core/interfaces/ActivityProvider;

.field private mContext:Landroid/content/Context;

.field private mEventEmitter:Lorg/unimodules/core/interfaces/services/EventEmitter;

.field private mGeocoderPaused:Z

.field private mGeofield:Landroid/hardware/GeomagneticField;

.field private mGeomagnetic:[F

.field private mGravity:[F

.field private mHeadingId:I

.field private mLastAzimut:F

.field private mLastUpdate:J

.field private mLocationCallbacks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/gms/location/LocationCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mLocationRequests:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/gms/location/LocationRequest;",
            ">;"
        }
    .end annotation
.end field

.field private mPendingLocationRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lexpo/modules/location/LocationActivityResultListener;",
            ">;"
        }
    .end annotation
.end field

.field private mPermissions:Lorg/unimodules/interfaces/permissions/Permissions;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mTaskManager:Lorg/unimodules/interfaces/taskManager/TaskManagerInterface;

.field private mUIManager:Lorg/unimodules/core/interfaces/services/UIManager;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 121
    invoke-direct {p0, p1}, Lorg/unimodules/core/ExportedModule;-><init>(Landroid/content/Context;)V

    .line 98
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lexpo/modules/location/LocationModule;->mLocationCallbacks:Ljava/util/Map;

    .line 99
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lexpo/modules/location/LocationModule;->mLocationRequests:Ljava/util/Map;

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lexpo/modules/location/LocationModule;->mPendingLocationRequests:Ljava/util/List;

    const/4 v0, 0x0

    .line 112
    iput v0, p0, Lexpo/modules/location/LocationModule;->mLastAzimut:F

    const/4 v0, 0x0

    .line 113
    iput v0, p0, Lexpo/modules/location/LocationModule;->mAccuracy:I

    const-wide/16 v1, 0x0

    .line 114
    iput-wide v1, p0, Lexpo/modules/location/LocationModule;->mLastUpdate:J

    .line 115
    iput-boolean v0, p0, Lexpo/modules/location/LocationModule;->mGeocoderPaused:Z

    .line 122
    iput-object p1, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lexpo/modules/location/LocationModule;)Landroid/content/Context;
    .locals 0

    .line 72
    iget-object p0, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$100(Lexpo/modules/location/LocationModule;I)V
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lexpo/modules/location/LocationModule;->executePendingRequests(I)V

    return-void
.end method

.method static synthetic access$200(Lexpo/modules/location/LocationModule;)Lorg/unimodules/core/interfaces/services/UIManager;
    .locals 0

    .line 72
    iget-object p0, p0, Lexpo/modules/location/LocationModule;->mUIManager:Lorg/unimodules/core/interfaces/services/UIManager;

    return-object p0
.end method

.method static synthetic access$302(Lexpo/modules/location/LocationModule;Landroid/hardware/GeomagneticField;)Landroid/hardware/GeomagneticField;
    .locals 0

    .line 72
    iput-object p1, p0, Lexpo/modules/location/LocationModule;->mGeofield:Landroid/hardware/GeomagneticField;

    return-object p1
.end method

.method private addPendingLocationRequest(Lcom/google/android/gms/location/LocationRequest;Lexpo/modules/location/LocationActivityResultListener;)V
    .locals 1

    .line 528
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mPendingLocationRequests:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 531
    iget-object p2, p0, Lexpo/modules/location/LocationModule;->mPendingLocationRequests:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 532
    invoke-direct {p0, p1}, Lexpo/modules/location/LocationModule;->resolveUserSettingsForRequest(Lcom/google/android/gms/location/LocationRequest;)V

    :cond_0
    return-void
.end method

.method private calcMagNorth(F)F
    .locals 2

    float-to-double v0, p1

    .line 691
    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    double-to-float p1, v0

    const/high16 v0, 0x43b40000    # 360.0f

    add-float/2addr p1, v0

    rem-float/2addr p1, v0

    return p1
.end method

.method private calcTrueNorth(F)F
    .locals 1

    .line 697
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->isMissingPermissions()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mGeofield:Landroid/hardware/GeomagneticField;

    if-nez v0, :cond_0

    goto :goto_0

    .line 700
    :cond_0
    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getDeclination()F

    move-result v0

    add-float/2addr p1, v0

    return p1

    :cond_1
    :goto_0
    const/high16 p1, -0x40800000    # -1.0f

    return p1
.end method

.method private destroyHeadingWatch()V
    .locals 2

    .line 711
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->stopHeadingWatch()V

    const/4 v0, 0x0

    .line 712
    iput-object v0, p0, Lexpo/modules/location/LocationModule;->mSensorManager:Landroid/hardware/SensorManager;

    .line 713
    iput-object v0, p0, Lexpo/modules/location/LocationModule;->mGravity:[F

    .line 714
    iput-object v0, p0, Lexpo/modules/location/LocationModule;->mGeomagnetic:[F

    .line 715
    iput-object v0, p0, Lexpo/modules/location/LocationModule;->mGeofield:Landroid/hardware/GeomagneticField;

    const/4 v0, 0x0

    .line 716
    iput v0, p0, Lexpo/modules/location/LocationModule;->mHeadingId:I

    const/4 v1, 0x0

    .line 717
    iput v1, p0, Lexpo/modules/location/LocationModule;->mLastAzimut:F

    .line 718
    iput v0, p0, Lexpo/modules/location/LocationModule;->mAccuracy:I

    return-void
.end method

.method private executePendingRequests(I)V
    .locals 2

    .line 625
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mPendingLocationRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lexpo/modules/location/LocationActivityResultListener;

    .line 626
    invoke-interface {v1, p1}, Lexpo/modules/location/LocationActivityResultListener;->onResult(I)V

    goto :goto_0

    .line 628
    :cond_0
    iget-object p1, p0, Lexpo/modules/location/LocationModule;->mPendingLocationRequests:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    return-void
.end method

.method private getLastKnownLocation(Ljava/lang/Double;Lcom/google/android/gms/tasks/OnSuccessListener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Double;",
            "Lcom/google/android/gms/tasks/OnSuccessListener<",
            "Landroid/location/Location;",
            ">;)V"
        }
    .end annotation

    .line 508
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/location/LocationServices;->getFusedLocationProviderClient(Landroid/content/Context;)Lcom/google/android/gms/location/FusedLocationProviderClient;

    move-result-object v0

    .line 511
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/location/FusedLocationProviderClient;->getLastLocation()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lexpo/modules/location/LocationModule$10;

    invoke-direct {v1, p0, p1, p2}, Lexpo/modules/location/LocationModule$10;-><init>(Lexpo/modules/location/LocationModule;Ljava/lang/Double;Lcom/google/android/gms/tasks/OnSuccessListener;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnSuccessListener(Lcom/google/android/gms/tasks/OnSuccessListener;)Lcom/google/android/gms/tasks/Task;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    .line 522
    invoke-interface {p2, p1}, Lcom/google/android/gms/tasks/OnSuccessListener;->onSuccess(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private isMissingPermissions()Z
    .locals 2

    .line 500
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mPermissions:Lorg/unimodules/interfaces/permissions/Permissions;

    if-eqz v0, :cond_1

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    .line 502
    invoke-interface {v0, v1}, Lorg/unimodules/interfaces/permissions/Permissions;->getPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mPermissions:Lorg/unimodules/interfaces/permissions/Permissions;

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    .line 503
    invoke-interface {v0, v1}, Lorg/unimodules/interfaces/permissions/Permissions;->getPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private pauseLocationUpdatesForRequest(Ljava/lang/Integer;)V
    .locals 2

    .line 587
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/location/LocationServices;->getFusedLocationProviderClient(Landroid/content/Context;)Lcom/google/android/gms/location/FusedLocationProviderClient;

    move-result-object v0

    .line 589
    iget-object v1, p0, Lexpo/modules/location/LocationModule;->mLocationCallbacks:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 590
    iget-object v1, p0, Lexpo/modules/location/LocationModule;->mLocationCallbacks:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/location/LocationCallback;

    .line 591
    invoke-virtual {v0, p1}, Lcom/google/android/gms/location/FusedLocationProviderClient;->removeLocationUpdates(Lcom/google/android/gms/location/LocationCallback;)Lcom/google/android/gms/tasks/Task;

    :cond_0
    return-void
.end method

.method private removeLocationUpdatesForRequest(Ljava/lang/Integer;)V
    .locals 1

    .line 613
    invoke-direct {p0, p1}, Lexpo/modules/location/LocationModule;->pauseLocationUpdatesForRequest(Ljava/lang/Integer;)V

    .line 614
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mLocationCallbacks:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 615
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mLocationRequests:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private resolveUserSettingsForRequest(Lcom/google/android/gms/location/LocationRequest;)V
    .locals 2

    .line 537
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mActivityProvider:Lorg/unimodules/core/interfaces/ActivityProvider;

    invoke-interface {v0}, Lorg/unimodules/core/interfaces/ActivityProvider;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    .line 541
    invoke-direct {p0, p1}, Lexpo/modules/location/LocationModule;->executePendingRequests(I)V

    return-void

    .line 545
    :cond_0
    new-instance v1, Lcom/google/android/gms/location/LocationSettingsRequest$Builder;

    invoke-direct {v1}, Lcom/google/android/gms/location/LocationSettingsRequest$Builder;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/android/gms/location/LocationSettingsRequest$Builder;->addLocationRequest(Lcom/google/android/gms/location/LocationRequest;)Lcom/google/android/gms/location/LocationSettingsRequest$Builder;

    move-result-object p1

    .line 546
    iget-object v1, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/location/LocationServices;->getSettingsClient(Landroid/content/Context;)Lcom/google/android/gms/location/SettingsClient;

    move-result-object v1

    .line 547
    invoke-virtual {p1}, Lcom/google/android/gms/location/LocationSettingsRequest$Builder;->build()Lcom/google/android/gms/location/LocationSettingsRequest;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/location/SettingsClient;->checkLocationSettings(Lcom/google/android/gms/location/LocationSettingsRequest;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    .line 549
    new-instance v1, Lexpo/modules/location/LocationModule$11;

    invoke-direct {v1, p0}, Lexpo/modules/location/LocationModule$11;-><init>(Lexpo/modules/location/LocationModule;)V

    invoke-virtual {p1, v1}, Lcom/google/android/gms/tasks/Task;->addOnSuccessListener(Lcom/google/android/gms/tasks/OnSuccessListener;)Lcom/google/android/gms/tasks/Task;

    .line 557
    new-instance v1, Lexpo/modules/location/LocationModule$12;

    invoke-direct {v1, p0, v0}, Lexpo/modules/location/LocationModule$12;-><init>(Lexpo/modules/location/LocationModule;Landroid/app/Activity;)V

    invoke-virtual {p1, v1}, Lcom/google/android/gms/tasks/Task;->addOnFailureListener(Lcom/google/android/gms/tasks/OnFailureListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method private resumeLocationUpdates()V
    .locals 6

    .line 596
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/location/LocationServices;->getFusedLocationProviderClient(Landroid/content/Context;)Lcom/google/android/gms/location/FusedLocationProviderClient;

    move-result-object v0

    .line 598
    iget-object v1, p0, Lexpo/modules/location/LocationModule;->mLocationCallbacks:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 599
    iget-object v3, p0, Lexpo/modules/location/LocationModule;->mLocationCallbacks:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/location/LocationCallback;

    .line 600
    iget-object v4, p0, Lexpo/modules/location/LocationModule;->mLocationRequests:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/location/LocationRequest;

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 604
    :try_start_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/gms/location/FusedLocationProviderClient;->requestLocationUpdates(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/LocationCallback;Landroid/os/Looper;)Lcom/google/android/gms/tasks/Task;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 606
    sget-object v3, Lexpo/modules/location/LocationModule;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error occurred while resuming location updates: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    return-void
.end method

.method private sendUpdate()V
    .locals 7

    const/16 v0, 0x9

    .line 662
    new-array v1, v0, [F

    .line 663
    new-array v0, v0, [F

    .line 664
    iget-object v2, p0, Lexpo/modules/location/LocationModule;->mGravity:[F

    iget-object v3, p0, Lexpo/modules/location/LocationModule;->mGeomagnetic:[F

    invoke-static {v1, v0, v2, v3}, Landroid/hardware/SensorManager;->getRotationMatrix([F[F[F[F)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    .line 667
    new-array v0, v0, [F

    .line 668
    invoke-static {v1, v0}, Landroid/hardware/SensorManager;->getOrientation([F[F)[F

    const/4 v1, 0x0

    .line 672
    aget v2, v0, v1

    iget v3, p0, Lexpo/modules/location/LocationModule;->mLastAzimut:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-double v2, v2

    const-wide v4, 0x3fa22d0e56041893L    # 0.0355

    cmpl-double v6, v2, v4

    if-lez v6, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lexpo/modules/location/LocationModule;->mLastUpdate:J

    sub-long/2addr v2, v4

    long-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 673
    aget v2, v0, v1

    iput v2, p0, Lexpo/modules/location/LocationModule;->mLastAzimut:F

    .line 674
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lexpo/modules/location/LocationModule;->mLastUpdate:J

    .line 675
    aget v0, v0, v1

    invoke-direct {p0, v0}, Lexpo/modules/location/LocationModule;->calcMagNorth(F)F

    move-result v0

    .line 676
    invoke-direct {p0, v0}, Lexpo/modules/location/LocationModule;->calcTrueNorth(F)F

    move-result v1

    .line 679
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    float-to-double v3, v1

    float-to-double v0, v0

    .line 680
    iget v5, p0, Lexpo/modules/location/LocationModule;->mAccuracy:I

    invoke-static {v3, v4, v0, v1, v5}, Lexpo/modules/location/LocationHelpers;->headingToBundle(DDI)Landroid/os/Bundle;

    move-result-object v0

    .line 682
    iget v1, p0, Lexpo/modules/location/LocationModule;->mHeadingId:I

    const-string v3, "watchId"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "heading"

    .line 683
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 685
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mEventEmitter:Lorg/unimodules/core/interfaces/services/EventEmitter;

    const-string v1, "Expo.headingChanged"

    invoke-interface {v0, v1, v2}, Lorg/unimodules/core/interfaces/services/EventEmitter;->emit(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method private startHeadingUpdate()V
    .locals 8

    .line 632
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    goto :goto_1

    .line 636
    :cond_0
    invoke-static {v0}, Lio/nlopez/smartlocation/SmartLocation;->with(Landroid/content/Context;)Lio/nlopez/smartlocation/SmartLocation;

    move-result-object v0

    invoke-virtual {v0}, Lio/nlopez/smartlocation/SmartLocation;->location()Lio/nlopez/smartlocation/SmartLocation$LocationControl;

    move-result-object v0

    invoke-virtual {v0}, Lio/nlopez/smartlocation/SmartLocation$LocationControl;->oneFix()Lio/nlopez/smartlocation/SmartLocation$LocationControl;

    move-result-object v0

    sget-object v1, Lio/nlopez/smartlocation/location/config/LocationParams;->BEST_EFFORT:Lio/nlopez/smartlocation/location/config/LocationParams;

    invoke-virtual {v0, v1}, Lio/nlopez/smartlocation/SmartLocation$LocationControl;->config(Lio/nlopez/smartlocation/location/config/LocationParams;)Lio/nlopez/smartlocation/SmartLocation$LocationControl;

    move-result-object v0

    .line 637
    invoke-virtual {v0}, Lio/nlopez/smartlocation/SmartLocation$LocationControl;->getLastLocation()Landroid/location/Location;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 639
    new-instance v0, Landroid/hardware/GeomagneticField;

    .line 640
    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    double-to-float v3, v2

    .line 641
    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    double-to-float v4, v4

    .line 642
    invoke-virtual {v1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v1

    double-to-float v5, v1

    .line 643
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Landroid/hardware/GeomagneticField;-><init>(FFFJ)V

    iput-object v0, p0, Lexpo/modules/location/LocationModule;->mGeofield:Landroid/hardware/GeomagneticField;

    goto :goto_0

    .line 645
    :cond_1
    new-instance v1, Lexpo/modules/location/LocationModule$13;

    invoke-direct {v1, p0}, Lexpo/modules/location/LocationModule$13;-><init>(Lexpo/modules/location/LocationModule;)V

    invoke-virtual {v0, v1}, Lio/nlopez/smartlocation/SmartLocation$LocationControl;->start(Lio/nlopez/smartlocation/OnLocationUpdatedListener;)V

    .line 656
    :goto_0
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 658
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    :cond_2
    :goto_1
    return-void
.end method

.method private startWatching()V
    .locals 1

    .line 722
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    .line 727
    :cond_0
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->isMissingPermissions()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 728
    iput-boolean v0, p0, Lexpo/modules/location/LocationModule;->mGeocoderPaused:Z

    .line 732
    :cond_1
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->resumeLocationUpdates()V

    return-void
.end method

.method private stopHeadingWatch()V
    .locals 1

    .line 704
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    return-void

    .line 707
    :cond_0
    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    return-void
.end method

.method private stopWatching()V
    .locals 2

    .line 736
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    .line 741
    :cond_0
    invoke-static {}, Landroid/location/Geocoder;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->isMissingPermissions()Z

    move-result v0

    if-nez v0, :cond_1

    .line 742
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lio/nlopez/smartlocation/SmartLocation;->with(Landroid/content/Context;)Lio/nlopez/smartlocation/SmartLocation;

    move-result-object v0

    invoke-virtual {v0}, Lio/nlopez/smartlocation/SmartLocation;->geocoding()Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;

    move-result-object v0

    invoke-virtual {v0}, Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;->stop()V

    const/4 v0, 0x1

    .line 743
    iput-boolean v0, p0, Lexpo/modules/location/LocationModule;->mGeocoderPaused:Z

    .line 746
    :cond_1
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mLocationCallbacks:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 747
    invoke-direct {p0, v1}, Lexpo/modules/location/LocationModule;->pauseLocationUpdatesForRequest(Ljava/lang/Integer;)V

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public enableNetworkProviderAsync(Lorg/unimodules/core/Promise;)V
    .locals 2
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 378
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lexpo/modules/location/LocationHelpers;->hasNetworkProviderEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 379
    invoke-virtual {p1, v0}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void

    .line 383
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Lexpo/modules/location/LocationHelpers;->prepareLocationRequest(Ljava/util/Map;)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    .line 385
    new-instance v1, Lexpo/modules/location/LocationModule$8;

    invoke-direct {v1, p0, p1}, Lexpo/modules/location/LocationModule$8;-><init>(Lexpo/modules/location/LocationModule;Lorg/unimodules/core/Promise;)V

    invoke-direct {p0, v0, v1}, Lexpo/modules/location/LocationModule;->addPendingLocationRequest(Lcom/google/android/gms/location/LocationRequest;Lexpo/modules/location/LocationActivityResultListener;)V

    return-void
.end method

.method public geocodeAsync(Ljava/lang/String;Lorg/unimodules/core/Promise;)V
    .locals 2
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 279
    iget-boolean v0, p0, Lexpo/modules/location/LocationModule;->mGeocoderPaused:Z

    if-eqz v0, :cond_0

    const-string p1, "E_CANNOT_GEOCODE"

    const-string v0, "Geocoder is not running."

    .line 280
    invoke-virtual {p2, p1, v0}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 284
    :cond_0
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->isMissingPermissions()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285
    new-instance p1, Lexpo/modules/location/exceptions/LocationUnauthorizedException;

    invoke-direct {p1}, Lexpo/modules/location/exceptions/LocationUnauthorizedException;-><init>()V

    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/Throwable;)V

    return-void

    .line 289
    :cond_1
    invoke-static {}, Landroid/location/Geocoder;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 290
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lio/nlopez/smartlocation/SmartLocation;->with(Landroid/content/Context;)Lio/nlopez/smartlocation/SmartLocation;

    move-result-object v0

    invoke-virtual {v0}, Lio/nlopez/smartlocation/SmartLocation;->geocoding()Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;

    move-result-object v0

    new-instance v1, Lexpo/modules/location/LocationModule$5;

    invoke-direct {v1, p0, p2}, Lexpo/modules/location/LocationModule$5;-><init>(Lexpo/modules/location/LocationModule;Lorg/unimodules/core/Promise;)V

    .line 291
    invoke-virtual {v0, p1, v1}, Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;->direct(Ljava/lang/String;Lio/nlopez/smartlocation/OnGeocodingListener;)V

    goto :goto_0

    :cond_2
    const-string p1, "E_NO_GEOCODER"

    const-string v0, "Geocoder service is not available for this device."

    .line 309
    invoke-virtual {p2, p1, v0}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public getCurrentPositionAsync(Ljava/util/Map;Lorg/unimodules/core/Promise;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/unimodules/core/Promise;",
            ")V"
        }
    .end annotation

    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    const-string v0, "timeout"

    .line 152
    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 153
    :goto_0
    invoke-static {p1}, Lexpo/modules/location/LocationHelpers;->prepareLocationRequest(Ljava/util/Map;)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    const-string v2, "mayShowUserSettingsDialog"

    .line 154
    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v2, 0x1

    .line 157
    :goto_2
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->isMissingPermissions()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 158
    new-instance p1, Lexpo/modules/location/exceptions/LocationUnauthorizedException;

    invoke-direct {p1}, Lexpo/modules/location/exceptions/LocationUnauthorizedException;-><init>()V

    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/Throwable;)V

    return-void

    .line 162
    :cond_3
    new-instance v3, Lexpo/modules/location/utils/TimeoutObject;

    invoke-direct {v3, v0}, Lexpo/modules/location/utils/TimeoutObject;-><init>(Ljava/lang/Long;)V

    .line 163
    new-instance v0, Lexpo/modules/location/LocationModule$1;

    invoke-direct {v0, p0, p2}, Lexpo/modules/location/LocationModule$1;-><init>(Lexpo/modules/location/LocationModule;Lorg/unimodules/core/Promise;)V

    invoke-virtual {v3, v0}, Lexpo/modules/location/utils/TimeoutObject;->onTimeout(Lexpo/modules/location/utils/TimeoutObject$TimeoutListener;)V

    .line 169
    invoke-virtual {v3}, Lexpo/modules/location/utils/TimeoutObject;->start()V

    const-string v0, "maximumAge"

    .line 172
    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 173
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Double;

    .line 175
    new-instance v0, Lexpo/modules/location/LocationModule$2;

    invoke-direct {v0, p0, p2, v3}, Lexpo/modules/location/LocationModule$2;-><init>(Lexpo/modules/location/LocationModule;Lorg/unimodules/core/Promise;Lexpo/modules/location/utils/TimeoutObject;)V

    invoke-direct {p0, p1, v0}, Lexpo/modules/location/LocationModule;->getLastKnownLocation(Ljava/lang/Double;Lcom/google/android/gms/tasks/OnSuccessListener;)V

    .line 186
    :cond_4
    iget-object p1, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lexpo/modules/location/LocationHelpers;->hasNetworkProviderEnabled(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_6

    if-nez v2, :cond_5

    goto :goto_3

    .line 190
    :cond_5
    new-instance p1, Lexpo/modules/location/LocationModule$3;

    invoke-direct {p1, p0, v1, v3, p2}, Lexpo/modules/location/LocationModule$3;-><init>(Lexpo/modules/location/LocationModule;Lcom/google/android/gms/location/LocationRequest;Lexpo/modules/location/utils/TimeoutObject;Lorg/unimodules/core/Promise;)V

    invoke-direct {p0, v1, p1}, Lexpo/modules/location/LocationModule;->addPendingLocationRequest(Lcom/google/android/gms/location/LocationRequest;Lexpo/modules/location/LocationActivityResultListener;)V

    goto :goto_4

    .line 187
    :cond_6
    :goto_3
    invoke-static {p0, v1, v3, p2}, Lexpo/modules/location/LocationHelpers;->requestSingleLocation(Lexpo/modules/location/LocationModule;Lcom/google/android/gms/location/LocationRequest;Lexpo/modules/location/utils/TimeoutObject;Lorg/unimodules/core/Promise;)V

    :goto_4
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "ExpoLocation"

    return-object v0
.end method

.method public getProviderStatusAsync(Lorg/unimodules/core/Promise;)V
    .locals 4
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 205
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    const-string v0, "E_CONTEXT_UNAVAILABLE"

    const-string v1, "Context is not available"

    .line 206
    invoke-virtual {p1, v0, v1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :cond_0
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lio/nlopez/smartlocation/SmartLocation;->with(Landroid/content/Context;)Lio/nlopez/smartlocation/SmartLocation;

    move-result-object v0

    invoke-virtual {v0}, Lio/nlopez/smartlocation/SmartLocation;->location()Lio/nlopez/smartlocation/SmartLocation$LocationControl;

    move-result-object v0

    invoke-virtual {v0}, Lio/nlopez/smartlocation/SmartLocation$LocationControl;->state()Lio/nlopez/smartlocation/location/utils/LocationState;

    move-result-object v0

    .line 211
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 213
    invoke-virtual {v0}, Lio/nlopez/smartlocation/location/utils/LocationState;->locationServicesEnabled()Z

    move-result v2

    const-string v3, "locationServicesEnabled"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 214
    invoke-virtual {v0}, Lio/nlopez/smartlocation/location/utils/LocationState;->isGpsAvailable()Z

    move-result v2

    const-string v3, "gpsAvailable"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 215
    invoke-virtual {v0}, Lio/nlopez/smartlocation/location/utils/LocationState;->isNetworkAvailable()Z

    move-result v2

    const-string v3, "networkAvailable"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 216
    invoke-virtual {v0}, Lio/nlopez/smartlocation/location/utils/LocationState;->isPassiveAvailable()Z

    move-result v2

    const-string v3, "passiveAvailable"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 217
    invoke-virtual {v0}, Lio/nlopez/smartlocation/location/utils/LocationState;->locationServicesEnabled()Z

    move-result v0

    const-string v2, "backgroundModeEnabled"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 219
    invoke-virtual {p1, v1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public hasServicesEnabledAsync(Lorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 399
    invoke-virtual {p0}, Lexpo/modules/location/LocationModule;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lexpo/modules/location/LocationHelpers;->isAnyProviderAvailable(Landroid/content/Context;)Z

    move-result v0

    .line 400
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public hasStartedGeofencingAsync(Ljava/lang/String;Lorg/unimodules/core/Promise;)V
    .locals 2
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 455
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mTaskManager:Lorg/unimodules/interfaces/taskManager/TaskManagerInterface;

    const-class v1, Lexpo/modules/location/taskConsumers/GeofencingTaskConsumer;

    invoke-interface {v0, p1, v1}, Lorg/unimodules/interfaces/taskManager/TaskManagerInterface;->taskHasConsumerOfClass(Ljava/lang/String;Ljava/lang/Class;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public hasStartedLocationUpdatesAsync(Ljava/lang/String;Lorg/unimodules/core/Promise;)V
    .locals 2
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 427
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mTaskManager:Lorg/unimodules/interfaces/taskManager/TaskManagerInterface;

    const-class v1, Lexpo/modules/location/taskConsumers/LocationTaskConsumer;

    invoke-interface {v0, p1, v1}, Lorg/unimodules/interfaces/taskManager/TaskManagerInterface;->taskHasConsumerOfClass(Ljava/lang/String;Ljava/lang/Class;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .line 768
    iput p2, p0, Lexpo/modules/location/LocationModule;->mAccuracy:I

    return-void
.end method

.method public onActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)V
    .locals 0

    const/16 p1, 0x2a

    if-eq p2, p1, :cond_0

    return-void

    .line 779
    :cond_0
    invoke-direct {p0, p3}, Lexpo/modules/location/LocationModule;->executePendingRequests(I)V

    .line 780
    iget-object p1, p0, Lexpo/modules/location/LocationModule;->mUIManager:Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-interface {p1, p0}, Lorg/unimodules/core/interfaces/services/UIManager;->unregisterActivityEventListener(Lorg/unimodules/core/interfaces/ActivityEventListener;)V

    return-void
.end method

.method public onCreate(Lorg/unimodules/core/ModuleRegistry;)V
    .locals 1

    .line 132
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mUIManager:Lorg/unimodules/core/interfaces/services/UIManager;

    if-eqz v0, :cond_0

    .line 133
    invoke-interface {v0, p0}, Lorg/unimodules/core/interfaces/services/UIManager;->unregisterLifecycleEventListener(Lorg/unimodules/core/interfaces/LifecycleEventListener;)V

    .line 136
    :cond_0
    const-class v0, Lorg/unimodules/core/interfaces/services/EventEmitter;

    invoke-virtual {p1, v0}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/services/EventEmitter;

    iput-object v0, p0, Lexpo/modules/location/LocationModule;->mEventEmitter:Lorg/unimodules/core/interfaces/services/EventEmitter;

    .line 137
    const-class v0, Lorg/unimodules/core/interfaces/services/UIManager;

    invoke-virtual {p1, v0}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/core/interfaces/services/UIManager;

    iput-object v0, p0, Lexpo/modules/location/LocationModule;->mUIManager:Lorg/unimodules/core/interfaces/services/UIManager;

    .line 138
    const-class v0, Lorg/unimodules/interfaces/permissions/Permissions;

    invoke-virtual {p1, v0}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/interfaces/permissions/Permissions;

    iput-object v0, p0, Lexpo/modules/location/LocationModule;->mPermissions:Lorg/unimodules/interfaces/permissions/Permissions;

    .line 139
    const-class v0, Lorg/unimodules/interfaces/taskManager/TaskManagerInterface;

    invoke-virtual {p1, v0}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/unimodules/interfaces/taskManager/TaskManagerInterface;

    iput-object v0, p0, Lexpo/modules/location/LocationModule;->mTaskManager:Lorg/unimodules/interfaces/taskManager/TaskManagerInterface;

    .line 140
    const-class v0, Lorg/unimodules/core/interfaces/ActivityProvider;

    invoke-virtual {p1, v0}, Lorg/unimodules/core/ModuleRegistry;->getModule(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/unimodules/core/interfaces/ActivityProvider;

    iput-object p1, p0, Lexpo/modules/location/LocationModule;->mActivityProvider:Lorg/unimodules/core/interfaces/ActivityProvider;

    .line 142
    iget-object p1, p0, Lexpo/modules/location/LocationModule;->mUIManager:Lorg/unimodules/core/interfaces/services/UIManager;

    if-eqz p1, :cond_1

    .line 143
    invoke-interface {p1, p0}, Lorg/unimodules/core/interfaces/services/UIManager;->registerLifecycleEventListener(Lorg/unimodules/core/interfaces/LifecycleEventListener;)V

    :cond_1
    return-void
.end method

.method public onHostDestroy()V
    .locals 0

    .line 803
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->stopWatching()V

    .line 804
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->stopHeadingWatch()V

    return-void
.end method

.method public onHostPause()V
    .locals 0

    .line 797
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->stopWatching()V

    .line 798
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->stopHeadingWatch()V

    return-void
.end method

.method public onHostResume()V
    .locals 0

    .line 791
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->startWatching()V

    .line 792
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->startHeadingUpdate()V

    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 0

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 2

    .line 755
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 756
    iget-object p1, p1, Landroid/hardware/SensorEvent;->values:[F

    iput-object p1, p0, Lexpo/modules/location/LocationModule;->mGravity:[F

    goto :goto_0

    .line 757
    :cond_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 758
    iget-object p1, p1, Landroid/hardware/SensorEvent;->values:[F

    iput-object p1, p0, Lexpo/modules/location/LocationModule;->mGeomagnetic:[F

    .line 760
    :cond_1
    :goto_0
    iget-object p1, p0, Lexpo/modules/location/LocationModule;->mGravity:[F

    if-eqz p1, :cond_2

    iget-object p1, p0, Lexpo/modules/location/LocationModule;->mGeomagnetic:[F

    if-eqz p1, :cond_2

    .line 761
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->sendUpdate()V

    :cond_2
    return-void
.end method

.method public removeWatchAsync(ILorg/unimodules/core/Promise;)V
    .locals 1
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 262
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->isMissingPermissions()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    new-instance p1, Lexpo/modules/location/exceptions/LocationUnauthorizedException;

    invoke-direct {p1}, Lexpo/modules/location/exceptions/LocationUnauthorizedException;-><init>()V

    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/Throwable;)V

    return-void

    .line 268
    :cond_0
    iget v0, p0, Lexpo/modules/location/LocationModule;->mHeadingId:I

    if-ne p1, v0, :cond_1

    .line 269
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->destroyHeadingWatch()V

    goto :goto_0

    .line 271
    :cond_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {p0, p1}, Lexpo/modules/location/LocationModule;->removeLocationUpdatesForRequest(Ljava/lang/Integer;)V

    :goto_0
    const/4 p1, 0x0

    .line 274
    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method requestLocationUpdates(Lcom/google/android/gms/location/LocationRequest;Ljava/lang/Integer;Lexpo/modules/location/LocationRequestCallbacks;)V
    .locals 3

    .line 463
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/location/LocationServices;->getFusedLocationProviderClient(Landroid/content/Context;)Lcom/google/android/gms/location/FusedLocationProviderClient;

    move-result-object v0

    .line 465
    new-instance v1, Lexpo/modules/location/LocationModule$9;

    invoke-direct {v1, p0, p3}, Lexpo/modules/location/LocationModule$9;-><init>(Lexpo/modules/location/LocationModule;Lexpo/modules/location/LocationRequestCallbacks;)V

    if-eqz p2, :cond_0

    .line 485
    iget-object v2, p0, Lexpo/modules/location/LocationModule;->mLocationCallbacks:Ljava/util/Map;

    invoke-interface {v2, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486
    iget-object v2, p0, Lexpo/modules/location/LocationModule;->mLocationRequests:Ljava/util/Map;

    invoke-interface {v2, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 490
    :cond_0
    :try_start_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-virtual {v0, p1, v1, p2}, Lcom/google/android/gms/location/FusedLocationProviderClient;->requestLocationUpdates(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/LocationCallback;Landroid/os/Looper;)Lcom/google/android/gms/tasks/Task;

    .line 491
    invoke-virtual {p3}, Lexpo/modules/location/LocationRequestCallbacks;->onRequestSuccess()V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 493
    new-instance p2, Lexpo/modules/location/exceptions/LocationRequestRejectedException;

    invoke-direct {p2, p1}, Lexpo/modules/location/exceptions/LocationRequestRejectedException;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {p3, p2}, Lexpo/modules/location/LocationRequestCallbacks;->onRequestFailed(Lorg/unimodules/core/errors/CodedException;)V

    :goto_0
    return-void
.end method

.method public requestPermissionsAsync(Lorg/unimodules/core/Promise;)V
    .locals 3
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 351
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mPermissions:Lorg/unimodules/interfaces/permissions/Permissions;

    if-nez v0, :cond_0

    const-string v0, "E_NO_PERMISSIONS"

    const-string v1, "Permissions module is null. Are you sure all the installed Expo modules are properly linked?"

    .line 352
    invoke-virtual {p1, v0, v1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    .line 356
    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lexpo/modules/location/LocationModule$7;

    invoke-direct {v2, p0, p1}, Lexpo/modules/location/LocationModule$7;-><init>(Lexpo/modules/location/LocationModule;Lorg/unimodules/core/Promise;)V

    invoke-interface {v0, v1, v2}, Lorg/unimodules/interfaces/permissions/Permissions;->askForPermissions([Ljava/lang/String;Lorg/unimodules/interfaces/permissions/Permissions$PermissionsRequestListener;)V

    return-void
.end method

.method public reverseGeocodeAsync(Ljava/util/Map;Lorg/unimodules/core/Promise;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/unimodules/core/Promise;",
            ")V"
        }
    .end annotation

    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 315
    iget-boolean v0, p0, Lexpo/modules/location/LocationModule;->mGeocoderPaused:Z

    if-eqz v0, :cond_0

    const-string p1, "E_CANNOT_GEOCODE"

    const-string v0, "Geocoder is not running."

    .line 316
    invoke-virtual {p2, p1, v0}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 320
    :cond_0
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->isMissingPermissions()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 321
    new-instance p1, Lexpo/modules/location/exceptions/LocationUnauthorizedException;

    invoke-direct {p1}, Lexpo/modules/location/exceptions/LocationUnauthorizedException;-><init>()V

    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/Throwable;)V

    return-void

    .line 325
    :cond_1
    new-instance v0, Landroid/location/Location;

    const-string v1, ""

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    const-string v1, "latitude"

    .line 326
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    const-string v1, "longitude"

    .line 327
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLongitude(D)V

    .line 329
    invoke-static {}, Landroid/location/Geocoder;->isPresent()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 330
    iget-object p1, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lio/nlopez/smartlocation/SmartLocation;->with(Landroid/content/Context;)Lio/nlopez/smartlocation/SmartLocation;

    move-result-object p1

    invoke-virtual {p1}, Lio/nlopez/smartlocation/SmartLocation;->geocoding()Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;

    move-result-object p1

    new-instance v1, Lexpo/modules/location/LocationModule$6;

    invoke-direct {v1, p0, p2}, Lexpo/modules/location/LocationModule$6;-><init>(Lexpo/modules/location/LocationModule;Lorg/unimodules/core/Promise;)V

    .line 331
    invoke-virtual {p1, v0, v1}, Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;->reverse(Landroid/location/Location;Lio/nlopez/smartlocation/OnReverseGeocodingListener;)V

    goto :goto_0

    :cond_2
    const-string p1, "E_NO_GEOCODER"

    const-string v0, "Geocoder service is not available for this device."

    .line 345
    invoke-virtual {p2, p1, v0}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method sendLocationResponse(ILandroid/os/Bundle;)V
    .locals 1

    const-string v0, "watchId"

    .line 619
    invoke-virtual {p2, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 620
    iget-object p1, p0, Lexpo/modules/location/LocationModule;->mEventEmitter:Lorg/unimodules/core/interfaces/services/EventEmitter;

    const-string v0, "Expo.locationChanged"

    invoke-interface {p1, v0, p2}, Lorg/unimodules/core/interfaces/services/EventEmitter;->emit(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public startGeofencingAsync(Ljava/lang/String;Ljava/util/Map;Lorg/unimodules/core/Promise;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/unimodules/core/Promise;",
            ")V"
        }
    .end annotation

    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 436
    :try_start_0
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mTaskManager:Lorg/unimodules/interfaces/taskManager/TaskManagerInterface;

    const-class v1, Lexpo/modules/location/taskConsumers/GeofencingTaskConsumer;

    invoke-interface {v0, p1, v1, p2}, Lorg/unimodules/interfaces/taskManager/TaskManagerInterface;->registerTask(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Map;)V

    const/4 p1, 0x0

    .line 437
    invoke-virtual {p3, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 439
    invoke-virtual {p3, p1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public startLocationUpdatesAsync(Ljava/lang/String;Ljava/util/Map;Lorg/unimodules/core/Promise;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/unimodules/core/Promise;",
            ")V"
        }
    .end annotation

    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 408
    :try_start_0
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mTaskManager:Lorg/unimodules/interfaces/taskManager/TaskManagerInterface;

    const-class v1, Lexpo/modules/location/taskConsumers/LocationTaskConsumer;

    invoke-interface {v0, p1, v1, p2}, Lorg/unimodules/interfaces/taskManager/TaskManagerInterface;->registerTask(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Map;)V

    const/4 p1, 0x0

    .line 409
    invoke-virtual {p3, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 411
    invoke-virtual {p3, p1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public stopGeofencingAsync(Ljava/lang/String;Lorg/unimodules/core/Promise;)V
    .locals 2
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 446
    :try_start_0
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mTaskManager:Lorg/unimodules/interfaces/taskManager/TaskManagerInterface;

    const-class v1, Lexpo/modules/location/taskConsumers/GeofencingTaskConsumer;

    invoke-interface {v0, p1, v1}, Lorg/unimodules/interfaces/taskManager/TaskManagerInterface;->unregisterTask(Ljava/lang/String;Ljava/lang/Class;)V

    const/4 p1, 0x0

    .line 447
    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 449
    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public stopLocationUpdatesAsync(Ljava/lang/String;Lorg/unimodules/core/Promise;)V
    .locals 2
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 418
    :try_start_0
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mTaskManager:Lorg/unimodules/interfaces/taskManager/TaskManagerInterface;

    const-class v1, Lexpo/modules/location/taskConsumers/LocationTaskConsumer;

    invoke-interface {v0, p1, v1}, Lorg/unimodules/interfaces/taskManager/TaskManagerInterface;->unregisterTask(Ljava/lang/String;Ljava/lang/Class;)V

    const/4 p1, 0x0

    .line 419
    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 421
    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public watchDeviceHeading(ILorg/unimodules/core/Promise;)V
    .locals 2
    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 226
    iget-object v0, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lexpo/modules/location/LocationModule;->mSensorManager:Landroid/hardware/SensorManager;

    .line 227
    iput p1, p0, Lexpo/modules/location/LocationModule;->mHeadingId:I

    .line 228
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->startHeadingUpdate()V

    const/4 p1, 0x0

    .line 229
    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public watchPositionImplAsync(ILjava/util/Map;Lorg/unimodules/core/Promise;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/unimodules/core/Promise;",
            ")V"
        }
    .end annotation

    .annotation runtime Lorg/unimodules/core/interfaces/ExpoMethod;
    .end annotation

    .line 235
    invoke-direct {p0}, Lexpo/modules/location/LocationModule;->isMissingPermissions()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    new-instance p1, Lexpo/modules/location/exceptions/LocationUnauthorizedException;

    invoke-direct {p1}, Lexpo/modules/location/exceptions/LocationUnauthorizedException;-><init>()V

    invoke-virtual {p3, p1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/Throwable;)V

    return-void

    .line 240
    :cond_0
    invoke-static {p2}, Lexpo/modules/location/LocationHelpers;->prepareLocationRequest(Ljava/util/Map;)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const-string v1, "mayShowUserSettingsDialog"

    .line 241
    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p2, 0x1

    .line 243
    :goto_1
    iget-object v1, p0, Lexpo/modules/location/LocationModule;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lexpo/modules/location/LocationHelpers;->hasNetworkProviderEnabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_4

    if-nez p2, :cond_3

    goto :goto_2

    .line 247
    :cond_3
    new-instance p2, Lexpo/modules/location/LocationModule$4;

    invoke-direct {p2, p0, v0, p1, p3}, Lexpo/modules/location/LocationModule$4;-><init>(Lexpo/modules/location/LocationModule;Lcom/google/android/gms/location/LocationRequest;ILorg/unimodules/core/Promise;)V

    invoke-direct {p0, v0, p2}, Lexpo/modules/location/LocationModule;->addPendingLocationRequest(Lcom/google/android/gms/location/LocationRequest;Lexpo/modules/location/LocationActivityResultListener;)V

    goto :goto_3

    .line 244
    :cond_4
    :goto_2
    invoke-static {p0, v0, p1, p3}, Lexpo/modules/location/LocationHelpers;->requestContinuousUpdates(Lexpo/modules/location/LocationModule;Lcom/google/android/gms/location/LocationRequest;ILorg/unimodules/core/Promise;)V

    :goto_3
    return-void
.end method
