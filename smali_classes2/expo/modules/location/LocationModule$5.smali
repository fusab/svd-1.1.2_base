.class Lexpo/modules/location/LocationModule$5;
.super Ljava/lang/Object;
.source "LocationModule.java"

# interfaces
.implements Lio/nlopez/smartlocation/OnGeocodingListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lexpo/modules/location/LocationModule;->geocodeAsync(Ljava/lang/String;Lorg/unimodules/core/Promise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lexpo/modules/location/LocationModule;

.field final synthetic val$promise:Lorg/unimodules/core/Promise;


# direct methods
.method constructor <init>(Lexpo/modules/location/LocationModule;Lorg/unimodules/core/Promise;)V
    .locals 0

    .line 291
    iput-object p1, p0, Lexpo/modules/location/LocationModule$5;->this$0:Lexpo/modules/location/LocationModule;

    iput-object p2, p0, Lexpo/modules/location/LocationModule$5;->val$promise:Lorg/unimodules/core/Promise;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationResolved(Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lio/nlopez/smartlocation/geocoding/utils/LocationAddress;",
            ">;)V"
        }
    .end annotation

    .line 294
    new-instance p1, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 296
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/nlopez/smartlocation/geocoding/utils/LocationAddress;

    .line 297
    invoke-virtual {v0}, Lio/nlopez/smartlocation/geocoding/utils/LocationAddress;->getLocation()Landroid/location/Location;

    move-result-object v0

    const-class v1, Landroid/os/Bundle;

    invoke-static {v0, v1}, Lexpo/modules/location/LocationHelpers;->locationToCoordsBundle(Landroid/location/Location;Ljava/lang/Class;)Landroid/os/BaseBundle;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 300
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 304
    :cond_1
    iget-object p2, p0, Lexpo/modules/location/LocationModule$5;->this$0:Lexpo/modules/location/LocationModule;

    invoke-static {p2}, Lexpo/modules/location/LocationModule;->access$000(Lexpo/modules/location/LocationModule;)Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lio/nlopez/smartlocation/SmartLocation;->with(Landroid/content/Context;)Lio/nlopez/smartlocation/SmartLocation;

    move-result-object p2

    invoke-virtual {p2}, Lio/nlopez/smartlocation/SmartLocation;->geocoding()Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;

    move-result-object p2

    invoke-virtual {p2}, Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;->stop()V

    .line 305
    iget-object p2, p0, Lexpo/modules/location/LocationModule$5;->val$promise:Lorg/unimodules/core/Promise;

    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method
