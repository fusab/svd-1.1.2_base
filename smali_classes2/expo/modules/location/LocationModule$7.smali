.class Lexpo/modules/location/LocationModule$7;
.super Ljava/lang/Object;
.source "LocationModule.java"

# interfaces
.implements Lorg/unimodules/interfaces/permissions/Permissions$PermissionsRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lexpo/modules/location/LocationModule;->requestPermissionsAsync(Lorg/unimodules/core/Promise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lexpo/modules/location/LocationModule;

.field final synthetic val$promise:Lorg/unimodules/core/Promise;


# direct methods
.method constructor <init>(Lexpo/modules/location/LocationModule;Lorg/unimodules/core/Promise;)V
    .locals 0

    .line 361
    iput-object p1, p0, Lexpo/modules/location/LocationModule$7;->this$0:Lexpo/modules/location/LocationModule;

    iput-object p2, p0, Lexpo/modules/location/LocationModule$7;->val$promise:Lorg/unimodules/core/Promise;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPermissionsResult([I)V
    .locals 3

    .line 364
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget v2, p1, v1

    if-nez v2, :cond_0

    .line 367
    iget-object p1, p0, Lexpo/modules/location/LocationModule$7;->val$promise:Lorg/unimodules/core/Promise;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 371
    :cond_1
    iget-object p1, p0, Lexpo/modules/location/LocationModule$7;->val$promise:Lorg/unimodules/core/Promise;

    new-instance v0, Lexpo/modules/location/exceptions/LocationUnauthorizedException;

    invoke-direct {v0}, Lexpo/modules/location/exceptions/LocationUnauthorizedException;-><init>()V

    invoke-virtual {p1, v0}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/Throwable;)V

    return-void
.end method
