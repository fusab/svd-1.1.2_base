.class Lexpo/modules/location/LocationModule$6;
.super Ljava/lang/Object;
.source "LocationModule.java"

# interfaces
.implements Lio/nlopez/smartlocation/OnReverseGeocodingListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lexpo/modules/location/LocationModule;->reverseGeocodeAsync(Ljava/util/Map;Lorg/unimodules/core/Promise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lexpo/modules/location/LocationModule;

.field final synthetic val$promise:Lorg/unimodules/core/Promise;


# direct methods
.method constructor <init>(Lexpo/modules/location/LocationModule;Lorg/unimodules/core/Promise;)V
    .locals 0

    .line 331
    iput-object p1, p0, Lexpo/modules/location/LocationModule$6;->this$0:Lexpo/modules/location/LocationModule;

    iput-object p2, p0, Lexpo/modules/location/LocationModule$6;->val$promise:Lorg/unimodules/core/Promise;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAddressResolved(Landroid/location/Location;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/location/Location;",
            "Ljava/util/List<",
            "Landroid/location/Address;",
            ">;)V"
        }
    .end annotation

    .line 334
    new-instance p1, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 336
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 337
    invoke-static {v0}, Lexpo/modules/location/LocationHelpers;->addressToBundle(Landroid/location/Address;)Landroid/os/Bundle;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 340
    :cond_0
    iget-object p2, p0, Lexpo/modules/location/LocationModule$6;->this$0:Lexpo/modules/location/LocationModule;

    invoke-static {p2}, Lexpo/modules/location/LocationModule;->access$000(Lexpo/modules/location/LocationModule;)Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lio/nlopez/smartlocation/SmartLocation;->with(Landroid/content/Context;)Lio/nlopez/smartlocation/SmartLocation;

    move-result-object p2

    invoke-virtual {p2}, Lio/nlopez/smartlocation/SmartLocation;->geocoding()Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;

    move-result-object p2

    invoke-virtual {p2}, Lio/nlopez/smartlocation/SmartLocation$GeocodingControl;->stop()V

    .line 341
    iget-object p2, p0, Lexpo/modules/location/LocationModule$6;->val$promise:Lorg/unimodules/core/Promise;

    invoke-virtual {p2, p1}, Lorg/unimodules/core/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method
