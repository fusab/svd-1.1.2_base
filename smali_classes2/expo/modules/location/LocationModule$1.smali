.class Lexpo/modules/location/LocationModule$1;
.super Ljava/lang/Object;
.source "LocationModule.java"

# interfaces
.implements Lexpo/modules/location/utils/TimeoutObject$TimeoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lexpo/modules/location/LocationModule;->getCurrentPositionAsync(Ljava/util/Map;Lorg/unimodules/core/Promise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lexpo/modules/location/LocationModule;

.field final synthetic val$promise:Lorg/unimodules/core/Promise;


# direct methods
.method constructor <init>(Lexpo/modules/location/LocationModule;Lorg/unimodules/core/Promise;)V
    .locals 0

    .line 163
    iput-object p1, p0, Lexpo/modules/location/LocationModule$1;->this$0:Lexpo/modules/location/LocationModule;

    iput-object p2, p0, Lexpo/modules/location/LocationModule$1;->val$promise:Lorg/unimodules/core/Promise;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeout()V
    .locals 2

    .line 166
    iget-object v0, p0, Lexpo/modules/location/LocationModule$1;->val$promise:Lorg/unimodules/core/Promise;

    new-instance v1, Lexpo/modules/location/exceptions/LocationRequestTimeoutException;

    invoke-direct {v1}, Lexpo/modules/location/exceptions/LocationRequestTimeoutException;-><init>()V

    invoke-virtual {v0, v1}, Lorg/unimodules/core/Promise;->reject(Ljava/lang/Throwable;)V

    return-void
.end method
