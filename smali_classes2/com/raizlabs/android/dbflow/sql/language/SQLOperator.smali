.class public interface abstract Lcom/raizlabs/android/dbflow/sql/language/SQLOperator;
.super Ljava/lang/Object;
.source "SQLOperator.java"


# virtual methods
.method public abstract appendConditionToQuery(Lcom/raizlabs/android/dbflow/sql/QueryBuilder;)V
    .param p1    # Lcom/raizlabs/android/dbflow/sql/QueryBuilder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract columnName()Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract hasSeparator()Z
.end method

.method public abstract operation()Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract separator(Ljava/lang/String;)Lcom/raizlabs/android/dbflow/sql/language/SQLOperator;
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract separator()Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract value()Ljava/lang/Object;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method
