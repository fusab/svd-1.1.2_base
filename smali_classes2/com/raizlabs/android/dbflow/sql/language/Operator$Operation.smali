.class public Lcom/raizlabs/android/dbflow/sql/language/Operator$Operation;
.super Ljava/lang/Object;
.source "Operator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/raizlabs/android/dbflow/sql/language/Operator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Operation"
.end annotation


# static fields
.field public static final AND:Ljava/lang/String; = "AND"

.field public static final BETWEEN:Ljava/lang/String; = "BETWEEN"

.field public static final CONCATENATE:Ljava/lang/String; = "||"

.field public static final DIVISION:Ljava/lang/String; = "/"

.field public static final EMPTY_PARAM:Ljava/lang/String; = "?"

.field public static final EQUALS:Ljava/lang/String; = "="

.field public static final GLOB:Ljava/lang/String; = "GLOB"

.field public static final GREATER_THAN:Ljava/lang/String; = ">"

.field public static final GREATER_THAN_OR_EQUALS:Ljava/lang/String; = ">="

.field public static final IN:Ljava/lang/String; = "IN"

.field public static final IS_NOT_NULL:Ljava/lang/String; = "IS NOT NULL"

.field public static final IS_NULL:Ljava/lang/String; = "IS NULL"

.field public static final LESS_THAN:Ljava/lang/String; = "<"

.field public static final LESS_THAN_OR_EQUALS:Ljava/lang/String; = "<="

.field public static final LIKE:Ljava/lang/String; = "LIKE"

.field public static final MINUS:Ljava/lang/String; = "-"

.field public static final MOD:Ljava/lang/String; = "%"

.field public static final MULTIPLY:Ljava/lang/String; = "*"

.field public static final NOT_EQUALS:Ljava/lang/String; = "!="

.field public static final NOT_IN:Ljava/lang/String; = "NOT IN"

.field public static final NOT_LIKE:Ljava/lang/String; = "NOT LIKE"

.field public static final OR:Ljava/lang/String; = "OR"

.field public static final PLUS:Ljava/lang/String; = "+"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 637
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
