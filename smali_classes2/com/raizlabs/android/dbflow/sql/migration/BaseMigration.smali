.class public abstract Lcom/raizlabs/android/dbflow/sql/migration/BaseMigration;
.super Ljava/lang/Object;
.source "BaseMigration.java"

# interfaces
.implements Lcom/raizlabs/android/dbflow/sql/migration/Migration;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract migrate(Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;)V
    .param p1    # Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public onPostMigrate()V
    .locals 0

    return-void
.end method

.method public onPreMigrate()V
    .locals 0

    return-void
.end method
