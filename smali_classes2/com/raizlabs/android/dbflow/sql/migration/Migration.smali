.class public interface abstract Lcom/raizlabs/android/dbflow/sql/migration/Migration;
.super Ljava/lang/Object;
.source "Migration.java"


# virtual methods
.method public abstract migrate(Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;)V
    .param p1    # Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onPostMigrate()V
.end method

.method public abstract onPreMigrate()V
.end method
