.class public Lcom/raizlabs/android/dbflow/converter/SqlDateConverter;
.super Lcom/raizlabs/android/dbflow/converter/TypeConverter;
.source "SqlDateConverter.java"


# annotations
.annotation build Lcom/raizlabs/android/dbflow/annotation/TypeConverter;
    allowedSubtypes = {
        Ljava/sql/Time;,
        Ljava/sql/Timestamp;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/raizlabs/android/dbflow/converter/TypeConverter<",
        "Ljava/lang/Long;",
        "Ljava/sql/Date;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/raizlabs/android/dbflow/converter/TypeConverter;-><init>()V

    return-void
.end method


# virtual methods
.method public getDBValue(Ljava/sql/Date;)Ljava/lang/Long;
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 16
    :cond_0
    invoke-virtual {p1}, Ljava/sql/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic getDBValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Ljava/sql/Date;

    invoke-virtual {p0, p1}, Lcom/raizlabs/android/dbflow/converter/SqlDateConverter;->getDBValue(Ljava/sql/Date;)Ljava/lang/Long;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getModelValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/raizlabs/android/dbflow/converter/SqlDateConverter;->getModelValue(Ljava/lang/Long;)Ljava/sql/Date;

    move-result-object p1

    return-object p1
.end method

.method public getModelValue(Ljava/lang/Long;)Ljava/sql/Date;
    .locals 3

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 21
    :cond_0
    new-instance v0, Ljava/sql/Date;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/sql/Date;-><init>(J)V

    move-object p1, v0

    :goto_0
    return-object p1
.end method
