.class public Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;
.super Ljava/lang/Thread;
.source "PriorityTransactionQueue.java"

# interfaces
.implements Lcom/raizlabs/android/dbflow/structure/database/transaction/ITransactionQueue;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue$PriorityEntry;
    }
.end annotation


# instance fields
.field private isQuitting:Z

.field private final queue:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue<",
            "Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue$PriorityEntry<",
            "Lcom/raizlabs/android/dbflow/structure/database/transaction/Transaction;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 19
    iput-boolean p1, p0, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->isQuitting:Z

    .line 28
    new-instance p1, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {p1}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object p1, p0, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->queue:Ljava/util/concurrent/PriorityBlockingQueue;

    return-void
.end method

.method private throwInvalidTransactionType(Lcom/raizlabs/android/dbflow/structure/database/transaction/Transaction;)V
    .locals 3

    .line 121
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Transaction of type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_0

    .line 122
    invoke-virtual {p1}, Lcom/raizlabs/android/dbflow/structure/database/transaction/Transaction;->transaction()Lcom/raizlabs/android/dbflow/structure/database/transaction/ITransaction;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, "Unknown"

    :goto_0
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " should be of type PriorityTransactionWrapper"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public add(Lcom/raizlabs/android/dbflow/structure/database/transaction/Transaction;)V
    .locals 2
    .param p1    # Lcom/raizlabs/android/dbflow/structure/database/transaction/Transaction;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 56
    iget-object v0, p0, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->queue:Ljava/util/concurrent/PriorityBlockingQueue;

    monitor-enter v0

    .line 57
    :try_start_0
    new-instance v1, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue$PriorityEntry;

    invoke-direct {v1, p0, p1}, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue$PriorityEntry;-><init>(Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;Lcom/raizlabs/android/dbflow/structure/database/transaction/Transaction;)V

    .line 58
    iget-object p1, p0, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->queue:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {p1, v1}, Ljava/util/concurrent/PriorityBlockingQueue;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 59
    iget-object p1, p0, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->queue:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {p1, v1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 61
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public cancel(Lcom/raizlabs/android/dbflow/structure/database/transaction/Transaction;)V
    .locals 2
    .param p1    # Lcom/raizlabs/android/dbflow/structure/database/transaction/Transaction;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 71
    iget-object v0, p0, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->queue:Ljava/util/concurrent/PriorityBlockingQueue;

    monitor-enter v0

    .line 72
    :try_start_0
    new-instance v1, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue$PriorityEntry;

    invoke-direct {v1, p0, p1}, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue$PriorityEntry;-><init>(Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;Lcom/raizlabs/android/dbflow/structure/database/transaction/Transaction;)V

    .line 73
    iget-object p1, p0, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->queue:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {p1, v1}, Ljava/util/concurrent/PriorityBlockingQueue;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 74
    iget-object p1, p0, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->queue:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {p1, v1}, Ljava/util/concurrent/PriorityBlockingQueue;->remove(Ljava/lang/Object;)Z

    .line 76
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public cancel(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 86
    iget-object v0, p0, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->queue:Ljava/util/concurrent/PriorityBlockingQueue;

    monitor-enter v0

    .line 87
    :try_start_0
    iget-object v1, p0, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->queue:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/PriorityBlockingQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 88
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 89
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue$PriorityEntry;

    iget-object v2, v2, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue$PriorityEntry;->entry:Lcom/raizlabs/android/dbflow/structure/database/transaction/Transaction;

    .line 90
    invoke-virtual {v2}, Lcom/raizlabs/android/dbflow/structure/database/transaction/Transaction;->name()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/raizlabs/android/dbflow/structure/database/transaction/Transaction;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 91
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 94
    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public quit()V
    .locals 1

    const/4 v0, 0x1

    .line 116
    iput-boolean v0, p0, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->isQuitting:Z

    .line 117
    invoke-virtual {p0}, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->interrupt()V

    return-void
.end method

.method public run()V
    .locals 2

    .line 34
    invoke-static {}, Landroid/os/Looper;->prepare()V

    const/16 v0, 0xa

    .line 35
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 39
    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->queue:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue$PriorityEntry;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    iget-object v0, v0, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue$PriorityEntry;->entry:Lcom/raizlabs/android/dbflow/structure/database/transaction/Transaction;

    invoke-virtual {v0}, Lcom/raizlabs/android/dbflow/structure/database/transaction/Transaction;->executeSync()V

    goto :goto_0

    :catch_0
    nop

    .line 41
    iget-boolean v0, p0, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->isQuitting:Z

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->queue:Ljava/util/concurrent/PriorityBlockingQueue;

    monitor-enter v0

    .line 43
    :try_start_1
    iget-object v1, p0, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->queue:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/PriorityBlockingQueue;->clear()V

    .line 44
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public startIfNotAlive()V
    .locals 2

    .line 99
    monitor-enter p0

    .line 100
    :try_start_0
    invoke-virtual {p0}, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->isAlive()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 102
    :try_start_1
    invoke-virtual {p0}, Lcom/raizlabs/android/dbflow/structure/database/transaction/PriorityTransactionQueue;->start()V
    :try_end_1
    .catch Ljava/lang/IllegalThreadStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 105
    :try_start_2
    sget-object v1, Lcom/raizlabs/android/dbflow/config/FlowLog$Level;->E:Lcom/raizlabs/android/dbflow/config/FlowLog$Level;

    invoke-static {v1, v0}, Lcom/raizlabs/android/dbflow/config/FlowLog;->log(Lcom/raizlabs/android/dbflow/config/FlowLog$Level;Ljava/lang/Throwable;)V

    .line 108
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
