.class public interface abstract Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;
.super Ljava/lang/Object;
.source "DatabaseStatement.java"


# virtual methods
.method public abstract bindBlob(I[B)V
.end method

.method public abstract bindBlobOrNull(I[B)V
    .param p2    # [B
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract bindDouble(ID)V
.end method

.method public abstract bindDoubleOrNull(ILjava/lang/Double;)V
    .param p2    # Ljava/lang/Double;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract bindFloatOrNull(ILjava/lang/Float;)V
    .param p2    # Ljava/lang/Float;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract bindLong(IJ)V
.end method

.method public abstract bindNull(I)V
.end method

.method public abstract bindNumber(ILjava/lang/Number;)V
    .param p2    # Ljava/lang/Number;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract bindNumberOrNull(ILjava/lang/Number;)V
    .param p2    # Ljava/lang/Number;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract bindString(ILjava/lang/String;)V
.end method

.method public abstract bindStringOrNull(ILjava/lang/String;)V
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract close()V
.end method

.method public abstract execute()V
.end method

.method public abstract executeInsert()J
.end method

.method public abstract executeUpdateDelete()J
.end method

.method public abstract simpleQueryForLong()J
.end method

.method public abstract simpleQueryForString()Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method
