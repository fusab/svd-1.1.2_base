.class public interface abstract Lcom/raizlabs/android/dbflow/structure/database/OpenHelper;
.super Ljava/lang/Object;
.source "OpenHelper.java"


# virtual methods
.method public abstract backupDB()V
.end method

.method public abstract closeDB()V
.end method

.method public abstract getDatabase()Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract getDelegate()Lcom/raizlabs/android/dbflow/structure/database/DatabaseHelperDelegate;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract isDatabaseIntegrityOk()Z
.end method

.method public abstract performRestoreFromBackup()V
.end method

.method public abstract setDatabaseListener(Lcom/raizlabs/android/dbflow/structure/database/DatabaseHelperListener;)V
    .param p1    # Lcom/raizlabs/android/dbflow/structure/database/DatabaseHelperListener;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method
