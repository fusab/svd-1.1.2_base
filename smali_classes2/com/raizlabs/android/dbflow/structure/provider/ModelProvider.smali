.class public interface abstract Lcom/raizlabs/android/dbflow/structure/provider/ModelProvider;
.super Ljava/lang/Object;
.source "ModelProvider.java"


# virtual methods
.method public abstract getDeleteUri()Landroid/net/Uri;
.end method

.method public abstract getInsertUri()Landroid/net/Uri;
.end method

.method public abstract getQueryUri()Landroid/net/Uri;
.end method

.method public abstract getUpdateUri()Landroid/net/Uri;
.end method

.method public abstract load()V
.end method

.method public varargs abstract load(Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;Ljava/lang/String;[Ljava/lang/String;)V
    .param p1    # Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method
