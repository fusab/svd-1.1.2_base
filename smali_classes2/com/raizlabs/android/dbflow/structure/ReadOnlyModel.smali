.class public interface abstract Lcom/raizlabs/android/dbflow/structure/ReadOnlyModel;
.super Ljava/lang/Object;
.source "ReadOnlyModel.java"


# virtual methods
.method public abstract exists()Z
.end method

.method public abstract exists(Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;)Z
    .param p1    # Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract load()V
.end method

.method public abstract load(Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;)V
    .param p1    # Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method
