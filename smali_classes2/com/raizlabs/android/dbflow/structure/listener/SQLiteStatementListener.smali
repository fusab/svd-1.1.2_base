.class public interface abstract Lcom/raizlabs/android/dbflow/structure/listener/SQLiteStatementListener;
.super Ljava/lang/Object;
.source "SQLiteStatementListener.java"


# virtual methods
.method public abstract onBindToDeleteStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;)V
    .param p1    # Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onBindToInsertStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;)V
    .param p1    # Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onBindToStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;)V
    .param p1    # Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onBindToUpdateStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;)V
    .param p1    # Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method
