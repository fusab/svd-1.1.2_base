.class public interface abstract annotation Lcom/raizlabs/android/dbflow/annotation/Table;
.super Ljava/lang/Object;
.source "Table.java"

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lcom/raizlabs/android/dbflow/annotation/Table;
        allFields = false
        assignDefaultValuesFromCursor = true
        cacheSize = 0x19
        cachingEnabled = false
        createWithDatabase = true
        indexGroups = {}
        inheritedColumns = {}
        inheritedPrimaryKeys = {}
        insertConflict = .enum Lcom/raizlabs/android/dbflow/annotation/ConflictAction;->NONE:Lcom/raizlabs/android/dbflow/annotation/ConflictAction;
        name = ""
        orderedCursorLookUp = false
        primaryKeyConflict = .enum Lcom/raizlabs/android/dbflow/annotation/ConflictAction;->NONE:Lcom/raizlabs/android/dbflow/annotation/ConflictAction;
        uniqueColumnGroups = {}
        updateConflict = .enum Lcom/raizlabs/android/dbflow/annotation/ConflictAction;->NONE:Lcom/raizlabs/android/dbflow/annotation/ConflictAction;
        useBooleanGetterSetters = true
    .end subannotation
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->SOURCE:Ljava/lang/annotation/RetentionPolicy;
.end annotation

.annotation runtime Ljava/lang/annotation/Target;
    value = {
        .enum Ljava/lang/annotation/ElementType;->TYPE:Ljava/lang/annotation/ElementType;
    }
.end annotation


# static fields
.field public static final DEFAULT_CACHE_SIZE:I = 0x19


# virtual methods
.method public abstract allFields()Z
.end method

.method public abstract assignDefaultValuesFromCursor()Z
.end method

.method public abstract cacheSize()I
.end method

.method public abstract cachingEnabled()Z
.end method

.method public abstract createWithDatabase()Z
.end method

.method public abstract database()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end method

.method public abstract indexGroups()[Lcom/raizlabs/android/dbflow/annotation/IndexGroup;
.end method

.method public abstract inheritedColumns()[Lcom/raizlabs/android/dbflow/annotation/InheritedColumn;
.end method

.method public abstract inheritedPrimaryKeys()[Lcom/raizlabs/android/dbflow/annotation/InheritedPrimaryKey;
.end method

.method public abstract insertConflict()Lcom/raizlabs/android/dbflow/annotation/ConflictAction;
.end method

.method public abstract name()Ljava/lang/String;
.end method

.method public abstract orderedCursorLookUp()Z
.end method

.method public abstract primaryKeyConflict()Lcom/raizlabs/android/dbflow/annotation/ConflictAction;
.end method

.method public abstract uniqueColumnGroups()[Lcom/raizlabs/android/dbflow/annotation/UniqueGroup;
.end method

.method public abstract updateConflict()Lcom/raizlabs/android/dbflow/annotation/ConflictAction;
.end method

.method public abstract useBooleanGetterSetters()Z
.end method
