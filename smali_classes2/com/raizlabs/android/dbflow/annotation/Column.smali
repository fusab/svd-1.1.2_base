.class public interface abstract annotation Lcom/raizlabs/android/dbflow/annotation/Column;
.super Ljava/lang/Object;
.source "Column.java"

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lcom/raizlabs/android/dbflow/annotation/Column;
        collate = .enum Lcom/raizlabs/android/dbflow/annotation/Collate;->NONE:Lcom/raizlabs/android/dbflow/annotation/Collate;
        defaultValue = ""
        getterName = ""
        length = -0x1
        name = ""
        setterName = ""
        typeConverter = Lcom/raizlabs/android/dbflow/converter/TypeConverter;
    .end subannotation
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->SOURCE:Ljava/lang/annotation/RetentionPolicy;
.end annotation

.annotation runtime Ljava/lang/annotation/Target;
    value = {
        .enum Ljava/lang/annotation/ElementType;->FIELD:Ljava/lang/annotation/ElementType;
    }
.end annotation


# virtual methods
.method public abstract collate()Lcom/raizlabs/android/dbflow/annotation/Collate;
.end method

.method public abstract defaultValue()Ljava/lang/String;
.end method

.method public abstract getterName()Ljava/lang/String;
.end method

.method public abstract length()I
.end method

.method public abstract name()Ljava/lang/String;
.end method

.method public abstract setterName()Ljava/lang/String;
.end method

.method public abstract typeConverter()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/raizlabs/android/dbflow/converter/TypeConverter;",
            ">;"
        }
    .end annotation
.end method
