.class public interface abstract annotation Lcom/raizlabs/android/dbflow/annotation/ForeignKey;
.super Ljava/lang/Object;
.source "ForeignKey.java"

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lcom/raizlabs/android/dbflow/annotation/ForeignKey;
        deferred = false
        deleteForeignKeyModel = false
        onDelete = .enum Lcom/raizlabs/android/dbflow/annotation/ForeignKeyAction;->NO_ACTION:Lcom/raizlabs/android/dbflow/annotation/ForeignKeyAction;
        onUpdate = .enum Lcom/raizlabs/android/dbflow/annotation/ForeignKeyAction;->NO_ACTION:Lcom/raizlabs/android/dbflow/annotation/ForeignKeyAction;
        references = {}
        saveForeignKeyModel = false
        stubbedRelationship = false
        tableClass = Ljava/lang/Object;
    .end subannotation
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->SOURCE:Ljava/lang/annotation/RetentionPolicy;
.end annotation

.annotation runtime Ljava/lang/annotation/Target;
    value = {
        .enum Ljava/lang/annotation/ElementType;->FIELD:Ljava/lang/annotation/ElementType;
    }
.end annotation


# virtual methods
.method public abstract deferred()Z
.end method

.method public abstract deleteForeignKeyModel()Z
.end method

.method public abstract onDelete()Lcom/raizlabs/android/dbflow/annotation/ForeignKeyAction;
.end method

.method public abstract onUpdate()Lcom/raizlabs/android/dbflow/annotation/ForeignKeyAction;
.end method

.method public abstract references()[Lcom/raizlabs/android/dbflow/annotation/ForeignKeyReference;
.end method

.method public abstract saveForeignKeyModel()Z
.end method

.method public abstract stubbedRelationship()Z
.end method

.method public abstract tableClass()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end method
