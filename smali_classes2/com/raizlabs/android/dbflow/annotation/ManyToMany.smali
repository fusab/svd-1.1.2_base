.class public interface abstract annotation Lcom/raizlabs/android/dbflow/annotation/ManyToMany;
.super Ljava/lang/Object;
.source "ManyToMany.java"

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lcom/raizlabs/android/dbflow/annotation/ManyToMany;
        generateAutoIncrement = true
        generatedTableClassName = ""
        referencedTableColumnName = ""
        saveForeignKeyModels = false
        thisTableColumnName = ""
    .end subannotation
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->SOURCE:Ljava/lang/annotation/RetentionPolicy;
.end annotation

.annotation runtime Ljava/lang/annotation/Target;
    value = {
        .enum Ljava/lang/annotation/ElementType;->TYPE:Ljava/lang/annotation/ElementType;
    }
.end annotation


# virtual methods
.method public abstract generateAutoIncrement()Z
.end method

.method public abstract generatedTableClassName()Ljava/lang/String;
.end method

.method public abstract referencedTable()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end method

.method public abstract referencedTableColumnName()Ljava/lang/String;
.end method

.method public abstract saveForeignKeyModels()Z
.end method

.method public abstract thisTableColumnName()Ljava/lang/String;
.end method
