.class public interface abstract annotation Lcom/raizlabs/android/dbflow/annotation/InheritedColumn;
.super Ljava/lang/Object;
.source "InheritedColumn.java"

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lcom/raizlabs/android/dbflow/annotation/InheritedColumn;
        nonNullConflict = .enum Lcom/raizlabs/android/dbflow/annotation/ConflictAction;->NONE:Lcom/raizlabs/android/dbflow/annotation/ConflictAction;
    .end subannotation
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->SOURCE:Ljava/lang/annotation/RetentionPolicy;
.end annotation

.annotation runtime Ljava/lang/annotation/Target;
    value = {
        .enum Ljava/lang/annotation/ElementType;->ANNOTATION_TYPE:Ljava/lang/annotation/ElementType;
    }
.end annotation


# virtual methods
.method public abstract column()Lcom/raizlabs/android/dbflow/annotation/Column;
.end method

.method public abstract fieldName()Ljava/lang/String;
.end method

.method public abstract nonNullConflict()Lcom/raizlabs/android/dbflow/annotation/ConflictAction;
.end method
