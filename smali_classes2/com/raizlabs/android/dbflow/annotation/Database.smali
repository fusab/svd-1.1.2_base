.class public interface abstract annotation Lcom/raizlabs/android/dbflow/annotation/Database;
.super Ljava/lang/Object;
.source "Database.java"

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lcom/raizlabs/android/dbflow/annotation/Database;
        backupEnabled = false
        consistencyCheckEnabled = false
        databaseExtension = ""
        foreignKeyConstraintsEnforced = false
        generatedClassSeparator = "_"
        inMemory = false
        insertConflict = .enum Lcom/raizlabs/android/dbflow/annotation/ConflictAction;->NONE:Lcom/raizlabs/android/dbflow/annotation/ConflictAction;
        name = ""
        updateConflict = .enum Lcom/raizlabs/android/dbflow/annotation/ConflictAction;->NONE:Lcom/raizlabs/android/dbflow/annotation/ConflictAction;
    .end subannotation
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->SOURCE:Ljava/lang/annotation/RetentionPolicy;
.end annotation

.annotation runtime Ljava/lang/annotation/Target;
    value = {
        .enum Ljava/lang/annotation/ElementType;->TYPE:Ljava/lang/annotation/ElementType;
    }
.end annotation


# virtual methods
.method public abstract backupEnabled()Z
.end method

.method public abstract consistencyCheckEnabled()Z
.end method

.method public abstract databaseExtension()Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract foreignKeyConstraintsEnforced()Z
.end method

.method public abstract generatedClassSeparator()Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract inMemory()Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract insertConflict()Lcom/raizlabs/android/dbflow/annotation/ConflictAction;
.end method

.method public abstract name()Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract updateConflict()Lcom/raizlabs/android/dbflow/annotation/ConflictAction;
.end method

.method public abstract version()I
.end method
