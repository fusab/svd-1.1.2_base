.class public interface abstract Lcom/raizlabs/android/dbflow/list/FlowCursorList$OnCursorRefreshListener;
.super Ljava/lang/Object;
.source "FlowCursorList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/raizlabs/android/dbflow/list/FlowCursorList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnCursorRefreshListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TModel:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract onCursorRefreshed(Lcom/raizlabs/android/dbflow/list/FlowCursorList;)V
    .param p1    # Lcom/raizlabs/android/dbflow/list/FlowCursorList;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/raizlabs/android/dbflow/list/FlowCursorList<",
            "TTModel;>;)V"
        }
    .end annotation
.end method
