.class Lcom/polidea/reactnativeble/BleModule$6;
.super Ljava/lang/Object;
.source "BleModule.java"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/polidea/reactnativeble/BleModule;->disable(Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/polidea/reactnativeble/BleModule;

.field final synthetic val$safePromise:Lcom/polidea/reactnativeble/utils/SafePromise;

.field final synthetic val$transactionId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;)V
    .locals 0

    .line 282
    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule$6;->this$0:Lcom/polidea/reactnativeble/BleModule;

    iput-object p2, p0, Lcom/polidea/reactnativeble/BleModule$6;->val$safePromise:Lcom/polidea/reactnativeble/utils/SafePromise;

    iput-object p3, p0, Lcom/polidea/reactnativeble/BleModule$6;->val$transactionId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 282
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/polidea/reactnativeble/BleModule$6;->call(Ljava/lang/Throwable;)V

    return-void
.end method

.method public call(Ljava/lang/Throwable;)V
    .locals 1

    .line 285
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$6;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {v0}, Lcom/polidea/reactnativeble/BleModule;->access$100(Lcom/polidea/reactnativeble/BleModule;)Lcom/polidea/reactnativeble/errors/ErrorConverter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/polidea/reactnativeble/errors/ErrorConverter;->toError(Ljava/lang/Throwable;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$6;->val$safePromise:Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-virtual {p1, v0}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/polidea/reactnativeble/utils/SafePromise;)V

    .line 286
    iget-object p1, p0, Lcom/polidea/reactnativeble/BleModule$6;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {p1}, Lcom/polidea/reactnativeble/BleModule;->access$000(Lcom/polidea/reactnativeble/BleModule;)Lcom/polidea/reactnativeble/utils/DisposableMap;

    move-result-object p1

    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$6;->val$transactionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/polidea/reactnativeble/utils/DisposableMap;->removeSubscription(Ljava/lang/String;)Z

    return-void
.end method
