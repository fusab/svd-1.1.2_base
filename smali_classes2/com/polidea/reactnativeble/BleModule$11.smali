.class Lcom/polidea/reactnativeble/BleModule$11;
.super Ljava/lang/Object;
.source "BleModule.java"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/polidea/reactnativeble/BleModule;->safeStartDeviceScan([Ljava/util/UUID;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/polidea/rxandroidble/scan/ScanResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/polidea/reactnativeble/BleModule;


# direct methods
.method constructor <init>(Lcom/polidea/reactnativeble/BleModule;)V
    .locals 0

    .line 418
    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule$11;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Lcom/polidea/rxandroidble/scan/ScanResult;)V
    .locals 5

    .line 421
    invoke-virtual {p1}, Lcom/polidea/rxandroidble/scan/ScanResult;->getBleDevice()Lcom/polidea/rxandroidble/RxBleDevice;

    move-result-object v0

    invoke-interface {v0}, Lcom/polidea/rxandroidble/RxBleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    .line 422
    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule$11;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {v1}, Lcom/polidea/reactnativeble/BleModule;->access$400(Lcom/polidea/reactnativeble/BleModule;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 423
    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule$11;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {v1}, Lcom/polidea/reactnativeble/BleModule;->access$400(Lcom/polidea/reactnativeble/BleModule;)Ljava/util/HashMap;

    move-result-object v1

    new-instance v2, Lcom/polidea/reactnativeble/wrapper/Device;

    invoke-virtual {p1}, Lcom/polidea/rxandroidble/scan/ScanResult;->getBleDevice()Lcom/polidea/rxandroidble/RxBleDevice;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/polidea/reactnativeble/wrapper/Device;-><init>(Lcom/polidea/rxandroidble/RxBleDevice;Lcom/polidea/rxandroidble/RxBleConnection;)V

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$11;->this$0:Lcom/polidea/reactnativeble/BleModule;

    sget-object v1, Lcom/polidea/reactnativeble/Event;->ScanEvent:Lcom/polidea/reactnativeble/Event;

    iget-object v2, p0, Lcom/polidea/reactnativeble/BleModule$11;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {v2}, Lcom/polidea/reactnativeble/BleModule;->access$500(Lcom/polidea/reactnativeble/BleModule;)Lcom/polidea/reactnativeble/converter/RxBleScanResultConverter;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/polidea/reactnativeble/converter/RxBleScanResultConverter;->toJSCallback(Ljava/lang/Object;)Lcom/facebook/react/bridge/WritableArray;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/polidea/reactnativeble/BleModule;->access$200(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/Event;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 418
    check-cast p1, Lcom/polidea/rxandroidble/scan/ScanResult;

    invoke-virtual {p0, p1}, Lcom/polidea/reactnativeble/BleModule$11;->call(Lcom/polidea/rxandroidble/scan/ScanResult;)V

    return-void
.end method
