.class Lcom/polidea/reactnativeble/BleModule$39;
.super Ljava/lang/Object;
.source "BleModule.java"

# interfaces
.implements Lrx/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/polidea/reactnativeble/BleModule;->safeWriteDescriptorForDevice(Lcom/polidea/reactnativeble/wrapper/Descriptor;Ljava/lang/String;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observer<",
        "[B>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/polidea/reactnativeble/BleModule;

.field final synthetic val$descriptor:Lcom/polidea/reactnativeble/wrapper/Descriptor;

.field final synthetic val$promise:Lcom/polidea/reactnativeble/utils/SafePromise;

.field final synthetic val$transactionId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/polidea/reactnativeble/BleModule;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;Lcom/polidea/reactnativeble/wrapper/Descriptor;)V
    .locals 0

    .line 1586
    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule$39;->this$0:Lcom/polidea/reactnativeble/BleModule;

    iput-object p2, p0, Lcom/polidea/reactnativeble/BleModule$39;->val$transactionId:Ljava/lang/String;

    iput-object p3, p0, Lcom/polidea/reactnativeble/BleModule$39;->val$promise:Lcom/polidea/reactnativeble/utils/SafePromise;

    iput-object p4, p0, Lcom/polidea/reactnativeble/BleModule$39;->val$descriptor:Lcom/polidea/reactnativeble/wrapper/Descriptor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 2

    .line 1589
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$39;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {v0}, Lcom/polidea/reactnativeble/BleModule;->access$000(Lcom/polidea/reactnativeble/BleModule;)Lcom/polidea/reactnativeble/utils/DisposableMap;

    move-result-object v0

    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule$39;->val$transactionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/polidea/reactnativeble/utils/DisposableMap;->removeSubscription(Ljava/lang/String;)Z

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 1594
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$39;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {v0}, Lcom/polidea/reactnativeble/BleModule;->access$100(Lcom/polidea/reactnativeble/BleModule;)Lcom/polidea/reactnativeble/errors/ErrorConverter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/polidea/reactnativeble/errors/ErrorConverter;->toError(Ljava/lang/Throwable;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$39;->val$promise:Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-virtual {p1, v0}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/polidea/reactnativeble/utils/SafePromise;)V

    .line 1595
    iget-object p1, p0, Lcom/polidea/reactnativeble/BleModule$39;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {p1}, Lcom/polidea/reactnativeble/BleModule;->access$000(Lcom/polidea/reactnativeble/BleModule;)Lcom/polidea/reactnativeble/utils/DisposableMap;

    move-result-object p1

    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$39;->val$transactionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/polidea/reactnativeble/utils/DisposableMap;->removeSubscription(Ljava/lang/String;)Z

    return-void
.end method

.method public bridge synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 1586
    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/polidea/reactnativeble/BleModule$39;->onNext([B)V

    return-void
.end method

.method public onNext([B)V
    .locals 2

    .line 1600
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$39;->val$descriptor:Lcom/polidea/reactnativeble/wrapper/Descriptor;

    const-string v1, "Write to"

    invoke-virtual {v0, v1, p1}, Lcom/polidea/reactnativeble/wrapper/Descriptor;->logValue(Ljava/lang/String;[B)V

    .line 1601
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$39;->val$promise:Lcom/polidea/reactnativeble/utils/SafePromise;

    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule$39;->val$descriptor:Lcom/polidea/reactnativeble/wrapper/Descriptor;

    invoke-virtual {v1, p1}, Lcom/polidea/reactnativeble/wrapper/Descriptor;->toJSObject([B)Lcom/facebook/react/bridge/WritableMap;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/polidea/reactnativeble/utils/SafePromise;->resolve(Ljava/lang/Object;)V

    return-void
.end method
