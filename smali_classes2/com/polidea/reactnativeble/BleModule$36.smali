.class Lcom/polidea/reactnativeble/BleModule$36;
.super Ljava/lang/Object;
.source "BleModule.java"

# interfaces
.implements Lrx/functions/Func0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/polidea/reactnativeble/BleModule;->safeMonitorCharacteristicForDevice(Lcom/polidea/reactnativeble/wrapper/Characteristic;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/Func0<",
        "Lrx/Observable<",
        "Lrx/Observable<",
        "[B>;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/polidea/reactnativeble/BleModule;

.field final synthetic val$connection:Lcom/polidea/rxandroidble/RxBleConnection;

.field final synthetic val$gattCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;


# direct methods
.method constructor <init>(Lcom/polidea/reactnativeble/BleModule;Landroid/bluetooth/BluetoothGattCharacteristic;Lcom/polidea/rxandroidble/RxBleConnection;)V
    .locals 0

    .line 1310
    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule$36;->this$0:Lcom/polidea/reactnativeble/BleModule;

    iput-object p2, p0, Lcom/polidea/reactnativeble/BleModule$36;->val$gattCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    iput-object p3, p0, Lcom/polidea/reactnativeble/BleModule$36;->val$connection:Lcom/polidea/rxandroidble/RxBleConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 1310
    invoke-virtual {p0}, Lcom/polidea/reactnativeble/BleModule$36;->call()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public call()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lrx/Observable<",
            "[B>;>;"
        }
    .end annotation

    .line 1313
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$36;->val$gattCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getProperties()I

    move-result v0

    .line 1314
    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule$36;->val$gattCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    sget-object v2, Lcom/polidea/reactnativeble/wrapper/Characteristic;->CLIENT_CHARACTERISTIC_CONFIG_UUID:Ljava/util/UUID;

    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1315
    sget-object v1, Lcom/polidea/rxandroidble/NotificationSetupMode;->QUICK_SETUP:Lcom/polidea/rxandroidble/NotificationSetupMode;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/polidea/rxandroidble/NotificationSetupMode;->COMPAT:Lcom/polidea/rxandroidble/NotificationSetupMode;

    :goto_0
    and-int/lit8 v2, v0, 0x10

    if-eqz v2, :cond_1

    .line 1319
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$36;->val$connection:Lcom/polidea/rxandroidble/RxBleConnection;

    iget-object v2, p0, Lcom/polidea/reactnativeble/BleModule$36;->val$gattCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-interface {v0, v2, v1}, Lcom/polidea/rxandroidble/RxBleConnection;->setupNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Lcom/polidea/rxandroidble/NotificationSetupMode;)Lrx/Observable;

    move-result-object v0

    return-object v0

    :cond_1
    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_2

    .line 1323
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$36;->val$connection:Lcom/polidea/rxandroidble/RxBleConnection;

    iget-object v2, p0, Lcom/polidea/reactnativeble/BleModule$36;->val$gattCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-interface {v0, v2, v1}, Lcom/polidea/rxandroidble/RxBleConnection;->setupIndication(Landroid/bluetooth/BluetoothGattCharacteristic;Lcom/polidea/rxandroidble/NotificationSetupMode;)Lrx/Observable;

    move-result-object v0

    return-object v0

    .line 1326
    :cond_2
    new-instance v0, Lcom/polidea/reactnativeble/exceptions/CannotMonitorCharacteristicException;

    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule$36;->val$gattCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-direct {v0, v1}, Lcom/polidea/reactnativeble/exceptions/CannotMonitorCharacteristicException;-><init>(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    invoke-static {v0}, Lrx/Observable;->error(Ljava/lang/Throwable;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method
