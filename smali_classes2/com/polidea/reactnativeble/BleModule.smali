.class public Lcom/polidea/reactnativeble/BleModule;
.super Lcom/facebook/react/bridge/ReactContextBaseJavaModule;
.source "BleModule.java"


# static fields
.field private static final NAME:Ljava/lang/String; = "BleClientManager"


# instance fields
.field private adapterStateChangesSubscription:Lrx/Subscription;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private connectedDevices:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/polidea/reactnativeble/wrapper/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final connectingDevices:Lcom/polidea/reactnativeble/utils/DisposableMap;

.field private currentLogLevel:I

.field private discoveredCharacteristics:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/polidea/reactnativeble/wrapper/Characteristic;",
            ">;"
        }
    .end annotation
.end field

.field private discoveredDescriptors:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/polidea/reactnativeble/wrapper/Descriptor;",
            ">;"
        }
    .end annotation
.end field

.field private discoveredDevices:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/polidea/reactnativeble/wrapper/Device;",
            ">;"
        }
    .end annotation
.end field

.field private discoveredServices:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/polidea/reactnativeble/wrapper/Service;",
            ">;"
        }
    .end annotation
.end field

.field private final errorConverter:Lcom/polidea/reactnativeble/errors/ErrorConverter;

.field private rxBleClient:Lcom/polidea/rxandroidble/RxBleClient;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final scanConverter:Lcom/polidea/reactnativeble/converter/RxBleScanResultConverter;

.field private scanSubscription:Lrx/Subscription;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final transactions:Lcom/polidea/reactnativeble/utils/DisposableMap;


# direct methods
.method public constructor <init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V
    .locals 0

    .line 121
    invoke-direct {p0, p1}, Lcom/facebook/react/bridge/ReactContextBaseJavaModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    .line 81
    new-instance p1, Lcom/polidea/reactnativeble/errors/ErrorConverter;

    invoke-direct {p1}, Lcom/polidea/reactnativeble/errors/ErrorConverter;-><init>()V

    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule;->errorConverter:Lcom/polidea/reactnativeble/errors/ErrorConverter;

    .line 82
    new-instance p1, Lcom/polidea/reactnativeble/converter/RxBleScanResultConverter;

    invoke-direct {p1}, Lcom/polidea/reactnativeble/converter/RxBleScanResultConverter;-><init>()V

    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule;->scanConverter:Lcom/polidea/reactnativeble/converter/RxBleScanResultConverter;

    .line 89
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredDevices:Ljava/util/HashMap;

    .line 92
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule;->connectedDevices:Ljava/util/HashMap;

    .line 95
    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredServices:Landroid/util/SparseArray;

    .line 98
    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredCharacteristics:Landroid/util/SparseArray;

    .line 101
    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredDescriptors:Landroid/util/SparseArray;

    .line 104
    new-instance p1, Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-direct {p1}, Lcom/polidea/reactnativeble/utils/DisposableMap;-><init>()V

    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule;->transactions:Lcom/polidea/reactnativeble/utils/DisposableMap;

    .line 107
    new-instance p1, Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-direct {p1}, Lcom/polidea/reactnativeble/utils/DisposableMap;-><init>()V

    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule;->connectingDevices:Lcom/polidea/reactnativeble/utils/DisposableMap;

    const p1, 0x7fffffff

    .line 118
    iput p1, p0, Lcom/polidea/reactnativeble/BleModule;->currentLogLevel:I

    return-void
.end method

.method static synthetic access$000(Lcom/polidea/reactnativeble/BleModule;)Lcom/polidea/reactnativeble/utils/DisposableMap;
    .locals 0

    .line 75
    iget-object p0, p0, Lcom/polidea/reactnativeble/BleModule;->transactions:Lcom/polidea/reactnativeble/utils/DisposableMap;

    return-object p0
.end method

.method static synthetic access$100(Lcom/polidea/reactnativeble/BleModule;)Lcom/polidea/reactnativeble/errors/ErrorConverter;
    .locals 0

    .line 75
    iget-object p0, p0, Lcom/polidea/reactnativeble/BleModule;->errorConverter:Lcom/polidea/reactnativeble/errors/ErrorConverter;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/polidea/reactnativeble/BleModule;)Landroid/util/SparseArray;
    .locals 0

    .line 75
    iget-object p0, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredCharacteristics:Landroid/util/SparseArray;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/polidea/reactnativeble/BleModule;)Landroid/util/SparseArray;
    .locals 0

    .line 75
    iget-object p0, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredDescriptors:Landroid/util/SparseArray;

    return-object p0
.end method

.method static synthetic access$200(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/Event;Ljava/lang/Object;)V
    .locals 0

    .line 75
    invoke-direct {p0, p1, p2}, Lcom/polidea/reactnativeble/BleModule;->sendEvent(Lcom/polidea/reactnativeble/Event;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$300(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/rxandroidble/RxBleAdapterStateObservable$BleAdapterState;)Ljava/lang/String;
    .locals 0

    .line 75
    invoke-direct {p0, p1}, Lcom/polidea/reactnativeble/BleModule;->rxAndroidBleAdapterStateToReactNativeBluetoothState(Lcom/polidea/rxandroidble/RxBleAdapterStateObservable$BleAdapterState;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$400(Lcom/polidea/reactnativeble/BleModule;)Ljava/util/HashMap;
    .locals 0

    .line 75
    iget-object p0, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredDevices:Ljava/util/HashMap;

    return-object p0
.end method

.method static synthetic access$500(Lcom/polidea/reactnativeble/BleModule;)Lcom/polidea/reactnativeble/converter/RxBleScanResultConverter;
    .locals 0

    .line 75
    iget-object p0, p0, Lcom/polidea/reactnativeble/BleModule;->scanConverter:Lcom/polidea/reactnativeble/converter/RxBleScanResultConverter;

    return-object p0
.end method

.method static synthetic access$600(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/rxandroidble/RxBleDevice;Lcom/polidea/reactnativeble/errors/BleError;)V
    .locals 0

    .line 75
    invoke-direct {p0, p1, p2}, Lcom/polidea/reactnativeble/BleModule;->onDeviceDisconnected(Lcom/polidea/rxandroidble/RxBleDevice;Lcom/polidea/reactnativeble/errors/BleError;)V

    return-void
.end method

.method static synthetic access$700(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/wrapper/Device;)V
    .locals 0

    .line 75
    invoke-direct {p0, p1}, Lcom/polidea/reactnativeble/BleModule;->cleanServicesAndCharacteristicsForDevice(Lcom/polidea/reactnativeble/wrapper/Device;)V

    return-void
.end method

.method static synthetic access$800(Lcom/polidea/reactnativeble/BleModule;)Ljava/util/HashMap;
    .locals 0

    .line 75
    iget-object p0, p0, Lcom/polidea/reactnativeble/BleModule;->connectedDevices:Ljava/util/HashMap;

    return-object p0
.end method

.method static synthetic access$900(Lcom/polidea/reactnativeble/BleModule;)Landroid/util/SparseArray;
    .locals 0

    .line 75
    iget-object p0, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredServices:Landroid/util/SparseArray;

    return-object p0
.end method

.method private characteristicsForService(Lcom/polidea/reactnativeble/wrapper/Service;Lcom/facebook/react/bridge/Promise;)V
    .locals 3

    .line 958
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createArray()Lcom/facebook/react/bridge/WritableArray;

    move-result-object v0

    .line 959
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Service;->getCharacteristics()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/polidea/reactnativeble/wrapper/Characteristic;

    const/4 v2, 0x0

    .line 960
    invoke-virtual {v1, v2}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->toJSObject([B)Lcom/facebook/react/bridge/WritableMap;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/WritableArray;->pushMap(Lcom/facebook/react/bridge/WritableMap;)V

    goto :goto_0

    .line 962
    :cond_0
    invoke-interface {p2, v0}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method private cleanServicesAndCharacteristicsForDevice(Lcom/polidea/reactnativeble/wrapper/Device;)V
    .locals 4
    .param p1    # Lcom/polidea/reactnativeble/wrapper/Device;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1849
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredServices:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 1850
    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredServices:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 1851
    iget-object v2, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredServices:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/polidea/reactnativeble/wrapper/Service;

    .line 1853
    invoke-virtual {v2}, Lcom/polidea/reactnativeble/wrapper/Service;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Device;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1854
    iget-object v2, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredServices:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->remove(I)V

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1857
    :cond_1
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredCharacteristics:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_3

    .line 1858
    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredCharacteristics:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 1859
    iget-object v2, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredCharacteristics:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/polidea/reactnativeble/wrapper/Characteristic;

    .line 1861
    invoke-virtual {v2}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Device;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1862
    iget-object v2, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredCharacteristics:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->remove(I)V

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 1865
    :cond_3
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredDescriptors:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-ltz v0, :cond_5

    .line 1866
    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredDescriptors:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 1867
    iget-object v2, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredDescriptors:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/polidea/reactnativeble/wrapper/Descriptor;

    .line 1868
    invoke-virtual {v2}, Lcom/polidea/reactnativeble/wrapper/Descriptor;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Device;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1869
    iget-object v2, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredDescriptors:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->remove(I)V

    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_5
    return-void
.end method

.method private descriptorForCharacteristic(Lcom/polidea/reactnativeble/wrapper/Characteristic;Lcom/facebook/react/bridge/Promise;)V
    .locals 3

    .line 1034
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createArray()Lcom/facebook/react/bridge/WritableArray;

    move-result-object v0

    .line 1035
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getDescriptors()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/polidea/reactnativeble/wrapper/Descriptor;

    const/4 v2, 0x0

    .line 1036
    invoke-virtual {v1, v2}, Lcom/polidea/reactnativeble/wrapper/Descriptor;->toJSObject([B)Lcom/facebook/react/bridge/WritableMap;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/WritableArray;->pushMap(Lcom/facebook/react/bridge/WritableMap;)V

    goto :goto_0

    .line 1038
    :cond_0
    invoke-interface {p2, v0}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method private getCharacteristicOrReject(ILcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Characteristic;
    .locals 1
    .param p2    # Lcom/facebook/react/bridge/Promise;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1785
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredCharacteristics:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/polidea/reactnativeble/wrapper/Characteristic;

    if-nez v0, :cond_0

    .line 1787
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->characteristicNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    return-object v0
.end method

.method private getCharacteristicOrReject(ILjava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Characteristic;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/react/bridge/Promise;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1760
    invoke-static {p2}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->convert(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 p1, 0x1

    .line 1762
    new-array p1, p1, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, p1, v0

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->invalidIdentifiers([Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v1

    .line 1766
    :cond_0
    iget-object v2, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredServices:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/polidea/reactnativeble/wrapper/Service;

    if-nez v2, :cond_1

    .line 1768
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->serviceNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v1

    .line 1772
    :cond_1
    invoke-virtual {v2, v0}, Lcom/polidea/reactnativeble/wrapper/Service;->getCharacteristicByUUID(Ljava/util/UUID;)Lcom/polidea/reactnativeble/wrapper/Characteristic;

    move-result-object p1

    if-nez p1, :cond_2

    .line 1774
    invoke-static {p2}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->characteristicNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v1

    :cond_2
    return-object p1
.end method

.method private getCharacteristicOrReject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Characteristic;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/react/bridge/Promise;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 v0, 0x2

    .line 1728
    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v3, 0x1

    aput-object p3, v1, v3

    invoke-static {v1}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->convert([Ljava/lang/String;)[Ljava/util/UUID;

    move-result-object v1

    const/4 v4, 0x0

    if-nez v1, :cond_0

    .line 1730
    new-array p1, v0, [Ljava/lang/String;

    aput-object p2, p1, v2

    aput-object p3, p1, v3

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->invalidIdentifiers([Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v4

    .line 1734
    :cond_0
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->connectedDevices:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/polidea/reactnativeble/wrapper/Device;

    if-nez v0, :cond_1

    .line 1736
    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->deviceNotConnected(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v4

    .line 1740
    :cond_1
    aget-object p1, v1, v2

    invoke-virtual {v0, p1}, Lcom/polidea/reactnativeble/wrapper/Device;->getServiceByUUID(Ljava/util/UUID;)Lcom/polidea/reactnativeble/wrapper/Service;

    move-result-object p1

    if-nez p1, :cond_2

    .line 1742
    invoke-static {p2}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->serviceNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v4

    .line 1746
    :cond_2
    aget-object p2, v1, v3

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/wrapper/Service;->getCharacteristicByUUID(Ljava/util/UUID;)Lcom/polidea/reactnativeble/wrapper/Characteristic;

    move-result-object p1

    if-nez p1, :cond_3

    .line 1748
    invoke-static {p3}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->characteristicNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v4

    :cond_3
    return-object p1
.end method

.method private getConnectionOrReject(Lcom/polidea/reactnativeble/wrapper/Device;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/rxandroidble/RxBleConnection;
    .locals 1
    .param p1    # Lcom/polidea/reactnativeble/wrapper/Device;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/react/bridge/Promise;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1799
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Device;->getConnection()Lcom/polidea/rxandroidble/RxBleConnection;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1801
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Device;->getDeviceId()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->deviceNotConnected(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    return-object v0
.end method

.method private getConnectionOrReject(Lcom/polidea/reactnativeble/wrapper/Device;Lcom/polidea/reactnativeble/utils/SafePromise;)Lcom/polidea/rxandroidble/RxBleConnection;
    .locals 1
    .param p1    # Lcom/polidea/reactnativeble/wrapper/Device;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/polidea/reactnativeble/utils/SafePromise;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1810
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Device;->getConnection()Lcom/polidea/rxandroidble/RxBleConnection;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1812
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Device;->getDeviceId()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->deviceNotConnected(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/polidea/reactnativeble/utils/SafePromise;)V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    return-object v0
.end method

.method private getCurrentState()Ljava/lang/String;
    .locals 2

    .line 325
    invoke-direct {p0}, Lcom/polidea/reactnativeble/BleModule;->supportsBluetoothLowEnergy()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Unsupported"

    return-object v0

    .line 329
    :cond_0
    invoke-virtual {p0}, Lcom/polidea/reactnativeble/BleModule;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    const-string v1, "bluetooth"

    .line 330
    invoke-virtual {v0, v1}, Lcom/facebook/react/bridge/ReactApplicationContext;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothManager;

    if-nez v0, :cond_1

    const-string v0, "PoweredOff"

    return-object v0

    .line 334
    :cond_1
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    .line 335
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/polidea/reactnativeble/BleModule;->nativeAdapterStateToReactNativeBluetoothState(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDescriptorOrReject(ILcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Descriptor;
    .locals 1
    .param p2    # Lcom/facebook/react/bridge/Promise;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1710
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredDescriptors:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/polidea/reactnativeble/wrapper/Descriptor;

    if-nez v0, :cond_0

    .line 1712
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->descriptorNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    return-object v0
.end method

.method private getDescriptorOrReject(ILjava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Descriptor;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/react/bridge/Promise;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1685
    invoke-static {p2}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->convert(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 p1, 0x1

    .line 1687
    new-array p1, p1, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, p1, v0

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->invalidIdentifiers([Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v1

    .line 1691
    :cond_0
    iget-object v2, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredCharacteristics:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/polidea/reactnativeble/wrapper/Characteristic;

    if-nez v2, :cond_1

    .line 1693
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->characteristicNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v1

    .line 1697
    :cond_1
    invoke-virtual {v2, v0}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getDescriptorByUUID(Ljava/util/UUID;)Lcom/polidea/reactnativeble/wrapper/Descriptor;

    move-result-object p1

    if-nez p1, :cond_2

    .line 1699
    invoke-static {p2}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->descriptorNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v1

    :cond_2
    return-object p1
.end method

.method private getDescriptorOrReject(ILjava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Descriptor;
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/react/bridge/Promise;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 v0, 0x2

    .line 1654
    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v3, 0x1

    aput-object p3, v1, v3

    invoke-static {v1}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->convert([Ljava/lang/String;)[Ljava/util/UUID;

    move-result-object v1

    const/4 v4, 0x0

    if-nez v1, :cond_0

    .line 1656
    new-array p1, v0, [Ljava/lang/String;

    aput-object p2, p1, v2

    aput-object p3, p1, v3

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->invalidIdentifiers([Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v4

    .line 1660
    :cond_0
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredServices:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/polidea/reactnativeble/wrapper/Service;

    if-nez v0, :cond_1

    .line 1662
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->serviceNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v4

    .line 1666
    :cond_1
    aget-object p1, v1, v2

    invoke-virtual {v0, p1}, Lcom/polidea/reactnativeble/wrapper/Service;->getCharacteristicByUUID(Ljava/util/UUID;)Lcom/polidea/reactnativeble/wrapper/Characteristic;

    move-result-object p1

    if-nez p1, :cond_2

    .line 1668
    invoke-static {p2}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->characteristicNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v4

    .line 1672
    :cond_2
    aget-object p2, v1, v3

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getDescriptorByUUID(Ljava/util/UUID;)Lcom/polidea/reactnativeble/wrapper/Descriptor;

    move-result-object p1

    if-nez p1, :cond_3

    .line 1674
    invoke-static {p3}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->descriptorNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v4

    :cond_3
    return-object p1
.end method

.method private getDescriptorOrReject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Descriptor;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/react/bridge/Promise;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 v0, 0x3

    .line 1616
    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v3, 0x1

    aput-object p3, v1, v3

    const/4 v4, 0x2

    aput-object p4, v1, v4

    invoke-static {v1}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->convert([Ljava/lang/String;)[Ljava/util/UUID;

    move-result-object v1

    const/4 v5, 0x0

    if-nez v1, :cond_0

    .line 1618
    new-array p1, v0, [Ljava/lang/String;

    aput-object p2, p1, v2

    aput-object p3, p1, v3

    aput-object p4, p1, v4

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->invalidIdentifiers([Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p5}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v5

    .line 1622
    :cond_0
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->connectedDevices:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/polidea/reactnativeble/wrapper/Device;

    if-nez v0, :cond_1

    .line 1624
    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->deviceNotConnected(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p5}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v5

    .line 1628
    :cond_1
    aget-object p1, v1, v2

    invoke-virtual {v0, p1}, Lcom/polidea/reactnativeble/wrapper/Device;->getServiceByUUID(Ljava/util/UUID;)Lcom/polidea/reactnativeble/wrapper/Service;

    move-result-object p1

    if-nez p1, :cond_2

    .line 1630
    invoke-static {p2}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->serviceNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p5}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v5

    .line 1634
    :cond_2
    aget-object p2, v1, v3

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/wrapper/Service;->getCharacteristicByUUID(Ljava/util/UUID;)Lcom/polidea/reactnativeble/wrapper/Characteristic;

    move-result-object p1

    if-nez p1, :cond_3

    .line 1636
    invoke-static {p3}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->characteristicNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p5}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v5

    .line 1640
    :cond_3
    aget-object p2, v1, v4

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getDescriptorByUUID(Ljava/util/UUID;)Lcom/polidea/reactnativeble/wrapper/Descriptor;

    move-result-object p1

    if-nez p1, :cond_4

    .line 1642
    invoke-static {p4}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->descriptorNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p5}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-object v5

    :cond_4
    return-object p1
.end method

.method private getDeviceOrReject(Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Device;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/react/bridge/Promise;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1832
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->connectedDevices:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/polidea/reactnativeble/wrapper/Device;

    if-nez v0, :cond_0

    .line 1834
    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->deviceNotConnected(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    return-object v0
.end method

.method private getServicesOrReject(Lcom/polidea/reactnativeble/wrapper/Device;Lcom/facebook/react/bridge/Promise;)Ljava/util/List;
    .locals 1
    .param p1    # Lcom/polidea/reactnativeble/wrapper/Device;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/react/bridge/Promise;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/polidea/reactnativeble/wrapper/Device;",
            "Lcom/facebook/react/bridge/Promise;",
            ")",
            "Ljava/util/List<",
            "Lcom/polidea/reactnativeble/wrapper/Service;",
            ">;"
        }
    .end annotation

    .line 1821
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Device;->getServices()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1823
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Device;->getDeviceId()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->deviceServicesNotDiscovered(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    return-object v0
.end method

.method private monitorAdapterStateChanges(Landroid/content/Context;)Lrx/Subscription;
    .locals 1

    .line 304
    invoke-direct {p0}, Lcom/polidea/reactnativeble/BleModule;->supportsBluetoothLowEnergy()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 308
    :cond_0
    new-instance v0, Lcom/polidea/rxandroidble/RxBleAdapterStateObservable;

    invoke-direct {v0, p1}, Lcom/polidea/rxandroidble/RxBleAdapterStateObservable;-><init>(Landroid/content/Context;)V

    new-instance p1, Lcom/polidea/reactnativeble/BleModule$10;

    invoke-direct {p1, p0}, Lcom/polidea/reactnativeble/BleModule$10;-><init>(Lcom/polidea/reactnativeble/BleModule;)V

    .line 309
    invoke-virtual {v0, p1}, Lcom/polidea/rxandroidble/RxBleAdapterStateObservable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/polidea/reactnativeble/BleModule$9;

    invoke-direct {v0, p0}, Lcom/polidea/reactnativeble/BleModule$9;-><init>(Lcom/polidea/reactnativeble/BleModule;)V

    .line 315
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method private nativeAdapterStateToReactNativeBluetoothState(I)Ljava/lang/String;
    .locals 0

    packed-switch p1, :pswitch_data_0

    const-string p1, "Unknown"

    return-object p1

    :pswitch_0
    const-string p1, "PoweredOn"

    return-object p1

    :pswitch_1
    const-string p1, "Resetting"

    return-object p1

    :pswitch_2
    const-string p1, "PoweredOff"

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private onDeviceDisconnected(Lcom/polidea/rxandroidble/RxBleDevice;Lcom/polidea/reactnativeble/errors/BleError;)V
    .locals 2

    .line 780
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->connectedDevices:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/polidea/rxandroidble/RxBleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/polidea/reactnativeble/wrapper/Device;

    if-nez v0, :cond_0

    return-void

    .line 785
    :cond_0
    invoke-direct {p0, v0}, Lcom/polidea/reactnativeble/BleModule;->cleanServicesAndCharacteristicsForDevice(Lcom/polidea/reactnativeble/wrapper/Device;)V

    .line 786
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createArray()Lcom/facebook/react/bridge/WritableArray;

    move-result-object v1

    if-eqz p2, :cond_1

    .line 788
    invoke-virtual {p2}, Lcom/polidea/reactnativeble/errors/BleError;->toJS()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v1, p2}, Lcom/facebook/react/bridge/WritableArray;->pushString(Ljava/lang/String;)V

    goto :goto_0

    .line 790
    :cond_1
    invoke-interface {v1}, Lcom/facebook/react/bridge/WritableArray;->pushNull()V

    :goto_0
    const/4 p2, 0x0

    .line 792
    invoke-virtual {v0, p2}, Lcom/polidea/reactnativeble/wrapper/Device;->toJSObject(Ljava/lang/Integer;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object p2

    invoke-interface {v1, p2}, Lcom/facebook/react/bridge/WritableArray;->pushMap(Lcom/facebook/react/bridge/WritableMap;)V

    .line 793
    sget-object p2, Lcom/polidea/reactnativeble/Event;->DisconnectionEvent:Lcom/polidea/reactnativeble/Event;

    invoke-direct {p0, p2, v1}, Lcom/polidea/reactnativeble/BleModule;->sendEvent(Lcom/polidea/reactnativeble/Event;Ljava/lang/Object;)V

    .line 794
    iget-object p2, p0, Lcom/polidea/reactnativeble/BleModule;->connectingDevices:Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-interface {p1}, Lcom/polidea/rxandroidble/RxBleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/polidea/reactnativeble/utils/DisposableMap;->removeSubscription(Ljava/lang/String;)Z

    return-void
.end method

.method private rxAndroidBleAdapterStateToReactNativeBluetoothState(Lcom/polidea/rxandroidble/RxBleAdapterStateObservable$BleAdapterState;)Ljava/lang/String;
    .locals 1

    .line 361
    sget-object v0, Lcom/polidea/rxandroidble/RxBleAdapterStateObservable$BleAdapterState;->STATE_ON:Lcom/polidea/rxandroidble/RxBleAdapterStateObservable$BleAdapterState;

    if-ne p1, v0, :cond_0

    const-string p1, "PoweredOn"

    return-object p1

    .line 363
    :cond_0
    sget-object v0, Lcom/polidea/rxandroidble/RxBleAdapterStateObservable$BleAdapterState;->STATE_OFF:Lcom/polidea/rxandroidble/RxBleAdapterStateObservable$BleAdapterState;

    if-ne p1, v0, :cond_1

    const-string p1, "PoweredOff"

    return-object p1

    :cond_1
    const-string p1, "Resetting"

    return-object p1
.end method

.method private safeConnectToDevice(Lcom/polidea/rxandroidble/RxBleDevice;ZILcom/polidea/reactnativeble/RefreshGattMoment;Ljava/lang/Integer;ILcom/polidea/reactnativeble/utils/SafePromise;)V
    .locals 1

    .line 685
    invoke-interface {p1, p2}, Lcom/polidea/rxandroidble/RxBleDevice;->establishConnection(Z)Lrx/Observable;

    move-result-object p2

    new-instance v0, Lcom/polidea/reactnativeble/BleModule$20;

    invoke-direct {v0, p0, p7, p1}, Lcom/polidea/reactnativeble/BleModule$20;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Lcom/polidea/rxandroidble/RxBleDevice;)V

    .line 686
    invoke-virtual {p2, v0}, Lrx/Observable;->doOnUnsubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p2

    .line 694
    sget-object v0, Lcom/polidea/reactnativeble/RefreshGattMoment;->ON_CONNECTED:Lcom/polidea/reactnativeble/RefreshGattMoment;

    if-ne p4, v0, :cond_0

    .line 695
    new-instance p4, Lcom/polidea/reactnativeble/BleModule$21;

    invoke-direct {p4, p0}, Lcom/polidea/reactnativeble/BleModule$21;-><init>(Lcom/polidea/reactnativeble/BleModule;)V

    invoke-virtual {p2, p4}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p2

    :cond_0
    const/16 p4, 0x15

    if-lez p6, :cond_1

    .line 710
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, p4, :cond_1

    .line 711
    new-instance v0, Lcom/polidea/reactnativeble/BleModule$22;

    invoke-direct {v0, p0, p6}, Lcom/polidea/reactnativeble/BleModule$22;-><init>(Lcom/polidea/reactnativeble/BleModule;I)V

    invoke-virtual {p2, v0}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p2

    :cond_1
    if-lez p3, :cond_2

    .line 722
    sget p6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt p6, p4, :cond_2

    .line 723
    new-instance p4, Lcom/polidea/reactnativeble/BleModule$23;

    invoke-direct {p4, p0, p3}, Lcom/polidea/reactnativeble/BleModule$23;-><init>(Lcom/polidea/reactnativeble/BleModule;I)V

    invoke-virtual {p2, p4}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p2

    :cond_2
    if-eqz p5, :cond_3

    .line 740
    new-instance p3, Lcom/polidea/reactnativeble/BleModule$24;

    invoke-direct {p3, p0, p5}, Lcom/polidea/reactnativeble/BleModule$24;-><init>(Lcom/polidea/reactnativeble/BleModule;Ljava/lang/Integer;)V

    new-instance p4, Lcom/polidea/reactnativeble/BleModule$25;

    invoke-direct {p4, p0}, Lcom/polidea/reactnativeble/BleModule$25;-><init>(Lcom/polidea/reactnativeble/BleModule;)V

    invoke-virtual {p2, p3, p4}, Lrx/Observable;->timeout(Lrx/functions/Func0;Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p2

    .line 754
    :cond_3
    new-instance p3, Lcom/polidea/reactnativeble/BleModule$26;

    invoke-direct {p3, p0, p7, p1}, Lcom/polidea/reactnativeble/BleModule$26;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Lcom/polidea/rxandroidble/RxBleDevice;)V

    .line 755
    invoke-virtual {p2, p3}, Lrx/Observable;->subscribe(Lrx/Observer;)Lrx/Subscription;

    move-result-object p2

    .line 776
    iget-object p3, p0, Lcom/polidea/reactnativeble/BleModule;->connectingDevices:Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-interface {p1}, Lcom/polidea/rxandroidble/RxBleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1, p2}, Lcom/polidea/reactnativeble/utils/DisposableMap;->replaceSubscription(Ljava/lang/String;Lrx/Subscription;)V

    return-void
.end method

.method private safeDiscoverAllServicesAndCharacteristicsForDevice(Lcom/polidea/reactnativeble/wrapper/Device;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V
    .locals 2

    .line 848
    invoke-direct {p0, p1, p3}, Lcom/polidea/reactnativeble/BleModule;->getConnectionOrReject(Lcom/polidea/reactnativeble/wrapper/Device;Lcom/polidea/reactnativeble/utils/SafePromise;)Lcom/polidea/rxandroidble/RxBleConnection;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 854
    :cond_0
    invoke-interface {v0}, Lcom/polidea/rxandroidble/RxBleConnection;->discoverServices()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/polidea/reactnativeble/BleModule$28;

    invoke-direct {v1, p0, p3, p2}, Lcom/polidea/reactnativeble/BleModule$28;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;)V

    .line 855
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnUnsubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/polidea/reactnativeble/BleModule$27;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/polidea/reactnativeble/BleModule$27;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Lcom/polidea/reactnativeble/wrapper/Device;Ljava/lang/String;)V

    .line 862
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/Observer;)Lrx/Subscription;

    move-result-object p1

    .line 897
    iget-object p3, p0, Lcom/polidea/reactnativeble/BleModule;->transactions:Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-virtual {p3, p2, p1}, Lcom/polidea/reactnativeble/utils/DisposableMap;->replaceSubscription(Ljava/lang/String;Lrx/Subscription;)V

    return-void
.end method

.method private safeMonitorCharacteristicForDevice(Lcom/polidea/reactnativeble/wrapper/Characteristic;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V
    .locals 3

    .line 1303
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getService()Lcom/polidea/reactnativeble/wrapper/Service;

    move-result-object v0

    invoke-virtual {v0}, Lcom/polidea/reactnativeble/wrapper/Service;->getDevice()Lcom/polidea/reactnativeble/wrapper/Device;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/polidea/reactnativeble/BleModule;->getConnectionOrReject(Lcom/polidea/reactnativeble/wrapper/Device;Lcom/polidea/reactnativeble/utils/SafePromise;)Lcom/polidea/rxandroidble/RxBleConnection;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 1308
    :cond_0
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getNativeCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v1

    .line 1310
    new-instance v2, Lcom/polidea/reactnativeble/BleModule$36;

    invoke-direct {v2, p0, v1, v0}, Lcom/polidea/reactnativeble/BleModule$36;-><init>(Lcom/polidea/reactnativeble/BleModule;Landroid/bluetooth/BluetoothGattCharacteristic;Lcom/polidea/rxandroidble/RxBleConnection;)V

    invoke-static {v2}, Lrx/Observable;->defer(Lrx/functions/Func0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/polidea/reactnativeble/BleModule$35;

    invoke-direct {v1, p0}, Lcom/polidea/reactnativeble/BleModule$35;-><init>(Lcom/polidea/reactnativeble/BleModule;)V

    .line 1329
    invoke-virtual {v0, v1}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/polidea/reactnativeble/BleModule$34;

    invoke-direct {v1, p0, p3, p2}, Lcom/polidea/reactnativeble/BleModule$34;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;)V

    .line 1335
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnUnsubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/polidea/reactnativeble/BleModule$33;

    invoke-direct {v1, p0, p3, p2, p1}, Lcom/polidea/reactnativeble/BleModule$33;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;Lcom/polidea/reactnativeble/wrapper/Characteristic;)V

    .line 1342
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/Observer;)Lrx/Subscription;

    move-result-object p1

    .line 1366
    iget-object p3, p0, Lcom/polidea/reactnativeble/BleModule;->transactions:Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-virtual {p3, p2, p1}, Lcom/polidea/reactnativeble/utils/DisposableMap;->replaceSubscription(Ljava/lang/String;Lrx/Subscription;)V

    return-void
.end method

.method private safeReadCharacteristicForDevice(Lcom/polidea/reactnativeble/wrapper/Characteristic;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V
    .locals 2

    .line 1220
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getService()Lcom/polidea/reactnativeble/wrapper/Service;

    move-result-object v0

    invoke-virtual {v0}, Lcom/polidea/reactnativeble/wrapper/Service;->getDevice()Lcom/polidea/reactnativeble/wrapper/Device;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/polidea/reactnativeble/BleModule;->getConnectionOrReject(Lcom/polidea/reactnativeble/wrapper/Device;Lcom/polidea/reactnativeble/utils/SafePromise;)Lcom/polidea/rxandroidble/RxBleConnection;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 1226
    :cond_0
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getNativeCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/polidea/rxandroidble/RxBleConnection;->readCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/polidea/reactnativeble/BleModule$32;

    invoke-direct {v1, p0, p3, p2}, Lcom/polidea/reactnativeble/BleModule$32;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;)V

    .line 1227
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnUnsubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/polidea/reactnativeble/BleModule$31;

    invoke-direct {v1, p0, p2, p3, p1}, Lcom/polidea/reactnativeble/BleModule$31;-><init>(Lcom/polidea/reactnativeble/BleModule;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;Lcom/polidea/reactnativeble/wrapper/Characteristic;)V

    .line 1234
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/Observer;)Lrx/Subscription;

    move-result-object p1

    .line 1253
    iget-object p3, p0, Lcom/polidea/reactnativeble/BleModule;->transactions:Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-virtual {p3, p2, p1}, Lcom/polidea/reactnativeble/utils/DisposableMap;->replaceSubscription(Ljava/lang/String;Lrx/Subscription;)V

    return-void
.end method

.method private safeReadDescriptorForDevice(Lcom/polidea/reactnativeble/wrapper/Descriptor;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V
    .locals 2

    .line 1435
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Descriptor;->getCharacteristic()Lcom/polidea/reactnativeble/wrapper/Characteristic;

    move-result-object v0

    invoke-virtual {v0}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getService()Lcom/polidea/reactnativeble/wrapper/Service;

    move-result-object v0

    invoke-virtual {v0}, Lcom/polidea/reactnativeble/wrapper/Service;->getDevice()Lcom/polidea/reactnativeble/wrapper/Device;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/polidea/reactnativeble/BleModule;->getConnectionOrReject(Lcom/polidea/reactnativeble/wrapper/Device;Lcom/polidea/reactnativeble/utils/SafePromise;)Lcom/polidea/rxandroidble/RxBleConnection;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 1441
    :cond_0
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Descriptor;->getNativeDescriptor()Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/polidea/rxandroidble/RxBleConnection;->readDescriptor(Landroid/bluetooth/BluetoothGattDescriptor;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/polidea/reactnativeble/BleModule$38;

    invoke-direct {v1, p0, p3, p2}, Lcom/polidea/reactnativeble/BleModule$38;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;)V

    .line 1442
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnUnsubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/polidea/reactnativeble/BleModule$37;

    invoke-direct {v1, p0, p2, p3, p1}, Lcom/polidea/reactnativeble/BleModule$37;-><init>(Lcom/polidea/reactnativeble/BleModule;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;Lcom/polidea/reactnativeble/wrapper/Descriptor;)V

    .line 1449
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/Observer;)Lrx/Subscription;

    move-result-object p1

    .line 1468
    iget-object p3, p0, Lcom/polidea/reactnativeble/BleModule;->transactions:Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-virtual {p3, p2, p1}, Lcom/polidea/reactnativeble/utils/DisposableMap;->replaceSubscription(Ljava/lang/String;Lrx/Subscription;)V

    return-void
.end method

.method private safeStartDeviceScan([Ljava/util/UUID;II)V
    .locals 4

    .line 401
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->rxBleClient:Lcom/polidea/rxandroidble/RxBleClient;

    if-eqz v0, :cond_2

    .line 405
    new-instance v0, Lcom/polidea/rxandroidble/scan/ScanSettings$Builder;

    invoke-direct {v0}, Lcom/polidea/rxandroidble/scan/ScanSettings$Builder;-><init>()V

    .line 406
    invoke-virtual {v0, p2}, Lcom/polidea/rxandroidble/scan/ScanSettings$Builder;->setScanMode(I)Lcom/polidea/rxandroidble/scan/ScanSettings$Builder;

    move-result-object p2

    .line 407
    invoke-virtual {p2, p3}, Lcom/polidea/rxandroidble/scan/ScanSettings$Builder;->setCallbackType(I)Lcom/polidea/rxandroidble/scan/ScanSettings$Builder;

    move-result-object p2

    .line 408
    invoke-virtual {p2}, Lcom/polidea/rxandroidble/scan/ScanSettings$Builder;->build()Lcom/polidea/rxandroidble/scan/ScanSettings;

    move-result-object p2

    const/4 p3, 0x0

    if-nez p1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 410
    :cond_0
    array-length v0, p1

    .line 411
    :goto_0
    new-array v1, v0, [Lcom/polidea/rxandroidble/scan/ScanFilter;

    :goto_1
    if-ge p3, v0, :cond_1

    .line 413
    new-instance v2, Lcom/polidea/rxandroidble/scan/ScanFilter$Builder;

    invoke-direct {v2}, Lcom/polidea/rxandroidble/scan/ScanFilter$Builder;-><init>()V

    aget-object v3, p1, p3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/polidea/rxandroidble/scan/ScanFilter$Builder;->setServiceUuid(Landroid/os/ParcelUuid;)Lcom/polidea/rxandroidble/scan/ScanFilter$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/polidea/rxandroidble/scan/ScanFilter$Builder;->build()Lcom/polidea/rxandroidble/scan/ScanFilter;

    move-result-object v2

    aput-object v2, v1, p3

    add-int/lit8 p3, p3, 0x1

    goto :goto_1

    .line 416
    :cond_1
    iget-object p1, p0, Lcom/polidea/reactnativeble/BleModule;->rxBleClient:Lcom/polidea/rxandroidble/RxBleClient;

    .line 417
    invoke-virtual {p1, p2, v1}, Lcom/polidea/rxandroidble/RxBleClient;->scanBleDevices(Lcom/polidea/rxandroidble/scan/ScanSettings;[Lcom/polidea/rxandroidble/scan/ScanFilter;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/polidea/reactnativeble/BleModule$11;

    invoke-direct {p2, p0}, Lcom/polidea/reactnativeble/BleModule$11;-><init>(Lcom/polidea/reactnativeble/BleModule;)V

    new-instance p3, Lcom/polidea/reactnativeble/BleModule$12;

    invoke-direct {p3, p0}, Lcom/polidea/reactnativeble/BleModule$12;-><init>(Lcom/polidea/reactnativeble/BleModule;)V

    .line 418
    invoke-virtual {p1, p2, p3}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule;->scanSubscription:Lrx/Subscription;

    return-void

    .line 402
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "BleManager not created when tried to start device scan"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private safeWriteCharacteristicForDevice(Lcom/polidea/reactnativeble/wrapper/Characteristic;[BLjava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V
    .locals 2

    .line 1137
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getService()Lcom/polidea/reactnativeble/wrapper/Service;

    move-result-object v0

    invoke-virtual {v0}, Lcom/polidea/reactnativeble/wrapper/Service;->getDevice()Lcom/polidea/reactnativeble/wrapper/Device;

    move-result-object v0

    invoke-direct {p0, v0, p4}, Lcom/polidea/reactnativeble/BleModule;->getConnectionOrReject(Lcom/polidea/reactnativeble/wrapper/Device;Lcom/polidea/reactnativeble/utils/SafePromise;)Lcom/polidea/rxandroidble/RxBleConnection;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 1142
    :cond_0
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getNativeCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/polidea/rxandroidble/RxBleConnection;->writeCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;[B)Lrx/Observable;

    move-result-object p2

    new-instance v0, Lcom/polidea/reactnativeble/BleModule$30;

    invoke-direct {v0, p0, p4, p3}, Lcom/polidea/reactnativeble/BleModule$30;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;)V

    .line 1143
    invoke-virtual {p2, v0}, Lrx/Observable;->doOnUnsubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p2

    new-instance v0, Lcom/polidea/reactnativeble/BleModule$29;

    invoke-direct {v0, p0, p3, p4, p1}, Lcom/polidea/reactnativeble/BleModule$29;-><init>(Lcom/polidea/reactnativeble/BleModule;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;Lcom/polidea/reactnativeble/wrapper/Characteristic;)V

    .line 1150
    invoke-virtual {p2, v0}, Lrx/Observable;->subscribe(Lrx/Observer;)Lrx/Subscription;

    move-result-object p1

    .line 1169
    iget-object p2, p0, Lcom/polidea/reactnativeble/BleModule;->transactions:Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-virtual {p2, p3, p1}, Lcom/polidea/reactnativeble/utils/DisposableMap;->replaceSubscription(Ljava/lang/String;Lrx/Subscription;)V

    return-void
.end method

.method private safeWriteDescriptorForDevice(Lcom/polidea/reactnativeble/wrapper/Descriptor;Ljava/lang/String;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V
    .locals 3

    .line 1556
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Descriptor;->getNativeDescriptor()Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v0

    .line 1558
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattDescriptor;->getUuid()Ljava/util/UUID;

    move-result-object v1

    sget-object v2, Lcom/polidea/reactnativeble/wrapper/Characteristic;->CLIENT_CHARACTERISTIC_CONFIG_UUID:Ljava/util/UUID;

    invoke-virtual {v1, v2}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1559
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattDescriptor;->getUuid()Ljava/util/UUID;

    move-result-object p1

    invoke-static {p1}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->fromUUID(Ljava/util/UUID;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->descriptorWriteNotAllowed(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void

    .line 1563
    :cond_0
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Descriptor;->getCharacteristic()Lcom/polidea/reactnativeble/wrapper/Characteristic;

    move-result-object v1

    invoke-virtual {v1}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getService()Lcom/polidea/reactnativeble/wrapper/Service;

    move-result-object v1

    invoke-virtual {v1}, Lcom/polidea/reactnativeble/wrapper/Service;->getDevice()Lcom/polidea/reactnativeble/wrapper/Device;

    move-result-object v1

    invoke-direct {p0, v1, p4}, Lcom/polidea/reactnativeble/BleModule;->getConnectionOrReject(Lcom/polidea/reactnativeble/wrapper/Device;Lcom/polidea/reactnativeble/utils/SafePromise;)Lcom/polidea/rxandroidble/RxBleConnection;

    move-result-object v1

    if-nez v1, :cond_1

    return-void

    .line 1570
    :cond_1
    :try_start_0
    invoke-static {p2}, Lcom/polidea/reactnativeble/utils/Base64Converter;->decode(Ljava/lang/String;)[B

    move-result-object p2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1578
    invoke-interface {v1, v0, p2}, Lcom/polidea/rxandroidble/RxBleConnection;->writeDescriptor(Landroid/bluetooth/BluetoothGattDescriptor;[B)Lrx/Observable;

    move-result-object p2

    new-instance v0, Lcom/polidea/reactnativeble/BleModule$40;

    invoke-direct {v0, p0, p4, p3}, Lcom/polidea/reactnativeble/BleModule$40;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;)V

    .line 1579
    invoke-virtual {p2, v0}, Lrx/Observable;->doOnUnsubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p2

    new-instance v0, Lcom/polidea/reactnativeble/BleModule$39;

    invoke-direct {v0, p0, p3, p4, p1}, Lcom/polidea/reactnativeble/BleModule$39;-><init>(Lcom/polidea/reactnativeble/BleModule;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;Lcom/polidea/reactnativeble/wrapper/Descriptor;)V

    .line 1586
    invoke-virtual {p2, v0}, Lrx/Observable;->subscribe(Lrx/Observer;)Lrx/Subscription;

    move-result-object p1

    .line 1605
    iget-object p2, p0, Lcom/polidea/reactnativeble/BleModule;->transactions:Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-virtual {p2, p3, p1}, Lcom/polidea/reactnativeble/utils/DisposableMap;->replaceSubscription(Ljava/lang/String;Lrx/Subscription;)V

    return-void

    .line 1572
    :catch_0
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattDescriptor;->getUuid()Ljava/util/UUID;

    move-result-object p1

    invoke-static {p1}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->fromUUID(Ljava/util/UUID;)Ljava/lang/String;

    move-result-object p1

    .line 1573
    invoke-static {p2, p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->invalidWriteDataForDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void
.end method

.method private sendEvent(Lcom/polidea/reactnativeble/Event;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Lcom/polidea/reactnativeble/Event;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1843
    invoke-virtual {p0}, Lcom/polidea/reactnativeble/BleModule;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    const-class v1, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    .line 1844
    invoke-virtual {v0, v1}, Lcom/facebook/react/bridge/ReactApplicationContext;->getJSModule(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    iget-object p1, p1, Lcom/polidea/reactnativeble/Event;->name:Ljava/lang/String;

    .line 1845
    invoke-interface {v0, p1, p2}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private supportsBluetoothLowEnergy()Z
    .locals 2

    .line 339
    invoke-virtual {p0}, Lcom/polidea/reactnativeble/BleModule;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/react/bridge/ReactApplicationContext;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.bluetooth_le"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private writeCharacteristicWithValue(Lcom/polidea/reactnativeble/wrapper/Characteristic;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 1

    .line 1113
    :try_start_0
    invoke-static {p2}, Lcom/polidea/reactnativeble/utils/Base64Converter;->decode(Ljava/lang/String;)[B

    move-result-object p2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1121
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getNativeCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v0

    .line 1122
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    if-eqz p3, :cond_0

    const/4 p3, 0x2

    goto :goto_0

    :cond_0
    const/4 p3, 0x1

    :goto_0
    invoke-virtual {v0, p3}, Landroid/bluetooth/BluetoothGattCharacteristic;->setWriteType(I)V

    .line 1126
    new-instance p3, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {p3, p5}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    invoke-direct {p0, p1, p2, p4, p3}, Lcom/polidea/reactnativeble/BleModule;->safeWriteCharacteristicForDevice(Lcom/polidea/reactnativeble/wrapper/Characteristic;[BLjava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void

    .line 1116
    :catch_0
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getNativeCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object p1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object p1

    invoke-static {p1}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->fromUUID(Ljava/util/UUID;)Ljava/lang/String;

    move-result-object p1

    .line 1115
    invoke-static {p2, p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->invalidWriteDataForCharacteristic(Ljava/lang/String;Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    .line 1117
    invoke-virtual {p1, p5}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-void
.end method


# virtual methods
.method public cancelDeviceConnection(Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 799
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->rxBleClient:Lcom/polidea/rxandroidble/RxBleClient;

    if-eqz v0, :cond_2

    .line 803
    invoke-virtual {v0, p1}, Lcom/polidea/rxandroidble/RxBleClient;->getBleDevice(Ljava/lang/String;)Lcom/polidea/rxandroidble/RxBleDevice;

    move-result-object v0

    .line 805
    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule;->connectingDevices:Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-virtual {v1, p1}, Lcom/polidea/reactnativeble/utils/DisposableMap;->removeSubscription(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 806
    new-instance p1, Lcom/polidea/reactnativeble/wrapper/Device;

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, Lcom/polidea/reactnativeble/wrapper/Device;-><init>(Lcom/polidea/rxandroidble/RxBleDevice;Lcom/polidea/rxandroidble/RxBleConnection;)V

    invoke-virtual {p1, v1}, Lcom/polidea/reactnativeble/wrapper/Device;->toJSObject(Ljava/lang/Integer;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    .line 809
    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->deviceNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    goto :goto_0

    .line 811
    :cond_1
    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->deviceNotConnected(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    :goto_0
    return-void

    .line 800
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "BleManager not created when tried cancel device connection"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public cancelTransaction(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 188
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->transactions:Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-virtual {v0, p1}, Lcom/polidea/reactnativeble/utils/DisposableMap;->removeSubscription(Ljava/lang/String;)Z

    return-void
.end method

.method public characteristicsForDevice(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 926
    invoke-static {p2}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->convert(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x1

    .line 928
    new-array p1, p1, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, p1, v0

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->invalidIdentifiers([Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-void

    .line 932
    :cond_0
    invoke-direct {p0, p1, p3}, Lcom/polidea/reactnativeble/BleModule;->getDeviceOrReject(Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Device;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    .line 937
    :cond_1
    invoke-virtual {p1, v0}, Lcom/polidea/reactnativeble/wrapper/Device;->getServiceByUUID(Ljava/util/UUID;)Lcom/polidea/reactnativeble/wrapper/Service;

    move-result-object p1

    if-nez p1, :cond_2

    .line 939
    invoke-static {p2}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->serviceNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-void

    .line 943
    :cond_2
    invoke-direct {p0, p1, p3}, Lcom/polidea/reactnativeble/BleModule;->characteristicsForService(Lcom/polidea/reactnativeble/wrapper/Service;Lcom/facebook/react/bridge/Promise;)V

    return-void
.end method

.method public characteristicsForService(ILcom/facebook/react/bridge/Promise;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 948
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredServices:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/polidea/reactnativeble/wrapper/Service;

    if-nez v0, :cond_0

    .line 950
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->serviceNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-void

    .line 954
    :cond_0
    invoke-direct {p0, v0, p2}, Lcom/polidea/reactnativeble/BleModule;->characteristicsForService(Lcom/polidea/reactnativeble/wrapper/Service;Lcom/facebook/react/bridge/Promise;)V

    return-void
.end method

.method public connectToDevice(Ljava/lang/String;Lcom/facebook/react/bridge/ReadableMap;Lcom/facebook/react/bridge/Promise;)V
    .locals 10
    .param p2    # Lcom/facebook/react/bridge/ReadableMap;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 637
    new-instance v0, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {v0, p3}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    .line 639
    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule;->rxBleClient:Lcom/polidea/rxandroidble/RxBleClient;

    if-eqz v1, :cond_7

    .line 643
    invoke-virtual {v1, p1}, Lcom/polidea/rxandroidble/RxBleClient;->getBleDevice(Ljava/lang/String;)Lcom/polidea/rxandroidble/RxBleDevice;

    move-result-object v3

    if-nez v3, :cond_0

    .line 645
    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->deviceNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void

    :cond_0
    const/4 p1, 0x0

    const/4 v0, 0x0

    if-eqz p2, :cond_6

    const-string v1, "autoConnect"

    .line 656
    invoke-interface {p2, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p2, v1}, Lcom/facebook/react/bridge/ReadableMap;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v2

    sget-object v4, Lcom/facebook/react/bridge/ReadableType;->Boolean:Lcom/facebook/react/bridge/ReadableType;

    if-ne v2, v4, :cond_1

    .line 657
    invoke-interface {p2, v1}, Lcom/facebook/react/bridge/ReadableMap;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    const-string v2, "requestMTU"

    .line 659
    invoke-interface {p2, v2}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {p2, v2}, Lcom/facebook/react/bridge/ReadableMap;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v4

    sget-object v5, Lcom/facebook/react/bridge/ReadableType;->Number:Lcom/facebook/react/bridge/ReadableType;

    if-ne v4, v5, :cond_2

    .line 660
    invoke-interface {p2, v2}, Lcom/facebook/react/bridge/ReadableMap;->getInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    const-string v4, "refreshGatt"

    .line 662
    invoke-interface {p2, v4}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {p2, v4}, Lcom/facebook/react/bridge/ReadableMap;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v5

    sget-object v6, Lcom/facebook/react/bridge/ReadableType;->String:Lcom/facebook/react/bridge/ReadableType;

    if-ne v5, v6, :cond_3

    .line 663
    invoke-interface {p2, v4}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/polidea/reactnativeble/RefreshGattMoment;->byJavaScriptName(Ljava/lang/String;)Lcom/polidea/reactnativeble/RefreshGattMoment;

    move-result-object v4

    goto :goto_2

    :cond_3
    move-object v4, p1

    :goto_2
    const-string v5, "timeout"

    .line 665
    invoke-interface {p2, v5}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {p2, v5}, Lcom/facebook/react/bridge/ReadableMap;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v6

    sget-object v7, Lcom/facebook/react/bridge/ReadableType;->Number:Lcom/facebook/react/bridge/ReadableType;

    if-ne v6, v7, :cond_4

    .line 666
    invoke-interface {p2, v5}, Lcom/facebook/react/bridge/ReadableMap;->getInt(Ljava/lang/String;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :cond_4
    const-string v5, "connectionPriority"

    .line 668
    invoke-interface {p2, v5}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {p2, v5}, Lcom/facebook/react/bridge/ReadableMap;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v6

    sget-object v7, Lcom/facebook/react/bridge/ReadableType;->Number:Lcom/facebook/react/bridge/ReadableType;

    if-ne v6, v7, :cond_5

    .line 669
    invoke-interface {p2, v5}, Lcom/facebook/react/bridge/ReadableMap;->getInt(Ljava/lang/String;)I

    move-result p2

    move-object v7, p1

    move v8, p2

    move v5, v2

    move-object v6, v4

    goto :goto_3

    :cond_5
    move-object v7, p1

    move v5, v2

    move-object v6, v4

    const/4 v8, 0x0

    :goto_3
    move v4, v1

    goto :goto_4

    :cond_6
    move-object v6, p1

    move-object v7, v6

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 673
    :goto_4
    new-instance v9, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {v9, p3}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    move-object v2, p0

    invoke-direct/range {v2 .. v9}, Lcom/polidea/reactnativeble/BleModule;->safeConnectToDevice(Lcom/polidea/rxandroidble/RxBleDevice;ZILcom/polidea/reactnativeble/RefreshGattMoment;Ljava/lang/Integer;ILcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void

    .line 640
    :cond_7
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "BleManager not created when tried connecting to device"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public connectedDevices(Lcom/facebook/react/bridge/ReadableArray;Lcom/facebook/react/bridge/Promise;)V
    .locals 7
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 473
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->rxBleClient:Lcom/polidea/rxandroidble/RxBleClient;

    if-eqz v0, :cond_5

    .line 477
    invoke-interface {p1}, Lcom/facebook/react/bridge/ReadableArray;->size()I

    move-result v0

    new-array v0, v0, [Ljava/util/UUID;

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 478
    :goto_0
    invoke-interface {p1}, Lcom/facebook/react/bridge/ReadableArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 479
    invoke-interface {p1, v2}, Lcom/facebook/react/bridge/ReadableArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->convert(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v3

    if-nez v3, :cond_0

    .line 482
    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->invalidIdentifiers(Lcom/facebook/react/bridge/ReadableArray;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-void

    .line 486
    :cond_0
    aput-object v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 489
    :cond_1
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createArray()Lcom/facebook/react/bridge/WritableArray;

    move-result-object p1

    .line 491
    iget-object v2, p0, Lcom/polidea/reactnativeble/BleModule;->connectedDevices:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/polidea/reactnativeble/wrapper/Device;

    .line 492
    array-length v4, v0

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v4, :cond_2

    aget-object v6, v0, v5

    .line 493
    invoke-virtual {v3, v6}, Lcom/polidea/reactnativeble/wrapper/Device;->getServiceByUUID(Ljava/util/UUID;)Lcom/polidea/reactnativeble/wrapper/Service;

    move-result-object v6

    if-eqz v6, :cond_3

    const/4 v4, 0x0

    .line 494
    invoke-virtual {v3, v4}, Lcom/polidea/reactnativeble/wrapper/Device;->toJSObject(Ljava/lang/Integer;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/facebook/react/bridge/WritableArray;->pushMap(Lcom/facebook/react/bridge/WritableMap;)V

    goto :goto_1

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 500
    :cond_4
    invoke-interface {p2, p1}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    return-void

    .line 474
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "BleManager not created when tried connecting to device"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public createClient(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 142
    invoke-virtual {p0}, Lcom/polidea/reactnativeble/BleModule;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    .line 143
    invoke-static {v0}, Lcom/polidea/rxandroidble/RxBleClient;->create(Landroid/content/Context;)Lcom/polidea/rxandroidble/RxBleClient;

    move-result-object v1

    iput-object v1, p0, Lcom/polidea/reactnativeble/BleModule;->rxBleClient:Lcom/polidea/rxandroidble/RxBleClient;

    .line 144
    invoke-direct {p0, v0}, Lcom/polidea/reactnativeble/BleModule;->monitorAdapterStateChanges(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v0

    iput-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->adapterStateChangesSubscription:Lrx/Subscription;

    if-eqz p1, :cond_0

    .line 148
    sget-object p1, Lcom/polidea/reactnativeble/Event;->RestoreStateEvent:Lcom/polidea/reactnativeble/Event;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/polidea/reactnativeble/BleModule;->sendEvent(Lcom/polidea/reactnativeble/Event;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public descriptorsForCharacteristic(ILcom/facebook/react/bridge/Promise;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 1024
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredCharacteristics:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/polidea/reactnativeble/wrapper/Characteristic;

    if-nez v0, :cond_0

    .line 1026
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->characteristicNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    return-void

    .line 1030
    :cond_0
    invoke-direct {p0, v0, p2}, Lcom/polidea/reactnativeble/BleModule;->descriptorForCharacteristic(Lcom/polidea/reactnativeble/wrapper/Characteristic;Lcom/facebook/react/bridge/Promise;)V

    return-void
.end method

.method public descriptorsForDevice(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    const/4 v0, 0x2

    .line 970
    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v3, 0x1

    aput-object p3, v1, v3

    invoke-static {v1}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->convert([Ljava/lang/String;)[Ljava/util/UUID;

    move-result-object v1

    if-nez v1, :cond_0

    .line 972
    new-array p1, v0, [Ljava/lang/String;

    aput-object p2, p1, v2

    aput-object p3, p1, v3

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->invalidIdentifiers([Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-void

    .line 976
    :cond_0
    invoke-direct {p0, p1, p4}, Lcom/polidea/reactnativeble/BleModule;->getDeviceOrReject(Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Device;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    .line 981
    :cond_1
    aget-object v0, v1, v2

    invoke-virtual {p1, v0}, Lcom/polidea/reactnativeble/wrapper/Device;->getServiceByUUID(Ljava/util/UUID;)Lcom/polidea/reactnativeble/wrapper/Service;

    move-result-object p1

    if-nez p1, :cond_2

    .line 983
    invoke-static {p2}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->serviceNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-void

    .line 987
    :cond_2
    aget-object p2, v1, v3

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/wrapper/Service;->getCharacteristicByUUID(Ljava/util/UUID;)Lcom/polidea/reactnativeble/wrapper/Characteristic;

    move-result-object p1

    if-nez p1, :cond_3

    .line 989
    invoke-static {p3}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->characteristicNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    return-void

    .line 993
    :cond_3
    invoke-direct {p0, p1, p4}, Lcom/polidea/reactnativeble/BleModule;->descriptorForCharacteristic(Lcom/polidea/reactnativeble/wrapper/Characteristic;Lcom/facebook/react/bridge/Promise;)V

    return-void
.end method

.method public descriptorsForService(ILjava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 1000
    invoke-static {p2}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->convert(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x1

    .line 1002
    new-array p1, p1, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, p1, v0

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->invalidIdentifiers([Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-void

    .line 1006
    :cond_0
    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredServices:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/polidea/reactnativeble/wrapper/Service;

    if-nez v1, :cond_1

    .line 1008
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->serviceNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-void

    .line 1012
    :cond_1
    invoke-virtual {v1, v0}, Lcom/polidea/reactnativeble/wrapper/Service;->getCharacteristicByUUID(Ljava/util/UUID;)Lcom/polidea/reactnativeble/wrapper/Characteristic;

    move-result-object p1

    if-nez p1, :cond_2

    .line 1014
    invoke-static {p2}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->characteristicNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    return-void

    .line 1018
    :cond_2
    invoke-direct {p0, p1, p3}, Lcom/polidea/reactnativeble/BleModule;->descriptorForCharacteristic(Lcom/polidea/reactnativeble/wrapper/Characteristic;Lcom/facebook/react/bridge/Promise;)V

    return-void
.end method

.method public destroyClient()V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 155
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->adapterStateChangesSubscription:Lrx/Subscription;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 156
    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    .line 157
    iput-object v1, p0, Lcom/polidea/reactnativeble/BleModule;->adapterStateChangesSubscription:Lrx/Subscription;

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->scanSubscription:Lrx/Subscription;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lrx/Subscription;->isUnsubscribed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->scanSubscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    .line 161
    iput-object v1, p0, Lcom/polidea/reactnativeble/BleModule;->scanSubscription:Lrx/Subscription;

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->transactions:Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-virtual {v0}, Lcom/polidea/reactnativeble/utils/DisposableMap;->removeAllSubscriptions()V

    .line 164
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->connectingDevices:Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-virtual {v0}, Lcom/polidea/reactnativeble/utils/DisposableMap;->removeAllSubscriptions()V

    .line 167
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredServices:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 168
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredCharacteristics:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 169
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredDescriptors:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 170
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->connectedDevices:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 171
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredDevices:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 174
    iput-object v1, p0, Lcom/polidea/reactnativeble/BleModule;->rxBleClient:Lcom/polidea/rxandroidble/RxBleClient;

    .line 175
    invoke-static {}, Lcom/polidea/reactnativeble/utils/IdGenerator;->clear()V

    return-void
.end method

.method public devices(Lcom/facebook/react/bridge/ReadableArray;Lcom/facebook/react/bridge/Promise;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 449
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->rxBleClient:Lcom/polidea/rxandroidble/RxBleClient;

    if-eqz v0, :cond_3

    .line 453
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createArray()Lcom/facebook/react/bridge/WritableArray;

    move-result-object v0

    const/4 v1, 0x0

    .line 454
    :goto_0
    invoke-interface {p1}, Lcom/facebook/react/bridge/ReadableArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 455
    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/ReadableArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 458
    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->invalidIdentifiers(Lcom/facebook/react/bridge/ReadableArray;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-void

    .line 462
    :cond_0
    iget-object v3, p0, Lcom/polidea/reactnativeble/BleModule;->discoveredDevices:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/polidea/reactnativeble/wrapper/Device;

    if-eqz v2, :cond_1

    const/4 v3, 0x0

    .line 464
    invoke-virtual {v2, v3}, Lcom/polidea/reactnativeble/wrapper/Device;->toJSObject(Ljava/lang/Integer;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/facebook/react/bridge/WritableArray;->pushMap(Lcom/facebook/react/bridge/WritableMap;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 468
    :cond_2
    invoke-interface {p2, v0}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    return-void

    .line 450
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "BleManager not created when tried connecting to device"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public disable(Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 5
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 253
    invoke-virtual {p0}, Lcom/polidea/reactnativeble/BleModule;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    const-string v1, "bluetooth"

    .line 254
    invoke-virtual {v0, v1}, Lcom/facebook/react/bridge/ReactApplicationContext;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothManager;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 256
    new-instance p1, Lcom/polidea/reactnativeble/errors/BleError;

    sget-object v0, Lcom/polidea/reactnativeble/errors/BleErrorCode;->BluetoothStateChangeFailed:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    const-string v1, "BluetoothManager is null"

    invoke-direct {p1, v0, v1, v2}, Lcom/polidea/reactnativeble/errors/BleError;-><init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-void

    .line 259
    :cond_0
    new-instance v3, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {v3, p2}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    .line 260
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object p2

    .line 261
    new-instance v1, Lcom/polidea/rxandroidble/RxBleAdapterStateObservable;

    invoke-direct {v1, v0}, Lcom/polidea/rxandroidble/RxBleAdapterStateObservable;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/polidea/reactnativeble/BleModule$8;

    invoke-direct {v0, p0}, Lcom/polidea/reactnativeble/BleModule$8;-><init>(Lcom/polidea/reactnativeble/BleModule;)V

    .line 262
    invoke-virtual {v1, v0}, Lcom/polidea/rxandroidble/RxBleAdapterStateObservable;->takeUntil(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 268
    invoke-virtual {v0}, Lrx/Observable;->toCompletable()Lrx/Completable;

    move-result-object v0

    new-instance v1, Lcom/polidea/reactnativeble/BleModule$7;

    invoke-direct {v1, p0, v3, p1}, Lcom/polidea/reactnativeble/BleModule$7;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;)V

    .line 269
    invoke-virtual {v0, v1}, Lrx/Completable;->doOnUnsubscribe(Lrx/functions/Action0;)Lrx/Completable;

    move-result-object v0

    new-instance v1, Lcom/polidea/reactnativeble/BleModule$5;

    invoke-direct {v1, p0, v3, p1}, Lcom/polidea/reactnativeble/BleModule$5;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;)V

    new-instance v4, Lcom/polidea/reactnativeble/BleModule$6;

    invoke-direct {v4, p0, v3, p1}, Lcom/polidea/reactnativeble/BleModule$6;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;)V

    .line 276
    invoke-virtual {v0, v1, v4}, Lrx/Completable;->subscribe(Lrx/functions/Action0;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 290
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    move-result p2

    if-nez p2, :cond_1

    .line 291
    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    .line 292
    new-instance p1, Lcom/polidea/reactnativeble/errors/BleError;

    sget-object p2, Lcom/polidea/reactnativeble/errors/BleErrorCode;->BluetoothStateChangeFailed:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    const-string v0, "Couldn\'t enable bluetooth adapter"

    invoke-direct {p1, p2, v0, v2}, Lcom/polidea/reactnativeble/errors/BleError;-><init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p1, v3}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/polidea/reactnativeble/utils/SafePromise;)V

    goto :goto_0

    .line 294
    :cond_1
    iget-object p2, p0, Lcom/polidea/reactnativeble/BleModule;->transactions:Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-virtual {p2, p1, v0}, Lcom/polidea/reactnativeble/utils/DisposableMap;->replaceSubscription(Ljava/lang/String;Lrx/Subscription;)V

    :goto_0
    return-void
.end method

.method public discoverAllServicesAndCharacteristicsForDevice(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 837
    invoke-direct {p0, p1, p3}, Lcom/polidea/reactnativeble/BleModule;->getDeviceOrReject(Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Device;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 842
    :cond_0
    new-instance v0, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {v0, p3}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/polidea/reactnativeble/BleModule;->safeDiscoverAllServicesAndCharacteristicsForDevice(Lcom/polidea/reactnativeble/wrapper/Device;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void
.end method

.method public enable(Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 5
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 206
    invoke-virtual {p0}, Lcom/polidea/reactnativeble/BleModule;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    const-string v1, "bluetooth"

    .line 207
    invoke-virtual {v0, v1}, Lcom/facebook/react/bridge/ReactApplicationContext;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothManager;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 209
    new-instance p1, Lcom/polidea/reactnativeble/errors/BleError;

    sget-object v0, Lcom/polidea/reactnativeble/errors/BleErrorCode;->BluetoothStateChangeFailed:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    const-string v1, "BluetoothManager is null"

    invoke-direct {p1, v0, v1, v2}, Lcom/polidea/reactnativeble/errors/BleError;-><init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-void

    .line 212
    :cond_0
    new-instance v3, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {v3, p2}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    .line 213
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object p2

    .line 214
    new-instance v1, Lcom/polidea/rxandroidble/RxBleAdapterStateObservable;

    invoke-direct {v1, v0}, Lcom/polidea/rxandroidble/RxBleAdapterStateObservable;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/polidea/reactnativeble/BleModule$4;

    invoke-direct {v0, p0}, Lcom/polidea/reactnativeble/BleModule$4;-><init>(Lcom/polidea/reactnativeble/BleModule;)V

    .line 215
    invoke-virtual {v1, v0}, Lcom/polidea/rxandroidble/RxBleAdapterStateObservable;->takeUntil(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 221
    invoke-virtual {v0}, Lrx/Observable;->toCompletable()Lrx/Completable;

    move-result-object v0

    new-instance v1, Lcom/polidea/reactnativeble/BleModule$3;

    invoke-direct {v1, p0, v3, p1}, Lcom/polidea/reactnativeble/BleModule$3;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;)V

    .line 222
    invoke-virtual {v0, v1}, Lrx/Completable;->doOnUnsubscribe(Lrx/functions/Action0;)Lrx/Completable;

    move-result-object v0

    new-instance v1, Lcom/polidea/reactnativeble/BleModule$1;

    invoke-direct {v1, p0, v3, p1}, Lcom/polidea/reactnativeble/BleModule$1;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;)V

    new-instance v4, Lcom/polidea/reactnativeble/BleModule$2;

    invoke-direct {v4, p0, v3, p1}, Lcom/polidea/reactnativeble/BleModule$2;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;)V

    .line 229
    invoke-virtual {v0, v1, v4}, Lrx/Completable;->subscribe(Lrx/functions/Action0;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 243
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    move-result p2

    if-nez p2, :cond_1

    .line 244
    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    .line 245
    new-instance p1, Lcom/polidea/reactnativeble/errors/BleError;

    sget-object p2, Lcom/polidea/reactnativeble/errors/BleErrorCode;->BluetoothStateChangeFailed:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    const-string v0, "Couldn\'t enable bluetooth adapter"

    invoke-direct {p1, p2, v0, v2}, Lcom/polidea/reactnativeble/errors/BleError;-><init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p1, v3}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/polidea/reactnativeble/utils/SafePromise;)V

    goto :goto_0

    .line 247
    :cond_1
    iget-object p2, p0, Lcom/polidea/reactnativeble/BleModule;->transactions:Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-virtual {p2, p1, v0}, Lcom/polidea/reactnativeble/utils/DisposableMap;->replaceSubscription(Ljava/lang/String;Lrx/Subscription;)V

    :goto_0
    return-void
.end method

.method public getConstants()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 131
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 132
    invoke-static {}, Lcom/polidea/reactnativeble/Event;->values()[Lcom/polidea/reactnativeble/Event;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v1, v3

    .line 133
    iget-object v5, v4, Lcom/polidea/reactnativeble/Event;->name:Ljava/lang/String;

    iget-object v4, v4, Lcom/polidea/reactnativeble/Event;->name:Ljava/lang/String;

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "BleClientManager"

    return-object v0
.end method

.method public isDeviceConnected(Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 818
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->rxBleClient:Lcom/polidea/rxandroidble/RxBleClient;

    if-eqz v0, :cond_1

    .line 822
    invoke-virtual {v0, p1}, Lcom/polidea/rxandroidble/RxBleClient;->getBleDevice(Ljava/lang/String;)Lcom/polidea/rxandroidble/RxBleDevice;

    move-result-object v0

    if-nez v0, :cond_0

    .line 824
    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->deviceNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/facebook/react/bridge/Promise;)V

    return-void

    .line 828
    :cond_0
    invoke-interface {v0}, Lcom/polidea/rxandroidble/RxBleDevice;->getConnectionState()Lcom/polidea/rxandroidble/RxBleConnection$RxBleConnectionState;

    move-result-object p1

    sget-object v0, Lcom/polidea/rxandroidble/RxBleConnection$RxBleConnectionState;->CONNECTED:Lcom/polidea/rxandroidble/RxBleConnection$RxBleConnectionState;

    .line 829
    invoke-virtual {p1, v0}, Lcom/polidea/rxandroidble/RxBleConnection$RxBleConnectionState;->equals(Ljava/lang/Object;)Z

    move-result p1

    .line 830
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    return-void

    .line 819
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "BleManager not created when tried cancel device connection"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public logLevel(Lcom/facebook/react/bridge/Promise;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 199
    iget v0, p0, Lcom/polidea/reactnativeble/BleModule;->currentLogLevel:I

    invoke-static {v0}, Lcom/polidea/reactnativeble/utils/LogLevel;->fromLogLevel(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public monitorCharacteristic(ILjava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 1292
    invoke-direct {p0, p1, p3}, Lcom/polidea/reactnativeble/BleModule;->getCharacteristicOrReject(ILcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Characteristic;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 1297
    :cond_0
    new-instance v0, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {v0, p3}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/polidea/reactnativeble/BleModule;->safeMonitorCharacteristicForDevice(Lcom/polidea/reactnativeble/wrapper/Characteristic;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void
.end method

.method public monitorCharacteristicForDevice(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 1263
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/polidea/reactnativeble/BleModule;->getCharacteristicOrReject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Characteristic;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 1269
    :cond_0
    new-instance p2, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {p2, p5}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    invoke-direct {p0, p1, p4, p2}, Lcom/polidea/reactnativeble/BleModule;->safeMonitorCharacteristicForDevice(Lcom/polidea/reactnativeble/wrapper/Characteristic;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void
.end method

.method public monitorCharacteristicForService(ILjava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 1278
    invoke-direct {p0, p1, p2, p4}, Lcom/polidea/reactnativeble/BleModule;->getCharacteristicOrReject(ILjava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Characteristic;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 1284
    :cond_0
    new-instance p2, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {p2, p4}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    invoke-direct {p0, p1, p3, p2}, Lcom/polidea/reactnativeble/BleModule;->safeMonitorCharacteristicForDevice(Lcom/polidea/reactnativeble/wrapper/Characteristic;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void
.end method

.method public onCatalystInstanceDestroy()V
    .locals 0

    .line 180
    invoke-super {p0}, Lcom/facebook/react/bridge/ReactContextBaseJavaModule;->onCatalystInstanceDestroy()V

    .line 181
    invoke-virtual {p0}, Lcom/polidea/reactnativeble/BleModule;->destroyClient()V

    return-void
.end method

.method public readCharacteristic(ILjava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 1208
    invoke-direct {p0, p1, p3}, Lcom/polidea/reactnativeble/BleModule;->getCharacteristicOrReject(ILcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Characteristic;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 1213
    :cond_0
    new-instance v0, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {v0, p3}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/polidea/reactnativeble/BleModule;->safeReadCharacteristicForDevice(Lcom/polidea/reactnativeble/wrapper/Characteristic;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void
.end method

.method public readCharacteristicForDevice(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 1179
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/polidea/reactnativeble/BleModule;->getCharacteristicOrReject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Characteristic;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 1185
    :cond_0
    new-instance p2, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {p2, p5}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    invoke-direct {p0, p1, p4, p2}, Lcom/polidea/reactnativeble/BleModule;->safeReadCharacteristicForDevice(Lcom/polidea/reactnativeble/wrapper/Characteristic;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void
.end method

.method public readCharacteristicForService(ILjava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 1194
    invoke-direct {p0, p1, p2, p4}, Lcom/polidea/reactnativeble/BleModule;->getCharacteristicOrReject(ILjava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Characteristic;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 1200
    :cond_0
    new-instance p2, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {p2, p4}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    invoke-direct {p0, p1, p3, p2}, Lcom/polidea/reactnativeble/BleModule;->safeReadCharacteristicForDevice(Lcom/polidea/reactnativeble/wrapper/Characteristic;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void
.end method

.method public readDescriptor(ILjava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 1424
    invoke-direct {p0, p1, p3}, Lcom/polidea/reactnativeble/BleModule;->getDescriptorOrReject(ILcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Descriptor;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 1429
    :cond_0
    new-instance v0, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {v0, p3}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/polidea/reactnativeble/BleModule;->safeReadDescriptorForDevice(Lcom/polidea/reactnativeble/wrapper/Descriptor;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void
.end method

.method public readDescriptorForCharacteristic(ILjava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 1410
    invoke-direct {p0, p1, p2, p4}, Lcom/polidea/reactnativeble/BleModule;->getDescriptorOrReject(ILjava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Descriptor;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 1416
    :cond_0
    new-instance p2, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {p2, p4}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    invoke-direct {p0, p1, p3, p2}, Lcom/polidea/reactnativeble/BleModule;->safeReadDescriptorForDevice(Lcom/polidea/reactnativeble/wrapper/Descriptor;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void
.end method

.method public readDescriptorForDevice(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    .line 1379
    invoke-direct/range {v0 .. v5}, Lcom/polidea/reactnativeble/BleModule;->getDescriptorOrReject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Descriptor;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 1385
    :cond_0
    new-instance p2, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {p2, p6}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    invoke-direct {p0, p1, p5, p2}, Lcom/polidea/reactnativeble/BleModule;->safeReadDescriptorForDevice(Lcom/polidea/reactnativeble/wrapper/Descriptor;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void
.end method

.method public readDescriptorForService(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 1395
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/polidea/reactnativeble/BleModule;->getDescriptorOrReject(ILjava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Descriptor;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 1401
    :cond_0
    new-instance p2, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {p2, p5}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    invoke-direct {p0, p1, p4, p2}, Lcom/polidea/reactnativeble/BleModule;->safeReadDescriptorForDevice(Lcom/polidea/reactnativeble/wrapper/Descriptor;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void
.end method

.method public readRSSIForDevice(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 595
    invoke-direct {p0, p1, p3}, Lcom/polidea/reactnativeble/BleModule;->getDeviceOrReject(Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Device;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 599
    :cond_0
    invoke-direct {p0, p1, p3}, Lcom/polidea/reactnativeble/BleModule;->getConnectionOrReject(Lcom/polidea/reactnativeble/wrapper/Device;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/rxandroidble/RxBleConnection;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 604
    :cond_1
    new-instance v1, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {v1, p3}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    .line 606
    invoke-interface {v0}, Lcom/polidea/rxandroidble/RxBleConnection;->readRssi()Lrx/Observable;

    move-result-object p3

    new-instance v0, Lcom/polidea/reactnativeble/BleModule$19;

    invoke-direct {v0, p0, v1, p2}, Lcom/polidea/reactnativeble/BleModule$19;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;)V

    .line 607
    invoke-virtual {p3, v0}, Lrx/Observable;->doOnUnsubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p3

    new-instance v0, Lcom/polidea/reactnativeble/BleModule$18;

    invoke-direct {v0, p0, p2, v1, p1}, Lcom/polidea/reactnativeble/BleModule$18;-><init>(Lcom/polidea/reactnativeble/BleModule;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;Lcom/polidea/reactnativeble/wrapper/Device;)V

    .line 614
    invoke-virtual {p3, v0}, Lrx/Observable;->subscribe(Lrx/Observer;)Lrx/Subscription;

    move-result-object p1

    .line 632
    iget-object p3, p0, Lcom/polidea/reactnativeble/BleModule;->transactions:Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-virtual {p3, p2, p1}, Lcom/polidea/reactnativeble/utils/DisposableMap;->replaceSubscription(Ljava/lang/String;Lrx/Subscription;)V

    return-void
.end method

.method public requestConnectionPriorityForDevice(Ljava/lang/String;ILjava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 507
    invoke-direct {p0, p1, p4}, Lcom/polidea/reactnativeble/BleModule;->getDeviceOrReject(Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Device;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 512
    :cond_0
    invoke-direct {p0, p1, p4}, Lcom/polidea/reactnativeble/BleModule;->getConnectionOrReject(Lcom/polidea/reactnativeble/wrapper/Device;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/rxandroidble/RxBleConnection;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 517
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_2

    .line 518
    new-instance v1, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {v1, p4}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    const-wide/16 v2, 0x1

    .line 519
    sget-object p4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 520
    invoke-interface {v0, p2, v2, v3, p4}, Lcom/polidea/rxandroidble/RxBleConnection;->requestConnectionPriority(IJLjava/util/concurrent/TimeUnit;)Lrx/Completable;

    move-result-object p2

    new-instance p4, Lcom/polidea/reactnativeble/BleModule$15;

    invoke-direct {p4, p0, v1, p3}, Lcom/polidea/reactnativeble/BleModule$15;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;)V

    .line 521
    invoke-virtual {p2, p4}, Lrx/Completable;->doOnUnsubscribe(Lrx/functions/Action0;)Lrx/Completable;

    move-result-object p2

    new-instance p4, Lcom/polidea/reactnativeble/BleModule$13;

    invoke-direct {p4, p0, v1, p1, p3}, Lcom/polidea/reactnativeble/BleModule$13;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Lcom/polidea/reactnativeble/wrapper/Device;Ljava/lang/String;)V

    new-instance p1, Lcom/polidea/reactnativeble/BleModule$14;

    invoke-direct {p1, p0, v1, p3}, Lcom/polidea/reactnativeble/BleModule$14;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;)V

    .line 527
    invoke-virtual {p2, p4, p1}, Lrx/Completable;->subscribe(Lrx/functions/Action0;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 541
    iget-object p2, p0, Lcom/polidea/reactnativeble/BleModule;->transactions:Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-virtual {p2, p3, p1}, Lcom/polidea/reactnativeble/utils/DisposableMap;->replaceSubscription(Ljava/lang/String;Lrx/Subscription;)V

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    .line 543
    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/wrapper/Device;->toJSObject(Ljava/lang/Integer;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object p1

    invoke-interface {p4, p1}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public requestMTUForDevice(Ljava/lang/String;ILjava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 549
    invoke-direct {p0, p1, p4}, Lcom/polidea/reactnativeble/BleModule;->getDeviceOrReject(Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Device;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 554
    :cond_0
    invoke-direct {p0, p1, p4}, Lcom/polidea/reactnativeble/BleModule;->getConnectionOrReject(Lcom/polidea/reactnativeble/wrapper/Device;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/rxandroidble/RxBleConnection;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 559
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_2

    .line 560
    new-instance v1, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {v1, p4}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    .line 562
    invoke-interface {v0, p2}, Lcom/polidea/rxandroidble/RxBleConnection;->requestMtu(I)Lrx/Observable;

    move-result-object p2

    new-instance p4, Lcom/polidea/reactnativeble/BleModule$17;

    invoke-direct {p4, p0, v1, p3}, Lcom/polidea/reactnativeble/BleModule$17;-><init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Ljava/lang/String;)V

    .line 563
    invoke-virtual {p2, p4}, Lrx/Observable;->doOnUnsubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p2

    new-instance p4, Lcom/polidea/reactnativeble/BleModule$16;

    invoke-direct {p4, p0, p3, v1, p1}, Lcom/polidea/reactnativeble/BleModule$16;-><init>(Lcom/polidea/reactnativeble/BleModule;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;Lcom/polidea/reactnativeble/wrapper/Device;)V

    .line 569
    invoke-virtual {p2, p4}, Lrx/Observable;->subscribe(Lrx/Observer;)Lrx/Subscription;

    move-result-object p1

    .line 587
    iget-object p2, p0, Lcom/polidea/reactnativeble/BleModule;->transactions:Lcom/polidea/reactnativeble/utils/DisposableMap;

    invoke-virtual {p2, p3, p1}, Lcom/polidea/reactnativeble/utils/DisposableMap;->replaceSubscription(Ljava/lang/String;Lrx/Subscription;)V

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    .line 589
    invoke-virtual {p1, p2}, Lcom/polidea/reactnativeble/wrapper/Device;->toJSObject(Ljava/lang/Integer;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object p1

    invoke-interface {p4, p1}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public servicesForDevice(Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 904
    invoke-direct {p0, p1, p2}, Lcom/polidea/reactnativeble/BleModule;->getDeviceOrReject(Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Device;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 908
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/polidea/reactnativeble/BleModule;->getServicesOrReject(Lcom/polidea/reactnativeble/wrapper/Device;Lcom/facebook/react/bridge/Promise;)Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    .line 913
    :cond_1
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createArray()Lcom/facebook/react/bridge/WritableArray;

    move-result-object v0

    .line 914
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/polidea/reactnativeble/wrapper/Service;

    .line 915
    invoke-virtual {v1}, Lcom/polidea/reactnativeble/wrapper/Service;->toJSObject()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/WritableArray;->pushMap(Lcom/facebook/react/bridge/WritableMap;)V

    goto :goto_0

    .line 918
    :cond_2
    invoke-interface {p2, v0}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public setLogLevel(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 193
    invoke-static {p1}, Lcom/polidea/reactnativeble/utils/LogLevel;->toLogLevel(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/polidea/reactnativeble/BleModule;->currentLogLevel:I

    .line 194
    iget p1, p0, Lcom/polidea/reactnativeble/BleModule;->currentLogLevel:I

    invoke-static {p1}, Lcom/polidea/rxandroidble/RxBleClient;->setLogLevel(I)V

    return-void
.end method

.method public startDeviceScan(Lcom/facebook/react/bridge/ReadableArray;Lcom/facebook/react/bridge/ReadableMap;)V
    .locals 5
    .param p1    # Lcom/facebook/react/bridge/ReadableArray;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/react/bridge/ReadableMap;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p2, :cond_1

    const-string v2, "scanMode"

    .line 380
    invoke-interface {p2, v2}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p2, v2}, Lcom/facebook/react/bridge/ReadableMap;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v3

    sget-object v4, Lcom/facebook/react/bridge/ReadableType;->Number:Lcom/facebook/react/bridge/ReadableType;

    if-ne v3, v4, :cond_0

    .line 381
    invoke-interface {p2, v2}, Lcom/facebook/react/bridge/ReadableMap;->getInt(Ljava/lang/String;)I

    move-result v0

    :cond_0
    const-string v2, "callbackType"

    .line 383
    invoke-interface {p2, v2}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p2, v2}, Lcom/facebook/react/bridge/ReadableMap;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v3

    sget-object v4, Lcom/facebook/react/bridge/ReadableType;->Number:Lcom/facebook/react/bridge/ReadableType;

    if-ne v3, v4, :cond_1

    .line 384
    invoke-interface {p2, v2}, Lcom/facebook/react/bridge/ReadableMap;->getInt(Ljava/lang/String;)I

    move-result v1

    :cond_1
    if-eqz p1, :cond_2

    .line 389
    invoke-static {p1}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->convert(Lcom/facebook/react/bridge/ReadableArray;)[Ljava/util/UUID;

    move-result-object p2

    if-nez p2, :cond_3

    .line 391
    sget-object p2, Lcom/polidea/reactnativeble/Event;->ScanEvent:Lcom/polidea/reactnativeble/Event;

    .line 392
    invoke-static {p1}, Lcom/polidea/reactnativeble/utils/ReadableArrayConverter;->toStringArray(Lcom/facebook/react/bridge/ReadableArray;)[Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->invalidIdentifiers([Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1}, Lcom/polidea/reactnativeble/errors/BleError;->toJSCallback()Lcom/facebook/react/bridge/ReadableArray;

    move-result-object p1

    .line 391
    invoke-direct {p0, p2, p1}, Lcom/polidea/reactnativeble/BleModule;->sendEvent(Lcom/polidea/reactnativeble/Event;Ljava/lang/Object;)V

    return-void

    :cond_2
    const/4 p2, 0x0

    .line 397
    :cond_3
    invoke-direct {p0, p2, v0, v1}, Lcom/polidea/reactnativeble/BleModule;->safeStartDeviceScan([Ljava/util/UUID;II)V

    return-void
.end method

.method public state(Lcom/facebook/react/bridge/Promise;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 300
    invoke-direct {p0}, Lcom/polidea/reactnativeble/BleModule;->getCurrentState()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public stopDeviceScan()V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 437
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->scanSubscription:Lrx/Subscription;

    if-eqz v0, :cond_1

    .line 438
    invoke-interface {v0}, Lrx/Subscription;->isUnsubscribed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->scanSubscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    const/4 v0, 0x0

    .line 441
    iput-object v0, p0, Lcom/polidea/reactnativeble/BleModule;->scanSubscription:Lrx/Subscription;

    :cond_1
    return-void
.end method

.method public writeCharacteristic(ILjava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 1093
    invoke-direct {p0, p1, p5}, Lcom/polidea/reactnativeble/BleModule;->getCharacteristicOrReject(ILcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Characteristic;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 1098
    invoke-direct/range {v0 .. v5}, Lcom/polidea/reactnativeble/BleModule;->writeCharacteristicWithValue(Lcom/polidea/reactnativeble/wrapper/Characteristic;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V

    return-void
.end method

.method public writeCharacteristicForDevice(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 1052
    invoke-direct {p0, p1, p2, p3, p7}, Lcom/polidea/reactnativeble/BleModule;->getCharacteristicOrReject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Characteristic;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    move-object v0, p0

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    .line 1058
    invoke-direct/range {v0 .. v5}, Lcom/polidea/reactnativeble/BleModule;->writeCharacteristicWithValue(Lcom/polidea/reactnativeble/wrapper/Characteristic;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V

    return-void
.end method

.method public writeCharacteristicForService(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 1073
    invoke-direct {p0, p1, p2, p6}, Lcom/polidea/reactnativeble/BleModule;->getCharacteristicOrReject(ILjava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Characteristic;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    move-object v0, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    .line 1079
    invoke-direct/range {v0 .. v5}, Lcom/polidea/reactnativeble/BleModule;->writeCharacteristicWithValue(Lcom/polidea/reactnativeble/wrapper/Characteristic;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V

    return-void
.end method

.method public writeDescriptor(ILjava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 1540
    invoke-direct {p0, p1, p4}, Lcom/polidea/reactnativeble/BleModule;->getDescriptorOrReject(ILcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Descriptor;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 1545
    :cond_0
    new-instance v0, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {v0, p4}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/polidea/reactnativeble/BleModule;->safeWriteDescriptorForDevice(Lcom/polidea/reactnativeble/wrapper/Descriptor;Ljava/lang/String;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void
.end method

.method public writeDescriptorForCharacteristic(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 1522
    invoke-direct {p0, p1, p2, p5}, Lcom/polidea/reactnativeble/BleModule;->getDescriptorOrReject(ILjava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Descriptor;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 1528
    :cond_0
    new-instance p2, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {p2, p5}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    invoke-direct {p0, p1, p3, p4, p2}, Lcom/polidea/reactnativeble/BleModule;->safeWriteDescriptorForDevice(Lcom/polidea/reactnativeble/wrapper/Descriptor;Ljava/lang/String;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void
.end method

.method public writeDescriptorForDevice(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p7

    .line 1483
    invoke-direct/range {v0 .. v5}, Lcom/polidea/reactnativeble/BleModule;->getDescriptorOrReject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Descriptor;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 1489
    :cond_0
    new-instance p2, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {p2, p7}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    invoke-direct {p0, p1, p5, p6, p2}, Lcom/polidea/reactnativeble/BleModule;->safeWriteDescriptorForDevice(Lcom/polidea/reactnativeble/wrapper/Descriptor;Ljava/lang/String;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void
.end method

.method public writeDescriptorForService(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 1503
    invoke-direct {p0, p1, p2, p3, p6}, Lcom/polidea/reactnativeble/BleModule;->getDescriptorOrReject(ILjava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)Lcom/polidea/reactnativeble/wrapper/Descriptor;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 1509
    :cond_0
    new-instance p2, Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-direct {p2, p6}, Lcom/polidea/reactnativeble/utils/SafePromise;-><init>(Lcom/facebook/react/bridge/Promise;)V

    invoke-direct {p0, p1, p4, p5, p2}, Lcom/polidea/reactnativeble/BleModule;->safeWriteDescriptorForDevice(Lcom/polidea/reactnativeble/wrapper/Descriptor;Ljava/lang/String;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V

    return-void
.end method
