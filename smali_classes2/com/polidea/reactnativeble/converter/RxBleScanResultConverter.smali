.class public Lcom/polidea/reactnativeble/converter/RxBleScanResultConverter;
.super Lcom/polidea/reactnativeble/converter/JSObjectConverter;
.source "RxBleScanResultConverter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/polidea/reactnativeble/converter/RxBleScanResultConverter$Metadata;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/polidea/reactnativeble/converter/JSObjectConverter<",
        "Lcom/polidea/rxandroidble/scan/ScanResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/polidea/reactnativeble/converter/JSObjectConverter;-><init>()V

    return-void
.end method


# virtual methods
.method public toJSObject(Lcom/polidea/rxandroidble/scan/ScanResult;)Lcom/facebook/react/bridge/WritableMap;
    .locals 6
    .param p1    # Lcom/polidea/rxandroidble/scan/ScanResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 37
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v0

    .line 38
    invoke-virtual {p1}, Lcom/polidea/rxandroidble/scan/ScanResult;->getBleDevice()Lcom/polidea/rxandroidble/RxBleDevice;

    move-result-object v1

    invoke-interface {v1}, Lcom/polidea/rxandroidble/RxBleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v1

    const-string v2, "id"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p1}, Lcom/polidea/rxandroidble/scan/ScanResult;->getBleDevice()Lcom/polidea/rxandroidble/RxBleDevice;

    move-result-object v1

    invoke-interface {v1}, Lcom/polidea/rxandroidble/RxBleDevice;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "name"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p1}, Lcom/polidea/rxandroidble/scan/ScanResult;->getRssi()I

    move-result v1

    const-string v2, "rssi"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    const-string v1, "mtu"

    const/16 v2, 0x17

    .line 41
    invoke-interface {v0, v1, v2}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    .line 43
    invoke-virtual {p1}, Lcom/polidea/rxandroidble/scan/ScanResult;->getScanRecord()Lcom/polidea/rxandroidble/scan/ScanRecord;

    move-result-object p1

    invoke-interface {p1}, Lcom/polidea/rxandroidble/scan/ScanRecord;->getBytes()[B

    move-result-object p1

    invoke-static {p1}, Lcom/polidea/reactnativeble/advertisement/AdvertisementData;->parseScanResponseData([B)Lcom/polidea/reactnativeble/advertisement/AdvertisementData;

    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/advertisement/AdvertisementData;->getManufacturerData()[B

    move-result-object v1

    if-eqz v1, :cond_0

    .line 46
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/advertisement/AdvertisementData;->getManufacturerData()[B

    move-result-object v1

    invoke-static {v1}, Lcom/polidea/reactnativeble/utils/Base64Converter;->encode([B)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "manufacturerData"

    .line 44
    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/advertisement/AdvertisementData;->getServiceData()Ljava/util/Map;

    move-result-object v1

    const-string v2, "serviceData"

    if-eqz v1, :cond_2

    .line 49
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v1

    .line 50
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/advertisement/AdvertisementData;->getServiceData()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 51
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/UUID;

    invoke-static {v5}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->fromUUID(Ljava/util/UUID;)Ljava/lang/String;

    move-result-object v5

    .line 52
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    invoke-static {v4}, Lcom/polidea/reactnativeble/utils/Base64Converter;->encode([B)Ljava/lang/String;

    move-result-object v4

    .line 51
    invoke-interface {v1, v5, v4}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 54
    :cond_1
    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putMap(Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    goto :goto_2

    .line 56
    :cond_2
    invoke-interface {v0, v2}, Lcom/facebook/react/bridge/WritableMap;->putNull(Ljava/lang/String;)V

    .line 59
    :goto_2
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/advertisement/AdvertisementData;->getServiceUUIDs()Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "serviceUUIDs"

    if-eqz v1, :cond_4

    .line 60
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createArray()Lcom/facebook/react/bridge/WritableArray;

    move-result-object v1

    .line 61
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/advertisement/AdvertisementData;->getServiceUUIDs()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/UUID;

    .line 62
    invoke-static {v4}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->fromUUID(Ljava/util/UUID;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Lcom/facebook/react/bridge/WritableArray;->pushString(Ljava/lang/String;)V

    goto :goto_3

    .line 64
    :cond_3
    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putArray(Ljava/lang/String;Lcom/facebook/react/bridge/WritableArray;)V

    goto :goto_4

    .line 66
    :cond_4
    invoke-interface {v0, v2}, Lcom/facebook/react/bridge/WritableMap;->putNull(Ljava/lang/String;)V

    .line 69
    :goto_4
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/advertisement/AdvertisementData;->getLocalName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "localName"

    if-eqz v1, :cond_5

    .line 70
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/advertisement/AdvertisementData;->getLocalName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 72
    :cond_5
    invoke-interface {v0, v2}, Lcom/facebook/react/bridge/WritableMap;->putNull(Ljava/lang/String;)V

    .line 75
    :goto_5
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/advertisement/AdvertisementData;->getTxPowerLevel()Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "txPowerLevel"

    if-eqz v1, :cond_6

    .line 76
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/advertisement/AdvertisementData;->getTxPowerLevel()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    goto :goto_6

    .line 78
    :cond_6
    invoke-interface {v0, v2}, Lcom/facebook/react/bridge/WritableMap;->putNull(Ljava/lang/String;)V

    .line 81
    :goto_6
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/advertisement/AdvertisementData;->getSolicitedServiceUUIDs()Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "solicitedServiceUUIDs"

    if-eqz v1, :cond_8

    .line 82
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createArray()Lcom/facebook/react/bridge/WritableArray;

    move-result-object v1

    .line 83
    invoke-virtual {p1}, Lcom/polidea/reactnativeble/advertisement/AdvertisementData;->getSolicitedServiceUUIDs()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_7
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/UUID;

    .line 84
    invoke-static {v3}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->fromUUID(Ljava/util/UUID;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/facebook/react/bridge/WritableArray;->pushString(Ljava/lang/String;)V

    goto :goto_7

    .line 86
    :cond_7
    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putArray(Ljava/lang/String;Lcom/facebook/react/bridge/WritableArray;)V

    goto :goto_8

    .line 88
    :cond_8
    invoke-interface {v0, v2}, Lcom/facebook/react/bridge/WritableMap;->putNull(Ljava/lang/String;)V

    :goto_8
    const-string p1, "isConnectable"

    .line 92
    invoke-interface {v0, p1}, Lcom/facebook/react/bridge/WritableMap;->putNull(Ljava/lang/String;)V

    const-string p1, "overflowServiceUUIDs"

    .line 93
    invoke-interface {v0, p1}, Lcom/facebook/react/bridge/WritableMap;->putNull(Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic toJSObject(Ljava/lang/Object;)Lcom/facebook/react/bridge/WritableMap;
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 17
    check-cast p1, Lcom/polidea/rxandroidble/scan/ScanResult;

    invoke-virtual {p0, p1}, Lcom/polidea/reactnativeble/converter/RxBleScanResultConverter;->toJSObject(Lcom/polidea/rxandroidble/scan/ScanResult;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object p1

    return-object p1
.end method
