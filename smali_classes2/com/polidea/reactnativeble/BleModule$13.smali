.class Lcom/polidea/reactnativeble/BleModule$13;
.super Ljava/lang/Object;
.source "BleModule.java"

# interfaces
.implements Lrx/functions/Action0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/polidea/reactnativeble/BleModule;->requestConnectionPriorityForDevice(Ljava/lang/String;ILjava/lang/String;Lcom/facebook/react/bridge/Promise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/polidea/reactnativeble/BleModule;

.field final synthetic val$device:Lcom/polidea/reactnativeble/wrapper/Device;

.field final synthetic val$safePromise:Lcom/polidea/reactnativeble/utils/SafePromise;

.field final synthetic val$transactionId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Lcom/polidea/reactnativeble/wrapper/Device;Ljava/lang/String;)V
    .locals 0

    .line 527
    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule$13;->this$0:Lcom/polidea/reactnativeble/BleModule;

    iput-object p2, p0, Lcom/polidea/reactnativeble/BleModule$13;->val$safePromise:Lcom/polidea/reactnativeble/utils/SafePromise;

    iput-object p3, p0, Lcom/polidea/reactnativeble/BleModule$13;->val$device:Lcom/polidea/reactnativeble/wrapper/Device;

    iput-object p4, p0, Lcom/polidea/reactnativeble/BleModule$13;->val$transactionId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .line 530
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$13;->val$safePromise:Lcom/polidea/reactnativeble/utils/SafePromise;

    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule$13;->val$device:Lcom/polidea/reactnativeble/wrapper/Device;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/polidea/reactnativeble/wrapper/Device;->toJSObject(Ljava/lang/Integer;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/polidea/reactnativeble/utils/SafePromise;->resolve(Ljava/lang/Object;)V

    .line 531
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$13;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {v0}, Lcom/polidea/reactnativeble/BleModule;->access$000(Lcom/polidea/reactnativeble/BleModule;)Lcom/polidea/reactnativeble/utils/DisposableMap;

    move-result-object v0

    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule$13;->val$transactionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/polidea/reactnativeble/utils/DisposableMap;->removeSubscription(Ljava/lang/String;)Z

    return-void
.end method
