.class Lcom/polidea/reactnativeble/BleModule$12;
.super Ljava/lang/Object;
.source "BleModule.java"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/polidea/reactnativeble/BleModule;->safeStartDeviceScan([Ljava/util/UUID;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/polidea/reactnativeble/BleModule;


# direct methods
.method constructor <init>(Lcom/polidea/reactnativeble/BleModule;)V
    .locals 0

    .line 427
    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule$12;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 427
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/polidea/reactnativeble/BleModule$12;->call(Ljava/lang/Throwable;)V

    return-void
.end method

.method public call(Ljava/lang/Throwable;)V
    .locals 3

    .line 430
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$12;->this$0:Lcom/polidea/reactnativeble/BleModule;

    sget-object v1, Lcom/polidea/reactnativeble/Event;->ScanEvent:Lcom/polidea/reactnativeble/Event;

    iget-object v2, p0, Lcom/polidea/reactnativeble/BleModule$12;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {v2}, Lcom/polidea/reactnativeble/BleModule;->access$100(Lcom/polidea/reactnativeble/BleModule;)Lcom/polidea/reactnativeble/errors/ErrorConverter;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/polidea/reactnativeble/errors/ErrorConverter;->toError(Ljava/lang/Throwable;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    invoke-virtual {p1}, Lcom/polidea/reactnativeble/errors/BleError;->toJSCallback()Lcom/facebook/react/bridge/ReadableArray;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/polidea/reactnativeble/BleModule;->access$200(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/Event;Ljava/lang/Object;)V

    return-void
.end method
