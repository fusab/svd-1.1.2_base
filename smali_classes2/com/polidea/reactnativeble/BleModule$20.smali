.class Lcom/polidea/reactnativeble/BleModule$20;
.super Ljava/lang/Object;
.source "BleModule.java"

# interfaces
.implements Lrx/functions/Action0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/polidea/reactnativeble/BleModule;->safeConnectToDevice(Lcom/polidea/rxandroidble/RxBleDevice;ZILcom/polidea/reactnativeble/RefreshGattMoment;Ljava/lang/Integer;ILcom/polidea/reactnativeble/utils/SafePromise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/polidea/reactnativeble/BleModule;

.field final synthetic val$device:Lcom/polidea/rxandroidble/RxBleDevice;

.field final synthetic val$promise:Lcom/polidea/reactnativeble/utils/SafePromise;


# direct methods
.method constructor <init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Lcom/polidea/rxandroidble/RxBleDevice;)V
    .locals 0

    .line 686
    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule$20;->this$0:Lcom/polidea/reactnativeble/BleModule;

    iput-object p2, p0, Lcom/polidea/reactnativeble/BleModule$20;->val$promise:Lcom/polidea/reactnativeble/utils/SafePromise;

    iput-object p3, p0, Lcom/polidea/reactnativeble/BleModule$20;->val$device:Lcom/polidea/rxandroidble/RxBleDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .line 689
    invoke-static {}, Lcom/polidea/reactnativeble/errors/BleErrorUtils;->cancelled()Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object v0

    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule$20;->val$promise:Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-virtual {v0, v1}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/polidea/reactnativeble/utils/SafePromise;)V

    .line 690
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$20;->this$0:Lcom/polidea/reactnativeble/BleModule;

    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule$20;->val$device:Lcom/polidea/rxandroidble/RxBleDevice;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/polidea/reactnativeble/BleModule;->access$600(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/rxandroidble/RxBleDevice;Lcom/polidea/reactnativeble/errors/BleError;)V

    return-void
.end method
