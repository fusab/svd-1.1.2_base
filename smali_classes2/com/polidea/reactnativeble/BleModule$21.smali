.class Lcom/polidea/reactnativeble/BleModule$21;
.super Ljava/lang/Object;
.source "BleModule.java"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/polidea/reactnativeble/BleModule;->safeConnectToDevice(Lcom/polidea/rxandroidble/RxBleDevice;ZILcom/polidea/reactnativeble/RefreshGattMoment;Ljava/lang/Integer;ILcom/polidea/reactnativeble/utils/SafePromise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Lcom/polidea/rxandroidble/RxBleConnection;",
        "Lrx/Observable<",
        "Lcom/polidea/rxandroidble/RxBleConnection;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/polidea/reactnativeble/BleModule;


# direct methods
.method constructor <init>(Lcom/polidea/reactnativeble/BleModule;)V
    .locals 0

    .line 695
    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule$21;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 695
    check-cast p1, Lcom/polidea/rxandroidble/RxBleConnection;

    invoke-virtual {p0, p1}, Lcom/polidea/reactnativeble/BleModule$21;->call(Lcom/polidea/rxandroidble/RxBleConnection;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public call(Lcom/polidea/rxandroidble/RxBleConnection;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/polidea/rxandroidble/RxBleConnection;",
            ")",
            "Lrx/Observable<",
            "Lcom/polidea/rxandroidble/RxBleConnection;",
            ">;"
        }
    .end annotation

    .line 698
    new-instance v0, Lcom/polidea/reactnativeble/utils/RefreshGattCustomOperation;

    invoke-direct {v0}, Lcom/polidea/reactnativeble/utils/RefreshGattCustomOperation;-><init>()V

    .line 699
    invoke-interface {p1, v0}, Lcom/polidea/rxandroidble/RxBleConnection;->queue(Lcom/polidea/rxandroidble/RxBleCustomOperation;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/polidea/reactnativeble/BleModule$21$1;

    invoke-direct {v1, p0, p1}, Lcom/polidea/reactnativeble/BleModule$21$1;-><init>(Lcom/polidea/reactnativeble/BleModule$21;Lcom/polidea/rxandroidble/RxBleConnection;)V

    .line 700
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
