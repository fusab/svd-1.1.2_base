.class public Lcom/polidea/reactnativeble/exceptions/CannotMonitorCharacteristicException;
.super Ljava/lang/RuntimeException;
.source "CannotMonitorCharacteristicException.java"


# instance fields
.field private characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/RuntimeException;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/polidea/reactnativeble/exceptions/CannotMonitorCharacteristicException;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-void
.end method


# virtual methods
.method public getCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/polidea/reactnativeble/exceptions/CannotMonitorCharacteristicException;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object v0
.end method
