.class public Lcom/polidea/reactnativeble/wrapper/Characteristic;
.super Ljava/lang/Object;
.source "Characteristic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/polidea/reactnativeble/wrapper/Characteristic$Metadata;
    }
.end annotation


# static fields
.field public static final CLIENT_CHARACTERISTIC_CONFIG_UUID:Ljava/util/UUID;


# instance fields
.field private characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

.field private id:I

.field private service:Lcom/polidea/reactnativeble/wrapper/Service;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "00002902-0000-1000-8000-00805f9b34fb"

    .line 23
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->CLIENT_CHARACTERISTIC_CONFIG_UUID:Ljava/util/UUID;

    return-void
.end method

.method public constructor <init>(Lcom/polidea/reactnativeble/wrapper/Service;Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 2
    .param p1    # Lcom/polidea/reactnativeble/wrapper/Service;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/bluetooth/BluetoothGattCharacteristic;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->service:Lcom/polidea/reactnativeble/wrapper/Service;

    .line 46
    iput-object p2, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 47
    new-instance v0, Lcom/polidea/reactnativeble/utils/IdGeneratorKey;

    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Service;->getDeviceId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getInstanceId()I

    move-result p2

    invoke-direct {v0, p1, v1, p2}, Lcom/polidea/reactnativeble/utils/IdGeneratorKey;-><init>(Ljava/lang/String;Ljava/util/UUID;I)V

    invoke-static {v0}, Lcom/polidea/reactnativeble/utils/IdGenerator;->getIdForKey(Lcom/polidea/reactnativeble/utils/IdGeneratorKey;)I

    move-result p1

    iput p1, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->id:I

    return-void
.end method


# virtual methods
.method public getDescriptorByUUID(Ljava/util/UUID;)Lcom/polidea/reactnativeble/wrapper/Descriptor;
    .locals 1
    .param p1    # Ljava/util/UUID;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 135
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 137
    :cond_0
    new-instance v0, Lcom/polidea/reactnativeble/wrapper/Descriptor;

    invoke-direct {v0, p0, p1}, Lcom/polidea/reactnativeble/wrapper/Descriptor;-><init>(Lcom/polidea/reactnativeble/wrapper/Characteristic;Landroid/bluetooth/BluetoothGattDescriptor;)V

    return-object v0
.end method

.method public getDescriptors()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/polidea/reactnativeble/wrapper/Descriptor;",
            ">;"
        }
    .end annotation

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptors()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 84
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptors()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothGattDescriptor;

    .line 85
    new-instance v3, Lcom/polidea/reactnativeble/wrapper/Descriptor;

    invoke-direct {v3, p0, v2}, Lcom/polidea/reactnativeble/wrapper/Descriptor;-><init>(Lcom/polidea/reactnativeble/wrapper/Characteristic;Landroid/bluetooth/BluetoothGattDescriptor;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->service:Lcom/polidea/reactnativeble/wrapper/Service;

    invoke-virtual {v0}, Lcom/polidea/reactnativeble/wrapper/Service;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .line 67
    iget v0, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->id:I

    return v0
.end method

.method public getInstanceId()I
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getInstanceId()I

    move-result v0

    return v0
.end method

.method public getNativeCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object v0
.end method

.method public getService()Lcom/polidea/reactnativeble/wrapper/Service;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->service:Lcom/polidea/reactnativeble/wrapper/Service;

    return-object v0
.end method

.method public getServiceId()I
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->service:Lcom/polidea/reactnativeble/wrapper/Service;

    invoke-virtual {v0}, Lcom/polidea/reactnativeble/wrapper/Service;->getId()I

    move-result v0

    return v0
.end method

.method public getServiceUUID()Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->service:Lcom/polidea/reactnativeble/wrapper/Service;

    invoke-virtual {v0}, Lcom/polidea/reactnativeble/wrapper/Service;->getUUID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUUID()Ljava/lang/String;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-static {v0}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->fromUUID(Ljava/util/UUID;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public logValue(Ljava/lang/String;[B)V
    .locals 1

    if-nez p2, :cond_0

    .line 124
    iget-object p2, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B

    move-result-object p2

    :cond_0
    if-eqz p2, :cond_1

    .line 126
    invoke-static {p2}, Lcom/polidea/reactnativeble/utils/ByteUtils;->bytesToHex([B)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_1
    const-string p2, "(null)"

    .line 127
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " Characteristic(uuid: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 128
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", id: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->id:I

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", value: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    .line 127
    invoke-static {p1, p2}, Lcom/polidea/rxandroidble/internal/RxBleLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public toJSObject([B)Lcom/facebook/react/bridge/WritableMap;
    .locals 5

    .line 91
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v0

    .line 93
    iget v1, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->id:I

    const-string v2, "id"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    .line 94
    invoke-virtual {p0}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getUUID()Ljava/lang/String;

    move-result-object v1

    const-string v2, "uuid"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->service:Lcom/polidea/reactnativeble/wrapper/Service;

    invoke-virtual {v1}, Lcom/polidea/reactnativeble/wrapper/Service;->getId()I

    move-result v1

    const-string v2, "serviceID"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    .line 96
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->service:Lcom/polidea/reactnativeble/wrapper/Service;

    invoke-virtual {v1}, Lcom/polidea/reactnativeble/wrapper/Service;->getUUID()Ljava/lang/String;

    move-result-object v1

    const-string v2, "serviceUUID"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->service:Lcom/polidea/reactnativeble/wrapper/Service;

    invoke-virtual {v1}, Lcom/polidea/reactnativeble/wrapper/Service;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "deviceID"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getProperties()I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v4, "isReadable"

    invoke-interface {v0, v4, v1}, Lcom/facebook/react/bridge/WritableMap;->putBoolean(Ljava/lang/String;Z)V

    .line 100
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getProperties()I

    move-result v1

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    const-string v4, "isWritableWithResponse"

    invoke-interface {v0, v4, v1}, Lcom/facebook/react/bridge/WritableMap;->putBoolean(Ljava/lang/String;Z)V

    .line 101
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getProperties()I

    move-result v1

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    const-string v4, "isWritableWithoutResponse"

    invoke-interface {v0, v4, v1}, Lcom/facebook/react/bridge/WritableMap;->putBoolean(Ljava/lang/String;Z)V

    .line 102
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getProperties()I

    move-result v1

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    const-string v4, "isNotifiable"

    invoke-interface {v0, v4, v1}, Lcom/facebook/react/bridge/WritableMap;->putBoolean(Ljava/lang/String;Z)V

    .line 103
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getProperties()I

    move-result v1

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    const-string v4, "isIndicatable"

    invoke-interface {v0, v4, v1}, Lcom/facebook/react/bridge/WritableMap;->putBoolean(Ljava/lang/String;Z)V

    .line 105
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    sget-object v4, Lcom/polidea/reactnativeble/wrapper/Characteristic;->CLIENT_CHARACTERISTIC_CONFIG_UUID:Ljava/util/UUID;

    invoke-virtual {v1, v4}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 108
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattDescriptor;->getValue()[B

    move-result-object v1

    if-eqz v1, :cond_5

    .line 110
    aget-byte v1, v1, v3

    and-int/2addr v1, v2

    if-eqz v1, :cond_5

    const/4 v3, 0x1

    :cond_5
    const-string v1, "isNotifying"

    .line 113
    invoke-interface {v0, v1, v3}, Lcom/facebook/react/bridge/WritableMap;->putBoolean(Ljava/lang/String;Z)V

    if-nez p1, :cond_6

    .line 116
    iget-object p1, p0, Lcom/polidea/reactnativeble/wrapper/Characteristic;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B

    move-result-object p1

    :cond_6
    if-eqz p1, :cond_7

    .line 118
    invoke-static {p1}, Lcom/polidea/reactnativeble/utils/Base64Converter;->encode([B)Ljava/lang/String;

    move-result-object p1

    goto :goto_5

    :cond_7
    const/4 p1, 0x0

    :goto_5
    const-string v1, "value"

    invoke-interface {v0, v1, p1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
