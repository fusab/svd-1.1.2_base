.class public Lcom/polidea/reactnativeble/wrapper/Service;
.super Ljava/lang/Object;
.source "Service.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/polidea/reactnativeble/wrapper/Service$Metadata;
    }
.end annotation


# instance fields
.field private device:Lcom/polidea/reactnativeble/wrapper/Device;

.field private id:I

.field private service:Landroid/bluetooth/BluetoothGattService;


# direct methods
.method public constructor <init>(Lcom/polidea/reactnativeble/wrapper/Device;Landroid/bluetooth/BluetoothGattService;)V
    .locals 2
    .param p1    # Lcom/polidea/reactnativeble/wrapper/Device;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/bluetooth/BluetoothGattService;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/polidea/reactnativeble/wrapper/Service;->device:Lcom/polidea/reactnativeble/wrapper/Device;

    .line 33
    iput-object p2, p0, Lcom/polidea/reactnativeble/wrapper/Service;->service:Landroid/bluetooth/BluetoothGattService;

    .line 34
    new-instance v0, Lcom/polidea/reactnativeble/utils/IdGeneratorKey;

    invoke-virtual {p1}, Lcom/polidea/reactnativeble/wrapper/Device;->getDeviceId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattService;->getInstanceId()I

    move-result p2

    invoke-direct {v0, p1, v1, p2}, Lcom/polidea/reactnativeble/utils/IdGeneratorKey;-><init>(Ljava/lang/String;Ljava/util/UUID;I)V

    invoke-static {v0}, Lcom/polidea/reactnativeble/utils/IdGenerator;->getIdForKey(Lcom/polidea/reactnativeble/utils/IdGeneratorKey;)I

    move-result p1

    iput p1, p0, Lcom/polidea/reactnativeble/wrapper/Service;->id:I

    return-void
.end method


# virtual methods
.method public getCharacteristicByUUID(Ljava/util/UUID;)Lcom/polidea/reactnativeble/wrapper/Characteristic;
    .locals 1
    .param p1    # Ljava/util/UUID;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Service;->service:Landroid/bluetooth/BluetoothGattService;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 61
    :cond_0
    new-instance v0, Lcom/polidea/reactnativeble/wrapper/Characteristic;

    invoke-direct {v0, p0, p1}, Lcom/polidea/reactnativeble/wrapper/Characteristic;-><init>(Lcom/polidea/reactnativeble/wrapper/Service;Landroid/bluetooth/BluetoothGattCharacteristic;)V

    return-object v0
.end method

.method public getCharacteristics()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/polidea/reactnativeble/wrapper/Characteristic;",
            ">;"
        }
    .end annotation

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Service;->service:Landroid/bluetooth/BluetoothGattService;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattService;->getCharacteristics()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 66
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Service;->service:Landroid/bluetooth/BluetoothGattService;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattService;->getCharacteristics()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 67
    new-instance v3, Lcom/polidea/reactnativeble/wrapper/Characteristic;

    invoke-direct {v3, p0, v2}, Lcom/polidea/reactnativeble/wrapper/Characteristic;-><init>(Lcom/polidea/reactnativeble/wrapper/Service;Landroid/bluetooth/BluetoothGattCharacteristic;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getDevice()Lcom/polidea/reactnativeble/wrapper/Device;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Service;->device:Lcom/polidea/reactnativeble/wrapper/Device;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Service;->device:Lcom/polidea/reactnativeble/wrapper/Device;

    invoke-virtual {v0}, Lcom/polidea/reactnativeble/wrapper/Device;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .line 38
    iget v0, p0, Lcom/polidea/reactnativeble/wrapper/Service;->id:I

    return v0
.end method

.method public getNativeService()Landroid/bluetooth/BluetoothGattService;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Service;->service:Landroid/bluetooth/BluetoothGattService;

    return-object v0
.end method

.method public getUUID()Ljava/lang/String;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Service;->service:Landroid/bluetooth/BluetoothGattService;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-static {v0}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->fromUUID(Ljava/util/UUID;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toJSObject()Lcom/facebook/react/bridge/WritableMap;
    .locals 3

    .line 73
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v0

    .line 74
    iget v1, p0, Lcom/polidea/reactnativeble/wrapper/Service;->id:I

    const-string v2, "id"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    .line 75
    invoke-virtual {p0}, Lcom/polidea/reactnativeble/wrapper/Service;->getUUID()Ljava/lang/String;

    move-result-object v1

    const-string v2, "uuid"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Service;->device:Lcom/polidea/reactnativeble/wrapper/Device;

    invoke-virtual {v1}, Lcom/polidea/reactnativeble/wrapper/Device;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "deviceID"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Service;->service:Landroid/bluetooth/BluetoothGattService;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattService;->getType()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "isPrimary"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putBoolean(Ljava/lang/String;Z)V

    return-object v0
.end method
