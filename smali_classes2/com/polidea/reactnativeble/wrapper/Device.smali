.class public Lcom/polidea/reactnativeble/wrapper/Device;
.super Ljava/lang/Object;
.source "Device.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/polidea/reactnativeble/wrapper/Device$Metadata;
    }
.end annotation


# instance fields
.field private connection:Lcom/polidea/rxandroidble/RxBleConnection;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private device:Lcom/polidea/rxandroidble/RxBleDevice;

.field private services:Ljava/util/List;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/polidea/reactnativeble/wrapper/Service;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/polidea/rxandroidble/RxBleDevice;Lcom/polidea/rxandroidble/RxBleConnection;)V
    .locals 0
    .param p1    # Lcom/polidea/rxandroidble/RxBleDevice;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/polidea/rxandroidble/RxBleConnection;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/polidea/reactnativeble/wrapper/Device;->device:Lcom/polidea/rxandroidble/RxBleDevice;

    .line 40
    iput-object p2, p0, Lcom/polidea/reactnativeble/wrapper/Device;->connection:Lcom/polidea/rxandroidble/RxBleConnection;

    return-void
.end method


# virtual methods
.method public getConnection()Lcom/polidea/rxandroidble/RxBleConnection;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Device;->connection:Lcom/polidea/rxandroidble/RxBleConnection;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Device;->device:Lcom/polidea/rxandroidble/RxBleDevice;

    invoke-interface {v0}, Lcom/polidea/rxandroidble/RxBleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getServiceByUUID(Ljava/util/UUID;)Lcom/polidea/reactnativeble/wrapper/Service;
    .locals 4
    .param p1    # Ljava/util/UUID;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Device;->services:Ljava/util/List;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 67
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/polidea/reactnativeble/wrapper/Service;

    .line 68
    invoke-virtual {v2}, Lcom/polidea/reactnativeble/wrapper/Service;->getNativeService()Landroid/bluetooth/BluetoothGattService;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    return-object v2

    :cond_2
    return-object v1
.end method

.method public getServices()Ljava/util/List;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/polidea/reactnativeble/wrapper/Service;",
            ">;"
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Device;->services:Ljava/util/List;

    return-object v0
.end method

.method public setServices(Ljava/util/List;)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/polidea/reactnativeble/wrapper/Service;",
            ">;)V"
        }
    .end annotation

    .line 44
    iput-object p1, p0, Lcom/polidea/reactnativeble/wrapper/Device;->services:Ljava/util/List;

    return-void
.end method

.method public toJSObject(Ljava/lang/Integer;)Lcom/facebook/react/bridge/WritableMap;
    .locals 3
    .param p1    # Ljava/lang/Integer;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 75
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v0

    .line 76
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Device;->device:Lcom/polidea/rxandroidble/RxBleDevice;

    invoke-interface {v1}, Lcom/polidea/rxandroidble/RxBleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v1

    const-string v2, "id"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Device;->device:Lcom/polidea/rxandroidble/RxBleDevice;

    invoke-interface {v1}, Lcom/polidea/rxandroidble/RxBleDevice;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "name"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "rssi"

    if-eqz p1, :cond_0

    .line 79
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-interface {v0, v1, p1}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 81
    :cond_0
    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/WritableMap;->putNull(Ljava/lang/String;)V

    .line 83
    :goto_0
    iget-object p1, p0, Lcom/polidea/reactnativeble/wrapper/Device;->connection:Lcom/polidea/rxandroidble/RxBleConnection;

    const-string v1, "mtu"

    if-eqz p1, :cond_1

    .line 84
    invoke-interface {p1}, Lcom/polidea/rxandroidble/RxBleConnection;->getMtu()I

    move-result p1

    invoke-interface {v0, v1, p1}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    :cond_1
    const/16 p1, 0x17

    .line 86
    invoke-interface {v0, v1, p1}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    :goto_1
    const-string p1, "manufacturerData"

    .line 90
    invoke-interface {v0, p1}, Lcom/facebook/react/bridge/WritableMap;->putNull(Ljava/lang/String;)V

    const-string p1, "serviceData"

    .line 91
    invoke-interface {v0, p1}, Lcom/facebook/react/bridge/WritableMap;->putNull(Ljava/lang/String;)V

    const-string p1, "serviceUUIDs"

    .line 92
    invoke-interface {v0, p1}, Lcom/facebook/react/bridge/WritableMap;->putNull(Ljava/lang/String;)V

    const-string p1, "localName"

    .line 93
    invoke-interface {v0, p1}, Lcom/facebook/react/bridge/WritableMap;->putNull(Ljava/lang/String;)V

    const-string p1, "txPowerLevel"

    .line 94
    invoke-interface {v0, p1}, Lcom/facebook/react/bridge/WritableMap;->putNull(Ljava/lang/String;)V

    const-string p1, "solicitedServiceUUIDs"

    .line 95
    invoke-interface {v0, p1}, Lcom/facebook/react/bridge/WritableMap;->putNull(Ljava/lang/String;)V

    const-string p1, "isConnectable"

    .line 96
    invoke-interface {v0, p1}, Lcom/facebook/react/bridge/WritableMap;->putNull(Ljava/lang/String;)V

    const-string p1, "overflowServiceUUIDs"

    .line 97
    invoke-interface {v0, p1}, Lcom/facebook/react/bridge/WritableMap;->putNull(Ljava/lang/String;)V

    return-object v0
.end method
