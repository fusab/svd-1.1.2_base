.class public Lcom/polidea/reactnativeble/wrapper/Descriptor;
.super Ljava/lang/Object;
.source "Descriptor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/polidea/reactnativeble/wrapper/Descriptor$Metadata;
    }
.end annotation


# instance fields
.field private characteristic:Lcom/polidea/reactnativeble/wrapper/Characteristic;

.field private descriptor:Landroid/bluetooth/BluetoothGattDescriptor;

.field private id:I


# direct methods
.method public constructor <init>(Lcom/polidea/reactnativeble/wrapper/Characteristic;Landroid/bluetooth/BluetoothGattDescriptor;)V
    .locals 2
    .param p1    # Lcom/polidea/reactnativeble/wrapper/Characteristic;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/bluetooth/BluetoothGattDescriptor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->characteristic:Lcom/polidea/reactnativeble/wrapper/Characteristic;

    .line 35
    iput-object p2, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->descriptor:Landroid/bluetooth/BluetoothGattDescriptor;

    .line 36
    new-instance p1, Lcom/polidea/reactnativeble/utils/IdGeneratorKey;

    iget-object p2, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->characteristic:Lcom/polidea/reactnativeble/wrapper/Characteristic;

    invoke-virtual {p2}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getDeviceId()Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->descriptor:Landroid/bluetooth/BluetoothGattDescriptor;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattDescriptor;->getUuid()Ljava/util/UUID;

    move-result-object v0

    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->characteristic:Lcom/polidea/reactnativeble/wrapper/Characteristic;

    invoke-virtual {v1}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getInstanceId()I

    move-result v1

    invoke-direct {p1, p2, v0, v1}, Lcom/polidea/reactnativeble/utils/IdGeneratorKey;-><init>(Ljava/lang/String;Ljava/util/UUID;I)V

    invoke-static {p1}, Lcom/polidea/reactnativeble/utils/IdGenerator;->getIdForKey(Lcom/polidea/reactnativeble/utils/IdGeneratorKey;)I

    move-result p1

    iput p1, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->id:I

    return-void
.end method


# virtual methods
.method public getCharacteristic()Lcom/polidea/reactnativeble/wrapper/Characteristic;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->characteristic:Lcom/polidea/reactnativeble/wrapper/Characteristic;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->characteristic:Lcom/polidea/reactnativeble/wrapper/Characteristic;

    invoke-virtual {v0}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .line 40
    iget v0, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->id:I

    return v0
.end method

.method public getNativeDescriptor()Landroid/bluetooth/BluetoothGattDescriptor;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->descriptor:Landroid/bluetooth/BluetoothGattDescriptor;

    return-object v0
.end method

.method public logValue(Ljava/lang/String;[B)V
    .locals 1

    if-nez p2, :cond_0

    .line 74
    iget-object p2, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->descriptor:Landroid/bluetooth/BluetoothGattDescriptor;

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattDescriptor;->getValue()[B

    move-result-object p2

    :cond_0
    if-eqz p2, :cond_1

    .line 76
    invoke-static {p2}, Lcom/polidea/reactnativeble/utils/ByteUtils;->bytesToHex([B)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_1
    const-string p2, "(null)"

    .line 77
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " Descriptor(uuid: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->descriptor:Landroid/bluetooth/BluetoothGattDescriptor;

    .line 78
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattDescriptor;->getUuid()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", id: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->id:I

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", value: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    .line 77
    invoke-static {p1, p2}, Lcom/polidea/rxandroidble/internal/RxBleLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public toJSObject([B)Lcom/facebook/react/bridge/WritableMap;
    .locals 3

    .line 56
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v0

    .line 57
    iget v1, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->id:I

    const-string v2, "id"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    .line 58
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->descriptor:Landroid/bluetooth/BluetoothGattDescriptor;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattDescriptor;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-static {v1}, Lcom/polidea/reactnativeble/utils/UUIDConverter;->fromUUID(Ljava/util/UUID;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "uuid"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->characteristic:Lcom/polidea/reactnativeble/wrapper/Characteristic;

    invoke-virtual {v1}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getId()I

    move-result v1

    const-string v2, "characteristicID"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    .line 60
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->characteristic:Lcom/polidea/reactnativeble/wrapper/Characteristic;

    invoke-virtual {v1}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getUUID()Ljava/lang/String;

    move-result-object v1

    const-string v2, "characteristicUUID"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->characteristic:Lcom/polidea/reactnativeble/wrapper/Characteristic;

    invoke-virtual {v1}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getServiceId()I

    move-result v1

    const-string v2, "serviceID"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    .line 62
    iget-object v1, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->characteristic:Lcom/polidea/reactnativeble/wrapper/Characteristic;

    invoke-virtual {v1}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getServiceUUID()Ljava/lang/String;

    move-result-object v1

    const-string v2, "serviceUUID"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-virtual {p0}, Lcom/polidea/reactnativeble/wrapper/Descriptor;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "deviceID"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    .line 66
    iget-object p1, p0, Lcom/polidea/reactnativeble/wrapper/Descriptor;->descriptor:Landroid/bluetooth/BluetoothGattDescriptor;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattDescriptor;->getValue()[B

    move-result-object p1

    :cond_0
    if-eqz p1, :cond_1

    .line 68
    invoke-static {p1}, Lcom/polidea/reactnativeble/utils/Base64Converter;->encode([B)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    const-string v1, "value"

    invoke-interface {v0, v1, p1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
