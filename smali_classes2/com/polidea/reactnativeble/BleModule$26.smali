.class Lcom/polidea/reactnativeble/BleModule$26;
.super Ljava/lang/Object;
.source "BleModule.java"

# interfaces
.implements Lrx/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/polidea/reactnativeble/BleModule;->safeConnectToDevice(Lcom/polidea/rxandroidble/RxBleDevice;ZILcom/polidea/reactnativeble/RefreshGattMoment;Ljava/lang/Integer;ILcom/polidea/reactnativeble/utils/SafePromise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observer<",
        "Lcom/polidea/rxandroidble/RxBleConnection;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/polidea/reactnativeble/BleModule;

.field final synthetic val$device:Lcom/polidea/rxandroidble/RxBleDevice;

.field final synthetic val$promise:Lcom/polidea/reactnativeble/utils/SafePromise;


# direct methods
.method constructor <init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Lcom/polidea/rxandroidble/RxBleDevice;)V
    .locals 0

    .line 755
    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule$26;->this$0:Lcom/polidea/reactnativeble/BleModule;

    iput-object p2, p0, Lcom/polidea/reactnativeble/BleModule$26;->val$promise:Lcom/polidea/reactnativeble/utils/SafePromise;

    iput-object p3, p0, Lcom/polidea/reactnativeble/BleModule$26;->val$device:Lcom/polidea/rxandroidble/RxBleDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 0

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2

    .line 762
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$26;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {v0}, Lcom/polidea/reactnativeble/BleModule;->access$100(Lcom/polidea/reactnativeble/BleModule;)Lcom/polidea/reactnativeble/errors/ErrorConverter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/polidea/reactnativeble/errors/ErrorConverter;->toError(Ljava/lang/Throwable;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    .line 763
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$26;->val$promise:Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-virtual {p1, v0}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/polidea/reactnativeble/utils/SafePromise;)V

    .line 764
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$26;->this$0:Lcom/polidea/reactnativeble/BleModule;

    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule$26;->val$device:Lcom/polidea/rxandroidble/RxBleDevice;

    invoke-static {v0, v1, p1}, Lcom/polidea/reactnativeble/BleModule;->access$600(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/rxandroidble/RxBleDevice;Lcom/polidea/reactnativeble/errors/BleError;)V

    return-void
.end method

.method public onNext(Lcom/polidea/rxandroidble/RxBleConnection;)V
    .locals 2

    .line 769
    new-instance v0, Lcom/polidea/reactnativeble/wrapper/Device;

    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule$26;->val$device:Lcom/polidea/rxandroidble/RxBleDevice;

    invoke-direct {v0, v1, p1}, Lcom/polidea/reactnativeble/wrapper/Device;-><init>(Lcom/polidea/rxandroidble/RxBleDevice;Lcom/polidea/rxandroidble/RxBleConnection;)V

    .line 770
    iget-object p1, p0, Lcom/polidea/reactnativeble/BleModule$26;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {p1, v0}, Lcom/polidea/reactnativeble/BleModule;->access$700(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/wrapper/Device;)V

    .line 771
    iget-object p1, p0, Lcom/polidea/reactnativeble/BleModule$26;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {p1}, Lcom/polidea/reactnativeble/BleModule;->access$800(Lcom/polidea/reactnativeble/BleModule;)Ljava/util/HashMap;

    move-result-object p1

    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule$26;->val$device:Lcom/polidea/rxandroidble/RxBleDevice;

    invoke-interface {v1}, Lcom/polidea/rxandroidble/RxBleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 772
    iget-object p1, p0, Lcom/polidea/reactnativeble/BleModule$26;->val$promise:Lcom/polidea/reactnativeble/utils/SafePromise;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/polidea/reactnativeble/wrapper/Device;->toJSObject(Ljava/lang/Integer;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/polidea/reactnativeble/utils/SafePromise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 755
    check-cast p1, Lcom/polidea/rxandroidble/RxBleConnection;

    invoke-virtual {p0, p1}, Lcom/polidea/reactnativeble/BleModule$26;->onNext(Lcom/polidea/rxandroidble/RxBleConnection;)V

    return-void
.end method
