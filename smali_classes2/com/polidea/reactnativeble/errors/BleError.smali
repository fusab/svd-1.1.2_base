.class public Lcom/polidea/reactnativeble/errors/BleError;
.super Ljava/lang/Object;
.source "BleError.java"


# instance fields
.field private androidCode:Ljava/lang/Integer;

.field public characteristicUUID:Ljava/lang/String;

.field public descriptorUUID:Ljava/lang/String;

.field public deviceID:Ljava/lang/String;

.field private errorCode:Lcom/polidea/reactnativeble/errors/BleErrorCode;

.field public internalMessage:Ljava/lang/String;

.field private reason:Ljava/lang/String;

.field public serviceUUID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/polidea/reactnativeble/errors/BleError;->errorCode:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    .line 25
    iput-object p2, p0, Lcom/polidea/reactnativeble/errors/BleError;->reason:Ljava/lang/String;

    .line 26
    iput-object p3, p0, Lcom/polidea/reactnativeble/errors/BleError;->androidCode:Ljava/lang/Integer;

    return-void
.end method

.method private appendString(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, ",\""

    .line 65
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\":"

    .line 67
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez p3, :cond_0

    const-string p2, "null"

    .line 69
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    const-string p2, "\""

    .line 71
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    return-void
.end method


# virtual methods
.method public reject(Lcom/facebook/react/bridge/Promise;)V
    .locals 2
    .param p1    # Lcom/facebook/react/bridge/Promise;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 85
    invoke-virtual {p0}, Lcom/polidea/reactnativeble/errors/BleError;->toJS()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1, v1, v0}, Lcom/facebook/react/bridge/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public reject(Lcom/polidea/reactnativeble/utils/SafePromise;)V
    .locals 2
    .param p1    # Lcom/polidea/reactnativeble/utils/SafePromise;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 89
    invoke-virtual {p0}, Lcom/polidea/reactnativeble/errors/BleError;->toJS()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Lcom/polidea/reactnativeble/utils/SafePromise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public toJS()Ljava/lang/String;
    .locals 4

    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{"

    .line 31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\"errorCode\":"

    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    iget-object v1, p0, Lcom/polidea/reactnativeble/errors/BleError;->errorCode:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    iget v1, v1, Lcom/polidea/reactnativeble/errors/BleErrorCode;->code:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ",\"attErrorCode\":"

    .line 36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    iget-object v1, p0, Lcom/polidea/reactnativeble/errors/BleError;->androidCode:Ljava/lang/Integer;

    const/16 v2, 0x80

    const-string v3, "null"

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v1, v2, :cond_1

    iget-object v1, p0, Lcom/polidea/reactnativeble/errors/BleError;->androidCode:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gez v1, :cond_0

    goto :goto_0

    .line 40
    :cond_0
    iget-object v1, p0, Lcom/polidea/reactnativeble/errors/BleError;->androidCode:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 38
    :cond_1
    :goto_0
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const-string v1, ",\"iosErrorCode\": null"

    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",\"androidErrorCode\":"

    .line 45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    iget-object v1, p0, Lcom/polidea/reactnativeble/errors/BleError;->androidCode:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v1, v2, :cond_2

    goto :goto_2

    .line 49
    :cond_2
    iget-object v1, p0, Lcom/polidea/reactnativeble/errors/BleError;->androidCode:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 47
    :cond_3
    :goto_2
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    :goto_3
    iget-object v1, p0, Lcom/polidea/reactnativeble/errors/BleError;->reason:Ljava/lang/String;

    const-string v2, "reason"

    invoke-direct {p0, v0, v2, v1}, Lcom/polidea/reactnativeble/errors/BleError;->appendString(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    iget-object v1, p0, Lcom/polidea/reactnativeble/errors/BleError;->deviceID:Ljava/lang/String;

    const-string v2, "deviceID"

    invoke-direct {p0, v0, v2, v1}, Lcom/polidea/reactnativeble/errors/BleError;->appendString(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    iget-object v1, p0, Lcom/polidea/reactnativeble/errors/BleError;->serviceUUID:Ljava/lang/String;

    const-string v2, "serviceUUID"

    invoke-direct {p0, v0, v2, v1}, Lcom/polidea/reactnativeble/errors/BleError;->appendString(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    iget-object v1, p0, Lcom/polidea/reactnativeble/errors/BleError;->characteristicUUID:Ljava/lang/String;

    const-string v2, "characteristicUUID"

    invoke-direct {p0, v0, v2, v1}, Lcom/polidea/reactnativeble/errors/BleError;->appendString(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    iget-object v1, p0, Lcom/polidea/reactnativeble/errors/BleError;->descriptorUUID:Ljava/lang/String;

    const-string v2, "descriptorUUID"

    invoke-direct {p0, v0, v2, v1}, Lcom/polidea/reactnativeble/errors/BleError;->appendString(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iget-object v1, p0, Lcom/polidea/reactnativeble/errors/BleError;->internalMessage:Ljava/lang/String;

    const-string v2, "internalMessage"

    invoke-direct {p0, v0, v2, v1}, Lcom/polidea/reactnativeble/errors/BleError;->appendString(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "}"

    .line 59
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toJSCallback()Lcom/facebook/react/bridge/ReadableArray;
    .locals 2

    .line 78
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createArray()Lcom/facebook/react/bridge/WritableArray;

    move-result-object v0

    .line 79
    invoke-virtual {p0}, Lcom/polidea/reactnativeble/errors/BleError;->toJS()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/WritableArray;->pushString(Ljava/lang/String;)V

    .line 80
    invoke-interface {v0}, Lcom/facebook/react/bridge/WritableArray;->pushNull()V

    return-object v0
.end method
