.class public Lcom/polidea/reactnativeble/errors/BleErrorUtils;
.super Ljava/lang/Object;
.source "BleErrorUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cancelled()Lcom/polidea/reactnativeble/errors/BleError;
    .locals 3

    .line 10
    new-instance v0, Lcom/polidea/reactnativeble/errors/BleError;

    sget-object v1, Lcom/polidea/reactnativeble/errors/BleErrorCode;->OperationCancelled:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/polidea/reactnativeble/errors/BleError;-><init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static cannotMonitorCharacteristic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;
    .locals 3

    .line 86
    new-instance v0, Lcom/polidea/reactnativeble/errors/BleError;

    sget-object v1, Lcom/polidea/reactnativeble/errors/BleErrorCode;->CharacteristicNotifyChangeFailed:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/polidea/reactnativeble/errors/BleError;-><init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 87
    iput-object p1, v0, Lcom/polidea/reactnativeble/errors/BleError;->deviceID:Ljava/lang/String;

    .line 88
    iput-object p2, v0, Lcom/polidea/reactnativeble/errors/BleError;->serviceUUID:Ljava/lang/String;

    .line 89
    iput-object p3, v0, Lcom/polidea/reactnativeble/errors/BleError;->characteristicUUID:Ljava/lang/String;

    return-object v0
.end method

.method public static characteristicNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;
    .locals 3

    .line 48
    new-instance v0, Lcom/polidea/reactnativeble/errors/BleError;

    sget-object v1, Lcom/polidea/reactnativeble/errors/BleErrorCode;->CharacteristicNotFound:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/polidea/reactnativeble/errors/BleError;-><init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 49
    iput-object p0, v0, Lcom/polidea/reactnativeble/errors/BleError;->characteristicUUID:Ljava/lang/String;

    return-object v0
.end method

.method public static descriptorNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;
    .locals 3

    .line 54
    new-instance v0, Lcom/polidea/reactnativeble/errors/BleError;

    sget-object v1, Lcom/polidea/reactnativeble/errors/BleErrorCode;->DescriptorNotFound:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/polidea/reactnativeble/errors/BleError;-><init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 55
    iput-object p0, v0, Lcom/polidea/reactnativeble/errors/BleError;->descriptorUUID:Ljava/lang/String;

    return-object v0
.end method

.method public static descriptorWriteNotAllowed(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;
    .locals 3

    .line 74
    new-instance v0, Lcom/polidea/reactnativeble/errors/BleError;

    sget-object v1, Lcom/polidea/reactnativeble/errors/BleErrorCode;->DescriptorWriteNotAllowed:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/polidea/reactnativeble/errors/BleError;-><init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 75
    iput-object p0, v0, Lcom/polidea/reactnativeble/errors/BleError;->descriptorUUID:Ljava/lang/String;

    return-object v0
.end method

.method public static deviceNotConnected(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;
    .locals 3

    .line 42
    new-instance v0, Lcom/polidea/reactnativeble/errors/BleError;

    sget-object v1, Lcom/polidea/reactnativeble/errors/BleErrorCode;->DeviceNotConnected:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/polidea/reactnativeble/errors/BleError;-><init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 43
    iput-object p0, v0, Lcom/polidea/reactnativeble/errors/BleError;->deviceID:Ljava/lang/String;

    return-object v0
.end method

.method public static deviceNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;
    .locals 3

    .line 36
    new-instance v0, Lcom/polidea/reactnativeble/errors/BleError;

    sget-object v1, Lcom/polidea/reactnativeble/errors/BleErrorCode;->DeviceNotFound:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/polidea/reactnativeble/errors/BleError;-><init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 37
    iput-object p0, v0, Lcom/polidea/reactnativeble/errors/BleError;->deviceID:Ljava/lang/String;

    return-object v0
.end method

.method public static deviceServicesNotDiscovered(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;
    .locals 3

    .line 94
    new-instance v0, Lcom/polidea/reactnativeble/errors/BleError;

    sget-object v1, Lcom/polidea/reactnativeble/errors/BleErrorCode;->ServicesNotDiscovered:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/polidea/reactnativeble/errors/BleError;-><init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 95
    iput-object p0, v0, Lcom/polidea/reactnativeble/errors/BleError;->deviceID:Ljava/lang/String;

    return-object v0
.end method

.method public static invalidIdentifiers(Lcom/facebook/react/bridge/ReadableArray;)Lcom/polidea/reactnativeble/errors/BleError;
    .locals 3
    .param p0    # Lcom/facebook/react/bridge/ReadableArray;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    .line 26
    :goto_0
    invoke-interface {p0}, Lcom/facebook/react/bridge/ReadableArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 27
    invoke-interface {p0, v1}, Lcom/facebook/react/bridge/ReadableArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 30
    :cond_0
    new-instance p0, Lcom/polidea/reactnativeble/errors/BleError;

    sget-object v1, Lcom/polidea/reactnativeble/errors/BleErrorCode;->InvalidIdentifiers:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, v2}, Lcom/polidea/reactnativeble/errors/BleError;-><init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/polidea/reactnativeble/errors/BleError;->internalMessage:Ljava/lang/String;

    return-object p0
.end method

.method public static varargs invalidIdentifiers([Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;
    .locals 4
    .param p0    # [Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p0, v2

    .line 16
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 19
    :cond_0
    new-instance p0, Lcom/polidea/reactnativeble/errors/BleError;

    sget-object v1, Lcom/polidea/reactnativeble/errors/BleErrorCode;->InvalidIdentifiers:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, v2}, Lcom/polidea/reactnativeble/errors/BleError;-><init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/polidea/reactnativeble/errors/BleError;->internalMessage:Ljava/lang/String;

    return-object p0
.end method

.method public static invalidWriteDataForCharacteristic(Ljava/lang/String;Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;
    .locals 3

    .line 60
    new-instance v0, Lcom/polidea/reactnativeble/errors/BleError;

    sget-object v1, Lcom/polidea/reactnativeble/errors/BleErrorCode;->CharacteristicInvalidDataFormat:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/polidea/reactnativeble/errors/BleError;-><init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 61
    iput-object p1, v0, Lcom/polidea/reactnativeble/errors/BleError;->characteristicUUID:Ljava/lang/String;

    .line 62
    iput-object p0, v0, Lcom/polidea/reactnativeble/errors/BleError;->internalMessage:Ljava/lang/String;

    return-object v0
.end method

.method public static invalidWriteDataForDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;
    .locals 3

    .line 67
    new-instance v0, Lcom/polidea/reactnativeble/errors/BleError;

    sget-object v1, Lcom/polidea/reactnativeble/errors/BleErrorCode;->DescriptorInvalidDataFormat:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/polidea/reactnativeble/errors/BleError;-><init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 68
    iput-object p1, v0, Lcom/polidea/reactnativeble/errors/BleError;->descriptorUUID:Ljava/lang/String;

    .line 69
    iput-object p0, v0, Lcom/polidea/reactnativeble/errors/BleError;->internalMessage:Ljava/lang/String;

    return-object v0
.end method

.method public static serviceNotFound(Ljava/lang/String;)Lcom/polidea/reactnativeble/errors/BleError;
    .locals 3

    .line 80
    new-instance v0, Lcom/polidea/reactnativeble/errors/BleError;

    sget-object v1, Lcom/polidea/reactnativeble/errors/BleErrorCode;->ServiceNotFound:Lcom/polidea/reactnativeble/errors/BleErrorCode;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/polidea/reactnativeble/errors/BleError;-><init>(Lcom/polidea/reactnativeble/errors/BleErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 81
    iput-object p0, v0, Lcom/polidea/reactnativeble/errors/BleError;->serviceUUID:Ljava/lang/String;

    return-object v0
.end method
