.class Lcom/polidea/reactnativeble/BleModule$24;
.super Ljava/lang/Object;
.source "BleModule.java"

# interfaces
.implements Lrx/functions/Func0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/polidea/reactnativeble/BleModule;->safeConnectToDevice(Lcom/polidea/rxandroidble/RxBleDevice;ZILcom/polidea/reactnativeble/RefreshGattMoment;Ljava/lang/Integer;ILcom/polidea/reactnativeble/utils/SafePromise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/Func0<",
        "Lrx/Observable<",
        "Ljava/lang/Long;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/polidea/reactnativeble/BleModule;

.field final synthetic val$timeout:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Lcom/polidea/reactnativeble/BleModule;Ljava/lang/Integer;)V
    .locals 0

    .line 740
    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule$24;->this$0:Lcom/polidea/reactnativeble/BleModule;

    iput-object p2, p0, Lcom/polidea/reactnativeble/BleModule$24;->val$timeout:Ljava/lang/Integer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 740
    invoke-virtual {p0}, Lcom/polidea/reactnativeble/BleModule$24;->call()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public call()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 743
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$24;->val$timeout:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v2}, Lrx/Observable;->timer(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method
