.class Lcom/polidea/reactnativeble/BleModule$27;
.super Ljava/lang/Object;
.source "BleModule.java"

# interfaces
.implements Lrx/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/polidea/reactnativeble/BleModule;->safeDiscoverAllServicesAndCharacteristicsForDevice(Lcom/polidea/reactnativeble/wrapper/Device;Ljava/lang/String;Lcom/polidea/reactnativeble/utils/SafePromise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observer<",
        "Lcom/polidea/rxandroidble/RxBleDeviceServices;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/polidea/reactnativeble/BleModule;

.field final synthetic val$device:Lcom/polidea/reactnativeble/wrapper/Device;

.field final synthetic val$promise:Lcom/polidea/reactnativeble/utils/SafePromise;

.field final synthetic val$transactionId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/polidea/reactnativeble/BleModule;Lcom/polidea/reactnativeble/utils/SafePromise;Lcom/polidea/reactnativeble/wrapper/Device;Ljava/lang/String;)V
    .locals 0

    .line 862
    iput-object p1, p0, Lcom/polidea/reactnativeble/BleModule$27;->this$0:Lcom/polidea/reactnativeble/BleModule;

    iput-object p2, p0, Lcom/polidea/reactnativeble/BleModule$27;->val$promise:Lcom/polidea/reactnativeble/utils/SafePromise;

    iput-object p3, p0, Lcom/polidea/reactnativeble/BleModule$27;->val$device:Lcom/polidea/reactnativeble/wrapper/Device;

    iput-object p4, p0, Lcom/polidea/reactnativeble/BleModule$27;->val$transactionId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 3

    .line 865
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$27;->val$promise:Lcom/polidea/reactnativeble/utils/SafePromise;

    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule$27;->val$device:Lcom/polidea/reactnativeble/wrapper/Device;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/polidea/reactnativeble/wrapper/Device;->toJSObject(Ljava/lang/Integer;)Lcom/facebook/react/bridge/WritableMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/polidea/reactnativeble/utils/SafePromise;->resolve(Ljava/lang/Object;)V

    .line 866
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$27;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {v0}, Lcom/polidea/reactnativeble/BleModule;->access$000(Lcom/polidea/reactnativeble/BleModule;)Lcom/polidea/reactnativeble/utils/DisposableMap;

    move-result-object v0

    iget-object v1, p0, Lcom/polidea/reactnativeble/BleModule$27;->val$transactionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/polidea/reactnativeble/utils/DisposableMap;->removeSubscription(Ljava/lang/String;)Z

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 871
    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$27;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {v0}, Lcom/polidea/reactnativeble/BleModule;->access$100(Lcom/polidea/reactnativeble/BleModule;)Lcom/polidea/reactnativeble/errors/ErrorConverter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/polidea/reactnativeble/errors/ErrorConverter;->toError(Ljava/lang/Throwable;)Lcom/polidea/reactnativeble/errors/BleError;

    move-result-object p1

    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$27;->val$promise:Lcom/polidea/reactnativeble/utils/SafePromise;

    invoke-virtual {p1, v0}, Lcom/polidea/reactnativeble/errors/BleError;->reject(Lcom/polidea/reactnativeble/utils/SafePromise;)V

    .line 872
    iget-object p1, p0, Lcom/polidea/reactnativeble/BleModule$27;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {p1}, Lcom/polidea/reactnativeble/BleModule;->access$000(Lcom/polidea/reactnativeble/BleModule;)Lcom/polidea/reactnativeble/utils/DisposableMap;

    move-result-object p1

    iget-object v0, p0, Lcom/polidea/reactnativeble/BleModule$27;->val$transactionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/polidea/reactnativeble/utils/DisposableMap;->removeSubscription(Ljava/lang/String;)Z

    return-void
.end method

.method public onNext(Lcom/polidea/rxandroidble/RxBleDeviceServices;)V
    .locals 8

    .line 877
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 878
    invoke-virtual {p1}, Lcom/polidea/rxandroidble/RxBleDeviceServices;->getBluetoothGattServices()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothGattService;

    .line 879
    new-instance v2, Lcom/polidea/reactnativeble/wrapper/Service;

    iget-object v3, p0, Lcom/polidea/reactnativeble/BleModule$27;->val$device:Lcom/polidea/reactnativeble/wrapper/Device;

    invoke-direct {v2, v3, v1}, Lcom/polidea/reactnativeble/wrapper/Service;-><init>(Lcom/polidea/reactnativeble/wrapper/Device;Landroid/bluetooth/BluetoothGattService;)V

    .line 880
    iget-object v3, p0, Lcom/polidea/reactnativeble/BleModule$27;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {v3}, Lcom/polidea/reactnativeble/BleModule;->access$900(Lcom/polidea/reactnativeble/BleModule;)Landroid/util/SparseArray;

    move-result-object v3

    invoke-virtual {v2}, Lcom/polidea/reactnativeble/wrapper/Service;->getId()I

    move-result v4

    invoke-virtual {v3, v4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 881
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 883
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattService;->getCharacteristics()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 884
    new-instance v4, Lcom/polidea/reactnativeble/wrapper/Characteristic;

    invoke-direct {v4, v2, v3}, Lcom/polidea/reactnativeble/wrapper/Characteristic;-><init>(Lcom/polidea/reactnativeble/wrapper/Service;Landroid/bluetooth/BluetoothGattCharacteristic;)V

    .line 885
    iget-object v5, p0, Lcom/polidea/reactnativeble/BleModule$27;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {v5}, Lcom/polidea/reactnativeble/BleModule;->access$1000(Lcom/polidea/reactnativeble/BleModule;)Landroid/util/SparseArray;

    move-result-object v5

    invoke-virtual {v4}, Lcom/polidea/reactnativeble/wrapper/Characteristic;->getId()I

    move-result v6

    invoke-virtual {v5, v6, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 887
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptors()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/bluetooth/BluetoothGattDescriptor;

    .line 888
    new-instance v6, Lcom/polidea/reactnativeble/wrapper/Descriptor;

    invoke-direct {v6, v4, v5}, Lcom/polidea/reactnativeble/wrapper/Descriptor;-><init>(Lcom/polidea/reactnativeble/wrapper/Characteristic;Landroid/bluetooth/BluetoothGattDescriptor;)V

    .line 889
    iget-object v5, p0, Lcom/polidea/reactnativeble/BleModule$27;->this$0:Lcom/polidea/reactnativeble/BleModule;

    invoke-static {v5}, Lcom/polidea/reactnativeble/BleModule;->access$1100(Lcom/polidea/reactnativeble/BleModule;)Landroid/util/SparseArray;

    move-result-object v5

    invoke-virtual {v6}, Lcom/polidea/reactnativeble/wrapper/Descriptor;->getId()I

    move-result v7

    invoke-virtual {v5, v7, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 893
    :cond_2
    iget-object p1, p0, Lcom/polidea/reactnativeble/BleModule$27;->val$device:Lcom/polidea/reactnativeble/wrapper/Device;

    invoke-virtual {p1, v0}, Lcom/polidea/reactnativeble/wrapper/Device;->setServices(Ljava/util/List;)V

    return-void
.end method

.method public bridge synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 862
    check-cast p1, Lcom/polidea/rxandroidble/RxBleDeviceServices;

    invoke-virtual {p0, p1}, Lcom/polidea/reactnativeble/BleModule$27;->onNext(Lcom/polidea/rxandroidble/RxBleDeviceServices;)V

    return-void
.end method
