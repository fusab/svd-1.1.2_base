.class public final Lcom/google/android/gms/internal/fitness/zzfa;
.super Ljava/lang/Object;


# static fields
.field private static final zzjb:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x7a

    .line 13
    new-array v0, v0, [Ljava/lang/String;

    .line 14
    sput-object v0, Lcom/google/android/gms/internal/fitness/zzfa;->zzjb:[Ljava/lang/String;

    const/16 v1, 0x9

    const-string v2, "aerobics"

    aput-object v2, v0, v1

    .line 15
    sget-object v0, Lcom/google/android/gms/internal/fitness/zzfa;->zzjb:[Ljava/lang/String;

    const/16 v1, 0x77

    const-string v2, "archery"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "badminton"

    .line 16
    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "baseball"

    .line 17
    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "basketball"

    .line 18
    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "biathlon"

    .line 19
    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "biking"

    .line 20
    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "biking.hand"

    .line 21
    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "biking.mountain"

    .line 22
    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "biking.road"

    .line 23
    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "biking.spinning"

    .line 24
    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "biking.stationary"

    .line 25
    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "biking.utility"

    .line 26
    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "boxing"

    .line 27
    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "calisthenics"

    .line 28
    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "circuit_training"

    .line 29
    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "cricket"

    .line 30
    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, "crossfit"

    .line 31
    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "curling"

    .line 32
    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "dancing"

    .line 33
    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, "diving"

    .line 34
    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, "elevator"

    .line 35
    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "elliptical"

    .line 36
    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "ergometer"

    .line 37
    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "escalator"

    .line 38
    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "exiting_vehicle"

    .line 39
    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "fencing"

    .line 40
    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, "flossing"

    .line 41
    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "football.american"

    .line 42
    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "football.australian"

    .line 43
    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "football.soccer"

    .line 44
    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "frisbee_disc"

    .line 45
    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "gardening"

    .line 46
    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "golf"

    .line 47
    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "gymnastics"

    .line 48
    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "handball"

    .line 49
    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, "interval_training.high_intensity"

    .line 50
    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "hiking"

    .line 51
    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "hockey"

    .line 52
    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "horseback_riding"

    .line 53
    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "housework"

    .line 54
    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, "ice_skating"

    .line 55
    aput-object v2, v0, v1

    const/4 v1, 0x0

    const-string v2, "in_vehicle"

    .line 56
    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, "interval_training"

    .line 57
    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "jump_rope"

    .line 58
    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "kayaking"

    .line 59
    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "kettlebell_training"

    .line 60
    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, "kick_scooter"

    .line 61
    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "kickboxing"

    .line 62
    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "kitesurfing"

    .line 63
    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "martial_arts"

    .line 64
    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "meditation"

    .line 65
    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "martial_arts.mixed"

    .line 66
    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "on_foot"

    .line 67
    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "other"

    .line 68
    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "p90x"

    .line 69
    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "paragliding"

    .line 70
    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "pilates"

    .line 71
    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "polo"

    .line 72
    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "racquetball"

    .line 73
    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "rock_climbing"

    .line 74
    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "rowing"

    .line 75
    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "rowing.machine"

    .line 76
    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "rugby"

    .line 77
    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "running"

    .line 78
    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "running.jogging"

    .line 79
    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "running.sand"

    .line 80
    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "running.treadmill"

    .line 81
    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "sailing"

    .line 82
    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "scuba_diving"

    .line 83
    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "skateboarding"

    .line 84
    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "skating"

    .line 85
    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "skating.cross"

    .line 86
    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "skating.indoor"

    .line 87
    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "skating.inline"

    .line 88
    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "skiing"

    .line 89
    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "skiing.back_country"

    .line 90
    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "skiing.cross_country"

    .line 91
    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "skiing.downhill"

    .line 92
    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "skiing.kite"

    .line 93
    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "skiing.roller"

    .line 94
    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "sledding"

    .line 95
    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "sleep"

    .line 96
    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, "sleep.light"

    .line 97
    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, "sleep.deep"

    .line 98
    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, "sleep.rem"

    .line 99
    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "sleep.awake"

    .line 100
    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "snowboarding"

    .line 101
    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "snowmobile"

    .line 102
    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "snowshoeing"

    .line 103
    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, "softball"

    .line 104
    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "squash"

    .line 105
    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "stair_climbing"

    .line 106
    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "stair_climbing.machine"

    .line 107
    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "standup_paddleboarding"

    .line 108
    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "still"

    .line 109
    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "strength_training"

    .line 110
    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "surfing"

    .line 111
    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "swimming"

    .line 112
    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "swimming.pool"

    .line 113
    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "swimming.open_water"

    .line 114
    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, "table_tennis"

    .line 115
    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "team_sports"

    .line 116
    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "tennis"

    .line 117
    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "tilting"

    .line 118
    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "treadmill"

    .line 119
    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "unknown"

    .line 120
    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "volleyball"

    .line 121
    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "volleyball.beach"

    .line 122
    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "volleyball.indoor"

    .line 123
    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "wakeboarding"

    .line 124
    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "walking"

    .line 125
    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "walking.fitness"

    .line 126
    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "walking.nordic"

    .line 127
    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, "walking.treadmill"

    .line 128
    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "walking.stroller"

    .line 129
    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, "water_polo"

    .line 130
    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "weightlifting"

    .line 131
    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, "wheelchair"

    .line 132
    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "windsurfing"

    .line 133
    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "yoga"

    .line 134
    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, "zumba"

    .line 135
    aput-object v2, v0, v1

    return-void
.end method

.method public static getMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 12
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const-string v1, "vnd.google.fitness.activity/"

    if-eqz v0, :cond_0

    invoke-virtual {v1, p0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    return-object p0
.end method

.method public static getName(I)Ljava/lang/String;
    .locals 3

    const-string v0, "unknown"

    if-ltz p0, :cond_2

    .line 6
    sget-object v1, Lcom/google/android/gms/internal/fitness/zzfa;->zzjb:[Ljava/lang/String;

    array-length v2, v1

    if-lt p0, v2, :cond_0

    goto :goto_0

    .line 8
    :cond_0
    aget-object p0, v1, p0

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    return-object p0

    :cond_2
    :goto_0
    return-object v0
.end method

.method public static zzl(Ljava/lang/String;)I
    .locals 3

    const/4 v0, 0x0

    .line 1
    :goto_0
    sget-object v1, Lcom/google/android/gms/internal/fitness/zzfa;->zzjb:[Ljava/lang/String;

    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 2
    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x4

    return p0
.end method
