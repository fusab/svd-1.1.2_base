.class public interface abstract Lcom/google/android/gms/internal/fitness/zzh;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract zzb(Ljava/lang/String;)Z
.end method

.method public abstract zzc(Ljava/lang/Object;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDT;)I"
        }
    .end annotation
.end method

.method public abstract zzd(Ljava/lang/Object;I)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDT;I)I"
        }
    .end annotation
.end method

.method public abstract zzd(Ljava/lang/Object;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDT;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method public abstract zze(Ljava/lang/Object;I)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDT;I)Z"
        }
    .end annotation
.end method

.method public abstract zzf(Ljava/lang/Object;I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDT;I)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method
