.class public final Lcom/google/android/gms/wallet/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/wallet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final appTheme:I = 0x7f04002f

.field public static final buyButtonAppearance:I = 0x7f040064

.field public static final buyButtonHeight:I = 0x7f040065

.field public static final buyButtonText:I = 0x7f040066

.field public static final buyButtonWidth:I = 0x7f040067

.field public static final customThemeStyle:I = 0x7f0400eb

.field public static final environment:I = 0x7f040100

.field public static final fragmentMode:I = 0x7f040129

.field public static final fragmentStyle:I = 0x7f04012a

.field public static final maskedWalletDetailsBackground:I = 0x7f04018c

.field public static final maskedWalletDetailsButtonBackground:I = 0x7f04018d

.field public static final maskedWalletDetailsButtonTextAppearance:I = 0x7f04018e

.field public static final maskedWalletDetailsHeaderTextAppearance:I = 0x7f04018f

.field public static final maskedWalletDetailsLogoImageType:I = 0x7f040190

.field public static final maskedWalletDetailsLogoTextColor:I = 0x7f040191

.field public static final maskedWalletDetailsTextAppearance:I = 0x7f040192

.field public static final toolbarTextColorStyle:I = 0x7f040264

.field public static final windowTransitionStyle:I = 0x7f040287


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
