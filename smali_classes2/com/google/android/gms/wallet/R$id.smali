.class public final Lcom/google/android/gms/wallet/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/wallet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final android_pay:I = 0x7f090032

.field public static final android_pay_dark:I = 0x7f090033

.field public static final android_pay_light:I = 0x7f090034

.field public static final android_pay_light_with_border:I = 0x7f090035

.field public static final book_now:I = 0x7f09003e

.field public static final buyButton:I = 0x7f09004a

.field public static final buy_now:I = 0x7f09004b

.field public static final buy_with:I = 0x7f09004c

.field public static final buy_with_google:I = 0x7f09004d

.field public static final classic:I = 0x7f090060

.field public static final dark:I = 0x7f09008b

.field public static final donate_with:I = 0x7f090096

.field public static final donate_with_google:I = 0x7f090097

.field public static final googleMaterial2:I = 0x7f0900d7

.field public static final google_wallet_classic:I = 0x7f0900d8

.field public static final google_wallet_grayscale:I = 0x7f0900d9

.field public static final google_wallet_monochrome:I = 0x7f0900da

.field public static final grayscale:I = 0x7f0900db

.field public static final holo_dark:I = 0x7f0900dd

.field public static final holo_light:I = 0x7f0900de

.field public static final light:I = 0x7f090102

.field public static final logo_only:I = 0x7f09010a

.field public static final match_parent:I = 0x7f09010e

.field public static final material:I = 0x7f09010f

.field public static final monochrome:I = 0x7f090115

.field public static final none:I = 0x7f09011d

.field public static final production:I = 0x7f090136

.field public static final sandbox:I = 0x7f090157

.field public static final selectionDetails:I = 0x7f090173

.field public static final slide:I = 0x7f09017b

.field public static final strict_sandbox:I = 0x7f09018f

.field public static final test:I = 0x7f09019b

.field public static final wrap_content:I = 0x7f0901c5


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
