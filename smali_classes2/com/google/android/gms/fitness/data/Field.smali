.class public final Lcom/google/android/gms/fitness/data/Field;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;


# annotations
.annotation build Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable$Class;
    creator = "FieldCreator"
.end annotation

.annotation build Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable$Reserved;
    value = {
        0x3e8
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/fitness/data/Field$zza;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/google/android/gms/fitness/data/Field;",
            ">;"
        }
    .end annotation
.end field

.field public static final FIELD_ACCURACY:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_ACTIVITY:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_ACTIVITY_CONFIDENCE:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_ALTITUDE:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_AVERAGE:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_BPM:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_CALORIES:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_CIRCUMFERENCE:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_CONFIDENCE:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_DISTANCE:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_DURATION:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_EXERCISE:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_FOOD_ITEM:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_HEIGHT:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_HIGH_LATITUDE:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_HIGH_LONGITUDE:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_INTENSITY:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_LATITUDE:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_LONGITUDE:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_LOW_LATITUDE:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_LOW_LONGITUDE:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_MAX:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_MEAL_TYPE:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_MIN:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_NUM_SEGMENTS:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_NUTRIENTS:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_OCCURRENCES:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_PERCENTAGE:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_REPETITIONS:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_RESISTANCE:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_RESISTANCE_TYPE:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_REVOLUTIONS:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_RPM:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_SPEED:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_STEPS:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_STEP_LENGTH:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_VOLUME:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_WATTS:Lcom/google/android/gms/fitness/data/Field;

.field public static final FIELD_WEIGHT:Lcom/google/android/gms/fitness/data/Field;

.field public static final FORMAT_FLOAT:I = 0x2

.field public static final FORMAT_INT32:I = 0x1

.field public static final FORMAT_MAP:I = 0x4

.field public static final FORMAT_STRING:I = 0x3

.field public static final MEAL_TYPE_BREAKFAST:I = 0x1

.field public static final MEAL_TYPE_DINNER:I = 0x3

.field public static final MEAL_TYPE_LUNCH:I = 0x2

.field public static final MEAL_TYPE_SNACK:I = 0x4

.field public static final MEAL_TYPE_UNKNOWN:I = 0x0

.field public static final NUTRIENT_CALCIUM:Ljava/lang/String; = "calcium"

.field public static final NUTRIENT_CALORIES:Ljava/lang/String; = "calories"

.field public static final NUTRIENT_CHOLESTEROL:Ljava/lang/String; = "cholesterol"

.field public static final NUTRIENT_DIETARY_FIBER:Ljava/lang/String; = "dietary_fiber"

.field public static final NUTRIENT_IRON:Ljava/lang/String; = "iron"

.field public static final NUTRIENT_MONOUNSATURATED_FAT:Ljava/lang/String; = "fat.monounsaturated"

.field public static final NUTRIENT_POLYUNSATURATED_FAT:Ljava/lang/String; = "fat.polyunsaturated"

.field public static final NUTRIENT_POTASSIUM:Ljava/lang/String; = "potassium"

.field public static final NUTRIENT_PROTEIN:Ljava/lang/String; = "protein"

.field public static final NUTRIENT_SATURATED_FAT:Ljava/lang/String; = "fat.saturated"

.field public static final NUTRIENT_SODIUM:Ljava/lang/String; = "sodium"

.field public static final NUTRIENT_SUGAR:Ljava/lang/String; = "sugar"

.field public static final NUTRIENT_TOTAL_CARBS:Ljava/lang/String; = "carbs.total"

.field public static final NUTRIENT_TOTAL_FAT:Ljava/lang/String; = "fat.total"

.field public static final NUTRIENT_TRANS_FAT:Ljava/lang/String; = "fat.trans"

.field public static final NUTRIENT_UNSATURATED_FAT:Ljava/lang/String; = "fat.unsaturated"

.field public static final NUTRIENT_VITAMIN_A:Ljava/lang/String; = "vitamin_a"

.field public static final NUTRIENT_VITAMIN_C:Ljava/lang/String; = "vitamin_c"

.field public static final RESISTANCE_TYPE_BARBELL:I = 0x1

.field public static final RESISTANCE_TYPE_BODY:I = 0x6

.field public static final RESISTANCE_TYPE_CABLE:I = 0x2

.field public static final RESISTANCE_TYPE_DUMBBELL:I = 0x3

.field public static final RESISTANCE_TYPE_KETTLEBELL:I = 0x4

.field public static final RESISTANCE_TYPE_MACHINE:I = 0x5

.field public static final RESISTANCE_TYPE_UNKNOWN:I

.field private static final zzcg:Lcom/google/android/gms/fitness/data/Field;

.field private static final zzch:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzci:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzcj:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzck:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzcl:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzcm:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzcn:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzco:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzcp:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzcq:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzcr:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzcs:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzct:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzcu:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzcv:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzcw:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzcx:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzcy:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzcz:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzda:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzdb:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzdc:Lcom/google/android/gms/fitness/data/Field;


# instance fields
.field private final format:I
    .annotation build Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable$Field;
        getter = "getFormat"
        id = 0x2
    .end annotation
.end field

.field private final name:Ljava/lang/String;
    .annotation build Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable$Field;
        getter = "getName"
        id = 0x1
    .end annotation
.end field

.field private final zzdd:Ljava/lang/Boolean;
    .annotation build Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable$Field;
        getter = "isOptional"
        id = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-string v0, "activity"

    .line 144
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzd(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_ACTIVITY:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "confidence"

    .line 146
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_CONFIDENCE:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "activity_confidence"

    .line 148
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzh(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_ACTIVITY_CONFIDENCE:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "steps"

    .line 150
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzd(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_STEPS:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "step_length"

    .line 152
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_STEP_LENGTH:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "duration"

    .line 154
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzd(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v1

    sput-object v1, Lcom/google/android/gms/fitness/data/Field;->FIELD_DURATION:Lcom/google/android/gms/fitness/data/Field;

    .line 156
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zze(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzcg:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "activity_duration"

    .line 158
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzh(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzch:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "activity_duration.ascending"

    .line 160
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzh(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzci:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "activity_duration.descending"

    .line 162
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzh(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzcj:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "bpm"

    .line 164
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_BPM:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "latitude"

    .line 166
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_LATITUDE:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "longitude"

    .line 168
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_LONGITUDE:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "accuracy"

    .line 170
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_ACCURACY:Lcom/google/android/gms/fitness/data/Field;

    .line 173
    new-instance v0, Lcom/google/android/gms/fitness/data/Field;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "altitude"

    const/4 v3, 0x2

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/gms/fitness/data/Field;-><init>(Ljava/lang/String;ILjava/lang/Boolean;)V

    .line 174
    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_ALTITUDE:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "distance"

    .line 176
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_DISTANCE:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "height"

    .line 178
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_HEIGHT:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "weight"

    .line 180
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_WEIGHT:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "circumference"

    .line 182
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_CIRCUMFERENCE:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "percentage"

    .line 184
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_PERCENTAGE:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "speed"

    .line 186
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_SPEED:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "rpm"

    .line 188
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_RPM:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "google.android.fitness.GoalV2"

    .line 190
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzi(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzck:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "prescription_event"

    .line 192
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzi(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzcl:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "symptom"

    .line 194
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzi(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzcm:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "google.android.fitness.StrideModel"

    .line 196
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzi(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzcn:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "google.android.fitness.Device"

    .line 198
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzi(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzco:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "revolutions"

    .line 200
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzd(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_REVOLUTIONS:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "calories"

    .line 202
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_CALORIES:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "watts"

    .line 204
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_WATTS:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "volume"

    .line 206
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_VOLUME:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "meal_type"

    .line 208
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzd(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_MEAL_TYPE:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "food_item"

    .line 210
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzg(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_FOOD_ITEM:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "nutrients"

    .line 212
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzh(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_NUTRIENTS:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "elevation.change"

    .line 214
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzcp:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "elevation.gain"

    .line 216
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzh(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzcq:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "elevation.loss"

    .line 218
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzh(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzcr:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "floors"

    .line 220
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzcs:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "floor.gain"

    .line 222
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzh(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzct:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "floor.loss"

    .line 224
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzh(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzcu:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "exercise"

    .line 226
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzg(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_EXERCISE:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "repetitions"

    .line 228
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzd(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_REPETITIONS:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "resistance"

    .line 230
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_RESISTANCE:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "resistance_type"

    .line 232
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzd(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_RESISTANCE_TYPE:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "num_segments"

    .line 234
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzd(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_NUM_SEGMENTS:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "average"

    .line 236
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_AVERAGE:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "max"

    .line 238
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_MAX:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "min"

    .line 240
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_MIN:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "low_latitude"

    .line 242
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_LOW_LATITUDE:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "low_longitude"

    .line 244
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_LOW_LONGITUDE:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "high_latitude"

    .line 246
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_HIGH_LATITUDE:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "high_longitude"

    .line 248
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_HIGH_LONGITUDE:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "occurrences"

    .line 250
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzd(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_OCCURRENCES:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "sensor_type"

    .line 252
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzd(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzcv:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "sensor_types"

    .line 254
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzd(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzcw:Lcom/google/android/gms/fitness/data/Field;

    .line 257
    new-instance v0, Lcom/google/android/gms/fitness/data/Field;

    const-string v1, "timestamps"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/fitness/data/Field;-><init>(Ljava/lang/String;I)V

    .line 258
    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzcx:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "sample_period"

    .line 260
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzd(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzcy:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "num_samples"

    .line 262
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzd(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzcz:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "num_dimensions"

    .line 264
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzd(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzda:Lcom/google/android/gms/fitness/data/Field;

    .line 267
    new-instance v0, Lcom/google/android/gms/fitness/data/Field;

    const-string v1, "sensor_values"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/fitness/data/Field;-><init>(Ljava/lang/String;I)V

    .line 268
    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzdb:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "intensity"

    .line 270
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->FIELD_INTENSITY:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "probability"

    .line 272
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->zzdc:Lcom/google/android/gms/fitness/data/Field;

    .line 273
    new-instance v0, Lcom/google/android/gms/fitness/data/zzq;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/data/zzq;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/data/Field;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    .line 112
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/fitness/data/Field;-><init>(Ljava/lang/String;ILjava/lang/Boolean;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ILjava/lang/Boolean;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable$Param;
            id = 0x1
        .end annotation
    .end param
    .param p2    # I
        .annotation build Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable$Param;
            id = 0x2
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation

        .annotation build Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable$Param;
            id = 0x3
        .end annotation
    .end param
    .annotation build Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable$Constructor;
    .end annotation

    .line 114
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    .line 115
    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/fitness/data/Field;->name:Ljava/lang/String;

    .line 116
    iput p2, p0, Lcom/google/android/gms/fitness/data/Field;->format:I

    .line 117
    iput-object p3, p0, Lcom/google/android/gms/fitness/data/Field;->zzdd:Ljava/lang/Boolean;

    return-void
.end method

.method public static zza(Ljava/lang/String;I)Lcom/google/android/gms/fitness/data/Field;
    .locals 2

    .line 2
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v0, "supplemental_oxygen_flow_rate"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x48

    goto/16 :goto_1

    :sswitch_1
    const-string v0, "cervical_firmness"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x19

    goto/16 :goto_1

    :sswitch_2
    const-string v0, "exercise"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x25

    goto/16 :goto_1

    :sswitch_3
    const-string v0, "altitude"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto/16 :goto_1

    :sswitch_4
    const-string v0, "blood_pressure_systolic_average"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x10

    goto/16 :goto_1

    :sswitch_5
    const-string v0, "body_position"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x13

    goto/16 :goto_1

    :sswitch_6
    const-string v0, "supplemental_oxygen_flow_rate_min"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x4b

    goto/16 :goto_1

    :sswitch_7
    const-string v0, "supplemental_oxygen_flow_rate_max"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x4a

    goto/16 :goto_1

    :sswitch_8
    const-string v0, "menstrual_flow"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x34

    goto/16 :goto_1

    :sswitch_9
    const-string v0, "oxygen_therapy_administration_mode"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x42

    goto/16 :goto_1

    :sswitch_a
    const-string v0, "resistance"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x50

    goto/16 :goto_1

    :sswitch_b
    const-string v0, "elevation.loss"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x24

    goto/16 :goto_1

    :sswitch_c
    const-string v0, "elevation.gain"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x23

    goto/16 :goto_1

    :sswitch_d
    const-string v0, "timestamps"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x43

    goto/16 :goto_1

    :sswitch_e
    const-string v0, "sensor_type"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x45

    goto/16 :goto_1

    :sswitch_f
    const-string v0, "blood_pressure_diastolic_min"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_1

    :sswitch_10
    const-string v0, "blood_pressure_diastolic_max"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_1

    :sswitch_11
    const-string v0, "oxygen_saturation_min"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x40

    goto/16 :goto_1

    :sswitch_12
    const-string v0, "oxygen_saturation_max"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3e

    goto/16 :goto_1

    :sswitch_13
    const-string v0, "blood_pressure_diastolic"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xa

    goto/16 :goto_1

    :sswitch_14
    const-string v0, "sample_period"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x44

    goto/16 :goto_1

    :sswitch_15
    const-string v0, "activity_confidence"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto/16 :goto_1

    :sswitch_16
    const-string v0, "repetitions"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x4f

    goto/16 :goto_1

    :sswitch_17
    const-string v0, "body_temperature"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x14

    goto/16 :goto_1

    :sswitch_18
    const-string v0, "temporal_relation_to_meal"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x59

    goto/16 :goto_1

    :sswitch_19
    const-string v0, "confidence"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1e

    goto/16 :goto_1

    :sswitch_1a
    const-string v0, "oxygen_saturation_system"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x41

    goto/16 :goto_1

    :sswitch_1b
    const-string v0, "revolutions"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x52

    goto/16 :goto_1

    :sswitch_1c
    const-string v0, "occurrences"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3a

    goto/16 :goto_1

    :sswitch_1d
    const-string v0, "google.android.fitness.StrideModel"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x58

    goto/16 :goto_1

    :sswitch_1e
    const-string v0, "oxygen_saturation_average"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3d

    goto/16 :goto_1

    :sswitch_1f
    const-string v0, "cervical_mucus_amount"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1a

    goto/16 :goto_1

    :sswitch_20
    const-string v0, "google.android.fitness.GoalV2"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x57

    goto/16 :goto_1

    :sswitch_21
    const-string v0, "intensity"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x2b

    goto/16 :goto_1

    :sswitch_22
    const-string v0, "blood_pressure_systolic_min"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x12

    goto/16 :goto_1

    :sswitch_23
    const-string v0, "blood_pressure_systolic_max"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x11

    goto/16 :goto_1

    :sswitch_24
    const-string v0, "activity_duration.ascending"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto/16 :goto_1

    :sswitch_25
    const-string v0, "google.android.fitness.Device"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1f

    goto/16 :goto_1

    :sswitch_26
    const-string v0, "cervical_mucus_texture"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1b

    goto/16 :goto_1

    :sswitch_27
    const-string v0, "google.android.fitness.SessionV2"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x62

    goto/16 :goto_1

    :sswitch_28
    const-string v0, "prescription_event"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x4c

    goto/16 :goto_1

    :sswitch_29
    const-string v0, "distance"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x20

    goto/16 :goto_1

    :sswitch_2a
    const-string v0, "activity_duration.descending"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto/16 :goto_1

    :sswitch_2b
    const-string v0, "blood_glucose_specimen_source"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    goto/16 :goto_1

    :sswitch_2c
    const-string v0, "blood_pressure_diastolic_average"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xb

    goto/16 :goto_1

    :sswitch_2d
    const-string v0, "low_latitude"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x30

    goto/16 :goto_1

    :sswitch_2e
    const-string v0, "longitude"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x2f

    goto/16 :goto_1

    :sswitch_2f
    const-string v0, "sensor_types"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x46

    goto/16 :goto_1

    :sswitch_30
    const-string v0, "watts"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x5c

    goto/16 :goto_1

    :sswitch_31
    const-string v0, "steps"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x55

    goto/16 :goto_1

    :sswitch_32
    const-string v0, "speed"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x54

    goto/16 :goto_1

    :sswitch_33
    const-string v0, "temporal_relation_to_sleep"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x5a

    goto/16 :goto_1

    :sswitch_34
    const-string v0, "rpm"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x53

    goto/16 :goto_1

    :sswitch_35
    const-string v0, "min"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x35

    goto/16 :goto_1

    :sswitch_36
    const-string v0, "max"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x32

    goto/16 :goto_1

    :sswitch_37
    const-string v0, "bpm"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x16

    goto/16 :goto_1

    :sswitch_38
    const-string v0, "z"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x60

    goto/16 :goto_1

    :sswitch_39
    const-string v0, "y"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x5f

    goto/16 :goto_1

    :sswitch_3a
    const-string v0, "x"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x5e

    goto/16 :goto_1

    :sswitch_3b
    const-string v0, "ovulation_test_result"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3b

    goto/16 :goto_1

    :sswitch_3c
    const-string v0, "resistance_type"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x51

    goto/16 :goto_1

    :sswitch_3d
    const-string v0, "calories"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x17

    goto/16 :goto_1

    :sswitch_3e
    const-string v0, "oxygen_saturation_measurement_method"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3f

    goto/16 :goto_1

    :sswitch_3f
    const-string v0, "nutrients"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x39

    goto/16 :goto_1

    :sswitch_40
    const-string v0, "circumference"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1d

    goto/16 :goto_1

    :sswitch_41
    const-string v0, "meal_type"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x33

    goto/16 :goto_1

    :sswitch_42
    const-string v0, "step_length"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x56

    goto/16 :goto_1

    :sswitch_43
    const-string v0, "high_latitude"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x2c

    goto/16 :goto_1

    :sswitch_44
    const-string v0, "sensor_values"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x47

    goto/16 :goto_1

    :sswitch_45
    const-string v0, "low_longitude"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x31

    goto/16 :goto_1

    :sswitch_46
    const-string v0, "high_longitude"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x2d

    goto/16 :goto_1

    :sswitch_47
    const-string v0, "average"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    goto/16 :goto_1

    :sswitch_48
    const-string v0, "weight"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x5d

    goto/16 :goto_1

    :sswitch_49
    const-string v0, "blood_pressure_systolic"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xf

    goto/16 :goto_1

    :sswitch_4a
    const-string v0, "volume"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x5b

    goto/16 :goto_1

    :sswitch_4b
    const-string v0, "cervical_position"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1c

    goto/16 :goto_1

    :sswitch_4c
    const-string v0, "percentage"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x4e

    goto/16 :goto_1

    :sswitch_4d
    const-string v0, "food_item"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x29

    goto/16 :goto_1

    :sswitch_4e
    const-string v0, "num_samples"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x37

    goto/16 :goto_1

    :sswitch_4f
    const-string v0, "activity_duration"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto/16 :goto_1

    :sswitch_50
    const-string v0, "blood_pressure_measurement_location"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xe

    goto/16 :goto_1

    :sswitch_51
    const-string v0, "height"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x2a

    goto/16 :goto_1

    :sswitch_52
    const-string v0, "supplemental_oxygen_flow_rate_average"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x49

    goto/16 :goto_1

    :sswitch_53
    const-string v0, "floors"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x28

    goto/16 :goto_1

    :sswitch_54
    const-string v0, "probability"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x63

    goto/16 :goto_1

    :sswitch_55
    const-string v0, "num_dimensions"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x36

    goto/16 :goto_1

    :sswitch_56
    const-string v0, "latitude"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x2e

    goto/16 :goto_1

    :sswitch_57
    const-string v0, "oxygen_saturation"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3c

    goto/16 :goto_1

    :sswitch_58
    const-string v0, "elevation.change"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x22

    goto/16 :goto_1

    :sswitch_59
    const-string v0, "num_segments"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x38

    goto/16 :goto_1

    :sswitch_5a
    const-string v0, "floor.loss"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x27

    goto/16 :goto_1

    :sswitch_5b
    const-string v0, "floor.gain"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x26

    goto :goto_1

    :sswitch_5c
    const-string v0, "cervical_dilation"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x18

    goto :goto_1

    :sswitch_5d
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_5e
    const-string v0, "symptom"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x4d

    goto :goto_1

    :sswitch_5f
    const-string v0, "blood_glucose_level"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    goto :goto_1

    :sswitch_60
    const-string v0, "duration"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x21

    goto :goto_1

    :sswitch_61
    const-string v0, "body_temperature_measurement_location"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x15

    goto :goto_1

    :sswitch_62
    const-string v0, "debug_session"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x61

    goto :goto_1

    :sswitch_63
    const-string v0, "accuracy"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :cond_0
    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 103
    new-instance v0, Lcom/google/android/gms/fitness/data/Field;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/fitness/data/Field;-><init>(Ljava/lang/String;ILjava/lang/Boolean;)V

    return-object v0

    .line 102
    :pswitch_0
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzdc:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 101
    :pswitch_1
    sget-object p0, Lcom/google/android/gms/fitness/data/Field$zza;->zzdi:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 100
    :pswitch_2
    sget-object p0, Lcom/google/android/gms/fitness/data/Field$zza;->zzdh:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 99
    :pswitch_3
    sget-object p0, Lcom/google/android/gms/fitness/data/Field$zza;->zzdg:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 98
    :pswitch_4
    sget-object p0, Lcom/google/android/gms/fitness/data/Field$zza;->zzdf:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 97
    :pswitch_5
    sget-object p0, Lcom/google/android/gms/fitness/data/Field$zza;->zzde:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 96
    :pswitch_6
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_WEIGHT:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 95
    :pswitch_7
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_WATTS:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 94
    :pswitch_8
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_VOLUME:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 93
    :pswitch_9
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_TEMPORAL_RELATION_TO_SLEEP:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 92
    :pswitch_a
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_TEMPORAL_RELATION_TO_MEAL:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 91
    :pswitch_b
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzcn:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 90
    :pswitch_c
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzck:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 89
    :pswitch_d
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_STEP_LENGTH:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 88
    :pswitch_e
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_STEPS:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 87
    :pswitch_f
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_SPEED:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 86
    :pswitch_10
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_RPM:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 85
    :pswitch_11
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_REVOLUTIONS:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 84
    :pswitch_12
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_RESISTANCE_TYPE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 83
    :pswitch_13
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_RESISTANCE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 82
    :pswitch_14
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_REPETITIONS:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 81
    :pswitch_15
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_PERCENTAGE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 80
    :pswitch_16
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzcm:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 79
    :pswitch_17
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzcl:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 78
    :pswitch_18
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_SUPPLEMENTAL_OXYGEN_FLOW_RATE_MIN:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 77
    :pswitch_19
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_SUPPLEMENTAL_OXYGEN_FLOW_RATE_MAX:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 76
    :pswitch_1a
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_SUPPLEMENTAL_OXYGEN_FLOW_RATE_AVERAGE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 75
    :pswitch_1b
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_SUPPLEMENTAL_OXYGEN_FLOW_RATE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 74
    :pswitch_1c
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzdb:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 73
    :pswitch_1d
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzcw:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 72
    :pswitch_1e
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzcv:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 71
    :pswitch_1f
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzcy:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 70
    :pswitch_20
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzcx:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 69
    :pswitch_21
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_OXYGEN_THERAPY_ADMINISTRATION_MODE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 68
    :pswitch_22
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_OXYGEN_SATURATION_SYSTEM:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 67
    :pswitch_23
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_OXYGEN_SATURATION_MIN:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 66
    :pswitch_24
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_OXYGEN_SATURATION_MEASUREMENT_METHOD:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 65
    :pswitch_25
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_OXYGEN_SATURATION_MAX:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 64
    :pswitch_26
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_OXYGEN_SATURATION_AVERAGE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 63
    :pswitch_27
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_OXYGEN_SATURATION:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 62
    :pswitch_28
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_OVULATION_TEST_RESULT:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 61
    :pswitch_29
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_OCCURRENCES:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 60
    :pswitch_2a
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_NUTRIENTS:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 59
    :pswitch_2b
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_NUM_SEGMENTS:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 58
    :pswitch_2c
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzcz:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 57
    :pswitch_2d
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzda:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 56
    :pswitch_2e
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_MIN:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 55
    :pswitch_2f
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_MENSTRUAL_FLOW:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 54
    :pswitch_30
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_MEAL_TYPE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 53
    :pswitch_31
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_MAX:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 52
    :pswitch_32
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_LOW_LONGITUDE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 51
    :pswitch_33
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_LOW_LATITUDE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 50
    :pswitch_34
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_LONGITUDE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 49
    :pswitch_35
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_LATITUDE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 48
    :pswitch_36
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_HIGH_LONGITUDE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 47
    :pswitch_37
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_HIGH_LATITUDE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 46
    :pswitch_38
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_INTENSITY:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 45
    :pswitch_39
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_HEIGHT:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 44
    :pswitch_3a
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_FOOD_ITEM:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 43
    :pswitch_3b
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzcs:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 42
    :pswitch_3c
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzcu:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 41
    :pswitch_3d
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzct:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 40
    :pswitch_3e
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_EXERCISE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 39
    :pswitch_3f
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzcr:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 38
    :pswitch_40
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzcq:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 37
    :pswitch_41
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzcp:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 36
    :pswitch_42
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_DURATION:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 35
    :pswitch_43
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_DISTANCE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 34
    :pswitch_44
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzco:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 33
    :pswitch_45
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_CONFIDENCE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 32
    :pswitch_46
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_CIRCUMFERENCE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 31
    :pswitch_47
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_CERVICAL_POSITION:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 30
    :pswitch_48
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_CERVICAL_MUCUS_TEXTURE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 29
    :pswitch_49
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_CERVICAL_MUCUS_AMOUNT:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 28
    :pswitch_4a
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_CERVICAL_FIRMNESS:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 27
    :pswitch_4b
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_CERVICAL_DILATION:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 26
    :pswitch_4c
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_CALORIES:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 25
    :pswitch_4d
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_BPM:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 24
    :pswitch_4e
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_BODY_TEMPERATURE_MEASUREMENT_LOCATION:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 23
    :pswitch_4f
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_BODY_TEMPERATURE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 22
    :pswitch_50
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_BODY_POSITION:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 21
    :pswitch_51
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_BLOOD_PRESSURE_SYSTOLIC_MIN:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 20
    :pswitch_52
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_BLOOD_PRESSURE_SYSTOLIC_MAX:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 19
    :pswitch_53
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_BLOOD_PRESSURE_SYSTOLIC_AVERAGE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 18
    :pswitch_54
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_BLOOD_PRESSURE_SYSTOLIC:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 17
    :pswitch_55
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_BLOOD_PRESSURE_MEASUREMENT_LOCATION:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 16
    :pswitch_56
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_BLOOD_PRESSURE_DIASTOLIC_MIN:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 15
    :pswitch_57
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_BLOOD_PRESSURE_DIASTOLIC_MAX:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 14
    :pswitch_58
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_BLOOD_PRESSURE_DIASTOLIC_AVERAGE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 13
    :pswitch_59
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_BLOOD_PRESSURE_DIASTOLIC:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 12
    :pswitch_5a
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_BLOOD_GLUCOSE_SPECIMEN_SOURCE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 11
    :pswitch_5b
    sget-object p0, Lcom/google/android/gms/fitness/data/HealthFields;->FIELD_BLOOD_GLUCOSE_LEVEL:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 10
    :pswitch_5c
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_AVERAGE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 9
    :pswitch_5d
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_ALTITUDE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 8
    :pswitch_5e
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzcj:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 7
    :pswitch_5f
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzci:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 6
    :pswitch_60
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->zzch:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 5
    :pswitch_61
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_ACTIVITY_CONFIDENCE:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 4
    :pswitch_62
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_ACTIVITY:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    .line 3
    :pswitch_63
    sget-object p0, Lcom/google/android/gms/fitness/data/Field;->FIELD_ACCURACY:Lcom/google/android/gms/fitness/data/Field;

    return-object p0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7f0f4707 -> :sswitch_63
        -0x7c354356 -> :sswitch_62
        -0x7796ca40 -> :sswitch_61
        -0x76bbb26c -> :sswitch_60
        -0x6ed4e992 -> :sswitch_5f
        -0x67e451d7 -> :sswitch_5e
        -0x62b40cf1 -> :sswitch_5d
        -0x5f1ca55e -> :sswitch_5c
        -0x5e26f7df -> :sswitch_5b
        -0x5e247c3b -> :sswitch_5a
        -0x5d8b9bc7 -> :sswitch_59
        -0x5b49e79f -> :sswitch_58
        -0x55df742f -> :sswitch_57
        -0x55d45394 -> :sswitch_56
        -0x509d65da -> :sswitch_55
        -0x4cec67cb -> :sswitch_54
        -0x4bcba219 -> :sswitch_53
        -0x4a6c0e75 -> :sswitch_52
        -0x48c76ed9 -> :sswitch_51
        -0x48c640f3 -> :sswitch_50
        -0x4393733c -> :sswitch_4f
        -0x435053b0 -> :sswitch_4e
        -0x4234cdac -> :sswitch_4d
        -0x36f20d66 -> :sswitch_4c
        -0x36c67f03 -> :sswitch_4b
        -0x305518e6 -> :sswitch_4a
        -0x2fe08acd -> :sswitch_49
        -0x2f2ebd88 -> :sswitch_48
        -0x25a321e3 -> :sswitch_47
        -0x255540ae -> :sswitch_46
        -0x24f2717c -> :sswitch_45
        -0x1e837eb9 -> :sswitch_44
        -0x1d7dc997 -> :sswitch_43
        -0x1afacc47 -> :sswitch_42
        -0x1a0ce9ca -> :sswitch_41
        -0x10875bf1 -> :sswitch_40
        -0xfdc4294 -> :sswitch_3f
        -0xd9c9a0e -> :sswitch_3e
        -0xa1234fa -> :sswitch_3d
        -0x78ad480 -> :sswitch_3c
        -0x1b440de -> :sswitch_3b
        0x78 -> :sswitch_3a
        0x79 -> :sswitch_39
        0x7a -> :sswitch_38
        0x17ddf -> :sswitch_37
        0x1a564 -> :sswitch_36
        0x1a652 -> :sswitch_35
        0x1b9ef -> :sswitch_34
        0x3f8d719 -> :sswitch_33
        0x6890047 -> :sswitch_32
        0x68ad327 -> :sswitch_31
        0x6bac6e9 -> :sswitch_30
        0x734dbb4 -> :sswitch_2f
        0x83009af -> :sswitch_2e
        0xbcfb8f7 -> :sswitch_2d
        0xd26d3dd -> :sswitch_2c
        0xed5c79c -> :sswitch_2b
        0x11155a62 -> :sswitch_2a
        0x11318bf5 -> :sswitch_29
        0x11697e35 -> :sswitch_28
        0x124659d8 -> :sswitch_27
        0x131c6321 -> :sswitch_26
        0x1903a5f0 -> :sswitch_25
        0x1b2d7a2e -> :sswitch_24
        0x1c587858 -> :sswitch_23
        0x1c587946 -> :sswitch_22
        0x1dc31833 -> :sswitch_21
        0x1ea59889 -> :sswitch_20
        0x22aee992 -> :sswitch_1f
        0x2530afaf -> :sswitch_1e
        0x2c003476 -> :sswitch_1d
        0x2ec250c2 -> :sswitch_1c
        0x305aea4a -> :sswitch_1b
        0x309f265d -> :sswitch_1a
        0x316d5e8a -> :sswitch_19
        0x31aa5b41 -> :sswitch_18
        0x34a3fa57 -> :sswitch_17
        0x3aac4222 -> :sswitch_16
        0x3b8291ba -> :sswitch_15
        0x43b629f6 -> :sswitch_14
        0x4c1cbdff -> :sswitch_13
        0x4c910ff6 -> :sswitch_12
        0x4c9110e4 -> :sswitch_11
        0x53ac7b24 -> :sswitch_10
        0x53ac7c12 -> :sswitch_f
        0x5b12389f -> :sswitch_e
        0x65dbfa1d -> :sswitch_d
        0x6ebac870 -> :sswitch_c
        0x6ebd4414 -> :sswitch_b
        0x6f175839 -> :sswitch_a
        0x701c35ef -> :sswitch_9
        0x70ce8848 -> :sswitch_8
        0x74b79fd2 -> :sswitch_7
        0x74b7a0c0 -> :sswitch_6
        0x76334726 -> :sswitch_5
        0x78691711 -> :sswitch_4
        0x79634aa2 -> :sswitch_3
        0x7a9101d8 -> :sswitch_2
        0x7b891969 -> :sswitch_1
        0x7be169ad -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_63
        :pswitch_62
        :pswitch_61
        :pswitch_60
        :pswitch_5f
        :pswitch_5e
        :pswitch_5d
        :pswitch_5c
        :pswitch_5b
        :pswitch_5a
        :pswitch_59
        :pswitch_58
        :pswitch_57
        :pswitch_56
        :pswitch_55
        :pswitch_54
        :pswitch_53
        :pswitch_52
        :pswitch_51
        :pswitch_50
        :pswitch_4f
        :pswitch_4e
        :pswitch_4d
        :pswitch_4c
        :pswitch_4b
        :pswitch_4a
        :pswitch_49
        :pswitch_48
        :pswitch_47
        :pswitch_46
        :pswitch_45
        :pswitch_44
        :pswitch_43
        :pswitch_42
        :pswitch_41
        :pswitch_40
        :pswitch_3f
        :pswitch_3e
        :pswitch_3d
        :pswitch_3c
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static zzd(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;
    .locals 2

    .line 105
    new-instance v0, Lcom/google/android/gms/fitness/data/Field;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/fitness/data/Field;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method static zze(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;
    .locals 3

    .line 106
    new-instance v0, Lcom/google/android/gms/fitness/data/Field;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/fitness/data/Field;-><init>(Ljava/lang/String;ILjava/lang/Boolean;)V

    return-object v0
.end method

.method static zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;
    .locals 2

    .line 107
    new-instance v0, Lcom/google/android/gms/fitness/data/Field;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/fitness/data/Field;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method private static zzg(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;
    .locals 2

    .line 108
    new-instance v0, Lcom/google/android/gms/fitness/data/Field;

    const/4 v1, 0x3

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/fitness/data/Field;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method private static zzh(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;
    .locals 2

    .line 109
    new-instance v0, Lcom/google/android/gms/fitness/data/Field;

    const/4 v1, 0x4

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/fitness/data/Field;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method private static zzi(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;
    .locals 2

    .line 110
    new-instance v0, Lcom/google/android/gms/fitness/data/Field;

    const/4 v1, 0x7

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/fitness/data/Field;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method static zzj(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;
    .locals 3

    .line 111
    new-instance v0, Lcom/google/android/gms/fitness/data/Field;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x7

    invoke-direct {v0, p0, v2, v1}, Lcom/google/android/gms/fitness/data/Field;-><init>(Ljava/lang/String;ILjava/lang/Boolean;)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 124
    :cond_0
    instance-of v1, p1, Lcom/google/android/gms/fitness/data/Field;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 126
    :cond_1
    check-cast p1, Lcom/google/android/gms/fitness/data/Field;

    .line 127
    iget-object v1, p0, Lcom/google/android/gms/fitness/data/Field;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/fitness/data/Field;->name:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/google/android/gms/fitness/data/Field;->format:I

    iget p1, p1, Lcom/google/android/gms/fitness/data/Field;->format:I

    if-ne v1, p1, :cond_2

    return v0

    :cond_2
    return v2
.end method

.method public final getFormat()I
    .locals 1

    .line 120
    iget v0, p0, Lcom/google/android/gms/fitness/data/Field;->format:I

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/fitness/data/Field;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/fitness/data/Field;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final isOptional()Ljava/lang/Boolean;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/fitness/data/Field;->zzdd:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x2

    .line 129
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/gms/fitness/data/Field;->name:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget v1, p0, Lcom/google/android/gms/fitness/data/Field;->format:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const-string v1, "i"

    goto :goto_0

    :cond_0
    const-string v1, "f"

    :goto_0
    aput-object v1, v0, v2

    const-string v1, "%s(%s)"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .line 131
    invoke-static {p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginObjectHeader(Landroid/os/Parcel;)I

    move-result p2

    .line 133
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/Field;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 134
    invoke-static {p1, v2, v0, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeString(Landroid/os/Parcel;ILjava/lang/String;Z)V

    .line 136
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/Field;->getFormat()I

    move-result v0

    const/4 v2, 0x2

    .line 137
    invoke-static {p1, v2, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeInt(Landroid/os/Parcel;II)V

    .line 139
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/Field;->isOptional()Ljava/lang/Boolean;

    move-result-object v0

    const/4 v2, 0x3

    .line 140
    invoke-static {p1, v2, v0, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeBooleanObject(Landroid/os/Parcel;ILjava/lang/Boolean;Z)V

    .line 141
    invoke-static {p1, p2}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishObjectHeader(Landroid/os/Parcel;I)V

    return-void
.end method
