.class public final Lcom/google/android/gms/fitness/data/Field$zza;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/fitness/data/Field;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "zza"
.end annotation


# static fields
.field public static final zzde:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzdf:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzdg:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzdh:Lcom/google/android/gms/fitness/data/Field;

.field public static final zzdi:Lcom/google/android/gms/fitness/data/Field;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "x"

    .line 2
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field$zza;->zzde:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "y"

    .line 4
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field$zza;->zzdf:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "z"

    .line 6
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzf(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field$zza;->zzdg:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "debug_session"

    .line 8
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzj(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field$zza;->zzdh:Lcom/google/android/gms/fitness/data/Field;

    const-string v0, "google.android.fitness.SessionV2"

    .line 10
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Field;->zzj(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/data/Field$zza;->zzdi:Lcom/google/android/gms/fitness/data/Field;

    return-void
.end method
