.class public final Lcom/google/maps/android/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/maps/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final ambientEnabled:I = 0x7f04002e

.field public static final buttonSize:I = 0x7f04005f

.field public static final cameraBearing:I = 0x7f040068

.field public static final cameraMaxZoomPreference:I = 0x7f040069

.field public static final cameraMinZoomPreference:I = 0x7f04006a

.field public static final cameraTargetLat:I = 0x7f04006b

.field public static final cameraTargetLng:I = 0x7f04006c

.field public static final cameraTilt:I = 0x7f04006d

.field public static final cameraZoom:I = 0x7f04006e

.field public static final circleCrop:I = 0x7f04008e

.field public static final colorScheme:I = 0x7f0400a5

.field public static final imageAspectRatio:I = 0x7f040146

.field public static final imageAspectRatioAdjust:I = 0x7f040147

.field public static final latLngBoundsNorthEastLatitude:I = 0x7f04015e

.field public static final latLngBoundsNorthEastLongitude:I = 0x7f04015f

.field public static final latLngBoundsSouthWestLatitude:I = 0x7f040160

.field public static final latLngBoundsSouthWestLongitude:I = 0x7f040161

.field public static final liteMode:I = 0x7f04017c

.field public static final mapType:I = 0x7f04018b

.field public static final scopeUris:I = 0x7f0401de

.field public static final uiCompass:I = 0x7f04026d

.field public static final uiMapToolbar:I = 0x7f04026e

.field public static final uiRotateGestures:I = 0x7f04026f

.field public static final uiScrollGestures:I = 0x7f040270

.field public static final uiTiltGestures:I = 0x7f040272

.field public static final uiZoomControls:I = 0x7f040273

.field public static final uiZoomGestures:I = 0x7f040274

.field public static final useViewLifecycle:I = 0x7f040277

.field public static final zOrderOnTop:I = 0x7f040288


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
