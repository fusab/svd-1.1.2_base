.class public Lcom/learnium/RNDeviceInfo/RNDeviceInfo;
.super Ljava/lang/Object;
.source "RNDeviceInfo.java"

# interfaces
.implements Lcom/facebook/react/ReactPackage;


# instance fields
.field private mLoadConstantsAsynchronously:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0, v0}, Lcom/learnium/RNDeviceInfo/RNDeviceInfo;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 14
    iput-boolean v0, p0, Lcom/learnium/RNDeviceInfo/RNDeviceInfo;->mLoadConstantsAsynchronously:Z

    .line 21
    iput-boolean p1, p0, Lcom/learnium/RNDeviceInfo/RNDeviceInfo;->mLoadConstantsAsynchronously:Z

    return-void
.end method


# virtual methods
.method public createJSModules()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Class<",
            "+",
            "Lcom/facebook/react/bridge/JavaScriptModule;",
            ">;>;"
        }
    .end annotation

    .line 35
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public createNativeModules(Lcom/facebook/react/bridge/ReactApplicationContext;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/react/bridge/ReactApplicationContext;",
            ")",
            "Ljava/util/List<",
            "Lcom/facebook/react/bridge/NativeModule;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 28
    new-instance v1, Lcom/learnium/RNDeviceInfo/RNDeviceModule;

    iget-boolean v2, p0, Lcom/learnium/RNDeviceInfo/RNDeviceInfo;->mLoadConstantsAsynchronously:Z

    invoke-direct {v1, p1, v2}, Lcom/learnium/RNDeviceInfo/RNDeviceModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public createViewManagers(Lcom/facebook/react/bridge/ReactApplicationContext;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/react/bridge/ReactApplicationContext;",
            ")",
            "Ljava/util/List<",
            "Lcom/facebook/react/uimanager/ViewManager;",
            ">;"
        }
    .end annotation

    .line 41
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
