.class public Lcom/segment/analytics/Properties;
.super Lcom/segment/analytics/ValueMap;
.source "Properties.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/segment/analytics/Properties$Product;
    }
.end annotation


# static fields
.field private static final CATEGORY_KEY:Ljava/lang/String; = "category"

.field private static final COUPON_KEY:Ljava/lang/String; = "coupon"

.field private static final CURRENCY_KEY:Ljava/lang/String; = "currency"

.field private static final DISCOUNT_KEY:Ljava/lang/String; = "discount"

.field private static final ID_KEY:Ljava/lang/String; = "id"

.field private static final NAME_KEY:Ljava/lang/String; = "name"

.field private static final ORDER_ID_KEY:Ljava/lang/String; = "orderId"

.field private static final PATH_KEY:Ljava/lang/String; = "path"

.field private static final PRICE_KEY:Ljava/lang/String; = "price"

.field private static final PRODUCTS_KEY:Ljava/lang/String; = "products"

.field private static final REFERRER_KEY:Ljava/lang/String; = "referrer"

.field private static final REPEAT_KEY:Ljava/lang/String; = "repeat"

.field private static final REVENUE_KEY:Ljava/lang/String; = "revenue"

.field private static final SHIPPING_KEY:Ljava/lang/String; = "shipping"

.field private static final SKU_KEY:Ljava/lang/String; = "sku"

.field private static final SUBTOTAL_KEY:Ljava/lang/String; = "subtotal"

.field private static final TAX_KEY:Ljava/lang/String; = "tax"

.field private static final TITLE_KEY:Ljava/lang/String; = "title"

.field private static final TOTAL_KEY:Ljava/lang/String; = "total"

.field private static final URL_KEY:Ljava/lang/String; = "url"

.field private static final VALUE_KEY:Ljava/lang/String; = "value"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 67
    invoke-direct {p0}, Lcom/segment/analytics/ValueMap;-><init>()V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .line 70
    invoke-direct {p0, p1}, Lcom/segment/analytics/ValueMap;-><init>(I)V

    return-void
.end method

.method constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 75
    invoke-direct {p0, p1}, Lcom/segment/analytics/ValueMap;-><init>(Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method public category()Ljava/lang/String;
    .locals 1

    const-string v0, "category"

    .line 199
    invoke-virtual {p0, v0}, Lcom/segment/analytics/Properties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public coupon()Ljava/lang/String;
    .locals 1

    const-string v0, "coupon"

    .line 338
    invoke-virtual {p0, v0}, Lcom/segment/analytics/Properties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public currency()Ljava/lang/String;
    .locals 1

    const-string v0, "currency"

    .line 119
    invoke-virtual {p0, v0}, Lcom/segment/analytics/Properties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public discount()D
    .locals 3

    const-string v0, "discount"

    const-wide/16 v1, 0x0

    .line 325
    invoke-virtual {p0, v0, v1, v2}, Lcom/segment/analytics/Properties;->getDouble(Ljava/lang/String;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public isRepeatCustomer()Z
    .locals 2

    const-string v0, "repeat"

    const/4 v1, 0x0

    .line 374
    invoke-virtual {p0, v0, v1}, Lcom/segment/analytics/Properties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public name()Ljava/lang/String;
    .locals 1

    const-string v0, "name"

    .line 185
    invoke-virtual {p0, v0}, Lcom/segment/analytics/Properties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public orderId()Ljava/lang/String;
    .locals 1

    const-string v0, "orderId"

    .line 251
    invoke-virtual {p0, v0}, Lcom/segment/analytics/Properties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public path()Ljava/lang/String;
    .locals 1

    const-string v0, "path"

    .line 132
    invoke-virtual {p0, v0}, Lcom/segment/analytics/Properties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public price()D
    .locals 3

    const-string v0, "price"

    const-wide/16 v1, 0x0

    .line 225
    invoke-virtual {p0, v0, v1, v2}, Lcom/segment/analytics/Properties;->getDouble(Ljava/lang/String;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public productId()Ljava/lang/String;
    .locals 1

    const-string v0, "id"

    .line 238
    invoke-virtual {p0, v0}, Lcom/segment/analytics/Properties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public products()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/segment/analytics/Properties$Product;",
            ">;"
        }
    .end annotation

    .line 361
    const-class v0, Lcom/segment/analytics/Properties$Product;

    const-string v1, "products"

    invoke-virtual {p0, v1, v0}, Lcom/segment/analytics/Properties;->getList(Ljava/lang/Object;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public varargs products([Lcom/segment/analytics/Properties$Product;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/segment/analytics/Properties$Product;",
            ")",
            "Ljava/util/List<",
            "Lcom/segment/analytics/Properties$Product;",
            ">;"
        }
    .end annotation

    .line 357
    invoke-virtual {p0}, Lcom/segment/analytics/Properties;->products()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public putCategory(Ljava/lang/String;)Lcom/segment/analytics/Properties;
    .locals 1

    const-string v0, "category"

    .line 195
    invoke-virtual {p0, v0, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putCoupon(Ljava/lang/String;)Lcom/segment/analytics/Properties;
    .locals 1

    const-string v0, "coupon"

    .line 334
    invoke-virtual {p0, v0, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putCurrency(Ljava/lang/String;)Lcom/segment/analytics/Properties;
    .locals 1

    const-string v0, "currency"

    .line 115
    invoke-virtual {p0, v0, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putDiscount(D)Lcom/segment/analytics/Properties;
    .locals 0

    .line 321
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    const-string p2, "discount"

    invoke-virtual {p0, p2, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putName(Ljava/lang/String;)Lcom/segment/analytics/Properties;
    .locals 1

    const-string v0, "name"

    .line 181
    invoke-virtual {p0, v0, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putOrderId(Ljava/lang/String;)Lcom/segment/analytics/Properties;
    .locals 1

    const-string v0, "orderId"

    .line 247
    invoke-virtual {p0, v0, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putPath(Ljava/lang/String;)Lcom/segment/analytics/Properties;
    .locals 1

    const-string v0, "path"

    .line 128
    invoke-virtual {p0, v0, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putPrice(D)Lcom/segment/analytics/Properties;
    .locals 0

    .line 221
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    const-string p2, "price"

    invoke-virtual {p0, p2, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putProductId(Ljava/lang/String;)Lcom/segment/analytics/Properties;
    .locals 1

    const-string v0, "id"

    .line 234
    invoke-virtual {p0, v0, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public varargs putProducts([Lcom/segment/analytics/Properties$Product;)Lcom/segment/analytics/Properties;
    .locals 2

    .line 347
    invoke-static {p1}, Lcom/segment/analytics/internal/Utils;->isNullOrEmpty([Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 350
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 351
    invoke-static {v0, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 352
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    const-string v0, "products"

    invoke-virtual {p0, v0, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1

    .line 348
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "products cannot be null or empty."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public putReferrer(Ljava/lang/String;)Lcom/segment/analytics/Properties;
    .locals 1

    const-string v0, "referrer"

    .line 142
    invoke-virtual {p0, v0, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putRepeatCustomer(Z)Lcom/segment/analytics/Properties;
    .locals 1

    .line 370
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const-string v0, "repeat"

    invoke-virtual {p0, v0, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putRevenue(D)Lcom/segment/analytics/Properties;
    .locals 0

    .line 89
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    const-string p2, "revenue"

    invoke-virtual {p0, p2, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putShipping(D)Lcom/segment/analytics/Properties;
    .locals 0

    .line 295
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    const-string p2, "shipping"

    invoke-virtual {p0, p2, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putSku(Ljava/lang/String;)Lcom/segment/analytics/Properties;
    .locals 1

    const-string v0, "sku"

    .line 208
    invoke-virtual {p0, v0, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putSubtotal()D
    .locals 3

    const-string v0, "subtotal"

    const-wide/16 v1, 0x0

    .line 286
    invoke-virtual {p0, v0, v1, v2}, Lcom/segment/analytics/Properties;->getDouble(Ljava/lang/String;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public putSubtotal(D)Lcom/segment/analytics/Properties;
    .locals 0

    .line 282
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    const-string p2, "subtotal"

    invoke-virtual {p0, p2, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putTax(D)Lcom/segment/analytics/Properties;
    .locals 0

    .line 308
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    const-string p2, "tax"

    invoke-virtual {p0, p2, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putTitle(Ljava/lang/String;)Lcom/segment/analytics/Properties;
    .locals 1

    const-string v0, "title"

    .line 155
    invoke-virtual {p0, v0, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putTotal(D)Lcom/segment/analytics/Properties;
    .locals 0

    .line 260
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    const-string p2, "total"

    invoke-virtual {p0, p2, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putUrl(Ljava/lang/String;)Lcom/segment/analytics/Properties;
    .locals 1

    const-string v0, "url"

    .line 168
    invoke-virtual {p0, v0, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putValue(D)Lcom/segment/analytics/Properties;
    .locals 0

    .line 102
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    const-string p2, "value"

    invoke-virtual {p0, p2, p1}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;
    .locals 0

    .line 80
    invoke-super {p0, p1, p2}, Lcom/segment/analytics/ValueMap;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/ValueMap;

    return-object p0
.end method

.method public bridge synthetic putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/ValueMap;
    .locals 0

    .line 40
    invoke-virtual {p0, p1, p2}, Lcom/segment/analytics/Properties;->putValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/segment/analytics/Properties;

    move-result-object p1

    return-object p1
.end method

.method public referrer()Ljava/lang/String;
    .locals 1

    const-string v0, "referrer"

    .line 146
    invoke-virtual {p0, v0}, Lcom/segment/analytics/Properties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public revenue()D
    .locals 3

    const-string v0, "revenue"

    const-wide/16 v1, 0x0

    .line 93
    invoke-virtual {p0, v0, v1, v2}, Lcom/segment/analytics/Properties;->getDouble(Ljava/lang/String;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public shipping()D
    .locals 3

    const-string v0, "shipping"

    const-wide/16 v1, 0x0

    .line 299
    invoke-virtual {p0, v0, v1, v2}, Lcom/segment/analytics/Properties;->getDouble(Ljava/lang/String;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public sku()Ljava/lang/String;
    .locals 1

    const-string v0, "sku"

    .line 212
    invoke-virtual {p0, v0}, Lcom/segment/analytics/Properties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public tax()D
    .locals 3

    const-string v0, "tax"

    const-wide/16 v1, 0x0

    .line 312
    invoke-virtual {p0, v0, v1, v2}, Lcom/segment/analytics/Properties;->getDouble(Ljava/lang/String;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public title()Ljava/lang/String;
    .locals 1

    const-string v0, "title"

    .line 159
    invoke-virtual {p0, v0}, Lcom/segment/analytics/Properties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public total()D
    .locals 5

    const-wide/16 v0, 0x0

    const-string v2, "total"

    .line 264
    invoke-virtual {p0, v2, v0, v1}, Lcom/segment/analytics/Properties;->getDouble(Ljava/lang/String;D)D

    move-result-wide v2

    cmpl-double v4, v2, v0

    if-eqz v4, :cond_0

    return-wide v2

    .line 268
    :cond_0
    invoke-virtual {p0}, Lcom/segment/analytics/Properties;->revenue()D

    move-result-wide v2

    cmpl-double v4, v2, v0

    if-eqz v4, :cond_1

    return-wide v2

    .line 272
    :cond_1
    invoke-virtual {p0}, Lcom/segment/analytics/Properties;->value()D

    move-result-wide v0

    return-wide v0
.end method

.method public url()Ljava/lang/String;
    .locals 1

    const-string v0, "url"

    .line 172
    invoke-virtual {p0, v0}, Lcom/segment/analytics/Properties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public value()D
    .locals 5

    const-wide/16 v0, 0x0

    const-string v2, "value"

    .line 106
    invoke-virtual {p0, v2, v0, v1}, Lcom/segment/analytics/Properties;->getDouble(Ljava/lang/String;D)D

    move-result-wide v2

    cmpl-double v4, v2, v0

    if-eqz v4, :cond_0

    return-wide v2

    .line 110
    :cond_0
    invoke-virtual {p0}, Lcom/segment/analytics/Properties;->revenue()D

    move-result-wide v0

    return-wide v0
.end method
