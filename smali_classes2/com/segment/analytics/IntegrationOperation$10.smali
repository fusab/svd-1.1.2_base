.class final Lcom/segment/analytics/IntegrationOperation$10;
.super Lcom/segment/analytics/IntegrationOperation;
.source "IntegrationOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/segment/analytics/IntegrationOperation;->track(Lcom/segment/analytics/integrations/TrackPayload;)Lcom/segment/analytics/IntegrationOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$trackPayload:Lcom/segment/analytics/integrations/TrackPayload;


# direct methods
.method constructor <init>(Lcom/segment/analytics/integrations/TrackPayload;)V
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/segment/analytics/IntegrationOperation$10;->val$trackPayload:Lcom/segment/analytics/integrations/TrackPayload;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/segment/analytics/IntegrationOperation;-><init>(Lcom/segment/analytics/IntegrationOperation$1;)V

    return-void
.end method


# virtual methods
.method public run(Ljava/lang/String;Lcom/segment/analytics/integrations/Integration;Lcom/segment/analytics/ProjectSettings;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/segment/analytics/integrations/Integration<",
            "*>;",
            "Lcom/segment/analytics/ProjectSettings;",
            ")V"
        }
    .end annotation

    .line 170
    iget-object v0, p0, Lcom/segment/analytics/IntegrationOperation$10;->val$trackPayload:Lcom/segment/analytics/integrations/TrackPayload;

    invoke-virtual {v0}, Lcom/segment/analytics/integrations/TrackPayload;->integrations()Lcom/segment/analytics/ValueMap;

    move-result-object v0

    .line 172
    invoke-virtual {p3}, Lcom/segment/analytics/ProjectSettings;->trackingPlan()Lcom/segment/analytics/ValueMap;

    move-result-object p3

    .line 173
    invoke-static {p3}, Lcom/segment/analytics/internal/Utils;->isNullOrEmpty(Ljava/util/Map;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 175
    invoke-static {v0, p1}, Lcom/segment/analytics/IntegrationOperation$10;->isIntegrationEnabled(Lcom/segment/analytics/ValueMap;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 176
    iget-object p1, p0, Lcom/segment/analytics/IntegrationOperation$10;->val$trackPayload:Lcom/segment/analytics/integrations/TrackPayload;

    invoke-virtual {p2, p1}, Lcom/segment/analytics/integrations/Integration;->track(Lcom/segment/analytics/integrations/TrackPayload;)V

    :cond_0
    return-void

    .line 181
    :cond_1
    iget-object v1, p0, Lcom/segment/analytics/IntegrationOperation$10;->val$trackPayload:Lcom/segment/analytics/integrations/TrackPayload;

    invoke-virtual {v1}, Lcom/segment/analytics/integrations/TrackPayload;->event()Ljava/lang/String;

    move-result-object v1

    .line 182
    invoke-virtual {p3, v1}, Lcom/segment/analytics/ValueMap;->getValueMap(Ljava/lang/Object;)Lcom/segment/analytics/ValueMap;

    move-result-object p3

    .line 183
    invoke-static {p3}, Lcom/segment/analytics/internal/Utils;->isNullOrEmpty(Ljava/util/Map;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 185
    invoke-static {v0, p1}, Lcom/segment/analytics/IntegrationOperation$10;->isIntegrationEnabled(Lcom/segment/analytics/ValueMap;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 186
    iget-object p1, p0, Lcom/segment/analytics/IntegrationOperation$10;->val$trackPayload:Lcom/segment/analytics/integrations/TrackPayload;

    invoke-virtual {p2, p1}, Lcom/segment/analytics/integrations/Integration;->track(Lcom/segment/analytics/integrations/TrackPayload;)V

    :cond_2
    return-void

    :cond_3
    const/4 v1, 0x1

    const-string v2, "enabled"

    .line 192
    invoke-virtual {p3, v2, v1}, Lcom/segment/analytics/ValueMap;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_4

    return-void

    .line 199
    :cond_4
    new-instance v1, Lcom/segment/analytics/ValueMap;

    invoke-direct {v1}, Lcom/segment/analytics/ValueMap;-><init>()V

    const-string v2, "integrations"

    .line 200
    invoke-virtual {p3, v2}, Lcom/segment/analytics/ValueMap;->getValueMap(Ljava/lang/Object;)Lcom/segment/analytics/ValueMap;

    move-result-object p3

    .line 201
    invoke-static {p3}, Lcom/segment/analytics/internal/Utils;->isNullOrEmpty(Ljava/util/Map;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 202
    invoke-virtual {v1, p3}, Lcom/segment/analytics/ValueMap;->putAll(Ljava/util/Map;)V

    .line 204
    :cond_5
    invoke-virtual {v1, v0}, Lcom/segment/analytics/ValueMap;->putAll(Ljava/util/Map;)V

    .line 205
    invoke-static {v1, p1}, Lcom/segment/analytics/IntegrationOperation$10;->isIntegrationEnabled(Lcom/segment/analytics/ValueMap;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 206
    iget-object p1, p0, Lcom/segment/analytics/IntegrationOperation$10;->val$trackPayload:Lcom/segment/analytics/integrations/TrackPayload;

    invoke-virtual {p2, p1}, Lcom/segment/analytics/integrations/Integration;->track(Lcom/segment/analytics/integrations/TrackPayload;)V

    :cond_6
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 212
    iget-object v0, p0, Lcom/segment/analytics/IntegrationOperation$10;->val$trackPayload:Lcom/segment/analytics/integrations/TrackPayload;

    invoke-virtual {v0}, Lcom/segment/analytics/integrations/TrackPayload;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
