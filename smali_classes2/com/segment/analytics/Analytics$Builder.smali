.class public Lcom/segment/analytics/Analytics$Builder;
.super Ljava/lang/Object;
.source "Analytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/segment/analytics/Analytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final application:Landroid/app/Application;

.field private collectDeviceID:Z

.field private connectionFactory:Lcom/segment/analytics/ConnectionFactory;

.field private crypto:Lcom/segment/analytics/Crypto;

.field private defaultOptions:Lcom/segment/analytics/Options;

.field private executor:Ljava/util/concurrent/ExecutorService;

.field private final factories:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/segment/analytics/integrations/Integration$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private flushIntervalInMillis:J

.field private flushQueueSize:I

.field private logLevel:Lcom/segment/analytics/Analytics$LogLevel;

.field private middlewares:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/segment/analytics/Middleware;",
            ">;"
        }
    .end annotation
.end field

.field private networkExecutor:Ljava/util/concurrent/ExecutorService;

.field private recordScreenViews:Z

.field private tag:Ljava/lang/String;

.field private trackApplicationLifecycleEvents:Z

.field private trackAttributionInformation:Z

.field private writeKey:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .line 1081
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 1064
    iput-boolean v0, p0, Lcom/segment/analytics/Analytics$Builder;->collectDeviceID:Z

    const/16 v0, 0x14

    .line 1065
    iput v0, p0, Lcom/segment/analytics/Analytics$Builder;->flushQueueSize:I

    const-wide/16 v0, 0x7530

    .line 1066
    iput-wide v0, p0, Lcom/segment/analytics/Analytics$Builder;->flushIntervalInMillis:J

    .line 1073
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/segment/analytics/Analytics$Builder;->factories:Ljava/util/List;

    const/4 v0, 0x0

    .line 1075
    iput-boolean v0, p0, Lcom/segment/analytics/Analytics$Builder;->trackApplicationLifecycleEvents:Z

    .line 1076
    iput-boolean v0, p0, Lcom/segment/analytics/Analytics$Builder;->recordScreenViews:Z

    .line 1077
    iput-boolean v0, p0, Lcom/segment/analytics/Analytics$Builder;->trackAttributionInformation:Z

    if-eqz p1, :cond_3

    const-string v0, "android.permission.INTERNET"

    .line 1085
    invoke-static {p1, v0}, Lcom/segment/analytics/internal/Utils;->hasPermission(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1088
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/app/Application;

    iput-object p1, p0, Lcom/segment/analytics/Analytics$Builder;->application:Landroid/app/Application;

    .line 1089
    iget-object p1, p0, Lcom/segment/analytics/Analytics$Builder;->application:Landroid/app/Application;

    if-eqz p1, :cond_1

    .line 1093
    invoke-static {p2}, Lcom/segment/analytics/internal/Utils;->isNullOrEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 1096
    iput-object p2, p0, Lcom/segment/analytics/Analytics$Builder;->writeKey:Ljava/lang/String;

    return-void

    .line 1094
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "writeKey must not be null or empty."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 1090
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Application context must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 1086
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "INTERNET permission is required."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 1083
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Context must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public build()Lcom/segment/analytics/Analytics;
    .locals 30

    move-object/from16 v1, p0

    .line 1289
    iget-object v0, v1, Lcom/segment/analytics/Analytics$Builder;->tag:Ljava/lang/String;

    invoke-static {v0}, Lcom/segment/analytics/internal/Utils;->isNullOrEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1290
    iget-object v0, v1, Lcom/segment/analytics/Analytics$Builder;->writeKey:Ljava/lang/String;

    iput-object v0, v1, Lcom/segment/analytics/Analytics$Builder;->tag:Ljava/lang/String;

    .line 1292
    :cond_0
    sget-object v2, Lcom/segment/analytics/Analytics;->INSTANCES:Ljava/util/List;

    monitor-enter v2

    .line 1293
    :try_start_0
    sget-object v0, Lcom/segment/analytics/Analytics;->INSTANCES:Ljava/util/List;

    iget-object v3, v1, Lcom/segment/analytics/Analytics$Builder;->tag:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1300
    sget-object v0, Lcom/segment/analytics/Analytics;->INSTANCES:Ljava/util/List;

    iget-object v3, v1, Lcom/segment/analytics/Analytics$Builder;->tag:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1301
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1303
    iget-object v0, v1, Lcom/segment/analytics/Analytics$Builder;->defaultOptions:Lcom/segment/analytics/Options;

    if-nez v0, :cond_1

    .line 1304
    new-instance v0, Lcom/segment/analytics/Options;

    invoke-direct {v0}, Lcom/segment/analytics/Options;-><init>()V

    iput-object v0, v1, Lcom/segment/analytics/Analytics$Builder;->defaultOptions:Lcom/segment/analytics/Options;

    .line 1306
    :cond_1
    iget-object v0, v1, Lcom/segment/analytics/Analytics$Builder;->logLevel:Lcom/segment/analytics/Analytics$LogLevel;

    if-nez v0, :cond_2

    .line 1307
    sget-object v0, Lcom/segment/analytics/Analytics$LogLevel;->NONE:Lcom/segment/analytics/Analytics$LogLevel;

    iput-object v0, v1, Lcom/segment/analytics/Analytics$Builder;->logLevel:Lcom/segment/analytics/Analytics$LogLevel;

    .line 1309
    :cond_2
    iget-object v0, v1, Lcom/segment/analytics/Analytics$Builder;->networkExecutor:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_3

    .line 1310
    new-instance v0, Lcom/segment/analytics/internal/Utils$AnalyticsNetworkExecutorService;

    invoke-direct {v0}, Lcom/segment/analytics/internal/Utils$AnalyticsNetworkExecutorService;-><init>()V

    iput-object v0, v1, Lcom/segment/analytics/Analytics$Builder;->networkExecutor:Ljava/util/concurrent/ExecutorService;

    .line 1312
    :cond_3
    iget-object v0, v1, Lcom/segment/analytics/Analytics$Builder;->connectionFactory:Lcom/segment/analytics/ConnectionFactory;

    if-nez v0, :cond_4

    .line 1313
    new-instance v0, Lcom/segment/analytics/ConnectionFactory;

    invoke-direct {v0}, Lcom/segment/analytics/ConnectionFactory;-><init>()V

    iput-object v0, v1, Lcom/segment/analytics/Analytics$Builder;->connectionFactory:Lcom/segment/analytics/ConnectionFactory;

    .line 1315
    :cond_4
    iget-object v0, v1, Lcom/segment/analytics/Analytics$Builder;->crypto:Lcom/segment/analytics/Crypto;

    if-nez v0, :cond_5

    .line 1316
    invoke-static {}, Lcom/segment/analytics/Crypto;->none()Lcom/segment/analytics/Crypto;

    move-result-object v0

    iput-object v0, v1, Lcom/segment/analytics/Analytics$Builder;->crypto:Lcom/segment/analytics/Crypto;

    .line 1319
    :cond_5
    new-instance v5, Lcom/segment/analytics/Stats;

    invoke-direct {v5}, Lcom/segment/analytics/Stats;-><init>()V

    .line 1320
    sget-object v13, Lcom/segment/analytics/Cartographer;->INSTANCE:Lcom/segment/analytics/Cartographer;

    .line 1321
    new-instance v12, Lcom/segment/analytics/Client;

    iget-object v0, v1, Lcom/segment/analytics/Analytics$Builder;->writeKey:Ljava/lang/String;

    iget-object v2, v1, Lcom/segment/analytics/Analytics$Builder;->connectionFactory:Lcom/segment/analytics/ConnectionFactory;

    invoke-direct {v12, v0, v2}, Lcom/segment/analytics/Client;-><init>(Ljava/lang/String;Lcom/segment/analytics/ConnectionFactory;)V

    .line 1323
    new-instance v14, Lcom/segment/analytics/ProjectSettings$Cache;

    iget-object v0, v1, Lcom/segment/analytics/Analytics$Builder;->application:Landroid/app/Application;

    iget-object v2, v1, Lcom/segment/analytics/Analytics$Builder;->tag:Ljava/lang/String;

    invoke-direct {v14, v0, v13, v2}, Lcom/segment/analytics/ProjectSettings$Cache;-><init>(Landroid/content/Context;Lcom/segment/analytics/Cartographer;Ljava/lang/String;)V

    .line 1326
    new-instance v0, Lcom/segment/analytics/BooleanPreference;

    iget-object v2, v1, Lcom/segment/analytics/Analytics$Builder;->application:Landroid/app/Application;

    iget-object v3, v1, Lcom/segment/analytics/Analytics$Builder;->tag:Ljava/lang/String;

    .line 1328
    invoke-static {v2, v3}, Lcom/segment/analytics/internal/Utils;->getSegmentSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, "opt-out"

    invoke-direct {v0, v2, v4, v3}, Lcom/segment/analytics/BooleanPreference;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 1330
    new-instance v6, Lcom/segment/analytics/Traits$Cache;

    iget-object v2, v1, Lcom/segment/analytics/Analytics$Builder;->application:Landroid/app/Application;

    iget-object v3, v1, Lcom/segment/analytics/Analytics$Builder;->tag:Ljava/lang/String;

    invoke-direct {v6, v2, v13, v3}, Lcom/segment/analytics/Traits$Cache;-><init>(Landroid/content/Context;Lcom/segment/analytics/Cartographer;Ljava/lang/String;)V

    .line 1331
    invoke-virtual {v6}, Lcom/segment/analytics/Traits$Cache;->isSet()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v6}, Lcom/segment/analytics/Traits$Cache;->get()Lcom/segment/analytics/ValueMap;

    move-result-object v2

    if-nez v2, :cond_7

    .line 1332
    :cond_6
    invoke-static {}, Lcom/segment/analytics/Traits;->create()Lcom/segment/analytics/Traits;

    move-result-object v2

    .line 1333
    invoke-virtual {v6, v2}, Lcom/segment/analytics/Traits$Cache;->set(Lcom/segment/analytics/ValueMap;)V

    .line 1336
    :cond_7
    iget-object v2, v1, Lcom/segment/analytics/Analytics$Builder;->logLevel:Lcom/segment/analytics/Analytics$LogLevel;

    invoke-static {v2}, Lcom/segment/analytics/integrations/Logger;->with(Lcom/segment/analytics/Analytics$LogLevel;)Lcom/segment/analytics/integrations/Logger;

    move-result-object v9

    .line 1337
    iget-object v2, v1, Lcom/segment/analytics/Analytics$Builder;->application:Landroid/app/Application;

    .line 1338
    invoke-virtual {v6}, Lcom/segment/analytics/Traits$Cache;->get()Lcom/segment/analytics/ValueMap;

    move-result-object v3

    check-cast v3, Lcom/segment/analytics/Traits;

    iget-boolean v4, v1, Lcom/segment/analytics/Analytics$Builder;->collectDeviceID:Z

    invoke-static {v2, v3, v4}, Lcom/segment/analytics/AnalyticsContext;->create(Landroid/content/Context;Lcom/segment/analytics/Traits;Z)Lcom/segment/analytics/AnalyticsContext;

    move-result-object v7

    .line 1339
    new-instance v15, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v15, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 1340
    iget-object v3, v1, Lcom/segment/analytics/Analytics$Builder;->application:Landroid/app/Application;

    invoke-virtual {v7, v3, v15, v9}, Lcom/segment/analytics/AnalyticsContext;->attachAdvertisingId(Landroid/content/Context;Ljava/util/concurrent/CountDownLatch;Lcom/segment/analytics/integrations/Logger;)V

    .line 1342
    new-instance v11, Ljava/util/ArrayList;

    iget-object v3, v1, Lcom/segment/analytics/Analytics$Builder;->factories:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v3, v2

    invoke-direct {v11, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1343
    sget-object v2, Lcom/segment/analytics/SegmentIntegration;->FACTORY:Lcom/segment/analytics/integrations/Integration$Factory;

    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1344
    iget-object v2, v1, Lcom/segment/analytics/Analytics$Builder;->factories:Ljava/util/List;

    invoke-interface {v11, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1346
    iget-object v2, v1, Lcom/segment/analytics/Analytics$Builder;->middlewares:Ljava/util/List;

    invoke-static {v2}, Lcom/segment/analytics/internal/Utils;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v26

    .line 1348
    iget-object v2, v1, Lcom/segment/analytics/Analytics$Builder;->executor:Ljava/util/concurrent/ExecutorService;

    if-nez v2, :cond_8

    .line 1350
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    :cond_8
    move-object/from16 v19, v2

    .line 1353
    new-instance v27, Lcom/segment/analytics/Analytics;

    move-object/from16 v2, v27

    iget-object v3, v1, Lcom/segment/analytics/Analytics$Builder;->application:Landroid/app/Application;

    iget-object v4, v1, Lcom/segment/analytics/Analytics$Builder;->networkExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v8, v1, Lcom/segment/analytics/Analytics$Builder;->defaultOptions:Lcom/segment/analytics/Options;

    iget-object v10, v1, Lcom/segment/analytics/Analytics$Builder;->tag:Ljava/lang/String;

    .line 1362
    invoke-static {v11}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v11

    move-object/from16 v16, v15

    iget-object v15, v1, Lcom/segment/analytics/Analytics$Builder;->writeKey:Ljava/lang/String;

    move-object/from16 v21, v16

    move-object/from16 v28, v2

    iget v2, v1, Lcom/segment/analytics/Analytics$Builder;->flushQueueSize:I

    move/from16 v16, v2

    move-object/from16 v29, v3

    iget-wide v2, v1, Lcom/segment/analytics/Analytics$Builder;->flushIntervalInMillis:J

    move-wide/from16 v17, v2

    iget-boolean v2, v1, Lcom/segment/analytics/Analytics$Builder;->trackApplicationLifecycleEvents:Z

    move/from16 v20, v2

    iget-boolean v2, v1, Lcom/segment/analytics/Analytics$Builder;->recordScreenViews:Z

    move/from16 v22, v2

    iget-boolean v2, v1, Lcom/segment/analytics/Analytics$Builder;->trackAttributionInformation:Z

    move/from16 v23, v2

    iget-object v2, v1, Lcom/segment/analytics/Analytics$Builder;->crypto:Lcom/segment/analytics/Crypto;

    move-object/from16 v25, v2

    move-object/from16 v24, v0

    move-object/from16 v2, v28

    move-object/from16 v3, v29

    invoke-direct/range {v2 .. v26}, Lcom/segment/analytics/Analytics;-><init>(Landroid/app/Application;Ljava/util/concurrent/ExecutorService;Lcom/segment/analytics/Stats;Lcom/segment/analytics/Traits$Cache;Lcom/segment/analytics/AnalyticsContext;Lcom/segment/analytics/Options;Lcom/segment/analytics/integrations/Logger;Ljava/lang/String;Ljava/util/List;Lcom/segment/analytics/Client;Lcom/segment/analytics/Cartographer;Lcom/segment/analytics/ProjectSettings$Cache;Ljava/lang/String;IJLjava/util/concurrent/ExecutorService;ZLjava/util/concurrent/CountDownLatch;ZZLcom/segment/analytics/BooleanPreference;Lcom/segment/analytics/Crypto;Ljava/util/List;)V

    return-object v27

    .line 1294
    :cond_9
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Duplicate analytics client created with tag: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v1, Lcom/segment/analytics/Analytics$Builder;->tag:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ". If you want to use multiple Analytics clients, use a different writeKey "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "or set a tag via the builder during construction."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    .line 1301
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public collectDeviceId(Z)Lcom/segment/analytics/Analytics$Builder;
    .locals 0

    .line 1141
    iput-boolean p1, p0, Lcom/segment/analytics/Analytics$Builder;->collectDeviceID:Z

    return-object p0
.end method

.method public connectionFactory(Lcom/segment/analytics/ConnectionFactory;)Lcom/segment/analytics/Analytics$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 1223
    iput-object p1, p0, Lcom/segment/analytics/Analytics$Builder;->connectionFactory:Lcom/segment/analytics/ConnectionFactory;

    return-object p0

    .line 1221
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "ConnectionFactory must not be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public crypto(Lcom/segment/analytics/Crypto;)Lcom/segment/analytics/Analytics$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 1232
    iput-object p1, p0, Lcom/segment/analytics/Analytics$Builder;->crypto:Lcom/segment/analytics/Crypto;

    return-object p0

    .line 1230
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Crypto must not be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public defaultOptions(Lcom/segment/analytics/Options;)Lcom/segment/analytics/Analytics$Builder;
    .locals 3

    if-eqz p1, :cond_2

    .line 1156
    new-instance v0, Lcom/segment/analytics/Options;

    invoke-direct {v0}, Lcom/segment/analytics/Options;-><init>()V

    iput-object v0, p0, Lcom/segment/analytics/Analytics$Builder;->defaultOptions:Lcom/segment/analytics/Options;

    .line 1157
    invoke-virtual {p1}, Lcom/segment/analytics/Options;->integrations()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1158
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 1159
    iget-object v1, p0, Lcom/segment/analytics/Analytics$Builder;->defaultOptions:Lcom/segment/analytics/Options;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/segment/analytics/Options;->setIntegration(Ljava/lang/String;Z)Lcom/segment/analytics/Options;

    goto :goto_0

    .line 1162
    :cond_0
    iget-object v1, p0, Lcom/segment/analytics/Analytics$Builder;->defaultOptions:Lcom/segment/analytics/Options;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/segment/analytics/Options;->setIntegration(Ljava/lang/String;Z)Lcom/segment/analytics/Options;

    goto :goto_0

    :cond_1
    return-object p0

    .line 1153
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "defaultOptions must not be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public disableBundledIntegrations()Lcom/segment/analytics/Analytics$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-object p0
.end method

.method executor(Ljava/util/concurrent/ExecutorService;)Lcom/segment/analytics/Analytics$Builder;
    .locals 1

    const-string v0, "executor"

    .line 1283
    invoke-static {p1, v0}, Lcom/segment/analytics/internal/Utils;->assertNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ExecutorService;

    iput-object p1, p0, Lcom/segment/analytics/Analytics$Builder;->executor:Ljava/util/concurrent/ExecutorService;

    return-object p0
.end method

.method public flushInterval(JLjava/util/concurrent/TimeUnit;)Lcom/segment/analytics/Analytics$Builder;
    .locals 3

    if-eqz p3, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    .line 1131
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide p1

    iput-wide p1, p0, Lcom/segment/analytics/Analytics$Builder;->flushIntervalInMillis:J

    return-object p0

    .line 1129
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "flushInterval must be greater than zero."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 1126
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "timeUnit must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public flushQueueSize(I)Lcom/segment/analytics/Analytics$Builder;
    .locals 1

    if-lez p1, :cond_1

    const/16 v0, 0xfa

    if-gt p1, v0, :cond_0

    .line 1114
    iput p1, p0, Lcom/segment/analytics/Analytics$Builder;->flushQueueSize:I

    return-object p0

    .line 1112
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "flushQueueSize must be less than or equal to 250."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 1107
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "flushQueueSize must be greater than or equal to zero."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public logLevel(Lcom/segment/analytics/Analytics$LogLevel;)Lcom/segment/analytics/Analytics$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 1188
    iput-object p1, p0, Lcom/segment/analytics/Analytics$Builder;->logLevel:Lcom/segment/analytics/Analytics$LogLevel;

    return-object p0

    .line 1186
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "LogLevel must not be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public middleware(Lcom/segment/analytics/Middleware;)Lcom/segment/analytics/Analytics$Builder;
    .locals 1

    const-string v0, "middleware"

    .line 1268
    invoke-static {p1, v0}, Lcom/segment/analytics/internal/Utils;->assertNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1269
    iget-object v0, p0, Lcom/segment/analytics/Analytics$Builder;->middlewares:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1270
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/segment/analytics/Analytics$Builder;->middlewares:Ljava/util/List;

    .line 1272
    :cond_0
    iget-object v0, p0, Lcom/segment/analytics/Analytics$Builder;->middlewares:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1275
    iget-object v0, p0, Lcom/segment/analytics/Analytics$Builder;->middlewares:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 1273
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Middleware is already registered."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public networkExecutor(Ljava/util/concurrent/ExecutorService;)Lcom/segment/analytics/Analytics$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 1209
    iput-object p1, p0, Lcom/segment/analytics/Analytics$Builder;->networkExecutor:Ljava/util/concurrent/ExecutorService;

    return-object p0

    .line 1207
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Executor service must not be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public recordScreenViews()Lcom/segment/analytics/Analytics$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1256
    iput-boolean v0, p0, Lcom/segment/analytics/Analytics$Builder;->recordScreenViews:Z

    return-object p0
.end method

.method public tag(Ljava/lang/String;)Lcom/segment/analytics/Analytics$Builder;
    .locals 1

    .line 1176
    invoke-static {p1}, Lcom/segment/analytics/internal/Utils;->isNullOrEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1179
    iput-object p1, p0, Lcom/segment/analytics/Analytics$Builder;->tag:Ljava/lang/String;

    return-object p0

    .line 1177
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "tag must not be null or empty."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public trackApplicationLifecycleEvents()Lcom/segment/analytics/Analytics$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1250
    iput-boolean v0, p0, Lcom/segment/analytics/Analytics$Builder;->trackApplicationLifecycleEvents:Z

    return-object p0
.end method

.method public trackAttributionInformation()Lcom/segment/analytics/Analytics$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 1262
    iput-boolean v0, p0, Lcom/segment/analytics/Analytics$Builder;->trackAttributionInformation:Z

    return-object p0
.end method

.method public use(Lcom/segment/analytics/integrations/Integration$Factory;)Lcom/segment/analytics/Analytics$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 1241
    iget-object v0, p0, Lcom/segment/analytics/Analytics$Builder;->factories:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 1239
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Factory must not be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
