.class Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$5;
.super Ljava/lang/Object;
.source "RNGoogleSafetyNetModule.java"

# interfaces
.implements Lcom/google/android/gms/tasks/OnCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;->getHarmfulApps(Lcom/facebook/react/bridge/Promise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/tasks/OnCompleteListener<",
        "Lcom/google/android/gms/safetynet/SafetyNetApi$HarmfulAppsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;

.field final synthetic val$promise:Lcom/facebook/react/bridge/Promise;


# direct methods
.method constructor <init>(Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;Lcom/facebook/react/bridge/Promise;)V
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$5;->this$0:Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;

    iput-object p2, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$5;->val$promise:Lcom/facebook/react/bridge/Promise;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/google/android/gms/tasks/Task;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/tasks/Task<",
            "Lcom/google/android/gms/safetynet/SafetyNetApi$HarmfulAppsResponse;",
            ">;)V"
        }
    .end annotation

    .line 166
    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->isSuccessful()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 167
    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/safetynet/SafetyNetApi$HarmfulAppsResponse;

    .line 168
    invoke-virtual {p1}, Lcom/google/android/gms/safetynet/SafetyNetApi$HarmfulAppsResponse;->getHarmfulAppsList()Ljava/util/List;

    move-result-object p1

    .line 170
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    iget-object p1, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$5;->val$promise:Lcom/facebook/react/bridge/Promise;

    const-string v0, "No harmful apps installed"

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    goto :goto_1

    .line 173
    :cond_0
    new-instance v0, Lcom/facebook/react/bridge/WritableNativeArray;

    invoke-direct {v0}, Lcom/facebook/react/bridge/WritableNativeArray;-><init>()V

    .line 174
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/safetynet/HarmfulAppsData;

    .line 175
    new-instance v2, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v2}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 176
    iget-object v3, v1, Lcom/google/android/gms/safetynet/HarmfulAppsData;->apkPackageName:Ljava/lang/String;

    const-string v4, "apkPackageName"

    invoke-interface {v2, v4, v3}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iget-object v3, v1, Lcom/google/android/gms/safetynet/HarmfulAppsData;->apkSha256:[B

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "apkSha256"

    invoke-interface {v2, v4, v3}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget v1, v1, Lcom/google/android/gms/safetynet/HarmfulAppsData;->apkCategory:I

    const-string v3, "apkCategory"

    invoke-interface {v2, v3, v1}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    .line 179
    invoke-interface {v0, v2}, Lcom/facebook/react/bridge/WritableArray;->pushMap(Lcom/facebook/react/bridge/WritableMap;)V

    goto :goto_0

    .line 181
    :cond_1
    iget-object p1, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$5;->val$promise:Lcom/facebook/react/bridge/Promise;

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    goto :goto_1

    .line 184
    :cond_2
    iget-object p1, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$5;->val$promise:Lcom/facebook/react/bridge/Promise;

    const-string v0, "Error"

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Promise;->reject(Ljava/lang/String;)V

    :goto_1
    return-void
.end method
