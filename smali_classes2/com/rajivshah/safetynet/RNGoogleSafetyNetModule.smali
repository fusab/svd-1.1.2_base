.class public Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;
.super Lcom/facebook/react/bridge/ReactContextBaseJavaModule;
.source "RNGoogleSafetyNetModule.java"


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final baseContext:Lcom/facebook/react/bridge/ReactApplicationContext;

.field private final reactContext:Lcom/facebook/react/bridge/ReactApplicationContext;


# direct methods
.method public constructor <init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/facebook/react/bridge/ReactContextBaseJavaModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    .line 47
    iput-object p1, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;->reactContext:Lcom/facebook/react/bridge/ReactApplicationContext;

    .line 48
    invoke-virtual {p0}, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object p1

    iput-object p1, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;->baseContext:Lcom/facebook/react/bridge/ReactApplicationContext;

    .line 49
    invoke-virtual {p0}, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;->getCurrentActivity()Landroid/app/Activity;

    move-result-object p1

    iput-object p1, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;->activity:Landroid/app/Activity;

    return-void
.end method

.method private bytesToString([B)Ljava/lang/String;
    .locals 2

    .line 203
    new-instance v0, Ljava/lang/String;

    sget-object v1, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v0, p1, v1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v0
.end method

.method private stringToBytes(Ljava/lang/String;)[B
    .locals 1

    const/4 v0, 0x0

    .line 194
    :try_start_0
    invoke-static {p1, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 196
    invoke-virtual {p1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method


# virtual methods
.method public getHarmfulApps(Lcom/facebook/react/bridge/Promise;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 160
    iget-object v0, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;->baseContext:Lcom/facebook/react/bridge/ReactApplicationContext;

    invoke-static {v0}, Lcom/google/android/gms/safetynet/SafetyNet;->getClient(Landroid/content/Context;)Lcom/google/android/gms/safetynet/SafetyNetClient;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Lcom/google/android/gms/safetynet/SafetyNetClient;->listHarmfulApps()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$5;

    invoke-direct {v1, p0, p1}, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$5;-><init>(Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;Lcom/facebook/react/bridge/Promise;)V

    .line 162
    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "RNGoogleSafetyNet"

    return-object v0
.end method

.method public isPlayServicesAvailable(Lcom/facebook/react/bridge/Promise;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 64
    new-instance v0, Lcom/google/android/gms/common/ConnectionResult;

    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v1

    iget-object v2, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;->baseContext:Lcom/facebook/react/bridge/ReactApplicationContext;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/ConnectionResult;-><init>(I)V

    .line 65
    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 66
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    goto :goto_0

    .line 69
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Promise;->reject(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public isVerificationEnabled(Lcom/facebook/react/bridge/Promise;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 110
    iget-object v0, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;->baseContext:Lcom/facebook/react/bridge/ReactApplicationContext;

    invoke-static {v0}, Lcom/google/android/gms/safetynet/SafetyNet;->getClient(Landroid/content/Context;)Lcom/google/android/gms/safetynet/SafetyNetClient;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Lcom/google/android/gms/safetynet/SafetyNetClient;->isVerifyAppsEnabled()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$3;

    invoke-direct {v1, p0, p1}, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$3;-><init>(Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;Lcom/facebook/react/bridge/Promise;)V

    .line 112
    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public requestVerification(Lcom/facebook/react/bridge/Promise;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;->baseContext:Lcom/facebook/react/bridge/ReactApplicationContext;

    invoke-static {v0}, Lcom/google/android/gms/safetynet/SafetyNet;->getClient(Landroid/content/Context;)Lcom/google/android/gms/safetynet/SafetyNetClient;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Lcom/google/android/gms/safetynet/SafetyNetClient;->enableVerifyApps()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$4;

    invoke-direct {v1, p0, p1}, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$4;-><init>(Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;Lcom/facebook/react/bridge/Promise;)V

    .line 136
    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public sendAttestationRequest(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 84
    invoke-direct {p0, p1}, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;->stringToBytes(Ljava/lang/String;)[B

    move-result-object p1

    .line 85
    invoke-virtual {p0}, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;->baseContext:Lcom/facebook/react/bridge/ReactApplicationContext;

    invoke-static {v1}, Lcom/google/android/gms/safetynet/SafetyNet;->getClient(Landroid/content/Context;)Lcom/google/android/gms/safetynet/SafetyNetClient;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/google/android/gms/safetynet/SafetyNetClient;->attest([BLjava/lang/String;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    new-instance p2, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$2;

    invoke-direct {p2, p0, p3}, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$2;-><init>(Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;Lcom/facebook/react/bridge/Promise;)V

    .line 87
    invoke-virtual {p1, v0, p2}, Lcom/google/android/gms/tasks/Task;->addOnSuccessListener(Landroid/app/Activity;Lcom/google/android/gms/tasks/OnSuccessListener;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    new-instance p2, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$1;

    invoke-direct {p2, p0, p3}, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$1;-><init>(Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;Lcom/facebook/react/bridge/Promise;)V

    .line 95
    invoke-virtual {p1, v0, p2}, Lcom/google/android/gms/tasks/Task;->addOnFailureListener(Landroid/app/Activity;Lcom/google/android/gms/tasks/OnFailureListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method
