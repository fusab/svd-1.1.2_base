.class Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$3;
.super Ljava/lang/Object;
.source "RNGoogleSafetyNetModule.java"

# interfaces
.implements Lcom/google/android/gms/tasks/OnCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;->isVerificationEnabled(Lcom/facebook/react/bridge/Promise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/tasks/OnCompleteListener<",
        "Lcom/google/android/gms/safetynet/SafetyNetApi$VerifyAppsUserResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;

.field final synthetic val$promise:Lcom/facebook/react/bridge/Promise;


# direct methods
.method constructor <init>(Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;Lcom/facebook/react/bridge/Promise;)V
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$3;->this$0:Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;

    iput-object p2, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$3;->val$promise:Lcom/facebook/react/bridge/Promise;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/google/android/gms/tasks/Task;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/tasks/Task<",
            "Lcom/google/android/gms/safetynet/SafetyNetApi$VerifyAppsUserResponse;",
            ">;)V"
        }
    .end annotation

    .line 115
    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->isSuccessful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/safetynet/SafetyNetApi$VerifyAppsUserResponse;

    .line 117
    iget-object v0, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$3;->val$promise:Lcom/facebook/react/bridge/Promise;

    invoke-virtual {p1}, Lcom/google/android/gms/safetynet/SafetyNetApi$VerifyAppsUserResponse;->isVerifyAppsEnabled()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    goto :goto_0

    .line 119
    :cond_0
    iget-object p1, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$3;->val$promise:Lcom/facebook/react/bridge/Promise;

    const-string v0, "Error"

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Promise;->reject(Ljava/lang/String;)V

    :goto_0
    return-void
.end method
