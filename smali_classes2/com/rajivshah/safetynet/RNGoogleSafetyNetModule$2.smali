.class Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$2;
.super Ljava/lang/Object;
.source "RNGoogleSafetyNetModule.java"

# interfaces
.implements Lcom/google/android/gms/tasks/OnSuccessListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;->sendAttestationRequest(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/Promise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/tasks/OnSuccessListener<",
        "Lcom/google/android/gms/safetynet/SafetyNetApi$AttestationResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;

.field final synthetic val$promise:Lcom/facebook/react/bridge/Promise;


# direct methods
.method constructor <init>(Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;Lcom/facebook/react/bridge/Promise;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$2;->this$0:Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule;

    iput-object p2, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$2;->val$promise:Lcom/facebook/react/bridge/Promise;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/android/gms/safetynet/SafetyNetApi$AttestationResponse;)V
    .locals 1

    .line 91
    invoke-virtual {p1}, Lcom/google/android/gms/safetynet/SafetyNetApi$AttestationResponse;->getJwsResult()Ljava/lang/String;

    move-result-object p1

    .line 92
    iget-object v0, p0, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$2;->val$promise:Lcom/facebook/react/bridge/Promise;

    invoke-interface {v0, p1}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .line 88
    check-cast p1, Lcom/google/android/gms/safetynet/SafetyNetApi$AttestationResponse;

    invoke-virtual {p0, p1}, Lcom/rajivshah/safetynet/RNGoogleSafetyNetModule$2;->onSuccess(Lcom/google/android/gms/safetynet/SafetyNetApi$AttestationResponse;)V

    return-void
.end method
