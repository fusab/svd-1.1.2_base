.class Les/iagt/adyen/RNAdyenIagtModule$1;
.super Ljava/lang/Object;
.source "RNAdyenIagtModule.java"

# interfaces
.implements Lcom/adyen/core/interfaces/PaymentRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Les/iagt/adyen/RNAdyenIagtModule;->doPayment(Lcom/facebook/react/bridge/ReadableMap;Lcom/facebook/react/bridge/ReadableMap;Lcom/facebook/react/bridge/Promise;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Les/iagt/adyen/RNAdyenIagtModule;

.field final synthetic val$headersMap:Lcom/facebook/react/bridge/ReadableMap;

.field final synthetic val$promise:Lcom/facebook/react/bridge/Promise;

.field final synthetic val$readableMap:Lcom/facebook/react/bridge/ReadableMap;


# direct methods
.method constructor <init>(Les/iagt/adyen/RNAdyenIagtModule;Lcom/facebook/react/bridge/ReadableMap;Lcom/facebook/react/bridge/ReadableMap;Lcom/facebook/react/bridge/Promise;)V
    .locals 0

    .line 55
    iput-object p1, p0, Les/iagt/adyen/RNAdyenIagtModule$1;->this$0:Les/iagt/adyen/RNAdyenIagtModule;

    iput-object p2, p0, Les/iagt/adyen/RNAdyenIagtModule$1;->val$headersMap:Lcom/facebook/react/bridge/ReadableMap;

    iput-object p3, p0, Les/iagt/adyen/RNAdyenIagtModule$1;->val$readableMap:Lcom/facebook/react/bridge/ReadableMap;

    iput-object p4, p0, Les/iagt/adyen/RNAdyenIagtModule$1;->val$promise:Lcom/facebook/react/bridge/Promise;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPaymentDataRequested(Lcom/adyen/core/PaymentRequest;Ljava/lang/String;Lcom/adyen/core/interfaces/PaymentDataCallback;)V
    .locals 16
    .param p1    # Lcom/adyen/core/PaymentRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/adyen/core/interfaces/PaymentDataCallback;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    move-object/from16 v1, p0

    move-object/from16 v0, p2

    const-string v2, "shopperReference"

    const-string v3, "currency"

    const-string v4, "value"

    const-string v5, "shopperLocale"

    const-string v6, "countryCode"

    const-string v7, "reference"

    const-string v8, "returnUrl"

    const-string v9, "Adyen Sdk Token"

    .line 58
    invoke-static {v9, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    const-string v10, "Content-Type"

    const-string v11, "application/json; charset=UTF-8"

    .line 60
    invoke-interface {v9, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    iget-object v10, v1, Les/iagt/adyen/RNAdyenIagtModule$1;->val$headersMap:Lcom/facebook/react/bridge/ReadableMap;

    if-eqz v10, :cond_0

    .line 64
    invoke-interface {v10}, Lcom/facebook/react/bridge/ReadableMap;->keySetIterator()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;

    move-result-object v10

    .line 65
    :goto_0
    invoke-interface {v10}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->hasNextKey()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 66
    invoke-interface {v10}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->nextKey()Ljava/lang/String;

    move-result-object v11

    .line 67
    iget-object v12, v1, Les/iagt/adyen/RNAdyenIagtModule$1;->val$headersMap:Lcom/facebook/react/bridge/ReadableMap;

    invoke-interface {v12, v11}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 68
    invoke-static {}, Les/iagt/adyen/RNAdyenIagtModule;->access$000()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Headers Iterator key: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    invoke-static {}, Les/iagt/adyen/RNAdyenIagtModule;->access$000()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Headers Iterator value: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-interface {v9, v11, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 73
    :cond_0
    iget-object v10, v1, Les/iagt/adyen/RNAdyenIagtModule$1;->val$readableMap:Lcom/facebook/react/bridge/ReadableMap;

    const-string v11, "demoServerApiKey"

    invoke-interface {v10, v11}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 74
    iget-object v10, v1, Les/iagt/adyen/RNAdyenIagtModule$1;->val$readableMap:Lcom/facebook/react/bridge/ReadableMap;

    invoke-interface {v10, v11}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "X-Demo-Server-API-Key"

    invoke-interface {v9, v11, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    :cond_1
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v11, "token"

    .line 79
    invoke-virtual {v10, v11, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 82
    iget-object v0, v1, Les/iagt/adyen/RNAdyenIagtModule$1;->val$readableMap:Lcom/facebook/react/bridge/ReadableMap;

    invoke-interface {v0, v8}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 83
    iget-object v0, v1, Les/iagt/adyen/RNAdyenIagtModule$1;->val$readableMap:Lcom/facebook/react/bridge/ReadableMap;

    invoke-interface {v0, v8}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v8, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    :cond_2
    const-string v0, "app://checkout"

    .line 85
    invoke-virtual {v10, v8, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 87
    :goto_1
    iget-object v0, v1, Les/iagt/adyen/RNAdyenIagtModule$1;->val$readableMap:Lcom/facebook/react/bridge/ReadableMap;

    invoke-interface {v0, v6}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v6, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 88
    iget-object v0, v1, Les/iagt/adyen/RNAdyenIagtModule$1;->val$readableMap:Lcom/facebook/react/bridge/ReadableMap;

    invoke-interface {v0, v5}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 89
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 90
    iget-object v5, v1, Les/iagt/adyen/RNAdyenIagtModule$1;->val$readableMap:Lcom/facebook/react/bridge/ReadableMap;

    invoke-interface {v5, v4}, Lcom/facebook/react/bridge/ReadableMap;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 91
    iget-object v4, v1, Les/iagt/adyen/RNAdyenIagtModule$1;->val$readableMap:Lcom/facebook/react/bridge/ReadableMap;

    invoke-interface {v4, v3}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "amount"

    .line 92
    invoke-virtual {v10, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 93
    iget-object v0, v1, Les/iagt/adyen/RNAdyenIagtModule$1;->val$readableMap:Lcom/facebook/react/bridge/ReadableMap;

    invoke-interface {v0, v2}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "channel"

    const-string v2, "android"

    .line 94
    invoke-virtual {v10, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 96
    :try_start_1
    iget-object v0, v1, Les/iagt/adyen/RNAdyenIagtModule$1;->val$readableMap:Lcom/facebook/react/bridge/ReadableMap;

    invoke-interface {v0, v7}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v7, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 98
    :catch_0
    :try_start_2
    iget-object v0, v1, Les/iagt/adyen/RNAdyenIagtModule$1;->val$readableMap:Lcom/facebook/react/bridge/ReadableMap;

    invoke-interface {v0, v7}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v7, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    const-string v2, "Unexpected error"

    const-string v3, "Setup failed"

    .line 101
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v2, v1, Les/iagt/adyen/RNAdyenIagtModule$1;->val$promise:Lcom/facebook/react/bridge/Promise;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "JsonException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    .line 104
    :goto_2
    iget-object v0, v1, Les/iagt/adyen/RNAdyenIagtModule$1;->val$readableMap:Lcom/facebook/react/bridge/ReadableMap;

    const-string v2, "url"

    invoke-interface {v0, v2}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Les/iagt/adyen/RNAdyenIagtModule$1$1;

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    invoke-direct {v3, v1, v5, v4}, Les/iagt/adyen/RNAdyenIagtModule$1$1;-><init>(Les/iagt/adyen/RNAdyenIagtModule$1;Lcom/adyen/core/interfaces/PaymentDataCallback;Lcom/adyen/core/PaymentRequest;)V

    invoke-static {v0, v9, v2, v3}, Lcom/adyen/core/utils/AsyncHttpClient;->post(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Lcom/adyen/core/interfaces/HttpResponseCallback;)V

    return-void
.end method

.method public onPaymentResult(Lcom/adyen/core/PaymentRequest;Lcom/adyen/core/models/PaymentRequestResult;)V
    .locals 5
    .param p1    # Lcom/adyen/core/PaymentRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/adyen/core/models/PaymentRequestResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 135
    invoke-virtual {p2}, Lcom/adyen/core/models/PaymentRequestResult;->isProcessed()Z

    move-result p1

    const-string v0, "status"

    const-string v1, "reference"

    if-eqz p1, :cond_1

    .line 136
    invoke-virtual {p2}, Lcom/adyen/core/models/PaymentRequestResult;->getPayment()Lcom/adyen/core/models/Payment;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adyen/core/models/Payment;->getPaymentStatus()Lcom/adyen/core/models/Payment$PaymentStatus;

    move-result-object p1

    sget-object v2, Lcom/adyen/core/models/Payment$PaymentStatus;->AUTHORISED:Lcom/adyen/core/models/Payment$PaymentStatus;

    if-eq p1, v2, :cond_0

    .line 137
    invoke-virtual {p2}, Lcom/adyen/core/models/PaymentRequestResult;->getPayment()Lcom/adyen/core/models/Payment;

    move-result-object p1

    invoke-virtual {p1}, Lcom/adyen/core/models/Payment;->getPaymentStatus()Lcom/adyen/core/models/Payment$PaymentStatus;

    move-result-object p1

    sget-object v2, Lcom/adyen/core/models/Payment$PaymentStatus;->RECEIVED:Lcom/adyen/core/models/Payment$PaymentStatus;

    if-ne p1, v2, :cond_1

    .line 138
    :cond_0
    new-instance p1, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {p1}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    const-string v2, "success"

    .line 139
    invoke-interface {p1, v0, v2}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    invoke-virtual {p2}, Lcom/adyen/core/models/PaymentRequestResult;->getPayment()Lcom/adyen/core/models/Payment;

    move-result-object p2

    invoke-virtual {p2}, Lcom/adyen/core/models/Payment;->getPayload()Ljava/lang/String;

    move-result-object p2

    const-string v0, "payload"

    invoke-interface {p1, v0, p2}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object p2, p0, Les/iagt/adyen/RNAdyenIagtModule$1;->this$0:Les/iagt/adyen/RNAdyenIagtModule;

    invoke-static {p2}, Les/iagt/adyen/RNAdyenIagtModule;->access$100(Les/iagt/adyen/RNAdyenIagtModule;)Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, v1, p2}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    iget-object p2, p0, Les/iagt/adyen/RNAdyenIagtModule$1;->val$promise:Lcom/facebook/react/bridge/Promise;

    invoke-interface {p2, p1}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 144
    :cond_1
    new-instance p1, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {p1}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    const-string v2, "error"

    .line 145
    invoke-interface {p1, v0, v2}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Les/iagt/adyen/RNAdyenIagtModule$1;->this$0:Les/iagt/adyen/RNAdyenIagtModule;

    invoke-static {v0}, Les/iagt/adyen/RNAdyenIagtModule;->access$100(Les/iagt/adyen/RNAdyenIagtModule;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-virtual {p2}, Lcom/adyen/core/models/PaymentRequestResult;->getError()Ljava/lang/Throwable;

    move-result-object v0

    const-string v2, "onPaymentResult: "

    if-eqz v0, :cond_2

    .line 148
    invoke-virtual {p2}, Lcom/adyen/core/models/PaymentRequestResult;->getError()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    const-string v3, "adyenError"

    invoke-interface {p1, v3, v0}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    invoke-static {}, Les/iagt/adyen/RNAdyenIagtModule;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/adyen/core/models/PaymentRequestResult;->getError()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    :cond_2
    invoke-virtual {p2}, Lcom/adyen/core/models/PaymentRequestResult;->isProcessed()Z

    move-result v0

    const-string v3, "errorCode"

    const-string v4, "message"

    if-nez v0, :cond_3

    const/16 p2, 0x3e9

    .line 152
    invoke-interface {p1, v3, p2}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    const-string p2, "Not processed"

    .line 153
    invoke-interface {p1, v4, p2}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-static {}, Les/iagt/adyen/RNAdyenIagtModule;->access$000()Ljava/lang/String;

    move-result-object p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1, v4}, Lcom/facebook/react/bridge/WritableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 155
    :cond_3
    invoke-virtual {p2}, Lcom/adyen/core/models/PaymentRequestResult;->getPayment()Lcom/adyen/core/models/Payment;

    move-result-object v0

    if-eqz v0, :cond_4

    const/16 v0, 0x3ea

    .line 156
    invoke-interface {p1, v3, v0}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    .line 157
    invoke-virtual {p2}, Lcom/adyen/core/models/PaymentRequestResult;->getPayment()Lcom/adyen/core/models/Payment;

    move-result-object p2

    invoke-virtual {p2}, Lcom/adyen/core/models/Payment;->getPaymentStatus()Lcom/adyen/core/models/Payment$PaymentStatus;

    move-result-object p2

    invoke-virtual {p2}, Lcom/adyen/core/models/Payment$PaymentStatus;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, v4, p2}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-static {}, Les/iagt/adyen/RNAdyenIagtModule;->access$000()Ljava/lang/String;

    move-result-object p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1, v4}, Lcom/facebook/react/bridge/WritableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const/16 p2, 0x3eb

    const-string v0, "errorcode"

    .line 160
    invoke-interface {p1, v0, p2}, Lcom/facebook/react/bridge/WritableMap;->putInt(Ljava/lang/String;I)V

    const-string p2, "Payment null"

    .line 161
    invoke-interface {p1, v4, p2}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-static {}, Les/iagt/adyen/RNAdyenIagtModule;->access$000()Ljava/lang/String;

    move-result-object p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1, v4}, Lcom/facebook/react/bridge/WritableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :goto_0
    invoke-static {}, Les/iagt/adyen/RNAdyenIagtModule;->access$000()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/WritableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget-object p2, p0, Les/iagt/adyen/RNAdyenIagtModule$1;->val$promise:Lcom/facebook/react/bridge/Promise;

    invoke-interface {p2, p1}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method
