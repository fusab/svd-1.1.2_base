.class Les/iagt/adyen/RNAdyenIagtModule$1$1;
.super Ljava/lang/Object;
.source "RNAdyenIagtModule.java"

# interfaces
.implements Lcom/adyen/core/interfaces/HttpResponseCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Les/iagt/adyen/RNAdyenIagtModule$1;->onPaymentDataRequested(Lcom/adyen/core/PaymentRequest;Ljava/lang/String;Lcom/adyen/core/interfaces/PaymentDataCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Les/iagt/adyen/RNAdyenIagtModule$1;

.field final synthetic val$callback:Lcom/adyen/core/interfaces/PaymentDataCallback;

.field final synthetic val$paymentRequest:Lcom/adyen/core/PaymentRequest;


# direct methods
.method constructor <init>(Les/iagt/adyen/RNAdyenIagtModule$1;Lcom/adyen/core/interfaces/PaymentDataCallback;Lcom/adyen/core/PaymentRequest;)V
    .locals 0

    .line 104
    iput-object p1, p0, Les/iagt/adyen/RNAdyenIagtModule$1$1;->this$1:Les/iagt/adyen/RNAdyenIagtModule$1;

    iput-object p2, p0, Les/iagt/adyen/RNAdyenIagtModule$1$1;->val$callback:Lcom/adyen/core/interfaces/PaymentDataCallback;

    iput-object p3, p0, Les/iagt/adyen/RNAdyenIagtModule$1$1;->val$paymentRequest:Lcom/adyen/core/PaymentRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .line 126
    iget-object v0, p0, Les/iagt/adyen/RNAdyenIagtModule$1$1;->val$paymentRequest:Lcom/adyen/core/PaymentRequest;

    invoke-virtual {v0}, Lcom/adyen/core/PaymentRequest;->cancel()V

    .line 127
    invoke-virtual {p1}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    const-string v0, "ErrorAdyen"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onSuccess([B)V
    .locals 6

    .line 108
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    new-instance v1, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 109
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 111
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->length()I

    move-result v3

    div-int/lit16 v3, v3, 0x3e8

    if-gt v2, v3, :cond_1

    mul-int/lit16 v3, v2, 0x3e8

    add-int/lit8 v2, v2, 0x1

    mul-int/lit16 v4, v2, 0x3e8

    .line 114
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-le v4, v5, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    .line 115
    :cond_0
    invoke-static {}, Les/iagt/adyen/RNAdyenIagtModule;->access$000()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 117
    :cond_1
    iget-object v1, p0, Les/iagt/adyen/RNAdyenIagtModule$1$1;->this$1:Les/iagt/adyen/RNAdyenIagtModule$1;

    iget-object v1, v1, Les/iagt/adyen/RNAdyenIagtModule$1;->this$0:Les/iagt/adyen/RNAdyenIagtModule;

    const-string v2, "payment"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "reference"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Les/iagt/adyen/RNAdyenIagtModule;->access$102(Les/iagt/adyen/RNAdyenIagtModule;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 119
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 121
    :goto_1
    iget-object v0, p0, Les/iagt/adyen/RNAdyenIagtModule$1$1;->val$callback:Lcom/adyen/core/interfaces/PaymentDataCallback;

    invoke-interface {v0, p1}, Lcom/adyen/core/interfaces/PaymentDataCallback;->completionWithPaymentData([B)V

    return-void
.end method
