.class public Les/iagt/adyen/RNAdyenIagtModule;
.super Lcom/facebook/react/bridge/ReactContextBaseJavaModule;
.source "RNAdyenIagtModule.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "RNAdyenIagtModule"


# instance fields
.field private final reactContext:Lcom/facebook/react/bridge/ReactApplicationContext;

.field private reference:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V
    .locals 1

    .line 42
    invoke-direct {p0, p1}, Lcom/facebook/react/bridge/ReactContextBaseJavaModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    const-string v0, ""

    .line 40
    iput-object v0, p0, Les/iagt/adyen/RNAdyenIagtModule;->reference:Ljava/lang/String;

    .line 43
    iput-object p1, p0, Les/iagt/adyen/RNAdyenIagtModule;->reactContext:Lcom/facebook/react/bridge/ReactApplicationContext;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .line 36
    sget-object v0, Les/iagt/adyen/RNAdyenIagtModule;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Les/iagt/adyen/RNAdyenIagtModule;)Ljava/lang/String;
    .locals 0

    .line 36
    iget-object p0, p0, Les/iagt/adyen/RNAdyenIagtModule;->reference:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$102(Les/iagt/adyen/RNAdyenIagtModule;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 36
    iput-object p1, p0, Les/iagt/adyen/RNAdyenIagtModule;->reference:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public doPayment(Lcom/facebook/react/bridge/ReadableMap;Lcom/facebook/react/bridge/ReadableMap;Lcom/facebook/react/bridge/Promise;)V
    .locals 3
    .param p2    # Lcom/facebook/react/bridge/ReadableMap;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .line 53
    sget-object v0, Les/iagt/adyen/RNAdyenIagtModule;->TAG:Ljava/lang/String;

    const-string v1, "doPayment: "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    new-instance v0, Lcom/adyen/core/PaymentRequest;

    invoke-virtual {p0}, Les/iagt/adyen/RNAdyenIagtModule;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Les/iagt/adyen/RNAdyenIagtModule;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Les/iagt/adyen/RNAdyenIagtModule;->reactContext:Lcom/facebook/react/bridge/ReactApplicationContext;

    invoke-virtual {v1}, Lcom/facebook/react/bridge/ReactApplicationContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    :goto_0
    new-instance v2, Les/iagt/adyen/RNAdyenIagtModule$1;

    invoke-direct {v2, p0, p2, p1, p3}, Les/iagt/adyen/RNAdyenIagtModule$1;-><init>(Les/iagt/adyen/RNAdyenIagtModule;Lcom/facebook/react/bridge/ReadableMap;Lcom/facebook/react/bridge/ReadableMap;Lcom/facebook/react/bridge/Promise;)V

    invoke-direct {v0, v1, v2}, Lcom/adyen/core/PaymentRequest;-><init>(Landroid/content/Context;Lcom/adyen/core/interfaces/PaymentRequestListener;)V

    .line 170
    invoke-virtual {v0}, Lcom/adyen/core/PaymentRequest;->start()V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "RNAdyenIagt"

    return-object v0
.end method
