.class public final enum Lio/branch/referral/util/CurrencyType;
.super Ljava/lang/Enum;
.source "CurrencyType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lio/branch/referral/util/CurrencyType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lio/branch/referral/util/CurrencyType;

.field public static final enum AED:Lio/branch/referral/util/CurrencyType;

.field public static final enum AFN:Lio/branch/referral/util/CurrencyType;

.field public static final enum ALL:Lio/branch/referral/util/CurrencyType;

.field public static final enum AMD:Lio/branch/referral/util/CurrencyType;

.field public static final enum ANG:Lio/branch/referral/util/CurrencyType;

.field public static final enum AOA:Lio/branch/referral/util/CurrencyType;

.field public static final enum ARS:Lio/branch/referral/util/CurrencyType;

.field public static final enum AUD:Lio/branch/referral/util/CurrencyType;

.field public static final enum AWG:Lio/branch/referral/util/CurrencyType;

.field public static final enum AZN:Lio/branch/referral/util/CurrencyType;

.field public static final enum BAM:Lio/branch/referral/util/CurrencyType;

.field public static final enum BBD:Lio/branch/referral/util/CurrencyType;

.field public static final enum BDT:Lio/branch/referral/util/CurrencyType;

.field public static final enum BGN:Lio/branch/referral/util/CurrencyType;

.field public static final enum BHD:Lio/branch/referral/util/CurrencyType;

.field public static final enum BIF:Lio/branch/referral/util/CurrencyType;

.field public static final enum BMD:Lio/branch/referral/util/CurrencyType;

.field public static final enum BND:Lio/branch/referral/util/CurrencyType;

.field public static final enum BOB:Lio/branch/referral/util/CurrencyType;

.field public static final enum BOV:Lio/branch/referral/util/CurrencyType;

.field public static final enum BRL:Lio/branch/referral/util/CurrencyType;

.field public static final enum BSD:Lio/branch/referral/util/CurrencyType;

.field public static final enum BTN:Lio/branch/referral/util/CurrencyType;

.field public static final enum BWP:Lio/branch/referral/util/CurrencyType;

.field public static final enum BYN:Lio/branch/referral/util/CurrencyType;

.field public static final enum BYR:Lio/branch/referral/util/CurrencyType;

.field public static final enum BZD:Lio/branch/referral/util/CurrencyType;

.field public static final enum CAD:Lio/branch/referral/util/CurrencyType;

.field public static final enum CDF:Lio/branch/referral/util/CurrencyType;

.field public static final enum CHE:Lio/branch/referral/util/CurrencyType;

.field public static final enum CHF:Lio/branch/referral/util/CurrencyType;

.field public static final enum CHW:Lio/branch/referral/util/CurrencyType;

.field public static final enum CLF:Lio/branch/referral/util/CurrencyType;

.field public static final enum CLP:Lio/branch/referral/util/CurrencyType;

.field public static final enum CNY:Lio/branch/referral/util/CurrencyType;

.field public static final enum COP:Lio/branch/referral/util/CurrencyType;

.field public static final enum COU:Lio/branch/referral/util/CurrencyType;

.field public static final enum CRC:Lio/branch/referral/util/CurrencyType;

.field public static final enum CUC:Lio/branch/referral/util/CurrencyType;

.field public static final enum CUP:Lio/branch/referral/util/CurrencyType;

.field public static final enum CVE:Lio/branch/referral/util/CurrencyType;

.field public static final enum CZK:Lio/branch/referral/util/CurrencyType;

.field public static final enum DJF:Lio/branch/referral/util/CurrencyType;

.field public static final enum DKK:Lio/branch/referral/util/CurrencyType;

.field public static final enum DOP:Lio/branch/referral/util/CurrencyType;

.field public static final enum DZD:Lio/branch/referral/util/CurrencyType;

.field public static final enum EGP:Lio/branch/referral/util/CurrencyType;

.field public static final enum ERN:Lio/branch/referral/util/CurrencyType;

.field public static final enum ETB:Lio/branch/referral/util/CurrencyType;

.field public static final enum EUR:Lio/branch/referral/util/CurrencyType;

.field public static final enum FJD:Lio/branch/referral/util/CurrencyType;

.field public static final enum FKP:Lio/branch/referral/util/CurrencyType;

.field public static final enum GBP:Lio/branch/referral/util/CurrencyType;

.field public static final enum GEL:Lio/branch/referral/util/CurrencyType;

.field public static final enum GHS:Lio/branch/referral/util/CurrencyType;

.field public static final enum GIP:Lio/branch/referral/util/CurrencyType;

.field public static final enum GMD:Lio/branch/referral/util/CurrencyType;

.field public static final enum GNF:Lio/branch/referral/util/CurrencyType;

.field public static final enum GTQ:Lio/branch/referral/util/CurrencyType;

.field public static final enum GYD:Lio/branch/referral/util/CurrencyType;

.field public static final enum HKD:Lio/branch/referral/util/CurrencyType;

.field public static final enum HNL:Lio/branch/referral/util/CurrencyType;

.field public static final enum HRK:Lio/branch/referral/util/CurrencyType;

.field public static final enum HTG:Lio/branch/referral/util/CurrencyType;

.field public static final enum HUF:Lio/branch/referral/util/CurrencyType;

.field public static final enum IDR:Lio/branch/referral/util/CurrencyType;

.field public static final enum ILS:Lio/branch/referral/util/CurrencyType;

.field public static final enum INR:Lio/branch/referral/util/CurrencyType;

.field public static final enum IQD:Lio/branch/referral/util/CurrencyType;

.field public static final enum IRR:Lio/branch/referral/util/CurrencyType;

.field public static final enum ISK:Lio/branch/referral/util/CurrencyType;

.field public static final enum JMD:Lio/branch/referral/util/CurrencyType;

.field public static final enum JOD:Lio/branch/referral/util/CurrencyType;

.field public static final enum JPY:Lio/branch/referral/util/CurrencyType;

.field public static final enum KES:Lio/branch/referral/util/CurrencyType;

.field public static final enum KGS:Lio/branch/referral/util/CurrencyType;

.field public static final enum KHR:Lio/branch/referral/util/CurrencyType;

.field public static final enum KMF:Lio/branch/referral/util/CurrencyType;

.field public static final enum KPW:Lio/branch/referral/util/CurrencyType;

.field public static final enum KRW:Lio/branch/referral/util/CurrencyType;

.field public static final enum KWD:Lio/branch/referral/util/CurrencyType;

.field public static final enum KYD:Lio/branch/referral/util/CurrencyType;

.field public static final enum KZT:Lio/branch/referral/util/CurrencyType;

.field public static final enum LAK:Lio/branch/referral/util/CurrencyType;

.field public static final enum LBP:Lio/branch/referral/util/CurrencyType;

.field public static final enum LKR:Lio/branch/referral/util/CurrencyType;

.field public static final enum LRD:Lio/branch/referral/util/CurrencyType;

.field public static final enum LSL:Lio/branch/referral/util/CurrencyType;

.field public static final enum LYD:Lio/branch/referral/util/CurrencyType;

.field public static final enum MAD:Lio/branch/referral/util/CurrencyType;

.field public static final enum MDL:Lio/branch/referral/util/CurrencyType;

.field public static final enum MGA:Lio/branch/referral/util/CurrencyType;

.field public static final enum MKD:Lio/branch/referral/util/CurrencyType;

.field public static final enum MMK:Lio/branch/referral/util/CurrencyType;

.field public static final enum MNT:Lio/branch/referral/util/CurrencyType;

.field public static final enum MOP:Lio/branch/referral/util/CurrencyType;

.field public static final enum MRO:Lio/branch/referral/util/CurrencyType;

.field public static final enum MUR:Lio/branch/referral/util/CurrencyType;

.field public static final enum MVR:Lio/branch/referral/util/CurrencyType;

.field public static final enum MWK:Lio/branch/referral/util/CurrencyType;

.field public static final enum MXN:Lio/branch/referral/util/CurrencyType;

.field public static final enum MXV:Lio/branch/referral/util/CurrencyType;

.field public static final enum MYR:Lio/branch/referral/util/CurrencyType;

.field public static final enum MZN:Lio/branch/referral/util/CurrencyType;

.field public static final enum NAD:Lio/branch/referral/util/CurrencyType;

.field public static final enum NGN:Lio/branch/referral/util/CurrencyType;

.field public static final enum NIO:Lio/branch/referral/util/CurrencyType;

.field public static final enum NOK:Lio/branch/referral/util/CurrencyType;

.field public static final enum NPR:Lio/branch/referral/util/CurrencyType;

.field public static final enum NZD:Lio/branch/referral/util/CurrencyType;

.field public static final enum OMR:Lio/branch/referral/util/CurrencyType;

.field public static final enum PAB:Lio/branch/referral/util/CurrencyType;

.field public static final enum PEN:Lio/branch/referral/util/CurrencyType;

.field public static final enum PGK:Lio/branch/referral/util/CurrencyType;

.field public static final enum PHP:Lio/branch/referral/util/CurrencyType;

.field public static final enum PKR:Lio/branch/referral/util/CurrencyType;

.field public static final enum PLN:Lio/branch/referral/util/CurrencyType;

.field public static final enum PYG:Lio/branch/referral/util/CurrencyType;

.field public static final enum QAR:Lio/branch/referral/util/CurrencyType;

.field public static final enum RON:Lio/branch/referral/util/CurrencyType;

.field public static final enum RSD:Lio/branch/referral/util/CurrencyType;

.field public static final enum RUB:Lio/branch/referral/util/CurrencyType;

.field public static final enum RWF:Lio/branch/referral/util/CurrencyType;

.field public static final enum SAR:Lio/branch/referral/util/CurrencyType;

.field public static final enum SBD:Lio/branch/referral/util/CurrencyType;

.field public static final enum SCR:Lio/branch/referral/util/CurrencyType;

.field public static final enum SDG:Lio/branch/referral/util/CurrencyType;

.field public static final enum SEK:Lio/branch/referral/util/CurrencyType;

.field public static final enum SGD:Lio/branch/referral/util/CurrencyType;

.field public static final enum SHP:Lio/branch/referral/util/CurrencyType;

.field public static final enum SLL:Lio/branch/referral/util/CurrencyType;

.field public static final enum SOS:Lio/branch/referral/util/CurrencyType;

.field public static final enum SRD:Lio/branch/referral/util/CurrencyType;

.field public static final enum SSP:Lio/branch/referral/util/CurrencyType;

.field public static final enum STD:Lio/branch/referral/util/CurrencyType;

.field public static final enum SYP:Lio/branch/referral/util/CurrencyType;

.field public static final enum SZL:Lio/branch/referral/util/CurrencyType;

.field public static final enum THB:Lio/branch/referral/util/CurrencyType;

.field public static final enum TJS:Lio/branch/referral/util/CurrencyType;

.field public static final enum TMT:Lio/branch/referral/util/CurrencyType;

.field public static final enum TND:Lio/branch/referral/util/CurrencyType;

.field public static final enum TOP:Lio/branch/referral/util/CurrencyType;

.field public static final enum TRY:Lio/branch/referral/util/CurrencyType;

.field public static final enum TTD:Lio/branch/referral/util/CurrencyType;

.field public static final enum TWD:Lio/branch/referral/util/CurrencyType;

.field public static final enum TZS:Lio/branch/referral/util/CurrencyType;

.field public static final enum UAH:Lio/branch/referral/util/CurrencyType;

.field public static final enum UGX:Lio/branch/referral/util/CurrencyType;

.field public static final enum USD:Lio/branch/referral/util/CurrencyType;

.field public static final enum USN:Lio/branch/referral/util/CurrencyType;

.field public static final enum UYI:Lio/branch/referral/util/CurrencyType;

.field public static final enum UYU:Lio/branch/referral/util/CurrencyType;

.field public static final enum UZS:Lio/branch/referral/util/CurrencyType;

.field public static final enum VEF:Lio/branch/referral/util/CurrencyType;

.field public static final enum VND:Lio/branch/referral/util/CurrencyType;

.field public static final enum VUV:Lio/branch/referral/util/CurrencyType;

.field public static final enum WST:Lio/branch/referral/util/CurrencyType;

.field public static final enum XAF:Lio/branch/referral/util/CurrencyType;

.field public static final enum XAG:Lio/branch/referral/util/CurrencyType;

.field public static final enum XAU:Lio/branch/referral/util/CurrencyType;

.field public static final enum XBA:Lio/branch/referral/util/CurrencyType;

.field public static final enum XBB:Lio/branch/referral/util/CurrencyType;

.field public static final enum XBC:Lio/branch/referral/util/CurrencyType;

.field public static final enum XBD:Lio/branch/referral/util/CurrencyType;

.field public static final enum XCD:Lio/branch/referral/util/CurrencyType;

.field public static final enum XDR:Lio/branch/referral/util/CurrencyType;

.field public static final enum XFU:Lio/branch/referral/util/CurrencyType;

.field public static final enum XOF:Lio/branch/referral/util/CurrencyType;

.field public static final enum XPD:Lio/branch/referral/util/CurrencyType;

.field public static final enum XPF:Lio/branch/referral/util/CurrencyType;

.field public static final enum XPT:Lio/branch/referral/util/CurrencyType;

.field public static final enum XSU:Lio/branch/referral/util/CurrencyType;

.field public static final enum XTS:Lio/branch/referral/util/CurrencyType;

.field public static final enum XUA:Lio/branch/referral/util/CurrencyType;

.field public static final enum XXX:Lio/branch/referral/util/CurrencyType;

.field public static final enum YER:Lio/branch/referral/util/CurrencyType;

.field public static final enum ZAR:Lio/branch/referral/util/CurrencyType;

.field public static final enum ZMW:Lio/branch/referral/util/CurrencyType;


# instance fields
.field private iso4217Code:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 10
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const/4 v1, 0x0

    const-string v2, "AED"

    invoke-direct {v0, v2, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->AED:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const/4 v2, 0x1

    const-string v3, "AFN"

    invoke-direct {v0, v3, v2, v3}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->AFN:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const/4 v3, 0x2

    const-string v4, "ALL"

    invoke-direct {v0, v4, v3, v4}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->ALL:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const/4 v4, 0x3

    const-string v5, "AMD"

    invoke-direct {v0, v5, v4, v5}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->AMD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const/4 v5, 0x4

    const-string v6, "ANG"

    invoke-direct {v0, v6, v5, v6}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->ANG:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const/4 v6, 0x5

    const-string v7, "AOA"

    invoke-direct {v0, v7, v6, v7}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->AOA:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const/4 v7, 0x6

    const-string v8, "ARS"

    invoke-direct {v0, v8, v7, v8}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->ARS:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "AUD"

    const/4 v9, 0x7

    invoke-direct {v0, v8, v9, v8}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->AUD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "AWG"

    const/16 v9, 0x8

    const-string v10, "AWG"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->AWG:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "AZN"

    const/16 v9, 0x9

    const-string v10, "AZN"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->AZN:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "BAM"

    const/16 v9, 0xa

    const-string v10, "BAM"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->BAM:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "BBD"

    const/16 v9, 0xb

    const-string v10, "BBD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->BBD:Lio/branch/referral/util/CurrencyType;

    .line 11
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "BDT"

    const/16 v9, 0xc

    const-string v10, "BDT"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->BDT:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "BGN"

    const/16 v9, 0xd

    const-string v10, "BGN"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->BGN:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "BHD"

    const/16 v9, 0xe

    const-string v10, "BHD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->BHD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "BIF"

    const/16 v9, 0xf

    const-string v10, "BIF"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->BIF:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "BMD"

    const/16 v9, 0x10

    const-string v10, "BMD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->BMD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "BND"

    const/16 v9, 0x11

    const-string v10, "BND"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->BND:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "BOB"

    const/16 v9, 0x12

    const-string v10, "BOB"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->BOB:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "BOV"

    const/16 v9, 0x13

    const-string v10, "BOV"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->BOV:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "BRL"

    const/16 v9, 0x14

    const-string v10, "BRL"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->BRL:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "BSD"

    const/16 v9, 0x15

    const-string v10, "BSD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->BSD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "BTN"

    const/16 v9, 0x16

    const-string v10, "BTN"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->BTN:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "BWP"

    const/16 v9, 0x17

    const-string v10, "BWP"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->BWP:Lio/branch/referral/util/CurrencyType;

    .line 12
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "BYN"

    const/16 v9, 0x18

    const-string v10, "BYN"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->BYN:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "BYR"

    const/16 v9, 0x19

    const-string v10, "BYR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->BYR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "BZD"

    const/16 v9, 0x1a

    const-string v10, "BZD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->BZD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "CAD"

    const/16 v9, 0x1b

    const-string v10, "CAD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->CAD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "CDF"

    const/16 v9, 0x1c

    const-string v10, "CDF"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->CDF:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "CHE"

    const/16 v9, 0x1d

    const-string v10, "CHE"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->CHE:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "CHF"

    const/16 v9, 0x1e

    const-string v10, "CHF"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->CHF:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "CHW"

    const/16 v9, 0x1f

    const-string v10, "CHW"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->CHW:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "CLF"

    const/16 v9, 0x20

    const-string v10, "CLF"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->CLF:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "CLP"

    const/16 v9, 0x21

    const-string v10, "CLP"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->CLP:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "CNY"

    const/16 v9, 0x22

    const-string v10, "CNY"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->CNY:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "COP"

    const/16 v9, 0x23

    const-string v10, "COP"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->COP:Lio/branch/referral/util/CurrencyType;

    .line 13
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "COU"

    const/16 v9, 0x24

    const-string v10, "COU"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->COU:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "CRC"

    const/16 v9, 0x25

    const-string v10, "CRC"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->CRC:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "CUC"

    const/16 v9, 0x26

    const-string v10, "CUC"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->CUC:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "CUP"

    const/16 v9, 0x27

    const-string v10, "CUP"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->CUP:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "CVE"

    const/16 v9, 0x28

    const-string v10, "CVE"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->CVE:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "CZK"

    const/16 v9, 0x29

    const-string v10, "CZK"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->CZK:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "DJF"

    const/16 v9, 0x2a

    const-string v10, "DJF"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->DJF:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "DKK"

    const/16 v9, 0x2b

    const-string v10, "DKK"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->DKK:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "DOP"

    const/16 v9, 0x2c

    const-string v10, "DOP"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->DOP:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "DZD"

    const/16 v9, 0x2d

    const-string v10, "DZD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->DZD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "EGP"

    const/16 v9, 0x2e

    const-string v10, "EGP"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->EGP:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "ERN"

    const/16 v9, 0x2f

    const-string v10, "ERN"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->ERN:Lio/branch/referral/util/CurrencyType;

    .line 14
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "ETB"

    const/16 v9, 0x30

    const-string v10, "ETB"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->ETB:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "EUR"

    const/16 v9, 0x31

    const-string v10, "EUR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->EUR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "FJD"

    const/16 v9, 0x32

    const-string v10, "FJD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->FJD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "FKP"

    const/16 v9, 0x33

    const-string v10, "FKP"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->FKP:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "GBP"

    const/16 v9, 0x34

    const-string v10, "GBP"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->GBP:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "GEL"

    const/16 v9, 0x35

    const-string v10, "GEL"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->GEL:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "GHS"

    const/16 v9, 0x36

    const-string v10, "GHS"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->GHS:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "GIP"

    const/16 v9, 0x37

    const-string v10, "GIP"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->GIP:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "GMD"

    const/16 v9, 0x38

    const-string v10, "GMD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->GMD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "GNF"

    const/16 v9, 0x39

    const-string v10, "GNF"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->GNF:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "GTQ"

    const/16 v9, 0x3a

    const-string v10, "GTQ"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->GTQ:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "GYD"

    const/16 v9, 0x3b

    const-string v10, "GYD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->GYD:Lio/branch/referral/util/CurrencyType;

    .line 15
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "HKD"

    const/16 v9, 0x3c

    const-string v10, "HKD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->HKD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "HNL"

    const/16 v9, 0x3d

    const-string v10, "HNL"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->HNL:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "HRK"

    const/16 v9, 0x3e

    const-string v10, "HRK"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->HRK:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "HTG"

    const/16 v9, 0x3f

    const-string v10, "HTG"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->HTG:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "HUF"

    const/16 v9, 0x40

    const-string v10, "HUF"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->HUF:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "IDR"

    const/16 v9, 0x41

    const-string v10, "IDR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->IDR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "ILS"

    const/16 v9, 0x42

    const-string v10, "ILS"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->ILS:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "INR"

    const/16 v9, 0x43

    const-string v10, "INR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->INR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "IQD"

    const/16 v9, 0x44

    const-string v10, "IQD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->IQD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "IRR"

    const/16 v9, 0x45

    const-string v10, "IRR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->IRR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "ISK"

    const/16 v9, 0x46

    const-string v10, "ISK"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->ISK:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "JMD"

    const/16 v9, 0x47

    const-string v10, "JMD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->JMD:Lio/branch/referral/util/CurrencyType;

    .line 16
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "JOD"

    const/16 v9, 0x48

    const-string v10, "JOD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->JOD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "JPY"

    const/16 v9, 0x49

    const-string v10, "JPY"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->JPY:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "KES"

    const/16 v9, 0x4a

    const-string v10, "KES"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->KES:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "KGS"

    const/16 v9, 0x4b

    const-string v10, "KGS"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->KGS:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "KHR"

    const/16 v9, 0x4c

    const-string v10, "KHR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->KHR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "KMF"

    const/16 v9, 0x4d

    const-string v10, "KMF"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->KMF:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "KPW"

    const/16 v9, 0x4e

    const-string v10, "KPW"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->KPW:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "KRW"

    const/16 v9, 0x4f

    const-string v10, "KRW"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->KRW:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "KWD"

    const/16 v9, 0x50

    const-string v10, "KWD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->KWD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "KYD"

    const/16 v9, 0x51

    const-string v10, "KYD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->KYD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "KZT"

    const/16 v9, 0x52

    const-string v10, "KZT"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->KZT:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "LAK"

    const/16 v9, 0x53

    const-string v10, "LAK"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->LAK:Lio/branch/referral/util/CurrencyType;

    .line 17
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "LBP"

    const/16 v9, 0x54

    const-string v10, "LBP"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->LBP:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "LKR"

    const/16 v9, 0x55

    const-string v10, "LKR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->LKR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "LRD"

    const/16 v9, 0x56

    const-string v10, "LRD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->LRD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "LSL"

    const/16 v9, 0x57

    const-string v10, "LSL"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->LSL:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "LYD"

    const/16 v9, 0x58

    const-string v10, "LYD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->LYD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "MAD"

    const/16 v9, 0x59

    const-string v10, "MAD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->MAD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "MDL"

    const/16 v9, 0x5a

    const-string v10, "MDL"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->MDL:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "MGA"

    const/16 v9, 0x5b

    const-string v10, "MGA"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->MGA:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "MKD"

    const/16 v9, 0x5c

    const-string v10, "MKD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->MKD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "MMK"

    const/16 v9, 0x5d

    const-string v10, "MMK"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->MMK:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "MNT"

    const/16 v9, 0x5e

    const-string v10, "MNT"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->MNT:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "MOP"

    const/16 v9, 0x5f

    const-string v10, "MOP"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->MOP:Lio/branch/referral/util/CurrencyType;

    .line 18
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "MRO"

    const/16 v9, 0x60

    const-string v10, "MRO"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->MRO:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "MUR"

    const/16 v9, 0x61

    const-string v10, "MUR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->MUR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "MVR"

    const/16 v9, 0x62

    const-string v10, "MVR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->MVR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "MWK"

    const/16 v9, 0x63

    const-string v10, "MWK"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->MWK:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "MXN"

    const/16 v9, 0x64

    const-string v10, "MXN"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->MXN:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "MXV"

    const/16 v9, 0x65

    const-string v10, "MXV"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->MXV:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "MYR"

    const/16 v9, 0x66

    const-string v10, "MYR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->MYR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "MZN"

    const/16 v9, 0x67

    const-string v10, "MZN"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->MZN:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "NAD"

    const/16 v9, 0x68

    const-string v10, "NAD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->NAD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "NGN"

    const/16 v9, 0x69

    const-string v10, "NGN"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->NGN:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "NIO"

    const/16 v9, 0x6a

    const-string v10, "NIO"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->NIO:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "NOK"

    const/16 v9, 0x6b

    const-string v10, "NOK"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->NOK:Lio/branch/referral/util/CurrencyType;

    .line 19
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "NPR"

    const/16 v9, 0x6c

    const-string v10, "NPR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->NPR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "NZD"

    const/16 v9, 0x6d

    const-string v10, "NZD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->NZD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "OMR"

    const/16 v9, 0x6e

    const-string v10, "OMR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->OMR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "PAB"

    const/16 v9, 0x6f

    const-string v10, "PAB"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->PAB:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "PEN"

    const/16 v9, 0x70

    const-string v10, "PEN"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->PEN:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "PGK"

    const/16 v9, 0x71

    const-string v10, "PGK"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->PGK:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "PHP"

    const/16 v9, 0x72

    const-string v10, "PHP"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->PHP:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "PKR"

    const/16 v9, 0x73

    const-string v10, "PKR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->PKR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "PLN"

    const/16 v9, 0x74

    const-string v10, "PLN"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->PLN:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "PYG"

    const/16 v9, 0x75

    const-string v10, "PYG"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->PYG:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "QAR"

    const/16 v9, 0x76

    const-string v10, "QAR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->QAR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "RON"

    const/16 v9, 0x77

    const-string v10, "RON"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->RON:Lio/branch/referral/util/CurrencyType;

    .line 20
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "RSD"

    const/16 v9, 0x78

    const-string v10, "RSD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->RSD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "RUB"

    const/16 v9, 0x79

    const-string v10, "RUB"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->RUB:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "RWF"

    const/16 v9, 0x7a

    const-string v10, "RWF"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->RWF:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "SAR"

    const/16 v9, 0x7b

    const-string v10, "SAR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->SAR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "SBD"

    const/16 v9, 0x7c

    const-string v10, "SBD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->SBD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "SCR"

    const/16 v9, 0x7d

    const-string v10, "SCR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->SCR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "SDG"

    const/16 v9, 0x7e

    const-string v10, "SDG"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->SDG:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "SEK"

    const/16 v9, 0x7f

    const-string v10, "SEK"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->SEK:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "SGD"

    const/16 v9, 0x80

    const-string v10, "SGD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->SGD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "SHP"

    const/16 v9, 0x81

    const-string v10, "SHP"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->SHP:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "SLL"

    const/16 v9, 0x82

    const-string v10, "SLL"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->SLL:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "SOS"

    const/16 v9, 0x83

    const-string v10, "SOS"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->SOS:Lio/branch/referral/util/CurrencyType;

    .line 21
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "SRD"

    const/16 v9, 0x84

    const-string v10, "SRD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->SRD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "SSP"

    const/16 v9, 0x85

    const-string v10, "SSP"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->SSP:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "STD"

    const/16 v9, 0x86

    const-string v10, "STD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->STD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "SYP"

    const/16 v9, 0x87

    const-string v10, "SYP"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->SYP:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "SZL"

    const/16 v9, 0x88

    const-string v10, "SZL"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->SZL:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "THB"

    const/16 v9, 0x89

    const-string v10, "THB"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->THB:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "TJS"

    const/16 v9, 0x8a

    const-string v10, "TJS"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->TJS:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "TMT"

    const/16 v9, 0x8b

    const-string v10, "TMT"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->TMT:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "TND"

    const/16 v9, 0x8c

    const-string v10, "TND"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->TND:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "TOP"

    const/16 v9, 0x8d

    const-string v10, "TOP"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->TOP:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "TRY"

    const/16 v9, 0x8e

    const-string v10, "TRY"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->TRY:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "TTD"

    const/16 v9, 0x8f

    const-string v10, "TTD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->TTD:Lio/branch/referral/util/CurrencyType;

    .line 22
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "TWD"

    const/16 v9, 0x90

    const-string v10, "TWD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->TWD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "TZS"

    const/16 v9, 0x91

    const-string v10, "TZS"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->TZS:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "UAH"

    const/16 v9, 0x92

    const-string v10, "UAH"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->UAH:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "UGX"

    const/16 v9, 0x93

    const-string v10, "UGX"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->UGX:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "USD"

    const/16 v9, 0x94

    const-string v10, "USD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->USD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "USN"

    const/16 v9, 0x95

    const-string v10, "USN"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->USN:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "UYI"

    const/16 v9, 0x96

    const-string v10, "UYI"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->UYI:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "UYU"

    const/16 v9, 0x97

    const-string v10, "UYU"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->UYU:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "UZS"

    const/16 v9, 0x98

    const-string v10, "UZS"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->UZS:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "VEF"

    const/16 v9, 0x99

    const-string v10, "VEF"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->VEF:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "VND"

    const/16 v9, 0x9a

    const-string v10, "VND"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->VND:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "VUV"

    const/16 v9, 0x9b

    const-string v10, "VUV"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->VUV:Lio/branch/referral/util/CurrencyType;

    .line 23
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "WST"

    const/16 v9, 0x9c

    const-string v10, "WST"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->WST:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XAF"

    const/16 v9, 0x9d

    const-string v10, "XAF"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XAF:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XAG"

    const/16 v9, 0x9e

    const-string v10, "XAG"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XAG:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XAU"

    const/16 v9, 0x9f

    const-string v10, "XAU"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XAU:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XBA"

    const/16 v9, 0xa0

    const-string v10, "XBA"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XBA:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XBB"

    const/16 v9, 0xa1

    const-string v10, "XBB"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XBB:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XBC"

    const/16 v9, 0xa2

    const-string v10, "XBC"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XBC:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XBD"

    const/16 v9, 0xa3

    const-string v10, "XBD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XBD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XCD"

    const/16 v9, 0xa4

    const-string v10, "XCD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XCD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XDR"

    const/16 v9, 0xa5

    const-string v10, "XDR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XDR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XFU"

    const/16 v9, 0xa6

    const-string v10, "XFU"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XFU:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XOF"

    const/16 v9, 0xa7

    const-string v10, "XOF"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XOF:Lio/branch/referral/util/CurrencyType;

    .line 24
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XPD"

    const/16 v9, 0xa8

    const-string v10, "XPD"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XPD:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XPF"

    const/16 v9, 0xa9

    const-string v10, "XPF"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XPF:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XPT"

    const/16 v9, 0xaa

    const-string v10, "XPT"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XPT:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XSU"

    const/16 v9, 0xab

    const-string v10, "XSU"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XSU:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XTS"

    const/16 v9, 0xac

    const-string v10, "XTS"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XTS:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XUA"

    const/16 v9, 0xad

    const-string v10, "XUA"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XUA:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "XXX"

    const/16 v9, 0xae

    const-string v10, "XXX"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->XXX:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "YER"

    const/16 v9, 0xaf

    const-string v10, "YER"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->YER:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "ZAR"

    const/16 v9, 0xb0

    const-string v10, "ZAR"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->ZAR:Lio/branch/referral/util/CurrencyType;

    new-instance v0, Lio/branch/referral/util/CurrencyType;

    const-string v8, "ZMW"

    const/16 v9, 0xb1

    const-string v10, "ZMW"

    invoke-direct {v0, v8, v9, v10}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/CurrencyType;->ZMW:Lio/branch/referral/util/CurrencyType;

    const/16 v0, 0xb2

    .line 9
    new-array v0, v0, [Lio/branch/referral/util/CurrencyType;

    sget-object v8, Lio/branch/referral/util/CurrencyType;->AED:Lio/branch/referral/util/CurrencyType;

    aput-object v8, v0, v1

    sget-object v1, Lio/branch/referral/util/CurrencyType;->AFN:Lio/branch/referral/util/CurrencyType;

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->ALL:Lio/branch/referral/util/CurrencyType;

    aput-object v1, v0, v3

    sget-object v1, Lio/branch/referral/util/CurrencyType;->AMD:Lio/branch/referral/util/CurrencyType;

    aput-object v1, v0, v4

    sget-object v1, Lio/branch/referral/util/CurrencyType;->ANG:Lio/branch/referral/util/CurrencyType;

    aput-object v1, v0, v5

    sget-object v1, Lio/branch/referral/util/CurrencyType;->AOA:Lio/branch/referral/util/CurrencyType;

    aput-object v1, v0, v6

    sget-object v1, Lio/branch/referral/util/CurrencyType;->ARS:Lio/branch/referral/util/CurrencyType;

    aput-object v1, v0, v7

    sget-object v1, Lio/branch/referral/util/CurrencyType;->AUD:Lio/branch/referral/util/CurrencyType;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->AWG:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->AZN:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->BAM:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->BBD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->BDT:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->BGN:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->BHD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->BIF:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->BMD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->BND:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->BOB:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->BOV:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->BRL:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->BSD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->BTN:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->BWP:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->BYN:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->BYR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->BZD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->CAD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->CDF:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->CHE:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->CHF:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->CHW:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->CLF:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->CLP:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->CNY:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->COP:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->COU:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->CRC:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->CUC:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->CUP:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->CVE:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->CZK:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->DJF:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->DKK:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->DOP:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->DZD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->EGP:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->ERN:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->ETB:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->EUR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->FJD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->FKP:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->GBP:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->GEL:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->GHS:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->GIP:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->GMD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->GNF:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->GTQ:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->GYD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->HKD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->HNL:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->HRK:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->HTG:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->HUF:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->IDR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->ILS:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->INR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->IQD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->IRR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->ISK:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->JMD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->JOD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->JPY:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->KES:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->KGS:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->KHR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->KMF:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->KPW:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->KRW:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->KWD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->KYD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->KZT:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->LAK:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->LBP:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->LKR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->LRD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->LSL:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->LYD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->MAD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->MDL:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->MGA:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->MKD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->MMK:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->MNT:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->MOP:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->MRO:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->MUR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->MVR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->MWK:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->MXN:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->MXV:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->MYR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->MZN:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->NAD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->NGN:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->NIO:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->NOK:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->NPR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->NZD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->OMR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->PAB:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->PEN:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->PGK:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->PHP:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->PKR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->PLN:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->PYG:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->QAR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->RON:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->RSD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->RUB:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->RWF:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->SAR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->SBD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->SCR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->SDG:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->SEK:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->SGD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->SHP:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->SLL:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->SOS:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->SRD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->SSP:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->STD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->SYP:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->SZL:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->THB:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->TJS:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->TMT:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->TND:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->TOP:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->TRY:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->TTD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->TWD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->TZS:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->UAH:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->UGX:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->USD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->USN:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->UYI:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->UYU:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->UZS:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->VEF:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->VND:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->VUV:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->WST:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XAF:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XAG:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XAU:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XBA:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XBB:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XBC:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XBD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XCD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XDR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XFU:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XOF:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XPD:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XPF:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XPT:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XSU:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XTS:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XUA:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->XXX:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->YER:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->ZAR:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/CurrencyType;->ZMW:Lio/branch/referral/util/CurrencyType;

    const/16 v2, 0xb1

    aput-object v1, v0, v2

    sput-object v0, Lio/branch/referral/util/CurrencyType;->$VALUES:[Lio/branch/referral/util/CurrencyType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    const-string p1, ""

    .line 26
    iput-object p1, p0, Lio/branch/referral/util/CurrencyType;->iso4217Code:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Lio/branch/referral/util/CurrencyType;->iso4217Code:Ljava/lang/String;

    return-void
.end method

.method public static getValue(Ljava/lang/String;)Lio/branch/referral/util/CurrencyType;
    .locals 5

    .line 39
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 40
    invoke-static {}, Lio/branch/referral/util/CurrencyType;->values()[Lio/branch/referral/util/CurrencyType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 41
    iget-object v4, v3, Lio/branch/referral/util/CurrencyType;->iso4217Code:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_1
    return-object v3
.end method

.method public static valueOf(Ljava/lang/String;)Lio/branch/referral/util/CurrencyType;
    .locals 1

    .line 9
    const-class v0, Lio/branch/referral/util/CurrencyType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lio/branch/referral/util/CurrencyType;

    return-object p0
.end method

.method public static values()[Lio/branch/referral/util/CurrencyType;
    .locals 1

    .line 9
    sget-object v0, Lio/branch/referral/util/CurrencyType;->$VALUES:[Lio/branch/referral/util/CurrencyType;

    invoke-virtual {v0}, [Lio/branch/referral/util/CurrencyType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/branch/referral/util/CurrencyType;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 34
    iget-object v0, p0, Lio/branch/referral/util/CurrencyType;->iso4217Code:Ljava/lang/String;

    return-object v0
.end method
