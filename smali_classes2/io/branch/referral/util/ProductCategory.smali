.class public final enum Lio/branch/referral/util/ProductCategory;
.super Ljava/lang/Enum;
.source "ProductCategory.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lio/branch/referral/util/ProductCategory;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lio/branch/referral/util/ProductCategory;

.field public static final enum ANIMALS_AND_PET_SUPPLIES:Lio/branch/referral/util/ProductCategory;

.field public static final enum APPAREL_AND_ACCESSORIES:Lio/branch/referral/util/ProductCategory;

.field public static final enum ARTS_AND_ENTERTAINMENT:Lio/branch/referral/util/ProductCategory;

.field public static final enum BABY_AND_TODDLER:Lio/branch/referral/util/ProductCategory;

.field public static final enum BUSINESS_AND_INDUSTRIAL:Lio/branch/referral/util/ProductCategory;

.field public static final enum CAMERAS_AND_OPTICS:Lio/branch/referral/util/ProductCategory;

.field public static final enum ELECTRONICS:Lio/branch/referral/util/ProductCategory;

.field public static final enum FOOD_BEVERAGES_AND_TOBACCO:Lio/branch/referral/util/ProductCategory;

.field public static final enum FURNITURE:Lio/branch/referral/util/ProductCategory;

.field public static final enum HARDWARE:Lio/branch/referral/util/ProductCategory;

.field public static final enum HEALTH_AND_BEAUTY:Lio/branch/referral/util/ProductCategory;

.field public static final enum HOME_AND_GARDEN:Lio/branch/referral/util/ProductCategory;

.field public static final enum LUGGAGE_AND_BAGS:Lio/branch/referral/util/ProductCategory;

.field public static final enum MATURE:Lio/branch/referral/util/ProductCategory;

.field public static final enum MEDIA:Lio/branch/referral/util/ProductCategory;

.field public static final enum OFFICE_SUPPLIES:Lio/branch/referral/util/ProductCategory;

.field public static final enum RELIGIOUS_AND_CEREMONIAL:Lio/branch/referral/util/ProductCategory;

.field public static final enum SOFTWARE:Lio/branch/referral/util/ProductCategory;

.field public static final enum SPORTING_GOODS:Lio/branch/referral/util/ProductCategory;

.field public static final enum TOYS_AND_GAMES:Lio/branch/referral/util/ProductCategory;

.field public static final enum VEHICLES_AND_PARTS:Lio/branch/referral/util/ProductCategory;


# instance fields
.field private name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 13
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const/4 v1, 0x0

    const-string v2, "ANIMALS_AND_PET_SUPPLIES"

    const-string v3, "Animals & Pet Supplies"

    invoke-direct {v0, v2, v1, v3}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->ANIMALS_AND_PET_SUPPLIES:Lio/branch/referral/util/ProductCategory;

    .line 14
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const/4 v2, 0x1

    const-string v3, "APPAREL_AND_ACCESSORIES"

    const-string v4, "Apparel & Accessories"

    invoke-direct {v0, v3, v2, v4}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->APPAREL_AND_ACCESSORIES:Lio/branch/referral/util/ProductCategory;

    .line 15
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const/4 v3, 0x2

    const-string v4, "ARTS_AND_ENTERTAINMENT"

    const-string v5, "Arts & Entertainment"

    invoke-direct {v0, v4, v3, v5}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->ARTS_AND_ENTERTAINMENT:Lio/branch/referral/util/ProductCategory;

    .line 16
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const/4 v4, 0x3

    const-string v5, "BABY_AND_TODDLER"

    const-string v6, "Baby & Toddler"

    invoke-direct {v0, v5, v4, v6}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->BABY_AND_TODDLER:Lio/branch/referral/util/ProductCategory;

    .line 17
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const/4 v5, 0x4

    const-string v6, "BUSINESS_AND_INDUSTRIAL"

    const-string v7, "Business & Industrial"

    invoke-direct {v0, v6, v5, v7}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->BUSINESS_AND_INDUSTRIAL:Lio/branch/referral/util/ProductCategory;

    .line 18
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const/4 v6, 0x5

    const-string v7, "CAMERAS_AND_OPTICS"

    const-string v8, "Cameras & Optics"

    invoke-direct {v0, v7, v6, v8}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->CAMERAS_AND_OPTICS:Lio/branch/referral/util/ProductCategory;

    .line 19
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const/4 v7, 0x6

    const-string v8, "ELECTRONICS"

    const-string v9, "Electronics"

    invoke-direct {v0, v8, v7, v9}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->ELECTRONICS:Lio/branch/referral/util/ProductCategory;

    .line 20
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const/4 v8, 0x7

    const-string v9, "FOOD_BEVERAGES_AND_TOBACCO"

    const-string v10, "Food, Beverages & Tobacco"

    invoke-direct {v0, v9, v8, v10}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->FOOD_BEVERAGES_AND_TOBACCO:Lio/branch/referral/util/ProductCategory;

    .line 21
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const/16 v9, 0x8

    const-string v10, "FURNITURE"

    const-string v11, "Furniture"

    invoke-direct {v0, v10, v9, v11}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->FURNITURE:Lio/branch/referral/util/ProductCategory;

    .line 22
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const/16 v10, 0x9

    const-string v11, "HARDWARE"

    const-string v12, "Hardware"

    invoke-direct {v0, v11, v10, v12}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->HARDWARE:Lio/branch/referral/util/ProductCategory;

    .line 23
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const/16 v11, 0xa

    const-string v12, "HEALTH_AND_BEAUTY"

    const-string v13, "Health & Beauty"

    invoke-direct {v0, v12, v11, v13}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->HEALTH_AND_BEAUTY:Lio/branch/referral/util/ProductCategory;

    .line 24
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const/16 v12, 0xb

    const-string v13, "HOME_AND_GARDEN"

    const-string v14, "Home & Garden"

    invoke-direct {v0, v13, v12, v14}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->HOME_AND_GARDEN:Lio/branch/referral/util/ProductCategory;

    .line 25
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const/16 v13, 0xc

    const-string v14, "LUGGAGE_AND_BAGS"

    const-string v15, "Luggage & Bags"

    invoke-direct {v0, v14, v13, v15}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->LUGGAGE_AND_BAGS:Lio/branch/referral/util/ProductCategory;

    .line 26
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const/16 v14, 0xd

    const-string v15, "MATURE"

    const-string v13, "Mature"

    invoke-direct {v0, v15, v14, v13}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->MATURE:Lio/branch/referral/util/ProductCategory;

    .line 27
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const/16 v13, 0xe

    const-string v15, "MEDIA"

    const-string v14, "Media"

    invoke-direct {v0, v15, v13, v14}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->MEDIA:Lio/branch/referral/util/ProductCategory;

    .line 28
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const-string v14, "OFFICE_SUPPLIES"

    const/16 v15, 0xf

    const-string v13, "Office Supplies"

    invoke-direct {v0, v14, v15, v13}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->OFFICE_SUPPLIES:Lio/branch/referral/util/ProductCategory;

    .line 29
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const-string v13, "RELIGIOUS_AND_CEREMONIAL"

    const/16 v14, 0x10

    const-string v15, "Religious & Ceremonial"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->RELIGIOUS_AND_CEREMONIAL:Lio/branch/referral/util/ProductCategory;

    .line 30
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const-string v13, "SOFTWARE"

    const/16 v14, 0x11

    const-string v15, "Software"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->SOFTWARE:Lio/branch/referral/util/ProductCategory;

    .line 31
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const-string v13, "SPORTING_GOODS"

    const/16 v14, 0x12

    const-string v15, "Sporting Goods"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->SPORTING_GOODS:Lio/branch/referral/util/ProductCategory;

    .line 32
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const-string v13, "TOYS_AND_GAMES"

    const/16 v14, 0x13

    const-string v15, "Toys & Games"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->TOYS_AND_GAMES:Lio/branch/referral/util/ProductCategory;

    .line 33
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    const-string v13, "VEHICLES_AND_PARTS"

    const/16 v14, 0x14

    const-string v15, "Vehicles & Parts"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/util/ProductCategory;->VEHICLES_AND_PARTS:Lio/branch/referral/util/ProductCategory;

    const/16 v0, 0x15

    .line 12
    new-array v0, v0, [Lio/branch/referral/util/ProductCategory;

    sget-object v13, Lio/branch/referral/util/ProductCategory;->ANIMALS_AND_PET_SUPPLIES:Lio/branch/referral/util/ProductCategory;

    aput-object v13, v0, v1

    sget-object v1, Lio/branch/referral/util/ProductCategory;->APPAREL_AND_ACCESSORIES:Lio/branch/referral/util/ProductCategory;

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/ProductCategory;->ARTS_AND_ENTERTAINMENT:Lio/branch/referral/util/ProductCategory;

    aput-object v1, v0, v3

    sget-object v1, Lio/branch/referral/util/ProductCategory;->BABY_AND_TODDLER:Lio/branch/referral/util/ProductCategory;

    aput-object v1, v0, v4

    sget-object v1, Lio/branch/referral/util/ProductCategory;->BUSINESS_AND_INDUSTRIAL:Lio/branch/referral/util/ProductCategory;

    aput-object v1, v0, v5

    sget-object v1, Lio/branch/referral/util/ProductCategory;->CAMERAS_AND_OPTICS:Lio/branch/referral/util/ProductCategory;

    aput-object v1, v0, v6

    sget-object v1, Lio/branch/referral/util/ProductCategory;->ELECTRONICS:Lio/branch/referral/util/ProductCategory;

    aput-object v1, v0, v7

    sget-object v1, Lio/branch/referral/util/ProductCategory;->FOOD_BEVERAGES_AND_TOBACCO:Lio/branch/referral/util/ProductCategory;

    aput-object v1, v0, v8

    sget-object v1, Lio/branch/referral/util/ProductCategory;->FURNITURE:Lio/branch/referral/util/ProductCategory;

    aput-object v1, v0, v9

    sget-object v1, Lio/branch/referral/util/ProductCategory;->HARDWARE:Lio/branch/referral/util/ProductCategory;

    aput-object v1, v0, v10

    sget-object v1, Lio/branch/referral/util/ProductCategory;->HEALTH_AND_BEAUTY:Lio/branch/referral/util/ProductCategory;

    aput-object v1, v0, v11

    sget-object v1, Lio/branch/referral/util/ProductCategory;->HOME_AND_GARDEN:Lio/branch/referral/util/ProductCategory;

    aput-object v1, v0, v12

    sget-object v1, Lio/branch/referral/util/ProductCategory;->LUGGAGE_AND_BAGS:Lio/branch/referral/util/ProductCategory;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/ProductCategory;->MATURE:Lio/branch/referral/util/ProductCategory;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/ProductCategory;->MEDIA:Lio/branch/referral/util/ProductCategory;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/ProductCategory;->OFFICE_SUPPLIES:Lio/branch/referral/util/ProductCategory;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/ProductCategory;->RELIGIOUS_AND_CEREMONIAL:Lio/branch/referral/util/ProductCategory;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/ProductCategory;->SOFTWARE:Lio/branch/referral/util/ProductCategory;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/ProductCategory;->SPORTING_GOODS:Lio/branch/referral/util/ProductCategory;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/ProductCategory;->TOYS_AND_GAMES:Lio/branch/referral/util/ProductCategory;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/ProductCategory;->VEHICLES_AND_PARTS:Lio/branch/referral/util/ProductCategory;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sput-object v0, Lio/branch/referral/util/ProductCategory;->$VALUES:[Lio/branch/referral/util/ProductCategory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput-object p3, p0, Lio/branch/referral/util/ProductCategory;->name:Ljava/lang/String;

    return-void
.end method

.method public static getValue(Ljava/lang/String;)Lio/branch/referral/util/ProductCategory;
    .locals 5

    .line 47
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 48
    invoke-static {}, Lio/branch/referral/util/ProductCategory;->values()[Lio/branch/referral/util/ProductCategory;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 49
    iget-object v4, v3, Lio/branch/referral/util/ProductCategory;->name:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_1
    return-object v3
.end method

.method public static valueOf(Ljava/lang/String;)Lio/branch/referral/util/ProductCategory;
    .locals 1

    .line 12
    const-class v0, Lio/branch/referral/util/ProductCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lio/branch/referral/util/ProductCategory;

    return-object p0
.end method

.method public static values()[Lio/branch/referral/util/ProductCategory;
    .locals 1

    .line 12
    sget-object v0, Lio/branch/referral/util/ProductCategory;->$VALUES:[Lio/branch/referral/util/ProductCategory;

    invoke-virtual {v0}, [Lio/branch/referral/util/ProductCategory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/branch/referral/util/ProductCategory;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 42
    iget-object v0, p0, Lio/branch/referral/util/ProductCategory;->name:Ljava/lang/String;

    return-object v0
.end method
