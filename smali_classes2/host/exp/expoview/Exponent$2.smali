.class Lhost/exp/expoview/Exponent$2;
.super Ljava/lang/Object;
.source "Exponent.java"

# interfaces
.implements Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/expoview/Exponent;->loadJSBundle(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/expoview/Exponent$BundleListener;ZZ)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/expoview/Exponent;

.field final synthetic val$bundleListener:Lhost/exp/expoview/Exponent$BundleListener;

.field final synthetic val$directory:Ljava/io/File;

.field final synthetic val$fileName:Ljava/lang/String;

.field final synthetic val$id:Ljava/lang/String;

.field final synthetic val$urlString:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhost/exp/expoview/Exponent;Lhost/exp/expoview/Exponent$BundleListener;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 390
    iput-object p1, p0, Lhost/exp/expoview/Exponent$2;->this$0:Lhost/exp/expoview/Exponent;

    iput-object p2, p0, Lhost/exp/expoview/Exponent$2;->val$bundleListener:Lhost/exp/expoview/Exponent$BundleListener;

    iput-object p3, p0, Lhost/exp/expoview/Exponent$2;->val$id:Ljava/lang/String;

    iput-object p4, p0, Lhost/exp/expoview/Exponent$2;->val$directory:Ljava/io/File;

    iput-object p5, p0, Lhost/exp/expoview/Exponent$2;->val$fileName:Ljava/lang/String;

    iput-object p6, p0, Lhost/exp/expoview/Exponent$2;->val$urlString:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCachedResponse(Lhost/exp/exponent/network/ExpoResponse;Z)V
    .locals 1

    .line 482
    invoke-static {}, Lhost/exp/expoview/Exponent;->access$000()Ljava/lang/String;

    move-result-object p2

    const-string v0, "Using cached or embedded response."

    invoke-static {p2, v0}, Lhost/exp/exponent/analytics/EXL;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    invoke-virtual {p0, p1}, Lhost/exp/expoview/Exponent$2;->onResponse(Lhost/exp/exponent/network/ExpoResponse;)V

    return-void
.end method

.method public onFailure(Ljava/io/IOException;)V
    .locals 1

    .line 393
    iget-object v0, p0, Lhost/exp/expoview/Exponent$2;->val$bundleListener:Lhost/exp/expoview/Exponent$BundleListener;

    invoke-interface {v0, p1}, Lhost/exp/expoview/Exponent$BundleListener;->onError(Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lhost/exp/exponent/network/ExpoResponse;)V
    .locals 8

    .line 398
    invoke-interface {p1}, Lhost/exp/exponent/network/ExpoResponse;->isSuccessful()Z

    move-result v0

    if-nez v0, :cond_0

    .line 401
    :try_start_0
    invoke-interface {p1}, Lhost/exp/exponent/network/ExpoResponse;->body()Lhost/exp/exponent/network/ExpoBody;

    move-result-object v0

    invoke-interface {v0}, Lhost/exp/exponent/network/ExpoBody;->string()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 403
    invoke-static {}, Lhost/exp/expoview/Exponent;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v0, "(could not render body)"

    .line 405
    :goto_0
    iget-object v1, p0, Lhost/exp/expoview/Exponent$2;->val$bundleListener:Lhost/exp/expoview/Exponent$BundleListener;

    new-instance v2, Ljava/lang/Exception;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bundle return code: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lhost/exp/exponent/network/ExpoResponse;->code()I

    move-result p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ". With body: "

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lhost/exp/expoview/Exponent$BundleListener;->onError(Ljava/lang/Exception;)V

    return-void

    .line 410
    :cond_0
    iget-object v0, p0, Lhost/exp/expoview/Exponent$2;->val$id:Ljava/lang/String;

    const-string v1, "kernel"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 411
    sget-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_FETCHING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0}, Lhost/exp/exponent/analytics/Analytics;->markEvent(Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    .line 415
    :cond_1
    :try_start_1
    iget-object v0, p0, Lhost/exp/expoview/Exponent$2;->val$id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 416
    sget-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_WRITING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0}, Lhost/exp/exponent/analytics/Analytics;->markEvent(Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    .line 418
    :cond_2
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lhost/exp/expoview/Exponent$2;->val$directory:Ljava/io/File;

    iget-object v3, p0, Lhost/exp/expoview/Exponent$2;->val$fileName:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 421
    invoke-interface {p1}, Lhost/exp/exponent/network/ExpoResponse;->networkResponse()Lhost/exp/exponent/network/ExpoResponse;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-interface {p1}, Lhost/exp/exponent/network/ExpoResponse;->networkResponse()Lhost/exp/exponent/network/ExpoResponse;

    move-result-object v3

    invoke-interface {v3}, Lhost/exp/exponent/network/ExpoResponse;->code()I

    move-result v3

    const/16 v4, 0x130

    if-ne v3, v4, :cond_4

    .line 423
    :cond_3
    invoke-static {}, Lhost/exp/expoview/Exponent;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Got cached OkHttp response for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lhost/exp/expoview/Exponent$2;->val$urlString:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lhost/exp/exponent/analytics/EXL;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v2, 0x1

    .line 426
    invoke-static {}, Lhost/exp/expoview/Exponent;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Have cached source file for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lhost/exp/expoview/Exponent$2;->val$urlString:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lhost/exp/exponent/analytics/EXL;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_4
    if-nez v2, :cond_5

    const/4 v2, 0x0

    .line 437
    :try_start_2
    invoke-static {}, Lhost/exp/expoview/Exponent;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Do not have cached source file for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lhost/exp/expoview/Exponent$2;->val$urlString:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lhost/exp/exponent/analytics/EXL;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    invoke-interface {p1}, Lhost/exp/exponent/network/ExpoResponse;->body()Lhost/exp/exponent/network/ExpoBody;

    move-result-object p1

    invoke-interface {p1}, Lhost/exp/exponent/network/ExpoBody;->byteStream()Ljava/io/InputStream;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 440
    :try_start_3
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 441
    :try_start_4
    new-instance v4, Lorg/apache/commons/io/output/ByteArrayOutputStream;

    invoke-direct {v4}, Lorg/apache/commons/io/output/ByteArrayOutputStream;-><init>()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 444
    :try_start_5
    new-instance v5, Lorg/apache/commons/io/output/TeeOutputStream;

    invoke-direct {v5, v3, v4}, Lorg/apache/commons/io/output/TeeOutputStream;-><init>(Ljava/io/OutputStream;Ljava/io/OutputStream;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 446
    :try_start_6
    invoke-static {p1, v5}, Lcom/facebook/common/internal/ByteStreams;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 447
    invoke-virtual {v5}, Lorg/apache/commons/io/output/TeeOutputStream;->flush()V

    .line 449
    iget-object v2, p0, Lhost/exp/expoview/Exponent$2;->this$0:Lhost/exp/expoview/Exponent;

    invoke-static {v2}, Lhost/exp/expoview/Exponent;->access$100(Lhost/exp/expoview/Exponent;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lorg/apache/commons/io/output/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    .line 452
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/FileDescriptor;->sync()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 454
    :try_start_7
    invoke-static {v5}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 455
    invoke-static {v3}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 456
    invoke-static {v4}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 457
    invoke-static {p1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    goto :goto_3

    :catchall_0
    move-exception v0

    move-object v2, v5

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_2

    :catchall_2
    move-exception v0

    move-object v4, v2

    goto :goto_2

    :catchall_3
    move-exception v0

    move-object v3, v2

    goto :goto_1

    :catchall_4
    move-exception v0

    move-object p1, v2

    move-object v3, p1

    :goto_1
    move-object v4, v3

    .line 454
    :goto_2
    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 455
    invoke-static {v3}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 456
    invoke-static {v4}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 457
    invoke-static {p1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 458
    throw v0

    .line 461
    :cond_5
    :goto_3
    iget-object p1, p0, Lhost/exp/expoview/Exponent$2;->val$id:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_6

    .line 462
    sget-object p1, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_WRITING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {p1}, Lhost/exp/exponent/analytics/Analytics;->markEvent(Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    .line 469
    :cond_6
    iget-object p1, p0, Lhost/exp/expoview/Exponent$2;->this$0:Lhost/exp/expoview/Exponent;

    iget-object p1, p1, Lhost/exp/expoview/Exponent;->mExpoHandler:Lhost/exp/exponent/ExpoHandler;

    new-instance v1, Lhost/exp/expoview/Exponent$2$1;

    invoke-direct {v1, p0, v0}, Lhost/exp/expoview/Exponent$2$1;-><init>(Lhost/exp/expoview/Exponent$2;Ljava/io/File;)V

    invoke-virtual {p1, v1}, Lhost/exp/exponent/ExpoHandler;->post(Ljava/lang/Runnable;)Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_4

    :catch_1
    move-exception p1

    .line 476
    iget-object v0, p0, Lhost/exp/expoview/Exponent$2;->val$bundleListener:Lhost/exp/expoview/Exponent$BundleListener;

    invoke-interface {v0, p1}, Lhost/exp/expoview/Exponent$BundleListener;->onError(Ljava/lang/Exception;)V

    :goto_4
    return-void
.end method
