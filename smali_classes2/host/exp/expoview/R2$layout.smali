.class public final Lhost/exp/expoview/R2$layout;
.super Ljava/lang/Object;
.source "R2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/expoview/R2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f0e0001
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_action_bar_up_container:I = 0x7f0e0002
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_action_menu_item_layout:I = 0x7f0e0003
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_action_menu_layout:I = 0x7f0e0004
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_action_mode_bar:I = 0x7f0e0005
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_action_mode_close_item_material:I = 0x7f0e0006
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_activity_chooser_view:I = 0x7f0e0007
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_activity_chooser_view_list_item:I = 0x7f0e0008
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f0e0009
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_alert_dialog_material:I = 0x7f0e000a
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_alert_dialog_title_material:I = 0x7f0e000b
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_cascading_menu_item_layout:I = 0x7f0e000c
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_dialog_title_material:I = 0x7f0e000d
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_expanded_menu_layout:I = 0x7f0e000e
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_list_menu_item_checkbox:I = 0x7f0e000f
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_list_menu_item_icon:I = 0x7f0e0010
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_list_menu_item_layout:I = 0x7f0e0011
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_list_menu_item_radio:I = 0x7f0e0012
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_popup_menu_header_item_layout:I = 0x7f0e0013
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_popup_menu_item_layout:I = 0x7f0e0014
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_screen_content_include:I = 0x7f0e0015
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_screen_simple:I = 0x7f0e0016
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f0e0017
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_screen_toolbar:I = 0x7f0e0018
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f0e0019
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_search_view:I = 0x7f0e001a
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_select_dialog_material:I = 0x7f0e001b
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final abc_tooltip:I = 0x7f0e001c
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final amu_info_window:I = 0x7f0e001d
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final amu_text_bubble:I = 0x7f0e001e
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final amu_webview:I = 0x7f0e001f
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final browser_actions_context_menu_page:I = 0x7f0e0020
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final browser_actions_context_menu_row:I = 0x7f0e0021
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final crop_image_activity:I = 0x7f0e0022
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final crop_image_view:I = 0x7f0e0023
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final design_bottom_navigation_item:I = 0x7f0e0024
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final design_bottom_sheet_dialog:I = 0x7f0e0025
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final design_layout_snackbar:I = 0x7f0e0026
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final design_layout_snackbar_include:I = 0x7f0e0027
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final design_layout_tab_icon:I = 0x7f0e0028
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final design_layout_tab_text:I = 0x7f0e0029
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final design_menu_item_action_area:I = 0x7f0e002a
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final design_navigation_item:I = 0x7f0e002b
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final design_navigation_item_header:I = 0x7f0e002c
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final design_navigation_item_separator:I = 0x7f0e002d
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final design_navigation_item_subheader:I = 0x7f0e002e
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final design_navigation_menu:I = 0x7f0e002f
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final design_navigation_menu_item:I = 0x7f0e0030
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final design_text_input_password_icon:I = 0x7f0e0031
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final dev_loading_view:I = 0x7f0e0032
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final error_activity_new:I = 0x7f0e0033
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final error_console_fragment:I = 0x7f0e0034
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final error_console_list_item:I = 0x7f0e0035
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final error_fragment:I = 0x7f0e0036
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final exponent_button:I = 0x7f0e0037
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final exponent_check_box:I = 0x7f0e0038
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final exponent_dev_activity:I = 0x7f0e0039
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final fps_view:I = 0x7f0e003a
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final info_activity:I = 0x7f0e003b
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final loading_view:I = 0x7f0e003c
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final mtrl_layout_snackbar:I = 0x7f0e003d
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final mtrl_layout_snackbar_include:I = 0x7f0e003e
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final notification:I = 0x7f0e003f
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final notification_action:I = 0x7f0e0040
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final notification_action_tombstone:I = 0x7f0e0041
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final notification_media_action:I = 0x7f0e0042
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final notification_media_cancel_action:I = 0x7f0e0043
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final notification_shell_app:I = 0x7f0e0044
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final notification_template_big_media:I = 0x7f0e0045
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final notification_template_big_media_custom:I = 0x7f0e0046
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final notification_template_big_media_narrow:I = 0x7f0e0047
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final notification_template_big_media_narrow_custom:I = 0x7f0e0048
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final notification_template_custom_big:I = 0x7f0e0049
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final notification_template_icon_group:I = 0x7f0e004a
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final notification_template_lines_media:I = 0x7f0e004b
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final notification_template_media:I = 0x7f0e004c
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final notification_template_media_custom:I = 0x7f0e004d
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final notification_template_part_chronometer:I = 0x7f0e004e
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final notification_template_part_time:I = 0x7f0e004f
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final oauth_webview_activity:I = 0x7f0e0050
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final redbox_item_frame:I = 0x7f0e0051
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final redbox_item_title:I = 0x7f0e0052
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final redbox_view:I = 0x7f0e0053
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final select_dialog_item_material:I = 0x7f0e0054
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final select_dialog_multichoice_material:I = 0x7f0e0055
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final select_dialog_singlechoice_material:I = 0x7f0e0056
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final support_simple_spinner_dropdown_item:I = 0x7f0e0057
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public static final wallet_test_layout:I = 0x7f0e0058
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
