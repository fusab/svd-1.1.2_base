.class public final Lhost/exp/expoview/R2$string;
.super Ljava/lang/Object;
.source "R2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/expoview/R2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f140001
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_action_bar_up_description:I = 0x7f140002
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_action_menu_overflow_description:I = 0x7f140003
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_action_mode_done:I = 0x7f140004
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_activity_chooser_view_see_all:I = 0x7f140005
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_activitychooserview_choose_application:I = 0x7f140006
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_capital_off:I = 0x7f140007
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_capital_on:I = 0x7f140008
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_font_family_body_1_material:I = 0x7f140009
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_font_family_body_2_material:I = 0x7f14000a
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_font_family_button_material:I = 0x7f14000b
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_font_family_caption_material:I = 0x7f14000c
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_font_family_display_1_material:I = 0x7f14000d
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_font_family_display_2_material:I = 0x7f14000e
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_font_family_display_3_material:I = 0x7f14000f
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_font_family_display_4_material:I = 0x7f140010
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_font_family_headline_material:I = 0x7f140011
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_font_family_menu_material:I = 0x7f140012
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_font_family_subhead_material:I = 0x7f140013
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_font_family_title_material:I = 0x7f140014
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_menu_alt_shortcut_label:I = 0x7f140015
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f140016
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_menu_delete_shortcut_label:I = 0x7f140017
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_menu_enter_shortcut_label:I = 0x7f140018
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_menu_function_shortcut_label:I = 0x7f140019
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_menu_meta_shortcut_label:I = 0x7f14001a
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_menu_shift_shortcut_label:I = 0x7f14001b
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_menu_space_shortcut_label:I = 0x7f14001c
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_menu_sym_shortcut_label:I = 0x7f14001d
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_prepend_shortcut_label:I = 0x7f14001e
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_search_hint:I = 0x7f14001f
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_searchview_description_clear:I = 0x7f140020
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_searchview_description_query:I = 0x7f140021
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_searchview_description_search:I = 0x7f140022
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_searchview_description_submit:I = 0x7f140023
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_searchview_description_voice:I = 0x7f140024
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_shareactionprovider_share_with:I = 0x7f140025
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f140026
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final abc_toolbar_collapse_description:I = 0x7f140027
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final adjustable_description:I = 0x7f140028
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final allow_experience_permissions:I = 0x7f140029
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final android_pay_unavaliable:I = 0x7f14002a
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final app_name:I = 0x7f14002b
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final appbar_scrolling_view_behavior:I = 0x7f14002c
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final bottom_sheet_behavior:I = 0x7f14002d
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final cardNumber:I = 0x7f14002e
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_copy_button:I = 0x7f14002f
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_debugjs:I = 0x7f140030
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_debugjs_nuclide:I = 0x7f140031
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_debugjs_nuclide_failure:I = 0x7f140032
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_debugjs_off:I = 0x7f140033
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_dismiss_button:I = 0x7f140034
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_element_inspector:I = 0x7f140035
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_heap_capture:I = 0x7f140036
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_hot_module_replacement:I = 0x7f140037
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_hot_module_replacement_off:I = 0x7f140038
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_jsload_error:I = 0x7f140039
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_live_reload:I = 0x7f14003a
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_live_reload_off:I = 0x7f14003b
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_loading_from_url:I = 0x7f14003c
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_perf_monitor:I = 0x7f14003d
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_perf_monitor_off:I = 0x7f14003e
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_poke_sampling_profiler:I = 0x7f14003f
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_reload_button:I = 0x7f140040
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_reloadjs:I = 0x7f140041
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_remotedbg_error:I = 0x7f140042
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_remotedbg_message:I = 0x7f140043
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_report_button:I = 0x7f140044
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_settings:I = 0x7f140045
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final catalyst_settings_title:I = 0x7f140046
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final character_counter_content_description:I = 0x7f140047
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final character_counter_pattern:I = 0x7f140048
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_enable_button:I = 0x7f140049
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_enable_text:I = 0x7f14004a
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_enable_title:I = 0x7f14004b
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_install_button:I = 0x7f14004c
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_install_text:I = 0x7f14004d
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_install_text_phone:I = 0x7f14004e
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_install_text_tablet:I = 0x7f14004f
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_install_title:I = 0x7f140050
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_notification_channel_name:I = 0x7f140051
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_notification_ticker:I = 0x7f140052
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_unknown_issue:I = 0x7f140053
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_unsupported_text:I = 0x7f140054
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_unsupported_title:I = 0x7f140055
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_update_button:I = 0x7f140056
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_update_text:I = 0x7f140057
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_update_title:I = 0x7f140058
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_updating_text:I = 0x7f140059
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_updating_title:I = 0x7f14005a
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_google_play_services_wear_update_text:I = 0x7f14005b
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_open_on_phone:I = 0x7f14005c
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_signin_button_text:I = 0x7f14005d
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final common_signin_button_text_long:I = 0x7f14005e
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final crop_image_activity_no_permissions:I = 0x7f14005f
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final crop_image_activity_title:I = 0x7f140060
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final crop_image_menu_crop:I = 0x7f140061
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final crop_image_menu_flip:I = 0x7f140062
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final crop_image_menu_flip_horizontally:I = 0x7f140063
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final crop_image_menu_flip_vertically:I = 0x7f140064
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final crop_image_menu_rotate_left:I = 0x7f140065
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final crop_image_menu_rotate_right:I = 0x7f140066
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final cvc:I = 0x7f140067
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final default_notification_channel_group:I = 0x7f140068
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final deny_experience_permissions:I = 0x7f140069
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final dev_activity_name:I = 0x7f14006a
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final error_default_client:I = 0x7f14006b
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final error_default_shell:I = 0x7f14006c
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final error_header:I = 0x7f14006d
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final error_unable_to_load_experience:I = 0x7f14006e
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final error_uncaught:I = 0x7f14006f
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final experience_needs_permissions:I = 0x7f140070
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final fab_transformation_scrim_behavior:I = 0x7f140071
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final fab_transformation_sheet_behavior:I = 0x7f140072
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final fcm_fallback_notification_channel_label:I = 0x7f140073
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final gcm_fallback_notification_channel_label:I = 0x7f140074
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final header_description:I = 0x7f140075
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final hide_bottom_view_on_scroll_behavior:I = 0x7f140076
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final image_button_description:I = 0x7f140077
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final image_description:I = 0x7f140078
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final info_activity_name:I = 0x7f140079
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final info_app_name_placeholder:I = 0x7f14007a
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final info_clear_data:I = 0x7f14007b
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final info_hide_manifest:I = 0x7f14007c
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final info_id:I = 0x7f14007d
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final info_is_verified:I = 0x7f14007e
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final info_published_time:I = 0x7f14007f
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final info_sdk_version:I = 0x7f140080
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final info_show_manifest:I = 0x7f140081
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final link_description:I = 0x7f140082
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final mtrl_chip_close_icon_content_description:I = 0x7f140083
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final password_toggle_content_description:I = 0x7f140084
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final path_password_eye:I = 0x7f140085
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final path_password_eye_mask_strike_through:I = 0x7f140086
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final path_password_eye_mask_visible:I = 0x7f140087
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final path_password_strike_through:I = 0x7f140088
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final perm_audio_recording:I = 0x7f140089
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final perm_calendar_read:I = 0x7f14008a
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final perm_calendar_write:I = 0x7f14008b
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final perm_camera:I = 0x7f14008c
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final perm_camera_roll_read:I = 0x7f14008d
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final perm_camera_roll_write:I = 0x7f14008e
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final perm_coarse_location:I = 0x7f14008f
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final perm_contacts:I = 0x7f140090
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final perm_fine_location:I = 0x7f140091
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final perm_system_brightness:I = 0x7f140092
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final persistent_notification_channel_desc:I = 0x7f140093
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final persistent_notification_channel_group:I = 0x7f140094
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final persistent_notification_channel_name:I = 0x7f140095
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final pick_image_intent_chooser_title:I = 0x7f140096
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final preference_file_key:I = 0x7f140097
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final save:I = 0x7f140098
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final search_description:I = 0x7f140099
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final search_menu_title:I = 0x7f14009a
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final status_bar_notification_info_overflow:I = 0x7f14009b
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final user_cancel_dialog:I = 0x7f14009c
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final view_error_log:I = 0x7f14009d
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public static final wallet_buy_button_place_holder:I = 0x7f14009e
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
