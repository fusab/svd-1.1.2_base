.class public final Lhost/exp/expoview/R2$dimen;
.super Ljava/lang/Object;
.source "R2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/expoview/R2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abc_action_bar_content_inset_material:I = 0x7f070001
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_action_bar_content_inset_with_nav:I = 0x7f070002
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_action_bar_default_height_material:I = 0x7f070003
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_action_bar_default_padding_end_material:I = 0x7f070004
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_action_bar_default_padding_start_material:I = 0x7f070005
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_action_bar_elevation_material:I = 0x7f070006
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f070007
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_action_bar_overflow_padding_end_material:I = 0x7f070008
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_action_bar_overflow_padding_start_material:I = 0x7f070009
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_action_bar_progress_bar_size:I = 0x7f07000a
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_action_bar_stacked_max_height:I = 0x7f07000b
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f07000c
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f07000d
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f07000e
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_action_button_min_height_material:I = 0x7f07000f
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_action_button_min_width_material:I = 0x7f070010
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f070011
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_alert_dialog_button_bar_height:I = 0x7f070012
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_alert_dialog_button_dimen:I = 0x7f070013
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_button_inset_horizontal_material:I = 0x7f070014
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_button_inset_vertical_material:I = 0x7f070015
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_button_padding_horizontal_material:I = 0x7f070016
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_button_padding_vertical_material:I = 0x7f070017
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_cascading_menus_min_smallest_width:I = 0x7f070018
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_config_prefDialogWidth:I = 0x7f070019
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_control_corner_material:I = 0x7f07001a
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_control_inset_material:I = 0x7f07001b
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_control_padding_material:I = 0x7f07001c
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_dialog_corner_radius_material:I = 0x7f07001d
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_dialog_fixed_height_major:I = 0x7f07001e
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_dialog_fixed_height_minor:I = 0x7f07001f
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_dialog_fixed_width_major:I = 0x7f070020
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_dialog_fixed_width_minor:I = 0x7f070021
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_dialog_list_padding_bottom_no_buttons:I = 0x7f070022
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_dialog_list_padding_top_no_title:I = 0x7f070023
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_dialog_min_width_major:I = 0x7f070024
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_dialog_min_width_minor:I = 0x7f070025
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_dialog_padding_material:I = 0x7f070026
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_dialog_padding_top_material:I = 0x7f070027
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_dialog_title_divider_material:I = 0x7f070028
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_disabled_alpha_material_dark:I = 0x7f070029
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_disabled_alpha_material_light:I = 0x7f07002a
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_dropdownitem_icon_width:I = 0x7f07002b
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f07002c
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f07002d
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_edit_text_inset_bottom_material:I = 0x7f07002e
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_edit_text_inset_horizontal_material:I = 0x7f07002f
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_edit_text_inset_top_material:I = 0x7f070030
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_floating_window_z:I = 0x7f070031
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_list_item_padding_horizontal_material:I = 0x7f070032
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_panel_menu_list_width:I = 0x7f070033
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_progress_bar_height_material:I = 0x7f070034
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_search_view_preferred_height:I = 0x7f070035
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_search_view_preferred_width:I = 0x7f070036
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_seekbar_track_background_height_material:I = 0x7f070037
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_seekbar_track_progress_height_material:I = 0x7f070038
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_select_dialog_padding_start_material:I = 0x7f070039
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_switch_padding:I = 0x7f07003a
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_body_1_material:I = 0x7f07003b
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_body_2_material:I = 0x7f07003c
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_button_material:I = 0x7f07003d
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_caption_material:I = 0x7f07003e
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_display_1_material:I = 0x7f07003f
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_display_2_material:I = 0x7f070040
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_display_3_material:I = 0x7f070041
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_display_4_material:I = 0x7f070042
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_headline_material:I = 0x7f070043
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_large_material:I = 0x7f070044
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_medium_material:I = 0x7f070045
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_menu_header_material:I = 0x7f070046
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_menu_material:I = 0x7f070047
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_small_material:I = 0x7f070048
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_subhead_material:I = 0x7f070049
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f07004a
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_title_material:I = 0x7f07004b
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final abc_text_size_title_material_toolbar:I = 0x7f07004c
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final activity_horizontal_margin:I = 0x7f07004d
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final activity_vertical_margin:I = 0x7f07004e
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final browser_actions_context_menu_max_width:I = 0x7f07004f
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final browser_actions_context_menu_min_padding:I = 0x7f070050
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final cardview_compat_inset_shadow:I = 0x7f070051
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final cardview_default_elevation:I = 0x7f070052
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final cardview_default_radius:I = 0x7f070053
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final compat_button_inset_horizontal_material:I = 0x7f070054
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final compat_button_inset_vertical_material:I = 0x7f070055
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final compat_button_padding_horizontal_material:I = 0x7f070056
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final compat_button_padding_vertical_material:I = 0x7f070057
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final compat_control_corner_material:I = 0x7f070058
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final compat_notification_large_icon_max_height:I = 0x7f070059
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final compat_notification_large_icon_max_width:I = 0x7f07005a
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_appbar_elevation:I = 0x7f07005b
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_bottom_navigation_active_item_max_width:I = 0x7f07005c
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_bottom_navigation_active_item_min_width:I = 0x7f07005d
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_bottom_navigation_active_text_size:I = 0x7f07005e
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_bottom_navigation_elevation:I = 0x7f07005f
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_bottom_navigation_height:I = 0x7f070060
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_bottom_navigation_icon_size:I = 0x7f070061
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_bottom_navigation_item_max_width:I = 0x7f070062
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_bottom_navigation_item_min_width:I = 0x7f070063
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_bottom_navigation_margin:I = 0x7f070064
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_bottom_navigation_shadow_height:I = 0x7f070065
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_bottom_navigation_text_size:I = 0x7f070066
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_bottom_sheet_modal_elevation:I = 0x7f070067
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_bottom_sheet_peek_height_min:I = 0x7f070068
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_fab_border_width:I = 0x7f070069
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_fab_elevation:I = 0x7f07006a
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_fab_image_size:I = 0x7f07006b
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_fab_size_mini:I = 0x7f07006c
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_fab_size_normal:I = 0x7f07006d
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_fab_translation_z_hovered_focused:I = 0x7f07006e
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_fab_translation_z_pressed:I = 0x7f07006f
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_navigation_elevation:I = 0x7f070070
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_navigation_icon_padding:I = 0x7f070071
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_navigation_icon_size:I = 0x7f070072
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_navigation_item_horizontal_padding:I = 0x7f070073
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_navigation_item_icon_padding:I = 0x7f070074
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_navigation_max_width:I = 0x7f070075
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_navigation_padding_bottom:I = 0x7f070076
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_navigation_separator_vertical_padding:I = 0x7f070077
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_snackbar_action_inline_max_width:I = 0x7f070078
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_snackbar_background_corner_radius:I = 0x7f070079
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_snackbar_elevation:I = 0x7f07007a
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_snackbar_extra_spacing_horizontal:I = 0x7f07007b
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_snackbar_max_width:I = 0x7f07007c
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_snackbar_min_width:I = 0x7f07007d
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_snackbar_padding_horizontal:I = 0x7f07007e
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_snackbar_padding_vertical:I = 0x7f07007f
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_snackbar_padding_vertical_2lines:I = 0x7f070080
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_snackbar_text_size:I = 0x7f070081
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_tab_max_width:I = 0x7f070082
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_tab_scrollable_min_width:I = 0x7f070083
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_tab_text_size:I = 0x7f070084
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_tab_text_size_2line:I = 0x7f070085
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final design_textinput_caption_translate_y:I = 0x7f070086
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final disabled_alpha_material_dark:I = 0x7f070087
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final disabled_alpha_material_light:I = 0x7f070088
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final fab_margin:I = 0x7f070089
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final fastscroll_default_thickness:I = 0x7f07008a
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final fastscroll_margin:I = 0x7f07008b
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final fastscroll_minimum_range:I = 0x7f07008c
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final highlight_alpha_material_colored:I = 0x7f07008d
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final highlight_alpha_material_dark:I = 0x7f07008e
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final highlight_alpha_material_light:I = 0x7f07008f
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final hint_alpha_material_dark:I = 0x7f070090
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final hint_alpha_material_light:I = 0x7f070091
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final hint_pressed_alpha_material_dark:I = 0x7f070092
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final hint_pressed_alpha_material_light:I = 0x7f070093
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final item_touch_helper_max_drag_scroll_per_frame:I = 0x7f070094
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final item_touch_helper_swipe_escape_max_velocity:I = 0x7f070095
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final item_touch_helper_swipe_escape_velocity:I = 0x7f070096
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_bottomappbar_fabOffsetEndMode:I = 0x7f070097
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_bottomappbar_fab_cradle_margin:I = 0x7f070098
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_bottomappbar_fab_cradle_rounded_corner_radius:I = 0x7f070099
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_bottomappbar_fab_cradle_vertical_offset:I = 0x7f07009a
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_bottomappbar_height:I = 0x7f07009b
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_corner_radius:I = 0x7f07009c
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_dialog_btn_min_width:I = 0x7f07009d
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_disabled_elevation:I = 0x7f07009e
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_disabled_z:I = 0x7f07009f
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_elevation:I = 0x7f0700a0
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_focused_z:I = 0x7f0700a1
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_hovered_z:I = 0x7f0700a2
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_icon_btn_padding_left:I = 0x7f0700a3
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_icon_padding:I = 0x7f0700a4
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_inset:I = 0x7f0700a5
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_letter_spacing:I = 0x7f0700a6
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_padding_bottom:I = 0x7f0700a7
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_padding_left:I = 0x7f0700a8
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_padding_right:I = 0x7f0700a9
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_padding_top:I = 0x7f0700aa
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_pressed_z:I = 0x7f0700ab
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_stroke_size:I = 0x7f0700ac
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_text_btn_icon_padding:I = 0x7f0700ad
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_text_btn_padding_left:I = 0x7f0700ae
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_text_btn_padding_right:I = 0x7f0700af
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_text_size:I = 0x7f0700b0
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_btn_z:I = 0x7f0700b1
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_card_elevation:I = 0x7f0700b2
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_card_spacing:I = 0x7f0700b3
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_chip_pressed_translation_z:I = 0x7f0700b4
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_chip_text_size:I = 0x7f0700b5
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_fab_elevation:I = 0x7f0700b6
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_fab_translation_z_hovered_focused:I = 0x7f0700b7
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_fab_translation_z_pressed:I = 0x7f0700b8
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_navigation_elevation:I = 0x7f0700b9
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_navigation_item_horizontal_padding:I = 0x7f0700ba
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_navigation_item_icon_padding:I = 0x7f0700bb
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_snackbar_background_corner_radius:I = 0x7f0700bc
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_snackbar_margin:I = 0x7f0700bd
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_textinput_box_bottom_offset:I = 0x7f0700be
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_textinput_box_corner_radius_medium:I = 0x7f0700bf
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_textinput_box_corner_radius_small:I = 0x7f0700c0
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_textinput_box_label_cutout_padding:I = 0x7f0700c1
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_textinput_box_padding_end:I = 0x7f0700c2
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_textinput_box_stroke_width_default:I = 0x7f0700c3
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_textinput_box_stroke_width_focused:I = 0x7f0700c4
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_textinput_outline_box_expanded_padding:I = 0x7f0700c5
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final mtrl_toolbar_default_height:I = 0x7f0700c6
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final notification_action_icon_size:I = 0x7f0700c7
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final notification_action_text_size:I = 0x7f0700c8
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final notification_big_circle_margin:I = 0x7f0700c9
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final notification_content_margin_start:I = 0x7f0700ca
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final notification_large_icon_height:I = 0x7f0700cb
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final notification_large_icon_width:I = 0x7f0700cc
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final notification_main_column_padding_top:I = 0x7f0700cd
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final notification_media_narrow_margin:I = 0x7f0700ce
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final notification_right_icon_size:I = 0x7f0700cf
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final notification_right_side_padding_top:I = 0x7f0700d0
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final notification_small_icon_background_padding:I = 0x7f0700d1
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final notification_small_icon_size_as_large:I = 0x7f0700d2
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final notification_subtext_size:I = 0x7f0700d3
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final notification_top_pad:I = 0x7f0700d4
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final notification_top_pad_large_text:I = 0x7f0700d5
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final subtitle_corner_radius:I = 0x7f0700d6
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final subtitle_outline_width:I = 0x7f0700d7
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final subtitle_shadow_offset:I = 0x7f0700d8
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final subtitle_shadow_radius:I = 0x7f0700d9
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final tooltip_corner_radius:I = 0x7f0700da
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final tooltip_horizontal_padding:I = 0x7f0700db
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final tooltip_margin:I = 0x7f0700dc
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final tooltip_precise_anchor_extra_offset:I = 0x7f0700dd
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final tooltip_precise_anchor_threshold:I = 0x7f0700de
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final tooltip_vertical_padding:I = 0x7f0700df
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final tooltip_y_offset_non_touch:I = 0x7f0700e0
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field

.field public static final tooltip_y_offset_touch:I = 0x7f0700e1
    .annotation build Landroidx/annotation/DimenRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2445
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
