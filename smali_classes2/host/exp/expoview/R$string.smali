.class public final Lhost/exp/expoview/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/expoview/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f100000

.field public static final abc_action_bar_up_description:I = 0x7f100001

.field public static final abc_action_menu_overflow_description:I = 0x7f100002

.field public static final abc_action_mode_done:I = 0x7f100003

.field public static final abc_activity_chooser_view_see_all:I = 0x7f100004

.field public static final abc_activitychooserview_choose_application:I = 0x7f100005

.field public static final abc_capital_off:I = 0x7f100006

.field public static final abc_capital_on:I = 0x7f100007

.field public static final abc_font_family_body_1_material:I = 0x7f100008

.field public static final abc_font_family_body_2_material:I = 0x7f100009

.field public static final abc_font_family_button_material:I = 0x7f10000a

.field public static final abc_font_family_caption_material:I = 0x7f10000b

.field public static final abc_font_family_display_1_material:I = 0x7f10000c

.field public static final abc_font_family_display_2_material:I = 0x7f10000d

.field public static final abc_font_family_display_3_material:I = 0x7f10000e

.field public static final abc_font_family_display_4_material:I = 0x7f10000f

.field public static final abc_font_family_headline_material:I = 0x7f100010

.field public static final abc_font_family_menu_material:I = 0x7f100011

.field public static final abc_font_family_subhead_material:I = 0x7f100012

.field public static final abc_font_family_title_material:I = 0x7f100013

.field public static final abc_menu_alt_shortcut_label:I = 0x7f100014

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f100015

.field public static final abc_menu_delete_shortcut_label:I = 0x7f100016

.field public static final abc_menu_enter_shortcut_label:I = 0x7f100017

.field public static final abc_menu_function_shortcut_label:I = 0x7f100018

.field public static final abc_menu_meta_shortcut_label:I = 0x7f100019

.field public static final abc_menu_shift_shortcut_label:I = 0x7f10001a

.field public static final abc_menu_space_shortcut_label:I = 0x7f10001b

.field public static final abc_menu_sym_shortcut_label:I = 0x7f10001c

.field public static final abc_prepend_shortcut_label:I = 0x7f10001d

.field public static final abc_search_hint:I = 0x7f10001e

.field public static final abc_searchview_description_clear:I = 0x7f10001f

.field public static final abc_searchview_description_query:I = 0x7f100020

.field public static final abc_searchview_description_search:I = 0x7f100021

.field public static final abc_searchview_description_submit:I = 0x7f100022

.field public static final abc_searchview_description_voice:I = 0x7f100023

.field public static final abc_shareactionprovider_share_with:I = 0x7f100024

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f100025

.field public static final abc_toolbar_collapse_description:I = 0x7f100026

.field public static final adjustable_description:I = 0x7f100027

.field public static final allow_experience_permissions:I = 0x7f100028

.field public static final android_pay_unavaliable:I = 0x7f100029

.field public static final app_name:I = 0x7f10002a

.field public static final appbar_scrolling_view_behavior:I = 0x7f10002b

.field public static final bottom_sheet_behavior:I = 0x7f10002c

.field public static final cardNumber:I = 0x7f10002f

.field public static final catalyst_copy_button:I = 0x7f100030

.field public static final catalyst_debugjs:I = 0x7f100031

.field public static final catalyst_debugjs_nuclide:I = 0x7f100032

.field public static final catalyst_debugjs_nuclide_failure:I = 0x7f100033

.field public static final catalyst_debugjs_off:I = 0x7f100034

.field public static final catalyst_dismiss_button:I = 0x7f100035

.field public static final catalyst_element_inspector:I = 0x7f100036

.field public static final catalyst_heap_capture:I = 0x7f100037

.field public static final catalyst_hot_module_replacement:I = 0x7f100038

.field public static final catalyst_hot_module_replacement_off:I = 0x7f100039

.field public static final catalyst_jsload_error:I = 0x7f10003a

.field public static final catalyst_live_reload:I = 0x7f10003b

.field public static final catalyst_live_reload_off:I = 0x7f10003c

.field public static final catalyst_loading_from_url:I = 0x7f10003d

.field public static final catalyst_perf_monitor:I = 0x7f10003e

.field public static final catalyst_perf_monitor_off:I = 0x7f10003f

.field public static final catalyst_poke_sampling_profiler:I = 0x7f100040

.field public static final catalyst_reload_button:I = 0x7f100041

.field public static final catalyst_reloadjs:I = 0x7f100042

.field public static final catalyst_remotedbg_error:I = 0x7f100043

.field public static final catalyst_remotedbg_message:I = 0x7f100044

.field public static final catalyst_report_button:I = 0x7f100045

.field public static final catalyst_settings:I = 0x7f100046

.field public static final catalyst_settings_title:I = 0x7f100047

.field public static final character_counter_content_description:I = 0x7f100048

.field public static final character_counter_pattern:I = 0x7f100049

.field public static final common_google_play_services_enable_button:I = 0x7f100063

.field public static final common_google_play_services_enable_text:I = 0x7f100064

.field public static final common_google_play_services_enable_title:I = 0x7f100065

.field public static final common_google_play_services_install_button:I = 0x7f100066

.field public static final common_google_play_services_install_text:I = 0x7f100067

.field public static final common_google_play_services_install_title:I = 0x7f100068

.field public static final common_google_play_services_notification_channel_name:I = 0x7f100069

.field public static final common_google_play_services_notification_ticker:I = 0x7f10006a

.field public static final common_google_play_services_unknown_issue:I = 0x7f10006b

.field public static final common_google_play_services_unsupported_text:I = 0x7f10006c

.field public static final common_google_play_services_update_button:I = 0x7f10006d

.field public static final common_google_play_services_update_text:I = 0x7f10006e

.field public static final common_google_play_services_update_title:I = 0x7f10006f

.field public static final common_google_play_services_updating_text:I = 0x7f100070

.field public static final common_google_play_services_wear_update_text:I = 0x7f100071

.field public static final common_open_on_phone:I = 0x7f100072

.field public static final common_signin_button_text:I = 0x7f100073

.field public static final common_signin_button_text_long:I = 0x7f100074

.field public static final crop_image_activity_no_permissions:I = 0x7f10008c

.field public static final crop_image_activity_title:I = 0x7f10008d

.field public static final crop_image_menu_crop:I = 0x7f10008e

.field public static final crop_image_menu_flip:I = 0x7f10008f

.field public static final crop_image_menu_flip_horizontally:I = 0x7f100090

.field public static final crop_image_menu_flip_vertically:I = 0x7f100091

.field public static final crop_image_menu_rotate_left:I = 0x7f100092

.field public static final crop_image_menu_rotate_right:I = 0x7f100093

.field public static final cvc:I = 0x7f100094

.field public static final default_notification_channel_group:I = 0x7f100096

.field public static final deny_experience_permissions:I = 0x7f100098

.field public static final dev_activity_name:I = 0x7f100099

.field public static final error_default_client:I = 0x7f10009c

.field public static final error_default_shell:I = 0x7f10009d

.field public static final error_header:I = 0x7f10009e

.field public static final error_unable_to_load_experience:I = 0x7f1000a7

.field public static final error_uncaught:I = 0x7f1000a8

.field public static final experience_needs_permissions:I = 0x7f1000cb

.field public static final fab_transformation_scrim_behavior:I = 0x7f1000cc

.field public static final fab_transformation_sheet_behavior:I = 0x7f1000cd

.field public static final fcm_fallback_notification_channel_label:I = 0x7f1000ce

.field public static final gcm_fallback_notification_channel_label:I = 0x7f1000d1

.field public static final header_description:I = 0x7f1000d9

.field public static final hide_bottom_view_on_scroll_behavior:I = 0x7f1000da

.field public static final image_button_description:I = 0x7f1000df

.field public static final image_description:I = 0x7f1000e0

.field public static final info_activity_name:I = 0x7f1000e1

.field public static final info_app_name_placeholder:I = 0x7f1000e2

.field public static final info_clear_data:I = 0x7f1000e3

.field public static final info_hide_manifest:I = 0x7f1000e4

.field public static final info_id:I = 0x7f1000e5

.field public static final info_is_verified:I = 0x7f1000e6

.field public static final info_published_time:I = 0x7f1000e7

.field public static final info_sdk_version:I = 0x7f1000e8

.field public static final info_show_manifest:I = 0x7f1000e9

.field public static final link_description:I = 0x7f1000ea

.field public static final mtrl_chip_close_icon_content_description:I = 0x7f1000ec

.field public static final password_toggle_content_description:I = 0x7f1000ee

.field public static final path_password_eye:I = 0x7f1000ef

.field public static final path_password_eye_mask_strike_through:I = 0x7f1000f0

.field public static final path_password_eye_mask_visible:I = 0x7f1000f1

.field public static final path_password_strike_through:I = 0x7f1000f2

.field public static final perm_audio_recording:I = 0x7f1000fb

.field public static final perm_calendar_read:I = 0x7f1000fc

.field public static final perm_calendar_write:I = 0x7f1000fd

.field public static final perm_camera:I = 0x7f1000fe

.field public static final perm_camera_roll_read:I = 0x7f1000ff

.field public static final perm_camera_roll_write:I = 0x7f100100

.field public static final perm_coarse_location:I = 0x7f100101

.field public static final perm_contacts:I = 0x7f100102

.field public static final perm_fine_location:I = 0x7f100103

.field public static final perm_system_brightness:I = 0x7f100104

.field public static final persistent_notification_channel_desc:I = 0x7f100105

.field public static final persistent_notification_channel_group:I = 0x7f100106

.field public static final persistent_notification_channel_name:I = 0x7f100107

.field public static final pick_image_intent_chooser_title:I = 0x7f100108

.field public static final preference_file_key:I = 0x7f100109

.field public static final save:I = 0x7f10010d

.field public static final search_description:I = 0x7f10010e

.field public static final search_menu_title:I = 0x7f10010f

.field public static final status_bar_notification_info_overflow:I = 0x7f100118

.field public static final user_cancel_dialog:I = 0x7f10011b

.field public static final view_error_log:I = 0x7f10011c

.field public static final wallet_buy_button_place_holder:I = 0x7f10011d


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1701
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
