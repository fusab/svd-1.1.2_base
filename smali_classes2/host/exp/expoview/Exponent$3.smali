.class Lhost/exp/expoview/Exponent$3;
.super Ljava/lang/Object;
.source "Exponent.java"

# interfaces
.implements Lokhttp3/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/expoview/Exponent;->testPackagerStatus(ZLorg/json/JSONObject;Lhost/exp/expoview/Exponent$PackagerStatusCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/expoview/Exponent;

.field final synthetic val$callback:Lhost/exp/expoview/Exponent$PackagerStatusCallback;

.field final synthetic val$debuggerHost:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhost/exp/expoview/Exponent;Lhost/exp/expoview/Exponent$PackagerStatusCallback;Ljava/lang/String;)V
    .locals 0

    .line 633
    iput-object p1, p0, Lhost/exp/expoview/Exponent$3;->this$0:Lhost/exp/expoview/Exponent;

    iput-object p2, p0, Lhost/exp/expoview/Exponent$3;->val$callback:Lhost/exp/expoview/Exponent$PackagerStatusCallback;

    iput-object p3, p0, Lhost/exp/expoview/Exponent$3;->val$debuggerHost:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lokhttp3/Call;Ljava/io/IOException;)V
    .locals 1

    .line 636
    invoke-static {}, Lhost/exp/expoview/Exponent;->access$000()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lhost/exp/exponent/analytics/EXL;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    iget-object p1, p0, Lhost/exp/expoview/Exponent$3;->val$callback:Lhost/exp/expoview/Exponent$PackagerStatusCallback;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Packager is not running at http://"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lhost/exp/expoview/Exponent$3;->val$debuggerHost:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lhost/exp/expoview/Exponent$PackagerStatusCallback;->onFailure(Ljava/lang/String;)V

    return-void
.end method

.method public onResponse(Lokhttp3/Call;Lokhttp3/Response;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 642
    invoke-virtual {p2}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object p1

    invoke-virtual {p1}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object p1

    const-string p2, "running"

    .line 643
    invoke-virtual {p1, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 644
    iget-object p1, p0, Lhost/exp/expoview/Exponent$3;->this$0:Lhost/exp/expoview/Exponent;

    new-instance p2, Lhost/exp/expoview/Exponent$3$1;

    invoke-direct {p2, p0}, Lhost/exp/expoview/Exponent$3$1;-><init>(Lhost/exp/expoview/Exponent$3;)V

    invoke-virtual {p1, p2}, Lhost/exp/expoview/Exponent;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 651
    :cond_0
    iget-object p1, p0, Lhost/exp/expoview/Exponent$3;->val$callback:Lhost/exp/expoview/Exponent$PackagerStatusCallback;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Packager is not running at http://"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lhost/exp/expoview/Exponent$3;->val$debuggerHost:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lhost/exp/expoview/Exponent$PackagerStatusCallback;->onFailure(Ljava/lang/String;)V

    :goto_0
    return-void
.end method
