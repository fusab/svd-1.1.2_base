.class public final Lhost/exp/expoview/R2$integer;
.super Ljava/lang/Object;
.source "R2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/expoview/R2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final abc_config_activityDefaultDur:I = 0x7f0c0001
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field

.field public static final abc_config_activityShortDur:I = 0x7f0c0002
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field

.field public static final app_bar_elevation_anim_duration:I = 0x7f0c0003
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field

.field public static final bottom_sheet_slide_duration:I = 0x7f0c0004
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field

.field public static final cancel_button_image_alpha:I = 0x7f0c0005
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field

.field public static final card_flip_time_full:I = 0x7f0c0006
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field

.field public static final card_flip_time_half:I = 0x7f0c0007
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field

.field public static final config_tooltipAnimTime:I = 0x7f0c0008
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field

.field public static final design_snackbar_text_max_lines:I = 0x7f0c0009
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field

.field public static final design_tab_indicator_anim_duration_ms:I = 0x7f0c000a
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field

.field public static final google_play_services_version:I = 0x7f0c000b
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field

.field public static final hide_password_duration:I = 0x7f0c000c
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field

.field public static final mtrl_btn_anim_delay_ms:I = 0x7f0c000d
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field

.field public static final mtrl_btn_anim_duration_ms:I = 0x7f0c000e
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field

.field public static final mtrl_chip_anim_duration:I = 0x7f0c000f
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field

.field public static final mtrl_tab_indicator_anim_duration_ms:I = 0x7f0c0010
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field

.field public static final show_password_duration:I = 0x7f0c0011
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field

.field public static final status_bar_notification_info_maxnum:I = 0x7f0c0012
    .annotation build Landroidx/annotation/IntegerRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
