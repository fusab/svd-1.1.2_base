.class public final Lhost/exp/expoview/R2$drawable;
.super Ljava/lang/Object;
.source "R2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/expoview/R2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final abc_ab_share_pack_mtrl_alpha:I = 0x7f080001
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_action_bar_item_background_material:I = 0x7f080002
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_btn_borderless_material:I = 0x7f080003
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_btn_check_material:I = 0x7f080004
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f080005
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f080006
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_btn_colored_material:I = 0x7f080007
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_btn_default_mtrl_shape:I = 0x7f080008
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_btn_radio_material:I = 0x7f080009
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f08000a
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f08000b
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f08000c
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f08000d
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_cab_background_internal_bg:I = 0x7f08000e
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_cab_background_top_material:I = 0x7f08000f
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f080010
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_control_background_material:I = 0x7f080011
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_dialog_material_background:I = 0x7f080012
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_edit_text_material:I = 0x7f080013
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_ab_back_material:I = 0x7f080014
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_arrow_drop_right_black_24dp:I = 0x7f080015
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_clear_material:I = 0x7f080016
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f080017
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_go_search_api_material:I = 0x7f080018
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f080019
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f08001a
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_menu_overflow_material:I = 0x7f08001b
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f08001c
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f08001d
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f08001e
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_search_api_material:I = 0x7f08001f
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_star_black_16dp:I = 0x7f080020
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_star_black_36dp:I = 0x7f080021
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_star_black_48dp:I = 0x7f080022
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_star_half_black_16dp:I = 0x7f080023
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_star_half_black_36dp:I = 0x7f080024
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_star_half_black_48dp:I = 0x7f080025
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ic_voice_search_api_material:I = 0x7f080026
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_item_background_holo_dark:I = 0x7f080027
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_item_background_holo_light:I = 0x7f080028
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_list_divider_material:I = 0x7f080029
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f08002a
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_list_focused_holo:I = 0x7f08002b
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_list_longpressed_holo:I = 0x7f08002c
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_list_pressed_holo_dark:I = 0x7f08002d
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_list_pressed_holo_light:I = 0x7f08002e
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f08002f
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f080030
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f080031
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f080032
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_list_selector_holo_dark:I = 0x7f080033
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_list_selector_holo_light:I = 0x7f080034
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f080035
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_popup_background_mtrl_mult:I = 0x7f080036
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ratingbar_indicator_material:I = 0x7f080037
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ratingbar_material:I = 0x7f080038
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_ratingbar_small_material:I = 0x7f080039
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_scrubber_control_off_mtrl_alpha:I = 0x7f08003a
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_scrubber_control_to_pressed_mtrl_000:I = 0x7f08003b
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_scrubber_control_to_pressed_mtrl_005:I = 0x7f08003c
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_scrubber_primary_mtrl_alpha:I = 0x7f08003d
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_scrubber_track_mtrl_alpha:I = 0x7f08003e
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_seekbar_thumb_material:I = 0x7f08003f
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_seekbar_tick_mark_material:I = 0x7f080040
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_seekbar_track_material:I = 0x7f080041
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f080042
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_spinner_textfield_background_material:I = 0x7f080043
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_switch_thumb_material:I = 0x7f080044
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f080045
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_tab_indicator_material:I = 0x7f080046
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f080047
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_text_cursor_material:I = 0x7f080048
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_text_select_handle_left_mtrl_dark:I = 0x7f080049
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_text_select_handle_left_mtrl_light:I = 0x7f08004a
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_text_select_handle_middle_mtrl_dark:I = 0x7f08004b
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_text_select_handle_middle_mtrl_light:I = 0x7f08004c
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_text_select_handle_right_mtrl_dark:I = 0x7f08004d
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_text_select_handle_right_mtrl_light:I = 0x7f08004e
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f08004f
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f080050
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f080051
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f080052
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_textfield_search_material:I = 0x7f080053
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final abc_vector_test:I = 0x7f080054
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final amu_bubble_mask:I = 0x7f080055
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final amu_bubble_shadow:I = 0x7f080056
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final avd_hide_password:I = 0x7f080057
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final avd_show_password:I = 0x7f080058
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final big_logo_dark:I = 0x7f080059
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final big_logo_dark_filled:I = 0x7f08005a
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final big_logo_filled:I = 0x7f08005b
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final big_logo_new_filled:I = 0x7f08005c
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final btn_default_disabled_focused_holo_dark:I = 0x7f08005d
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final btn_default_disabled_holo_dark:I = 0x7f08005e
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final btn_default_focused_holo_dark:I = 0x7f08005f
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final btn_default_holo_dark:I = 0x7f080060
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final btn_default_normal_holo_dark:I = 0x7f080061
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final btn_default_pressed_holo_dark:I = 0x7f080062
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final card_input_custom_background:I = 0x7f080063
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_full_open_on_phone:I = 0x7f080064
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_icon_dark:I = 0x7f080065
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_icon_dark_disabled:I = 0x7f080066
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_icon_dark_focused:I = 0x7f080067
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_icon_dark_normal:I = 0x7f080068
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_icon_dark_normal_background:I = 0x7f080069
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_icon_dark_pressed:I = 0x7f08006a
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_icon_disabled:I = 0x7f08006b
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_icon_light:I = 0x7f08006c
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_icon_light_disabled:I = 0x7f08006d
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_icon_light_focused:I = 0x7f08006e
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_icon_light_normal:I = 0x7f08006f
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_icon_light_normal_background:I = 0x7f080070
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_icon_light_pressed:I = 0x7f080071
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_dark:I = 0x7f080072
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_dark_disabled:I = 0x7f080073
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f080074
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_dark_normal:I = 0x7f080075
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_dark_normal_background:I = 0x7f080076
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_dark_pressed:I = 0x7f080077
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_disabled:I = 0x7f080078
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_light:I = 0x7f080079
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_light_disabled:I = 0x7f08007a
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f08007b
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_light_normal:I = 0x7f08007c
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_light_normal_background:I = 0x7f08007d
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_light_pressed:I = 0x7f08007e
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final common_ic_googleplayservices:I = 0x7f08007f
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final crop_image_menu_flip:I = 0x7f080080
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final crop_image_menu_rotate_left:I = 0x7f080081
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final crop_image_menu_rotate_right:I = 0x7f080082
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final design_bottom_navigation_item_background:I = 0x7f080083
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final design_fab_background:I = 0x7f080084
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final design_ic_visibility:I = 0x7f080085
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final design_ic_visibility_off:I = 0x7f080086
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final design_password_eye:I = 0x7f080087
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final design_snackbar_background:I = 0x7f080088
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final edit_text_holo_dark:I = 0x7f080089
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final googleg_disabled_color_18:I = 0x7f08008a
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final googleg_standard_color_18:I = 0x7f08008b
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final header_dark:I = 0x7f08008c
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final ic_arrow_back_white_36dp:I = 0x7f08008d
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final ic_home_white_36dp:I = 0x7f08008e
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final ic_logo_white_32dp:I = 0x7f08008f
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final ic_mtrl_chip_checked_black:I = 0x7f080090
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final ic_mtrl_chip_checked_circle:I = 0x7f080091
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final ic_mtrl_chip_close_circle:I = 0x7f080092
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final ic_refresh_white_36dp:I = 0x7f080093
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final ic_share_white_36dp:I = 0x7f080094
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final loading_status_bar_background:I = 0x7f080095
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final mtrl_snackbar_background:I = 0x7f080096
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final mtrl_tabs_default_indicator:I = 0x7f080097
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final navigation_empty_icon:I = 0x7f080098
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final notification_action_background:I = 0x7f080099
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final notification_bg:I = 0x7f08009a
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final notification_bg_low:I = 0x7f08009b
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final notification_bg_low_normal:I = 0x7f08009c
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final notification_bg_low_pressed:I = 0x7f08009d
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final notification_bg_normal:I = 0x7f08009e
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final notification_bg_normal_pressed:I = 0x7f08009f
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final notification_icon:I = 0x7f0800a0
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final notification_icon_background:I = 0x7f0800a1
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final notification_template_icon_bg:I = 0x7f0800a2
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final notification_template_icon_low_bg:I = 0x7f0800a3
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final notification_tile_bg:I = 0x7f0800a4
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final notify_panel_notification_icon_bg:I = 0x7f0800a5
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final pin_white:I = 0x7f0800a6
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final pin_white_fade:I = 0x7f0800a7
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final redbox_top_border_background:I = 0x7f0800a8
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final shell_launch_background_image:I = 0x7f0800a9
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final shell_notification_icon:I = 0x7f0800aa
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final spinner_background_holo_dark:I = 0x7f0800ab
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final spinner_default_holo_dark:I = 0x7f0800ac
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final spinner_disabled_holo_dark:I = 0x7f0800ad
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final spinner_focused_holo_dark:I = 0x7f0800ae
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final spinner_pressed_holo_dark:I = 0x7f0800af
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final splash_background:I = 0x7f0800b0
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final textfield_activated_holo_dark:I = 0x7f0800b1
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final textfield_default_holo_dark:I = 0x7f0800b2
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final textfield_disabled_focused_holo_dark:I = 0x7f0800b3
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final textfield_disabled_holo_dark:I = 0x7f0800b4
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final tooltip_frame_dark:I = 0x7f0800b5
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public static final tooltip_frame_light:I = 0x7f0800b6
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
