.class public Lhost/exp/expoview/Exponent;
.super Ljava/lang/Object;
.source "Exponent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;,
        Lhost/exp/expoview/Exponent$PackagerStatusCallback;,
        Lhost/exp/expoview/Exponent$BundleListener;,
        Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;,
        Lhost/exp/expoview/Exponent$PermissionsListener;
    }
.end annotation


# static fields
.field private static final ABIVERSION_PATTERN:Ljava/util/regex/Pattern;

.field private static final PACKAGER_RUNNING:Ljava/lang/String; = "running"

.field private static final TAG:Ljava/lang/String; = "Exponent"

.field private static sBouncyCastleProvider:Ljava/security/Provider;

.field private static sInstance:Lhost/exp/expoview/Exponent;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mActivityResultListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lhost/exp/exponent/ActivityResultListener;",
            ">;"
        }
    .end annotation
.end field

.field private mApplication:Landroid/app/Application;

.field private mBundleStrings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field mExpoHandler:Lhost/exp/exponent/ExpoHandler;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mExponentManifest:Lhost/exp/exponent/ExponentManifest;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mGCMSenderId:Ljava/lang/String;

.field private mPermissionsHelper:Lhost/exp/exponent/utils/PermissionsHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "\\d+\\.\\d+\\.\\d+|UNVERSIONED"

    .line 90
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lhost/exp/expoview/Exponent;->ABIVERSION_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/app/Application;)V
    .locals 6

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhost/exp/expoview/Exponent;->mBundleStrings:Ljava/util/Map;

    .line 244
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhost/exp/expoview/Exponent;->mActivityResultListeners:Ljava/util/List;

    .line 134
    sput-object p0, Lhost/exp/expoview/Exponent;->sInstance:Lhost/exp/expoview/Exponent;

    .line 136
    iput-object p1, p0, Lhost/exp/expoview/Exponent;->mContext:Landroid/content/Context;

    .line 137
    iput-object p2, p0, Lhost/exp/expoview/Exponent;->mApplication:Landroid/app/Application;

    .line 141
    invoke-static {}, Lhost/exp/expoview/Exponent;->getBouncyCastleProvider()Ljava/security/Provider;

    .line 143
    invoke-static {p2}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->initialize(Landroid/app/Application;)V

    .line 144
    invoke-static {}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->getInstance()Lhost/exp/exponent/di/NativeModuleDepsProvider;

    move-result-object v0

    const-class v1, Lhost/exp/expoview/Exponent;

    invoke-virtual {v0, v1, p0}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->inject(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 149
    :try_start_0
    iget-object v0, p0, Lhost/exp/expoview/Exponent;->mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;

    invoke-virtual {v0}, Lhost/exp/exponent/network/ExponentNetwork;->getClient()Lhost/exp/exponent/network/ExponentHttpClient;

    move-result-object v0

    new-instance v1, Lokhttp3/Request$Builder;

    invoke-direct {v1}, Lokhttp3/Request$Builder;-><init>()V

    const-string v2, "https://exp.host/status"

    invoke-virtual {v1, v2}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v1

    new-instance v2, Lhost/exp/expoview/Exponent$1;

    invoke-direct {v2, p0}, Lhost/exp/expoview/Exponent$1;-><init>(Lhost/exp/expoview/Exponent;)V

    invoke-virtual {v0, v1, v2}, Lhost/exp/exponent/network/ExponentHttpClient;->call(Lokhttp3/Request;Lhost/exp/exponent/network/ExpoHttpCallback;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 162
    sget-object v1, Lhost/exp/expoview/Exponent;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 168
    :goto_0
    :try_start_1
    const-class v0, Landroid/os/UserManager;

    const-string v1, "get"

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Landroid/content/Context;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    .line 170
    invoke-static {v0}, Lhost/exp/exponent/analytics/EXL;->testError(Ljava/lang/Throwable;)V

    .line 174
    :goto_1
    :try_start_2
    invoke-static {p1}, Lcom/facebook/drawee/backends/pipeline/Fresco;->initialize(Landroid/content/Context;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    .line 176
    invoke-static {v0}, Lhost/exp/exponent/analytics/EXL;->testError(Ljava/lang/Throwable;)V

    .line 181
    :goto_2
    invoke-static {p1, p2}, Lhost/exp/exponent/analytics/Analytics;->initializeAmplitude(Landroid/content/Context;Landroid/app/Application;)V

    .line 184
    invoke-static {p1}, Lcom/raizlabs/android/dbflow/config/FlowConfig;->builder(Landroid/content/Context;)Lcom/raizlabs/android/dbflow/config/FlowConfig$Builder;

    move-result-object p2

    const-class v0, Lhost/exp/exponent/notifications/managers/SchedulersDatabase;

    .line 185
    invoke-static {v0}, Lcom/raizlabs/android/dbflow/config/DatabaseConfig;->builder(Ljava/lang/Class;)Lcom/raizlabs/android/dbflow/config/DatabaseConfig$Builder;

    move-result-object v0

    const-string v1, "SchedulersDatabase"

    .line 186
    invoke-virtual {v0, v1}, Lcom/raizlabs/android/dbflow/config/DatabaseConfig$Builder;->databaseName(Ljava/lang/String;)Lcom/raizlabs/android/dbflow/config/DatabaseConfig$Builder;

    move-result-object v0

    .line 187
    invoke-virtual {v0}, Lcom/raizlabs/android/dbflow/config/DatabaseConfig$Builder;->build()Lcom/raizlabs/android/dbflow/config/DatabaseConfig;

    move-result-object v0

    .line 185
    invoke-virtual {p2, v0}, Lcom/raizlabs/android/dbflow/config/FlowConfig$Builder;->addDatabaseConfig(Lcom/raizlabs/android/dbflow/config/DatabaseConfig;)Lcom/raizlabs/android/dbflow/config/FlowConfig$Builder;

    move-result-object p2

    const-class v0, Lhost/exp/exponent/notifications/ActionDatabase;

    .line 188
    invoke-static {v0}, Lcom/raizlabs/android/dbflow/config/DatabaseConfig;->builder(Ljava/lang/Class;)Lcom/raizlabs/android/dbflow/config/DatabaseConfig$Builder;

    move-result-object v0

    const-string v1, "ExpoNotificationActions"

    .line 189
    invoke-virtual {v0, v1}, Lcom/raizlabs/android/dbflow/config/DatabaseConfig$Builder;->databaseName(Ljava/lang/String;)Lcom/raizlabs/android/dbflow/config/DatabaseConfig$Builder;

    move-result-object v0

    .line 190
    invoke-virtual {v0}, Lcom/raizlabs/android/dbflow/config/DatabaseConfig$Builder;->build()Lcom/raizlabs/android/dbflow/config/DatabaseConfig;

    move-result-object v0

    .line 188
    invoke-virtual {p2, v0}, Lcom/raizlabs/android/dbflow/config/FlowConfig$Builder;->addDatabaseConfig(Lcom/raizlabs/android/dbflow/config/DatabaseConfig;)Lcom/raizlabs/android/dbflow/config/FlowConfig$Builder;

    move-result-object p2

    const-class v0, Lhost/exp/exponent/storage/ExponentDB;

    .line 191
    invoke-static {v0}, Lcom/raizlabs/android/dbflow/config/DatabaseConfig;->builder(Ljava/lang/Class;)Lcom/raizlabs/android/dbflow/config/DatabaseConfig$Builder;

    move-result-object v0

    const-string v1, "ExponentKernel"

    .line 192
    invoke-virtual {v0, v1}, Lcom/raizlabs/android/dbflow/config/DatabaseConfig$Builder;->databaseName(Ljava/lang/String;)Lcom/raizlabs/android/dbflow/config/DatabaseConfig$Builder;

    move-result-object v0

    .line 193
    invoke-virtual {v0}, Lcom/raizlabs/android/dbflow/config/DatabaseConfig$Builder;->build()Lcom/raizlabs/android/dbflow/config/DatabaseConfig;

    move-result-object v0

    .line 191
    invoke-virtual {p2, v0}, Lcom/raizlabs/android/dbflow/config/FlowConfig$Builder;->addDatabaseConfig(Lcom/raizlabs/android/dbflow/config/DatabaseConfig;)Lcom/raizlabs/android/dbflow/config/FlowConfig$Builder;

    move-result-object p2

    .line 194
    invoke-virtual {p2}, Lcom/raizlabs/android/dbflow/config/FlowConfig$Builder;->build()Lcom/raizlabs/android/dbflow/config/FlowConfig;

    move-result-object p2

    .line 184
    invoke-static {p2}, Lcom/raizlabs/android/dbflow/config/FlowManager;->init(Lcom/raizlabs/android/dbflow/config/FlowConfig;)V

    .line 197
    sget-boolean p2, Lhost/exp/expoview/ExpoViewBuildConfig;->DEBUG:Z

    if-eqz p2, :cond_0

    .line 198
    invoke-static {p1}, Lcom/facebook/stetho/Stetho;->initializeWithDefaults(Landroid/content/Context;)V

    .line 201
    :cond_0
    sget-boolean p1, Lhost/exp/expoview/ExpoViewBuildConfig;->DEBUG:Z

    if-nez p1, :cond_1

    .line 205
    new-instance p1, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {p1}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    invoke-virtual {p1}, Landroid/os/StrictMode$ThreadPolicy$Builder;->permitAll()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object p1

    .line 206
    invoke-static {p1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    :cond_1
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .line 86
    sget-object v0, Lhost/exp/expoview/Exponent;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lhost/exp/expoview/Exponent;)Ljava/util/Map;
    .locals 0

    .line 86
    iget-object p0, p0, Lhost/exp/expoview/Exponent;->mBundleStrings:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$200(Lhost/exp/expoview/Exponent;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 86
    invoke-direct/range {p0 .. p5}, Lhost/exp/expoview/Exponent;->preloadBundle(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static enableDeveloperSupport(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/RNObject;)V
    .locals 5

    .line 575
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_0

    .line 577
    :try_start_0
    new-instance p0, Lhost/exp/exponent/RNObject;

    const-string v0, "com.facebook.react.modules.systeminfo.AndroidInfoHelpers"

    invoke-direct {p0, v0}, Lhost/exp/exponent/RNObject;-><init>(Ljava/lang/String;)V

    .line 578
    invoke-virtual {p3}, Lhost/exp/exponent/RNObject;->version()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhost/exp/exponent/RNObject;->loadVersion(Ljava/lang/String;)Lhost/exp/exponent/RNObject;

    .line 580
    invoke-static {p1}, Lhost/exp/expoview/Exponent;->getHostname(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 581
    invoke-static {p1}, Lhost/exp/expoview/Exponent;->getPort(Ljava/lang/String;)I

    move-result p1

    .line 583
    invoke-virtual {p0}, Lhost/exp/exponent/RNObject;->rnClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "DEVICE_LOCALHOST"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    const/4 v2, 0x1

    .line 584
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    const/4 v3, 0x0

    .line 585
    invoke-virtual {v1, v3, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 587
    invoke-virtual {p0}, Lhost/exp/exponent/RNObject;->rnClass()Ljava/lang/Class;

    move-result-object v1

    const-string v4, "GENYMOTION_LOCALHOST"

    invoke-virtual {v1, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 588
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 589
    invoke-virtual {v1, v3, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 591
    invoke-virtual {p0}, Lhost/exp/exponent/RNObject;->rnClass()Ljava/lang/Class;

    move-result-object v1

    const-string v4, "EMULATOR_LOCALHOST"

    invoke-virtual {v1, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 592
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 593
    invoke-virtual {v1, v3, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 595
    invoke-virtual {p0}, Lhost/exp/exponent/RNObject;->rnClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "DEBUG_SERVER_HOST_PORT"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 596
    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 597
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 599
    invoke-virtual {p0}, Lhost/exp/exponent/RNObject;->rnClass()Ljava/lang/Class;

    move-result-object p0

    const-string v0, "INSPECTOR_PROXY_PORT"

    invoke-virtual {p0, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object p0

    .line 600
    invoke-virtual {p0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 601
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0, v3, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    const-string p0, "setUseDeveloperSupport"

    .line 603
    new-array p1, v2, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p1, v1

    invoke-virtual {p3, p0, p1}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    const-string p0, "setJSMainModulePath"

    .line 604
    new-array p1, v2, [Ljava/lang/Object;

    aput-object p2, p1, v1

    invoke-virtual {p3, p0, p1}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    goto :goto_0

    :catch_1
    move-exception p0

    .line 606
    :goto_0
    invoke-virtual {p0}, Ljava/lang/ReflectiveOperationException;->printStackTrace()V

    :cond_0
    :goto_1
    return-void
.end method

.method public static declared-synchronized getBouncyCastleProvider()Ljava/security/Provider;
    .locals 3

    const-class v0, Lhost/exp/expoview/Exponent;

    monitor-enter v0

    .line 313
    :try_start_0
    sget-object v1, Lhost/exp/expoview/Exponent;->sBouncyCastleProvider:Ljava/security/Provider;

    if-nez v1, :cond_0

    .line 314
    new-instance v1, Lorg/spongycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v1}, Lorg/spongycastle/jce/provider/BouncyCastleProvider;-><init>()V

    sput-object v1, Lhost/exp/expoview/Exponent;->sBouncyCastleProvider:Ljava/security/Provider;

    .line 315
    sget-object v1, Lhost/exp/expoview/Exponent;->sBouncyCastleProvider:Ljava/security/Provider;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Ljava/security/Security;->insertProviderAt(Ljava/security/Provider;I)I

    .line 318
    :cond_0
    sget-object v1, Lhost/exp/expoview/Exponent;->sBouncyCastleProvider:Ljava/security/Provider;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static getHostname(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "://"

    .line 565
    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 566
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 569
    :cond_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    .line 570
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getInstance()Lhost/exp/expoview/Exponent;
    .locals 1

    .line 130
    sget-object v0, Lhost/exp/expoview/Exponent;->sInstance:Lhost/exp/expoview/Exponent;

    return-object v0
.end method

.method public static getPort(Ljava/lang/String;)I
    .locals 2

    const-string v0, "://"

    .line 551
    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 552
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 555
    :cond_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    .line 556
    invoke-virtual {p0}, Landroid/net/Uri;->getPort()I

    move-result p0

    const/4 v0, -0x1

    if-ne p0, v0, :cond_1

    const/16 p0, 0x50

    :cond_1
    return p0
.end method

.method private static hasDeclaredField(Ljava/lang/Class;Ljava/lang/String;)Z
    .locals 0

    .line 613
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p0, 0x1

    return p0

    :catch_0
    const/4 p0, 0x0

    return p0
.end method

.method public static initialize(Landroid/content/Context;Landroid/app/Application;)V
    .locals 1

    .line 124
    sget-object v0, Lhost/exp/expoview/Exponent;->sInstance:Lhost/exp/expoview/Exponent;

    if-nez v0, :cond_0

    .line 125
    new-instance v0, Lhost/exp/expoview/Exponent;

    invoke-direct {v0, p0, p1}, Lhost/exp/expoview/Exponent;-><init>(Landroid/content/Context;Landroid/app/Application;)V

    :cond_0
    return-void
.end method

.method public static logException(Ljava/lang/Throwable;)V
    .locals 1

    .line 301
    sget-boolean v0, Lhost/exp/expoview/ExpoViewBuildConfig;->DEBUG:Z

    if-nez v0, :cond_0

    .line 303
    :try_start_0
    invoke-static {p0}, Lcom/crashlytics/android/Crashlytics;->logException(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 305
    sget-object v0, Lhost/exp/expoview/Exponent;->TAG:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void
.end method

.method private preloadBundle(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 708
    :try_start_0
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v1

    invoke-virtual {v1, p4}, Lhost/exp/expoview/Exponent;->encodeExperienceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lhost/exp/expoview/Exponent$5;

    invoke-direct {v5, p0, p2, p3}, Lhost/exp/expoview/Exponent$5;-><init>(Lhost/exp/expoview/Exponent;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v6, 0x1

    move-object v1, p1

    move-object v2, p3

    move-object v4, p5

    invoke-virtual/range {v0 .. v6}, Lhost/exp/expoview/Exponent;->loadJSBundle(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/expoview/Exponent$BundleListener;Z)Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 720
    sget-object p2, Lhost/exp/expoview/Exponent;->TAG:Ljava/lang/String;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Couldn\'t encode preloaded bundle id: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private printSourceFile(Ljava/lang/String;)V
    .locals 3

    const-string v0, "BUNDLE"

    const-string v1, "Printing bundle:"

    .line 522
    invoke-static {v0, v1}, Lhost/exp/exponent/analytics/EXL;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 525
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 527
    :try_start_1
    new-instance p1, Ljava/io/InputStreamReader;

    invoke-direct {p1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 528
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, p1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 532
    :cond_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object p1

    .line 533
    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez p1, :cond_0

    .line 540
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_0
    move-exception p1

    move-object v1, v2

    goto :goto_0

    :catchall_1
    move-exception p1

    move-object v2, v1

    goto :goto_2

    :catch_1
    move-exception p1

    .line 536
    :goto_0
    :try_start_3
    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_1

    .line 540
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    move-exception p1

    .line 542
    invoke-virtual {p1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :goto_2
    if-eqz v2, :cond_2

    .line 540
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_3

    :catch_3
    move-exception v1

    .line 542
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    :cond_2
    :goto_3
    throw p1
.end method


# virtual methods
.method public addActivityResultListener(Lhost/exp/exponent/ActivityResultListener;)V
    .locals 1

    .line 287
    iget-object v0, p0, Lhost/exp/expoview/Exponent;->mActivityResultListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clearAllJSBundleCache(Ljava/lang/String;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 506
    iget-object v0, p0, Lhost/exp/expoview/Exponent;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 507
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 508
    sget-object v2, Lhost/exp/expoview/Exponent;->ABIVERSION_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/regex/Matcher;->matches()Z

    move-result p1

    const/4 v2, 0x0

    if-nez p1, :cond_0

    return v2

    .line 511
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    return v2

    .line 514
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 515
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result p1

    return p1

    :cond_2
    return v2
.end method

.method public encodeExperienceId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 323
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "experience-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "UTF-8"

    invoke-static {p1, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getApplication()Landroid/app/Application;
    .locals 1

    .line 297
    iget-object v0, p0, Lhost/exp/expoview/Exponent;->mApplication:Landroid/app/Application;

    return-object v0
.end method

.method public getBundleSource(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 100
    iget-object v0, p0, Lhost/exp/expoview/Exponent;->mBundleStrings:Ljava/util/Map;

    monitor-enter v0

    .line 101
    :try_start_0
    iget-object v1, p0, Lhost/exp/expoview/Exponent;->mBundleStrings:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    iget-object v1, p0, Lhost/exp/expoview/Exponent;->mBundleStrings:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 103
    iget-object v2, p0, Lhost/exp/expoview/Exponent;->mBundleStrings:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    monitor-exit v0

    return-object v1

    :cond_0
    const/4 p1, 0x0

    .line 106
    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    .line 108
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getCurrentActivity()Landroid/app/Activity;
    .locals 1

    .line 215
    iget-object v0, p0, Lhost/exp/expoview/Exponent;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public getGCMSenderId()Ljava/lang/String;
    .locals 1

    .line 234
    iget-object v0, p0, Lhost/exp/expoview/Exponent;->mGCMSenderId:Ljava/lang/String;

    return-object v0
.end method

.method public getPermissions(Ljava/lang/String;Lhost/exp/exponent/kernel/ExperienceId;)Z
    .locals 1

    .line 248
    new-instance v0, Lhost/exp/exponent/utils/PermissionsHelper;

    invoke-direct {v0, p2}, Lhost/exp/exponent/utils/PermissionsHelper;-><init>(Lhost/exp/exponent/kernel/ExperienceId;)V

    invoke-virtual {v0, p1}, Lhost/exp/exponent/utils/PermissionsHelper;->getPermissions(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public loadJSBundle(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/expoview/Exponent$BundleListener;)Z
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 344
    invoke-virtual/range {v0 .. v6}, Lhost/exp/expoview/Exponent;->loadJSBundle(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/expoview/Exponent$BundleListener;Z)Z

    move-result p1

    return p1
.end method

.method public loadJSBundle(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/expoview/Exponent$BundleListener;Z)Z
    .locals 8

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    .line 348
    invoke-virtual/range {v0 .. v7}, Lhost/exp/expoview/Exponent;->loadJSBundle(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/expoview/Exponent$BundleListener;ZZ)Z

    move-result p1

    return p1
.end method

.method public loadJSBundle(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/expoview/Exponent$BundleListener;ZZ)Z
    .locals 18

    move-object/from16 v8, p0

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    const-string v2, "kernel"

    .line 352
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 353
    sget-object v3, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_FETCHING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v3}, Lhost/exp/exponent/analytics/Analytics;->markEvent(Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    :cond_0
    if-nez p1, :cond_1

    .line 357
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    goto :goto_0

    :cond_1
    move-object/from16 v3, p1

    :goto_0
    const-string v4, "developer"

    .line 360
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    const/4 v9, 0x1

    goto :goto_1

    :cond_2
    move/from16 v9, p6

    .line 374
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cached-bundle-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x2d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 375
    new-instance v11, Ljava/io/File;

    iget-object v3, v8, Lhost/exp/expoview/Exponent;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-direct {v11, v3, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 376
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_3

    .line 377
    invoke-virtual {v11}, Ljava/io/File;->mkdir()Z

    .line 381
    :cond_3
    :try_start_0
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 383
    invoke-static/range {p2 .. p2}, Lhost/exp/exponent/kernel/ExponentUrls;->addExponentHeadersToUrl(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v1

    move-object/from16 v7, p2

    goto :goto_2

    :cond_4
    new-instance v1, Lokhttp3/Request$Builder;

    invoke-direct {v1}, Lokhttp3/Request$Builder;-><init>()V

    move-object/from16 v7, p2

    .line 384
    invoke-virtual {v1, v7}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v1

    :goto_2
    if-eqz v9, :cond_5

    .line 386
    sget-object v2, Lokhttp3/CacheControl;->FORCE_NETWORK:Lokhttp3/CacheControl;

    invoke-virtual {v1, v2}, Lokhttp3/Request$Builder;->cacheControl(Lokhttp3/CacheControl;)Lokhttp3/Request$Builder;

    .line 388
    :cond_5
    invoke-virtual {v1}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v14

    .line 390
    new-instance v15, Lhost/exp/expoview/Exponent$2;

    move-object v1, v15

    move-object/from16 v2, p0

    move-object/from16 v3, p5

    move-object/from16 v4, p3

    move-object v5, v11

    move-object v6, v10

    move-object/from16 v7, p2

    invoke-direct/range {v1 .. v7}, Lhost/exp/expoview/Exponent$2;-><init>(Lhost/exp/expoview/Exponent;Lhost/exp/expoview/Exponent$BundleListener;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p7, :cond_6

    .line 488
    iget-object v0, v8, Lhost/exp/expoview/Exponent;->mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;

    invoke-virtual {v0}, Lhost/exp/exponent/network/ExponentNetwork;->getLongTimeoutClient()Lhost/exp/exponent/network/ExponentHttpClient;

    move-result-object v12

    invoke-virtual {v14}, Lokhttp3/Request;->url()Lokhttp3/HttpUrl;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/HttpUrl;->toString()Ljava/lang/String;

    move-result-object v13

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {v12 .. v17}, Lhost/exp/exponent/network/ExponentHttpClient;->tryForcedCachedResponse(Ljava/lang/String;Lokhttp3/Request;Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;Lokhttp3/Response;Ljava/io/IOException;)V

    goto :goto_3

    :cond_6
    if-eqz v9, :cond_7

    .line 490
    iget-object v0, v8, Lhost/exp/expoview/Exponent;->mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;

    invoke-virtual {v0}, Lhost/exp/exponent/network/ExponentNetwork;->getLongTimeoutClient()Lhost/exp/exponent/network/ExponentHttpClient;

    move-result-object v0

    invoke-virtual {v0, v14, v15}, Lhost/exp/exponent/network/ExponentHttpClient;->callSafe(Lokhttp3/Request;Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;)V

    goto :goto_3

    .line 492
    :cond_7
    iget-object v0, v8, Lhost/exp/expoview/Exponent;->mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;

    invoke-virtual {v0}, Lhost/exp/exponent/network/ExponentNetwork;->getLongTimeoutClient()Lhost/exp/exponent/network/ExponentHttpClient;

    move-result-object v0

    invoke-virtual {v0, v14, v15}, Lhost/exp/exponent/network/ExponentHttpClient;->callDefaultCache(Lokhttp3/Request;Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    move-object/from16 v1, p5

    .line 495
    invoke-interface {v1, v0}, Lhost/exp/expoview/Exponent$BundleListener;->onError(Ljava/lang/Exception;)V

    .line 499
    :goto_3
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v11, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 500
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .line 291
    iget-object v0, p0, Lhost/exp/expoview/Exponent;->mActivityResultListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhost/exp/exponent/ActivityResultListener;

    .line 292
    invoke-interface {v1, p1, p2, p3}, Lhost/exp/exponent/ActivityResultListener;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 1

    .line 263
    array-length v0, p2

    if-lez v0, :cond_0

    array-length v0, p3

    if-lez v0, :cond_0

    iget-object v0, p0, Lhost/exp/expoview/Exponent;->mPermissionsHelper:Lhost/exp/exponent/utils/PermissionsHelper;

    if-eqz v0, :cond_0

    .line 264
    invoke-virtual {v0, p1, p2, p3}, Lhost/exp/exponent/utils/PermissionsHelper;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    const/4 p1, 0x0

    .line 265
    iput-object p1, p0, Lhost/exp/expoview/Exponent;->mPermissionsHelper:Lhost/exp/exponent/utils/PermissionsHelper;

    :cond_0
    return-void
.end method

.method public preloadManifestAndBundle(Ljava/lang/String;)V
    .locals 3

    .line 671
    :try_start_0
    iget-object v0, p0, Lhost/exp/expoview/Exponent;->mExponentManifest:Lhost/exp/exponent/ExponentManifest;

    new-instance v1, Lhost/exp/expoview/Exponent$4;

    invoke-direct {v1, p0, p1}, Lhost/exp/expoview/Exponent$4;-><init>(Lhost/exp/expoview/Exponent;Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Lhost/exp/exponent/ExponentManifest;->fetchManifest(Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$ManifestListener;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 702
    sget-object v0, Lhost/exp/expoview/Exponent;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t preload manifest: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public requestExperiencePermissions(Lhost/exp/expoview/Exponent$PermissionsListener;[Ljava/lang/String;Lhost/exp/exponent/kernel/ExperienceId;Ljava/lang/String;)V
    .locals 1

    .line 259
    new-instance v0, Lhost/exp/exponent/utils/PermissionsHelper;

    invoke-direct {v0, p3}, Lhost/exp/exponent/utils/PermissionsHelper;-><init>(Lhost/exp/exponent/kernel/ExperienceId;)V

    invoke-virtual {v0, p1, p2, p4}, Lhost/exp/exponent/utils/PermissionsHelper;->requestExperiencePermissions(Lhost/exp/expoview/Exponent$PermissionsListener;[Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public requestPermissions(Lhost/exp/expoview/Exponent$PermissionsListener;[Ljava/lang/String;Lhost/exp/exponent/kernel/ExperienceId;Ljava/lang/String;)Z
    .locals 1

    .line 253
    new-instance v0, Lhost/exp/exponent/utils/PermissionsHelper;

    invoke-direct {v0, p3}, Lhost/exp/exponent/utils/PermissionsHelper;-><init>(Lhost/exp/exponent/kernel/ExperienceId;)V

    iput-object v0, p0, Lhost/exp/expoview/Exponent;->mPermissionsHelper:Lhost/exp/exponent/utils/PermissionsHelper;

    .line 254
    iget-object p3, p0, Lhost/exp/expoview/Exponent;->mPermissionsHelper:Lhost/exp/exponent/utils/PermissionsHelper;

    invoke-virtual {p3, p1, p2, p4}, Lhost/exp/exponent/utils/PermissionsHelper;->requestPermissions(Lhost/exp/expoview/Exponent$PermissionsListener;[Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public final runOnUiThread(Ljava/lang/Runnable;)V
    .locals 2

    .line 219
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 220
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lhost/exp/expoview/Exponent;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 222
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void
.end method

.method public setCurrentActivity(Landroid/app/Activity;)V
    .locals 0

    .line 211
    iput-object p1, p0, Lhost/exp/expoview/Exponent;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method public setGCMSenderId(Ljava/lang/String;)V
    .locals 0

    .line 230
    iput-object p1, p0, Lhost/exp/expoview/Exponent;->mGCMSenderId:Ljava/lang/String;

    return-void
.end method

.method public shouldRequestDrawOverOtherAppsPermission()Z
    .locals 2

    .line 666
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lhost/exp/expoview/Exponent;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/provider/Settings;->canDrawOverlays(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public testPackagerStatus(ZLorg/json/JSONObject;Lhost/exp/expoview/Exponent$PackagerStatusCallback;)V
    .locals 3

    if-nez p1, :cond_0

    .line 628
    invoke-interface {p3}, Lhost/exp/expoview/Exponent$PackagerStatusCallback;->onSuccess()V

    return-void

    :cond_0
    const-string p1, "debuggerHost"

    .line 632
    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 633
    iget-object p2, p0, Lhost/exp/expoview/Exponent;->mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;

    invoke-virtual {p2}, Lhost/exp/exponent/network/ExponentNetwork;->getNoCacheClient()Lokhttp3/OkHttpClient;

    move-result-object p2

    new-instance v0, Lokhttp3/Request$Builder;

    invoke-direct {v0}, Lokhttp3/Request$Builder;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/status"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v0

    invoke-virtual {p2, v0}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object p2

    new-instance v0, Lhost/exp/expoview/Exponent$3;

    invoke-direct {v0, p0, p3, p1}, Lhost/exp/expoview/Exponent$3;-><init>(Lhost/exp/expoview/Exponent;Lhost/exp/expoview/Exponent$PackagerStatusCallback;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lokhttp3/Call;->enqueue(Lokhttp3/Callback;)V

    return-void
.end method
