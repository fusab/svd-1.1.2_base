.class public final Lhost/exp/expoview/R2$color;
.super Ljava/lang/Object;
.source "R2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/expoview/R2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f060001
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f060002
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f060003
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_btn_colored_text_material:I = 0x7f060004
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_color_highlight_material:I = 0x7f060005
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_hint_foreground_material_dark:I = 0x7f060006
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_hint_foreground_material_light:I = 0x7f060007
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_input_method_navigation_guard:I = 0x7f060008
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f060009
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f06000a
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_primary_text_material_dark:I = 0x7f06000b
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_primary_text_material_light:I = 0x7f06000c
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_search_url_text:I = 0x7f06000d
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_search_url_text_normal:I = 0x7f06000e
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_search_url_text_pressed:I = 0x7f06000f
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_search_url_text_selected:I = 0x7f060010
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_secondary_text_material_dark:I = 0x7f060011
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_secondary_text_material_light:I = 0x7f060012
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_tint_btn_checkable:I = 0x7f060013
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_tint_default:I = 0x7f060014
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_tint_edittext:I = 0x7f060015
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_tint_seek_thumb:I = 0x7f060016
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_tint_spinner:I = 0x7f060017
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final abc_tint_switch_track:I = 0x7f060018
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final accent_material_dark:I = 0x7f060019
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final accent_material_light:I = 0x7f06001a
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final background_floating_material_dark:I = 0x7f06001b
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final background_floating_material_light:I = 0x7f06001c
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final background_material_dark:I = 0x7f06001d
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final background_material_light:I = 0x7f06001e
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final bright_foreground_disabled_material_dark:I = 0x7f06001f
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final bright_foreground_disabled_material_light:I = 0x7f060020
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final bright_foreground_inverse_material_dark:I = 0x7f060021
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final bright_foreground_inverse_material_light:I = 0x7f060022
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final bright_foreground_material_dark:I = 0x7f060023
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final bright_foreground_material_light:I = 0x7f060024
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final browser_actions_bg_grey:I = 0x7f060025
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final browser_actions_divider_color:I = 0x7f060026
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final browser_actions_text_color:I = 0x7f060027
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final browser_actions_title_color:I = 0x7f060028
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final buttonText:I = 0x7f060029
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final button_material_dark:I = 0x7f06002a
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final button_material_light:I = 0x7f06002b
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final cardview_dark_background:I = 0x7f06002c
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final cardview_light_background:I = 0x7f06002d
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final cardview_shadow_end_color:I = 0x7f06002e
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final cardview_shadow_start_color:I = 0x7f06002f
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final catalyst_redbox_background:I = 0x7f060030
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final colorAccent:I = 0x7f060031
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final colorAccentDark:I = 0x7f060032
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final colorAccentLight:I = 0x7f060033
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final colorLight:I = 0x7f060034
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final colorPrimary:I = 0x7f060035
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final colorPrimaryDark:I = 0x7f060036
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final colorText:I = 0x7f060037
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_dark:I = 0x7f060038
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_dark_default:I = 0x7f060039
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_dark_disabled:I = 0x7f06003a
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f06003b
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_dark_pressed:I = 0x7f06003c
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_light:I = 0x7f06003d
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_light_default:I = 0x7f06003e
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_light_disabled:I = 0x7f06003f
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f060040
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_text_light_pressed:I = 0x7f060041
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final common_google_signin_btn_tint:I = 0x7f060042
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final design_bottom_navigation_shadow_color:I = 0x7f060043
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final design_default_color_primary:I = 0x7f060044
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final design_default_color_primary_dark:I = 0x7f060045
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final design_error:I = 0x7f060046
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final design_fab_shadow_end_color:I = 0x7f060047
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final design_fab_shadow_mid_color:I = 0x7f060048
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final design_fab_shadow_start_color:I = 0x7f060049
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final design_fab_stroke_end_inner_color:I = 0x7f06004a
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final design_fab_stroke_end_outer_color:I = 0x7f06004b
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final design_fab_stroke_top_inner_color:I = 0x7f06004c
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final design_fab_stroke_top_outer_color:I = 0x7f06004d
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final design_snackbar_background_color:I = 0x7f06004e
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final design_tint_password_toggle:I = 0x7f06004f
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final dim_foreground_disabled_material_dark:I = 0x7f060050
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final dim_foreground_disabled_material_light:I = 0x7f060051
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final dim_foreground_material_dark:I = 0x7f060052
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final dim_foreground_material_light:I = 0x7f060053
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final editText:I = 0x7f060054
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final errorLogButton:I = 0x7f060055
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final errorMessage:I = 0x7f060056
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final error_color_material:I = 0x7f060057
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final error_color_material_dark:I = 0x7f060058
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final error_color_material_light:I = 0x7f060059
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final foreground_material_dark:I = 0x7f06005a
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final foreground_material_light:I = 0x7f06005b
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final headerText:I = 0x7f06005c
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final highlighted_text_material_dark:I = 0x7f06005d
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final highlighted_text_material_light:I = 0x7f06005e
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final iconBackground:I = 0x7f06005f
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final material_blue_grey_800:I = 0x7f060060
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final material_blue_grey_900:I = 0x7f060061
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final material_blue_grey_950:I = 0x7f060062
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final material_deep_teal_200:I = 0x7f060063
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final material_deep_teal_500:I = 0x7f060064
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final material_grey_100:I = 0x7f060065
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final material_grey_300:I = 0x7f060066
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final material_grey_50:I = 0x7f060067
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final material_grey_600:I = 0x7f060068
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final material_grey_800:I = 0x7f060069
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final material_grey_850:I = 0x7f06006a
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final material_grey_900:I = 0x7f06006b
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_bottom_nav_colored_item_tint:I = 0x7f06006c
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_bottom_nav_item_tint:I = 0x7f06006d
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_btn_bg_color_disabled:I = 0x7f06006e
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_btn_bg_color_selector:I = 0x7f06006f
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_btn_ripple_color:I = 0x7f060070
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_btn_stroke_color_selector:I = 0x7f060071
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_btn_text_btn_ripple_color:I = 0x7f060072
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_btn_text_color_disabled:I = 0x7f060073
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_btn_text_color_selector:I = 0x7f060074
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_btn_transparent_bg_color:I = 0x7f060075
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_chip_background_color:I = 0x7f060076
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_chip_close_icon_tint:I = 0x7f060077
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_chip_ripple_color:I = 0x7f060078
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_chip_text_color:I = 0x7f060079
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_fab_ripple_color:I = 0x7f06007a
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_scrim_color:I = 0x7f06007b
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_tabs_colored_ripple_color:I = 0x7f06007c
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_tabs_icon_color_selector:I = 0x7f06007d
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_tabs_icon_color_selector_colored:I = 0x7f06007e
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_tabs_legacy_text_color_selector:I = 0x7f06007f
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_tabs_ripple_color:I = 0x7f060080
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_text_btn_text_color_selector:I = 0x7f060081
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_textinput_default_box_stroke_color:I = 0x7f060082
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_textinput_disabled_color:I = 0x7f060083
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_textinput_filled_box_default_background_color:I = 0x7f060084
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final mtrl_textinput_hovered_box_stroke_color:I = 0x7f060085
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final notification_action_color_filter:I = 0x7f060086
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final notification_icon_bg_color:I = 0x7f060087
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final notification_material_background_media_default_color:I = 0x7f060088
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final primary_dark_material_dark:I = 0x7f060089
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final primary_dark_material_light:I = 0x7f06008a
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final primary_material_dark:I = 0x7f06008b
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final primary_material_light:I = 0x7f06008c
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final primary_text_default_material_dark:I = 0x7f06008d
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final primary_text_default_material_light:I = 0x7f06008e
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final primary_text_disabled_material_dark:I = 0x7f06008f
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final primary_text_disabled_material_light:I = 0x7f060090
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final ripple_material_dark:I = 0x7f060091
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final ripple_material_light:I = 0x7f060092
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final secondary_text_default_material_dark:I = 0x7f060093
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final secondary_text_default_material_light:I = 0x7f060094
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final secondary_text_disabled_material_dark:I = 0x7f060095
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final secondary_text_disabled_material_light:I = 0x7f060096
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final spinnerItem:I = 0x7f060097
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final splashBackground:I = 0x7f060098
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final switch_thumb_disabled_material_dark:I = 0x7f060099
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final switch_thumb_disabled_material_light:I = 0x7f06009a
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final switch_thumb_material_dark:I = 0x7f06009b
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final switch_thumb_material_light:I = 0x7f06009c
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final switch_thumb_normal_material_dark:I = 0x7f06009d
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final switch_thumb_normal_material_light:I = 0x7f06009e
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final tooltip_background_dark:I = 0x7f06009f
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final tooltip_background_light:I = 0x7f0600a0
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final wallet_bright_foreground_disabled_holo_light:I = 0x7f0600a1
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final wallet_bright_foreground_holo_dark:I = 0x7f0600a2
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final wallet_bright_foreground_holo_light:I = 0x7f0600a3
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final wallet_dim_foreground_disabled_holo_dark:I = 0x7f0600a4
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final wallet_dim_foreground_holo_dark:I = 0x7f0600a5
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final wallet_highlighted_text_holo_dark:I = 0x7f0600a6
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final wallet_highlighted_text_holo_light:I = 0x7f0600a7
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final wallet_hint_foreground_holo_dark:I = 0x7f0600a8
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final wallet_hint_foreground_holo_light:I = 0x7f0600a9
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final wallet_holo_blue_light:I = 0x7f0600aa
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final wallet_link_text_light:I = 0x7f0600ab
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final wallet_primary_text_holo_light:I = 0x7f0600ac
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final wallet_secondary_text_holo_dark:I = 0x7f0600ad
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final white:I = 0x7f0600ae
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field

.field public static final white_background:I = 0x7f0600af
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
