.class public final Lhost/exp/expoview/R2$id;
.super Ljava/lang/Object;
.source "R2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/expoview/R2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final CropOverlayView:I = 0x7f0b0001
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final CropProgressBar:I = 0x7f0b0002
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final ImageView_image:I = 0x7f0b0003
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final accessibility_hint:I = 0x7f0b0004
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final accessibility_role:I = 0x7f0b0005
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action0:I = 0x7f0b0006
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action_bar:I = 0x7f0b0007
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action_bar_activity_content:I = 0x7f0b0008
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action_bar_container:I = 0x7f0b0009
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action_bar_root:I = 0x7f0b000a
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action_bar_spinner:I = 0x7f0b000b
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action_bar_subtitle:I = 0x7f0b000c
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action_bar_title:I = 0x7f0b000d
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action_container:I = 0x7f0b000e
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action_context_bar:I = 0x7f0b000f
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action_divider:I = 0x7f0b0010
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action_image:I = 0x7f0b0011
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action_menu_divider:I = 0x7f0b0012
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action_menu_presenter:I = 0x7f0b0013
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action_mode_bar:I = 0x7f0b0014
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action_mode_bar_stub:I = 0x7f0b0015
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action_mode_close_button:I = 0x7f0b0016
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final action_text:I = 0x7f0b0017
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final actions:I = 0x7f0b0018
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final activity_chooser_view_content:I = 0x7f0b0019
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final add:I = 0x7f0b001a
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final adjust_height:I = 0x7f0b001b
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final adjust_width:I = 0x7f0b001c
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final alertTitle:I = 0x7f0b001d
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final amu_text:I = 0x7f0b001e
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final android_pay:I = 0x7f0b001f
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final android_pay_dark:I = 0x7f0b0020
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final android_pay_light:I = 0x7f0b0021
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final android_pay_light_with_border:I = 0x7f0b0022
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final app_icon_small:I = 0x7f0b0023
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final app_name:I = 0x7f0b0024
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final async:I = 0x7f0b0025
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final auto:I = 0x7f0b0026
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final background_image_view:I = 0x7f0b0027
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final blocking:I = 0x7f0b0028
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final book_now:I = 0x7f0b0029
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final bottom:I = 0x7f0b002a
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final browser_actions_header_text:I = 0x7f0b002b
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final browser_actions_menu_item_icon:I = 0x7f0b002c
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final browser_actions_menu_item_text:I = 0x7f0b002d
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final browser_actions_menu_items:I = 0x7f0b002e
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final browser_actions_menu_view:I = 0x7f0b002f
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final buttonPanel:I = 0x7f0b0030
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final buyButton:I = 0x7f0b0031
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final buy_now:I = 0x7f0b0032
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final buy_with:I = 0x7f0b0033
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final buy_with_google:I = 0x7f0b0034
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final cancel_action:I = 0x7f0b0035
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final catalyst_redbox_title:I = 0x7f0b0036
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final center:I = 0x7f0b0037
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final centerCrop:I = 0x7f0b0038
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final centerInside:I = 0x7f0b0039
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final checkbox:I = 0x7f0b003a
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final chronometer:I = 0x7f0b003b
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final classic:I = 0x7f0b003c
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final clear_data:I = 0x7f0b003d
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final console_editText:I = 0x7f0b003e
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final console_home_button:I = 0x7f0b003f
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final console_reload_button:I = 0x7f0b0040
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final container:I = 0x7f0b0041
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final content:I = 0x7f0b0042
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final contentPanel:I = 0x7f0b0043
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final coordinator:I = 0x7f0b0044
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final cropImageView:I = 0x7f0b0045
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final crop_image_menu_crop:I = 0x7f0b0046
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final crop_image_menu_flip:I = 0x7f0b0047
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final crop_image_menu_flip_horizontally:I = 0x7f0b0048
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final crop_image_menu_flip_vertically:I = 0x7f0b0049
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final crop_image_menu_rotate_left:I = 0x7f0b004a
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final crop_image_menu_rotate_right:I = 0x7f0b004b
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final custom:I = 0x7f0b004c
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final customPanel:I = 0x7f0b004d
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final dark:I = 0x7f0b004e
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final decor_content_parent:I = 0x7f0b004f
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final default_activity_button:I = 0x7f0b0050
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final design_bottom_sheet:I = 0x7f0b0051
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final design_menu_item_action_area:I = 0x7f0b0052
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final design_menu_item_action_area_stub:I = 0x7f0b0053
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final design_menu_item_text:I = 0x7f0b0054
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final design_navigation_view:I = 0x7f0b0055
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final donate_with:I = 0x7f0b0056
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final donate_with_google:I = 0x7f0b0057
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final editText:I = 0x7f0b0058
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final edit_query:I = 0x7f0b0059
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final end:I = 0x7f0b005a
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final end_padder:I = 0x7f0b005b
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final error_console_item_message:I = 0x7f0b005c
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final error_console_item_stack_preview:I = 0x7f0b005d
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final error_console_item_timestamp:I = 0x7f0b005e
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final error_header:I = 0x7f0b005f
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final error_message:I = 0x7f0b0060
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final error_viewPager:I = 0x7f0b0061
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final expand_activities_button:I = 0x7f0b0062
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final expanded_menu:I = 0x7f0b0063
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final experience_id:I = 0x7f0b0064
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final fill:I = 0x7f0b0065
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final filled:I = 0x7f0b0066
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final fitBottomStart:I = 0x7f0b0067
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final fitCenter:I = 0x7f0b0068
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final fitEnd:I = 0x7f0b0069
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final fitStart:I = 0x7f0b006a
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final fitXY:I = 0x7f0b006b
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final fixed:I = 0x7f0b006c
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final focusCrop:I = 0x7f0b006d
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final forever:I = 0x7f0b006e
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final fps_text:I = 0x7f0b006f
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final ghost_view:I = 0x7f0b0070
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final googleMaterial2:I = 0x7f0b0071
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final google_wallet_classic:I = 0x7f0b0072
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final google_wallet_grayscale:I = 0x7f0b0073
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final google_wallet_monochrome:I = 0x7f0b0074
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final grayscale:I = 0x7f0b0075
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final group_divider:I = 0x7f0b0076
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final holo_dark:I = 0x7f0b0077
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final holo_light:I = 0x7f0b0078
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final home:I = 0x7f0b0079
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final home_button:I = 0x7f0b007a
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final home_image_button:I = 0x7f0b007b
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final home_text_button:I = 0x7f0b007c
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final hybrid:I = 0x7f0b007d
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final icon:I = 0x7f0b007e
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final icon_group:I = 0x7f0b007f
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final icon_only:I = 0x7f0b0080
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final image:I = 0x7f0b0081
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final image_view:I = 0x7f0b0082
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final info:I = 0x7f0b0083
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final is_verified:I = 0x7f0b0084
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final italic:I = 0x7f0b0085
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final item_touch_helper_previous_elevation:I = 0x7f0b0086
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final labeled:I = 0x7f0b0087
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final largeLabel:I = 0x7f0b0088
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final left:I = 0x7f0b0089
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final light:I = 0x7f0b008a
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final line1:I = 0x7f0b008b
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final line3:I = 0x7f0b008c
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final linearLayout:I = 0x7f0b008d
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final listMode:I = 0x7f0b008e
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final list_item:I = 0x7f0b008f
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final list_view:I = 0x7f0b0090
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final logo_only:I = 0x7f0b0091
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final lottie_layer_name:I = 0x7f0b0092
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final manifest:I = 0x7f0b0093
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final masked:I = 0x7f0b0094
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final match_parent:I = 0x7f0b0095
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final material:I = 0x7f0b0096
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final media_actions:I = 0x7f0b0097
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final message:I = 0x7f0b0098
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final mini:I = 0x7f0b0099
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final monochrome:I = 0x7f0b009a
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final mtrl_child_content_container:I = 0x7f0b009b
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final mtrl_internal_children_alpha_tag:I = 0x7f0b009c
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final multiply:I = 0x7f0b009d
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final navigation_header_container:I = 0x7f0b009e
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final none:I = 0x7f0b009f
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final normal:I = 0x7f0b00a0
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final notification_background:I = 0x7f0b00a1
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final notification_main_column:I = 0x7f0b00a2
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final notification_main_column_container:I = 0x7f0b00a3
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final off:I = 0x7f0b00a4
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final on:I = 0x7f0b00a5
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final onTouch:I = 0x7f0b00a6
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final outline:I = 0x7f0b00a7
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final oval:I = 0x7f0b00a8
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final parallax:I = 0x7f0b00a9
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final parentPanel:I = 0x7f0b00aa
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final parent_matrix:I = 0x7f0b00ab
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final percentage_text_view:I = 0x7f0b00ac
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final pin:I = 0x7f0b00ad
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final production:I = 0x7f0b00ae
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final progress_bar:I = 0x7f0b00af
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final progress_circular:I = 0x7f0b00b0
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final progress_horizontal:I = 0x7f0b00b1
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final published_time:I = 0x7f0b00b2
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final radio:I = 0x7f0b00b3
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final react_test_id:I = 0x7f0b00b4
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final rectangle:I = 0x7f0b00b5
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final reload_button:I = 0x7f0b00b6
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final restart:I = 0x7f0b00b7
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final reverse:I = 0x7f0b00b8
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final right:I = 0x7f0b00b9
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final right_icon:I = 0x7f0b00ba
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final right_side:I = 0x7f0b00bb
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final rn_frame_file:I = 0x7f0b00bc
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final rn_frame_method:I = 0x7f0b00bd
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final rn_redbox_copy_button:I = 0x7f0b00be
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final rn_redbox_dismiss_button:I = 0x7f0b00bf
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final rn_redbox_line_separator:I = 0x7f0b00c0
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final rn_redbox_loading_indicator:I = 0x7f0b00c1
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final rn_redbox_reload_button:I = 0x7f0b00c2
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final rn_redbox_report_button:I = 0x7f0b00c3
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final rn_redbox_report_label:I = 0x7f0b00c4
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final rn_redbox_stack:I = 0x7f0b00c5
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final sandbox:I = 0x7f0b00c6
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final satellite:I = 0x7f0b00c7
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final save_button:I = 0x7f0b00c8
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final save_image_matrix:I = 0x7f0b00c9
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final save_non_transition_alpha:I = 0x7f0b00ca
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final save_scale_type:I = 0x7f0b00cb
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final screen:I = 0x7f0b00cc
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final scrollIndicatorDown:I = 0x7f0b00cd
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final scrollIndicatorUp:I = 0x7f0b00ce
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final scrollView:I = 0x7f0b00cf
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final scrollable:I = 0x7f0b00d0
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final sdk_version:I = 0x7f0b00d1
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final search_badge:I = 0x7f0b00d2
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final search_bar:I = 0x7f0b00d3
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final search_button:I = 0x7f0b00d4
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final search_close_btn:I = 0x7f0b00d5
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final search_edit_frame:I = 0x7f0b00d6
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final search_go_btn:I = 0x7f0b00d7
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final search_mag_icon:I = 0x7f0b00d8
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final search_plate:I = 0x7f0b00d9
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final search_src_text:I = 0x7f0b00da
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final search_voice_btn:I = 0x7f0b00db
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final select_dialog_listview:I = 0x7f0b00dc
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final selected:I = 0x7f0b00dd
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final selectionDetails:I = 0x7f0b00de
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final share_button:I = 0x7f0b00df
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final shortcut:I = 0x7f0b00e0
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final slide:I = 0x7f0b00e1
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final smallLabel:I = 0x7f0b00e2
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final snackbar_action:I = 0x7f0b00e3
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final snackbar_text:I = 0x7f0b00e4
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final spacer:I = 0x7f0b00e5
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final split_action_bar:I = 0x7f0b00e6
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final src_atop:I = 0x7f0b00e7
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final src_in:I = 0x7f0b00e8
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final src_over:I = 0x7f0b00e9
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final standard:I = 0x7f0b00ea
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final start:I = 0x7f0b00eb
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final status_bar:I = 0x7f0b00ec
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final status_bar_latest_event_content:I = 0x7f0b00ed
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final status_text_view:I = 0x7f0b00ee
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final stretch:I = 0x7f0b00ef
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final strict_sandbox:I = 0x7f0b00f0
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final strong:I = 0x7f0b00f1
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final submenuarrow:I = 0x7f0b00f2
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final submit_area:I = 0x7f0b00f3
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final tabMode:I = 0x7f0b00f4
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final tag_transition_group:I = 0x7f0b00f5
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final tag_unhandled_key_event_manager:I = 0x7f0b00f6
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final tag_unhandled_key_listeners:I = 0x7f0b00f7
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final terrain:I = 0x7f0b00f8
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final test:I = 0x7f0b00f9
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final text:I = 0x7f0b00fa
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final text2:I = 0x7f0b00fb
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final textSpacerNoButtons:I = 0x7f0b00fc
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final textSpacerNoTitle:I = 0x7f0b00fd
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final textView:I = 0x7f0b00fe
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final text_input_password_toggle:I = 0x7f0b00ff
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final textinput_counter:I = 0x7f0b0100
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final textinput_error:I = 0x7f0b0101
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final textinput_helper_text:I = 0x7f0b0102
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final time:I = 0x7f0b0103
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final title:I = 0x7f0b0104
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final titleDividerNoCustom:I = 0x7f0b0105
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final title_template:I = 0x7f0b0106
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final toggle_manifest:I = 0x7f0b0107
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final toolbar:I = 0x7f0b0108
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final top:I = 0x7f0b0109
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final topPanel:I = 0x7f0b010a
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final touch_outside:I = 0x7f0b010b
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final transition_current_scene:I = 0x7f0b010c
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final transition_layout_save:I = 0x7f0b010d
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final transition_position:I = 0x7f0b010e
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final transition_scene_layoutid_cache:I = 0x7f0b010f
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final transition_transform:I = 0x7f0b0110
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final uniform:I = 0x7f0b0111
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final unlabeled:I = 0x7f0b0112
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final up:I = 0x7f0b0113
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final view_error_log:I = 0x7f0b0114
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final view_offset_helper:I = 0x7f0b0115
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final view_tag_instance_handle:I = 0x7f0b0116
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final view_tag_native_id:I = 0x7f0b0117
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final visible:I = 0x7f0b0118
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final weak:I = 0x7f0b0119
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final webview:I = 0x7f0b011a
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final wide:I = 0x7f0b011b
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final window:I = 0x7f0b011c
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field

.field public static final wrap_content:I = 0x7f0b011d
    .annotation build Landroidx/annotation/IdRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
