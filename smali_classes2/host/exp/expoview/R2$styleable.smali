.class public final Lhost/exp/expoview/R2$styleable;
.super Ljava/lang/Object;
.source "R2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/expoview/R2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final ActionBarLayout_android_layout_gravity:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_background:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_backgroundSplit:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_backgroundStacked:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_contentInsetEnd:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_contentInsetEndWithActions:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_contentInsetLeft:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_contentInsetRight:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_contentInsetStart:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_contentInsetStartWithNavigation:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_customNavigationLayout:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_displayOptions:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_divider:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_elevation:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_height:I = 0xd
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_hideOnContentScroll:I = 0xe
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_homeAsUpIndicator:I = 0xf
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_homeLayout:I = 0x10
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_icon:I = 0x11
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_indeterminateProgressStyle:I = 0x12
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_itemPadding:I = 0x13
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_logo:I = 0x14
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_navigationMode:I = 0x15
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_popupTheme:I = 0x16
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_progressBarPadding:I = 0x17
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_progressBarStyle:I = 0x18
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_subtitle:I = 0x19
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_subtitleTextStyle:I = 0x1a
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_title:I = 0x1b
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionBar_titleTextStyle:I = 0x1c
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionMenuItemView_android_minWidth:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionMode_background:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionMode_backgroundSplit:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionMode_closeItemLayout:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionMode_height:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionMode_subtitleTextStyle:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActionMode_titleTextStyle:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActivityChooserView_expandActivityOverflowButtonDrawable:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ActivityChooserView_initialActivityCount:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AlertDialog_android_layout:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AlertDialog_buttonIconDimen:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AlertDialog_buttonPanelSideLayout:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AlertDialog_listItemLayout:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AlertDialog_listLayout:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AlertDialog_multiChoiceItemLayout:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AlertDialog_showTitle:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AlertDialog_singleChoiceItemLayout:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AnimatedStateListDrawableCompat_android_constantSize:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AnimatedStateListDrawableCompat_android_dither:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AnimatedStateListDrawableCompat_android_enterFadeDuration:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AnimatedStateListDrawableCompat_android_exitFadeDuration:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AnimatedStateListDrawableCompat_android_variablePadding:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AnimatedStateListDrawableCompat_android_visible:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AnimatedStateListDrawableItem_android_drawable:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AnimatedStateListDrawableItem_android_id:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AnimatedStateListDrawableTransition_android_drawable:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AnimatedStateListDrawableTransition_android_fromId:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AnimatedStateListDrawableTransition_android_reversible:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AnimatedStateListDrawableTransition_android_toId:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppBarLayoutStates_state_collapsed:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppBarLayoutStates_state_collapsible:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppBarLayoutStates_state_liftable:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppBarLayoutStates_state_lifted:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppBarLayout_Layout_layout_scrollFlags:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppBarLayout_Layout_layout_scrollInterpolator:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppBarLayout_android_background:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppBarLayout_android_keyboardNavigationCluster:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppBarLayout_android_touchscreenBlocksFocus:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppBarLayout_elevation:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppBarLayout_expanded:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppBarLayout_liftOnScroll:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatImageView_android_src:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatImageView_srcCompat:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatImageView_tint:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatImageView_tintMode:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatSeekBar_android_thumb:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatSeekBar_tickMark:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatSeekBar_tickMarkTint:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatSeekBar_tickMarkTintMode:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextHelper_android_drawableBottom:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextHelper_android_drawableEnd:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextHelper_android_drawableLeft:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextHelper_android_drawableRight:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextHelper_android_drawableStart:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextHelper_android_drawableTop:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextHelper_android_textAppearance:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextView_android_textAppearance:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextView_autoSizeMaxTextSize:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextView_autoSizeMinTextSize:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextView_autoSizePresetSizes:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextView_autoSizeStepGranularity:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextView_autoSizeTextType:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextView_firstBaselineToTopHeight:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextView_fontFamily:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextView_lastBaselineToBottomHeight:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextView_lineHeight:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTextView_textAllCaps:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionBarDivider:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionBarItemBackground:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionBarPopupTheme:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionBarSize:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionBarSplitStyle:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionBarStyle:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionBarTabBarStyle:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionBarTabStyle:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionBarTabTextStyle:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionBarTheme:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionBarWidgetTheme:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionButtonStyle:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionDropDownStyle:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionMenuTextAppearance:I = 0xd
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionMenuTextColor:I = 0xe
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionModeBackground:I = 0xf
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionModeCloseButtonStyle:I = 0x10
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionModeCloseDrawable:I = 0x11
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionModeCopyDrawable:I = 0x12
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionModeCutDrawable:I = 0x13
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionModeFindDrawable:I = 0x14
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionModePasteDrawable:I = 0x15
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionModePopupWindowStyle:I = 0x16
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionModeSelectAllDrawable:I = 0x17
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionModeShareDrawable:I = 0x18
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionModeSplitBackground:I = 0x19
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionModeStyle:I = 0x1a
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionModeWebSearchDrawable:I = 0x1b
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionOverflowButtonStyle:I = 0x1c
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_actionOverflowMenuStyle:I = 0x1d
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_activityChooserViewStyle:I = 0x1e
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_alertDialogButtonGroupStyle:I = 0x1f
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_alertDialogCenterButtons:I = 0x20
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_alertDialogStyle:I = 0x21
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_alertDialogTheme:I = 0x22
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_android_windowAnimationStyle:I = 0x23
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_android_windowIsFloating:I = 0x24
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_autoCompleteTextViewStyle:I = 0x25
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_borderlessButtonStyle:I = 0x26
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_buttonBarButtonStyle:I = 0x27
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_buttonBarNegativeButtonStyle:I = 0x28
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_buttonBarNeutralButtonStyle:I = 0x29
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_buttonBarPositiveButtonStyle:I = 0x2a
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_buttonBarStyle:I = 0x2b
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_buttonStyle:I = 0x2c
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_buttonStyleSmall:I = 0x2d
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_checkboxStyle:I = 0x2e
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_checkedTextViewStyle:I = 0x2f
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_colorAccent:I = 0x30
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_colorBackgroundFloating:I = 0x31
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_colorButtonNormal:I = 0x32
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_colorControlActivated:I = 0x33
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_colorControlHighlight:I = 0x34
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_colorControlNormal:I = 0x35
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_colorError:I = 0x36
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_colorPrimary:I = 0x37
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_colorPrimaryDark:I = 0x38
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_colorSwitchThumbNormal:I = 0x39
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_controlBackground:I = 0x3a
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_dialogCornerRadius:I = 0x3b
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_dialogPreferredPadding:I = 0x3c
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_dialogTheme:I = 0x3d
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_dividerHorizontal:I = 0x3e
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_dividerVertical:I = 0x3f
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_dropDownListViewStyle:I = 0x40
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_dropdownListPreferredItemHeight:I = 0x41
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_editTextBackground:I = 0x42
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_editTextColor:I = 0x43
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_editTextStyle:I = 0x44
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_homeAsUpIndicator:I = 0x45
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_imageButtonStyle:I = 0x46
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_listChoiceBackgroundIndicator:I = 0x47
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_listDividerAlertDialog:I = 0x48
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_listMenuViewStyle:I = 0x49
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_listPopupWindowStyle:I = 0x4a
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_listPreferredItemHeight:I = 0x4b
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_listPreferredItemHeightLarge:I = 0x4c
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_listPreferredItemHeightSmall:I = 0x4d
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_listPreferredItemPaddingLeft:I = 0x4e
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_listPreferredItemPaddingRight:I = 0x4f
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_panelBackground:I = 0x50
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_panelMenuListTheme:I = 0x51
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_panelMenuListWidth:I = 0x52
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_popupMenuStyle:I = 0x53
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_popupWindowStyle:I = 0x54
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_radioButtonStyle:I = 0x55
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_ratingBarStyle:I = 0x56
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_ratingBarStyleIndicator:I = 0x57
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_ratingBarStyleSmall:I = 0x58
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_searchViewStyle:I = 0x59
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_seekBarStyle:I = 0x5a
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_selectableItemBackground:I = 0x5b
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_selectableItemBackgroundBorderless:I = 0x5c
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_spinnerDropDownItemStyle:I = 0x5d
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_spinnerStyle:I = 0x5e
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_switchStyle:I = 0x5f
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_textAppearanceLargePopupMenu:I = 0x60
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_textAppearanceListItem:I = 0x61
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_textAppearanceListItemSecondary:I = 0x62
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_textAppearanceListItemSmall:I = 0x63
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_textAppearancePopupMenuHeader:I = 0x64
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_textAppearanceSearchResultSubtitle:I = 0x65
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_textAppearanceSearchResultTitle:I = 0x66
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_textAppearanceSmallPopupMenu:I = 0x67
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_textColorAlertDialogListItem:I = 0x68
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_textColorSearchUrl:I = 0x69
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_toolbarNavigationButtonStyle:I = 0x6a
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_toolbarStyle:I = 0x6b
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_tooltipForegroundColor:I = 0x6c
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_tooltipFrameBackground:I = 0x6d
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_viewInflaterClass:I = 0x6e
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_windowActionBar:I = 0x6f
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_windowActionBarOverlay:I = 0x70
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_windowActionModeOverlay:I = 0x71
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_windowFixedHeightMajor:I = 0x72
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_windowFixedHeightMinor:I = 0x73
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_windowFixedWidthMajor:I = 0x74
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_windowFixedWidthMinor:I = 0x75
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_windowMinWidthMajor:I = 0x76
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_windowMinWidthMinor:I = 0x77
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final AppCompatTheme_windowNoTitle:I = 0x78
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomAppBar_backgroundTint:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomAppBar_fabAlignmentMode:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomAppBar_fabCradleMargin:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomAppBar_fabCradleRoundedCornerRadius:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomAppBar_fabCradleVerticalOffset:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomAppBar_hideOnScroll:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomNavigationView_elevation:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomNavigationView_itemBackground:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomNavigationView_itemHorizontalTranslationEnabled:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomNavigationView_itemIconSize:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomNavigationView_itemIconTint:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomNavigationView_itemTextAppearanceActive:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomNavigationView_itemTextAppearanceInactive:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomNavigationView_itemTextColor:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomNavigationView_labelVisibilityMode:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomNavigationView_menu:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomSheetBehavior_Layout_behavior_fitToContents:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomSheetBehavior_Layout_behavior_hideable:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomSheetBehavior_Layout_behavior_peekHeight:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final BottomSheetBehavior_Layout_behavior_skipCollapsed:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ButtonBarLayout_allowStacking:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CardView_android_minHeight:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CardView_android_minWidth:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CardView_cardBackgroundColor:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CardView_cardCornerRadius:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CardView_cardElevation:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CardView_cardMaxElevation:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CardView_cardPreventCornerOverlap:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CardView_cardUseCompatPadding:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CardView_contentPadding:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CardView_contentPaddingBottom:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CardView_contentPaddingLeft:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CardView_contentPaddingRight:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CardView_contentPaddingTop:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ChipGroup_checkedChip:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ChipGroup_chipSpacing:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ChipGroup_chipSpacingHorizontal:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ChipGroup_chipSpacingVertical:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ChipGroup_singleLine:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ChipGroup_singleSelection:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_android_checkable:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_android_ellipsize:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_android_maxWidth:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_android_text:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_android_textAppearance:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_checkedIcon:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_checkedIconEnabled:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_checkedIconVisible:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_chipBackgroundColor:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_chipCornerRadius:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_chipEndPadding:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_chipIcon:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_chipIconEnabled:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_chipIconSize:I = 0xd
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_chipIconTint:I = 0xe
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_chipIconVisible:I = 0xf
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_chipMinHeight:I = 0x10
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_chipStartPadding:I = 0x11
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_chipStrokeColor:I = 0x12
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_chipStrokeWidth:I = 0x13
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_closeIcon:I = 0x14
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_closeIconEnabled:I = 0x15
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_closeIconEndPadding:I = 0x16
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_closeIconSize:I = 0x17
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_closeIconStartPadding:I = 0x18
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_closeIconTint:I = 0x19
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_closeIconVisible:I = 0x1a
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_hideMotionSpec:I = 0x1b
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_iconEndPadding:I = 0x1c
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_iconStartPadding:I = 0x1d
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_rippleColor:I = 0x1e
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_showMotionSpec:I = 0x1f
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_textEndPadding:I = 0x20
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Chip_textStartPadding:I = 0x21
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_Layout_layout_collapseMode:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_Layout_layout_collapseParallaxMultiplier:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_collapsedTitleGravity:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_collapsedTitleTextAppearance:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_contentScrim:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_expandedTitleGravity:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_expandedTitleMargin:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_expandedTitleMarginBottom:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_expandedTitleMarginEnd:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_expandedTitleMarginStart:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_expandedTitleMarginTop:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_expandedTitleTextAppearance:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_scrimAnimationDuration:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_scrimVisibleHeightTrigger:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_statusBarScrim:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_title:I = 0xd
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_titleEnabled:I = 0xe
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CollapsingToolbarLayout_toolbarId:I = 0xf
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ColorStateListItem_alpha:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ColorStateListItem_android_alpha:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ColorStateListItem_android_color:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CompoundButton_android_button:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CompoundButton_buttonTint:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CompoundButton_buttonTintMode:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CoordinatorLayout_Layout_android_layout_gravity:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CoordinatorLayout_Layout_layout_anchor:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CoordinatorLayout_Layout_layout_anchorGravity:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CoordinatorLayout_Layout_layout_behavior:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CoordinatorLayout_Layout_layout_dodgeInsetEdges:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CoordinatorLayout_Layout_layout_insetEdge:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CoordinatorLayout_Layout_layout_keyline:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CoordinatorLayout_keylines:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CoordinatorLayout_statusBarBackground:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropAspectRatioX:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropAspectRatioY:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropAutoZoomEnabled:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropBackgroundColor:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropBorderCornerColor:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropBorderCornerLength:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropBorderCornerOffset:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropBorderCornerThickness:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropBorderLineColor:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropBorderLineThickness:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropFixAspectRatio:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropFlipHorizontally:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropFlipVertically:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropGuidelines:I = 0xd
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropGuidelinesColor:I = 0xe
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropGuidelinesThickness:I = 0xf
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropInitialCropWindowPaddingRatio:I = 0x10
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropMaxCropResultHeightPX:I = 0x11
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropMaxCropResultWidthPX:I = 0x12
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropMaxZoom:I = 0x13
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropMinCropResultHeightPX:I = 0x14
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropMinCropResultWidthPX:I = 0x15
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropMinCropWindowHeight:I = 0x16
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropMinCropWindowWidth:I = 0x17
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropMultiTouchEnabled:I = 0x18
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropSaveBitmapToInstanceState:I = 0x19
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropScaleType:I = 0x1a
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropShape:I = 0x1b
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropShowCropOverlay:I = 0x1c
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropShowProgressBar:I = 0x1d
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropSnapRadius:I = 0x1e
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CropImageView_cropTouchRadius:I = 0x1f
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CustomWalletTheme_customThemeStyle:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CustomWalletTheme_toolbarTextColorStyle:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final CustomWalletTheme_windowTransitionStyle:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final DesignTheme_bottomSheetDialogTheme:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final DesignTheme_bottomSheetStyle:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final DrawerArrowToggle_arrowHeadLength:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final DrawerArrowToggle_arrowShaftLength:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final DrawerArrowToggle_barLength:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final DrawerArrowToggle_color:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final DrawerArrowToggle_drawableSize:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final DrawerArrowToggle_gapBetweenBars:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final DrawerArrowToggle_spinBars:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final DrawerArrowToggle_thickness:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FloatingActionButton_Behavior_Layout_behavior_autoHide:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FloatingActionButton_backgroundTint:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FloatingActionButton_backgroundTintMode:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FloatingActionButton_borderWidth:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FloatingActionButton_elevation:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FloatingActionButton_fabCustomSize:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FloatingActionButton_fabSize:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FloatingActionButton_hideMotionSpec:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FloatingActionButton_hoveredFocusedTranslationZ:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FloatingActionButton_maxImageSize:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FloatingActionButton_pressedTranslationZ:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FloatingActionButton_rippleColor:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FloatingActionButton_showMotionSpec:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FloatingActionButton_useCompatPadding:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FlowLayout_itemSpacing:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FlowLayout_lineSpacing:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FontFamilyFont_android_font:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FontFamilyFont_android_fontStyle:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FontFamilyFont_android_fontVariationSettings:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FontFamilyFont_android_fontWeight:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FontFamilyFont_android_ttcIndex:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FontFamilyFont_font:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FontFamilyFont_fontStyle:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FontFamilyFont_fontVariationSettings:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FontFamilyFont_fontWeight:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FontFamilyFont_ttcIndex:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FontFamily_fontProviderAuthority:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FontFamily_fontProviderCerts:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FontFamily_fontProviderFetchStrategy:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FontFamily_fontProviderFetchTimeout:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FontFamily_fontProviderPackage:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final FontFamily_fontProviderQuery:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ForegroundLinearLayout_android_foreground:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ForegroundLinearLayout_android_foregroundGravity:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ForegroundLinearLayout_foregroundInsidePadding:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_actualImageScaleType:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_backgroundImage:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_fadeDuration:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_failureImage:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_failureImageScaleType:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_overlayImage:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_placeholderImage:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_placeholderImageScaleType:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_pressedStateOverlayImage:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_progressBarAutoRotateInterval:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_progressBarImage:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_progressBarImageScaleType:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_retryImage:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_retryImageScaleType:I = 0xd
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_roundAsCircle:I = 0xe
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_roundBottomEnd:I = 0xf
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_roundBottomLeft:I = 0x10
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_roundBottomRight:I = 0x11
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_roundBottomStart:I = 0x12
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_roundTopEnd:I = 0x13
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_roundTopLeft:I = 0x14
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_roundTopRight:I = 0x15
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_roundTopStart:I = 0x16
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_roundWithOverlayColor:I = 0x17
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_roundedCornerRadius:I = 0x18
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_roundingBorderColor:I = 0x19
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_roundingBorderPadding:I = 0x1a
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_roundingBorderWidth:I = 0x1b
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GenericDraweeHierarchy_viewAspectRatio:I = 0x1c
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GradientColorItem_android_color:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GradientColorItem_android_offset:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GradientColor_android_centerColor:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GradientColor_android_centerX:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GradientColor_android_centerY:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GradientColor_android_endColor:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GradientColor_android_endX:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GradientColor_android_endY:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GradientColor_android_gradientRadius:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GradientColor_android_startColor:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GradientColor_android_startX:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GradientColor_android_startY:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GradientColor_android_tileMode:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final GradientColor_android_type:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LinearLayoutCompat_Layout_android_layout_gravity:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LinearLayoutCompat_Layout_android_layout_height:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LinearLayoutCompat_Layout_android_layout_weight:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LinearLayoutCompat_Layout_android_layout_width:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LinearLayoutCompat_android_baselineAligned:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LinearLayoutCompat_android_baselineAlignedChildIndex:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LinearLayoutCompat_android_gravity:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LinearLayoutCompat_android_orientation:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LinearLayoutCompat_android_weightSum:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LinearLayoutCompat_divider:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LinearLayoutCompat_dividerPadding:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LinearLayoutCompat_measureWithLargestChild:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LinearLayoutCompat_showDividers:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ListPopupWindow_android_dropDownHorizontalOffset:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ListPopupWindow_android_dropDownVerticalOffset:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LoadingImageView_circleCrop:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LoadingImageView_imageAspectRatio:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LoadingImageView_imageAspectRatioAdjust:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LottieAnimationView_lottie_autoPlay:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LottieAnimationView_lottie_cacheStrategy:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LottieAnimationView_lottie_colorFilter:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LottieAnimationView_lottie_enableMergePathsForKitKatAndAbove:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LottieAnimationView_lottie_fileName:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LottieAnimationView_lottie_imageAssetsFolder:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LottieAnimationView_lottie_loop:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LottieAnimationView_lottie_progress:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LottieAnimationView_lottie_rawRes:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LottieAnimationView_lottie_repeatCount:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LottieAnimationView_lottie_repeatMode:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final LottieAnimationView_lottie_scale:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_ambientEnabled:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_cameraBearing:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_cameraMaxZoomPreference:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_cameraMinZoomPreference:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_cameraTargetLat:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_cameraTargetLng:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_cameraTilt:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_cameraZoom:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_latLngBoundsNorthEastLatitude:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_latLngBoundsNorthEastLongitude:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_latLngBoundsSouthWestLatitude:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_latLngBoundsSouthWestLongitude:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_liteMode:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_mapType:I = 0xd
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_uiCompass:I = 0xe
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_uiMapToolbar:I = 0xf
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_uiRotateGestures:I = 0x10
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_uiScrollGestures:I = 0x11
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_uiScrollGesturesDuringRotateOrZoom:I = 0x12
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_uiTiltGestures:I = 0x13
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_uiZoomControls:I = 0x14
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_uiZoomGestures:I = 0x15
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_useViewLifecycle:I = 0x16
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MapAttrs_zOrderOnTop:I = 0x17
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialButton_android_insetBottom:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialButton_android_insetLeft:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialButton_android_insetRight:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialButton_android_insetTop:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialButton_backgroundTint:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialButton_backgroundTintMode:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialButton_cornerRadius:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialButton_icon:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialButton_iconGravity:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialButton_iconPadding:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialButton_iconSize:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialButton_iconTint:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialButton_iconTintMode:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialButton_rippleColor:I = 0xd
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialButton_strokeColor:I = 0xe
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialButton_strokeWidth:I = 0xf
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialCardView_strokeColor:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialCardView_strokeWidth:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_bottomSheetDialogTheme:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_bottomSheetStyle:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_chipGroupStyle:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_chipStandaloneStyle:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_chipStyle:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_colorAccent:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_colorBackgroundFloating:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_colorPrimary:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_colorPrimaryDark:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_colorSecondary:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_editTextStyle:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_floatingActionButtonStyle:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_materialButtonStyle:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_materialCardViewStyle:I = 0xd
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_navigationViewStyle:I = 0xe
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_scrimBackground:I = 0xf
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_snackbarButtonStyle:I = 0x10
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_tabStyle:I = 0x11
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_textAppearanceBody1:I = 0x12
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_textAppearanceBody2:I = 0x13
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_textAppearanceButton:I = 0x14
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_textAppearanceCaption:I = 0x15
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_textAppearanceHeadline1:I = 0x16
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_textAppearanceHeadline2:I = 0x17
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_textAppearanceHeadline3:I = 0x18
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_textAppearanceHeadline4:I = 0x19
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_textAppearanceHeadline5:I = 0x1a
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_textAppearanceHeadline6:I = 0x1b
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_textAppearanceOverline:I = 0x1c
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_textAppearanceSubtitle1:I = 0x1d
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_textAppearanceSubtitle2:I = 0x1e
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MaterialComponentsTheme_textInputStyle:I = 0x1f
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuGroup_android_checkableBehavior:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuGroup_android_enabled:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuGroup_android_id:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuGroup_android_menuCategory:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuGroup_android_orderInCategory:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuGroup_android_visible:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_actionLayout:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_actionProviderClass:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_actionViewClass:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_alphabeticModifiers:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_android_alphabeticShortcut:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_android_checkable:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_android_checked:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_android_enabled:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_android_icon:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_android_id:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_android_menuCategory:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_android_numericShortcut:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_android_onClick:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_android_orderInCategory:I = 0xd
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_android_title:I = 0xe
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_android_titleCondensed:I = 0xf
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_android_visible:I = 0x10
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_contentDescription:I = 0x11
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_iconTint:I = 0x12
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_iconTintMode:I = 0x13
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_numericModifiers:I = 0x14
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_showAsAction:I = 0x15
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuItem_tooltipText:I = 0x16
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuView_android_headerBackground:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuView_android_horizontalDivider:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuView_android_itemBackground:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuView_android_itemIconDisabledAlpha:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuView_android_itemTextAppearance:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuView_android_verticalDivider:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuView_android_windowAnimationStyle:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuView_preserveIconSpacing:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final MenuView_subMenuArrow:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final NavigationView_android_background:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final NavigationView_android_fitsSystemWindows:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final NavigationView_android_maxWidth:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final NavigationView_elevation:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final NavigationView_headerLayout:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final NavigationView_itemBackground:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final NavigationView_itemHorizontalPadding:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final NavigationView_itemIconPadding:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final NavigationView_itemIconTint:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final NavigationView_itemTextAppearance:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final NavigationView_itemTextColor:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final NavigationView_menu:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final PopupWindowBackgroundState_state_above_anchor:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final PopupWindow_android_popupAnimationStyle:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final PopupWindow_android_popupBackground:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final PopupWindow_overlapAnchor:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final RecycleListView_paddingBottomNoButtons:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final RecycleListView_paddingTopNoTitle:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final RecyclerView_android_descendantFocusability:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final RecyclerView_android_orientation:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final RecyclerView_fastScrollEnabled:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final RecyclerView_fastScrollHorizontalThumbDrawable:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final RecyclerView_fastScrollHorizontalTrackDrawable:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final RecyclerView_fastScrollVerticalThumbDrawable:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final RecyclerView_fastScrollVerticalTrackDrawable:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final RecyclerView_layoutManager:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final RecyclerView_reverseLayout:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final RecyclerView_spanCount:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final RecyclerView_stackFromEnd:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ScrimInsetsFrameLayout_insetForeground:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ScrollingViewBehavior_Layout_behavior_overlapTop:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SearchView_android_focusable:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SearchView_android_imeOptions:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SearchView_android_inputType:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SearchView_android_maxWidth:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SearchView_closeIcon:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SearchView_commitIcon:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SearchView_defaultQueryHint:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SearchView_goIcon:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SearchView_iconifiedByDefault:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SearchView_layout:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SearchView_queryBackground:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SearchView_queryHint:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SearchView_searchHintIcon:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SearchView_searchIcon:I = 0xd
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SearchView_submitBackground:I = 0xe
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SearchView_suggestionRowLayout:I = 0xf
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SearchView_voiceIcon:I = 0x10
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SignInButton_buttonSize:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SignInButton_colorScheme:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SignInButton_scopeUris:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_actualImageResource:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_actualImageScaleType:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_actualImageUri:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_backgroundImage:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_fadeDuration:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_failureImage:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_failureImageScaleType:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_overlayImage:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_placeholderImage:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_placeholderImageScaleType:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_pressedStateOverlayImage:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_progressBarAutoRotateInterval:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_progressBarImage:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_progressBarImageScaleType:I = 0xd
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_retryImage:I = 0xe
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_retryImageScaleType:I = 0xf
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_roundAsCircle:I = 0x10
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_roundBottomEnd:I = 0x11
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_roundBottomLeft:I = 0x12
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_roundBottomRight:I = 0x13
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_roundBottomStart:I = 0x14
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_roundTopEnd:I = 0x15
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_roundTopLeft:I = 0x16
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_roundTopRight:I = 0x17
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_roundTopStart:I = 0x18
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_roundWithOverlayColor:I = 0x19
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_roundedCornerRadius:I = 0x1a
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_roundingBorderColor:I = 0x1b
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_roundingBorderPadding:I = 0x1c
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_roundingBorderWidth:I = 0x1d
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SimpleDraweeView_viewAspectRatio:I = 0x1e
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SnackbarLayout_android_maxWidth:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SnackbarLayout_elevation:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SnackbarLayout_maxActionInlineWidth:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Snackbar_snackbarButtonStyle:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Snackbar_snackbarStyle:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Spinner_android_dropDownWidth:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Spinner_android_entries:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Spinner_android_popupBackground:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Spinner_android_prompt:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Spinner_popupTheme:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final StateListDrawableItem_android_drawable:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final StateListDrawable_android_constantSize:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final StateListDrawable_android_dither:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final StateListDrawable_android_enterFadeDuration:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final StateListDrawable_android_exitFadeDuration:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final StateListDrawable_android_variablePadding:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final StateListDrawable_android_visible:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SwitchCompat_android_textOff:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SwitchCompat_android_textOn:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SwitchCompat_android_thumb:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SwitchCompat_showText:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SwitchCompat_splitTrack:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SwitchCompat_switchMinWidth:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SwitchCompat_switchPadding:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SwitchCompat_switchTextAppearance:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SwitchCompat_thumbTextPadding:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SwitchCompat_thumbTint:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SwitchCompat_thumbTintMode:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SwitchCompat_track:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SwitchCompat_trackTint:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final SwitchCompat_trackTintMode:I = 0xd
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabItem_android_icon:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabItem_android_layout:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabItem_android_text:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabBackground:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabContentStart:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabGravity:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabIconTint:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabIconTintMode:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabIndicator:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabIndicatorAnimationDuration:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabIndicatorColor:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabIndicatorFullWidth:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabIndicatorGravity:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabIndicatorHeight:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabInlineLabel:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabMaxWidth:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabMinWidth:I = 0xd
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabMode:I = 0xe
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabPadding:I = 0xf
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabPaddingBottom:I = 0x10
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabPaddingEnd:I = 0x11
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabPaddingStart:I = 0x12
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabPaddingTop:I = 0x13
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabRippleColor:I = 0x14
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabSelectedTextColor:I = 0x15
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabTextAppearance:I = 0x16
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabTextColor:I = 0x17
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TabLayout_tabUnboundedRipple:I = 0x18
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextAppearance_android_fontFamily:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextAppearance_android_shadowColor:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextAppearance_android_shadowDx:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextAppearance_android_shadowDy:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextAppearance_android_shadowRadius:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextAppearance_android_textColor:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextAppearance_android_textColorHint:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextAppearance_android_textColorLink:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextAppearance_android_textSize:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextAppearance_android_textStyle:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextAppearance_android_typeface:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextAppearance_fontFamily:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextAppearance_textAllCaps:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_android_hint:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_android_textColorHint:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_boxBackgroundColor:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_boxBackgroundMode:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_boxCollapsedPaddingTop:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_boxCornerRadiusBottomEnd:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_boxCornerRadiusBottomStart:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_boxCornerRadiusTopEnd:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_boxCornerRadiusTopStart:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_boxStrokeColor:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_boxStrokeWidth:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_counterEnabled:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_counterMaxLength:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_counterOverflowTextAppearance:I = 0xd
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_counterTextAppearance:I = 0xe
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_errorEnabled:I = 0xf
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_errorTextAppearance:I = 0x10
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_helperText:I = 0x11
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_helperTextEnabled:I = 0x12
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_helperTextTextAppearance:I = 0x13
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_hintAnimationEnabled:I = 0x14
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_hintEnabled:I = 0x15
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_hintTextAppearance:I = 0x16
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_passwordToggleContentDescription:I = 0x17
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_passwordToggleDrawable:I = 0x18
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_passwordToggleEnabled:I = 0x19
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_passwordToggleTint:I = 0x1a
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final TextInputLayout_passwordToggleTintMode:I = 0x1b
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ThemeEnforcement_android_textAppearance:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ThemeEnforcement_enforceMaterialTheme:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ThemeEnforcement_enforceTextAppearance:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_android_gravity:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_android_minHeight:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_buttonGravity:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_collapseContentDescription:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_collapseIcon:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_contentInsetEnd:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_contentInsetEndWithActions:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_contentInsetLeft:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_contentInsetRight:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_contentInsetStart:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_contentInsetStartWithNavigation:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_logo:I = 0xb
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_logoDescription:I = 0xc
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_maxButtonHeight:I = 0xd
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_navigationContentDescription:I = 0xe
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_navigationIcon:I = 0xf
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_popupTheme:I = 0x10
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_subtitle:I = 0x11
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_subtitleTextAppearance:I = 0x12
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_subtitleTextColor:I = 0x13
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_title:I = 0x14
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_titleMargin:I = 0x15
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_titleMarginBottom:I = 0x16
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_titleMarginEnd:I = 0x17
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_titleMarginStart:I = 0x18
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_titleMarginTop:I = 0x19
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_titleMargins:I = 0x1a
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_titleTextAppearance:I = 0x1b
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final Toolbar_titleTextColor:I = 0x1c
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ViewBackgroundHelper_android_background:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ViewBackgroundHelper_backgroundTint:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ViewBackgroundHelper_backgroundTintMode:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ViewStubCompat_android_id:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ViewStubCompat_android_inflatedId:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final ViewStubCompat_android_layout:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final View_android_focusable:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final View_android_theme:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final View_paddingEnd:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final View_paddingStart:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final View_theme:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final WalletFragmentOptions_appTheme:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final WalletFragmentOptions_environment:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final WalletFragmentOptions_fragmentMode:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final WalletFragmentOptions_fragmentStyle:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final WalletFragmentStyle_buyButtonAppearance:I = 0x0
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final WalletFragmentStyle_buyButtonHeight:I = 0x1
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final WalletFragmentStyle_buyButtonText:I = 0x2
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final WalletFragmentStyle_buyButtonWidth:I = 0x3
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final WalletFragmentStyle_maskedWalletDetailsBackground:I = 0x4
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final WalletFragmentStyle_maskedWalletDetailsButtonBackground:I = 0x5
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final WalletFragmentStyle_maskedWalletDetailsButtonTextAppearance:I = 0x6
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final WalletFragmentStyle_maskedWalletDetailsHeaderTextAppearance:I = 0x7
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final WalletFragmentStyle_maskedWalletDetailsLogoImageType:I = 0x8
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final WalletFragmentStyle_maskedWalletDetailsLogoTextColor:I = 0x9
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field

.field public static final WalletFragmentStyle_maskedWalletDetailsTextAppearance:I = 0xa
    .annotation build Landroidx/annotation/StyleableRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
