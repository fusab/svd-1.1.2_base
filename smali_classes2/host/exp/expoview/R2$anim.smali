.class public final Lhost/exp/expoview/R2$anim;
.super Ljava/lang/Object;
.source "R2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/expoview/R2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "anim"
.end annotation


# static fields
.field public static final abc_fade_in:I = 0x7f010001
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final abc_fade_out:I = 0x7f010002
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final abc_grow_fade_in_from_bottom:I = 0x7f010003
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final abc_popup_enter:I = 0x7f010004
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final abc_popup_exit:I = 0x7f010005
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final abc_shrink_fade_out_from_bottom:I = 0x7f010006
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final abc_slide_in_bottom:I = 0x7f010007
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final abc_slide_in_top:I = 0x7f010008
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final abc_slide_out_bottom:I = 0x7f010009
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final abc_slide_out_top:I = 0x7f01000a
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final abc_tooltip_enter:I = 0x7f01000b
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final abc_tooltip_exit:I = 0x7f01000c
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final catalyst_fade_in:I = 0x7f01000d
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final catalyst_fade_out:I = 0x7f01000e
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final catalyst_push_up_in:I = 0x7f01000f
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final catalyst_push_up_out:I = 0x7f010010
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final catalyst_slide_down:I = 0x7f010011
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final catalyst_slide_up:I = 0x7f010012
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final design_bottom_sheet_slide_in:I = 0x7f010013
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final design_bottom_sheet_slide_out:I = 0x7f010014
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final design_snackbar_in:I = 0x7f010015
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field

.field public static final design_snackbar_out:I = 0x7f010016
    .annotation build Landroidx/annotation/AnimRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
