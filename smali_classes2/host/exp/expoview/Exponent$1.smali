.class Lhost/exp/expoview/Exponent$1;
.super Ljava/lang/Object;
.source "Exponent.java"

# interfaces
.implements Lhost/exp/exponent/network/ExpoHttpCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/expoview/Exponent;-><init>(Landroid/content/Context;Landroid/app/Application;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/expoview/Exponent;


# direct methods
.method constructor <init>(Lhost/exp/expoview/Exponent;)V
    .locals 0

    .line 149
    iput-object p1, p0, Lhost/exp/expoview/Exponent$1;->this$0:Lhost/exp/expoview/Exponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/io/IOException;)V
    .locals 1

    .line 152
    invoke-static {}, Lhost/exp/expoview/Exponent;->access$000()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onResponse(Lhost/exp/exponent/network/ExpoResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 157
    invoke-static {p1}, Lhost/exp/exponent/network/ExponentNetwork;->flushResponse(Lhost/exp/exponent/network/ExpoResponse;)V

    .line 158
    invoke-static {}, Lhost/exp/expoview/Exponent;->access$000()Ljava/lang/String;

    move-result-object p1

    const-string v0, "Loaded exp.host status page."

    invoke-static {p1, v0}, Lhost/exp/exponent/analytics/EXL;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
