.class public final Lhost/exp/expoview/R2$attr;
.super Ljava/lang/Object;
.source "R2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/expoview/R2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final actionBarDivider:I = 0x7f040001
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionBarItemBackground:I = 0x7f040002
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionBarPopupTheme:I = 0x7f040003
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionBarSize:I = 0x7f040004
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionBarSplitStyle:I = 0x7f040005
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionBarStyle:I = 0x7f040006
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionBarTabBarStyle:I = 0x7f040007
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionBarTabStyle:I = 0x7f040008
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionBarTabTextStyle:I = 0x7f040009
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionBarTheme:I = 0x7f04000a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionBarWidgetTheme:I = 0x7f04000b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionButtonStyle:I = 0x7f04000c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionDropDownStyle:I = 0x7f04000d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionLayout:I = 0x7f04000e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionMenuTextAppearance:I = 0x7f04000f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionMenuTextColor:I = 0x7f040010
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionModeBackground:I = 0x7f040011
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionModeCloseButtonStyle:I = 0x7f040012
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionModeCloseDrawable:I = 0x7f040013
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionModeCopyDrawable:I = 0x7f040014
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionModeCutDrawable:I = 0x7f040015
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionModeFindDrawable:I = 0x7f040016
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionModePasteDrawable:I = 0x7f040017
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionModePopupWindowStyle:I = 0x7f040018
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionModeSelectAllDrawable:I = 0x7f040019
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionModeShareDrawable:I = 0x7f04001a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionModeSplitBackground:I = 0x7f04001b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionModeStyle:I = 0x7f04001c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionModeWebSearchDrawable:I = 0x7f04001d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionOverflowButtonStyle:I = 0x7f04001e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionOverflowMenuStyle:I = 0x7f04001f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionProviderClass:I = 0x7f040020
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actionViewClass:I = 0x7f040021
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final activityChooserViewStyle:I = 0x7f040022
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actualImageResource:I = 0x7f040023
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actualImageScaleType:I = 0x7f040024
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final actualImageUri:I = 0x7f040025
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final alertDialogButtonGroupStyle:I = 0x7f040026
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final alertDialogCenterButtons:I = 0x7f040027
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final alertDialogStyle:I = 0x7f040028
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final alertDialogTheme:I = 0x7f040029
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final allowStacking:I = 0x7f04002a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final alpha:I = 0x7f04002b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final alphabeticModifiers:I = 0x7f04002c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final ambientEnabled:I = 0x7f04002d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final appTheme:I = 0x7f04002e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final arrowHeadLength:I = 0x7f04002f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final arrowShaftLength:I = 0x7f040030
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final autoCompleteTextViewStyle:I = 0x7f040031
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final autoSizeMaxTextSize:I = 0x7f040032
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final autoSizeMinTextSize:I = 0x7f040033
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final autoSizePresetSizes:I = 0x7f040034
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final autoSizeStepGranularity:I = 0x7f040035
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final autoSizeTextType:I = 0x7f040036
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final background:I = 0x7f040037
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final backgroundImage:I = 0x7f040038
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final backgroundSplit:I = 0x7f040039
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final backgroundStacked:I = 0x7f04003a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final backgroundTint:I = 0x7f04003b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final backgroundTintMode:I = 0x7f04003c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final barLength:I = 0x7f04003d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final behavior_autoHide:I = 0x7f04003e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final behavior_fitToContents:I = 0x7f04003f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final behavior_hideable:I = 0x7f040040
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final behavior_overlapTop:I = 0x7f040041
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final behavior_peekHeight:I = 0x7f040042
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final behavior_skipCollapsed:I = 0x7f040043
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final borderWidth:I = 0x7f040044
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final borderlessButtonStyle:I = 0x7f040045
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final bottomAppBarStyle:I = 0x7f040046
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final bottomNavigationStyle:I = 0x7f040047
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final bottomSheetDialogTheme:I = 0x7f040048
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final bottomSheetStyle:I = 0x7f040049
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final boxBackgroundColor:I = 0x7f04004a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final boxBackgroundMode:I = 0x7f04004b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final boxCollapsedPaddingTop:I = 0x7f04004c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final boxCornerRadiusBottomEnd:I = 0x7f04004d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final boxCornerRadiusBottomStart:I = 0x7f04004e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final boxCornerRadiusTopEnd:I = 0x7f04004f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final boxCornerRadiusTopStart:I = 0x7f040050
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final boxStrokeColor:I = 0x7f040051
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final boxStrokeWidth:I = 0x7f040052
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final buttonBarButtonStyle:I = 0x7f040053
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final buttonBarNegativeButtonStyle:I = 0x7f040054
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final buttonBarNeutralButtonStyle:I = 0x7f040055
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final buttonBarPositiveButtonStyle:I = 0x7f040056
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final buttonBarStyle:I = 0x7f040057
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final buttonGravity:I = 0x7f040058
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final buttonIconDimen:I = 0x7f040059
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final buttonPanelSideLayout:I = 0x7f04005a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final buttonSize:I = 0x7f04005b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final buttonStyle:I = 0x7f04005c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final buttonStyleSmall:I = 0x7f04005d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final buttonTint:I = 0x7f04005e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final buttonTintMode:I = 0x7f04005f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final buyButtonAppearance:I = 0x7f040060
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final buyButtonHeight:I = 0x7f040061
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final buyButtonText:I = 0x7f040062
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final buyButtonWidth:I = 0x7f040063
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cameraBearing:I = 0x7f040064
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cameraMaxZoomPreference:I = 0x7f040065
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cameraMinZoomPreference:I = 0x7f040066
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cameraTargetLat:I = 0x7f040067
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cameraTargetLng:I = 0x7f040068
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cameraTilt:I = 0x7f040069
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cameraZoom:I = 0x7f04006a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cardBackgroundColor:I = 0x7f04006b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cardCornerRadius:I = 0x7f04006c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cardElevation:I = 0x7f04006d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cardMaxElevation:I = 0x7f04006e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cardPreventCornerOverlap:I = 0x7f04006f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cardUseCompatPadding:I = 0x7f040070
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cardViewStyle:I = 0x7f040071
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final checkboxStyle:I = 0x7f040072
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final checkedChip:I = 0x7f040073
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final checkedIcon:I = 0x7f040074
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final checkedIconEnabled:I = 0x7f040075
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final checkedIconVisible:I = 0x7f040076
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final checkedTextViewStyle:I = 0x7f040077
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipBackgroundColor:I = 0x7f040078
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipCornerRadius:I = 0x7f040079
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipEndPadding:I = 0x7f04007a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipGroupStyle:I = 0x7f04007b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipIcon:I = 0x7f04007c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipIconEnabled:I = 0x7f04007d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipIconSize:I = 0x7f04007e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipIconTint:I = 0x7f04007f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipIconVisible:I = 0x7f040080
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipMinHeight:I = 0x7f040081
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipSpacing:I = 0x7f040082
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipSpacingHorizontal:I = 0x7f040083
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipSpacingVertical:I = 0x7f040084
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipStandaloneStyle:I = 0x7f040085
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipStartPadding:I = 0x7f040086
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipStrokeColor:I = 0x7f040087
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipStrokeWidth:I = 0x7f040088
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final chipStyle:I = 0x7f040089
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final circleCrop:I = 0x7f04008a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final closeIcon:I = 0x7f04008b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final closeIconEnabled:I = 0x7f04008c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final closeIconEndPadding:I = 0x7f04008d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final closeIconSize:I = 0x7f04008e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final closeIconStartPadding:I = 0x7f04008f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final closeIconTint:I = 0x7f040090
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final closeIconVisible:I = 0x7f040091
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final closeItemLayout:I = 0x7f040092
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final collapseContentDescription:I = 0x7f040093
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final collapseIcon:I = 0x7f040094
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final collapsedTitleGravity:I = 0x7f040095
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final collapsedTitleTextAppearance:I = 0x7f040096
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final color:I = 0x7f040097
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final colorAccent:I = 0x7f040098
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final colorBackgroundFloating:I = 0x7f040099
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final colorButtonNormal:I = 0x7f04009a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final colorControlActivated:I = 0x7f04009b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final colorControlHighlight:I = 0x7f04009c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final colorControlNormal:I = 0x7f04009d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final colorError:I = 0x7f04009e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final colorPrimary:I = 0x7f04009f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final colorPrimaryDark:I = 0x7f0400a0
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final colorScheme:I = 0x7f0400a1
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final colorSecondary:I = 0x7f0400a2
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final colorSwitchThumbNormal:I = 0x7f0400a3
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final commitIcon:I = 0x7f0400a4
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final contentDescription:I = 0x7f0400a5
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final contentInsetEnd:I = 0x7f0400a6
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final contentInsetEndWithActions:I = 0x7f0400a7
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final contentInsetLeft:I = 0x7f0400a8
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final contentInsetRight:I = 0x7f0400a9
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final contentInsetStart:I = 0x7f0400aa
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final contentInsetStartWithNavigation:I = 0x7f0400ab
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final contentPadding:I = 0x7f0400ac
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final contentPaddingBottom:I = 0x7f0400ad
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final contentPaddingLeft:I = 0x7f0400ae
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final contentPaddingRight:I = 0x7f0400af
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final contentPaddingTop:I = 0x7f0400b0
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final contentScrim:I = 0x7f0400b1
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final controlBackground:I = 0x7f0400b2
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final coordinatorLayoutStyle:I = 0x7f0400b3
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cornerRadius:I = 0x7f0400b4
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final counterEnabled:I = 0x7f0400b5
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final counterMaxLength:I = 0x7f0400b6
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final counterOverflowTextAppearance:I = 0x7f0400b7
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final counterTextAppearance:I = 0x7f0400b8
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropAspectRatioX:I = 0x7f0400b9
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropAspectRatioY:I = 0x7f0400ba
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropAutoZoomEnabled:I = 0x7f0400bb
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropBackgroundColor:I = 0x7f0400bc
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropBorderCornerColor:I = 0x7f0400bd
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropBorderCornerLength:I = 0x7f0400be
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropBorderCornerOffset:I = 0x7f0400bf
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropBorderCornerThickness:I = 0x7f0400c0
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropBorderLineColor:I = 0x7f0400c1
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropBorderLineThickness:I = 0x7f0400c2
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropFixAspectRatio:I = 0x7f0400c3
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropFlipHorizontally:I = 0x7f0400c4
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropFlipVertically:I = 0x7f0400c5
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropGuidelines:I = 0x7f0400c6
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropGuidelinesColor:I = 0x7f0400c7
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropGuidelinesThickness:I = 0x7f0400c8
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropInitialCropWindowPaddingRatio:I = 0x7f0400c9
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropMaxCropResultHeightPX:I = 0x7f0400ca
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropMaxCropResultWidthPX:I = 0x7f0400cb
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropMaxZoom:I = 0x7f0400cc
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropMinCropResultHeightPX:I = 0x7f0400cd
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropMinCropResultWidthPX:I = 0x7f0400ce
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropMinCropWindowHeight:I = 0x7f0400cf
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropMinCropWindowWidth:I = 0x7f0400d0
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropMultiTouchEnabled:I = 0x7f0400d1
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropSaveBitmapToInstanceState:I = 0x7f0400d2
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropScaleType:I = 0x7f0400d3
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropShape:I = 0x7f0400d4
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropShowCropOverlay:I = 0x7f0400d5
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropShowProgressBar:I = 0x7f0400d6
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropSnapRadius:I = 0x7f0400d7
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final cropTouchRadius:I = 0x7f0400d8
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final customNavigationLayout:I = 0x7f0400d9
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final customThemeStyle:I = 0x7f0400da
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final defaultQueryHint:I = 0x7f0400db
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final dialogCornerRadius:I = 0x7f0400dc
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final dialogPreferredPadding:I = 0x7f0400dd
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final dialogTheme:I = 0x7f0400de
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final displayOptions:I = 0x7f0400df
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final divider:I = 0x7f0400e0
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final dividerHorizontal:I = 0x7f0400e1
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final dividerPadding:I = 0x7f0400e2
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final dividerVertical:I = 0x7f0400e3
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final drawableSize:I = 0x7f0400e4
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final drawerArrowStyle:I = 0x7f0400e5
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final dropDownListViewStyle:I = 0x7f0400e6
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final dropdownListPreferredItemHeight:I = 0x7f0400e7
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final editTextBackground:I = 0x7f0400e8
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final editTextColor:I = 0x7f0400e9
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final editTextStyle:I = 0x7f0400ea
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final elevation:I = 0x7f0400eb
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final enforceMaterialTheme:I = 0x7f0400ec
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final enforceTextAppearance:I = 0x7f0400ed
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final environment:I = 0x7f0400ee
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final errorEnabled:I = 0x7f0400ef
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final errorTextAppearance:I = 0x7f0400f0
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final expandActivityOverflowButtonDrawable:I = 0x7f0400f1
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final expanded:I = 0x7f0400f2
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final expandedTitleGravity:I = 0x7f0400f3
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final expandedTitleMargin:I = 0x7f0400f4
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final expandedTitleMarginBottom:I = 0x7f0400f5
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final expandedTitleMarginEnd:I = 0x7f0400f6
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final expandedTitleMarginStart:I = 0x7f0400f7
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final expandedTitleMarginTop:I = 0x7f0400f8
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final expandedTitleTextAppearance:I = 0x7f0400f9
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fabAlignmentMode:I = 0x7f0400fa
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fabCradleMargin:I = 0x7f0400fb
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fabCradleRoundedCornerRadius:I = 0x7f0400fc
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fabCradleVerticalOffset:I = 0x7f0400fd
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fabCustomSize:I = 0x7f0400fe
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fabSize:I = 0x7f0400ff
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fadeDuration:I = 0x7f040100
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final failureImage:I = 0x7f040101
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final failureImageScaleType:I = 0x7f040102
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fastScrollEnabled:I = 0x7f040103
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fastScrollHorizontalThumbDrawable:I = 0x7f040104
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fastScrollHorizontalTrackDrawable:I = 0x7f040105
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fastScrollVerticalThumbDrawable:I = 0x7f040106
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fastScrollVerticalTrackDrawable:I = 0x7f040107
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final firstBaselineToTopHeight:I = 0x7f040108
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final floatingActionButtonStyle:I = 0x7f040109
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final font:I = 0x7f04010a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fontFamily:I = 0x7f04010b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fontProviderAuthority:I = 0x7f04010c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fontProviderCerts:I = 0x7f04010d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fontProviderFetchStrategy:I = 0x7f04010e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fontProviderFetchTimeout:I = 0x7f04010f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fontProviderPackage:I = 0x7f040110
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fontProviderQuery:I = 0x7f040111
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fontStyle:I = 0x7f040112
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fontVariationSettings:I = 0x7f040113
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fontWeight:I = 0x7f040114
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final foregroundInsidePadding:I = 0x7f040115
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fragmentMode:I = 0x7f040116
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final fragmentStyle:I = 0x7f040117
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final gapBetweenBars:I = 0x7f040118
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final goIcon:I = 0x7f040119
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final headerLayout:I = 0x7f04011a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final height:I = 0x7f04011b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final helperText:I = 0x7f04011c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final helperTextEnabled:I = 0x7f04011d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final helperTextTextAppearance:I = 0x7f04011e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final hideMotionSpec:I = 0x7f04011f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final hideOnContentScroll:I = 0x7f040120
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final hideOnScroll:I = 0x7f040121
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final hintAnimationEnabled:I = 0x7f040122
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final hintEnabled:I = 0x7f040123
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final hintTextAppearance:I = 0x7f040124
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final homeAsUpIndicator:I = 0x7f040125
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final homeLayout:I = 0x7f040126
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final hoveredFocusedTranslationZ:I = 0x7f040127
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final icon:I = 0x7f040128
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final iconEndPadding:I = 0x7f040129
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final iconGravity:I = 0x7f04012a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final iconPadding:I = 0x7f04012b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final iconSize:I = 0x7f04012c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final iconStartPadding:I = 0x7f04012d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final iconTint:I = 0x7f04012e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final iconTintMode:I = 0x7f04012f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final iconifiedByDefault:I = 0x7f040130
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final imageAspectRatio:I = 0x7f040131
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final imageAspectRatioAdjust:I = 0x7f040132
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final imageButtonStyle:I = 0x7f040133
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final indeterminateProgressStyle:I = 0x7f040134
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final initialActivityCount:I = 0x7f040135
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final insetForeground:I = 0x7f040136
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final isLightTheme:I = 0x7f040137
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final itemBackground:I = 0x7f040138
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final itemHorizontalPadding:I = 0x7f040139
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final itemHorizontalTranslationEnabled:I = 0x7f04013a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final itemIconPadding:I = 0x7f04013b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final itemIconSize:I = 0x7f04013c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final itemIconTint:I = 0x7f04013d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final itemPadding:I = 0x7f04013e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final itemSpacing:I = 0x7f04013f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final itemTextAppearance:I = 0x7f040140
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final itemTextAppearanceActive:I = 0x7f040141
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final itemTextAppearanceInactive:I = 0x7f040142
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final itemTextColor:I = 0x7f040143
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final keylines:I = 0x7f040144
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final labelVisibilityMode:I = 0x7f040145
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final lastBaselineToBottomHeight:I = 0x7f040146
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final latLngBoundsNorthEastLatitude:I = 0x7f040147
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final latLngBoundsNorthEastLongitude:I = 0x7f040148
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final latLngBoundsSouthWestLatitude:I = 0x7f040149
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final latLngBoundsSouthWestLongitude:I = 0x7f04014a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final layout:I = 0x7f04014b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final layoutManager:I = 0x7f04014c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final layout_anchor:I = 0x7f04014d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final layout_anchorGravity:I = 0x7f04014e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final layout_behavior:I = 0x7f04014f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final layout_collapseMode:I = 0x7f040150
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final layout_collapseParallaxMultiplier:I = 0x7f040151
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final layout_dodgeInsetEdges:I = 0x7f040152
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final layout_insetEdge:I = 0x7f040153
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final layout_keyline:I = 0x7f040154
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final layout_scrollFlags:I = 0x7f040155
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final layout_scrollInterpolator:I = 0x7f040156
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final liftOnScroll:I = 0x7f040157
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final lineHeight:I = 0x7f040158
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final lineSpacing:I = 0x7f040159
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final listChoiceBackgroundIndicator:I = 0x7f04015a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final listDividerAlertDialog:I = 0x7f04015b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final listItemLayout:I = 0x7f04015c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final listLayout:I = 0x7f04015d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final listMenuViewStyle:I = 0x7f04015e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final listPopupWindowStyle:I = 0x7f04015f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final listPreferredItemHeight:I = 0x7f040160
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final listPreferredItemHeightLarge:I = 0x7f040161
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final listPreferredItemHeightSmall:I = 0x7f040162
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final listPreferredItemPaddingLeft:I = 0x7f040163
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final listPreferredItemPaddingRight:I = 0x7f040164
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final liteMode:I = 0x7f040165
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final logo:I = 0x7f040166
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final logoDescription:I = 0x7f040167
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final lottie_autoPlay:I = 0x7f040168
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final lottie_cacheStrategy:I = 0x7f040169
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final lottie_colorFilter:I = 0x7f04016a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final lottie_enableMergePathsForKitKatAndAbove:I = 0x7f04016b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final lottie_fileName:I = 0x7f04016c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final lottie_imageAssetsFolder:I = 0x7f04016d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final lottie_loop:I = 0x7f04016e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final lottie_progress:I = 0x7f04016f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final lottie_rawRes:I = 0x7f040170
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final lottie_repeatCount:I = 0x7f040171
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final lottie_repeatMode:I = 0x7f040172
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final lottie_scale:I = 0x7f040173
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final mapType:I = 0x7f040174
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final maskedWalletDetailsBackground:I = 0x7f040175
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final maskedWalletDetailsButtonBackground:I = 0x7f040176
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final maskedWalletDetailsButtonTextAppearance:I = 0x7f040177
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final maskedWalletDetailsHeaderTextAppearance:I = 0x7f040178
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final maskedWalletDetailsLogoImageType:I = 0x7f040179
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final maskedWalletDetailsLogoTextColor:I = 0x7f04017a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final maskedWalletDetailsTextAppearance:I = 0x7f04017b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final materialButtonStyle:I = 0x7f04017c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final materialCardViewStyle:I = 0x7f04017d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final maxActionInlineWidth:I = 0x7f04017e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final maxButtonHeight:I = 0x7f04017f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final maxImageSize:I = 0x7f040180
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final measureWithLargestChild:I = 0x7f040181
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final menu:I = 0x7f040182
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final multiChoiceItemLayout:I = 0x7f040183
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final navigationContentDescription:I = 0x7f040184
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final navigationIcon:I = 0x7f040185
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final navigationMode:I = 0x7f040186
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final navigationViewStyle:I = 0x7f040187
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final numericModifiers:I = 0x7f040188
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final overlapAnchor:I = 0x7f040189
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final overlayImage:I = 0x7f04018a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final paddingBottomNoButtons:I = 0x7f04018b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final paddingEnd:I = 0x7f04018c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final paddingStart:I = 0x7f04018d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final paddingTopNoTitle:I = 0x7f04018e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final panelBackground:I = 0x7f04018f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final panelMenuListTheme:I = 0x7f040190
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final panelMenuListWidth:I = 0x7f040191
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final passwordToggleContentDescription:I = 0x7f040192
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final passwordToggleDrawable:I = 0x7f040193
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final passwordToggleEnabled:I = 0x7f040194
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final passwordToggleTint:I = 0x7f040195
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final passwordToggleTintMode:I = 0x7f040196
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final placeholderImage:I = 0x7f040197
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final placeholderImageScaleType:I = 0x7f040198
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final popupMenuStyle:I = 0x7f040199
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final popupTheme:I = 0x7f04019a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final popupWindowStyle:I = 0x7f04019b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final preserveIconSpacing:I = 0x7f04019c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final pressedStateOverlayImage:I = 0x7f04019d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final pressedTranslationZ:I = 0x7f04019e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final progressBarAutoRotateInterval:I = 0x7f04019f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final progressBarImage:I = 0x7f0401a0
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final progressBarImageScaleType:I = 0x7f0401a1
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final progressBarPadding:I = 0x7f0401a2
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final progressBarStyle:I = 0x7f0401a3
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final queryBackground:I = 0x7f0401a4
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final queryHint:I = 0x7f0401a5
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final radioButtonStyle:I = 0x7f0401a6
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final ratingBarStyle:I = 0x7f0401a7
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final ratingBarStyleIndicator:I = 0x7f0401a8
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final ratingBarStyleSmall:I = 0x7f0401a9
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final retryImage:I = 0x7f0401aa
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final retryImageScaleType:I = 0x7f0401ab
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final reverseLayout:I = 0x7f0401ac
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final rippleColor:I = 0x7f0401ad
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final roundAsCircle:I = 0x7f0401ae
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final roundBottomEnd:I = 0x7f0401af
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final roundBottomLeft:I = 0x7f0401b0
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final roundBottomRight:I = 0x7f0401b1
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final roundBottomStart:I = 0x7f0401b2
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final roundTopEnd:I = 0x7f0401b3
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final roundTopLeft:I = 0x7f0401b4
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final roundTopRight:I = 0x7f0401b5
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final roundTopStart:I = 0x7f0401b6
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final roundWithOverlayColor:I = 0x7f0401b7
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final roundedCornerRadius:I = 0x7f0401b8
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final roundingBorderColor:I = 0x7f0401b9
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final roundingBorderPadding:I = 0x7f0401ba
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final roundingBorderWidth:I = 0x7f0401bb
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final scopeUris:I = 0x7f0401bc
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final scrimAnimationDuration:I = 0x7f0401bd
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final scrimBackground:I = 0x7f0401be
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final scrimVisibleHeightTrigger:I = 0x7f0401bf
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final searchHintIcon:I = 0x7f0401c0
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final searchIcon:I = 0x7f0401c1
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final searchViewStyle:I = 0x7f0401c2
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final seekBarStyle:I = 0x7f0401c3
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final selectableItemBackground:I = 0x7f0401c4
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final selectableItemBackgroundBorderless:I = 0x7f0401c5
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final showAsAction:I = 0x7f0401c6
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final showDividers:I = 0x7f0401c7
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final showMotionSpec:I = 0x7f0401c8
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final showText:I = 0x7f0401c9
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final showTitle:I = 0x7f0401ca
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final singleChoiceItemLayout:I = 0x7f0401cb
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final singleLine:I = 0x7f0401cc
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final singleSelection:I = 0x7f0401cd
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final snackbarButtonStyle:I = 0x7f0401ce
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final snackbarStyle:I = 0x7f0401cf
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final spanCount:I = 0x7f0401d0
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final spinBars:I = 0x7f0401d1
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final spinnerDropDownItemStyle:I = 0x7f0401d2
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final spinnerStyle:I = 0x7f0401d3
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final splitTrack:I = 0x7f0401d4
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final srcCompat:I = 0x7f0401d5
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final stackFromEnd:I = 0x7f0401d6
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final state_above_anchor:I = 0x7f0401d7
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final state_collapsed:I = 0x7f0401d8
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final state_collapsible:I = 0x7f0401d9
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final state_liftable:I = 0x7f0401da
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final state_lifted:I = 0x7f0401db
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final statusBarBackground:I = 0x7f0401dc
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final statusBarScrim:I = 0x7f0401dd
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final strokeColor:I = 0x7f0401de
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final strokeWidth:I = 0x7f0401df
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final subMenuArrow:I = 0x7f0401e0
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final submitBackground:I = 0x7f0401e1
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final subtitle:I = 0x7f0401e2
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final subtitleTextAppearance:I = 0x7f0401e3
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final subtitleTextColor:I = 0x7f0401e4
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final subtitleTextStyle:I = 0x7f0401e5
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final suggestionRowLayout:I = 0x7f0401e6
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final switchMinWidth:I = 0x7f0401e7
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final switchPadding:I = 0x7f0401e8
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final switchStyle:I = 0x7f0401e9
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final switchTextAppearance:I = 0x7f0401ea
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabBackground:I = 0x7f0401eb
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabContentStart:I = 0x7f0401ec
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabGravity:I = 0x7f0401ed
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabIconTint:I = 0x7f0401ee
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabIconTintMode:I = 0x7f0401ef
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabIndicator:I = 0x7f0401f0
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabIndicatorAnimationDuration:I = 0x7f0401f1
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabIndicatorColor:I = 0x7f0401f2
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabIndicatorFullWidth:I = 0x7f0401f3
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabIndicatorGravity:I = 0x7f0401f4
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabIndicatorHeight:I = 0x7f0401f5
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabInlineLabel:I = 0x7f0401f6
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabMaxWidth:I = 0x7f0401f7
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabMinWidth:I = 0x7f0401f8
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabMode:I = 0x7f0401f9
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabPadding:I = 0x7f0401fa
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabPaddingBottom:I = 0x7f0401fb
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabPaddingEnd:I = 0x7f0401fc
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabPaddingStart:I = 0x7f0401fd
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabPaddingTop:I = 0x7f0401fe
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabRippleColor:I = 0x7f0401ff
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabSelectedTextColor:I = 0x7f040200
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabStyle:I = 0x7f040201
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabTextAppearance:I = 0x7f040202
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabTextColor:I = 0x7f040203
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tabUnboundedRipple:I = 0x7f040204
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAllCaps:I = 0x7f040205
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceBody1:I = 0x7f040206
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceBody2:I = 0x7f040207
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceButton:I = 0x7f040208
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceCaption:I = 0x7f040209
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceHeadline1:I = 0x7f04020a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceHeadline2:I = 0x7f04020b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceHeadline3:I = 0x7f04020c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceHeadline4:I = 0x7f04020d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceHeadline5:I = 0x7f04020e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceHeadline6:I = 0x7f04020f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceLargePopupMenu:I = 0x7f040210
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceListItem:I = 0x7f040211
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceListItemSecondary:I = 0x7f040212
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceListItemSmall:I = 0x7f040213
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceOverline:I = 0x7f040214
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearancePopupMenuHeader:I = 0x7f040215
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceSearchResultSubtitle:I = 0x7f040216
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceSearchResultTitle:I = 0x7f040217
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceSmallPopupMenu:I = 0x7f040218
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceSubtitle1:I = 0x7f040219
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textAppearanceSubtitle2:I = 0x7f04021a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textColorAlertDialogListItem:I = 0x7f04021b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textColorSearchUrl:I = 0x7f04021c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textEndPadding:I = 0x7f04021d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textInputStyle:I = 0x7f04021e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final textStartPadding:I = 0x7f04021f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final theme:I = 0x7f040220
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final thickness:I = 0x7f040221
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final thumbTextPadding:I = 0x7f040222
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final thumbTint:I = 0x7f040223
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final thumbTintMode:I = 0x7f040224
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tickMark:I = 0x7f040225
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tickMarkTint:I = 0x7f040226
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tickMarkTintMode:I = 0x7f040227
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tint:I = 0x7f040228
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tintMode:I = 0x7f040229
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final title:I = 0x7f04022a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final titleEnabled:I = 0x7f04022b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final titleMargin:I = 0x7f04022c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final titleMarginBottom:I = 0x7f04022d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final titleMarginEnd:I = 0x7f04022e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final titleMarginStart:I = 0x7f04022f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final titleMarginTop:I = 0x7f040230
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final titleMargins:I = 0x7f040231
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final titleTextAppearance:I = 0x7f040232
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final titleTextColor:I = 0x7f040233
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final titleTextStyle:I = 0x7f040234
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final toolbarId:I = 0x7f040235
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final toolbarNavigationButtonStyle:I = 0x7f040236
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final toolbarStyle:I = 0x7f040237
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final toolbarTextColorStyle:I = 0x7f040238
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tooltipForegroundColor:I = 0x7f040239
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tooltipFrameBackground:I = 0x7f04023a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final tooltipText:I = 0x7f04023b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final track:I = 0x7f04023c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final trackTint:I = 0x7f04023d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final trackTintMode:I = 0x7f04023e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final ttcIndex:I = 0x7f04023f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final uiCompass:I = 0x7f040240
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final uiMapToolbar:I = 0x7f040241
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final uiRotateGestures:I = 0x7f040242
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final uiScrollGestures:I = 0x7f040243
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final uiScrollGesturesDuringRotateOrZoom:I = 0x7f040244
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final uiTiltGestures:I = 0x7f040245
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final uiZoomControls:I = 0x7f040246
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final uiZoomGestures:I = 0x7f040247
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final useCompatPadding:I = 0x7f040248
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final useViewLifecycle:I = 0x7f040249
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final viewAspectRatio:I = 0x7f04024a
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final viewInflaterClass:I = 0x7f04024b
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final voiceIcon:I = 0x7f04024c
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final windowActionBar:I = 0x7f04024d
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final windowActionBarOverlay:I = 0x7f04024e
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final windowActionModeOverlay:I = 0x7f04024f
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final windowFixedHeightMajor:I = 0x7f040250
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final windowFixedHeightMinor:I = 0x7f040251
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final windowFixedWidthMajor:I = 0x7f040252
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final windowFixedWidthMinor:I = 0x7f040253
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final windowMinWidthMajor:I = 0x7f040254
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final windowMinWidthMinor:I = 0x7f040255
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final windowNoTitle:I = 0x7f040256
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final windowTransitionStyle:I = 0x7f040257
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field

.field public static final zOrderOnTop:I = 0x7f040258
    .annotation build Landroidx/annotation/AttrRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
