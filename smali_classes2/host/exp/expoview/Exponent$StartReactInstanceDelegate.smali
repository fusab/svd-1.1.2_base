.class public interface abstract Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;
.super Ljava/lang/Object;
.source "Exponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/expoview/Exponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "StartReactInstanceDelegate"
.end annotation


# virtual methods
.method public abstract getExponentPackageDelegate()Lversioned/host/exp/exponent/ExponentPackageDelegate;
.end method

.method public abstract handleUnreadNotifications(Lorg/json/JSONArray;)V
.end method

.method public abstract isDebugModeEnabled()Z
.end method

.method public abstract isInForeground()Z
.end method
