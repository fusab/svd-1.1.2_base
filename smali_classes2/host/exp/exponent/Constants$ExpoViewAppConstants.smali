.class public Lhost/exp/exponent/Constants$ExpoViewAppConstants;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/exponent/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExpoViewAppConstants"
.end annotation


# instance fields
.field public ANALYTICS_ENABLED:Z

.field public ANDROID_VERSION_CODE:I

.field public ARE_REMOTE_UPDATES_ENABLED:Z

.field public EMBEDDED_RESPONSES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lhost/exp/exponent/Constants$EmbeddedResponse;",
            ">;"
        }
    .end annotation
.end field

.field public FCM_ENABLED:Z

.field public INITIAL_URL:Ljava/lang/String;

.field public IS_DETACHED:Z

.field public RELEASE_CHANNEL:Ljava/lang/String;

.field public SHELL_APP_SCHEME:Ljava/lang/String;

.field public SHOW_LOADING_VIEW_IN_SHELL_APP:Z

.field public VERSION_NAME:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
