.class public Lhost/exp/exponent/network/OkHttpV1ExpoResponse;
.super Ljava/lang/Object;
.source "OkHttpV1ExpoResponse.java"

# interfaces
.implements Lhost/exp/exponent/network/ExpoResponse;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhost/exp/exponent/network/OkHttpV1ExpoResponse$OkHttpV1ExpoHeaders;,
        Lhost/exp/exponent/network/OkHttpV1ExpoResponse$OkHttpV1ExpoBody;
    }
.end annotation


# instance fields
.field mOkHttpResponse:Lokhttp3/Response;


# direct methods
.method public constructor <init>(Lokhttp3/Response;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lhost/exp/exponent/network/OkHttpV1ExpoResponse;->mOkHttpResponse:Lokhttp3/Response;

    return-void
.end method


# virtual methods
.method public body()Lhost/exp/exponent/network/ExpoBody;
    .locals 2

    .line 57
    new-instance v0, Lhost/exp/exponent/network/OkHttpV1ExpoResponse$OkHttpV1ExpoBody;

    iget-object v1, p0, Lhost/exp/exponent/network/OkHttpV1ExpoResponse;->mOkHttpResponse:Lokhttp3/Response;

    invoke-virtual {v1}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lhost/exp/exponent/network/OkHttpV1ExpoResponse$OkHttpV1ExpoBody;-><init>(Lhost/exp/exponent/network/OkHttpV1ExpoResponse;Lokhttp3/ResponseBody;)V

    return-object v0
.end method

.method public code()I
    .locals 1

    .line 62
    iget-object v0, p0, Lhost/exp/exponent/network/OkHttpV1ExpoResponse;->mOkHttpResponse:Lokhttp3/Response;

    invoke-virtual {v0}, Lokhttp3/Response;->code()I

    move-result v0

    return v0
.end method

.method public headers()Lhost/exp/exponent/network/ExpoHeaders;
    .locals 2

    .line 67
    new-instance v0, Lhost/exp/exponent/network/OkHttpV1ExpoResponse$OkHttpV1ExpoHeaders;

    iget-object v1, p0, Lhost/exp/exponent/network/OkHttpV1ExpoResponse;->mOkHttpResponse:Lokhttp3/Response;

    invoke-virtual {v1}, Lokhttp3/Response;->headers()Lokhttp3/Headers;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lhost/exp/exponent/network/OkHttpV1ExpoResponse$OkHttpV1ExpoHeaders;-><init>(Lhost/exp/exponent/network/OkHttpV1ExpoResponse;Lokhttp3/Headers;)V

    return-object v0
.end method

.method public isSuccessful()Z
    .locals 1

    .line 52
    iget-object v0, p0, Lhost/exp/exponent/network/OkHttpV1ExpoResponse;->mOkHttpResponse:Lokhttp3/Response;

    invoke-virtual {v0}, Lokhttp3/Response;->isSuccessful()Z

    move-result v0

    return v0
.end method

.method public networkResponse()Lhost/exp/exponent/network/ExpoResponse;
    .locals 2

    .line 72
    iget-object v0, p0, Lhost/exp/exponent/network/OkHttpV1ExpoResponse;->mOkHttpResponse:Lokhttp3/Response;

    invoke-virtual {v0}, Lokhttp3/Response;->networkResponse()Lokhttp3/Response;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 75
    :cond_0
    new-instance v0, Lhost/exp/exponent/network/OkHttpV1ExpoResponse;

    iget-object v1, p0, Lhost/exp/exponent/network/OkHttpV1ExpoResponse;->mOkHttpResponse:Lokhttp3/Response;

    invoke-virtual {v1}, Lokhttp3/Response;->networkResponse()Lokhttp3/Response;

    move-result-object v1

    invoke-direct {v0, v1}, Lhost/exp/exponent/network/OkHttpV1ExpoResponse;-><init>(Lokhttp3/Response;)V

    return-object v0
.end method
