.class Lhost/exp/exponent/network/ManualExpoResponse$ManualExpoBody;
.super Ljava/lang/Object;
.source "ManualExpoResponse.java"

# interfaces
.implements Lhost/exp/exponent/network/ExpoBody;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/exponent/network/ManualExpoResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ManualExpoBody"
.end annotation


# instance fields
.field private mString:Ljava/lang/String;

.field final synthetic this$0:Lhost/exp/exponent/network/ManualExpoResponse;


# direct methods
.method constructor <init>(Lhost/exp/exponent/network/ManualExpoResponse;Ljava/lang/String;)V
    .locals 0

    .line 22
    iput-object p1, p0, Lhost/exp/exponent/network/ManualExpoResponse$ManualExpoBody;->this$0:Lhost/exp/exponent/network/ManualExpoResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p2, p0, Lhost/exp/exponent/network/ManualExpoResponse$ManualExpoBody;->mString:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public byteStream()Ljava/io/InputStream;
    .locals 2

    .line 33
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lhost/exp/exponent/network/ManualExpoResponse$ManualExpoBody;->mString:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v0
.end method

.method public bytes()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lhost/exp/exponent/network/ManualExpoResponse$ManualExpoBody;->mString:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public string()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lhost/exp/exponent/network/ManualExpoResponse$ManualExpoBody;->mString:Ljava/lang/String;

    return-object v0
.end method
