.class Lhost/exp/exponent/network/ExponentHttpClient$2;
.super Ljava/lang/Object;
.source "ExponentHttpClient.java"

# interfaces
.implements Lokhttp3/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/network/ExponentHttpClient;->callSafe(Lokhttp3/Request;Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/network/ExponentHttpClient;

.field final synthetic val$callback:Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;

.field final synthetic val$request:Lokhttp3/Request;

.field final synthetic val$uri:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhost/exp/exponent/network/ExponentHttpClient;Ljava/lang/String;Lokhttp3/Request;Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;)V
    .locals 0

    .line 72
    iput-object p1, p0, Lhost/exp/exponent/network/ExponentHttpClient$2;->this$0:Lhost/exp/exponent/network/ExponentHttpClient;

    iput-object p2, p0, Lhost/exp/exponent/network/ExponentHttpClient$2;->val$uri:Ljava/lang/String;

    iput-object p3, p0, Lhost/exp/exponent/network/ExponentHttpClient$2;->val$request:Lokhttp3/Request;

    iput-object p4, p0, Lhost/exp/exponent/network/ExponentHttpClient$2;->val$callback:Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lokhttp3/Call;Ljava/io/IOException;)V
    .locals 6

    .line 75
    iget-object v0, p0, Lhost/exp/exponent/network/ExponentHttpClient$2;->this$0:Lhost/exp/exponent/network/ExponentHttpClient;

    iget-object v1, p0, Lhost/exp/exponent/network/ExponentHttpClient$2;->val$uri:Ljava/lang/String;

    iget-object v2, p0, Lhost/exp/exponent/network/ExponentHttpClient$2;->val$request:Lokhttp3/Request;

    iget-object v3, p0, Lhost/exp/exponent/network/ExponentHttpClient$2;->val$callback:Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;

    const/4 v4, 0x0

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lhost/exp/exponent/network/ExponentHttpClient;->tryForcedCachedResponse(Ljava/lang/String;Lokhttp3/Request;Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;Lokhttp3/Response;Ljava/io/IOException;)V

    return-void
.end method

.method public onResponse(Lokhttp3/Call;Lokhttp3/Response;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 80
    invoke-virtual {p2}, Lokhttp3/Response;->isSuccessful()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 81
    iget-object p1, p0, Lhost/exp/exponent/network/ExponentHttpClient$2;->val$callback:Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;

    new-instance v0, Lhost/exp/exponent/network/OkHttpV1ExpoResponse;

    invoke-direct {v0, p2}, Lhost/exp/exponent/network/OkHttpV1ExpoResponse;-><init>(Lokhttp3/Response;)V

    invoke-interface {p1, v0}, Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;->onResponse(Lhost/exp/exponent/network/ExpoResponse;)V

    goto :goto_0

    .line 83
    :cond_0
    iget-object v1, p0, Lhost/exp/exponent/network/ExponentHttpClient$2;->this$0:Lhost/exp/exponent/network/ExponentHttpClient;

    iget-object v2, p0, Lhost/exp/exponent/network/ExponentHttpClient$2;->val$uri:Ljava/lang/String;

    iget-object v3, p0, Lhost/exp/exponent/network/ExponentHttpClient$2;->val$request:Lokhttp3/Request;

    iget-object v4, p0, Lhost/exp/exponent/network/ExponentHttpClient$2;->val$callback:Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;

    const/4 v6, 0x0

    move-object v5, p2

    invoke-virtual/range {v1 .. v6}, Lhost/exp/exponent/network/ExponentHttpClient;->tryForcedCachedResponse(Ljava/lang/String;Lokhttp3/Request;Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;Lokhttp3/Response;Ljava/io/IOException;)V

    :goto_0
    return-void
.end method
