.class public Lhost/exp/exponent/network/ManualExpoResponse;
.super Ljava/lang/Object;
.source "ManualExpoResponse.java"

# interfaces
.implements Lhost/exp/exponent/network/ExpoResponse;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhost/exp/exponent/network/ManualExpoResponse$ManualExpoHeaders;,
        Lhost/exp/exponent/network/ManualExpoResponse$ManualExpoBody;
    }
.end annotation


# instance fields
.field private mBody:Lhost/exp/exponent/network/ExpoBody;

.field private mCode:I

.field private mHeaders:Lhost/exp/exponent/network/ManualExpoResponse$ManualExpoHeaders;

.field private mIsSuccessful:Z

.field private mNetworkResponse:Lhost/exp/exponent/network/ExpoResponse;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 12
    iput-boolean v0, p0, Lhost/exp/exponent/network/ManualExpoResponse;->mIsSuccessful:Z

    const/4 v0, 0x0

    .line 13
    iput-object v0, p0, Lhost/exp/exponent/network/ManualExpoResponse;->mBody:Lhost/exp/exponent/network/ExpoBody;

    const/16 v1, 0xc8

    .line 14
    iput v1, p0, Lhost/exp/exponent/network/ManualExpoResponse;->mCode:I

    .line 15
    new-instance v1, Lhost/exp/exponent/network/ManualExpoResponse$ManualExpoHeaders;

    invoke-direct {v1, p0}, Lhost/exp/exponent/network/ManualExpoResponse$ManualExpoHeaders;-><init>(Lhost/exp/exponent/network/ManualExpoResponse;)V

    iput-object v1, p0, Lhost/exp/exponent/network/ManualExpoResponse;->mHeaders:Lhost/exp/exponent/network/ManualExpoResponse$ManualExpoHeaders;

    .line 16
    iput-object v0, p0, Lhost/exp/exponent/network/ManualExpoResponse;->mNetworkResponse:Lhost/exp/exponent/network/ExpoResponse;

    return-void
.end method


# virtual methods
.method public body()Lhost/exp/exponent/network/ExpoBody;
    .locals 1

    .line 79
    iget-object v0, p0, Lhost/exp/exponent/network/ManualExpoResponse;->mBody:Lhost/exp/exponent/network/ExpoBody;

    return-object v0
.end method

.method public code()I
    .locals 1

    .line 84
    iget v0, p0, Lhost/exp/exponent/network/ManualExpoResponse;->mCode:I

    return v0
.end method

.method public headers()Lhost/exp/exponent/network/ExpoHeaders;
    .locals 1

    .line 89
    iget-object v0, p0, Lhost/exp/exponent/network/ManualExpoResponse;->mHeaders:Lhost/exp/exponent/network/ManualExpoResponse$ManualExpoHeaders;

    return-object v0
.end method

.method public isSuccessful()Z
    .locals 1

    .line 74
    iget-boolean v0, p0, Lhost/exp/exponent/network/ManualExpoResponse;->mIsSuccessful:Z

    return v0
.end method

.method public networkResponse()Lhost/exp/exponent/network/ExpoResponse;
    .locals 1

    .line 94
    iget-object v0, p0, Lhost/exp/exponent/network/ManualExpoResponse;->mNetworkResponse:Lhost/exp/exponent/network/ExpoResponse;

    return-object v0
.end method

.method public setBody(Ljava/lang/String;)V
    .locals 1

    .line 57
    new-instance v0, Lhost/exp/exponent/network/ManualExpoResponse$ManualExpoBody;

    invoke-direct {v0, p0, p1}, Lhost/exp/exponent/network/ManualExpoResponse$ManualExpoBody;-><init>(Lhost/exp/exponent/network/ManualExpoResponse;Ljava/lang/String;)V

    iput-object v0, p0, Lhost/exp/exponent/network/ManualExpoResponse;->mBody:Lhost/exp/exponent/network/ExpoBody;

    return-void
.end method

.method public setCode(I)V
    .locals 0

    .line 61
    iput p1, p0, Lhost/exp/exponent/network/ManualExpoResponse;->mCode:I

    return-void
.end method

.method public setHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 65
    iget-object v0, p0, Lhost/exp/exponent/network/ManualExpoResponse;->mHeaders:Lhost/exp/exponent/network/ManualExpoResponse$ManualExpoHeaders;

    iget-object v0, v0, Lhost/exp/exponent/network/ManualExpoResponse$ManualExpoHeaders;->headers:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setIsSuccessful(Z)V
    .locals 0

    .line 53
    iput-boolean p1, p0, Lhost/exp/exponent/network/ManualExpoResponse;->mIsSuccessful:Z

    return-void
.end method

.method public setNetworkResponse(Lhost/exp/exponent/network/ExpoResponse;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lhost/exp/exponent/network/ManualExpoResponse;->mNetworkResponse:Lhost/exp/exponent/network/ExpoResponse;

    return-void
.end method
