.class Lhost/exp/exponent/network/ExponentHttpClient$4;
.super Ljava/lang/Object;
.source "ExponentHttpClient.java"

# interfaces
.implements Lokhttp3/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/network/ExponentHttpClient;->tryForcedCachedResponse(Ljava/lang/String;Lokhttp3/Request;Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;Lokhttp3/Response;Ljava/io/IOException;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/network/ExponentHttpClient;

.field final synthetic val$callback:Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;

.field final synthetic val$initialException:Ljava/io/IOException;

.field final synthetic val$initialResponse:Lokhttp3/Response;

.field final synthetic val$uri:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhost/exp/exponent/network/ExponentHttpClient;Ljava/lang/String;Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;Lokhttp3/Response;Ljava/io/IOException;)V
    .locals 0

    .line 128
    iput-object p1, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->this$0:Lhost/exp/exponent/network/ExponentHttpClient;

    iput-object p2, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->val$uri:Ljava/lang/String;

    iput-object p3, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->val$callback:Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;

    iput-object p4, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->val$initialResponse:Lokhttp3/Response;

    iput-object p5, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->val$initialException:Ljava/io/IOException;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lokhttp3/Call;Ljava/io/IOException;)V
    .locals 6

    .line 131
    iget-object v0, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->this$0:Lhost/exp/exponent/network/ExponentHttpClient;

    iget-object v1, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->val$uri:Ljava/lang/String;

    iget-object v3, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->val$callback:Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;

    iget-object v4, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->val$initialResponse:Lokhttp3/Response;

    iget-object v5, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->val$initialException:Ljava/io/IOException;

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lhost/exp/exponent/network/ExponentHttpClient;->access$000(Lhost/exp/exponent/network/ExponentHttpClient;Ljava/lang/String;Lokhttp3/Call;Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;Lokhttp3/Response;Ljava/io/IOException;)V

    return-void
.end method

.method public onResponse(Lokhttp3/Call;Lokhttp3/Response;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 136
    invoke-virtual {p2}, Lokhttp3/Response;->isSuccessful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    iget-object p1, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->val$callback:Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;

    new-instance v0, Lhost/exp/exponent/network/OkHttpV1ExpoResponse;

    invoke-direct {v0, p2}, Lhost/exp/exponent/network/OkHttpV1ExpoResponse;-><init>(Lokhttp3/Response;)V

    const/4 p2, 0x0

    invoke-interface {p1, v0, p2}, Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;->onCachedResponse(Lhost/exp/exponent/network/ExpoResponse;Z)V

    .line 138
    iget-object p1, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->this$0:Lhost/exp/exponent/network/ExponentHttpClient;

    iget-object p2, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->val$uri:Ljava/lang/String;

    const-string v0, "HTTP_USED_CACHE_RESPONSE"

    invoke-static {p1, v0, p2}, Lhost/exp/exponent/network/ExponentHttpClient;->access$100(Lhost/exp/exponent/network/ExponentHttpClient;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 140
    :cond_0
    iget-object v1, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->this$0:Lhost/exp/exponent/network/ExponentHttpClient;

    iget-object v2, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->val$uri:Ljava/lang/String;

    iget-object v4, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->val$callback:Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;

    iget-object v5, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->val$initialResponse:Lokhttp3/Response;

    iget-object v6, p0, Lhost/exp/exponent/network/ExponentHttpClient$4;->val$initialException:Ljava/io/IOException;

    move-object v3, p1

    invoke-static/range {v1 .. v6}, Lhost/exp/exponent/network/ExponentHttpClient;->access$000(Lhost/exp/exponent/network/ExponentHttpClient;Ljava/lang/String;Lokhttp3/Call;Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;Lokhttp3/Response;Ljava/io/IOException;)V

    :goto_0
    return-void
.end method
