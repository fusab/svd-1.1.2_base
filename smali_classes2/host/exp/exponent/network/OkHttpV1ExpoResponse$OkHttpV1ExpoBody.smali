.class Lhost/exp/exponent/network/OkHttpV1ExpoResponse$OkHttpV1ExpoBody;
.super Ljava/lang/Object;
.source "OkHttpV1ExpoResponse.java"

# interfaces
.implements Lhost/exp/exponent/network/ExpoBody;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/exponent/network/OkHttpV1ExpoResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OkHttpV1ExpoBody"
.end annotation


# instance fields
.field mResponseBody:Lokhttp3/ResponseBody;

.field final synthetic this$0:Lhost/exp/exponent/network/OkHttpV1ExpoResponse;


# direct methods
.method public constructor <init>(Lhost/exp/exponent/network/OkHttpV1ExpoResponse;Lokhttp3/ResponseBody;)V
    .locals 0

    .line 17
    iput-object p1, p0, Lhost/exp/exponent/network/OkHttpV1ExpoResponse$OkHttpV1ExpoBody;->this$0:Lhost/exp/exponent/network/OkHttpV1ExpoResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p2, p0, Lhost/exp/exponent/network/OkHttpV1ExpoResponse$OkHttpV1ExpoBody;->mResponseBody:Lokhttp3/ResponseBody;

    return-void
.end method


# virtual methods
.method public byteStream()Ljava/io/InputStream;
    .locals 1

    .line 28
    iget-object v0, p0, Lhost/exp/exponent/network/OkHttpV1ExpoResponse$OkHttpV1ExpoBody;->mResponseBody:Lokhttp3/ResponseBody;

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->byteStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public bytes()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lhost/exp/exponent/network/OkHttpV1ExpoResponse$OkHttpV1ExpoBody;->mResponseBody:Lokhttp3/ResponseBody;

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->bytes()[B

    move-result-object v0

    return-object v0
.end method

.method public string()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lhost/exp/exponent/network/OkHttpV1ExpoResponse$OkHttpV1ExpoBody;->mResponseBody:Lokhttp3/ResponseBody;

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
