.class Lhost/exp/exponent/network/ExponentHttpClient$3;
.super Ljava/lang/Object;
.source "ExponentHttpClient.java"

# interfaces
.implements Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/network/ExponentHttpClient;->callDefaultCache(Lokhttp3/Request;Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/network/ExponentHttpClient;

.field final synthetic val$callback:Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;

.field final synthetic val$request:Lokhttp3/Request;


# direct methods
.method constructor <init>(Lhost/exp/exponent/network/ExponentHttpClient;Lokhttp3/Request;Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lhost/exp/exponent/network/ExponentHttpClient$3;->this$0:Lhost/exp/exponent/network/ExponentHttpClient;

    iput-object p2, p0, Lhost/exp/exponent/network/ExponentHttpClient$3;->val$request:Lokhttp3/Request;

    iput-object p3, p0, Lhost/exp/exponent/network/ExponentHttpClient$3;->val$callback:Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCachedResponse(Lhost/exp/exponent/network/ExpoResponse;Z)V
    .locals 1

    .line 116
    iget-object v0, p0, Lhost/exp/exponent/network/ExponentHttpClient$3;->val$callback:Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;

    invoke-interface {v0, p1, p2}, Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;->onCachedResponse(Lhost/exp/exponent/network/ExpoResponse;Z)V

    return-void
.end method

.method public onFailure(Ljava/io/IOException;)V
    .locals 2

    .line 96
    iget-object p1, p0, Lhost/exp/exponent/network/ExponentHttpClient$3;->this$0:Lhost/exp/exponent/network/ExponentHttpClient;

    iget-object v0, p0, Lhost/exp/exponent/network/ExponentHttpClient$3;->val$request:Lokhttp3/Request;

    new-instance v1, Lhost/exp/exponent/network/ExponentHttpClient$3$1;

    invoke-direct {v1, p0}, Lhost/exp/exponent/network/ExponentHttpClient$3$1;-><init>(Lhost/exp/exponent/network/ExponentHttpClient$3;)V

    invoke-virtual {p1, v0, v1}, Lhost/exp/exponent/network/ExponentHttpClient;->call(Lokhttp3/Request;Lhost/exp/exponent/network/ExpoHttpCallback;)V

    return-void
.end method

.method public onResponse(Lhost/exp/exponent/network/ExpoResponse;)V
    .locals 1

    .line 111
    iget-object v0, p0, Lhost/exp/exponent/network/ExponentHttpClient$3;->val$callback:Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;

    invoke-interface {v0, p1}, Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;->onResponse(Lhost/exp/exponent/network/ExpoResponse;)V

    return-void
.end method
