.class final Lhost/exp/exponent/storage/ExponentDB$1;
.super Ljava/lang/Object;
.source "ExponentDB.java"

# interfaces
.implements Lcom/raizlabs/android/dbflow/structure/database/transaction/QueryTransaction$QueryResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/storage/ExponentDB;->experienceIdToExperience(Ljava/lang/String;Lhost/exp/exponent/storage/ExponentDB$ExperienceResultListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/raizlabs/android/dbflow/structure/database/transaction/QueryTransaction$QueryResultCallback<",
        "Lhost/exp/exponent/storage/ExperienceDBObject;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$listener:Lhost/exp/exponent/storage/ExponentDB$ExperienceResultListener;


# direct methods
.method constructor <init>(Lhost/exp/exponent/storage/ExponentDB$ExperienceResultListener;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lhost/exp/exponent/storage/ExponentDB$1;->val$listener:Lhost/exp/exponent/storage/ExponentDB$ExperienceResultListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryResult(Lcom/raizlabs/android/dbflow/structure/database/transaction/QueryTransaction;Lcom/raizlabs/android/dbflow/sql/language/CursorResult;)V
    .locals 4
    .param p2    # Lcom/raizlabs/android/dbflow/sql/language/CursorResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/raizlabs/android/dbflow/structure/database/transaction/QueryTransaction<",
            "Lhost/exp/exponent/storage/ExperienceDBObject;",
            ">;",
            "Lcom/raizlabs/android/dbflow/sql/language/CursorResult<",
            "Lhost/exp/exponent/storage/ExperienceDBObject;",
            ">;)V"
        }
    .end annotation

    .line 54
    invoke-virtual {p2}, Lcom/raizlabs/android/dbflow/sql/language/CursorResult;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    .line 55
    iget-object p1, p0, Lhost/exp/exponent/storage/ExponentDB$1;->val$listener:Lhost/exp/exponent/storage/ExponentDB$ExperienceResultListener;

    invoke-interface {p1}, Lhost/exp/exponent/storage/ExponentDB$ExperienceResultListener;->onFailure()V

    goto :goto_0

    .line 57
    :cond_0
    iget-object p1, p0, Lhost/exp/exponent/storage/ExponentDB$1;->val$listener:Lhost/exp/exponent/storage/ExponentDB$ExperienceResultListener;

    invoke-virtual {p2, v2, v3}, Lcom/raizlabs/android/dbflow/sql/language/CursorResult;->getItem(J)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lhost/exp/exponent/storage/ExperienceDBObject;

    invoke-interface {p1, p2}, Lhost/exp/exponent/storage/ExponentDB$ExperienceResultListener;->onSuccess(Lhost/exp/exponent/storage/ExperienceDBObject;)V

    :goto_0
    return-void
.end method
