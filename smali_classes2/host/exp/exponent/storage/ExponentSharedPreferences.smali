.class public Lhost/exp/exponent/storage/ExponentSharedPreferences;
.super Ljava/lang/Object;
.source "ExponentSharedPreferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhost/exp/exponent/storage/ExponentSharedPreferences$ManifestAndBundleUrl;
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final DEFAULT_VALUES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final EXPERIENCE_METADATA_ALL_NOTIFICATION_IDS:Ljava/lang/String; = "allNotificationIds"

.field public static final EXPERIENCE_METADATA_ALL_SCHEDULED_NOTIFICATION_IDS:Ljava/lang/String; = "allScheduledNotificationIds"

.field public static final EXPERIENCE_METADATA_LAST_ERRORS:Ljava/lang/String; = "lastErrors"

.field public static final EXPERIENCE_METADATA_LOADING_ERROR:Ljava/lang/String; = "loadingError"

.field public static final EXPERIENCE_METADATA_NOTIFICATION_CHANNELS:Ljava/lang/String; = "notificationChannels"

.field public static final EXPERIENCE_METADATA_PERMISSIONS:Ljava/lang/String; = "permissions"

.field public static final EXPERIENCE_METADATA_PREFIX:Ljava/lang/String; = "experience_metadata_"

.field public static final EXPERIENCE_METADATA_UNREAD_REMOTE_NOTIFICATIONS:Ljava/lang/String; = "unreadNotifications"

.field public static final EXPO_AUTH_SESSION:Ljava/lang/String; = "expo_auth_session"

.field public static final EXPO_AUTH_SESSION_SECRET_KEY:Ljava/lang/String; = "sessionSecret"

.field public static final FCM_TOKEN_KEY:Ljava/lang/String; = "fcm_token"

.field public static final GCM_TOKEN_KEY:Ljava/lang/String; = "gcm_token"

.field public static final HAS_SAVED_SHORTCUT_KEY:Ljava/lang/String; = "has_saved_shortcut"

.field public static final IS_FIRST_KERNEL_RUN_KEY:Ljava/lang/String; = "is_first_kernel_run"

.field public static final KERNEL_REVISION_ID:Ljava/lang/String; = "kernel_revision_id"

.field public static final NUX_HAS_FINISHED_FIRST_RUN_KEY:Ljava/lang/String; = "nux_has_finished_first_run"

.field public static final REFERRER_KEY:Ljava/lang/String; = "referrer"

.field public static final SAFE_MANIFEST_KEY:Ljava/lang/String; = "safe_manifest"

.field public static final SHOULD_NOT_USE_KERNEL_CACHE:Ljava/lang/String; = "should_not_use_kernel_cache"

.field private static final TAG:Ljava/lang/String; = "ExponentSharedPreferences"

.field public static final USE_INTERNET_KERNEL_KEY:Ljava/lang/String; = "use_internet_kernel"

.field public static final UUID_KEY:Ljava/lang/String; = "uuid"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mSharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->DEFAULT_VALUES:Ljava/util/Map;

    .line 70
    sget-object v0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->DEFAULT_VALUES:Ljava/util/Map;

    sget-boolean v1, Lhost/exp/expoview/ExpoViewBuildConfig;->USE_INTERNET_KERNEL:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "use_internet_kernel"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->DEFAULT_VALUES:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "has_saved_shortcut"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->DEFAULT_VALUES:Ljava/util/Map;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "is_first_kernel_run"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->DEFAULT_VALUES:Ljava/util/Map;

    const-string v2, "nux_has_finished_first_run"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->DEFAULT_VALUES:Ljava/util/Map;

    const-string v2, "should_not_use_kernel_cache"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    sget v0, Lhost/exp/expoview/R$string;->preference_file_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 83
    iput-object p1, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public delete(Ljava/lang/String;)V
    .locals 1

    .line 115
    iget-object v0, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public getBoolean(Ljava/lang/String;)Z
    .locals 2

    .line 91
    iget-object v0, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    sget-object v1, Lhost/exp/exponent/storage/ExponentSharedPreferences;->DEFAULT_VALUES:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method public getBoolean(Ljava/lang/String;Z)Z
    .locals 1

    .line 95
    iget-object v0, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .line 87
    iget-object v0, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getExperienceMetadata(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 3

    .line 231
    iget-object v0, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "experience_metadata_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    return-object v1

    .line 237
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    .line 239
    sget-object v0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->TAG:Ljava/lang/String;

    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v1
.end method

.method public getManifest(Ljava/lang/String;)Lhost/exp/exponent/storage/ExponentSharedPreferences$ManifestAndBundleUrl;
    .locals 3

    .line 177
    iget-object v0, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    return-object v1

    .line 183
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "manifest"

    .line 184
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    const-string v2, "bundleUrl"

    .line 185
    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 187
    new-instance v2, Lhost/exp/exponent/storage/ExponentSharedPreferences$ManifestAndBundleUrl;

    invoke-direct {v2, p1, v0}, Lhost/exp/exponent/storage/ExponentSharedPreferences$ManifestAndBundleUrl;-><init>(Lorg/json/JSONObject;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    :catch_0
    move-exception p1

    .line 189
    sget-object v0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->TAG:Ljava/lang/String;

    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v1
.end method

.method public getOrCreateUUID()Ljava/lang/String;
    .locals 3

    .line 131
    iget-object v0, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "uuid"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 136
    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 137
    invoke-virtual {p0, v1, v0}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getSafeManifestString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 212
    iget-object v0, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    return-object v1

    .line 218
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "safe_manifest"

    .line 219
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 221
    sget-object v0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->TAG:Ljava/lang/String;

    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v1
.end method

.method public getSessionSecret()Ljava/lang/String;
    .locals 3

    const-string v0, "expo_auth_session"

    .line 150
    invoke-virtual {p0, v0}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 155
    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "sessionSecret"

    .line 156
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 158
    sget-object v2, Lhost/exp/exponent/storage/ExponentSharedPreferences;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v1
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 103
    invoke-virtual {p0, p1, v0}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 107
    iget-object v0, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getUUID()Ljava/lang/String;
    .locals 3

    .line 127
    iget-object v0, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "uuid"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasSavedShortcut()Z
    .locals 1

    const-string v0, "has_saved_shortcut"

    .line 123
    invoke-virtual {p0, v0}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public removeSession()V
    .locals 2

    const-string v0, "expo_auth_session"

    const/4 v1, 0x0

    .line 146
    invoke-virtual {p0, v0, v1}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setBoolean(Ljava/lang/String;Z)V
    .locals 1

    .line 99
    iget-object v0, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public setString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 111
    iget-object v0, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public shouldUseInternetKernel()Z
    .locals 1

    const-string v0, "use_internet_kernel"

    .line 119
    invoke-virtual {p0, v0}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public updateExperienceMetadata(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 3

    .line 227
    iget-object v0, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "experience_metadata_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public updateManifest(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 2

    .line 165
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "manifest"

    .line 166
    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "bundleUrl"

    .line 167
    invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p3, "safe_manifest"

    .line 168
    invoke-virtual {v0, p3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 170
    iget-object p2, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {p2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p2

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p2, p1, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 172
    sget-object p2, Lhost/exp/exponent/storage/ExponentSharedPreferences;->TAG:Ljava/lang/String;

    invoke-static {p2, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public updateSafeManifest(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 2

    .line 197
    :try_start_0
    iget-object v0, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 201
    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    :goto_0
    const-string v0, "safe_manifest"

    .line 203
    invoke-virtual {v1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 205
    iget-object p2, p0, Lhost/exp/exponent/storage/ExponentSharedPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {p2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p2

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 207
    sget-object p2, Lhost/exp/exponent/storage/ExponentSharedPreferences;->TAG:Ljava/lang/String;

    invoke-static {p2, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method

.method public updateSession(Lorg/json/JSONObject;)V
    .locals 1

    .line 142
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "expo_auth_session"

    invoke-virtual {p0, v0, p1}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
