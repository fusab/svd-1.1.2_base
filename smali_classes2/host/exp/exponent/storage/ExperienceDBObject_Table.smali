.class public final Lhost/exp/exponent/storage/ExperienceDBObject_Table;
.super Lcom/raizlabs/android/dbflow/structure/ModelAdapter;
.source "ExperienceDBObject_Table.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/raizlabs/android/dbflow/structure/ModelAdapter<",
        "Lhost/exp/exponent/storage/ExperienceDBObject;",
        ">;"
    }
.end annotation


# static fields
.field public static final ALL_COLUMN_PROPERTIES:[Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;

.field public static final bundleUrl:Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/Property<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final id:Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/Property<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final manifest:Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/Property<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final manifestUrl:Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/Property<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 26
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const-class v1, Lhost/exp/exponent/storage/ExperienceDBObject;

    const-string v2, "id"

    invoke-direct {v0, v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->id:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    .line 28
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const-class v1, Lhost/exp/exponent/storage/ExperienceDBObject;

    const-string v2, "manifestUrl"

    invoke-direct {v0, v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->manifestUrl:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    .line 30
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const-class v1, Lhost/exp/exponent/storage/ExperienceDBObject;

    const-string v2, "bundleUrl"

    invoke-direct {v0, v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->bundleUrl:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    .line 32
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const-class v1, Lhost/exp/exponent/storage/ExperienceDBObject;

    const-string v2, "manifest"

    invoke-direct {v0, v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->manifest:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/4 v0, 0x4

    .line 34
    new-array v0, v0, [Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;

    sget-object v1, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->id:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->manifestUrl:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->bundleUrl:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->manifest:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->ALL_COLUMN_PROPERTIES:[Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;

    return-void
.end method

.method public constructor <init>(Lcom/raizlabs/android/dbflow/config/DatabaseDefinition;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/raizlabs/android/dbflow/structure/ModelAdapter;-><init>(Lcom/raizlabs/android/dbflow/config/DatabaseDefinition;)V

    return-void
.end method


# virtual methods
.method public final bindToDeleteStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/storage/ExperienceDBObject;)V
    .locals 1

    .line 110
    iget-object p2, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->id:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-interface {p1, v0, p2}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    return-void
.end method

.method public bridge synthetic bindToDeleteStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p2, Lhost/exp/exponent/storage/ExperienceDBObject;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->bindToDeleteStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/storage/ExperienceDBObject;)V

    return-void
.end method

.method public final bindToInsertStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/storage/ExperienceDBObject;I)V
    .locals 2

    add-int/lit8 v0, p3, 0x1

    .line 93
    iget-object v1, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->id:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    add-int/lit8 v0, p3, 0x2

    .line 94
    iget-object v1, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->manifestUrl:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    add-int/lit8 v0, p3, 0x3

    .line 95
    iget-object v1, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->bundleUrl:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    add-int/lit8 p3, p3, 0x4

    .line 96
    iget-object p2, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->manifest:Ljava/lang/String;

    invoke-interface {p1, p3, p2}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    return-void
.end method

.method public bridge synthetic bindToInsertStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Ljava/lang/Object;I)V
    .locals 0

    .line 22
    check-cast p2, Lhost/exp/exponent/storage/ExperienceDBObject;

    invoke-virtual {p0, p1, p2, p3}, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->bindToInsertStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/storage/ExperienceDBObject;I)V

    return-void
.end method

.method public final bindToInsertValues(Landroid/content/ContentValues;Lhost/exp/exponent/storage/ExperienceDBObject;)V
    .locals 2

    .line 84
    iget-object v0, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->id:Ljava/lang/String;

    const-string v1, "`id`"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    iget-object v0, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->manifestUrl:Ljava/lang/String;

    const-string v1, "`manifestUrl`"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    iget-object v0, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->bundleUrl:Ljava/lang/String;

    const-string v1, "`bundleUrl`"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object p2, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->manifest:Ljava/lang/String;

    const-string v0, "`manifest`"

    invoke-virtual {p1, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic bindToInsertValues(Landroid/content/ContentValues;Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p2, Lhost/exp/exponent/storage/ExperienceDBObject;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->bindToInsertValues(Landroid/content/ContentValues;Lhost/exp/exponent/storage/ExperienceDBObject;)V

    return-void
.end method

.method public final bindToUpdateStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/storage/ExperienceDBObject;)V
    .locals 2

    .line 101
    iget-object v0, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->id:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-interface {p1, v1, v0}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    .line 102
    iget-object v0, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->manifestUrl:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-interface {p1, v1, v0}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    .line 103
    iget-object v0, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->bundleUrl:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-interface {p1, v1, v0}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    .line 104
    iget-object v0, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->manifest:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-interface {p1, v1, v0}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    .line 105
    iget-object p2, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->id:Ljava/lang/String;

    const/4 v0, 0x5

    invoke-interface {p1, v0, p2}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    return-void
.end method

.method public bridge synthetic bindToUpdateStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p2, Lhost/exp/exponent/storage/ExperienceDBObject;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->bindToUpdateStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/storage/ExperienceDBObject;)V

    return-void
.end method

.method public final exists(Lhost/exp/exponent/storage/ExperienceDBObject;Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;)Z
    .locals 3

    const/4 v0, 0x0

    .line 143
    new-array v1, v0, [Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;

    invoke-static {v1}, Lcom/raizlabs/android/dbflow/sql/language/SQLite;->selectCountOf([Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;)Lcom/raizlabs/android/dbflow/sql/language/Select;

    move-result-object v1

    const-class v2, Lhost/exp/exponent/storage/ExperienceDBObject;

    .line 144
    invoke-virtual {v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/Select;->from(Ljava/lang/Class;)Lcom/raizlabs/android/dbflow/sql/language/From;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/raizlabs/android/dbflow/sql/language/SQLOperator;

    .line 145
    invoke-virtual {p0, p1}, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->getPrimaryConditionClause(Lhost/exp/exponent/storage/ExperienceDBObject;)Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;

    move-result-object p1

    aput-object p1, v2, v0

    invoke-virtual {v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/From;->where([Lcom/raizlabs/android/dbflow/sql/language/SQLOperator;)Lcom/raizlabs/android/dbflow/sql/language/Where;

    move-result-object p1

    .line 146
    invoke-virtual {p1, p2}, Lcom/raizlabs/android/dbflow/sql/language/Where;->hasData(Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic exists(Ljava/lang/Object;Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;)Z
    .locals 0

    .line 22
    check-cast p1, Lhost/exp/exponent/storage/ExperienceDBObject;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->exists(Lhost/exp/exponent/storage/ExperienceDBObject;Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;)Z

    move-result p1

    return p1
.end method

.method public final getAllColumnProperties()[Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;
    .locals 1

    .line 79
    sget-object v0, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->ALL_COLUMN_PROPERTIES:[Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;

    return-object v0
.end method

.method public final getCompiledStatementQuery()Ljava/lang/String;
    .locals 1

    const-string v0, "INSERT INTO `ExperienceDBObject`(`id`,`manifestUrl`,`bundleUrl`,`manifest`) VALUES (?,?,?,?)"

    return-object v0
.end method

.method public final getCreationQuery()Ljava/lang/String;
    .locals 1

    const-string v0, "CREATE TABLE IF NOT EXISTS `ExperienceDBObject`(`id` TEXT, `manifestUrl` TEXT, `bundleUrl` TEXT, `manifest` TEXT, PRIMARY KEY(`id`))"

    return-object v0
.end method

.method public final getDeleteStatementQuery()Ljava/lang/String;
    .locals 1

    const-string v0, "DELETE FROM `ExperienceDBObject` WHERE `id`=?"

    return-object v0
.end method

.method public final getModelClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "Lhost/exp/exponent/storage/ExperienceDBObject;",
            ">;"
        }
    .end annotation

    .line 42
    const-class v0, Lhost/exp/exponent/storage/ExperienceDBObject;

    return-object v0
.end method

.method public final getPrimaryConditionClause(Lhost/exp/exponent/storage/ExperienceDBObject;)Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;
    .locals 2

    .line 151
    invoke-static {}, Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;->clause()Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;

    move-result-object v0

    .line 152
    sget-object v1, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->id:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    iget-object p1, p1, Lhost/exp/exponent/storage/ExperienceDBObject;->id:Ljava/lang/String;

    invoke-virtual {v1, p1}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;->eq(Ljava/lang/Object;)Lcom/raizlabs/android/dbflow/sql/language/Operator;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;->and(Lcom/raizlabs/android/dbflow/sql/language/SQLOperator;)Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;

    return-object v0
.end method

.method public bridge synthetic getPrimaryConditionClause(Ljava/lang/Object;)Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;
    .locals 0

    .line 22
    check-cast p1, Lhost/exp/exponent/storage/ExperienceDBObject;

    invoke-virtual {p0, p1}, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->getPrimaryConditionClause(Lhost/exp/exponent/storage/ExperienceDBObject;)Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;

    move-result-object p1

    return-object p1
.end method

.method public final getProperty(Ljava/lang/String;)Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .locals 4

    .line 57
    invoke-static {p1}, Lcom/raizlabs/android/dbflow/sql/QueryBuilder;->quoteIfNeeded(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 58
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x1

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v0, "`bundleUrl`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    goto :goto_1

    :sswitch_1
    const-string v0, "`id`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_1

    :sswitch_2
    const-string v0, "`manifestUrl`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_1

    :sswitch_3
    const-string v0, "`manifest`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x3

    goto :goto_1

    :cond_0
    :goto_0
    const/4 p1, -0x1

    :goto_1
    if-eqz p1, :cond_4

    if-eq p1, v3, :cond_3

    if-eq p1, v2, :cond_2

    if-ne p1, v1, :cond_1

    .line 69
    sget-object p1, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->manifest:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    return-object p1

    .line 72
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Invalid column name passed. Ensure you are calling the correct table\'s column"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 66
    :cond_2
    sget-object p1, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->bundleUrl:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    return-object p1

    .line 63
    :cond_3
    sget-object p1, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->manifestUrl:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    return-object p1

    .line 60
    :cond_4
    sget-object p1, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->id:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    return-object p1

    :sswitch_data_0
    .sparse-switch
        -0x7318fc4f -> :sswitch_3
        -0x89cb400 -> :sswitch_2
        0x2d3a45 -> :sswitch_1
        0x27da84d3 -> :sswitch_0
    .end sparse-switch
.end method

.method public final getTableName()Ljava/lang/String;
    .locals 1

    const-string v0, "`ExperienceDBObject`"

    return-object v0
.end method

.method public final getUpdateStatementQuery()Ljava/lang/String;
    .locals 1

    const-string v0, "UPDATE `ExperienceDBObject` SET `id`=?,`manifestUrl`=?,`bundleUrl`=?,`manifest`=? WHERE `id`=?"

    return-object v0
.end method

.method public final loadFromCursor(Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;Lhost/exp/exponent/storage/ExperienceDBObject;)V
    .locals 1

    const-string v0, "id"

    .line 135
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getStringOrDefault(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->id:Ljava/lang/String;

    const-string v0, "manifestUrl"

    .line 136
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getStringOrDefault(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->manifestUrl:Ljava/lang/String;

    const-string v0, "bundleUrl"

    .line 137
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getStringOrDefault(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->bundleUrl:Ljava/lang/String;

    const-string v0, "manifest"

    .line 138
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getStringOrDefault(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p2, Lhost/exp/exponent/storage/ExperienceDBObject;->manifest:Ljava/lang/String;

    return-void
.end method

.method public bridge synthetic loadFromCursor(Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p2, Lhost/exp/exponent/storage/ExperienceDBObject;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->loadFromCursor(Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;Lhost/exp/exponent/storage/ExperienceDBObject;)V

    return-void
.end method

.method public final newInstance()Lhost/exp/exponent/storage/ExperienceDBObject;
    .locals 1

    .line 52
    new-instance v0, Lhost/exp/exponent/storage/ExperienceDBObject;

    invoke-direct {v0}, Lhost/exp/exponent/storage/ExperienceDBObject;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->newInstance()Lhost/exp/exponent/storage/ExperienceDBObject;

    move-result-object v0

    return-object v0
.end method
