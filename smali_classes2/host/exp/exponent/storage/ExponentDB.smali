.class public Lhost/exp/exponent/storage/ExponentDB;
.super Ljava/lang/Object;
.source "ExponentDB.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhost/exp/exponent/storage/ExponentDB$ExperienceResultListener;
    }
.end annotation


# static fields
.field public static final NAME:Ljava/lang/String; = "ExponentKernel"

.field private static final TAG:Ljava/lang/String; = "ExponentDB"

.field public static final VERSION:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static experienceIdToExperience(Ljava/lang/String;Lhost/exp/exponent/storage/ExponentDB$ExperienceResultListener;)V
    .locals 4

    const/4 v0, 0x0

    .line 47
    new-array v1, v0, [Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;

    invoke-static {v1}, Lcom/raizlabs/android/dbflow/sql/language/SQLite;->select([Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;)Lcom/raizlabs/android/dbflow/sql/language/Select;

    move-result-object v1

    const-class v2, Lhost/exp/exponent/storage/ExperienceDBObject;

    .line 48
    invoke-virtual {v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/Select;->from(Ljava/lang/Class;)Lcom/raizlabs/android/dbflow/sql/language/From;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/raizlabs/android/dbflow/sql/language/SQLOperator;

    sget-object v3, Lhost/exp/exponent/storage/ExperienceDBObject_Table;->id:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    .line 49
    invoke-virtual {v3, p0}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;->is(Ljava/lang/Object;)Lcom/raizlabs/android/dbflow/sql/language/Operator;

    move-result-object p0

    aput-object p0, v2, v0

    invoke-virtual {v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/From;->where([Lcom/raizlabs/android/dbflow/sql/language/SQLOperator;)Lcom/raizlabs/android/dbflow/sql/language/Where;

    move-result-object p0

    .line 50
    invoke-virtual {p0}, Lcom/raizlabs/android/dbflow/sql/language/Where;->async()Lcom/raizlabs/android/dbflow/sql/queriable/AsyncQuery;

    move-result-object p0

    new-instance v0, Lhost/exp/exponent/storage/ExponentDB$1;

    invoke-direct {v0, p1}, Lhost/exp/exponent/storage/ExponentDB$1;-><init>(Lhost/exp/exponent/storage/ExponentDB$ExperienceResultListener;)V

    .line 51
    invoke-virtual {p0, v0}, Lcom/raizlabs/android/dbflow/sql/queriable/AsyncQuery;->queryResultCallback(Lcom/raizlabs/android/dbflow/structure/database/transaction/QueryTransaction$QueryResultCallback;)Lcom/raizlabs/android/dbflow/sql/queriable/AsyncQuery;

    move-result-object p0

    .line 60
    invoke-virtual {p0}, Lcom/raizlabs/android/dbflow/sql/queriable/AsyncQuery;->execute()V

    return-void
.end method

.method public static saveExperience(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 2

    .line 35
    :try_start_0
    new-instance v0, Lhost/exp/exponent/storage/ExperienceDBObject;

    invoke-direct {v0}, Lhost/exp/exponent/storage/ExperienceDBObject;-><init>()V

    const-string v1, "id"

    .line 36
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lhost/exp/exponent/storage/ExperienceDBObject;->id:Ljava/lang/String;

    .line 37
    iput-object p0, v0, Lhost/exp/exponent/storage/ExperienceDBObject;->manifestUrl:Ljava/lang/String;

    .line 38
    iput-object p2, v0, Lhost/exp/exponent/storage/ExperienceDBObject;->bundleUrl:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lhost/exp/exponent/storage/ExperienceDBObject;->manifest:Ljava/lang/String;

    .line 40
    const-class p0, Lhost/exp/exponent/storage/ExponentDB;

    invoke-static {p0}, Lcom/raizlabs/android/dbflow/config/FlowManager;->getDatabase(Ljava/lang/Class;)Lcom/raizlabs/android/dbflow/config/DatabaseDefinition;

    move-result-object p0

    invoke-virtual {p0}, Lcom/raizlabs/android/dbflow/config/DatabaseDefinition;->getTransactionManager()Lcom/raizlabs/android/dbflow/runtime/BaseTransactionManager;

    move-result-object p0

    invoke-virtual {p0}, Lcom/raizlabs/android/dbflow/runtime/BaseTransactionManager;->getSaveQueue()Lcom/raizlabs/android/dbflow/runtime/DBBatchSaveQueue;

    move-result-object p0

    invoke-virtual {p0, v0}, Lcom/raizlabs/android/dbflow/runtime/DBBatchSaveQueue;->add(Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 42
    sget-object p1, Lhost/exp/exponent/storage/ExponentDB;->TAG:Ljava/lang/String;

    invoke-virtual {p0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-static {p1, p0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method
