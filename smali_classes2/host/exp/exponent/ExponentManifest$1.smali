.class Lhost/exp/exponent/ExponentManifest$1;
.super Landroid/util/LruCache;
.source "ExponentManifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/ExponentManifest;-><init>(Landroid/content/Context;Lhost/exp/exponent/network/ExponentNetwork;Lhost/exp/exponent/kernel/Crypto;Lhost/exp/exponent/storage/ExponentSharedPreferences;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache<",
        "Ljava/lang/String;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/ExponentManifest;


# direct methods
.method constructor <init>(Lhost/exp/exponent/ExponentManifest;I)V
    .locals 0

    .line 162
    iput-object p1, p0, Lhost/exp/exponent/ExponentManifest$1;->this$0:Lhost/exp/exponent/ExponentManifest;

    invoke-direct {p0, p2}, Landroid/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 162
    check-cast p1, Ljava/lang/String;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/ExponentManifest$1;->sizeOf(Ljava/lang/String;Landroid/graphics/Bitmap;)I

    move-result p1

    return p1
.end method

.method protected sizeOf(Ljava/lang/String;Landroid/graphics/Bitmap;)I
    .locals 0

    .line 165
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result p1

    div-int/lit16 p1, p1, 0x400

    return p1
.end method
