.class public Lhost/exp/exponent/gcm/GcmRegistrationIntentService;
.super Lhost/exp/exponent/notifications/ExponentNotificationIntentService;
.source "GcmRegistrationIntentService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "GcmRegistrationIntentService"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 19
    sget-object v0, Lhost/exp/exponent/gcm/GcmRegistrationIntentService;->TAG:Ljava/lang/String;

    invoke-direct {p0, v0}, Lhost/exp/exponent/notifications/ExponentNotificationIntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getServerType()Ljava/lang/String;
    .locals 1

    const-string v0, "gcm"

    return-object v0
.end method

.method public getSharedPrefsKey()Ljava/lang/String;
    .locals 1

    const-string v0, "gcm_token"

    return-object v0
.end method

.method public getToken()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 24
    invoke-static {p0}, Lcom/google/android/gms/iid/InstanceID;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/iid/InstanceID;

    move-result-object v0

    .line 25
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v1

    invoke-virtual {v1}, Lhost/exp/expoview/Exponent;->getGCMSenderId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GCM"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/iid/InstanceID;->getToken(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
