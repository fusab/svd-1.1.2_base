.class public Lhost/exp/exponent/MainActivity;
.super Lhost/exp/exponent/experience/DetachActivity;
.source "MainActivity.java"

# interfaces
.implements Lcom/facebook/react/modules/core/PermissionAwareActivity;


# instance fields
.field private mPermissionListener:Lcom/facebook/react/modules/core/PermissionListener;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Lhost/exp/exponent/experience/DetachActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public developmentUrl()Ljava/lang/String;
    .locals 1

    const-string v0, "expc7499babb3a64ec8bd8919052329a05a://192.168.43.243:80"

    return-object v0
.end method

.method public expoPackages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/unimodules/core/interfaces/Package;",
            ">;"
        }
    .end annotation

    .line 77
    invoke-virtual {p0}, Lhost/exp/exponent/MainActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lhost/exp/exponent/MainApplication;

    invoke-virtual {v0}, Lhost/exp/exponent/MainApplication;->getExpoPackages()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public initialProps(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0

    return-object p1
.end method

.method public isDebug()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 1

    .line 62
    invoke-static {p1, p2, p3}, Lcom/calendarevents/CalendarEventsPackage;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 63
    iget-object v0, p0, Lhost/exp/exponent/MainActivity;->mPermissionListener:Lcom/facebook/react/modules/core/PermissionListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3}, Lcom/facebook/react/modules/core/PermissionListener;->onRequestPermissionsResult(I[Ljava/lang/String;[I)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 64
    iput-object p1, p0, Lhost/exp/exponent/MainActivity;->mPermissionListener:Lcom/facebook/react/modules/core/PermissionListener;

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .line 70
    invoke-super {p0}, Lhost/exp/exponent/experience/DetachActivity;->onResume()V

    const-string v0, "notification"

    .line 71
    invoke-virtual {p0, v0}, Lhost/exp/exponent/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 72
    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    return-void
.end method

.method public publishedUrl()Ljava/lang/String;
    .locals 1

    const-string v0, "exp://exp.host/@epavon-bemobile/svd-app"

    return-object v0
.end method

.method public reactPackages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/facebook/react/ReactPackage;",
            ">;"
        }
    .end annotation

    .line 40
    invoke-virtual {p0}, Lhost/exp/exponent/MainActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lhost/exp/exponent/MainApplication;

    invoke-virtual {v0}, Lhost/exp/exponent/MainApplication;->getPackages()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public requestPermissions([Ljava/lang/String;ILcom/facebook/react/modules/core/PermissionListener;)V
    .locals 0
    .annotation build Landroidx/annotation/RequiresApi;
        api = 0x17
    .end annotation

    .line 57
    iput-object p3, p0, Lhost/exp/exponent/MainActivity;->mPermissionListener:Lcom/facebook/react/modules/core/PermissionListener;

    .line 58
    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/MainActivity;->requestPermissions([Ljava/lang/String;I)V

    return-void
.end method
