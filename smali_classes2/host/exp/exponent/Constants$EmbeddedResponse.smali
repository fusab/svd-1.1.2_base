.class public Lhost/exp/exponent/Constants$EmbeddedResponse;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/exponent/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EmbeddedResponse"
.end annotation


# instance fields
.field public final mediaType:Ljava/lang/String;

.field public final responseFilePath:Ljava/lang/String;

.field public final url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    iput-object p1, p0, Lhost/exp/exponent/Constants$EmbeddedResponse;->url:Ljava/lang/String;

    .line 142
    iput-object p2, p0, Lhost/exp/exponent/Constants$EmbeddedResponse;->responseFilePath:Ljava/lang/String;

    .line 143
    iput-object p3, p0, Lhost/exp/exponent/Constants$EmbeddedResponse;->mediaType:Ljava/lang/String;

    return-void
.end method
