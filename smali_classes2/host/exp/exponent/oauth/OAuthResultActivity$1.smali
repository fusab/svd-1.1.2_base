.class Lhost/exp/exponent/oauth/OAuthResultActivity$1;
.super Ljava/lang/Object;
.source "OAuthResultActivity.java"

# interfaces
.implements Lnet/openid/appauth/AuthorizationService$TokenResponseCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/oauth/OAuthResultActivity;->handleIntent(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/oauth/OAuthResultActivity;

.field final synthetic val$authorizationResponse:Lnet/openid/appauth/AuthorizationResponse;


# direct methods
.method constructor <init>(Lhost/exp/exponent/oauth/OAuthResultActivity;Lnet/openid/appauth/AuthorizationResponse;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lhost/exp/exponent/oauth/OAuthResultActivity$1;->this$0:Lhost/exp/exponent/oauth/OAuthResultActivity;

    iput-object p2, p0, Lhost/exp/exponent/oauth/OAuthResultActivity$1;->val$authorizationResponse:Lnet/openid/appauth/AuthorizationResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTokenRequestCompleted(Lnet/openid/appauth/TokenResponse;Lnet/openid/appauth/AuthorizationException;)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 62
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object p2

    new-instance v1, Lhost/exp/exponent/oauth/OAuthResultActivity$OAuthResultEvent;

    iget-object v2, p0, Lhost/exp/exponent/oauth/OAuthResultActivity$1;->val$authorizationResponse:Lnet/openid/appauth/AuthorizationResponse;

    invoke-direct {v1, v2, p1, v0}, Lhost/exp/exponent/oauth/OAuthResultActivity$OAuthResultEvent;-><init>(Lnet/openid/appauth/AuthorizationResponse;Lnet/openid/appauth/TokenResponse;Lnet/openid/appauth/AuthorizationException;)V

    invoke-virtual {p2, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 64
    :cond_0
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object p1

    new-instance v1, Lhost/exp/exponent/oauth/OAuthResultActivity$OAuthResultEvent;

    invoke-direct {v1, v0, v0, p2}, Lhost/exp/exponent/oauth/OAuthResultActivity$OAuthResultEvent;-><init>(Lnet/openid/appauth/AuthorizationResponse;Lnet/openid/appauth/TokenResponse;Lnet/openid/appauth/AuthorizationException;)V

    invoke-virtual {p1, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
