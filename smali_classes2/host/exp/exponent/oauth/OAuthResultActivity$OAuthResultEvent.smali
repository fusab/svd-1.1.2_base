.class public Lhost/exp/exponent/oauth/OAuthResultActivity$OAuthResultEvent;
.super Ljava/lang/Object;
.source "OAuthResultActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/exponent/oauth/OAuthResultActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OAuthResultEvent"
.end annotation


# instance fields
.field public authorizationResponse:Lnet/openid/appauth/AuthorizationResponse;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public error:Lnet/openid/appauth/AuthorizationException;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public tokenResponse:Lnet/openid/appauth/TokenResponse;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lnet/openid/appauth/AuthorizationResponse;Lnet/openid/appauth/TokenResponse;Lnet/openid/appauth/AuthorizationException;)V
    .locals 0
    .param p1    # Lnet/openid/appauth/AuthorizationResponse;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lnet/openid/appauth/TokenResponse;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lnet/openid/appauth/AuthorizationException;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lhost/exp/exponent/oauth/OAuthResultActivity$OAuthResultEvent;->authorizationResponse:Lnet/openid/appauth/AuthorizationResponse;

    .line 30
    iput-object p2, p0, Lhost/exp/exponent/oauth/OAuthResultActivity$OAuthResultEvent;->tokenResponse:Lnet/openid/appauth/TokenResponse;

    .line 31
    iput-object p3, p0, Lhost/exp/exponent/oauth/OAuthResultActivity$OAuthResultEvent;->error:Lnet/openid/appauth/AuthorizationException;

    return-void
.end method
