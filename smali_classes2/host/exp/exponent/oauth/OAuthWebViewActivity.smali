.class public Lhost/exp/exponent/oauth/OAuthWebViewActivity;
.super Landroidx/appcompat/app/AppCompatActivity;
.source "OAuthWebViewActivity.java"


# static fields
.field public static final DATA_RESULT_URL:Ljava/lang/String; = "result_url"

.field public static final DATA_URL:Ljava/lang/String; = "url"

.field private static final OAUTH_CALLBACK_URL_PATTERN:Ljava/lang/String; = "https://oauth.host.exp.com"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Landroidx/appcompat/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lhost/exp/exponent/oauth/OAuthWebViewActivity;Ljava/lang/String;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lhost/exp/exponent/oauth/OAuthWebViewActivity;->processCallback(Ljava/lang/String;)V

    return-void
.end method

.method private processCallback(Ljava/lang/String;)V
    .locals 2

    .line 68
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "result_url"

    .line 69
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 p1, -0x1

    .line 70
    invoke-virtual {p0, p1, v0}, Lhost/exp/exponent/oauth/OAuthWebViewActivity;->setResult(ILandroid/content/Intent;)V

    .line 71
    invoke-virtual {p0}, Lhost/exp/exponent/oauth/OAuthWebViewActivity;->finish()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 27
    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    sget p1, Lhost/exp/expoview/R$layout;->oauth_webview_activity:I

    invoke-virtual {p0, p1}, Lhost/exp/exponent/oauth/OAuthWebViewActivity;->setContentView(I)V

    .line 30
    sget p1, Lhost/exp/expoview/R$id;->toolbar:I

    invoke-virtual {p0, p1}, Lhost/exp/exponent/oauth/OAuthWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    invoke-static {p1}, Lcom/facebook/infer/annotation/Assertions;->assertNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    .line 31
    invoke-virtual {p0, p1}, Lhost/exp/exponent/oauth/OAuthWebViewActivity;->setSupportActionBar(Landroidx/appcompat/widget/Toolbar;)V

    .line 33
    invoke-virtual {p0}, Lhost/exp/exponent/oauth/OAuthWebViewActivity;->getSupportActionBar()Landroidx/appcompat/app/ActionBar;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, ""

    .line 35
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    .line 36
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 37
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 40
    :cond_0
    sget p1, Lhost/exp/expoview/R$id;->webview:I

    invoke-virtual {p0, p1}, Lhost/exp/exponent/oauth/OAuthWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/webkit/WebView;

    invoke-static {p1}, Lcom/facebook/infer/annotation/Assertions;->assertNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/webkit/WebView;

    .line 41
    new-instance v0, Landroid/webkit/WebChromeClient;

    invoke-direct {v0}, Landroid/webkit/WebChromeClient;-><init>()V

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 42
    new-instance v0, Lhost/exp/exponent/oauth/OAuthWebViewActivity$1;

    invoke-direct {v0, p0}, Lhost/exp/exponent/oauth/OAuthWebViewActivity$1;-><init>(Lhost/exp/exponent/oauth/OAuthWebViewActivity;)V

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 53
    invoke-virtual {p0}, Lhost/exp/exponent/oauth/OAuthWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "url"

    .line 54
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 59
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .line 60
    invoke-virtual {p0, v0}, Lhost/exp/exponent/oauth/OAuthWebViewActivity;->setResult(I)V

    .line 61
    invoke-virtual {p0}, Lhost/exp/exponent/oauth/OAuthWebViewActivity;->finish()V

    .line 64
    :cond_0
    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method
