.class public Lhost/exp/exponent/oauth/OAuthResultActivity;
.super Landroid/app/Activity;
.source "OAuthResultActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhost/exp/exponent/oauth/OAuthResultActivity$OAuthResultEvent;
    }
.end annotation


# static fields
.field public static final EXTRA_REDIRECT_EXPERIENCE_URL:Ljava/lang/String; = "redirectExperienceUrl"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private handleIntent(Landroid/content/Intent;)V
    .locals 4

    .line 50
    new-instance v0, Lnet/openid/appauth/AuthorizationService;

    invoke-direct {v0, p0}, Lnet/openid/appauth/AuthorizationService;-><init>(Landroid/content/Context;)V

    .line 52
    invoke-static {p1}, Lnet/openid/appauth/AuthorizationResponse;->fromIntent(Landroid/content/Intent;)Lnet/openid/appauth/AuthorizationResponse;

    move-result-object v1

    .line 53
    invoke-static {p1}, Lnet/openid/appauth/AuthorizationException;->fromIntent(Landroid/content/Intent;)Lnet/openid/appauth/AuthorizationException;

    move-result-object v2

    if-eqz v1, :cond_0

    .line 57
    invoke-virtual {v1}, Lnet/openid/appauth/AuthorizationResponse;->createTokenExchangeRequest()Lnet/openid/appauth/TokenRequest;

    move-result-object v2

    new-instance v3, Lhost/exp/exponent/oauth/OAuthResultActivity$1;

    invoke-direct {v3, p0, v1}, Lhost/exp/exponent/oauth/OAuthResultActivity$1;-><init>(Lhost/exp/exponent/oauth/OAuthResultActivity;Lnet/openid/appauth/AuthorizationResponse;)V

    .line 56
    invoke-virtual {v0, v2, v3}, Lnet/openid/appauth/AuthorizationService;->performTokenRequest(Lnet/openid/appauth/TokenRequest;Lnet/openid/appauth/AuthorizationService$TokenResponseCallback;)V

    goto :goto_0

    .line 69
    :cond_0
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lhost/exp/exponent/oauth/OAuthResultActivity$OAuthResultEvent;

    const/4 v3, 0x0

    invoke-direct {v1, v3, v3, v2}, Lhost/exp/exponent/oauth/OAuthResultActivity$OAuthResultEvent;-><init>(Lnet/openid/appauth/AuthorizationResponse;Lnet/openid/appauth/TokenResponse;Lnet/openid/appauth/AuthorizationException;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    :goto_0
    const-string v0, "redirectExperienceUrl"

    .line 72
    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 73
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/high16 p1, 0x10000000

    .line 75
    invoke-virtual {v1, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 76
    invoke-virtual {p0, v1}, Lhost/exp/exponent/oauth/OAuthResultActivity;->startActivity(Landroid/content/Intent;)V

    .line 79
    :cond_1
    invoke-virtual {p0}, Lhost/exp/exponent/oauth/OAuthResultActivity;->finish()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 37
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0}, Lhost/exp/exponent/oauth/OAuthResultActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-direct {p0, p1}, Lhost/exp/exponent/oauth/OAuthResultActivity;->handleIntent(Landroid/content/Intent;)V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .line 44
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 46
    invoke-direct {p0, p1}, Lhost/exp/exponent/oauth/OAuthResultActivity;->handleIntent(Landroid/content/Intent;)V

    return-void
.end method
