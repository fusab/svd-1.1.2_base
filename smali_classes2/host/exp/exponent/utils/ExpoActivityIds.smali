.class public Lhost/exp/exponent/utils/ExpoActivityIds;
.super Ljava/lang/Object;
.source "ExpoActivityIds.java"


# static fields
.field private static currentAppActivityId:I = 0x0

.field private static currentHeadlessActivityId:I = -0x2


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getNextAppActivityId()I
    .locals 2

    .line 14
    sget v0, Lhost/exp/exponent/utils/ExpoActivityIds;->currentAppActivityId:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lhost/exp/exponent/utils/ExpoActivityIds;->currentAppActivityId:I

    return v0
.end method

.method public static getNextHeadlessActivityId()I
    .locals 2

    .line 18
    sget v0, Lhost/exp/exponent/utils/ExpoActivityIds;->currentHeadlessActivityId:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lhost/exp/exponent/utils/ExpoActivityIds;->currentHeadlessActivityId:I

    return v0
.end method
