.class public Lhost/exp/exponent/utils/PermissionsHelper;
.super Ljava/lang/Object;
.source "PermissionsHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;
    }
.end annotation


# static fields
.field private static final EXPONENT_PERMISSIONS_REQUEST:I = 0xd


# instance fields
.field private mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

.field private mExperienceName:Ljava/lang/String;

.field private mExperiencePermissionsGranted:Z

.field mExpoKernelServiceRegistry:Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mPermissionsAskedCount:I

.field private mPermissionsListener:Lhost/exp/expoview/Exponent$PermissionsListener;

.field private mPermissionsToRequestGlobally:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPermissionsToRequestPerExperience:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lhost/exp/exponent/kernel/ExperienceId;)V
    .locals 2

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 31
    iput-boolean v0, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mExperiencePermissionsGranted:Z

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsToRequestGlobally:Ljava/util/List;

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsToRequestPerExperience:Ljava/util/List;

    const/4 v0, 0x0

    .line 34
    iput v0, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsAskedCount:I

    .line 37
    invoke-static {}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->getInstance()Lhost/exp/exponent/di/NativeModuleDepsProvider;

    move-result-object v0

    const-class v1, Lhost/exp/exponent/utils/PermissionsHelper;

    invoke-virtual {v0, v1, p0}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->inject(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 38
    iput-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    return-void
.end method

.method static synthetic access$000(Lhost/exp/exponent/utils/PermissionsHelper;)I
    .locals 0

    .line 21
    iget p0, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsAskedCount:I

    return p0
.end method

.method static synthetic access$002(Lhost/exp/exponent/utils/PermissionsHelper;I)I
    .locals 0

    .line 21
    iput p1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsAskedCount:I

    return p1
.end method

.method static synthetic access$100(Lhost/exp/exponent/utils/PermissionsHelper;)Lhost/exp/exponent/kernel/ExperienceId;
    .locals 0

    .line 21
    iget-object p0, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    return-object p0
.end method

.method static synthetic access$200(Lhost/exp/exponent/utils/PermissionsHelper;)Z
    .locals 0

    .line 21
    iget-boolean p0, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mExperiencePermissionsGranted:Z

    return p0
.end method

.method static synthetic access$202(Lhost/exp/exponent/utils/PermissionsHelper;Z)Z
    .locals 0

    .line 21
    iput-boolean p1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mExperiencePermissionsGranted:Z

    return p1
.end method

.method static synthetic access$300(Lhost/exp/exponent/utils/PermissionsHelper;)Ljava/util/List;
    .locals 0

    .line 21
    iget-object p0, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsToRequestPerExperience:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$400(Lhost/exp/exponent/utils/PermissionsHelper;Ljava/lang/String;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lhost/exp/exponent/utils/PermissionsHelper;->showPermissionsDialogForExperience(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lhost/exp/exponent/utils/PermissionsHelper;)Ljava/util/List;
    .locals 0

    .line 21
    iget-object p0, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsToRequestGlobally:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$600(Lhost/exp/exponent/utils/PermissionsHelper;)Lhost/exp/expoview/Exponent$PermissionsListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsListener:Lhost/exp/expoview/Exponent$PermissionsListener;

    return-object p0
.end method

.method private permissionToResId(Ljava/lang/String;)I
    .locals 2

    .line 152
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, -0x1

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v0, "android.permission.READ_CONTACTS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_1

    :sswitch_1
    const-string v0, "android.permission.RECORD_AUDIO"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x4

    goto :goto_1

    :sswitch_2
    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x3

    goto :goto_1

    :sswitch_3
    const-string v0, "android.settings.action.MANAGE_WRITE_SETTINGS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x5

    goto :goto_1

    :sswitch_4
    const-string v0, "android.permission.WRITE_CALENDAR"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x7

    goto :goto_1

    :sswitch_5
    const-string v0, "android.permission.CAMERA"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_1

    :sswitch_6
    const-string v0, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 p1, 0x9

    goto :goto_1

    :sswitch_7
    const-string v0, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    goto :goto_1

    :sswitch_8
    const-string v0, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 p1, 0x8

    goto :goto_1

    :sswitch_9
    const-string v0, "android.permission.READ_CALENDAR"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x6

    goto :goto_1

    :cond_0
    :goto_0
    const/4 p1, -0x1

    :goto_1
    packed-switch p1, :pswitch_data_0

    return v1

    .line 172
    :pswitch_0
    sget p1, Lhost/exp/expoview/R$string;->perm_coarse_location:I

    return p1

    .line 170
    :pswitch_1
    sget p1, Lhost/exp/expoview/R$string;->perm_fine_location:I

    return p1

    .line 168
    :pswitch_2
    sget p1, Lhost/exp/expoview/R$string;->perm_calendar_write:I

    return p1

    .line 166
    :pswitch_3
    sget p1, Lhost/exp/expoview/R$string;->perm_calendar_read:I

    return p1

    .line 164
    :pswitch_4
    sget p1, Lhost/exp/expoview/R$string;->perm_system_brightness:I

    return p1

    .line 162
    :pswitch_5
    sget p1, Lhost/exp/expoview/R$string;->perm_audio_recording:I

    return p1

    .line 160
    :pswitch_6
    sget p1, Lhost/exp/expoview/R$string;->perm_camera_roll_write:I

    return p1

    .line 158
    :pswitch_7
    sget p1, Lhost/exp/expoview/R$string;->perm_camera_roll_read:I

    return p1

    .line 156
    :pswitch_8
    sget p1, Lhost/exp/expoview/R$string;->perm_contacts:I

    return p1

    .line 154
    :pswitch_9
    sget p1, Lhost/exp/expoview/R$string;->perm_camera:I

    return p1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x72f13779 -> :sswitch_9
        -0x70918bc1 -> :sswitch_8
        -0x1833add0 -> :sswitch_7
        -0x3c1ac56 -> :sswitch_6
        0x1b9efa65 -> :sswitch_5
        0x23fb06fe -> :sswitch_4
        0x3bf63eb1 -> :sswitch_3
        0x516a29a7 -> :sswitch_2
        0x6d24f988 -> :sswitch_1
        0x75dd2d9c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private showPermissionsDialogForExperience(Ljava/lang/String;)V
    .locals 7

    .line 138
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    invoke-virtual {v0}, Lhost/exp/expoview/Exponent;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v0

    .line 140
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 141
    new-instance v2, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;

    invoke-direct {v2, p0, p1}, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;-><init>(Lhost/exp/exponent/utils/PermissionsHelper;Ljava/lang/String;)V

    .line 142
    sget v3, Lhost/exp/expoview/R$string;->experience_needs_permissions:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mExperienceName:Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    .line 145
    invoke-direct {p0, p1}, Lhost/exp/exponent/utils/PermissionsHelper;->permissionToResId(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v5, 0x1

    aput-object p1, v4, v5

    .line 142
    invoke-virtual {v0, v3, v4}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    sget v0, Lhost/exp/expoview/R$string;->allow_experience_permissions:I

    .line 146
    invoke-virtual {p1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    sget v0, Lhost/exp/expoview/R$string;->deny_experience_permissions:I

    .line 147
    invoke-virtual {p1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method


# virtual methods
.method public getPermissions(Ljava/lang/String;)Z
    .locals 3

    .line 42
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    invoke-virtual {v0}, Lhost/exp/expoview/Exponent;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v0

    .line 43
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_0

    invoke-static {v0, p1}, Landroidx/core/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mExpoKernelServiceRegistry:Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;

    .line 44
    invoke-virtual {v0}, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->getPermissionsKernelService()Lhost/exp/exponent/kernel/services/PermissionsKernelService;

    move-result-object v0

    iget-object v1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    invoke-virtual {v0, p1, v1}, Lhost/exp/exponent/kernel/services/PermissionsKernelService;->hasGrantedPermissions(Ljava/lang/String;Lhost/exp/exponent/kernel/ExperienceId;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 4

    const/16 v0, 0xd

    if-ne p1, v0, :cond_6

    .line 103
    iget-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsListener:Lhost/exp/expoview/Exponent$PermissionsListener;

    if-nez p1, :cond_0

    return-void

    .line 110
    :cond_0
    array-length p1, p3

    const/4 v0, 0x0

    if-lez p1, :cond_4

    const/4 p1, 0x0

    .line 112
    :goto_0
    array-length v1, p3

    if-ge p1, v1, :cond_3

    .line 113
    aget v1, p3, p1

    if-eqz v1, :cond_1

    goto :goto_1

    .line 117
    :cond_1
    iget-object v1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    if-eqz v1, :cond_2

    .line 118
    iget-object v1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mExpoKernelServiceRegistry:Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;

    invoke-virtual {v1}, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->getPermissionsKernelService()Lhost/exp/exponent/kernel/services/PermissionsKernelService;

    move-result-object v1

    aget-object v2, p2, p1

    iget-object v3, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    invoke-virtual {v1, v2, v3}, Lhost/exp/exponent/kernel/services/PermissionsKernelService;->grantPermissions(Ljava/lang/String;Lhost/exp/exponent/kernel/ExperienceId;)V

    :cond_2
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    :cond_4
    :goto_1
    if-eqz v0, :cond_5

    .line 123
    iget-boolean p1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mExperiencePermissionsGranted:Z

    if-eqz p1, :cond_5

    .line 124
    iget-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsListener:Lhost/exp/expoview/Exponent$PermissionsListener;

    invoke-interface {p1}, Lhost/exp/expoview/Exponent$PermissionsListener;->permissionsGranted()V

    goto :goto_2

    .line 126
    :cond_5
    iget-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsListener:Lhost/exp/expoview/Exponent$PermissionsListener;

    invoke-interface {p1}, Lhost/exp/expoview/Exponent$PermissionsListener;->permissionsDenied()V

    :goto_2
    const/4 p1, 0x0

    .line 128
    iput-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsListener:Lhost/exp/expoview/Exponent$PermissionsListener;

    goto :goto_3

    .line 130
    :cond_6
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-le v0, v1, :cond_7

    .line 131
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    invoke-virtual {v0}, Lhost/exp/expoview/Exponent;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v0

    .line 132
    invoke-virtual {v0, p1, p2, p3}, Landroid/app/Activity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    :cond_7
    :goto_3
    return-void
.end method

.method public requestExperiencePermissions(Lhost/exp/expoview/Exponent$PermissionsListener;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 95
    iput-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsListener:Lhost/exp/expoview/Exponent$PermissionsListener;

    .line 96
    iput-object p3, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mExperienceName:Ljava/lang/String;

    .line 97
    array-length p1, p2

    iput p1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsAskedCount:I

    .line 98
    iget p1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsAskedCount:I

    add-int/lit8 p1, p1, -0x1

    aget-object p1, p2, p1

    invoke-direct {p0, p1}, Lhost/exp/exponent/utils/PermissionsHelper;->showPermissionsDialogForExperience(Ljava/lang/String;)V

    return-void
.end method

.method public requestPermissions(Lhost/exp/expoview/Exponent$PermissionsListener;[Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9

    .line 49
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    invoke-virtual {v0}, Lhost/exp/expoview/Exponent;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 55
    :cond_0
    iput-object p3, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mExperienceName:Ljava/lang/String;

    .line 56
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 57
    array-length v2, p2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    :goto_0
    const/16 v6, 0x17

    if-ge v4, v2, :cond_4

    aget-object v7, p2, v4

    .line 58
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v8, v6, :cond_2

    .line 59
    invoke-static {v0, v7}, Landroidx/core/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_2

    .line 61
    iget-object v5, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsToRequestGlobally:Ljava/util/List;

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    invoke-virtual {v0, v7}, Landroid/app/Activity;->shouldShowRequestPermissionRationale(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 64
    invoke-interface {p3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    const/4 v5, 0x0

    goto :goto_2

    .line 66
    :cond_2
    iget-object v6, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mExpoKernelServiceRegistry:Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;

    .line 67
    invoke-virtual {v6}, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->getPermissionsKernelService()Lhost/exp/exponent/kernel/services/PermissionsKernelService;

    move-result-object v6

    iget-object v8, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    invoke-virtual {v6, v7, v8}, Lhost/exp/exponent/kernel/services/PermissionsKernelService;->hasGrantedPermissions(Ljava/lang/String;Lhost/exp/exponent/kernel/ExperienceId;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 69
    iget-object v5, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsToRequestPerExperience:Ljava/util/List;

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_4
    if-eqz v5, :cond_5

    .line 74
    invoke-interface {p1}, Lhost/exp/expoview/Exponent$PermissionsListener;->permissionsGranted()V

    return v3

    .line 80
    :cond_5
    iget-object p2, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsToRequestPerExperience:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    iput p2, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsAskedCount:I

    .line 81
    iput-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsListener:Lhost/exp/expoview/Exponent$PermissionsListener;

    .line 83
    iget-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsToRequestPerExperience:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_6

    .line 84
    iget-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsToRequestPerExperience:Ljava/util/List;

    iget p2, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsAskedCount:I

    sub-int/2addr p2, v3

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-direct {p0, p1}, Lhost/exp/exponent/utils/PermissionsHelper;->showPermissionsDialogForExperience(Ljava/lang/String;)V

    goto :goto_3

    .line 85
    :cond_6
    iget-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsToRequestGlobally:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_7

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt p1, v6, :cond_7

    .line 86
    iget-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper;->mPermissionsToRequestGlobally:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    new-array p2, p2, [Ljava/lang/String;

    invoke-interface {p1, p2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    const/16 p2, 0xd

    invoke-virtual {v0, p1, p2}, Landroid/app/Activity;->requestPermissions([Ljava/lang/String;I)V

    :cond_7
    :goto_3
    return v3
.end method
