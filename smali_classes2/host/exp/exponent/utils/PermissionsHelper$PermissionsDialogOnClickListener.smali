.class Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;
.super Ljava/lang/Object;
.source "PermissionsHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/exponent/utils/PermissionsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PermissionsDialogOnClickListener"
.end annotation


# instance fields
.field private mPermission:Ljava/lang/String;

.field final synthetic this$0:Lhost/exp/exponent/utils/PermissionsHelper;


# direct methods
.method constructor <init>(Lhost/exp/exponent/utils/PermissionsHelper;Ljava/lang/String;)V
    .locals 0

    .line 182
    iput-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->this$0:Lhost/exp/exponent/utils/PermissionsHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    iput-object p2, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->mPermission:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 188
    iget-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->this$0:Lhost/exp/exponent/utils/PermissionsHelper;

    invoke-static {p1}, Lhost/exp/exponent/utils/PermissionsHelper;->access$000(Lhost/exp/exponent/utils/PermissionsHelper;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Lhost/exp/exponent/utils/PermissionsHelper;->access$002(Lhost/exp/exponent/utils/PermissionsHelper;I)I

    const/4 p1, -0x2

    if-eq p2, p1, :cond_1

    const/4 p1, -0x1

    if-eq p2, p1, :cond_0

    goto :goto_0

    .line 191
    :cond_0
    iget-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->this$0:Lhost/exp/exponent/utils/PermissionsHelper;

    iget-object p1, p1, Lhost/exp/exponent/utils/PermissionsHelper;->mExpoKernelServiceRegistry:Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;

    invoke-virtual {p1}, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->getPermissionsKernelService()Lhost/exp/exponent/kernel/services/PermissionsKernelService;

    move-result-object p1

    iget-object p2, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->mPermission:Ljava/lang/String;

    iget-object v0, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->this$0:Lhost/exp/exponent/utils/PermissionsHelper;

    invoke-static {v0}, Lhost/exp/exponent/utils/PermissionsHelper;->access$100(Lhost/exp/exponent/utils/PermissionsHelper;)Lhost/exp/exponent/kernel/ExperienceId;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lhost/exp/exponent/kernel/services/PermissionsKernelService;->grantPermissions(Ljava/lang/String;Lhost/exp/exponent/kernel/ExperienceId;)V

    goto :goto_0

    .line 195
    :cond_1
    iget-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->this$0:Lhost/exp/exponent/utils/PermissionsHelper;

    iget-object p1, p1, Lhost/exp/exponent/utils/PermissionsHelper;->mExpoKernelServiceRegistry:Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;

    invoke-virtual {p1}, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->getPermissionsKernelService()Lhost/exp/exponent/kernel/services/PermissionsKernelService;

    move-result-object p1

    iget-object p2, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->mPermission:Ljava/lang/String;

    iget-object v0, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->this$0:Lhost/exp/exponent/utils/PermissionsHelper;

    invoke-static {v0}, Lhost/exp/exponent/utils/PermissionsHelper;->access$100(Lhost/exp/exponent/utils/PermissionsHelper;)Lhost/exp/exponent/kernel/ExperienceId;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lhost/exp/exponent/kernel/services/PermissionsKernelService;->revokePermissions(Ljava/lang/String;Lhost/exp/exponent/kernel/ExperienceId;)V

    .line 196
    iget-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->this$0:Lhost/exp/exponent/utils/PermissionsHelper;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lhost/exp/exponent/utils/PermissionsHelper;->access$202(Lhost/exp/exponent/utils/PermissionsHelper;Z)Z

    .line 200
    :goto_0
    iget-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->this$0:Lhost/exp/exponent/utils/PermissionsHelper;

    invoke-static {p1}, Lhost/exp/exponent/utils/PermissionsHelper;->access$000(Lhost/exp/exponent/utils/PermissionsHelper;)I

    move-result p1

    if-lez p1, :cond_2

    .line 201
    iget-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->this$0:Lhost/exp/exponent/utils/PermissionsHelper;

    invoke-static {p1}, Lhost/exp/exponent/utils/PermissionsHelper;->access$300(Lhost/exp/exponent/utils/PermissionsHelper;)Ljava/util/List;

    move-result-object p2

    iget-object v0, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->this$0:Lhost/exp/exponent/utils/PermissionsHelper;

    invoke-static {v0}, Lhost/exp/exponent/utils/PermissionsHelper;->access$000(Lhost/exp/exponent/utils/PermissionsHelper;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-static {p1, p2}, Lhost/exp/exponent/utils/PermissionsHelper;->access$400(Lhost/exp/exponent/utils/PermissionsHelper;Ljava/lang/String;)V

    goto :goto_1

    .line 203
    :cond_2
    iget-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->this$0:Lhost/exp/exponent/utils/PermissionsHelper;

    invoke-static {p1}, Lhost/exp/exponent/utils/PermissionsHelper;->access$500(Lhost/exp/exponent/utils/PermissionsHelper;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_3

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x17

    if-lt p1, p2, :cond_3

    .line 204
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object p1

    invoke-virtual {p1}, Lhost/exp/expoview/Exponent;->getCurrentActivity()Landroid/app/Activity;

    move-result-object p1

    iget-object p2, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->this$0:Lhost/exp/exponent/utils/PermissionsHelper;

    .line 205
    invoke-static {p2}, Lhost/exp/exponent/utils/PermissionsHelper;->access$500(Lhost/exp/exponent/utils/PermissionsHelper;)Ljava/util/List;

    move-result-object p2

    iget-object v0, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->this$0:Lhost/exp/exponent/utils/PermissionsHelper;

    invoke-static {v0}, Lhost/exp/exponent/utils/PermissionsHelper;->access$500(Lhost/exp/exponent/utils/PermissionsHelper;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Ljava/lang/String;

    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, Landroid/app/Activity;->requestPermissions([Ljava/lang/String;I)V

    goto :goto_1

    .line 207
    :cond_3
    iget-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->this$0:Lhost/exp/exponent/utils/PermissionsHelper;

    invoke-static {p1}, Lhost/exp/exponent/utils/PermissionsHelper;->access$200(Lhost/exp/exponent/utils/PermissionsHelper;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 208
    iget-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->this$0:Lhost/exp/exponent/utils/PermissionsHelper;

    invoke-static {p1}, Lhost/exp/exponent/utils/PermissionsHelper;->access$600(Lhost/exp/exponent/utils/PermissionsHelper;)Lhost/exp/expoview/Exponent$PermissionsListener;

    move-result-object p1

    invoke-interface {p1}, Lhost/exp/expoview/Exponent$PermissionsListener;->permissionsGranted()V

    goto :goto_1

    .line 210
    :cond_4
    iget-object p1, p0, Lhost/exp/exponent/utils/PermissionsHelper$PermissionsDialogOnClickListener;->this$0:Lhost/exp/exponent/utils/PermissionsHelper;

    invoke-static {p1}, Lhost/exp/exponent/utils/PermissionsHelper;->access$600(Lhost/exp/exponent/utils/PermissionsHelper;)Lhost/exp/expoview/Exponent$PermissionsListener;

    move-result-object p1

    invoke-interface {p1}, Lhost/exp/expoview/Exponent$PermissionsListener;->permissionsDenied()V

    :goto_1
    return-void
.end method
