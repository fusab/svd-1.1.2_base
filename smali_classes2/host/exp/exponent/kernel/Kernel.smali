.class public Lhost/exp/exponent/kernel/Kernel;
.super Lhost/exp/exponent/kernel/KernelInterface;
.source "Kernel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;,
        Lhost/exp/exponent/kernel/Kernel$KernelStartedRunningEvent;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Kernel"

.field private static final mManifestUrlToOptions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;",
            ">;"
        }
    .end annotation
.end field

.field private static sInstance:Lhost/exp/exponent/kernel/Kernel;

.field private static sManifestUrlToExperienceActivityTask:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActivityContext:Landroid/app/Activity;

.field mApplicationContext:Landroid/app/Application;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mContext:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mExponentManifest:Lhost/exp/exponent/ExponentManifest;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mHasError:Z

.field private mIsRunning:Z

.field private mIsStarted:Z

.field private mOptimisticActivity:Lhost/exp/exponent/experience/ExperienceActivity;

.field private mOptimisticTaskId:Ljava/lang/Integer;

.field private mReactInstanceManager:Lcom/facebook/react/ReactInstanceManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 117
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lhost/exp/exponent/kernel/Kernel;->sManifestUrlToExperienceActivityTask:Ljava/util/Map;

    .line 141
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lhost/exp/exponent/kernel/Kernel;->mManifestUrlToOptions:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 146
    invoke-direct {p0}, Lhost/exp/exponent/kernel/KernelInterface;-><init>()V

    const/4 v0, 0x0

    .line 131
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/Kernel;->mIsStarted:Z

    .line 132
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/Kernel;->mIsRunning:Z

    .line 133
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/Kernel;->mHasError:Z

    .line 147
    invoke-static {}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->getInstance()Lhost/exp/exponent/di/NativeModuleDepsProvider;

    move-result-object v0

    const-class v1, Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0, v1, p0}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->inject(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 149
    sput-object p0, Lhost/exp/exponent/kernel/Kernel;->sInstance:Lhost/exp/exponent/kernel/Kernel;

    .line 151
    invoke-direct {p0}, Lhost/exp/exponent/kernel/Kernel;->updateKernelRNOkHttp()V

    return-void
.end method

.method static synthetic access$000(Lhost/exp/exponent/kernel/Kernel;)Landroid/app/Activity;
    .locals 0

    .line 83
    iget-object p0, p0, Lhost/exp/exponent/kernel/Kernel;->mActivityContext:Landroid/app/Activity;

    return-object p0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .line 83
    sget-object v0, Lhost/exp/exponent/kernel/Kernel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lhost/exp/exponent/kernel/Kernel;)Ljava/lang/String;
    .locals 0

    .line 83
    invoke-direct {p0}, Lhost/exp/exponent/kernel/Kernel;->getKernelRevisionId()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lhost/exp/exponent/kernel/Kernel;)Lcom/facebook/react/ReactInstanceManager;
    .locals 0

    .line 83
    iget-object p0, p0, Lhost/exp/exponent/kernel/Kernel;->mReactInstanceManager:Lcom/facebook/react/ReactInstanceManager;

    return-object p0
.end method

.method static synthetic access$302(Lhost/exp/exponent/kernel/Kernel;Lcom/facebook/react/ReactInstanceManager;)Lcom/facebook/react/ReactInstanceManager;
    .locals 0

    .line 83
    iput-object p1, p0, Lhost/exp/exponent/kernel/Kernel;->mReactInstanceManager:Lcom/facebook/react/ReactInstanceManager;

    return-object p1
.end method

.method static synthetic access$402(Lhost/exp/exponent/kernel/Kernel;Z)Z
    .locals 0

    .line 83
    iput-boolean p1, p0, Lhost/exp/exponent/kernel/Kernel;->mIsRunning:Z

    return p1
.end method

.method static synthetic access$500(Lhost/exp/exponent/kernel/Kernel;Ljava/lang/String;Lorg/json/JSONObject;Landroid/app/ActivityManager$AppTask;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 83
    invoke-direct {p0, p1, p2, p3}, Lhost/exp/exponent/kernel/Kernel;->openManifestUrlStep2(Ljava/lang/String;Lorg/json/JSONObject;Landroid/app/ActivityManager$AppTask;)V

    return-void
.end method

.method static synthetic access$600()Ljava/util/Map;
    .locals 1

    .line 83
    sget-object v0, Lhost/exp/exponent/kernel/Kernel;->sManifestUrlToExperienceActivityTask:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$700(Lhost/exp/exponent/kernel/Kernel;)Lhost/exp/exponent/experience/ExperienceActivity;
    .locals 0

    .line 83
    iget-object p0, p0, Lhost/exp/exponent/kernel/Kernel;->mOptimisticActivity:Lhost/exp/exponent/experience/ExperienceActivity;

    return-object p0
.end method

.method static synthetic access$702(Lhost/exp/exponent/kernel/Kernel;Lhost/exp/exponent/experience/ExperienceActivity;)Lhost/exp/exponent/experience/ExperienceActivity;
    .locals 0

    .line 83
    iput-object p1, p0, Lhost/exp/exponent/kernel/Kernel;->mOptimisticActivity:Lhost/exp/exponent/experience/ExperienceActivity;

    return-object p1
.end method

.method static synthetic access$800(Lhost/exp/exponent/kernel/Kernel;)Ljava/lang/Integer;
    .locals 0

    .line 83
    iget-object p0, p0, Lhost/exp/exponent/kernel/Kernel;->mOptimisticTaskId:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$802(Lhost/exp/exponent/kernel/Kernel;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .line 83
    iput-object p1, p0, Lhost/exp/exponent/kernel/Kernel;->mOptimisticTaskId:Ljava/lang/Integer;

    return-object p1
.end method

.method public static addIntentDocumentFlags(Landroid/content/Intent;)V
    .locals 1

    const/high16 v0, 0x4000000

    .line 258
    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v0, 0x80000

    .line 260
    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v0, 0x8000000

    .line 261
    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    return-void
.end method

.method private experienceActivityTaskForTaskId(I)Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;
    .locals 3

    .line 121
    sget-object v0, Lhost/exp/exponent/kernel/Kernel;->sManifestUrlToExperienceActivityTask:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;

    .line 122
    iget v2, v1, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->taskId:I

    if-ne v2, p1, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private getBundleUrl()Ljava/lang/String;
    .locals 2

    .line 335
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentManifest:Lhost/exp/exponent/ExponentManifest;

    const-string v1, "bundleUrl"

    invoke-virtual {v0, v1}, Lhost/exp/exponent/ExponentManifest;->getKernelManifestField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getBundleUrlForActivityId(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    const/4 p1, -0x1

    if-ne p0, p1, :cond_0

    .line 771
    sget-object p0, Lhost/exp/exponent/kernel/Kernel;->sInstance:Lhost/exp/exponent/kernel/Kernel;

    invoke-direct {p0}, Lhost/exp/exponent/kernel/Kernel;->getBundleUrl()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 774
    :cond_0
    invoke-static {p0}, Lhost/exp/exponent/headless/HeadlessAppLoader;->hasBundleUrlForActivityId(I)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 775
    invoke-static {p0}, Lhost/exp/exponent/headless/HeadlessAppLoader;->getBundleUrlForActivityId(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 778
    :cond_1
    sget-object p1, Lhost/exp/exponent/kernel/Kernel;->sManifestUrlToExperienceActivityTask:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;

    .line 779
    iget p3, p2, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->activityId:I

    if-ne p3, p0, :cond_2

    .line 780
    iget-object p0, p2, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->bundleUrl:Ljava/lang/String;

    return-object p0

    :cond_3
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getBundleUrlForActivityId(ILjava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    const/4 p1, -0x1

    if-ne p0, p1, :cond_0

    .line 792
    sget-object p0, Lhost/exp/exponent/kernel/Kernel;->sInstance:Lhost/exp/exponent/kernel/Kernel;

    invoke-direct {p0}, Lhost/exp/exponent/kernel/Kernel;->getBundleUrl()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 795
    :cond_0
    sget-object p1, Lhost/exp/exponent/kernel/Kernel;->sManifestUrlToExperienceActivityTask:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;

    .line 796
    iget p3, p2, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->activityId:I

    if-ne p3, p0, :cond_1

    .line 797
    iget-object p0, p2, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->bundleUrl:Ljava/lang/String;

    return-object p0

    :cond_2
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getBundleUrlForActivityId(ILjava/lang/String;Ljava/lang/String;ZZZ)Ljava/lang/String;
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    const/4 p1, -0x1

    if-ne p0, p1, :cond_0

    .line 809
    sget-object p0, Lhost/exp/exponent/kernel/Kernel;->sInstance:Lhost/exp/exponent/kernel/Kernel;

    invoke-direct {p0}, Lhost/exp/exponent/kernel/Kernel;->getBundleUrl()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 812
    :cond_0
    sget-object p1, Lhost/exp/exponent/kernel/Kernel;->sManifestUrlToExperienceActivityTask:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    const/4 p3, 0x0

    if-eqz p2, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;

    .line 813
    iget p5, p2, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->activityId:I

    if-ne p5, p0, :cond_1

    .line 814
    iget-object p0, p2, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->bundleUrl:Ljava/lang/String;

    if-nez p0, :cond_2

    return-object p3

    :cond_2
    if-eqz p4, :cond_4

    const-string p1, "hot=false"

    .line 820
    invoke-virtual {p0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_3

    const-string p2, "hot=true"

    .line 821
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 823
    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "&hot=true"

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_4
    :goto_0
    return-object p0

    :cond_5
    return-object p3
.end method

.method private static getExceptionId(Ljava/lang/Integer;)I
    .locals 4

    if-eqz p0, :cond_1

    .line 1104
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 1108
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    return p0

    .line 1105
    :cond_1
    :goto_0
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x41dfffffffc00000L    # 2.147483647E9

    mul-double v0, v0, v2

    neg-double v0, v0

    double-to-int p0, v0

    return p0
.end method

.method private getKernelLaunchOptions()Landroid/os/Bundle;
    .locals 3

    .line 375
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 376
    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    const-string v2, "referrer"

    invoke-virtual {v1, v2}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 379
    :try_start_0
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 381
    sget-object v2, Lhost/exp/exponent/kernel/Kernel;->TAG:Ljava/lang/String;

    invoke-static {v2, v1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 385
    :cond_0
    :goto_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 386
    invoke-static {v0}, Lhost/exp/exponent/utils/JSONBundleConverter;->JSONToBundle(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "exp"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v1
.end method

.method private getKernelRevisionId()Ljava/lang/String;
    .locals 2

    .line 339
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentManifest:Lhost/exp/exponent/ExponentManifest;

    const-string v1, "revisionId"

    invoke-virtual {v0, v1}, Lhost/exp/exponent/ExponentManifest;->getKernelManifestField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getManifestUrlFromFullUri(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v0, 0x0

    if-eqz p1, :cond_5

    .line 556
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 557
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "--/"

    .line 558
    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p1

    if-ltz p1, :cond_1

    .line 561
    invoke-virtual {v1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    .line 562
    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 564
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "--"

    .line 565
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_1

    .line 568
    :cond_0
    invoke-virtual {v2, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    :cond_1
    :goto_1
    const-string v3, "release-channel"

    .line 574
    invoke-virtual {v1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 575
    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    const/4 v4, 0x0

    if-eqz v1, :cond_3

    const/16 v5, 0x20

    .line 581
    invoke-virtual {v1, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    const/4 v6, -0x1

    if-le v5, v6, :cond_2

    .line 583
    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 585
    :cond_2
    invoke-virtual {v2, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 589
    :cond_3
    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 591
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2b

    .line 593
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ltz v1, :cond_4

    if-gez p1, :cond_4

    .line 596
    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 600
    :cond_4
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_5

    .line 601
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result p1

    const/16 v1, 0x2f

    if-ne p1, v1, :cond_5

    .line 603
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    invoke-virtual {v0, v4, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_5
    return-object v0
.end method

.method private goToHome()V
    .locals 2

    .line 1171
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.category.HOME"

    .line 1172
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    .line 1173
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1174
    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private static handleReactNativeError(Lhost/exp/exponent/kernel/ExponentErrorMessage;Ljava/lang/Object;Ljava/lang/Integer;Ljava/lang/Boolean;)V
    .locals 9

    .line 1054
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_0

    .line 1056
    invoke-static {p1}, Lhost/exp/exponent/RNObject;->wrap(Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object p1

    .line 1057
    new-instance v1, Lhost/exp/exponent/RNObject;

    const-string v2, "com.facebook.react.bridge.Arguments"

    invoke-direct {v1, v2}, Lhost/exp/exponent/RNObject;-><init>(Ljava/lang/String;)V

    .line 1058
    invoke-virtual {p1}, Lhost/exp/exponent/RNObject;->version()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhost/exp/exponent/RNObject;->loadVersion(Ljava/lang/String;)Lhost/exp/exponent/RNObject;

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1060
    :goto_0
    new-array v4, v2, [Ljava/lang/Object;

    const-string v5, "size"

    invoke-virtual {p1, v5, v4}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ge v3, v4, :cond_0

    :try_start_0
    const-string v4, "toBundle"

    const/4 v5, 0x1

    .line 1062
    new-array v6, v5, [Ljava/lang/Object;

    const-string v7, "getMap"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v2

    invoke-virtual {p1, v7, v5}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v6, v2

    invoke-virtual {v1, v4, v6}, Lhost/exp/exponent/RNObject;->callStatic(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Bundle;

    .line 1063
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v4

    .line 1065
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1088
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p1

    new-array p1, p1, [Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Landroid/os/Bundle;

    .line 1090
    new-instance v0, Lhost/exp/exponent/kernel/ExponentError;

    .line 1091
    invoke-static {p2}, Lhost/exp/exponent/kernel/Kernel;->getExceptionId(Ljava/lang/Integer;)I

    move-result p2

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    invoke-direct {v0, p0, p1, p2, p3}, Lhost/exp/exponent/kernel/ExponentError;-><init>(Lhost/exp/exponent/kernel/ExponentErrorMessage;[Landroid/os/Bundle;IZ)V

    .line 1090
    invoke-static {v0}, Lhost/exp/exponent/experience/BaseExperienceActivity;->addError(Lhost/exp/exponent/kernel/ExponentError;)V

    return-void
.end method

.method public static handleReactNativeError(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;Ljava/lang/Boolean;)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .line 1039
    invoke-static {p0}, Lhost/exp/exponent/kernel/ExponentErrorMessage;->developerErrorMessage(Ljava/lang/String;)Lhost/exp/exponent/kernel/ExponentErrorMessage;

    move-result-object p0

    invoke-static {p0, p1, p2, p3}, Lhost/exp/exponent/kernel/Kernel;->handleReactNativeError(Lhost/exp/exponent/kernel/ExponentErrorMessage;Ljava/lang/Object;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    return-void
.end method

.method public static handleReactNativeError(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;Ljava/lang/Boolean;)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .line 1046
    invoke-static {p1}, Lhost/exp/exponent/kernel/ExponentErrorMessage;->developerErrorMessage(Ljava/lang/String;)Lhost/exp/exponent/kernel/ExponentErrorMessage;

    move-result-object p1

    invoke-static {p1, p2, p3, p4}, Lhost/exp/exponent/kernel/Kernel;->handleReactNativeError(Lhost/exp/exponent/kernel/ExponentErrorMessage;Ljava/lang/Object;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    if-eqz p0, :cond_0

    .line 1048
    invoke-static {p0}, Lhost/exp/expoview/Exponent;->logException(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method private kernelBundleListener()Lhost/exp/expoview/Exponent$BundleListener;
    .locals 1

    .line 265
    new-instance v0, Lhost/exp/exponent/kernel/Kernel$3;

    invoke-direct {v0, p0}, Lhost/exp/exponent/kernel/Kernel$3;-><init>(Lhost/exp/exponent/kernel/Kernel;)V

    return-object v0
.end method

.method private killOrphanedLauncherActivities()V
    .locals 5

    .line 933
    :try_start_0
    const-class v0, Landroid/app/ActivityManager$RecentTaskInfo;

    const-string v1, "numActivities"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    .line 935
    invoke-virtual {p0}, Lhost/exp/exponent/kernel/Kernel;->getTasks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$AppTask;

    .line 936
    invoke-virtual {v1}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v2

    .line 937
    iget v3, v2, Landroid/app/ActivityManager$RecentTaskInfo;->numActivities:I

    if-nez v3, :cond_1

    iget-object v3, v2, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.intent.action.MAIN"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 938
    invoke-virtual {v1}, Landroid/app/ActivityManager$AppTask;->finishAndRemoveTask()V

    return-void

    .line 942
    :cond_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x17

    if-lt v3, v4, :cond_0

    .line 943
    iget v3, v2, Landroid/app/ActivityManager$RecentTaskInfo;->numActivities:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    iget-object v2, v2, Landroid/app/ActivityManager$RecentTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lhost/exp/exponent/LauncherActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 944
    invoke-virtual {v1}, Landroid/app/ActivityManager$AppTask;->finishAndRemoveTask()V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 953
    sget-object v1, Lhost/exp/exponent/kernel/Kernel;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    .line 951
    sget-object v1, Lhost/exp/exponent/kernel/Kernel;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_0
    return-void
.end method

.method private openDevActivity(Landroid/app/Activity;)V
    .locals 4

    const-string v0, "activity"

    .line 535
    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 536
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$AppTask;

    .line 537
    invoke-virtual {v1}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    .line 539
    const-class v3, Lhost/exp/exponent/ExponentDevActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 540
    invoke-virtual {v1}, Landroid/app/ActivityManager$AppTask;->moveToFront()V

    return-void

    .line 545
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lhost/exp/exponent/ExponentDevActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 546
    invoke-static {v0}, Lhost/exp/exponent/kernel/Kernel;->addIntentDocumentFlags(Landroid/content/Intent;)V

    .line 547
    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private openHomeActivity()V
    .locals 4

    .line 416
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 417
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$AppTask;

    .line 418
    invoke-virtual {v1}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    .line 420
    const-class v3, Lhost/exp/exponent/experience/HomeActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 421
    invoke-virtual {v1}, Landroid/app/ActivityManager$AppTask;->moveToFront()V

    return-void

    .line 426
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mActivityContext:Landroid/app/Activity;

    const-class v2, Lhost/exp/exponent/experience/HomeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 427
    invoke-static {v0}, Lhost/exp/exponent/kernel/Kernel;->addIntentDocumentFlags(Landroid/content/Intent;)V

    .line 429
    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mActivityContext:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private openManifestUrl(Ljava/lang/String;Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;Ljava/lang/Boolean;)V
    .locals 1

    const/4 v0, 0x0

    .line 613
    invoke-direct {p0, p1, p2, p3, v0}, Lhost/exp/exponent/kernel/Kernel;->openManifestUrl(Ljava/lang/String;Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;Ljava/lang/Boolean;Z)V

    return-void
.end method

.method private openManifestUrl(Ljava/lang/String;Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;Ljava/lang/Boolean;Z)V
    .locals 9

    .line 617
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/soloader/SoLoader;->init(Landroid/content/Context;Z)V

    if-nez p2, :cond_0

    .line 620
    sget-object p2, Lhost/exp/exponent/kernel/Kernel;->mManifestUrlToOptions:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 622
    :cond_0
    sget-object v0, Lhost/exp/exponent/kernel/Kernel;->mManifestUrlToOptions:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    if-eqz p1, :cond_8

    const-string p2, ""

    .line 625
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    goto/16 :goto_5

    .line 630
    :cond_1
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 631
    invoke-direct {p0, p4}, Lhost/exp/exponent/kernel/Kernel;->openShellAppActivity(Z)V

    return-void

    .line 635
    :cond_2
    invoke-static {}, Lhost/exp/exponent/experience/ErrorActivity;->clearErrorList()V

    .line 637
    invoke-virtual {p0}, Lhost/exp/exponent/kernel/Kernel;->getExperienceActivityTasks()Ljava/util/List;

    move-result-object p2

    const/4 v0, 0x0

    if-eqz p2, :cond_4

    .line 640
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 641
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$AppTask;

    .line 642
    invoke-virtual {v2}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    const-string v4, "experienceUrl"

    .line 643
    invoke-virtual {v3, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    move-object v2, v0

    .line 650
    :goto_2
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_5

    if-nez v2, :cond_5

    .line 651
    invoke-virtual {p0, p1}, Lhost/exp/exponent/kernel/Kernel;->openOptimisticExperienceActivity(Ljava/lang/String;)V

    :cond_5
    if-eqz v2, :cond_6

    .line 656
    :try_start_0
    invoke-virtual {v2}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object p2

    iget p2, p2, Landroid/app/ActivityManager$RecentTaskInfo;->id:I

    invoke-virtual {p0, p2}, Lhost/exp/exponent/kernel/Kernel;->moveTaskToFront(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 660
    :catch_0
    invoke-virtual {p0, p1}, Lhost/exp/exponent/kernel/Kernel;->openOptimisticExperienceActivity(Ljava/lang/String;)V

    move-object v8, v0

    goto :goto_4

    :cond_6
    :goto_3
    move-object v8, v2

    :goto_4
    if-nez v8, :cond_7

    .line 666
    new-instance p2, Lhost/exp/exponent/kernel/Kernel$4;

    move-object v3, p2

    move-object v4, p0

    move-object v5, p1

    move v6, p4

    move-object v7, p1

    invoke-direct/range {v3 .. v8}, Lhost/exp/exponent/kernel/Kernel$4;-><init>(Lhost/exp/exponent/kernel/Kernel;Ljava/lang/String;ZLjava/lang/String;Landroid/app/ActivityManager$AppTask;)V

    .line 716
    invoke-virtual {p2}, Lhost/exp/exponent/kernel/Kernel$4;->start()V

    :cond_7
    return-void

    .line 626
    :cond_8
    :goto_5
    invoke-direct {p0}, Lhost/exp/exponent/kernel/Kernel;->openHomeActivity()V

    return-void
.end method

.method private openManifestUrlStep2(Ljava/lang/String;Lorg/json/JSONObject;Landroid/app/ActivityManager$AppTask;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const-string v0, "bundleUrl"

    .line 721
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lhost/exp/exponent/kernel/ExponentUrls;->toHttp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 722
    invoke-virtual {p0, p1}, Lhost/exp/exponent/kernel/Kernel;->getExperienceActivityTask(Ljava/lang/String;)Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;

    move-result-object v1

    .line 723
    iput-object v0, v1, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->bundleUrl:Ljava/lang/String;

    .line 725
    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentManifest:Lhost/exp/exponent/ExponentManifest;

    invoke-virtual {v1, p1, p2}, Lhost/exp/exponent/ExponentManifest;->normalizeManifest(Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p2

    .line 726
    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    const-string v2, "nux_has_finished_first_run"

    invoke-virtual {v1, v2}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 730
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-nez v3, :cond_0

    sget-boolean v3, Lhost/exp/exponent/kernel/KernelConfig;->HIDE_NUX:Z

    if-nez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_1

    if-nez v1, :cond_1

    const/4 v4, 0x1

    .line 732
    :cond_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "loadNux"

    .line 733
    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    if-nez p3, :cond_2

    .line 736
    invoke-virtual {p0, p1, p2, v0, v1}, Lhost/exp/exponent/kernel/Kernel;->sendManifestToExperienceActivity(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONObject;)V

    :cond_2
    if-eqz v4, :cond_3

    .line 740
    iget-object p3, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-virtual {p3, v2, v5}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->setBoolean(Ljava/lang/String;Z)V

    .line 743
    :cond_3
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object p3

    const-string v0, "manifestUrl"

    .line 744
    invoke-interface {p3, v0, p1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "manifestString"

    invoke-interface {p3, p2, p1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 746
    new-instance p1, Lhost/exp/exponent/kernel/Kernel$5;

    invoke-direct {p1, p0}, Lhost/exp/exponent/kernel/Kernel$5;-><init>(Lhost/exp/exponent/kernel/Kernel;)V

    const-string p2, "ExponentKernel.addHistoryItem"

    invoke-static {p2, p3, p1}, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider;->queueEvent(Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$KernelEventCallback;)V

    .line 758
    invoke-direct {p0}, Lhost/exp/exponent/kernel/Kernel;->killOrphanedLauncherActivities()V

    return-void
.end method

.method private openShellAppActivity(Z)V
    .locals 5

    :try_start_0
    const-string v0, "host.exp.exponent.MainActivity"

    .line 434
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 435
    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mContext:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 436
    invoke-virtual {v1}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$AppTask;

    .line 437
    invoke-virtual {v2}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    .line 439
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 440
    invoke-virtual {v2}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object p1

    iget p1, p1, Landroid/app/ActivityManager$RecentTaskInfo;->id:I

    invoke-virtual {p0, p1}, Lhost/exp/exponent/kernel/Kernel;->moveTaskToFront(I)V

    return-void

    .line 445
    :cond_1
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lhost/exp/exponent/kernel/Kernel;->mActivityContext:Landroid/app/Activity;

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 446
    invoke-static {v1}, Lhost/exp/exponent/kernel/Kernel;->addIntentDocumentFlags(Landroid/content/Intent;)V

    if-eqz p1, :cond_2

    const-string p1, "loadFromCache"

    const/4 v0, 0x1

    .line 449
    invoke-virtual {v1, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 452
    :cond_2
    iget-object p1, p0, Lhost/exp/exponent/kernel/Kernel;->mActivityContext:Landroid/app/Activity;

    invoke-virtual {p1, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 454
    :catch_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Could not find activity to open (MainActivity is not present)."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private updateKernelRNOkHttp()V
    .locals 4

    .line 155
    new-instance v0, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v0}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x0

    .line 156
    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/OkHttpClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 157
    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/OkHttpClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 158
    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/OkHttpClient$Builder;->writeTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    new-instance v1, Lcom/facebook/react/modules/network/ReactCookieJarContainer;

    invoke-direct {v1}, Lcom/facebook/react/modules/network/ReactCookieJarContainer;-><init>()V

    .line 159
    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->cookieJar(Lokhttp3/CookieJar;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;

    .line 160
    invoke-virtual {v1}, Lhost/exp/exponent/network/ExponentNetwork;->getCache()Lokhttp3/Cache;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->cache(Lokhttp3/Cache;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 168
    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;

    invoke-virtual {v1, v0}, Lhost/exp/exponent/network/ExponentNetwork;->addInterceptors(Lokhttp3/OkHttpClient$Builder;)V

    .line 169
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;

    invoke-static {v0}, Lhost/exp/exponent/ReactNativeStaticHelpers;->setExponentNetwork(Lhost/exp/exponent/network/ExponentNetwork;)V

    return-void
.end method


# virtual methods
.method public addDevMenu()V
    .locals 4

    .line 1155
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mContext:Landroid/content/Context;

    const-class v2, Lhost/exp/exponent/LauncherActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.MAIN"

    .line 1156
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x20000000

    .line 1157
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v1, "dev_flag"

    const/4 v2, 0x1

    .line 1158
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1160
    new-instance v1, Landroidx/core/content/pm/ShortcutInfoCompat$Builder;

    iget-object v2, p0, Lhost/exp/exponent/kernel/Kernel;->mContext:Landroid/content/Context;

    const-string v3, "devtools_shortcut"

    invoke-direct {v1, v2, v3}, Landroidx/core/content/pm/ShortcutInfoCompat$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v2, p0, Lhost/exp/exponent/kernel/Kernel;->mContext:Landroid/content/Context;

    sget v3, Lhost/exp/expoview/R$mipmap;->dev_icon:I

    .line 1162
    invoke-static {v2, v3}, Landroidx/core/graphics/drawable/IconCompat;->createWithResource(Landroid/content/Context;I)Landroidx/core/graphics/drawable/IconCompat;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroidx/core/content/pm/ShortcutInfoCompat$Builder;->setIcon(Landroidx/core/graphics/drawable/IconCompat;)Landroidx/core/content/pm/ShortcutInfoCompat$Builder;

    move-result-object v1

    const-string v2, "Dev Tools"

    .line 1163
    invoke-virtual {v1, v2}, Landroidx/core/content/pm/ShortcutInfoCompat$Builder;->setShortLabel(Ljava/lang/CharSequence;)Landroidx/core/content/pm/ShortcutInfoCompat$Builder;

    move-result-object v1

    .line 1164
    invoke-virtual {v1, v0}, Landroidx/core/content/pm/ShortcutInfoCompat$Builder;->setIntent(Landroid/content/Intent;)Landroidx/core/content/pm/ShortcutInfoCompat$Builder;

    move-result-object v0

    .line 1165
    invoke-virtual {v0}, Landroidx/core/content/pm/ShortcutInfoCompat$Builder;->build()Landroidx/core/content/pm/ShortcutInfoCompat;

    move-result-object v0

    .line 1167
    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroidx/core/content/pm/ShortcutManagerCompat;->requestPinShortcut(Landroid/content/Context;Landroidx/core/content/pm/ShortcutInfoCompat;Landroid/content/IntentSender;)Z

    return-void
.end method

.method public getActivityContext()Landroid/app/Activity;
    .locals 1

    .line 351
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel;->mActivityContext:Landroid/app/Activity;

    return-object v0
.end method

.method public getExperienceActivityTask(Ljava/lang/String;)Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;
    .locals 2

    .line 399
    sget-object v0, Lhost/exp/exponent/kernel/Kernel;->sManifestUrlToExperienceActivityTask:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;

    if-eqz v0, :cond_0

    return-object v0

    .line 404
    :cond_0
    new-instance v0, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;

    invoke-direct {v0, p1}, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;-><init>(Ljava/lang/String;)V

    .line 405
    sget-object v1, Lhost/exp/exponent/kernel/Kernel;->sManifestUrlToExperienceActivityTask:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public getExperienceActivityTasks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/app/ActivityManager$AppTask;",
            ">;"
        }
    .end annotation

    .line 921
    invoke-virtual {p0}, Lhost/exp/exponent/kernel/Kernel;->getTasks()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getReactInstanceManager()Lcom/facebook/react/ReactInstanceManager;
    .locals 1

    .line 361
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel;->mReactInstanceManager:Lcom/facebook/react/ReactInstanceManager;

    return-object v0
.end method

.method public getReactRootView()Lcom/facebook/react/ReactRootView;
    .locals 4

    .line 365
    new-instance v0, Lversioned/host/exp/exponent/ReactUnthemedRootView;

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lversioned/host/exp/exponent/ReactUnthemedRootView;-><init>(Landroid/content/Context;)V

    .line 366
    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mReactInstanceManager:Lcom/facebook/react/ReactInstanceManager;

    .line 369
    invoke-direct {p0}, Lhost/exp/exponent/kernel/Kernel;->getKernelLaunchOptions()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "main"

    .line 366
    invoke-virtual {v0, v1, v3, v2}, Lcom/facebook/react/ReactRootView;->startReactApplication(Lcom/facebook/react/ReactInstanceManager;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v0
.end method

.method public getTasks()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/app/ActivityManager$AppTask;",
            ">;"
        }
    .end annotation

    .line 915
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 916
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public handleError(Ljava/lang/Exception;)V
    .locals 4

    .line 1099
    invoke-static {p1}, Lhost/exp/exponent/exceptions/ExceptionUtils;->exceptionToErrorMessage(Ljava/lang/Exception;)Lhost/exp/exponent/kernel/ExponentErrorMessage;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v2}, Lhost/exp/exponent/kernel/Kernel;->handleReactNativeError(Lhost/exp/exponent/kernel/ExponentErrorMessage;Ljava/lang/Object;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    .line 1100
    invoke-static {p1}, Lhost/exp/expoview/Exponent;->logException(Ljava/lang/Throwable;)V

    return-void
.end method

.method public handleError(Ljava/lang/String;)V
    .locals 3

    .line 1095
    invoke-static {p1}, Lhost/exp/exponent/kernel/ExponentErrorMessage;->developerErrorMessage(Ljava/lang/String;)Lhost/exp/exponent/kernel/ExponentErrorMessage;

    move-result-object p1

    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v1}, Lhost/exp/exponent/kernel/Kernel;->handleReactNativeError(Lhost/exp/exponent/kernel/ExponentErrorMessage;Ljava/lang/Object;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    return-void
.end method

.method public handleIntent(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 6

    :try_start_0
    const-string v0, "EXKernelDisableNuxDefaultsKey"

    const/4 v1, 0x0

    .line 466
    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 467
    sput-boolean v0, Lhost/exp/exponent/Constants;->DISABLE_NUX:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 471
    :catch_0
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 472
    invoke-virtual {p0, p1}, Lhost/exp/exponent/kernel/Kernel;->setActivityContext(Landroid/app/Activity;)V

    .line 474
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_1

    move-object v3, v2

    goto :goto_0

    .line 475
    :cond_1
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_0
    if-eqz v0, :cond_7

    const-string v4, "dev_flag"

    .line 478
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 479
    invoke-direct {p0, p1}, Lhost/exp/exponent/kernel/Kernel;->openDevActivity(Landroid/app/Activity;)V

    return-void

    :cond_2
    const-string p1, "notification"

    .line 484
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v4, "notification_object"

    .line 485
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "notificationExperienceUrl"

    .line 486
    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_6

    .line 488
    invoke-static {v4}, Lhost/exp/exponent/notifications/ExponentNotification;->fromJSONObjectString(Ljava/lang/String;)Lhost/exp/exponent/notifications/ExponentNotification;

    move-result-object v1

    if-eqz v1, :cond_4

    const-string v2, "actionType"

    .line 491
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 492
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lhost/exp/exponent/notifications/ExponentNotification;->setActionType(Ljava/lang/String;)V

    .line 493
    new-instance v0, Lhost/exp/exponent/notifications/ExponentNotificationManager;

    iget-object v2, p0, Lhost/exp/exponent/kernel/Kernel;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lhost/exp/exponent/notifications/ExponentNotificationManager;-><init>(Landroid/content/Context;)V

    .line 494
    iget-object v2, v1, Lhost/exp/exponent/notifications/ExponentNotification;->experienceId:Ljava/lang/String;

    iget v4, v1, Lhost/exp/exponent/notifications/ExponentNotification;->notificationId:I

    invoke-virtual {v0, v2, v4}, Lhost/exp/exponent/notifications/ExponentNotificationManager;->cancel(Ljava/lang/String;I)V

    .line 497
    :cond_3
    invoke-static {p2}, Landroid/app/RemoteInput;->getResultsFromIntent(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object p2

    if-eqz p2, :cond_4

    const-string v0, "notification_remote_input"

    .line 499
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Lhost/exp/exponent/notifications/ExponentNotification;->setInputText(Ljava/lang/String;)V

    .line 502
    :cond_4
    new-instance p2, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;

    if-nez v3, :cond_5

    move-object v3, v5

    :cond_5
    invoke-direct {p2, v5, v3, p1, v1}, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/notifications/ExponentNotification;)V

    invoke-virtual {p0, p2}, Lhost/exp/exponent/kernel/Kernel;->openExperience(Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;)V

    return-void

    :cond_6
    const-string p1, "shortcutExperienceUrl"

    .line 507
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 509
    new-instance p2, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;

    invoke-direct {p2, p1, v3, v2}, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lhost/exp/exponent/kernel/Kernel;->openExperience(Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;)V

    return-void

    :cond_7
    if-eqz v1, :cond_9

    .line 515
    sget-object p1, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    if-nez p1, :cond_8

    .line 517
    new-instance p1, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;

    invoke-direct {p1, v3, v3, v2}, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lhost/exp/exponent/kernel/Kernel;->openExperience(Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;)V

    return-void

    .line 525
    :cond_8
    new-instance p1, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;

    sget-object p2, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    invoke-direct {p1, p2, v3, v2}, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lhost/exp/exponent/kernel/Kernel;->openExperience(Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;)V

    return-void

    .line 530
    :cond_9
    sget-object p1, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    if-nez p1, :cond_a

    const-string p1, ""

    goto :goto_1

    :cond_a
    sget-object p1, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    .line 531
    :goto_1
    new-instance p2, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;

    invoke-direct {p2, p1, p1, v2}, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lhost/exp/exponent/kernel/Kernel;->openExperience(Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;)V

    return-void
.end method

.method public hasOptionsForManifestUrl(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 1

    .line 391
    sget-object v0, Lhost/exp/exponent/kernel/Kernel;->mManifestUrlToOptions:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public installShortcut(Ljava/lang/String;)V
    .locals 4

    .line 1129
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-virtual {v0, p1}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getManifest(Ljava/lang/String;)Lhost/exp/exponent/storage/ExponentSharedPreferences$ManifestAndBundleUrl;

    move-result-object v0

    .line 1130
    iget-object v0, v0, Lhost/exp/exponent/storage/ExponentSharedPreferences$ManifestAndBundleUrl;->manifest:Lorg/json/JSONObject;

    const-string v1, "iconUrl"

    .line 1133
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1134
    iget-object v2, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentManifest:Lhost/exp/exponent/ExponentManifest;

    new-instance v3, Lhost/exp/exponent/kernel/Kernel$10;

    invoke-direct {v3, p0, p1, v0}, Lhost/exp/exponent/kernel/Kernel$10;-><init>(Lhost/exp/exponent/kernel/Kernel;Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-virtual {v2, v1, v3}, Lhost/exp/exponent/ExponentManifest;->loadIconBitmap(Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$BitmapListener;)V

    return-void
.end method

.method public installShortcut(Ljava/lang/String;Lcom/facebook/react/bridge/ReadableMap;Ljava/lang/String;)V
    .locals 1

    .line 1123
    invoke-static {p2}, Lversioned/host/exp/exponent/ReadableObjectUtils;->readableToJson(Lcom/facebook/react/bridge/ReadableMap;)Lorg/json/JSONObject;

    move-result-object p2

    .line 1124
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-virtual {v0, p1, p2, p3}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->updateManifest(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1125
    invoke-virtual {p0, p1}, Lhost/exp/exponent/kernel/Kernel;->installShortcut(Ljava/lang/String;)V

    return-void
.end method

.method public isRunning()Ljava/lang/Boolean;
    .locals 1

    .line 343
    iget-boolean v0, p0, Lhost/exp/exponent/kernel/Kernel;->mIsRunning:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhost/exp/exponent/kernel/Kernel;->mHasError:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public isStarted()Ljava/lang/Boolean;
    .locals 1

    .line 347
    iget-boolean v0, p0, Lhost/exp/exponent/kernel/Kernel;->mIsStarted:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public killActivityStack(Landroid/app/Activity;)V
    .locals 4

    .line 976
    invoke-virtual {p1}, Landroid/app/Activity;->getTaskId()I

    move-result v0

    invoke-direct {p0, v0}, Lhost/exp/exponent/kernel/Kernel;->experienceActivityTaskForTaskId(I)Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 978
    iget-object v0, v0, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->manifestUrl:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lhost/exp/exponent/kernel/Kernel;->removeExperienceActivityTask(Ljava/lang/String;)V

    :cond_0
    const-string v0, "activity"

    .line 982
    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 983
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$AppTask;

    .line 984
    invoke-virtual {v1}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v2

    iget v2, v2, Landroid/app/ActivityManager$RecentTaskInfo;->id:I

    invoke-virtual {p1}, Landroid/app/Activity;->getTaskId()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 985
    invoke-virtual {v1}, Landroid/app/ActivityManager$AppTask;->finishAndRemoveTask()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public moveTaskToFront(I)V
    .locals 3

    .line 958
    invoke-virtual {p0}, Lhost/exp/exponent/kernel/Kernel;->getTasks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$AppTask;

    .line 959
    invoke-virtual {v1}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v2

    iget v2, v2, Landroid/app/ActivityManager$RecentTaskInfo;->id:I

    if-ne v2, p1, :cond_0

    .line 962
    invoke-direct {p0, p1}, Lhost/exp/exponent/kernel/Kernel;->experienceActivityTaskForTaskId(I)Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 964
    iget-object v2, v2, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->experienceActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhost/exp/exponent/experience/ExperienceActivity;

    if-eqz v2, :cond_1

    .line 966
    invoke-virtual {v2}, Lhost/exp/exponent/experience/ExperienceActivity;->shouldCheckOptions()V

    .line 970
    :cond_1
    invoke-virtual {v1}, Landroid/app/ActivityManager$AppTask;->moveToFront()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public openExperience(Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;)V
    .locals 2

    .line 551
    iget-object v0, p1, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->manifestUri:Ljava/lang/String;

    invoke-direct {p0, v0}, Lhost/exp/exponent/kernel/Kernel;->getManifestUrlFromFullUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lhost/exp/exponent/kernel/Kernel;->openManifestUrl(Ljava/lang/String;Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;Ljava/lang/Boolean;)V

    return-void
.end method

.method public openOptimisticExperienceActivity(Ljava/lang/String;)V
    .locals 3

    .line 843
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mActivityContext:Landroid/app/Activity;

    const-class v2, Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 844
    invoke-static {v0}, Lhost/exp/exponent/kernel/Kernel;->addIntentDocumentFlags(Landroid/content/Intent;)V

    const-string v1, "experienceUrl"

    .line 845
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "isOptimistic"

    const/4 v1, 0x1

    .line 846
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 847
    iget-object p1, p0, Lhost/exp/exponent/kernel/Kernel;->mActivityContext:Landroid/app/Activity;

    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 849
    sget-object v0, Lhost/exp/exponent/kernel/Kernel;->TAG:Ljava/lang/String;

    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public popOptionsForManifestUrl(Ljava/lang/String;)Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;
    .locals 1

    .line 395
    sget-object v0, Lhost/exp/exponent/kernel/Kernel;->mManifestUrlToOptions:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;

    return-object p1
.end method

.method public reloadJSBundle()V
    .locals 8

    .line 249
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 252
    :cond_0
    invoke-direct {p0}, Lhost/exp/exponent/kernel/Kernel;->getBundleUrl()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    .line 253
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/Kernel;->mHasError:Z

    .line 254
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0}, Lhost/exp/exponent/kernel/Kernel;->kernelBundleListener()Lhost/exp/expoview/Exponent$BundleListener;

    move-result-object v6

    const/4 v7, 0x1

    const-string v4, "kernel"

    const-string v5, "UNVERSIONED"

    invoke-virtual/range {v1 .. v7}, Lhost/exp/expoview/Exponent;->loadJSBundle(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/expoview/Exponent$BundleListener;Z)Z

    return-void
.end method

.method public reloadVisibleExperience(Ljava/lang/String;Z)Z
    .locals 5

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 997
    :cond_0
    sget-object v0, Lhost/exp/exponent/kernel/Kernel;->sManifestUrlToExperienceActivityTask:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;

    .line 998
    iget-object v4, v1, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->manifestUrl:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 999
    iget-object v0, v1, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->experienceActivity:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_2

    move-object v0, v3

    goto :goto_0

    :cond_2
    iget-object v0, v1, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->experienceActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhost/exp/exponent/experience/ExperienceActivity;

    :goto_0
    if-nez v0, :cond_3

    goto :goto_1

    .line 1006
    :cond_3
    invoke-virtual {v0}, Lhost/exp/exponent/experience/ExperienceActivity;->isLoading()Z

    move-result v1

    if-eqz v1, :cond_4

    return v2

    .line 1010
    :cond_4
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v1

    new-instance v4, Lhost/exp/exponent/kernel/Kernel$9;

    invoke-direct {v4, p0, v0}, Lhost/exp/exponent/kernel/Kernel$9;-><init>(Lhost/exp/exponent/kernel/Kernel;Lhost/exp/exponent/experience/ExperienceActivity;)V

    invoke-virtual {v1, v4}, Lhost/exp/expoview/Exponent;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_5
    move-object v0, v3

    :goto_1
    if-eqz v0, :cond_6

    .line 1022
    invoke-virtual {p0, v0}, Lhost/exp/exponent/kernel/Kernel;->killActivityStack(Landroid/app/Activity;)V

    .line 1024
    :cond_6
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v3, v0, p2}, Lhost/exp/exponent/kernel/Kernel;->openManifestUrl(Ljava/lang/String;Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;Ljava/lang/Boolean;Z)V

    return v2
.end method

.method public removeExperienceActivityTask(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 411
    sget-object v0, Lhost/exp/exponent/kernel/Kernel;->sManifestUrlToExperienceActivityTask:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public sendBundleToExperienceActivity(Ljava/lang/String;)V
    .locals 1

    .line 892
    new-instance v0, Lhost/exp/exponent/kernel/Kernel$8;

    invoke-direct {v0, p0, p1}, Lhost/exp/exponent/kernel/Kernel$8;-><init>(Lhost/exp/exponent/kernel/Kernel;Ljava/lang/String;)V

    const-string p1, "loadBundleForExperienceActivity"

    invoke-static {p1, v0}, Lhost/exp/exponent/utils/AsyncCondition;->wait(Ljava/lang/String;Lhost/exp/exponent/utils/AsyncCondition$AsyncConditionListener;)V

    return-void
.end method

.method public sendLoadingScreenManifestToExperienceActivity(Lorg/json/JSONObject;)V
    .locals 1

    .line 862
    new-instance v0, Lhost/exp/exponent/kernel/Kernel$6;

    invoke-direct {v0, p0, p1}, Lhost/exp/exponent/kernel/Kernel$6;-><init>(Lhost/exp/exponent/kernel/Kernel;Lorg/json/JSONObject;)V

    const-string p1, "openOptimisticExperienceActivity"

    invoke-static {p1, v0}, Lhost/exp/exponent/utils/AsyncCondition;->wait(Ljava/lang/String;Lhost/exp/exponent/utils/AsyncCondition$AsyncConditionListener;)V

    return-void
.end method

.method public sendManifestToExperienceActivity(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 7

    .line 877
    new-instance v6, Lhost/exp/exponent/kernel/Kernel$7;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lhost/exp/exponent/kernel/Kernel$7;-><init>(Lhost/exp/exponent/kernel/Kernel;Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONObject;)V

    const-string p1, "openExperienceActivity"

    invoke-static {p1, v6}, Lhost/exp/exponent/utils/AsyncCondition;->wait(Ljava/lang/String;Lhost/exp/exponent/utils/AsyncCondition$AsyncConditionListener;)V

    return-void
.end method

.method public setActivityContext(Landroid/app/Activity;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 356
    iput-object p1, p0, Lhost/exp/exponent/kernel/Kernel;->mActivityContext:Landroid/app/Activity;

    :cond_0
    return-void
.end method

.method public setHasError()V
    .locals 1

    const/4 v0, 0x1

    .line 1113
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/Kernel;->mHasError:Z

    return-void
.end method

.method public setOptimisticActivity(Lhost/exp/exponent/experience/ExperienceActivity;I)V
    .locals 0

    .line 854
    iput-object p1, p0, Lhost/exp/exponent/kernel/Kernel;->mOptimisticActivity:Lhost/exp/exponent/experience/ExperienceActivity;

    .line 855
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lhost/exp/exponent/kernel/Kernel;->mOptimisticTaskId:Ljava/lang/Integer;

    const-string p1, "openOptimisticExperienceActivity"

    .line 857
    invoke-static {p1}, Lhost/exp/exponent/utils/AsyncCondition;->notify(Ljava/lang/String;)V

    const-string p1, "openExperienceActivity"

    .line 858
    invoke-static {p1}, Lhost/exp/exponent/utils/AsyncCondition;->notify(Ljava/lang/String;)V

    return-void
.end method

.method public startJSKernel()V
    .locals 10

    .line 174
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 178
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/soloader/SoLoader;->init(Landroid/content/Context;Z)V

    .line 180
    monitor-enter p0

    .line 181
    :try_start_0
    iget-boolean v0, p0, Lhost/exp/exponent/kernel/Kernel;->mIsStarted:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lhost/exp/exponent/kernel/Kernel;->mHasError:Z

    if-nez v0, :cond_1

    .line 182
    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x1

    .line 184
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/Kernel;->mIsStarted:Z

    .line 185
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    iput-boolean v1, p0, Lhost/exp/exponent/kernel/Kernel;->mHasError:Z

    .line 189
    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-virtual {v1}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->shouldUseInternetKernel()Z

    move-result v1

    if-nez v1, :cond_2

    .line 193
    :try_start_1
    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentManifest:Lhost/exp/exponent/ExponentManifest;

    invoke-virtual {v1}, Lhost/exp/exponent/ExponentManifest;->getKernelManifest()Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 195
    :catch_0
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    new-instance v1, Lhost/exp/exponent/kernel/Kernel$1;

    invoke-direct {v1, p0}, Lhost/exp/exponent/kernel/Kernel$1;-><init>(Lhost/exp/exponent/kernel/Kernel;)V

    invoke-virtual {v0, v1}, Lhost/exp/expoview/Exponent;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void

    .line 209
    :cond_2
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lhost/exp/exponent/kernel/Kernel;->getBundleUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v2, Lhost/exp/expoview/ExpoViewBuildConfig;->DEBUG:Z

    if-eqz v2, :cond_3

    const-string v2, ""

    goto :goto_1

    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?versionName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lhost/exp/exponent/kernel/ExpoViewKernel;->getInstance()Lhost/exp/exponent/kernel/ExpoViewKernel;

    move-result-object v3

    invoke-virtual {v3}, Lhost/exp/exponent/kernel/ExpoViewKernel;->getVersionName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 211
    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-virtual {v1}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->shouldUseInternetKernel()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    const-string v2, "is_first_kernel_run"

    .line 212
    invoke-virtual {v1, v2}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 213
    invoke-direct {p0}, Lhost/exp/exponent/kernel/Kernel;->kernelBundleListener()Lhost/exp/expoview/Exponent$BundleListener;

    move-result-object v0

    const-string v1, "assets://kernel.android.bundle"

    invoke-interface {v0, v1}, Lhost/exp/expoview/Exponent$BundleListener;->onBundleLoaded(Ljava/lang/String;)V

    .line 216
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lhost/exp/exponent/kernel/Kernel$2;

    invoke-direct {v1, p0, v5}, Lhost/exp/exponent/kernel/Kernel$2;-><init>(Lhost/exp/exponent/kernel/Kernel;Ljava/lang/String;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_3

    .line 234
    :cond_4
    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    const-string v2, "should_not_use_kernel_cache"

    invoke-virtual {v1, v2}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 236
    sget-boolean v2, Lhost/exp/expoview/ExpoViewBuildConfig;->DEBUG:Z

    if-nez v2, :cond_5

    .line 237
    iget-object v2, p0, Lhost/exp/exponent/kernel/Kernel;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    const-string v3, "kernel_revision_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 239
    invoke-direct {p0}, Lhost/exp/exponent/kernel/Kernel;->getKernelRevisionId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v9, 0x1

    goto :goto_2

    :cond_5
    move v9, v1

    .line 244
    :goto_2
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {p0}, Lhost/exp/exponent/kernel/Kernel;->kernelBundleListener()Lhost/exp/expoview/Exponent$BundleListener;

    move-result-object v8

    const-string v6, "kernel"

    const-string v7, "UNVERSIONED"

    invoke-virtual/range {v3 .. v9}, Lhost/exp/expoview/Exponent;->loadJSBundle(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/expoview/Exponent$BundleListener;Z)Z

    :goto_3
    return-void

    :catchall_0
    move-exception v0

    .line 185
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
