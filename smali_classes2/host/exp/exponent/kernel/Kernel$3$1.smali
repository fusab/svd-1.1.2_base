.class Lhost/exp/exponent/kernel/Kernel$3$1;
.super Ljava/lang/Object;
.source "Kernel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/kernel/Kernel$3;->onBundleLoaded(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lhost/exp/exponent/kernel/Kernel$3;

.field final synthetic val$localBundlePath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhost/exp/exponent/kernel/Kernel$3;Ljava/lang/String;)V
    .locals 0

    .line 272
    iput-object p1, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->this$1:Lhost/exp/exponent/kernel/Kernel$3;

    iput-object p2, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->val$localBundlePath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 275
    invoke-static {}, Lcom/facebook/react/ReactInstanceManager;->builder()Lcom/facebook/react/ReactInstanceManagerBuilder;

    move-result-object v0

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->this$1:Lhost/exp/exponent/kernel/Kernel$3;

    iget-object v1, v1, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    iget-object v1, v1, Lhost/exp/exponent/kernel/Kernel;->mApplicationContext:Landroid/app/Application;

    .line 276
    invoke-virtual {v0, v1}, Lcom/facebook/react/ReactInstanceManagerBuilder;->setApplication(Landroid/app/Application;)Lcom/facebook/react/ReactInstanceManagerBuilder;

    move-result-object v0

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->val$localBundlePath:Ljava/lang/String;

    .line 277
    invoke-virtual {v0, v1}, Lcom/facebook/react/ReactInstanceManagerBuilder;->setJSBundleFile(Ljava/lang/String;)Lcom/facebook/react/ReactInstanceManagerBuilder;

    move-result-object v0

    new-instance v1, Lcom/facebook/react/shell/MainReactPackage;

    invoke-direct {v1}, Lcom/facebook/react/shell/MainReactPackage;-><init>()V

    .line 278
    invoke-virtual {v0, v1}, Lcom/facebook/react/ReactInstanceManagerBuilder;->addPackage(Lcom/facebook/react/ReactPackage;)Lcom/facebook/react/ReactInstanceManagerBuilder;

    move-result-object v0

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->this$1:Lhost/exp/exponent/kernel/Kernel$3;

    iget-object v1, v1, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    iget-object v1, v1, Lhost/exp/exponent/kernel/Kernel;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->this$1:Lhost/exp/exponent/kernel/Kernel$3;

    iget-object v2, v2, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    iget-object v2, v2, Lhost/exp/exponent/kernel/Kernel;->mExponentManifest:Lhost/exp/exponent/ExponentManifest;

    .line 279
    invoke-virtual {v2}, Lhost/exp/exponent/ExponentManifest;->getKernelManifest()Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {}, Lhost/exp/exponent/experience/HomeActivity;->homeExpoPackages()Ljava/util/List;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lversioned/host/exp/exponent/ExponentPackage;->kernelExponentPackage(Landroid/content/Context;Lorg/json/JSONObject;Ljava/util/List;)Lversioned/host/exp/exponent/ExponentPackage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/react/ReactInstanceManagerBuilder;->addPackage(Lcom/facebook/react/ReactPackage;)Lcom/facebook/react/ReactInstanceManagerBuilder;

    move-result-object v0

    sget-object v1, Lcom/facebook/react/common/LifecycleState;->RESUMED:Lcom/facebook/react/common/LifecycleState;

    .line 280
    invoke-virtual {v0, v1}, Lcom/facebook/react/ReactInstanceManagerBuilder;->setInitialLifecycleState(Lcom/facebook/react/common/LifecycleState;)Lcom/facebook/react/ReactInstanceManagerBuilder;

    move-result-object v0

    .line 282
    sget-boolean v1, Lhost/exp/exponent/kernel/KernelConfig;->FORCE_NO_KERNEL_DEBUG_MODE:Z

    const/4 v2, 0x0

    if-nez v1, :cond_1

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->this$1:Lhost/exp/exponent/kernel/Kernel$3;

    iget-object v1, v1, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    iget-object v1, v1, Lhost/exp/exponent/kernel/Kernel;->mExponentManifest:Lhost/exp/exponent/ExponentManifest;

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->this$1:Lhost/exp/exponent/kernel/Kernel$3;

    iget-object v1, v1, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    iget-object v1, v1, Lhost/exp/exponent/kernel/Kernel;->mExponentManifest:Lhost/exp/exponent/ExponentManifest;

    invoke-virtual {v1}, Lhost/exp/exponent/ExponentManifest;->getKernelManifest()Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lhost/exp/exponent/ExponentManifest;->isDebugModeEnabled(Lorg/json/JSONObject;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 283
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v1

    invoke-virtual {v1}, Lhost/exp/expoview/Exponent;->shouldRequestDrawOverOtherAppsPermission()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 284
    new-instance v0, Landroidx/appcompat/app/AlertDialog$Builder;

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->this$1:Lhost/exp/exponent/kernel/Kernel$3;

    iget-object v1, v1, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-static {v1}, Lhost/exp/exponent/kernel/Kernel;->access$000(Lhost/exp/exponent/kernel/Kernel;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Please enable \"Permit drawing over other apps\""

    .line 285
    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Click \"ok\" to open settings. Once you\'ve enabled the setting you\'ll have to restart the app."

    .line 286
    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v3, Lhost/exp/exponent/kernel/Kernel$3$1$1;

    invoke-direct {v3, p0}, Lhost/exp/exponent/kernel/Kernel$3$1$1;-><init>(Lhost/exp/exponent/kernel/Kernel$3$1;)V

    .line 287
    invoke-virtual {v0, v1, v3}, Landroidx/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 294
    invoke-virtual {v0, v2}, Landroidx/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 295
    invoke-virtual {v0}, Landroidx/appcompat/app/AlertDialog$Builder;->show()Landroidx/appcompat/app/AlertDialog;

    return-void

    .line 299
    :cond_0
    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->this$1:Lhost/exp/exponent/kernel/Kernel$3;

    iget-object v1, v1, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    iget-object v1, v1, Lhost/exp/exponent/kernel/Kernel;->mExponentManifest:Lhost/exp/exponent/ExponentManifest;

    const-string v3, "debuggerHost"

    invoke-virtual {v1, v3}, Lhost/exp/exponent/ExponentManifest;->getKernelManifestField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->this$1:Lhost/exp/exponent/kernel/Kernel$3;

    iget-object v3, v3, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    iget-object v3, v3, Lhost/exp/exponent/kernel/Kernel;->mExponentManifest:Lhost/exp/exponent/ExponentManifest;

    const-string v4, "mainModuleName"

    .line 300
    invoke-virtual {v3, v4}, Lhost/exp/exponent/ExponentManifest;->getKernelManifestField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lhost/exp/exponent/RNObject;->wrap(Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object v4

    const-string v5, "UNVERSIONED"

    .line 299
    invoke-static {v5, v1, v3, v4}, Lhost/exp/expoview/Exponent;->enableDeveloperSupport(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/RNObject;)V

    .line 303
    :cond_1
    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->this$1:Lhost/exp/exponent/kernel/Kernel$3;

    iget-object v1, v1, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0}, Lcom/facebook/react/ReactInstanceManagerBuilder;->build()Lcom/facebook/react/ReactInstanceManager;

    move-result-object v0

    invoke-static {v1, v0}, Lhost/exp/exponent/kernel/Kernel;->access$302(Lhost/exp/exponent/kernel/Kernel;Lcom/facebook/react/ReactInstanceManager;)Lcom/facebook/react/ReactInstanceManager;

    .line 304
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->this$1:Lhost/exp/exponent/kernel/Kernel$3;

    iget-object v0, v0, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-static {v0}, Lhost/exp/exponent/kernel/Kernel;->access$300(Lhost/exp/exponent/kernel/Kernel;)Lcom/facebook/react/ReactInstanceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/react/ReactInstanceManager;->createReactContextInBackground()V

    .line 305
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->this$1:Lhost/exp/exponent/kernel/Kernel$3;

    iget-object v0, v0, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0}, Lhost/exp/exponent/kernel/Kernel;->getActivityContext()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 307
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->this$1:Lhost/exp/exponent/kernel/Kernel$3;

    iget-object v0, v0, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-static {v0}, Lhost/exp/exponent/kernel/Kernel;->access$300(Lhost/exp/exponent/kernel/Kernel;)Lcom/facebook/react/ReactInstanceManager;

    move-result-object v0

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->this$1:Lhost/exp/exponent/kernel/Kernel$3;

    iget-object v1, v1, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v1}, Lhost/exp/exponent/kernel/Kernel;->getActivityContext()Landroid/app/Activity;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lcom/facebook/react/ReactInstanceManager;->onHostResume(Landroid/app/Activity;Lcom/facebook/react/modules/core/DefaultHardwareBackBtnHandler;)V

    .line 310
    :cond_2
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->this$1:Lhost/exp/exponent/kernel/Kernel$3;

    iget-object v0, v0, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lhost/exp/exponent/kernel/Kernel;->access$402(Lhost/exp/exponent/kernel/Kernel;Z)Z

    .line 311
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lhost/exp/exponent/kernel/Kernel$KernelStartedRunningEvent;

    invoke-direct {v1}, Lhost/exp/exponent/kernel/Kernel$KernelStartedRunningEvent;-><init>()V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->postSticky(Ljava/lang/Object;)V

    .line 312
    invoke-static {}, Lhost/exp/exponent/kernel/Kernel;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Kernel started running."

    invoke-static {v0, v1}, Lhost/exp/exponent/analytics/EXL;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$3$1;->this$1:Lhost/exp/exponent/kernel/Kernel$3;

    iget-object v0, v0, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    iget-object v0, v0, Lhost/exp/exponent/kernel/Kernel;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    const-string v1, "should_not_use_kernel_cache"

    invoke-virtual {v0, v1, v2}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->setBoolean(Ljava/lang/String;Z)V

    return-void
.end method
