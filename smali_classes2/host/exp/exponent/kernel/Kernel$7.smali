.class Lhost/exp/exponent/kernel/Kernel$7;
.super Ljava/lang/Object;
.source "Kernel.java"

# interfaces
.implements Lhost/exp/exponent/utils/AsyncCondition$AsyncConditionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/kernel/Kernel;->sendManifestToExperienceActivity(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONObject;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/kernel/Kernel;

.field final synthetic val$bundleUrl:Ljava/lang/String;

.field final synthetic val$kernelOptions:Lorg/json/JSONObject;

.field final synthetic val$manifest:Lorg/json/JSONObject;

.field final synthetic val$manifestUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhost/exp/exponent/kernel/Kernel;Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 0

    .line 877
    iput-object p1, p0, Lhost/exp/exponent/kernel/Kernel$7;->this$0:Lhost/exp/exponent/kernel/Kernel;

    iput-object p2, p0, Lhost/exp/exponent/kernel/Kernel$7;->val$manifestUrl:Ljava/lang/String;

    iput-object p3, p0, Lhost/exp/exponent/kernel/Kernel$7;->val$manifest:Lorg/json/JSONObject;

    iput-object p4, p0, Lhost/exp/exponent/kernel/Kernel$7;->val$bundleUrl:Ljava/lang/String;

    iput-object p5, p0, Lhost/exp/exponent/kernel/Kernel$7;->val$kernelOptions:Lorg/json/JSONObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 5

    .line 885
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$7;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-static {v0}, Lhost/exp/exponent/kernel/Kernel;->access$700(Lhost/exp/exponent/kernel/Kernel;)Lhost/exp/exponent/experience/ExperienceActivity;

    move-result-object v0

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel$7;->val$manifestUrl:Ljava/lang/String;

    iget-object v2, p0, Lhost/exp/exponent/kernel/Kernel$7;->val$manifest:Lorg/json/JSONObject;

    iget-object v3, p0, Lhost/exp/exponent/kernel/Kernel$7;->val$bundleUrl:Ljava/lang/String;

    iget-object v4, p0, Lhost/exp/exponent/kernel/Kernel$7;->val$kernelOptions:Lorg/json/JSONObject;

    invoke-virtual {v0, v1, v2, v3, v4}, Lhost/exp/exponent/experience/ExperienceActivity;->setManifest(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONObject;)V

    const-string v0, "loadBundleForExperienceActivity"

    .line 886
    invoke-static {v0}, Lhost/exp/exponent/utils/AsyncCondition;->notify(Ljava/lang/String;)V

    return-void
.end method

.method public isReady()Z
    .locals 1

    .line 880
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$7;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-static {v0}, Lhost/exp/exponent/kernel/Kernel;->access$700(Lhost/exp/exponent/kernel/Kernel;)Lhost/exp/exponent/experience/ExperienceActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$7;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-static {v0}, Lhost/exp/exponent/kernel/Kernel;->access$800(Lhost/exp/exponent/kernel/Kernel;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
