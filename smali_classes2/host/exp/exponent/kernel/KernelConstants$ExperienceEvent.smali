.class public Lhost/exp/exponent/kernel/KernelConstants$ExperienceEvent;
.super Ljava/lang/Object;
.source "KernelConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/exponent/kernel/KernelConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExperienceEvent"
.end annotation


# instance fields
.field public final eventName:Ljava/lang/String;

.field public final eventPayload:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lhost/exp/exponent/kernel/KernelConstants$ExperienceEvent;->eventName:Ljava/lang/String;

    .line 80
    iput-object p2, p0, Lhost/exp/exponent/kernel/KernelConstants$ExperienceEvent;->eventPayload:Ljava/lang/String;

    return-void
.end method
