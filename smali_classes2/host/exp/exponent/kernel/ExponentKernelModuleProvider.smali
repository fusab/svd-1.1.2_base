.class public Lhost/exp/exponent/kernel/ExponentKernelModuleProvider;
.super Ljava/lang/Object;
.source "ExponentKernelModuleProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$KernelEvent;,
        Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$ExponentKernelModuleFactory;,
        Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$KernelEventCallback;
    }
.end annotation


# static fields
.field public static sEventQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$KernelEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static sFactory:Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$ExponentKernelModuleFactory;

.field private static sInstance:Lhost/exp/exponent/kernel/ExponentKernelModuleInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$1;

    invoke-direct {v0}, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$1;-><init>()V

    sput-object v0, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider;->sFactory:Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$ExponentKernelModuleFactory;

    .line 54
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider;->sEventQueue:Ljava/util/Queue;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newInstance(Lcom/facebook/react/bridge/ReactApplicationContext;)Lhost/exp/exponent/kernel/ExponentKernelModuleInterface;
    .locals 1

    .line 37
    sget-object v0, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider;->sFactory:Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$ExponentKernelModuleFactory;

    invoke-interface {v0, p0}, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$ExponentKernelModuleFactory;->create(Lcom/facebook/react/bridge/ReactApplicationContext;)Lhost/exp/exponent/kernel/ExponentKernelModuleInterface;

    move-result-object p0

    sput-object p0, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider;->sInstance:Lhost/exp/exponent/kernel/ExponentKernelModuleInterface;

    .line 38
    sget-object p0, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider;->sInstance:Lhost/exp/exponent/kernel/ExponentKernelModuleInterface;

    return-object p0
.end method

.method public static queueEvent(Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$KernelEvent;)V
    .locals 1

    .line 61
    sget-object v0, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider;->sEventQueue:Ljava/util/Queue;

    invoke-interface {v0, p0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 63
    sget-object p0, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider;->sInstance:Lhost/exp/exponent/kernel/ExponentKernelModuleInterface;

    if-eqz p0, :cond_0

    .line 64
    invoke-interface {p0}, Lhost/exp/exponent/kernel/ExponentKernelModuleInterface;->consumeEventQueue()V

    :cond_0
    return-void
.end method

.method public static queueEvent(Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$KernelEventCallback;)V
    .locals 1

    .line 57
    new-instance v0, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$KernelEvent;

    invoke-direct {v0, p0, p1, p2}, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$KernelEvent;-><init>(Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$KernelEventCallback;)V

    invoke-static {v0}, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider;->queueEvent(Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$KernelEvent;)V

    return-void
.end method

.method public static setFactory(Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$ExponentKernelModuleFactory;)V
    .locals 0

    .line 33
    sput-object p0, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider;->sFactory:Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$ExponentKernelModuleFactory;

    return-void
.end method
