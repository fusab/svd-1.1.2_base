.class public Lhost/exp/exponent/kernel/KernelConstants;
.super Ljava/lang/Object;
.source "KernelConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhost/exp/exponent/kernel/KernelConstants$AddedExperienceEventEvent;,
        Lhost/exp/exponent/kernel/KernelConstants$ExperienceEvent;,
        Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;
    }
.end annotation


# static fields
.field public static final BUNDLE_FILE_PREFIX:Ljava/lang/String; = "cached-bundle-"

.field public static final BUNDLE_TAG:Ljava/lang/String; = "BUNDLE"

.field public static final BUNDLE_URL_KEY:Ljava/lang/String; = "bundleUrl"

.field public static final DEFAULT_APPLICATION_KEY:Ljava/lang/String; = "main"

.field public static final DELAY_TO_PRELOAD_KERNEL_JS:J = 0x1388L

.field public static final DEV_FLAG:Ljava/lang/String; = "dev_flag"

.field public static final EXPERIENCE_ID_SET_FOR_ACTIVITY_KEY:Ljava/lang/String; = "experienceIdSetForActivity"

.field public static final HOME_MANIFEST_URL:Ljava/lang/String; = ""

.field public static final HOME_MODULE_NAME:Ljava/lang/String; = "main"

.field public static final HTTP_NOT_MODIFIED:I = 0x130

.field public static final INTENT_URI_KEY:Ljava/lang/String; = "intentUri"

.field public static final IS_HEADLESS_KEY:Ljava/lang/String; = "isHeadless"

.field public static final IS_OPTIMISTIC_KEY:Ljava/lang/String; = "isOptimistic"

.field public static final KERNEL_BUNDLE_ID:Ljava/lang/String; = "kernel"

.field public static final LINKING_URI_KEY:Ljava/lang/String; = "linkingUri"

.field public static final LOAD_BUNDLE_FOR_EXPERIENCE_ACTIVITY_KEY:Ljava/lang/String; = "loadBundleForExperienceActivity"

.field public static final LOAD_FROM_CACHE_KEY:Ljava/lang/String; = "loadFromCache"

.field public static MAIN_ACTIVITY_CLASS:Ljava/lang/Class; = null

.field public static final MANIFEST_KEY:Ljava/lang/String; = "manifest"

.field public static final MANIFEST_URL_KEY:Ljava/lang/String; = "experienceUrl"

.field public static final NOTIFICATION_ACTION_TYPE_KEY:Ljava/lang/String; = "actionType"

.field public static final NOTIFICATION_ID_KEY:Ljava/lang/String; = "notification_id"

.field public static final NOTIFICATION_KEY:Ljava/lang/String; = "notification"

.field public static final NOTIFICATION_MANIFEST_URL_KEY:Ljava/lang/String; = "notificationExperienceUrl"

.field public static final NOTIFICATION_OBJECT_KEY:Ljava/lang/String; = "notification_object"

.field public static final OPEN_EXPERIENCE_ACTIVITY_KEY:Ljava/lang/String; = "openExperienceActivity"

.field public static final OPEN_OPTIMISTIC_EXPERIENCE_ACTIVITY_KEY:Ljava/lang/String; = "openOptimisticExperienceActivity"

.field public static final OPTION_LOAD_NUX_KEY:Ljava/lang/String; = "loadNux"

.field public static final OVERLAY_PERMISSION_REQUEST_CODE:I = 0x7b

.field public static final SHORTCUT_MANIFEST_URL_KEY:Ljava/lang/String; = "shortcutExperienceUrl"

.field public static final TAG:Ljava/lang/String; = "KernelConstants"


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 43
    const-class v0, Lhost/exp/exponent/experience/DetachActivity;

    sput-object v0, Lhost/exp/exponent/kernel/KernelConstants;->MAIN_ACTIVITY_CLASS:Ljava/lang/Class;

    :try_start_0
    const-string v0, "host.exp.exponent.MainActivity"

    .line 47
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lhost/exp/exponent/kernel/KernelConstants;->MAIN_ACTIVITY_CLASS:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 49
    sget-object v1, Lhost/exp/exponent/kernel/KernelConstants;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not find MainActivity, falling back to DetachActivity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
