.class Lhost/exp/exponent/kernel/Kernel$10;
.super Ljava/lang/Object;
.source "Kernel.java"

# interfaces
.implements Lhost/exp/exponent/ExponentManifest$BitmapListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/kernel/Kernel;->installShortcut(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/kernel/Kernel;

.field final synthetic val$manifestJson:Lorg/json/JSONObject;

.field final synthetic val$manifestUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhost/exp/exponent/kernel/Kernel;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 0

    .line 1134
    iput-object p1, p0, Lhost/exp/exponent/kernel/Kernel$10;->this$0:Lhost/exp/exponent/kernel/Kernel;

    iput-object p2, p0, Lhost/exp/exponent/kernel/Kernel$10;->val$manifestUrl:Ljava/lang/String;

    iput-object p3, p0, Lhost/exp/exponent/kernel/Kernel$10;->val$manifestJson:Lorg/json/JSONObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadBitmap(Landroid/graphics/Bitmap;)V
    .locals 4

    .line 1137
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel$10;->this$0:Lhost/exp/exponent/kernel/Kernel;

    iget-object v1, v1, Lhost/exp/exponent/kernel/Kernel;->mContext:Landroid/content/Context;

    const-class v2, Lhost/exp/exponent/LauncherActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.MAIN"

    .line 1138
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x20000000

    .line 1139
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1140
    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel$10;->val$manifestUrl:Ljava/lang/String;

    const-string v2, "shortcutExperienceUrl"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1142
    new-instance v1, Landroidx/core/content/pm/ShortcutInfoCompat$Builder;

    iget-object v2, p0, Lhost/exp/exponent/kernel/Kernel$10;->this$0:Lhost/exp/exponent/kernel/Kernel;

    iget-object v2, v2, Lhost/exp/exponent/kernel/Kernel;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lhost/exp/exponent/kernel/Kernel$10;->val$manifestUrl:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Landroidx/core/content/pm/ShortcutInfoCompat$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 1144
    invoke-static {p1}, Landroidx/core/graphics/drawable/IconCompat;->createWithBitmap(Landroid/graphics/Bitmap;)Landroidx/core/graphics/drawable/IconCompat;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroidx/core/content/pm/ShortcutInfoCompat$Builder;->setIcon(Landroidx/core/graphics/drawable/IconCompat;)Landroidx/core/content/pm/ShortcutInfoCompat$Builder;

    move-result-object p1

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel$10;->val$manifestJson:Lorg/json/JSONObject;

    const-string v2, "name"

    .line 1145
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroidx/core/content/pm/ShortcutInfoCompat$Builder;->setShortLabel(Ljava/lang/CharSequence;)Landroidx/core/content/pm/ShortcutInfoCompat$Builder;

    move-result-object p1

    .line 1146
    invoke-virtual {p1, v0}, Landroidx/core/content/pm/ShortcutInfoCompat$Builder;->setIntent(Landroid/content/Intent;)Landroidx/core/content/pm/ShortcutInfoCompat$Builder;

    move-result-object p1

    .line 1147
    invoke-virtual {p1}, Landroidx/core/content/pm/ShortcutInfoCompat$Builder;->build()Landroidx/core/content/pm/ShortcutInfoCompat;

    move-result-object p1

    .line 1149
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$10;->this$0:Lhost/exp/exponent/kernel/Kernel;

    iget-object v0, v0, Lhost/exp/exponent/kernel/Kernel;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroidx/core/content/pm/ShortcutManagerCompat;->requestPinShortcut(Landroid/content/Context;Landroidx/core/content/pm/ShortcutInfoCompat;Landroid/content/IntentSender;)Z

    return-void
.end method
