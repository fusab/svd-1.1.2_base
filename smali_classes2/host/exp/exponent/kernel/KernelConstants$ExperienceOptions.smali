.class public Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;
.super Ljava/lang/Object;
.source "KernelConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/exponent/kernel/KernelConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExperienceOptions"
.end annotation


# instance fields
.field public final manifestUri:Ljava/lang/String;

.field public final notification:Ljava/lang/String;

.field public final notificationObject:Lhost/exp/exponent/notifications/ExponentNotification;

.field public final uri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->manifestUri:Ljava/lang/String;

    .line 61
    iput-object p2, p0, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->uri:Ljava/lang/String;

    .line 62
    iput-object p3, p0, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->notification:Ljava/lang/String;

    const/4 p1, 0x0

    .line 63
    iput-object p1, p0, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->notificationObject:Lhost/exp/exponent/notifications/ExponentNotification;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/notifications/ExponentNotification;)V
    .locals 0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->manifestUri:Ljava/lang/String;

    .line 68
    iput-object p2, p0, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->uri:Ljava/lang/String;

    .line 69
    iput-object p3, p0, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->notification:Ljava/lang/String;

    .line 70
    iput-object p4, p0, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->notificationObject:Lhost/exp/exponent/notifications/ExponentNotification;

    return-void
.end method
