.class Lhost/exp/exponent/kernel/Kernel$8;
.super Ljava/lang/Object;
.source "Kernel.java"

# interfaces
.implements Lhost/exp/exponent/utils/AsyncCondition$AsyncConditionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/kernel/Kernel;->sendBundleToExperienceActivity(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/kernel/Kernel;

.field final synthetic val$localBundlePath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhost/exp/exponent/kernel/Kernel;Ljava/lang/String;)V
    .locals 0

    .line 892
    iput-object p1, p0, Lhost/exp/exponent/kernel/Kernel$8;->this$0:Lhost/exp/exponent/kernel/Kernel;

    iput-object p2, p0, Lhost/exp/exponent/kernel/Kernel$8;->val$localBundlePath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 2

    .line 900
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$8;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-static {v0}, Lhost/exp/exponent/kernel/Kernel;->access$700(Lhost/exp/exponent/kernel/Kernel;)Lhost/exp/exponent/experience/ExperienceActivity;

    move-result-object v0

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel$8;->val$localBundlePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhost/exp/exponent/experience/ExperienceActivity;->setBundle(Ljava/lang/String;)V

    .line 902
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$8;->this$0:Lhost/exp/exponent/kernel/Kernel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lhost/exp/exponent/kernel/Kernel;->access$702(Lhost/exp/exponent/kernel/Kernel;Lhost/exp/exponent/experience/ExperienceActivity;)Lhost/exp/exponent/experience/ExperienceActivity;

    .line 903
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$8;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-static {v0, v1}, Lhost/exp/exponent/kernel/Kernel;->access$802(Lhost/exp/exponent/kernel/Kernel;Ljava/lang/Integer;)Ljava/lang/Integer;

    return-void
.end method

.method public isReady()Z
    .locals 1

    .line 895
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$8;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-static {v0}, Lhost/exp/exponent/kernel/Kernel;->access$700(Lhost/exp/exponent/kernel/Kernel;)Lhost/exp/exponent/experience/ExperienceActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$8;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-static {v0}, Lhost/exp/exponent/kernel/Kernel;->access$800(Lhost/exp/exponent/kernel/Kernel;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
