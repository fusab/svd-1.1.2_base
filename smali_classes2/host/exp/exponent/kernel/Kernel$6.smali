.class Lhost/exp/exponent/kernel/Kernel$6;
.super Ljava/lang/Object;
.source "Kernel.java"

# interfaces
.implements Lhost/exp/exponent/utils/AsyncCondition$AsyncConditionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/kernel/Kernel;->sendLoadingScreenManifestToExperienceActivity(Lorg/json/JSONObject;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/kernel/Kernel;

.field final synthetic val$manifest:Lorg/json/JSONObject;


# direct methods
.method constructor <init>(Lhost/exp/exponent/kernel/Kernel;Lorg/json/JSONObject;)V
    .locals 0

    .line 862
    iput-object p1, p0, Lhost/exp/exponent/kernel/Kernel$6;->this$0:Lhost/exp/exponent/kernel/Kernel;

    iput-object p2, p0, Lhost/exp/exponent/kernel/Kernel$6;->val$manifest:Lorg/json/JSONObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 2

    .line 870
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$6;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-static {v0}, Lhost/exp/exponent/kernel/Kernel;->access$700(Lhost/exp/exponent/kernel/Kernel;)Lhost/exp/exponent/experience/ExperienceActivity;

    move-result-object v0

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel$6;->val$manifest:Lorg/json/JSONObject;

    invoke-virtual {v0, v1}, Lhost/exp/exponent/experience/ExperienceActivity;->setLoadingScreenManifest(Lorg/json/JSONObject;)V

    return-void
.end method

.method public isReady()Z
    .locals 1

    .line 865
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$6;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-static {v0}, Lhost/exp/exponent/kernel/Kernel;->access$700(Lhost/exp/exponent/kernel/Kernel;)Lhost/exp/exponent/experience/ExperienceActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$6;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-static {v0}, Lhost/exp/exponent/kernel/Kernel;->access$800(Lhost/exp/exponent/kernel/Kernel;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
