.class public Lhost/exp/exponent/kernel/KernelProvider;
.super Ljava/lang/Object;
.source "KernelProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhost/exp/exponent/kernel/KernelProvider$KernelFactory;
    }
.end annotation


# static fields
.field private static sFactory:Lhost/exp/exponent/kernel/KernelProvider$KernelFactory;

.field private static sInstance:Lhost/exp/exponent/kernel/KernelInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    new-instance v0, Lhost/exp/exponent/kernel/KernelProvider$1;

    invoke-direct {v0}, Lhost/exp/exponent/kernel/KernelProvider$1;-><init>()V

    sput-object v0, Lhost/exp/exponent/kernel/KernelProvider;->sFactory:Lhost/exp/exponent/kernel/KernelProvider$KernelFactory;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lhost/exp/exponent/kernel/KernelInterface;
    .locals 1

    .line 25
    sget-object v0, Lhost/exp/exponent/kernel/KernelProvider;->sInstance:Lhost/exp/exponent/kernel/KernelInterface;

    if-nez v0, :cond_0

    .line 26
    sget-object v0, Lhost/exp/exponent/kernel/KernelProvider;->sFactory:Lhost/exp/exponent/kernel/KernelProvider$KernelFactory;

    invoke-interface {v0}, Lhost/exp/exponent/kernel/KernelProvider$KernelFactory;->create()Lhost/exp/exponent/kernel/KernelInterface;

    move-result-object v0

    sput-object v0, Lhost/exp/exponent/kernel/KernelProvider;->sInstance:Lhost/exp/exponent/kernel/KernelInterface;

    .line 29
    :cond_0
    sget-object v0, Lhost/exp/exponent/kernel/KernelProvider;->sInstance:Lhost/exp/exponent/kernel/KernelInterface;

    return-object v0
.end method

.method public static setFactory(Lhost/exp/exponent/kernel/KernelProvider$KernelFactory;)V
    .locals 0

    .line 21
    sput-object p0, Lhost/exp/exponent/kernel/KernelProvider;->sFactory:Lhost/exp/exponent/kernel/KernelProvider$KernelFactory;

    return-void
.end method
