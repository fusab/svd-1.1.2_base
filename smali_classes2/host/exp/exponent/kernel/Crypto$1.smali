.class Lhost/exp/exponent/kernel/Crypto$1;
.super Ljava/lang/Object;
.source "Crypto.java"

# interfaces
.implements Lhost/exp/exponent/network/ExpoHttpCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/kernel/Crypto;->fetchPublicKeyAndVerifyPublicRSASignature(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/kernel/Crypto$RSASignatureListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/kernel/Crypto;

.field final synthetic val$cipherText:Ljava/lang/String;

.field final synthetic val$isFirstAttempt:Z

.field final synthetic val$listener:Lhost/exp/exponent/kernel/Crypto$RSASignatureListener;

.field final synthetic val$plainText:Ljava/lang/String;

.field final synthetic val$publicKeyUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhost/exp/exponent/kernel/Crypto;Lhost/exp/exponent/kernel/Crypto$RSASignatureListener;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .line 63
    iput-object p1, p0, Lhost/exp/exponent/kernel/Crypto$1;->this$0:Lhost/exp/exponent/kernel/Crypto;

    iput-object p2, p0, Lhost/exp/exponent/kernel/Crypto$1;->val$listener:Lhost/exp/exponent/kernel/Crypto$RSASignatureListener;

    iput-object p3, p0, Lhost/exp/exponent/kernel/Crypto$1;->val$plainText:Ljava/lang/String;

    iput-object p4, p0, Lhost/exp/exponent/kernel/Crypto$1;->val$cipherText:Ljava/lang/String;

    iput-boolean p5, p0, Lhost/exp/exponent/kernel/Crypto$1;->val$isFirstAttempt:Z

    iput-object p6, p0, Lhost/exp/exponent/kernel/Crypto$1;->val$publicKeyUrl:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/io/IOException;)V
    .locals 2

    .line 66
    iget-object v0, p0, Lhost/exp/exponent/kernel/Crypto$1;->val$listener:Lhost/exp/exponent/kernel/Crypto$RSASignatureListener;

    invoke-virtual {p1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lhost/exp/exponent/kernel/Crypto$RSASignatureListener;->onError(Ljava/lang/String;Z)V

    return-void
.end method

.method public onResponse(Lhost/exp/exponent/network/ExpoResponse;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "Error with RSA key."

    const-string v1, "Error verifying."

    .line 74
    :try_start_0
    iget-object v2, p0, Lhost/exp/exponent/kernel/Crypto$1;->this$0:Lhost/exp/exponent/kernel/Crypto;

    invoke-interface {p1}, Lhost/exp/exponent/network/ExpoResponse;->body()Lhost/exp/exponent/network/ExpoBody;

    move-result-object p1

    invoke-interface {p1}, Lhost/exp/exponent/network/ExpoBody;->string()Ljava/lang/String;

    move-result-object p1

    iget-object v3, p0, Lhost/exp/exponent/kernel/Crypto$1;->val$plainText:Ljava/lang/String;

    iget-object v4, p0, Lhost/exp/exponent/kernel/Crypto$1;->val$cipherText:Ljava/lang/String;

    invoke-static {v2, p1, v3, v4}, Lhost/exp/exponent/kernel/Crypto;->access$000(Lhost/exp/exponent/kernel/Crypto;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    .line 75
    iget-object v2, p0, Lhost/exp/exponent/kernel/Crypto$1;->val$listener:Lhost/exp/exponent/kernel/Crypto$RSASignatureListener;

    invoke-interface {v2, p1}, Lhost/exp/exponent/kernel/Crypto$RSASignatureListener;->onCompleted(Z)V
    :try_end_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    nop

    goto :goto_0

    :catch_1
    move-object v1, v0

    .line 93
    :goto_0
    iget-boolean p1, p0, Lhost/exp/exponent/kernel/Crypto$1;->val$isFirstAttempt:Z

    if-eqz p1, :cond_0

    .line 94
    iget-object v2, p0, Lhost/exp/exponent/kernel/Crypto$1;->this$0:Lhost/exp/exponent/kernel/Crypto;

    const/4 v3, 0x0

    iget-object v4, p0, Lhost/exp/exponent/kernel/Crypto$1;->val$publicKeyUrl:Ljava/lang/String;

    iget-object v5, p0, Lhost/exp/exponent/kernel/Crypto$1;->val$plainText:Ljava/lang/String;

    iget-object v6, p0, Lhost/exp/exponent/kernel/Crypto$1;->val$cipherText:Ljava/lang/String;

    iget-object v7, p0, Lhost/exp/exponent/kernel/Crypto$1;->val$listener:Lhost/exp/exponent/kernel/Crypto$RSASignatureListener;

    invoke-static/range {v2 .. v7}, Lhost/exp/exponent/kernel/Crypto;->access$100(Lhost/exp/exponent/kernel/Crypto;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/kernel/Crypto$RSASignatureListener;)V

    goto :goto_1

    .line 96
    :cond_0
    iget-object p1, p0, Lhost/exp/exponent/kernel/Crypto$1;->val$listener:Lhost/exp/exponent/kernel/Crypto$RSASignatureListener;

    const/4 v0, 0x0

    invoke-interface {p1, v1, v0}, Lhost/exp/exponent/kernel/Crypto$RSASignatureListener;->onError(Ljava/lang/String;Z)V

    :goto_1
    return-void
.end method
