.class Lhost/exp/exponent/kernel/Kernel$4;
.super Lhost/exp/exponent/AppLoader;
.source "Kernel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/kernel/Kernel;->openManifestUrl(Ljava/lang/String;Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;Ljava/lang/Boolean;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/kernel/Kernel;

.field final synthetic val$finalExistingTask:Landroid/app/ActivityManager$AppTask;

.field final synthetic val$manifestUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhost/exp/exponent/kernel/Kernel;Ljava/lang/String;ZLjava/lang/String;Landroid/app/ActivityManager$AppTask;)V
    .locals 0

    .line 666
    iput-object p1, p0, Lhost/exp/exponent/kernel/Kernel$4;->this$0:Lhost/exp/exponent/kernel/Kernel;

    iput-object p4, p0, Lhost/exp/exponent/kernel/Kernel$4;->val$manifestUrl:Ljava/lang/String;

    iput-object p5, p0, Lhost/exp/exponent/kernel/Kernel$4;->val$finalExistingTask:Landroid/app/ActivityManager$AppTask;

    invoke-direct {p0, p2, p3}, Lhost/exp/exponent/AppLoader;-><init>(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public emitEvent(Lorg/json/JSONObject;)V
    .locals 2

    .line 698
    invoke-static {}, Lhost/exp/exponent/kernel/Kernel;->access$600()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel$4;->val$manifestUrl:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;

    if-eqz v0, :cond_0

    .line 700
    iget-object v0, v0, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->experienceActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhost/exp/exponent/experience/ExperienceActivity;

    if-eqz v0, :cond_0

    .line 702
    invoke-virtual {v0, p1}, Lhost/exp/exponent/experience/ExperienceActivity;->emitUpdatesEvent(Lorg/json/JSONObject;)V

    :cond_0
    return-void
.end method

.method public onBundleCompleted(Ljava/lang/String;)V
    .locals 1

    .line 693
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$4;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0, p1}, Lhost/exp/exponent/kernel/Kernel;->sendBundleToExperienceActivity(Ljava/lang/String;)V

    return-void
.end method

.method public onError(Ljava/lang/Exception;)V
    .locals 1

    .line 709
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$4;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0, p1}, Lhost/exp/exponent/kernel/Kernel;->handleError(Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;)V
    .locals 1

    .line 714
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$4;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0, p1}, Lhost/exp/exponent/kernel/Kernel;->handleError(Ljava/lang/String;)V

    return-void
.end method

.method public onManifestCompleted(Lorg/json/JSONObject;)V
    .locals 2

    .line 679
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    new-instance v1, Lhost/exp/exponent/kernel/Kernel$4$2;

    invoke-direct {v1, p0, p1}, Lhost/exp/exponent/kernel/Kernel$4$2;-><init>(Lhost/exp/exponent/kernel/Kernel$4;Lorg/json/JSONObject;)V

    invoke-virtual {v0, v1}, Lhost/exp/expoview/Exponent;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onOptimisticManifest(Lorg/json/JSONObject;)V
    .locals 2

    .line 669
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    new-instance v1, Lhost/exp/exponent/kernel/Kernel$4$1;

    invoke-direct {v1, p0, p1}, Lhost/exp/exponent/kernel/Kernel$4$1;-><init>(Lhost/exp/exponent/kernel/Kernel$4;Lorg/json/JSONObject;)V

    invoke-virtual {v0, v1}, Lhost/exp/expoview/Exponent;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method
