.class public abstract Lhost/exp/exponent/kernel/services/BaseKernelService;
.super Ljava/lang/Object;
.source "BaseKernelService.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentExperienceId:Lhost/exp/exponent/kernel/ExperienceId;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 13
    iput-object v0, p0, Lhost/exp/exponent/kernel/services/BaseKernelService;->mCurrentExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    .line 16
    iput-object p1, p0, Lhost/exp/exponent/kernel/services/BaseKernelService;->mContext:Landroid/content/Context;

    .line 17
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object p1

    invoke-virtual {p1, p0}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected getContext()Landroid/content/Context;
    .locals 1

    .line 21
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/BaseKernelService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method protected getCurrentExperienceId()Lhost/exp/exponent/kernel/ExperienceId;
    .locals 1

    .line 25
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/BaseKernelService;->mCurrentExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    return-object v0
.end method

.method public onEvent(Lhost/exp/exponent/experience/BaseExperienceActivity$ExperienceBackgroundedEvent;)V
    .locals 1

    const/4 v0, 0x0

    .line 32
    iput-object v0, p0, Lhost/exp/exponent/kernel/services/BaseKernelService;->mCurrentExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    .line 33
    invoke-virtual {p1}, Lhost/exp/exponent/experience/BaseExperienceActivity$ExperienceBackgroundedEvent;->getExperienceId()Lhost/exp/exponent/kernel/ExperienceId;

    move-result-object p1

    invoke-virtual {p0, p1}, Lhost/exp/exponent/kernel/services/BaseKernelService;->onExperienceBackgrounded(Lhost/exp/exponent/kernel/ExperienceId;)V

    return-void
.end method

.method public onEvent(Lhost/exp/exponent/experience/BaseExperienceActivity$ExperienceForegroundedEvent;)V
    .locals 1

    .line 37
    invoke-virtual {p1}, Lhost/exp/exponent/experience/BaseExperienceActivity$ExperienceForegroundedEvent;->getExperienceId()Lhost/exp/exponent/kernel/ExperienceId;

    move-result-object v0

    iput-object v0, p0, Lhost/exp/exponent/kernel/services/BaseKernelService;->mCurrentExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    .line 38
    invoke-virtual {p1}, Lhost/exp/exponent/experience/BaseExperienceActivity$ExperienceForegroundedEvent;->getExperienceId()Lhost/exp/exponent/kernel/ExperienceId;

    move-result-object p1

    invoke-virtual {p0, p1}, Lhost/exp/exponent/kernel/services/BaseKernelService;->onExperienceForegrounded(Lhost/exp/exponent/kernel/ExperienceId;)V

    return-void
.end method

.method public abstract onExperienceBackgrounded(Lhost/exp/exponent/kernel/ExperienceId;)V
.end method

.method public abstract onExperienceForegrounded(Lhost/exp/exponent/kernel/ExperienceId;)V
.end method
