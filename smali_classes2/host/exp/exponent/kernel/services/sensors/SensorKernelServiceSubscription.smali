.class public Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;
.super Ljava/lang/Object;
.source "SensorKernelServiceSubscription.java"


# instance fields
.field private final mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

.field private mHasBeenReleased:Z

.field private mIsEnabled:Z

.field private final mSensorEventListener:Lhost/exp/exponent/kernel/services/sensors/SensorEventListener;

.field private final mSubscribableSensorKernelService:Lhost/exp/exponent/kernel/services/sensors/SubscribableSensorKernelService;

.field private mUpdateInterval:Ljava/lang/Long;


# direct methods
.method constructor <init>(Lhost/exp/exponent/kernel/ExperienceId;Lhost/exp/exponent/kernel/services/sensors/SubscribableSensorKernelService;Lhost/exp/exponent/kernel/services/sensors/SensorEventListener;)V
    .locals 2

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 8
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mIsEnabled:Z

    const/4 v1, 0x0

    .line 9
    iput-object v1, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mUpdateInterval:Ljava/lang/Long;

    .line 11
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mHasBeenReleased:Z

    .line 16
    iput-object p1, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    .line 17
    iput-object p3, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mSensorEventListener:Lhost/exp/exponent/kernel/services/sensors/SensorEventListener;

    .line 18
    iput-object p2, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mSubscribableSensorKernelService:Lhost/exp/exponent/kernel/services/sensors/SubscribableSensorKernelService;

    return-void
.end method

.method private assertSubscriptionIsAlive()V
    .locals 2

    .line 65
    iget-boolean v0, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mHasBeenReleased:Z

    if-nez v0, :cond_0

    return-void

    .line 66
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Subscription has been released, cannot call methods on a released subscription."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public getExperienceId()Lhost/exp/exponent/kernel/ExperienceId;
    .locals 1

    .line 34
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    return-object v0
.end method

.method getSensorEventListener()Lhost/exp/exponent/kernel/services/sensors/SensorEventListener;
    .locals 1

    .line 42
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mSensorEventListener:Lhost/exp/exponent/kernel/services/sensors/SensorEventListener;

    return-object v0
.end method

.method public getUpdateInterval()Ljava/lang/Long;
    .locals 1

    .line 38
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mUpdateInterval:Ljava/lang/Long;

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    .line 30
    iget-boolean v0, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mHasBeenReleased:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mIsEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public release()V
    .locals 1

    .line 59
    invoke-direct {p0}, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->assertSubscriptionIsAlive()V

    .line 60
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mSubscribableSensorKernelService:Lhost/exp/exponent/kernel/services/sensors/SubscribableSensorKernelService;

    invoke-virtual {v0, p0}, Lhost/exp/exponent/kernel/services/sensors/SubscribableSensorKernelService;->removeSubscription(Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;)V

    const/4 v0, 0x1

    .line 61
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mHasBeenReleased:Z

    return-void
.end method

.method public setUpdateInterval(J)V
    .locals 0

    .line 46
    invoke-direct {p0}, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->assertSubscriptionIsAlive()V

    .line 47
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mUpdateInterval:Ljava/lang/Long;

    return-void
.end method

.method public start()V
    .locals 1

    .line 22
    invoke-direct {p0}, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->assertSubscriptionIsAlive()V

    .line 23
    iget-boolean v0, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mIsEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 24
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mIsEnabled:Z

    .line 25
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mSubscribableSensorKernelService:Lhost/exp/exponent/kernel/services/sensors/SubscribableSensorKernelService;

    invoke-virtual {v0, p0}, Lhost/exp/exponent/kernel/services/sensors/SubscribableSensorKernelService;->onSubscriptionEnabledChanged(Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;)V

    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    .line 51
    invoke-direct {p0}, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->assertSubscriptionIsAlive()V

    .line 52
    iget-boolean v0, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mIsEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 53
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mIsEnabled:Z

    .line 54
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;->mSubscribableSensorKernelService:Lhost/exp/exponent/kernel/services/sensors/SubscribableSensorKernelService;

    invoke-virtual {v0, p0}, Lhost/exp/exponent/kernel/services/sensors/SubscribableSensorKernelService;->onSubscriptionEnabledChanged(Lhost/exp/exponent/kernel/services/sensors/SensorKernelServiceSubscription;)V

    :cond_0
    return-void
.end method
