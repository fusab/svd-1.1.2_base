.class public abstract Lhost/exp/exponent/kernel/services/sensors/BaseSensorKernelService;
.super Lhost/exp/exponent/kernel/services/BaseKernelService;
.source "BaseSensorKernelService.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field private mSensor:Landroid/hardware/Sensor;

.field private mSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 18
    invoke-direct {p0, p1}, Lhost/exp/exponent/kernel/services/BaseKernelService;-><init>(Landroid/content/Context;)V

    .line 19
    invoke-virtual {p0}, Lhost/exp/exponent/kernel/services/sensors/BaseSensorKernelService;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/hardware/SensorManager;

    iput-object p1, p0, Lhost/exp/exponent/kernel/services/sensors/BaseSensorKernelService;->mSensorManager:Landroid/hardware/SensorManager;

    return-void
.end method


# virtual methods
.method abstract getSensorType()I
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    return-void
.end method

.method abstract onSensorDataChanged(Landroid/hardware/SensorEvent;)V
.end method

.method protected startObserving()V
    .locals 3

    .line 29
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/sensors/BaseSensorKernelService;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {p0}, Lhost/exp/exponent/kernel/services/sensors/BaseSensorKernelService;->getSensorType()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lhost/exp/exponent/kernel/services/sensors/BaseSensorKernelService;->mSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/sensors/BaseSensorKernelService;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lhost/exp/exponent/kernel/services/sensors/BaseSensorKernelService;->mSensor:Landroid/hardware/Sensor;

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    :cond_0
    return-void
.end method

.method protected stopObserving()V
    .locals 1

    .line 35
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/sensors/BaseSensorKernelService;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    return-void
.end method
