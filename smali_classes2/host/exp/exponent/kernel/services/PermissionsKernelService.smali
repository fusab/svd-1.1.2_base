.class public Lhost/exp/exponent/kernel/services/PermissionsKernelService;
.super Lhost/exp/exponent/kernel/services/BaseKernelService;
.source "PermissionsKernelService.java"


# instance fields
.field mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhost/exp/exponent/storage/ExponentSharedPreferences;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lhost/exp/exponent/kernel/services/BaseKernelService;-><init>(Landroid/content/Context;)V

    .line 18
    iput-object p2, p0, Lhost/exp/exponent/kernel/services/PermissionsKernelService;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    return-void
.end method


# virtual methods
.method public grantPermissions(Ljava/lang/String;Lhost/exp/exponent/kernel/ExperienceId;)V
    .locals 6

    const-string v0, "permissions"

    .line 23
    :try_start_0
    iget-object v1, p0, Lhost/exp/exponent/kernel/services/PermissionsKernelService;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-virtual {p2}, Lhost/exp/exponent/kernel/ExperienceId;->get()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getExperienceMetadata(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    if-nez v1, :cond_0

    .line 25
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 29
    :cond_0
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 30
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    goto :goto_0

    .line 32
    :cond_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 36
    :goto_0
    invoke-virtual {v2, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 37
    invoke-virtual {v2, p1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    goto :goto_1

    .line 39
    :cond_2
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    :goto_1
    const-string v4, "status"

    const-string v5, "granted"

    .line 42
    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 43
    invoke-virtual {v2, p1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 44
    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 46
    iget-object p1, p0, Lhost/exp/exponent/kernel/services/PermissionsKernelService;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-virtual {p2}, Lhost/exp/exponent/kernel/ExperienceId;->get()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2, v1}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->updateExperienceMetadata(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    .line 48
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :goto_2
    return-void
.end method

.method public hasGrantedPermissions(Ljava/lang/String;Lhost/exp/exponent/kernel/ExperienceId;)Z
    .locals 5

    const-string v0, "status"

    const-string v1, "permissions"

    .line 74
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    return v3

    .line 77
    :cond_0
    iget-object v2, p0, Lhost/exp/exponent/kernel/services/PermissionsKernelService;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-virtual {p2}, Lhost/exp/exponent/kernel/ExperienceId;->get()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getExperienceMetadata(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p2

    const/4 v2, 0x0

    if-nez p2, :cond_1

    return v2

    .line 82
    :cond_1
    :try_start_0
    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 83
    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p2

    .line 84
    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 85
    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    .line 86
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 87
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "granted"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_2

    const/4 v2, 0x1

    :cond_2
    return v2

    :catch_0
    move-exception p1

    .line 91
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_3
    return v2
.end method

.method public onExperienceBackgrounded(Lhost/exp/exponent/kernel/ExperienceId;)V
    .locals 0

    return-void
.end method

.method public onExperienceForegrounded(Lhost/exp/exponent/kernel/ExperienceId;)V
    .locals 0

    return-void
.end method

.method public revokePermissions(Ljava/lang/String;Lhost/exp/exponent/kernel/ExperienceId;)V
    .locals 4

    const-string v0, "permissions"

    .line 54
    :try_start_0
    iget-object v1, p0, Lhost/exp/exponent/kernel/services/PermissionsKernelService;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-virtual {p2}, Lhost/exp/exponent/kernel/ExperienceId;->get()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getExperienceMetadata(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    .line 59
    :cond_0
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 60
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 61
    invoke-virtual {v2, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 62
    invoke-virtual {v2, p1}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 63
    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 64
    iget-object p1, p0, Lhost/exp/exponent/kernel/services/PermissionsKernelService;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-virtual {p2}, Lhost/exp/exponent/kernel/ExperienceId;->get()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2, v1}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->updateExperienceMetadata(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 68
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_1
    :goto_0
    return-void
.end method
