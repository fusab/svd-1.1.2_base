.class public Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;
.super Ljava/lang/Object;
.source "ExpoKernelServiceRegistry.java"


# instance fields
.field private mAccelerometerKernelService:Lhost/exp/exponent/kernel/services/sensors/AccelerometerKernelService;

.field private mBarometerKernelService:Lhost/exp/exponent/kernel/services/sensors/BarometerKernelService;

.field private mGravitySensorKernelService:Lhost/exp/exponent/kernel/services/sensors/GravitySensorKernelService;

.field private mGyroscopeKernelService:Lhost/exp/exponent/kernel/services/sensors/GyroscopeKernelService;

.field private mLinearAccelerationSensorKernelService:Lhost/exp/exponent/kernel/services/sensors/LinearAccelerationSensorKernelService;

.field private mLinkingKernelService:Lhost/exp/exponent/kernel/services/linking/LinkingKernelService;

.field private mMagnetometerKernelService:Lhost/exp/exponent/kernel/services/sensors/MagnetometerKernelService;

.field private mMagnetometerUncalibratedKernelService:Lhost/exp/exponent/kernel/services/sensors/MagnetometerUncalibratedKernelService;

.field private mPermissionsKernelService:Lhost/exp/exponent/kernel/services/PermissionsKernelService;

.field private mRotationVectorSensorKernelService:Lhost/exp/exponent/kernel/services/sensors/RotationVectorSensorKernelService;

.field private mSplashScreenKernelService:Lhost/exp/exponent/kernel/services/SplashScreenKernelService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhost/exp/exponent/storage/ExponentSharedPreferences;)V
    .locals 1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 19
    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mLinkingKernelService:Lhost/exp/exponent/kernel/services/linking/LinkingKernelService;

    .line 20
    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mGyroscopeKernelService:Lhost/exp/exponent/kernel/services/sensors/GyroscopeKernelService;

    .line 21
    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mMagnetometerKernelService:Lhost/exp/exponent/kernel/services/sensors/MagnetometerKernelService;

    .line 22
    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mAccelerometerKernelService:Lhost/exp/exponent/kernel/services/sensors/AccelerometerKernelService;

    .line 23
    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mBarometerKernelService:Lhost/exp/exponent/kernel/services/sensors/BarometerKernelService;

    .line 25
    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mGravitySensorKernelService:Lhost/exp/exponent/kernel/services/sensors/GravitySensorKernelService;

    .line 26
    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mRotationVectorSensorKernelService:Lhost/exp/exponent/kernel/services/sensors/RotationVectorSensorKernelService;

    .line 27
    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mLinearAccelerationSensorKernelService:Lhost/exp/exponent/kernel/services/sensors/LinearAccelerationSensorKernelService;

    .line 28
    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mMagnetometerUncalibratedKernelService:Lhost/exp/exponent/kernel/services/sensors/MagnetometerUncalibratedKernelService;

    .line 29
    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mPermissionsKernelService:Lhost/exp/exponent/kernel/services/PermissionsKernelService;

    .line 30
    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mSplashScreenKernelService:Lhost/exp/exponent/kernel/services/SplashScreenKernelService;

    .line 33
    new-instance v0, Lhost/exp/exponent/kernel/services/linking/LinkingKernelService;

    invoke-direct {v0}, Lhost/exp/exponent/kernel/services/linking/LinkingKernelService;-><init>()V

    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mLinkingKernelService:Lhost/exp/exponent/kernel/services/linking/LinkingKernelService;

    .line 34
    new-instance v0, Lhost/exp/exponent/kernel/services/sensors/GyroscopeKernelService;

    invoke-direct {v0, p1}, Lhost/exp/exponent/kernel/services/sensors/GyroscopeKernelService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mGyroscopeKernelService:Lhost/exp/exponent/kernel/services/sensors/GyroscopeKernelService;

    .line 35
    new-instance v0, Lhost/exp/exponent/kernel/services/sensors/MagnetometerKernelService;

    invoke-direct {v0, p1}, Lhost/exp/exponent/kernel/services/sensors/MagnetometerKernelService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mMagnetometerKernelService:Lhost/exp/exponent/kernel/services/sensors/MagnetometerKernelService;

    .line 36
    new-instance v0, Lhost/exp/exponent/kernel/services/sensors/AccelerometerKernelService;

    invoke-direct {v0, p1}, Lhost/exp/exponent/kernel/services/sensors/AccelerometerKernelService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mAccelerometerKernelService:Lhost/exp/exponent/kernel/services/sensors/AccelerometerKernelService;

    .line 37
    new-instance v0, Lhost/exp/exponent/kernel/services/sensors/BarometerKernelService;

    invoke-direct {v0, p1}, Lhost/exp/exponent/kernel/services/sensors/BarometerKernelService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mBarometerKernelService:Lhost/exp/exponent/kernel/services/sensors/BarometerKernelService;

    .line 38
    new-instance v0, Lhost/exp/exponent/kernel/services/sensors/GravitySensorKernelService;

    invoke-direct {v0, p1}, Lhost/exp/exponent/kernel/services/sensors/GravitySensorKernelService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mGravitySensorKernelService:Lhost/exp/exponent/kernel/services/sensors/GravitySensorKernelService;

    .line 39
    new-instance v0, Lhost/exp/exponent/kernel/services/sensors/RotationVectorSensorKernelService;

    invoke-direct {v0, p1}, Lhost/exp/exponent/kernel/services/sensors/RotationVectorSensorKernelService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mRotationVectorSensorKernelService:Lhost/exp/exponent/kernel/services/sensors/RotationVectorSensorKernelService;

    .line 40
    new-instance v0, Lhost/exp/exponent/kernel/services/sensors/LinearAccelerationSensorKernelService;

    invoke-direct {v0, p1}, Lhost/exp/exponent/kernel/services/sensors/LinearAccelerationSensorKernelService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mLinearAccelerationSensorKernelService:Lhost/exp/exponent/kernel/services/sensors/LinearAccelerationSensorKernelService;

    .line 41
    new-instance v0, Lhost/exp/exponent/kernel/services/sensors/MagnetometerUncalibratedKernelService;

    invoke-direct {v0, p1}, Lhost/exp/exponent/kernel/services/sensors/MagnetometerUncalibratedKernelService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mMagnetometerUncalibratedKernelService:Lhost/exp/exponent/kernel/services/sensors/MagnetometerUncalibratedKernelService;

    .line 42
    new-instance v0, Lhost/exp/exponent/kernel/services/PermissionsKernelService;

    invoke-direct {v0, p1, p2}, Lhost/exp/exponent/kernel/services/PermissionsKernelService;-><init>(Landroid/content/Context;Lhost/exp/exponent/storage/ExponentSharedPreferences;)V

    iput-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mPermissionsKernelService:Lhost/exp/exponent/kernel/services/PermissionsKernelService;

    .line 43
    new-instance p2, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;

    invoke-direct {p2, p1}, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mSplashScreenKernelService:Lhost/exp/exponent/kernel/services/SplashScreenKernelService;

    return-void
.end method


# virtual methods
.method public getAccelerometerKernelService()Lhost/exp/exponent/kernel/services/sensors/AccelerometerKernelService;
    .locals 1

    .line 60
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mAccelerometerKernelService:Lhost/exp/exponent/kernel/services/sensors/AccelerometerKernelService;

    return-object v0
.end method

.method public getBarometerKernelService()Lhost/exp/exponent/kernel/services/sensors/BarometerKernelService;
    .locals 1

    .line 64
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mBarometerKernelService:Lhost/exp/exponent/kernel/services/sensors/BarometerKernelService;

    return-object v0
.end method

.method public getGravitySensorKernelService()Lhost/exp/exponent/kernel/services/sensors/GravitySensorKernelService;
    .locals 1

    .line 68
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mGravitySensorKernelService:Lhost/exp/exponent/kernel/services/sensors/GravitySensorKernelService;

    return-object v0
.end method

.method public getGyroscopeKernelService()Lhost/exp/exponent/kernel/services/sensors/GyroscopeKernelService;
    .locals 1

    .line 51
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mGyroscopeKernelService:Lhost/exp/exponent/kernel/services/sensors/GyroscopeKernelService;

    return-object v0
.end method

.method public getLinearAccelerationSensorKernelService()Lhost/exp/exponent/kernel/services/sensors/LinearAccelerationSensorKernelService;
    .locals 1

    .line 76
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mLinearAccelerationSensorKernelService:Lhost/exp/exponent/kernel/services/sensors/LinearAccelerationSensorKernelService;

    return-object v0
.end method

.method public getLinkingKernelService()Lhost/exp/exponent/kernel/services/linking/LinkingKernelService;
    .locals 1

    .line 47
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mLinkingKernelService:Lhost/exp/exponent/kernel/services/linking/LinkingKernelService;

    return-object v0
.end method

.method public getMagnetometerKernelService()Lhost/exp/exponent/kernel/services/sensors/MagnetometerKernelService;
    .locals 1

    .line 56
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mMagnetometerKernelService:Lhost/exp/exponent/kernel/services/sensors/MagnetometerKernelService;

    return-object v0
.end method

.method public getMagnetometerUncalibratedKernelService()Lhost/exp/exponent/kernel/services/sensors/MagnetometerUncalibratedKernelService;
    .locals 1

    .line 80
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mMagnetometerUncalibratedKernelService:Lhost/exp/exponent/kernel/services/sensors/MagnetometerUncalibratedKernelService;

    return-object v0
.end method

.method public getPermissionsKernelService()Lhost/exp/exponent/kernel/services/PermissionsKernelService;
    .locals 1

    .line 84
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mPermissionsKernelService:Lhost/exp/exponent/kernel/services/PermissionsKernelService;

    return-object v0
.end method

.method public getRotationVectorSensorKernelService()Lhost/exp/exponent/kernel/services/sensors/RotationVectorSensorKernelService;
    .locals 1

    .line 72
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mRotationVectorSensorKernelService:Lhost/exp/exponent/kernel/services/sensors/RotationVectorSensorKernelService;

    return-object v0
.end method

.method public getSplashScreenKernelService()Lhost/exp/exponent/kernel/services/SplashScreenKernelService;
    .locals 1

    .line 88
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->mSplashScreenKernelService:Lhost/exp/exponent/kernel/services/SplashScreenKernelService;

    return-object v0
.end method
