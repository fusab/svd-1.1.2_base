.class public Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;
.super Ljava/lang/Object;
.source "ErrorRecoveryManager.java"


# static fields
.field private static final AUTO_RELOAD_BUFFER_BASE_MS:J = 0x1388L

.field private static final FIVE_MINUTES_MS:J = 0x493e0L

.field private static sExperienceIdToManager:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lhost/exp/exponent/kernel/ExperienceId;",
            "Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;",
            ">;"
        }
    .end annotation
.end field

.field private static sReloadBufferDepth:J

.field private static sTimeAnyExperienceLoaded:J


# instance fields
.field private mErrored:Z

.field private mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

.field mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mRecoveryProps:Lorg/json/JSONObject;

.field private mTimeLastLoaded:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->sExperienceIdToManager:Ljava/util/Map;

    const-wide/16 v0, 0x0

    .line 23
    sput-wide v0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->sTimeAnyExperienceLoaded:J

    .line 26
    sput-wide v0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->sReloadBufferDepth:J

    return-void
.end method

.method public constructor <init>(Lhost/exp/exponent/kernel/ExperienceId;)V
    .locals 2

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 37
    iput-wide v0, p0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->mTimeLastLoaded:J

    const/4 v0, 0x0

    .line 38
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->mErrored:Z

    .line 45
    iput-object p1, p0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    .line 46
    invoke-static {}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->getInstance()Lhost/exp/exponent/di/NativeModuleDepsProvider;

    move-result-object p1

    const-class v0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;

    invoke-virtual {p1, v0, p0}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->inject(Ljava/lang/Class;Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance(Lhost/exp/exponent/kernel/ExperienceId;)Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;
    .locals 2

    .line 29
    sget-object v0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->sExperienceIdToManager:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    sget-object v0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->sExperienceIdToManager:Ljava/util/Map;

    new-instance v1, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;

    invoke-direct {v1, p0}, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;-><init>(Lhost/exp/exponent/kernel/ExperienceId;)V

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    :cond_0
    sget-object v0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->sExperienceIdToManager:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;

    return-object p0
.end method

.method private reloadBuffer()J
    .locals 7

    .line 96
    sget-wide v0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->sReloadBufferDepth:J

    long-to-double v0, v0

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    const-wide v2, 0x40b3880000000000L    # 5000.0

    mul-double v0, v0, v2

    double-to-long v0, v0

    const-wide/32 v2, 0x493e0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 97
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v4, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->sTimeAnyExperienceLoaded:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x2

    mul-long v4, v4, v0

    cmp-long v6, v2, v4

    if-lez v6, :cond_0

    const-wide/16 v0, 0x0

    .line 100
    sput-wide v0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->sReloadBufferDepth:J

    const-wide/16 v0, 0x1388

    :cond_0
    return-wide v0
.end method


# virtual methods
.method public markErrored()V
    .locals 1

    const/4 v0, 0x1

    .line 70
    invoke-virtual {p0, v0}, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->markErrored(Z)V

    return-void
.end method

.method public markErrored(Z)V
    .locals 2

    .line 74
    iput-boolean p1, p0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->mErrored:Z

    .line 75
    iget-object v0, p0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    if-eqz v0, :cond_1

    .line 76
    iget-object v1, p0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-virtual {v0}, Lhost/exp/exponent/kernel/ExperienceId;->get()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getExperienceMetadata(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :cond_0
    :try_start_0
    const-string v1, "loadingError"

    .line 81
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 82
    iget-object p1, p0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    iget-object v1, p0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    invoke-virtual {v1}, Lhost/exp/exponent/kernel/ExperienceId;->get()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->updateExperienceMetadata(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 84
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_1
    :goto_0
    return-void
.end method

.method public markExperienceLoaded()V
    .locals 2

    .line 50
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->mTimeLastLoaded:J

    .line 51
    iget-wide v0, p0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->mTimeLastLoaded:J

    sput-wide v0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->sTimeAnyExperienceLoaded:J

    const/4 v0, 0x0

    .line 52
    invoke-virtual {p0, v0}, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->markErrored(Z)V

    return-void
.end method

.method public popRecoveryProps()Lorg/json/JSONObject;
    .locals 6

    .line 60
    iget-boolean v0, p0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->mErrored:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->mRecoveryProps:Lorg/json/JSONObject;

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 61
    :goto_0
    iget-boolean v2, p0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->mErrored:Z

    if-eqz v2, :cond_1

    .line 62
    sget-wide v2, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->sReloadBufferDepth:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    sput-wide v2, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->sReloadBufferDepth:J

    :cond_1
    const/4 v2, 0x0

    .line 64
    invoke-virtual {p0, v2}, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->markErrored(Z)V

    .line 65
    iput-object v1, p0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->mRecoveryProps:Lorg/json/JSONObject;

    return-object v0
.end method

.method public setRecoveryProps(Lorg/json/JSONObject;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->mRecoveryProps:Lorg/json/JSONObject;

    return-void
.end method

.method public shouldReloadOnError()Z
    .locals 5

    .line 90
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->mTimeLastLoaded:J

    sub-long/2addr v0, v2

    .line 91
    invoke-direct {p0}, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->reloadBuffer()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-ltz v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
