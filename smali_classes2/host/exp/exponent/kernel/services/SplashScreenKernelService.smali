.class public Lhost/exp/exponent/kernel/services/SplashScreenKernelService;
.super Lhost/exp/exponent/kernel/services/BaseKernelService;
.source "SplashScreenKernelService.java"


# instance fields
.field private mAppLoadingFinished:Z

.field private mAppLoadingStarted:Z

.field private mAutoHide:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1}, Lhost/exp/exponent/kernel/services/BaseKernelService;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    .line 11
    iput-boolean p1, p0, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;->mAutoHide:Z

    const/4 p1, 0x0

    .line 12
    iput-boolean p1, p0, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;->mAppLoadingStarted:Z

    .line 13
    iput-boolean p1, p0, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;->mAppLoadingFinished:Z

    return-void
.end method


# virtual methods
.method public isAppLoadingFinished()Z
    .locals 1

    .line 44
    iget-boolean v0, p0, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;->mAppLoadingFinished:Z

    return v0
.end method

.method public isAppLoadingStarted()Z
    .locals 1

    .line 40
    iget-boolean v0, p0, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;->mAppLoadingStarted:Z

    return v0
.end method

.method public onExperienceBackgrounded(Lhost/exp/exponent/kernel/ExperienceId;)V
    .locals 0

    return-void
.end method

.method public onExperienceContentsLoaded(Lhost/exp/exponent/kernel/ExperienceId;)V
    .locals 2

    const/4 v0, 0x1

    .line 20
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;->mAppLoadingFinished:Z

    const/4 v0, 0x0

    .line 21
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;->mAppLoadingStarted:Z

    .line 22
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lhost/exp/exponent/experience/BaseExperienceActivity$ExperienceContentLoaded;

    invoke-direct {v1, p1}, Lhost/exp/exponent/experience/BaseExperienceActivity$ExperienceContentLoaded;-><init>(Lhost/exp/exponent/kernel/ExperienceId;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    return-void
.end method

.method public onExperienceForegrounded(Lhost/exp/exponent/kernel/ExperienceId;)V
    .locals 0

    return-void
.end method

.method public preventAutoHide()V
    .locals 1

    const/4 v0, 0x1

    .line 48
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;->mAppLoadingStarted:Z

    const/4 v0, 0x0

    .line 49
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;->mAutoHide:Z

    return-void
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x1

    .line 53
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;->mAutoHide:Z

    const/4 v0, 0x0

    .line 54
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;->mAppLoadingStarted:Z

    .line 55
    iput-boolean v0, p0, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;->mAppLoadingFinished:Z

    return-void
.end method

.method public shouldAutoHide()Z
    .locals 1

    .line 36
    iget-boolean v0, p0, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;->mAutoHide:Z

    return v0
.end method
