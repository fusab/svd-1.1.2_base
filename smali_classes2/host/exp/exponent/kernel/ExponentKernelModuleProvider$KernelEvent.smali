.class public Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$KernelEvent;
.super Ljava/lang/Object;
.source "ExponentKernelModuleProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/exponent/kernel/ExponentKernelModuleProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "KernelEvent"
.end annotation


# instance fields
.field public final callback:Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$KernelEventCallback;

.field public final data:Lcom/facebook/react/bridge/WritableMap;

.field public final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$KernelEventCallback;)V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$KernelEvent;->name:Ljava/lang/String;

    .line 48
    iput-object p2, p0, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$KernelEvent;->data:Lcom/facebook/react/bridge/WritableMap;

    .line 49
    iput-object p3, p0, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$KernelEvent;->callback:Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$KernelEventCallback;

    return-void
.end method
