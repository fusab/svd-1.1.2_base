.class Lhost/exp/exponent/kernel/Kernel$3;
.super Ljava/lang/Object;
.source "Kernel.java"

# interfaces
.implements Lhost/exp/expoview/Exponent$BundleListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/kernel/Kernel;->kernelBundleListener()Lhost/exp/expoview/Exponent$BundleListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/kernel/Kernel;


# direct methods
.method constructor <init>(Lhost/exp/exponent/kernel/Kernel;)V
    .locals 0

    .line 265
    iput-object p1, p0, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBundleLoaded(Ljava/lang/String;)V
    .locals 3

    .line 268
    sget-boolean v0, Lhost/exp/expoview/ExpoViewBuildConfig;->DEBUG:Z

    if-nez v0, :cond_0

    .line 269
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    iget-object v0, v0, Lhost/exp/exponent/kernel/Kernel;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    iget-object v1, p0, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-static {v1}, Lhost/exp/exponent/kernel/Kernel;->access$200(Lhost/exp/exponent/kernel/Kernel;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "kernel_revision_id"

    invoke-virtual {v0, v2, v1}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    :cond_0
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    new-instance v1, Lhost/exp/exponent/kernel/Kernel$3$1;

    invoke-direct {v1, p0, p1}, Lhost/exp/exponent/kernel/Kernel$3$1;-><init>(Lhost/exp/exponent/kernel/Kernel$3;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lhost/exp/expoview/Exponent;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onError(Ljava/lang/Exception;)V
    .locals 3

    .line 322
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0}, Lhost/exp/exponent/kernel/Kernel;->setHasError()V

    .line 324
    sget-boolean v0, Lhost/exp/expoview/ExpoViewBuildConfig;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t load kernel. Are you sure your packager is running and your phone is on the same wifi? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lhost/exp/exponent/kernel/Kernel;->handleError(Ljava/lang/String;)V

    goto :goto_0

    .line 327
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/kernel/Kernel$3;->this$0:Lhost/exp/exponent/kernel/Kernel;

    const-string v1, "Expo requires an internet connection."

    invoke-virtual {v0, v1}, Lhost/exp/exponent/kernel/Kernel;->handleError(Ljava/lang/String;)V

    .line 328
    invoke-static {}, Lhost/exp/exponent/kernel/Kernel;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method
