.class public final enum Lhost/exp/exponent/analytics/Analytics$TimedEvent;
.super Ljava/lang/Enum;
.source "Analytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/exponent/analytics/Analytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TimedEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lhost/exp/exponent/analytics/Analytics$TimedEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lhost/exp/exponent/analytics/Analytics$TimedEvent;

.field public static final enum FINISHED_FETCHING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

.field public static final enum FINISHED_FETCHING_MANIFEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

.field public static final enum FINISHED_LOADING_REACT_NATIVE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

.field public static final enum FINISHED_MANIFEST_NETWORK_REQUEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

.field public static final enum FINISHED_WRITING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

.field public static final enum LAUNCHER_ACTIVITY_STARTED:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

.field public static final enum STARTED_FETCHING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

.field public static final enum STARTED_FETCHING_MANIFEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

.field public static final enum STARTED_LOADING_REACT_NATIVE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

.field public static final enum STARTED_MANIFEST_NETWORK_REQUEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

.field public static final enum STARTED_WRITING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 44
    new-instance v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    const/4 v1, 0x0

    const-string v2, "LAUNCHER_ACTIVITY_STARTED"

    invoke-direct {v0, v2, v1}, Lhost/exp/exponent/analytics/Analytics$TimedEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->LAUNCHER_ACTIVITY_STARTED:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    .line 45
    new-instance v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    const/4 v2, 0x1

    const-string v3, "STARTED_FETCHING_MANIFEST"

    invoke-direct {v0, v3, v2}, Lhost/exp/exponent/analytics/Analytics$TimedEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_FETCHING_MANIFEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    .line 46
    new-instance v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    const/4 v3, 0x2

    const-string v4, "STARTED_MANIFEST_NETWORK_REQUEST"

    invoke-direct {v0, v4, v3}, Lhost/exp/exponent/analytics/Analytics$TimedEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_MANIFEST_NETWORK_REQUEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    .line 47
    new-instance v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    const/4 v4, 0x3

    const-string v5, "FINISHED_MANIFEST_NETWORK_REQUEST"

    invoke-direct {v0, v5, v4}, Lhost/exp/exponent/analytics/Analytics$TimedEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_MANIFEST_NETWORK_REQUEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    .line 48
    new-instance v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    const/4 v5, 0x4

    const-string v6, "FINISHED_FETCHING_MANIFEST"

    invoke-direct {v0, v6, v5}, Lhost/exp/exponent/analytics/Analytics$TimedEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_FETCHING_MANIFEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    .line 49
    new-instance v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    const/4 v6, 0x5

    const-string v7, "STARTED_FETCHING_BUNDLE"

    invoke-direct {v0, v7, v6}, Lhost/exp/exponent/analytics/Analytics$TimedEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_FETCHING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    .line 50
    new-instance v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    const/4 v7, 0x6

    const-string v8, "FINISHED_FETCHING_BUNDLE"

    invoke-direct {v0, v8, v7}, Lhost/exp/exponent/analytics/Analytics$TimedEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_FETCHING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    .line 51
    new-instance v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    const/4 v8, 0x7

    const-string v9, "STARTED_WRITING_BUNDLE"

    invoke-direct {v0, v9, v8}, Lhost/exp/exponent/analytics/Analytics$TimedEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_WRITING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    .line 52
    new-instance v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    const/16 v9, 0x8

    const-string v10, "FINISHED_WRITING_BUNDLE"

    invoke-direct {v0, v10, v9}, Lhost/exp/exponent/analytics/Analytics$TimedEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_WRITING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    .line 53
    new-instance v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    const/16 v10, 0x9

    const-string v11, "STARTED_LOADING_REACT_NATIVE"

    invoke-direct {v0, v11, v10}, Lhost/exp/exponent/analytics/Analytics$TimedEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_LOADING_REACT_NATIVE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    .line 54
    new-instance v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    const/16 v11, 0xa

    const-string v12, "FINISHED_LOADING_REACT_NATIVE"

    invoke-direct {v0, v12, v11}, Lhost/exp/exponent/analytics/Analytics$TimedEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_LOADING_REACT_NATIVE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    const/16 v0, 0xb

    .line 43
    new-array v0, v0, [Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    sget-object v12, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->LAUNCHER_ACTIVITY_STARTED:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    aput-object v12, v0, v1

    sget-object v1, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_FETCHING_MANIFEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    aput-object v1, v0, v2

    sget-object v1, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_MANIFEST_NETWORK_REQUEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    aput-object v1, v0, v3

    sget-object v1, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_MANIFEST_NETWORK_REQUEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    aput-object v1, v0, v4

    sget-object v1, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_FETCHING_MANIFEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    aput-object v1, v0, v5

    sget-object v1, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_FETCHING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    aput-object v1, v0, v6

    sget-object v1, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_FETCHING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    aput-object v1, v0, v7

    sget-object v1, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_WRITING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    aput-object v1, v0, v8

    sget-object v1, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_WRITING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    aput-object v1, v0, v9

    sget-object v1, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_LOADING_REACT_NATIVE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    aput-object v1, v0, v10

    sget-object v1, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_LOADING_REACT_NATIVE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    aput-object v1, v0, v11

    sput-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->$VALUES:[Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhost/exp/exponent/analytics/Analytics$TimedEvent;
    .locals 1

    .line 43
    const-class v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    return-object p0
.end method

.method public static values()[Lhost/exp/exponent/analytics/Analytics$TimedEvent;
    .locals 1

    .line 43
    sget-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->$VALUES:[Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-virtual {v0}, [Lhost/exp/exponent/analytics/Analytics$TimedEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    return-object v0
.end method
