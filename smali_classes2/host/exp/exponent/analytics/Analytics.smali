.class public Lhost/exp/exponent/analytics/Analytics;
.super Ljava/lang/Object;
.source "Analytics.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhost/exp/exponent/analytics/Analytics$TimedEvent;
    }
.end annotation


# static fields
.field public static final DEVELOPER_ERROR_MESSAGE:Ljava/lang/String; = "DEVELOPER_ERROR_MESSAGE"

.field public static final ERROR_APPEARED:Ljava/lang/String; = "ERROR_APPEARED"

.field public static final ERROR_RELOADED:Ljava/lang/String; = "ERROR_RELOADED"

.field public static final ERROR_SCREEN:Ljava/lang/String; = "ERROR_SCREEN"

.field public static final EXPERIENCE_APPEARED:Ljava/lang/String; = "EXPERIENCE_APPEARED"

.field public static final HTTP_USED_CACHE_RESPONSE:Ljava/lang/String; = "HTTP_USED_CACHE_RESPONSE"

.field public static final HTTP_USED_EMBEDDED_RESPONSE:Ljava/lang/String; = "HTTP_USED_EMBEDDED_RESPONSE"

.field public static final INFO_SCREEN:Ljava/lang/String; = "INFO_SCREEN"

.field public static final LOAD_DEVELOPER_MANIFEST:Ljava/lang/String; = "LOAD_DEVELOPER_MANIFEST"

.field public static final LOAD_EXPERIENCE:Ljava/lang/String; = "LOAD_EXPERIENCE"

.field public static final MANIFEST_URL:Ljava/lang/String; = "MANIFEST_URL"

.field private static final MAX_DURATION:J = 0x7530L

.field public static final RELOAD_EXPERIENCE:Ljava/lang/String; = "RELOAD_EXPERIENCE"

.field public static final SAVE_EXPERIENCE:Ljava/lang/String; = "SAVE_EXPERIENCE"

.field public static final SAVE_EXPERIENCE_ALERT:Ljava/lang/String; = "SAVE_EXPERIENCE_ALERT"

.field public static final SAVE_EXPERIENCE_OPTION_NO:Ljava/lang/String; = "SAVE_EXPERIENCE_OPTION_NO"

.field public static final SAVE_EXPERIENCE_OPTION_YES:Ljava/lang/String; = "SAVE_EXPERIENCE_OPTION_YES"

.field public static final SDK_VERSION:Ljava/lang/String; = "SDK_VERSION"

.field private static final TAG:Ljava/lang/String; = "Analytics"

.field public static final USER_ERROR_MESSAGE:Ljava/lang/String; = "USER_ERROR_MESSAGE"

.field private static final sShellTimedEvents:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lhost/exp/exponent/analytics/Analytics$TimedEvent;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lhost/exp/exponent/analytics/Analytics;->sShellTimedEvents:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addDuration(Lorg/json/JSONObject;Ljava/lang/String;Lhost/exp/exponent/analytics/Analytics$TimedEvent;Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 175
    invoke-static {p2, p3}, Lhost/exp/exponent/analytics/Analytics;->getDuration(Lhost/exp/exponent/analytics/Analytics$TimedEvent;Lhost/exp/exponent/analytics/Analytics$TimedEvent;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 176
    invoke-static {p2, p3}, Lhost/exp/exponent/analytics/Analytics;->getDuration(Lhost/exp/exponent/analytics/Analytics$TimedEvent;Lhost/exp/exponent/analytics/Analytics$TimedEvent;)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    return-void
.end method

.method public static clearTimedEvents()V
    .locals 1

    .line 163
    sget-object v0, Lhost/exp/exponent/analytics/Analytics;->sShellTimedEvents:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method private static getDuration(Lhost/exp/exponent/analytics/Analytics$TimedEvent;Lhost/exp/exponent/analytics/Analytics$TimedEvent;)Ljava/lang/Long;
    .locals 2

    .line 167
    sget-object v0, Lhost/exp/exponent/analytics/Analytics;->sShellTimedEvents:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lhost/exp/exponent/analytics/Analytics;->sShellTimedEvents:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 171
    :cond_0
    sget-object v0, Lhost/exp/exponent/analytics/Analytics;->sShellTimedEvents:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object p0, Lhost/exp/exponent/analytics/Analytics;->sShellTimedEvents:Ljava/util/Map;

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide p0

    sub-long/2addr v0, p0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    return-object p0

    :cond_1
    :goto_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static initializeAmplitude(Landroid/content/Context;Landroid/app/Application;)V
    .locals 2

    .line 64
    sget-boolean v0, Lhost/exp/exponent/Constants;->ANALYTICS_ENABLED:Z

    if-nez v0, :cond_0

    return-void

    .line 68
    :cond_0
    invoke-static {}, Lhost/exp/exponent/analytics/Analytics;->resetAmplitudeDatabaseHelper()V

    .line 71
    :try_start_0
    invoke-static {}, Lcom/amplitude/api/Amplitude;->getInstance()Lcom/amplitude/api/AmplitudeClient;

    move-result-object v0

    sget-boolean v1, Lhost/exp/expoview/ExpoViewBuildConfig;->DEBUG:Z

    if-eqz v1, :cond_1

    const-string v1, "39c2521e973c2fe323143f1b1c4cde74"

    goto :goto_0

    :cond_1
    const-string v1, "1b9250eab537ead4557ae2e38b02f9d9"

    :goto_0
    invoke-virtual {v0, p0, v1}, Lcom/amplitude/api/AmplitudeClient;->initialize(Landroid/content/Context;Ljava/lang/String;)Lcom/amplitude/api/AmplitudeClient;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    .line 73
    invoke-static {p0}, Lhost/exp/exponent/analytics/EXL;->testError(Ljava/lang/Throwable;)V

    :goto_1
    if-eqz p1, :cond_2

    .line 77
    invoke-static {}, Lcom/amplitude/api/Amplitude;->getInstance()Lcom/amplitude/api/AmplitudeClient;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/amplitude/api/AmplitudeClient;->enableForegroundTracking(Landroid/app/Application;)Lcom/amplitude/api/AmplitudeClient;

    .line 80
    :cond_2
    :try_start_1
    new-instance p0, Lorg/json/JSONObject;

    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    const-string p1, "INITIAL_URL"

    .line 81
    sget-object v0, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "ABI_VERSIONS"

    .line 82
    sget-object v0, Lhost/exp/exponent/Constants;->ABI_VERSIONS:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "TEMPORARY_ABI_VERSION"

    const-string v0, "35.0.0"

    .line 83
    invoke-virtual {p0, p1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 84
    invoke-static {}, Lcom/amplitude/api/Amplitude;->getInstance()Lcom/amplitude/api/AmplitudeClient;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/amplitude/api/AmplitudeClient;->setUserProperties(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception p0

    .line 86
    sget-object p1, Lhost/exp/exponent/analytics/Analytics;->TAG:Ljava/lang/String;

    invoke-static {p1, p0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_2
    return-void
.end method

.method public static logEvent(Ljava/lang/String;)V
    .locals 1

    .line 91
    sget-boolean v0, Lhost/exp/exponent/Constants;->ANALYTICS_ENABLED:Z

    if-nez v0, :cond_0

    return-void

    .line 94
    :cond_0
    invoke-static {}, Lcom/amplitude/api/Amplitude;->getInstance()Lcom/amplitude/api/AmplitudeClient;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/amplitude/api/AmplitudeClient;->logEvent(Ljava/lang/String;)V

    return-void
.end method

.method public static logEvent(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 1

    .line 98
    sget-boolean v0, Lhost/exp/exponent/Constants;->ANALYTICS_ENABLED:Z

    if-nez v0, :cond_0

    return-void

    .line 101
    :cond_0
    invoke-static {}, Lcom/amplitude/api/Amplitude;->getInstance()Lcom/amplitude/api/AmplitudeClient;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/amplitude/api/AmplitudeClient;->logEvent(Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method public static logEventWithManifestUrl(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 105
    invoke-static {p0, p1, v0}, Lhost/exp/exponent/analytics/Analytics;->logEventWithManifestUrlSdkVersion(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static logEventWithManifestUrlSdkVersion(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 109
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "LOAD_EXPERIENCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 113
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    if-eqz p1, :cond_1

    const-string v1, "MANIFEST_URL"

    .line 115
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    if-eqz p2, :cond_2

    const-string p1, "SDK_VERSION"

    .line 118
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 120
    :cond_2
    invoke-static {p0, v0}, Lhost/exp/exponent/analytics/Analytics;->logEvent(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 122
    sget-object p1, Lhost/exp/exponent/analytics/Analytics;->TAG:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-static {p1, p0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public static markEvent(Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V
    .locals 3

    .line 127
    sget-object v0, Lhost/exp/exponent/analytics/Analytics;->sShellTimedEvents:Ljava/util/Map;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static resetAmplitudeDatabaseHelper()V
    .locals 2

    :try_start_0
    const-string v0, "com.amplitude.api.DatabaseHelper"

    .line 182
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "instance"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x1

    .line 183
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    const/4 v1, 0x0

    .line 184
    invoke-virtual {v0, v1, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 186
    sget-object v1, Lhost/exp/exponent/analytics/Analytics;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public static sendTimedEvents(Ljava/lang/String;)V
    .locals 5

    if-nez p0, :cond_0

    return-void

    .line 136
    :cond_0
    :try_start_0
    sget-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_LOADING_REACT_NATIVE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    sget-object v1, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->LAUNCHER_ACTIVITY_STARTED:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0, v1}, Lhost/exp/exponent/analytics/Analytics;->getDuration(Lhost/exp/exponent/analytics/Analytics$TimedEvent;Lhost/exp/exponent/analytics/Analytics$TimedEvent;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 137
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x7530

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    goto :goto_1

    .line 142
    :cond_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "TOTAL_DURATION"

    .line 143
    sget-object v2, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_LOADING_REACT_NATIVE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    sget-object v3, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->LAUNCHER_ACTIVITY_STARTED:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0, v1, v2, v3}, Lhost/exp/exponent/analytics/Analytics;->addDuration(Lorg/json/JSONObject;Ljava/lang/String;Lhost/exp/exponent/analytics/Analytics$TimedEvent;Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    const-string v1, "LAUNCH_TO_MANIFEST_START_DURATION"

    .line 144
    sget-object v2, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_FETCHING_MANIFEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    sget-object v3, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->LAUNCHER_ACTIVITY_STARTED:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0, v1, v2, v3}, Lhost/exp/exponent/analytics/Analytics;->addDuration(Lorg/json/JSONObject;Ljava/lang/String;Lhost/exp/exponent/analytics/Analytics$TimedEvent;Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    const-string v1, "MANIFEST_TOTAL_DURATION"

    .line 145
    sget-object v2, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_FETCHING_MANIFEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    sget-object v3, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_FETCHING_MANIFEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0, v1, v2, v3}, Lhost/exp/exponent/analytics/Analytics;->addDuration(Lorg/json/JSONObject;Ljava/lang/String;Lhost/exp/exponent/analytics/Analytics$TimedEvent;Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    const-string v1, "MANIFEST_NETWORK_DURATION"

    .line 146
    sget-object v2, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_MANIFEST_NETWORK_REQUEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    sget-object v3, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_MANIFEST_NETWORK_REQUEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0, v1, v2, v3}, Lhost/exp/exponent/analytics/Analytics;->addDuration(Lorg/json/JSONObject;Ljava/lang/String;Lhost/exp/exponent/analytics/Analytics$TimedEvent;Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    const-string v1, "BUNDLE_FETCH_DURATION"

    .line 147
    sget-object v2, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_FETCHING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    sget-object v3, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_FETCHING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0, v1, v2, v3}, Lhost/exp/exponent/analytics/Analytics;->addDuration(Lorg/json/JSONObject;Ljava/lang/String;Lhost/exp/exponent/analytics/Analytics$TimedEvent;Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    const-string v1, "BUNDLE_WRITE_DURATION"

    .line 148
    sget-object v2, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_WRITING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    sget-object v3, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_WRITING_BUNDLE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0, v1, v2, v3}, Lhost/exp/exponent/analytics/Analytics;->addDuration(Lorg/json/JSONObject;Ljava/lang/String;Lhost/exp/exponent/analytics/Analytics$TimedEvent;Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    const-string v1, "REACT_NATIVE_DURATION"

    .line 149
    sget-object v2, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_LOADING_REACT_NATIVE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    sget-object v3, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_LOADING_REACT_NATIVE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0, v1, v2, v3}, Lhost/exp/exponent/analytics/Analytics;->addDuration(Lorg/json/JSONObject;Ljava/lang/String;Lhost/exp/exponent/analytics/Analytics$TimedEvent;Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    const-string v1, "MANIFEST_URL"

    .line 151
    invoke-virtual {v0, v1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 153
    sget-object v1, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    const-string p0, "SHELL_EXPERIENCE_LOADED"

    goto :goto_0

    :cond_2
    const-string p0, "EXPERIENCE_LOADED"

    .line 154
    :goto_0
    invoke-static {p0, v0}, Lhost/exp/exponent/analytics/Analytics;->logEvent(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_2

    .line 138
    :cond_3
    :goto_1
    sget-object p0, Lhost/exp/exponent/analytics/Analytics;->sShellTimedEvents:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->clear()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 158
    sget-object p0, Lhost/exp/exponent/analytics/Analytics;->sShellTimedEvents:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->clear()V

    return-void

    :catchall_0
    move-exception p0

    goto :goto_3

    :catch_0
    move-exception p0

    .line 156
    :try_start_1
    sget-object v0, Lhost/exp/exponent/analytics/Analytics;->TAG:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 158
    :goto_2
    sget-object p0, Lhost/exp/exponent/analytics/Analytics;->sShellTimedEvents:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->clear()V

    return-void

    :goto_3
    sget-object v0, Lhost/exp/exponent/analytics/Analytics;->sShellTimedEvents:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 159
    throw p0
.end method
