.class public abstract Lhost/exp/exponent/AppLoader;
.super Ljava/lang/Object;
.source "AppLoader.java"


# static fields
.field private static final DEFAULT_TIMEOUT_LENGTH:I = 0x7530

.field private static final DEFAULT_TIMEOUT_LENGTH_BEFORE_SDK26:I = 0x0

.field private static final TAG:Ljava/lang/String; = "AppLoader"

.field public static final UPDATES_EVENT_NAME:Ljava/lang/String; = "Exponent.nativeUpdatesEvent"

.field public static final UPDATE_DOWNLOAD_FINISHED_EVENT:Ljava/lang/String; = "downloadFinished"

.field public static final UPDATE_DOWNLOAD_PROGRESS_EVENT:Ljava/lang/String; = "downloadProgress"

.field public static final UPDATE_DOWNLOAD_START_EVENT:Ljava/lang/String; = "downloadStart"

.field public static final UPDATE_ERROR_EVENT:Ljava/lang/String; = "error"

.field public static final UPDATE_NO_UPDATE_AVAILABLE_EVENT:Ljava/lang/String; = "noUpdateAvailable"


# instance fields
.field private hasResolved:Z

.field private mCachedManifest:Lorg/json/JSONObject;

.field mExpoHandler:Lhost/exp/exponent/ExpoHandler;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mExponentManifest:Lhost/exp/exponent/ExponentManifest;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mLocalBundlePath:Ljava/lang/String;

.field private mManifest:Lorg/json/JSONObject;

.field private mManifestUrl:Ljava/lang/String;

.field private mRunnable:Ljava/lang/Runnable;

.field private final mUseCacheOnly:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 52
    invoke-direct {p0, p1, v0}, Lhost/exp/exponent/AppLoader;-><init>(Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 37
    iput-boolean v0, p0, Lhost/exp/exponent/AppLoader;->hasResolved:Z

    .line 56
    invoke-static {}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->getInstance()Lhost/exp/exponent/di/NativeModuleDepsProvider;

    move-result-object v0

    const-class v1, Lhost/exp/exponent/AppLoader;

    invoke-virtual {v0, v1, p0}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->inject(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 58
    iput-object p1, p0, Lhost/exp/exponent/AppLoader;->mManifestUrl:Ljava/lang/String;

    .line 59
    iput-boolean p2, p0, Lhost/exp/exponent/AppLoader;->mUseCacheOnly:Z

    .line 60
    new-instance p1, Lhost/exp/exponent/AppLoader$1;

    invoke-direct {p1, p0}, Lhost/exp/exponent/AppLoader$1;-><init>(Lhost/exp/exponent/AppLoader;)V

    iput-object p1, p0, Lhost/exp/exponent/AppLoader;->mRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lhost/exp/exponent/AppLoader;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Lhost/exp/exponent/AppLoader;->resolve()V

    return-void
.end method

.method static synthetic access$1000(Lhost/exp/exponent/AppLoader;Z)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lhost/exp/exponent/AppLoader;->fetchJSBundle(Z)V

    return-void
.end method

.method static synthetic access$102(Lhost/exp/exponent/AppLoader;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 0

    .line 20
    iput-object p1, p0, Lhost/exp/exponent/AppLoader;->mManifest:Lorg/json/JSONObject;

    return-object p1
.end method

.method static synthetic access$1102(Lhost/exp/exponent/AppLoader;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 20
    iput-object p1, p0, Lhost/exp/exponent/AppLoader;->mLocalBundlePath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1200(Lhost/exp/exponent/AppLoader;)Ljava/lang/String;
    .locals 0

    .line 20
    iget-object p0, p0, Lhost/exp/exponent/AppLoader;->mManifestUrl:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lhost/exp/exponent/AppLoader;ZZ)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Lhost/exp/exponent/AppLoader;->fetchJSBundle(ZZ)V

    return-void
.end method

.method static synthetic access$300(Lhost/exp/exponent/AppLoader;Ljava/lang/Exception;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lhost/exp/exponent/AppLoader;->resolve(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$400(Lhost/exp/exponent/AppLoader;)Lorg/json/JSONObject;
    .locals 0

    .line 20
    iget-object p0, p0, Lhost/exp/exponent/AppLoader;->mCachedManifest:Lorg/json/JSONObject;

    return-object p0
.end method

.method static synthetic access$402(Lhost/exp/exponent/AppLoader;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 0

    .line 20
    iput-object p1, p0, Lhost/exp/exponent/AppLoader;->mCachedManifest:Lorg/json/JSONObject;

    return-object p1
.end method

.method static synthetic access$500(Lhost/exp/exponent/AppLoader;)Z
    .locals 0

    .line 20
    iget-boolean p0, p0, Lhost/exp/exponent/AppLoader;->mUseCacheOnly:Z

    return p0
.end method

.method static synthetic access$600(Lhost/exp/exponent/AppLoader;I)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lhost/exp/exponent/AppLoader;->startTimerAndFetchRemoteManifest(I)V

    return-void
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    .line 20
    sget-object v0, Lhost/exp/exponent/AppLoader;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lhost/exp/exponent/AppLoader;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Lhost/exp/exponent/AppLoader;->startTimerAndFetchRemoteManifest()V

    return-void
.end method

.method static synthetic access$900(Lhost/exp/exponent/AppLoader;)Z
    .locals 0

    .line 20
    iget-boolean p0, p0, Lhost/exp/exponent/AppLoader;->hasResolved:Z

    return p0
.end method

.method private fetchJSBundle(Z)V
    .locals 1

    const/4 v0, 0x0

    .line 263
    invoke-direct {p0, p1, v0}, Lhost/exp/exponent/AppLoader;->fetchJSBundle(ZZ)V

    return-void
.end method

.method private fetchJSBundle(ZZ)V
    .locals 13

    const-string v0, "bundleUrl"

    .line 268
    iget-object v1, p0, Lhost/exp/exponent/AppLoader;->mManifest:Lorg/json/JSONObject;

    invoke-static {v1}, Lhost/exp/exponent/ExponentManifest;->isDebugModeEnabled(Lorg/json/JSONObject;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p1, ""

    .line 269
    iput-object p1, p0, Lhost/exp/exponent/AppLoader;->mLocalBundlePath:Ljava/lang/String;

    .line 270
    invoke-direct {p0}, Lhost/exp/exponent/AppLoader;->resolve()V

    return-void

    :cond_0
    const/4 v1, 0x0

    .line 277
    :try_start_0
    iget-object v2, p0, Lhost/exp/exponent/AppLoader;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    iget-object v3, p0, Lhost/exp/exponent/AppLoader;->mManifestUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getManifest(Ljava/lang/String;)Lhost/exp/exponent/storage/ExponentSharedPreferences$ManifestAndBundleUrl;

    move-result-object v2

    iget-object v2, v2, Lhost/exp/exponent/storage/ExponentSharedPreferences$ManifestAndBundleUrl;->manifest:Lorg/json/JSONObject;

    .line 278
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    goto/16 :goto_2

    .line 280
    :catch_1
    :try_start_1
    sget-object v2, Lhost/exp/exponent/AppLoader;->TAG:Ljava/lang/String;

    const-string v3, "Couldn\'t get old manifest from shared preferences"

    invoke-static {v2, v3}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 285
    :goto_0
    :try_start_2
    iget-object v2, p0, Lhost/exp/exponent/AppLoader;->mManifest:Lorg/json/JSONObject;

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 286
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    .line 287
    :goto_1
    iget-object v1, p0, Lhost/exp/exponent/AppLoader;->mManifest:Lorg/json/JSONObject;

    const-string v2, "id"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 288
    iget-object v2, p0, Lhost/exp/exponent/AppLoader;->mManifest:Lorg/json/JSONObject;

    const-string v3, "sdkVersion"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 290
    iget-object v11, p0, Lhost/exp/exponent/AppLoader;->mManifest:Lorg/json/JSONObject;

    .line 292
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v3

    iget-object v4, p0, Lhost/exp/exponent/AppLoader;->mManifest:Lorg/json/JSONObject;

    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v6

    invoke-virtual {v6, v1}, Lhost/exp/expoview/Exponent;->encodeExperienceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v12, Lhost/exp/exponent/AppLoader$5;

    move-object v6, v12

    move-object v7, p0

    move v8, p2

    move v9, p1

    move v10, v0

    invoke-direct/range {v6 .. v11}, Lhost/exp/exponent/AppLoader$5;-><init>(Lhost/exp/exponent/AppLoader;ZZZLorg/json/JSONObject;)V

    move-object v6, v1

    move-object v7, v2

    move-object v8, v12

    move v9, v0

    move v10, p1

    invoke-virtual/range {v3 .. v10}, Lhost/exp/expoview/Exponent;->loadJSBundle(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/expoview/Exponent$BundleListener;ZZ)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_3

    :catch_2
    move-exception p1

    .line 348
    :try_start_3
    sget-object p2, Lhost/exp/exponent/AppLoader;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Couldn\'t load bundle: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    invoke-direct {p0, p1}, Lhost/exp/exponent/AppLoader;->resolve(Ljava/lang/Exception;)V

    goto :goto_3

    :catch_3
    move-exception p1

    .line 344
    sget-object p2, Lhost/exp/exponent/AppLoader;->TAG:Ljava/lang/String;

    invoke-static {p2, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 345
    invoke-direct {p0, p1}, Lhost/exp/exponent/AppLoader;->resolve(Ljava/lang/Exception;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_3

    .line 352
    :goto_2
    sget-object p2, Lhost/exp/exponent/AppLoader;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Couldn\'t load manifest: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    invoke-direct {p0, p1}, Lhost/exp/exponent/AppLoader;->resolve(Ljava/lang/Exception;)V

    :goto_3
    return-void
.end method

.method private resolve()V
    .locals 1

    const/4 v0, 0x0

    .line 215
    invoke-direct {p0, v0}, Lhost/exp/exponent/AppLoader;->resolve(Ljava/lang/Exception;)V

    return-void
.end method

.method private resolve(Ljava/lang/Exception;)V
    .locals 3

    .line 219
    iget-boolean v0, p0, Lhost/exp/exponent/AppLoader;->hasResolved:Z

    if-eqz v0, :cond_0

    return-void

    .line 223
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/AppLoader;->mManifest:Lorg/json/JSONObject;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhost/exp/exponent/AppLoader;->mLocalBundlePath:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 224
    invoke-direct {p0}, Lhost/exp/exponent/AppLoader;->stopTimer()V

    .line 225
    iput-boolean v1, p0, Lhost/exp/exponent/AppLoader;->hasResolved:Z

    .line 229
    :try_start_0
    iget-object p1, p0, Lhost/exp/exponent/AppLoader;->mManifest:Lorg/json/JSONObject;

    const-string v0, "bundleUrl"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lhost/exp/exponent/kernel/ExponentUrls;->toHttp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235
    sget-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_FETCHING_MANIFEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0}, Lhost/exp/exponent/analytics/Analytics;->markEvent(Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    .line 237
    iget-object v0, p0, Lhost/exp/exponent/AppLoader;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    iget-object v1, p0, Lhost/exp/exponent/AppLoader;->mManifestUrl:Ljava/lang/String;

    iget-object v2, p0, Lhost/exp/exponent/AppLoader;->mManifest:Lorg/json/JSONObject;

    invoke-virtual {v0, v1, v2, p1}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->updateManifest(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lhost/exp/exponent/AppLoader;->mManifestUrl:Ljava/lang/String;

    iget-object v1, p0, Lhost/exp/exponent/AppLoader;->mManifest:Lorg/json/JSONObject;

    invoke-static {v0, v1, p1}, Lhost/exp/exponent/storage/ExponentDB;->saveExperience(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 240
    iget-object p1, p0, Lhost/exp/exponent/AppLoader;->mManifest:Lorg/json/JSONObject;

    invoke-virtual {p0, p1}, Lhost/exp/exponent/AppLoader;->onManifestCompleted(Lorg/json/JSONObject;)V

    .line 243
    iget-object p1, p0, Lhost/exp/exponent/AppLoader;->mManifest:Lorg/json/JSONObject;

    invoke-static {p1}, Lhost/exp/exponent/ExponentManifest;->isDebugModeEnabled(Lorg/json/JSONObject;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 244
    iget-object p1, p0, Lhost/exp/exponent/AppLoader;->mLocalBundlePath:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lhost/exp/exponent/AppLoader;->onBundleCompleted(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception p1

    .line 231
    invoke-virtual {p0, p1}, Lhost/exp/exponent/AppLoader;->onError(Ljava/lang/Exception;)V

    return-void

    .line 246
    :cond_1
    iget-object v0, p0, Lhost/exp/exponent/AppLoader;->mCachedManifest:Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    .line 247
    iput-object v0, p0, Lhost/exp/exponent/AppLoader;->mManifest:Lorg/json/JSONObject;

    const/4 p1, 0x0

    .line 249
    iput-object p1, p0, Lhost/exp/exponent/AppLoader;->mCachedManifest:Lorg/json/JSONObject;

    .line 250
    invoke-direct {p0, v1}, Lhost/exp/exponent/AppLoader;->fetchJSBundle(Z)V

    goto :goto_0

    .line 252
    :cond_2
    iput-boolean v1, p0, Lhost/exp/exponent/AppLoader;->hasResolved:Z

    if-eqz p1, :cond_3

    .line 254
    sget-object v0, Lhost/exp/exponent/AppLoader;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not load app: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lhost/exp/exponent/exceptions/ExceptionUtils;->exceptionToErrorMessage(Ljava/lang/Exception;)Lhost/exp/exponent/kernel/ExponentErrorMessage;

    move-result-object v2

    invoke-virtual {v2}, Lhost/exp/exponent/kernel/ExponentErrorMessage;->developerErrorMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    invoke-virtual {p0, p1}, Lhost/exp/exponent/AppLoader;->onError(Ljava/lang/Exception;)V

    goto :goto_0

    .line 257
    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Could not load request from "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lhost/exp/exponent/AppLoader;->mManifestUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ": the request timed out"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lhost/exp/exponent/AppLoader;->onError(Ljava/lang/String;)V

    :cond_4
    :goto_0
    return-void
.end method

.method private startTimerAndFetchRemoteManifest()V
    .locals 1

    const/16 v0, 0x7530

    .line 176
    invoke-direct {p0, v0}, Lhost/exp/exponent/AppLoader;->startTimerAndFetchRemoteManifest(I)V

    return-void
.end method

.method private startTimerAndFetchRemoteManifest(I)V
    .locals 4

    .line 180
    iget-object v0, p0, Lhost/exp/exponent/AppLoader;->mExpoHandler:Lhost/exp/exponent/ExpoHandler;

    iget-object v1, p0, Lhost/exp/exponent/AppLoader;->mRunnable:Ljava/lang/Runnable;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lhost/exp/exponent/ExpoHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 182
    invoke-virtual {p0}, Lhost/exp/exponent/AppLoader;->fetchRemoteManifest()V

    return-void
.end method

.method private stopTimer()V
    .locals 2

    .line 211
    iget-object v0, p0, Lhost/exp/exponent/AppLoader;->mExpoHandler:Lhost/exp/exponent/ExpoHandler;

    iget-object v1, p0, Lhost/exp/exponent/AppLoader;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lhost/exp/exponent/ExpoHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public abstract emitEvent(Lorg/json/JSONObject;)V
.end method

.method public fetchRemoteManifest()V
    .locals 3

    .line 186
    iget-object v0, p0, Lhost/exp/exponent/AppLoader;->mExponentManifest:Lhost/exp/exponent/ExponentManifest;

    iget-object v1, p0, Lhost/exp/exponent/AppLoader;->mManifestUrl:Ljava/lang/String;

    new-instance v2, Lhost/exp/exponent/AppLoader$4;

    invoke-direct {v2, p0}, Lhost/exp/exponent/AppLoader$4;-><init>(Lhost/exp/exponent/AppLoader;)V

    invoke-virtual {v0, v1, v2}, Lhost/exp/exponent/ExponentManifest;->fetchManifest(Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$ManifestListener;)V

    return-void
.end method

.method public abstract onBundleCompleted(Ljava/lang/String;)V
.end method

.method public abstract onError(Ljava/lang/Exception;)V
.end method

.method public abstract onError(Ljava/lang/String;)V
.end method

.method public abstract onManifestCompleted(Lorg/json/JSONObject;)V
.end method

.method public abstract onOptimisticManifest(Lorg/json/JSONObject;)V
.end method

.method public start()V
    .locals 3

    .line 70
    sget-boolean v0, Lhost/exp/exponent/Constants;->ARE_REMOTE_UPDATES_ENABLED:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lhost/exp/expoview/ExpoViewBuildConfig;->DEBUG:Z

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lhost/exp/exponent/AppLoader;->mExponentManifest:Lhost/exp/exponent/ExponentManifest;

    iget-object v1, p0, Lhost/exp/exponent/AppLoader;->mManifestUrl:Ljava/lang/String;

    new-instance v2, Lhost/exp/exponent/AppLoader$2;

    invoke-direct {v2, p0}, Lhost/exp/exponent/AppLoader$2;-><init>(Lhost/exp/exponent/AppLoader;)V

    invoke-virtual {v0, v1, v2}, Lhost/exp/exponent/ExponentManifest;->fetchEmbeddedManifest(Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$ManifestListener;)V

    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/AppLoader;->mExponentManifest:Lhost/exp/exponent/ExponentManifest;

    iget-object v1, p0, Lhost/exp/exponent/AppLoader;->mManifestUrl:Ljava/lang/String;

    new-instance v2, Lhost/exp/exponent/AppLoader$3;

    invoke-direct {v2, p0}, Lhost/exp/exponent/AppLoader$3;-><init>(Lhost/exp/exponent/AppLoader;)V

    invoke-virtual {v0, v1, v2}, Lhost/exp/exponent/ExponentManifest;->fetchCachedManifest(Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$ManifestListener;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 159
    invoke-virtual {p0}, Lhost/exp/exponent/AppLoader;->fetchRemoteManifest()V

    :cond_1
    return-void
.end method
