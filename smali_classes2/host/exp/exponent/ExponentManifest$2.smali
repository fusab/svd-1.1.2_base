.class Lhost/exp/exponent/ExponentManifest$2;
.super Ljava/lang/Object;
.source "ExponentManifest.java"

# interfaces
.implements Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/ExponentManifest;->fetchManifest(Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$ManifestListener;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/ExponentManifest;

.field final synthetic val$finalUri:Ljava/lang/String;

.field final synthetic val$listener:Lhost/exp/exponent/ExponentManifest$ManifestListener;

.field final synthetic val$manifestUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhost/exp/exponent/ExponentManifest;Lhost/exp/exponent/ExponentManifest$ManifestListener;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 235
    iput-object p1, p0, Lhost/exp/exponent/ExponentManifest$2;->this$0:Lhost/exp/exponent/ExponentManifest;

    iput-object p2, p0, Lhost/exp/exponent/ExponentManifest$2;->val$listener:Lhost/exp/exponent/ExponentManifest$ManifestListener;

    iput-object p3, p0, Lhost/exp/exponent/ExponentManifest$2;->val$manifestUrl:Ljava/lang/String;

    iput-object p4, p0, Lhost/exp/exponent/ExponentManifest$2;->val$finalUri:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCachedResponse(Lhost/exp/exponent/network/ExpoResponse;Z)V
    .locals 7

    .line 254
    iget-object v0, p0, Lhost/exp/exponent/ExponentManifest$2;->this$0:Lhost/exp/exponent/ExponentManifest;

    iget-object v2, p0, Lhost/exp/exponent/ExponentManifest$2;->val$manifestUrl:Ljava/lang/String;

    iget-object v3, p0, Lhost/exp/exponent/ExponentManifest$2;->val$finalUri:Ljava/lang/String;

    iget-object v4, p0, Lhost/exp/exponent/ExponentManifest$2;->val$listener:Lhost/exp/exponent/ExponentManifest$ManifestListener;

    const/4 v6, 0x1

    move-object v1, p1

    move v5, p2

    invoke-static/range {v0 .. v6}, Lhost/exp/exponent/ExponentManifest;->access$000(Lhost/exp/exponent/ExponentManifest;Lhost/exp/exponent/network/ExpoResponse;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$ManifestListener;ZZ)V

    return-void
.end method

.method public onFailure(Ljava/io/IOException;)V
    .locals 3

    .line 238
    iget-object v0, p0, Lhost/exp/exponent/ExponentManifest$2;->val$listener:Lhost/exp/exponent/ExponentManifest$ManifestListener;

    new-instance v1, Lhost/exp/exponent/exceptions/ManifestException;

    iget-object v2, p0, Lhost/exp/exponent/ExponentManifest$2;->val$manifestUrl:Ljava/lang/String;

    invoke-direct {v1, p1, v2}, Lhost/exp/exponent/exceptions/ManifestException;-><init>(Ljava/lang/Exception;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lhost/exp/exponent/ExponentManifest$ManifestListener;->onError(Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lhost/exp/exponent/network/ExpoResponse;)V
    .locals 8

    .line 245
    invoke-interface {p1}, Lhost/exp/exponent/network/ExpoResponse;->networkResponse()Lhost/exp/exponent/network/ExpoResponse;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    const/4 v7, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 248
    :goto_0
    iget-object v1, p0, Lhost/exp/exponent/ExponentManifest$2;->this$0:Lhost/exp/exponent/ExponentManifest;

    iget-object v3, p0, Lhost/exp/exponent/ExponentManifest$2;->val$manifestUrl:Ljava/lang/String;

    iget-object v4, p0, Lhost/exp/exponent/ExponentManifest$2;->val$finalUri:Ljava/lang/String;

    iget-object v5, p0, Lhost/exp/exponent/ExponentManifest$2;->val$listener:Lhost/exp/exponent/ExponentManifest$ManifestListener;

    const/4 v6, 0x0

    move-object v2, p1

    invoke-static/range {v1 .. v7}, Lhost/exp/exponent/ExponentManifest;->access$000(Lhost/exp/exponent/ExponentManifest;Lhost/exp/exponent/network/ExpoResponse;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$ManifestListener;ZZ)V

    return-void
.end method
