.class public Lhost/exp/exponent/di/NativeModuleDepsProvider;
.super Ljava/lang/Object;
.source "NativeModuleDepsProvider.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "NativeModuleDepsProvider"

.field private static sInstance:Lhost/exp/exponent/di/NativeModuleDepsProvider; = null

.field private static sUseTestInstance:Z = false


# instance fields
.field mApplicationContext:Landroid/app/Application;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mClassesToInjectedObjects:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field mContext:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mCrypto:Lhost/exp/exponent/kernel/Crypto;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mExpoHandler:Lhost/exp/exponent/ExpoHandler;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mExponentManifest:Lhost/exp/exponent/ExponentManifest;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mKernelServiceRegistry:Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;)V
    .locals 5

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mClassesToInjectedObjects:Ljava/util/Map;

    .line 55
    iput-object p1, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mContext:Landroid/content/Context;

    .line 56
    iput-object p1, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mApplicationContext:Landroid/app/Application;

    .line 57
    new-instance p1, Lhost/exp/exponent/ExpoHandler;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p1, v0}, Lhost/exp/exponent/ExpoHandler;-><init>(Landroid/os/Handler;)V

    iput-object p1, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mExpoHandler:Lhost/exp/exponent/ExpoHandler;

    .line 58
    new-instance p1, Lhost/exp/exponent/storage/ExponentSharedPreferences;

    iget-object v0, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mContext:Landroid/content/Context;

    invoke-direct {p1, v0}, Lhost/exp/exponent/storage/ExponentSharedPreferences;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    .line 59
    new-instance p1, Lhost/exp/exponent/network/ExponentNetwork;

    iget-object v0, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-direct {p1, v0, v1}, Lhost/exp/exponent/network/ExponentNetwork;-><init>(Landroid/content/Context;Lhost/exp/exponent/storage/ExponentSharedPreferences;)V

    iput-object p1, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;

    .line 60
    new-instance p1, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;

    iget-object v0, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-direct {p1, v0, v1}, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;-><init>(Landroid/content/Context;Lhost/exp/exponent/storage/ExponentSharedPreferences;)V

    iput-object p1, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mKernelServiceRegistry:Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;

    .line 61
    new-instance p1, Lhost/exp/exponent/kernel/Crypto;

    iget-object v0, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;

    invoke-direct {p1, v0}, Lhost/exp/exponent/kernel/Crypto;-><init>(Lhost/exp/exponent/network/ExponentNetwork;)V

    iput-object p1, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mCrypto:Lhost/exp/exponent/kernel/Crypto;

    .line 62
    new-instance p1, Lhost/exp/exponent/ExponentManifest;

    iget-object v0, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;

    iget-object v2, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mCrypto:Lhost/exp/exponent/kernel/Crypto;

    iget-object v3, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-direct {p1, v0, v1, v2, v3}, Lhost/exp/exponent/ExponentManifest;-><init>(Landroid/content/Context;Lhost/exp/exponent/network/ExponentNetwork;Lhost/exp/exponent/kernel/Crypto;Lhost/exp/exponent/storage/ExponentSharedPreferences;)V

    iput-object p1, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mExponentManifest:Lhost/exp/exponent/ExponentManifest;

    .line 64
    const-class p1, Lhost/exp/exponent/di/NativeModuleDepsProvider;

    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object p1

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p1, v1

    .line 65
    const-class v3, Ljavax/inject/Inject;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 67
    :try_start_0
    iget-object v3, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mClassesToInjectedObjects:Ljava/util/Map;

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v2, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    .line 69
    sget-object v3, Lhost/exp/exponent/di/NativeModuleDepsProvider;->TAG:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static getInstance()Lhost/exp/exponent/di/NativeModuleDepsProvider;
    .locals 1

    .line 91
    sget-object v0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->sInstance:Lhost/exp/exponent/di/NativeModuleDepsProvider;

    return-object v0
.end method

.method public static initialize(Landroid/app/Application;)V
    .locals 1

    .line 79
    sget-boolean v0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->sUseTestInstance:Z

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Lhost/exp/exponent/di/NativeModuleDepsProvider;

    invoke-direct {v0, p0}, Lhost/exp/exponent/di/NativeModuleDepsProvider;-><init>(Landroid/app/Application;)V

    sput-object v0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->sInstance:Lhost/exp/exponent/di/NativeModuleDepsProvider;

    :cond_0
    return-void
.end method

.method private injectField(Ljava/lang/Object;Ljava/lang/reflect/Field;)V
    .locals 2

    .line 105
    const-class v0, Ljavax/inject/Inject;

    invoke-virtual {p2, v0}, Ljava/lang/reflect/Field;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    .line 107
    iget-object v1, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mClassesToInjectedObjects:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    iget-object v1, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mClassesToInjectedObjects:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x1

    .line 113
    :try_start_0
    invoke-virtual {p2, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 114
    invoke-virtual {p2, p1, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 116
    sget-object p2, Lhost/exp/exponent/di/NativeModuleDepsProvider;->TAG:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/IllegalAccessException;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 108
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NativeModuleDepsProvider could not find object for class "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method public static setTestInstance(Lhost/exp/exponent/di/NativeModuleDepsProvider;)V
    .locals 0

    .line 86
    sput-object p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->sInstance:Lhost/exp/exponent/di/NativeModuleDepsProvider;

    const/4 p0, 0x1

    .line 87
    sput-boolean p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->sUseTestInstance:Z

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 1

    .line 95
    iget-object v0, p0, Lhost/exp/exponent/di/NativeModuleDepsProvider;->mClassesToInjectedObjects:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public inject(Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 3

    .line 99
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object p1

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    .line 100
    invoke-direct {p0, p2, v2}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->injectField(Ljava/lang/Object;Ljava/lang/reflect/Field;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
