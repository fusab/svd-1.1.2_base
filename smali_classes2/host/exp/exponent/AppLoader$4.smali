.class Lhost/exp/exponent/AppLoader$4;
.super Ljava/lang/Object;
.source "AppLoader.java"

# interfaces
.implements Lhost/exp/exponent/ExponentManifest$ManifestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/AppLoader;->fetchRemoteManifest()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/AppLoader;


# direct methods
.method constructor <init>(Lhost/exp/exponent/AppLoader;)V
    .locals 0

    .line 186
    iput-object p1, p0, Lhost/exp/exponent/AppLoader$4;->this$0:Lhost/exp/exponent/AppLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lorg/json/JSONObject;)V
    .locals 1

    .line 189
    iget-object v0, p0, Lhost/exp/exponent/AppLoader$4;->this$0:Lhost/exp/exponent/AppLoader;

    invoke-static {v0, p1}, Lhost/exp/exponent/AppLoader;->access$102(Lhost/exp/exponent/AppLoader;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    .line 192
    invoke-static {p1}, Lhost/exp/exponent/ExponentManifest;->isDebugModeEnabled(Lorg/json/JSONObject;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhost/exp/exponent/AppLoader$4;->this$0:Lhost/exp/exponent/AppLoader;

    invoke-static {v0}, Lhost/exp/exponent/AppLoader;->access$900(Lhost/exp/exponent/AppLoader;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 193
    iget-object v0, p0, Lhost/exp/exponent/AppLoader$4;->this$0:Lhost/exp/exponent/AppLoader;

    invoke-virtual {v0, p1}, Lhost/exp/exponent/AppLoader;->onOptimisticManifest(Lorg/json/JSONObject;)V

    .line 195
    :cond_0
    iget-object p1, p0, Lhost/exp/exponent/AppLoader$4;->this$0:Lhost/exp/exponent/AppLoader;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lhost/exp/exponent/AppLoader;->access$1000(Lhost/exp/exponent/AppLoader;Z)V

    return-void
.end method

.method public onError(Ljava/lang/Exception;)V
    .locals 1

    .line 200
    iget-object v0, p0, Lhost/exp/exponent/AppLoader$4;->this$0:Lhost/exp/exponent/AppLoader;

    invoke-static {v0, p1}, Lhost/exp/exponent/AppLoader;->access$300(Lhost/exp/exponent/AppLoader;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;)V
    .locals 2

    .line 205
    iget-object v0, p0, Lhost/exp/exponent/AppLoader$4;->this$0:Lhost/exp/exponent/AppLoader;

    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lhost/exp/exponent/AppLoader;->access$300(Lhost/exp/exponent/AppLoader;Ljava/lang/Exception;)V

    return-void
.end method
