.class Lhost/exp/exponent/ExponentManifest$4;
.super Ljava/lang/Object;
.source "ExponentManifest.java"

# interfaces
.implements Lhost/exp/exponent/kernel/Crypto$RSASignatureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/ExponentManifest;->fetchManifestStep2(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/network/ExpoHeaders;Lhost/exp/exponent/ExponentManifest$ManifestListener;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/ExponentManifest;

.field final synthetic val$finalManifest:Lorg/json/JSONObject;

.field final synthetic val$isOffline:Z

.field final synthetic val$listener:Lhost/exp/exponent/ExponentManifest$ManifestListener;

.field final synthetic val$manifestUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhost/exp/exponent/ExponentManifest;ZLjava/lang/String;Lorg/json/JSONObject;Lhost/exp/exponent/ExponentManifest$ManifestListener;)V
    .locals 0

    .line 495
    iput-object p1, p0, Lhost/exp/exponent/ExponentManifest$4;->this$0:Lhost/exp/exponent/ExponentManifest;

    iput-boolean p2, p0, Lhost/exp/exponent/ExponentManifest$4;->val$isOffline:Z

    iput-object p3, p0, Lhost/exp/exponent/ExponentManifest$4;->val$manifestUrl:Ljava/lang/String;

    iput-object p4, p0, Lhost/exp/exponent/ExponentManifest$4;->val$finalManifest:Lorg/json/JSONObject;

    iput-object p5, p0, Lhost/exp/exponent/ExponentManifest$4;->val$listener:Lhost/exp/exponent/ExponentManifest$ManifestListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Z)V
    .locals 4

    .line 510
    iget-object v0, p0, Lhost/exp/exponent/ExponentManifest$4;->this$0:Lhost/exp/exponent/ExponentManifest;

    iget-object v1, p0, Lhost/exp/exponent/ExponentManifest$4;->val$manifestUrl:Ljava/lang/String;

    iget-object v2, p0, Lhost/exp/exponent/ExponentManifest$4;->val$finalManifest:Lorg/json/JSONObject;

    iget-object v3, p0, Lhost/exp/exponent/ExponentManifest$4;->val$listener:Lhost/exp/exponent/ExponentManifest$ManifestListener;

    invoke-static {v0, v1, v2, p1, v3}, Lhost/exp/exponent/ExponentManifest;->access$100(Lhost/exp/exponent/ExponentManifest;Ljava/lang/String;Lorg/json/JSONObject;ZLhost/exp/exponent/ExponentManifest$ManifestListener;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Z)V
    .locals 3

    .line 498
    iget-boolean v0, p0, Lhost/exp/exponent/ExponentManifest$4;->val$isOffline:Z

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 501
    iget-object p1, p0, Lhost/exp/exponent/ExponentManifest$4;->this$0:Lhost/exp/exponent/ExponentManifest;

    iget-object p2, p0, Lhost/exp/exponent/ExponentManifest$4;->val$manifestUrl:Ljava/lang/String;

    iget-object v0, p0, Lhost/exp/exponent/ExponentManifest$4;->val$finalManifest:Lorg/json/JSONObject;

    const/4 v1, 0x1

    iget-object v2, p0, Lhost/exp/exponent/ExponentManifest$4;->val$listener:Lhost/exp/exponent/ExponentManifest$ManifestListener;

    invoke-static {p1, p2, v0, v1, v2}, Lhost/exp/exponent/ExponentManifest;->access$100(Lhost/exp/exponent/ExponentManifest;Ljava/lang/String;Lorg/json/JSONObject;ZLhost/exp/exponent/ExponentManifest$ManifestListener;)V

    goto :goto_0

    .line 503
    :cond_0
    invoke-static {}, Lhost/exp/exponent/ExponentManifest;->access$200()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    iget-object p1, p0, Lhost/exp/exponent/ExponentManifest$4;->this$0:Lhost/exp/exponent/ExponentManifest;

    iget-object p2, p0, Lhost/exp/exponent/ExponentManifest$4;->val$manifestUrl:Ljava/lang/String;

    iget-object v0, p0, Lhost/exp/exponent/ExponentManifest$4;->val$finalManifest:Lorg/json/JSONObject;

    const/4 v1, 0x0

    iget-object v2, p0, Lhost/exp/exponent/ExponentManifest$4;->val$listener:Lhost/exp/exponent/ExponentManifest$ManifestListener;

    invoke-static {p1, p2, v0, v1, v2}, Lhost/exp/exponent/ExponentManifest;->access$100(Lhost/exp/exponent/ExponentManifest;Ljava/lang/String;Lorg/json/JSONObject;ZLhost/exp/exponent/ExponentManifest$ManifestListener;)V

    :goto_0
    return-void
.end method
