.class public Lhost/exp/exponent/generated/ExponentBuildConstants;
.super Ljava/lang/Object;
.source "ExponentBuildConstants.java"


# static fields
.field public static final BUILD_MACHINE_KERNEL_MANIFEST:Ljava/lang/String; = "{\"android\":{\"package\":\"host.exp.exponent\"},\"dependencies\":[\"@expo/react-native-action-sheet\",\"@expo/react-native-touchable-native-feedback-safe\",\"@react-navigation/web\",\"apollo-boost\",\"apollo-cache-inmemory\",\"dedent\",\"es6-error\",\"expo\",\"expo-analytics-amplitude\",\"expo-asset\",\"expo-barcode-scanner\",\"expo-blur\",\"expo-camera\",\"expo-constants\",\"expo-font\",\"expo-linear-gradient\",\"expo-location\",\"expo-permissions\",\"expo-task-manager\",\"expo-web-browser\",\"graphql\",\"graphql-tag\",\"immutable\",\"lodash\",\"prop-types\",\"querystring\",\"react\",\"react-apollo\",\"react-native\",\"react-native-appearance\",\"react-native-fade-in-image\",\"react-native-gesture-handler\",\"react-native-infinite-scroll-view\",\"react-native-maps\",\"react-navigation\",\"react-navigation-material-bottom-tabs\",\"react-navigation-stack\",\"react-navigation-tabs\",\"react-redux\",\"redux\",\"redux-thunk\",\"semver\",\"sha1\",\"url\"],\"description\":\"\",\"extra\":{\"amplitudeApiKey\":\"081e5ec53f869b440b225d5e40ec73f9\"},\"icon\":\"https://s3.amazonaws.com/exp-brand-assets/ExponentEmptyManifest_192.png\",\"iconUrl\":\"https://s3.amazonaws.com/exp-brand-assets/ExponentEmptyManifest_192.png\",\"ios\":{\"bundleIdentifier\":\"host.exp.exponent\",\"supportsTablet\":true},\"locales\":{},\"name\":\"expo-home\",\"orientation\":\"portrait\",\"packagerOpts\":{\"config\":\"metro.config.js\"},\"platforms\":[\"ios\",\"android\"],\"primaryColor\":\"#cccccc\",\"privacy\":\"unlisted\",\"scheme\":\"exp\",\"sdkVersion\":\"UNVERSIONED\",\"slug\":\"expo-home-dev-4207c5b7dad576285280451b76c6b0fdd7ad99fe\",\"updates\":{\"checkAutomatically\":\"ON_LOAD\",\"fallbackToCacheTimeout\":0},\"version\":\"35.0.0\",\"id\":\"@expo-home-dev/expo-home-dev-4207c5b7dad576285280451b76c6b0fdd7ad99fe\",\"revisionId\":\"35.0.0-r.z6BOjJRda8\",\"publishedTime\":\"2019-09-17T17:10:35.846Z\",\"commitTime\":\"2019-09-17T17:10:35.892Z\",\"bundleUrl\":\"https://d1wp6m56sqw74a.cloudfront.net/%40expo-home-dev%2Fexpo-home-dev-4207c5b7dad576285280451b76c6b0fdd7ad99fe%2F35.0.0%2Ffb6e6731bafa66b1c5b253b2789e02eb-35.0.0-android.js\",\"releaseChannel\":\"default\",\"hostUri\":\"expo.io/@expo-home-dev/expo-home-dev-4207c5b7dad576285280451b76c6b0fdd7ad99fe\"}"

.field public static final BUILD_MACHINE_LOCAL_HOSTNAME:Ljava/lang/String; = "Charless-MacBook-Pro.local"

.field public static final INITIAL_URL:Ljava/lang/String; = null

.field public static final TEMPORARY_SDK_VERSION:Ljava/lang/String; = "35.0.0"

.field public static final TEST_APP_URI:Ljava/lang/String; = ""

.field public static final TEST_CONFIG:Ljava/lang/String; = ""

.field public static final TEST_RUN_ID:Ljava/lang/String; = "6d08d3ca-08d2-4228-beb1-c05c1d393a27"

.field public static final TEST_SERVER_URL:Ljava/lang/String; = "TODO"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
