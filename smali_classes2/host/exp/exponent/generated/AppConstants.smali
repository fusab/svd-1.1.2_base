.class public Lhost/exp/exponent/generated/AppConstants;
.super Ljava/lang/Object;
.source "AppConstants.java"


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation


# static fields
.field public static ARE_REMOTE_UPDATES_ENABLED:Z = true

.field public static final EMBEDDED_RESPONSES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lhost/exp/exponent/Constants$EmbeddedResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static FCM_ENABLED:Z = false

.field public static INITIAL_URL:Ljava/lang/String; = "exp://exp.host/@epavon-bemobile/svd-app"

.field public static final IS_DETACHED:Z = true

.field public static final RELEASE_CHANNEL:Ljava/lang/String; = "1.1.2"

.field public static final SHELL_APP_SCHEME:Ljava/lang/String; = "expc7499babb3a64ec8bd8919052329a05a"

.field public static SHOW_LOADING_VIEW_IN_SHELL_APP:Z = false

.field public static final VERSION_NAME:Ljava/lang/String; = "2.6.4"


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 125
    new-instance v1, Lhost/exp/exponent/Constants$EmbeddedResponse;

    const-string v2, "https://exp.host/@epavon-bemobile/svd-app"

    const-string v3, "assets://shell-app-manifest.json"

    const-string v4, "application/json"

    invoke-direct {v1, v2, v3, v4}, Lhost/exp/exponent/Constants$EmbeddedResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    new-instance v1, Lhost/exp/exponent/Constants$EmbeddedResponse;

    const-string v2, "https://d1wp6m56sqw74a.cloudfront.net/%40epavon-bemobile%2Fsvd-app%2F0.1.0%2F2d13d847576d92ac926e00448e410a75-35.0.0-android.js"

    const-string v3, "assets://shell-app.bundle"

    const-string v4, "application/javascript"

    invoke-direct {v1, v2, v3, v4}, Lhost/exp/exponent/Constants$EmbeddedResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    .line 130
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhost/exp/exponent/Constants$EmbeddedResponse;

    iget-object v2, v2, Lhost/exp/exponent/Constants$EmbeddedResponse;->url:Ljava/lang/String;

    const-string v3, "index.exp"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 132
    new-instance v2, Lhost/exp/exponent/Constants$EmbeddedResponse;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 133
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lhost/exp/exponent/Constants$EmbeddedResponse;

    iget-object v4, v4, Lhost/exp/exponent/Constants$EmbeddedResponse;->url:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "/index.exp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 134
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lhost/exp/exponent/Constants$EmbeddedResponse;

    iget-object v4, v4, Lhost/exp/exponent/Constants$EmbeddedResponse;->responseFilePath:Ljava/lang/String;

    .line 135
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhost/exp/exponent/Constants$EmbeddedResponse;

    iget-object v1, v1, Lhost/exp/exponent/Constants$EmbeddedResponse;->mediaType:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v1}, Lhost/exp/exponent/Constants$EmbeddedResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    :cond_0
    sput-object v0, Lhost/exp/exponent/generated/AppConstants;->EMBEDDED_RESPONSES:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get()Lhost/exp/exponent/Constants$ExpoViewAppConstants;
    .locals 2

    .line 144
    new-instance v0, Lhost/exp/exponent/Constants$ExpoViewAppConstants;

    invoke-direct {v0}, Lhost/exp/exponent/Constants$ExpoViewAppConstants;-><init>()V

    const-string v1, "2.6.4"

    .line 145
    iput-object v1, v0, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->VERSION_NAME:Ljava/lang/String;

    .line 146
    sget-object v1, Lhost/exp/exponent/generated/AppConstants;->INITIAL_URL:Ljava/lang/String;

    iput-object v1, v0, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->INITIAL_URL:Ljava/lang/String;

    const/4 v1, 0x1

    .line 147
    iput-boolean v1, v0, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->IS_DETACHED:Z

    const-string v1, "expc7499babb3a64ec8bd8919052329a05a"

    .line 148
    iput-object v1, v0, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->SHELL_APP_SCHEME:Ljava/lang/String;

    const-string v1, "1.1.2"

    .line 149
    iput-object v1, v0, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->RELEASE_CHANNEL:Ljava/lang/String;

    .line 150
    sget-boolean v1, Lhost/exp/exponent/generated/AppConstants;->SHOW_LOADING_VIEW_IN_SHELL_APP:Z

    iput-boolean v1, v0, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->SHOW_LOADING_VIEW_IN_SHELL_APP:Z

    .line 151
    sget-boolean v1, Lhost/exp/exponent/generated/AppConstants;->ARE_REMOTE_UPDATES_ENABLED:Z

    iput-boolean v1, v0, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->ARE_REMOTE_UPDATES_ENABLED:Z

    .line 152
    sget-object v1, Lhost/exp/exponent/generated/AppConstants;->EMBEDDED_RESPONSES:Ljava/util/List;

    iput-object v1, v0, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->EMBEDDED_RESPONSES:Ljava/util/List;

    const v1, 0x71facc56

    .line 153
    iput v1, v0, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->ANDROID_VERSION_CODE:I

    .line 154
    sget-boolean v1, Lhost/exp/exponent/generated/AppConstants;->FCM_ENABLED:Z

    iput-boolean v1, v0, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->FCM_ENABLED:Z

    return-object v0
.end method
