.class Lhost/exp/exponent/experience/ExperienceActivity$7;
.super Ljava/lang/Object;
.source "ExperienceActivity.java"

# interfaces
.implements Lhost/exp/exponent/utils/AsyncCondition$AsyncConditionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/experience/ExperienceActivity;->addNuxView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/experience/ExperienceActivity;


# direct methods
.method constructor <init>(Lhost/exp/exponent/experience/ExperienceActivity;)V
    .locals 0

    .line 684
    iput-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity$7;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 4

    .line 692
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$7;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v0, v0, Lhost/exp/exponent/experience/ExperienceActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0}, Lhost/exp/exponent/kernel/Kernel;->getReactInstanceManager()Lcom/facebook/react/ReactInstanceManager;

    move-result-object v0

    .line 693
    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity$7;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    new-instance v2, Lversioned/host/exp/exponent/ReactUnthemedRootView;

    invoke-direct {v2, v1}, Lversioned/host/exp/exponent/ReactUnthemedRootView;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Lhost/exp/exponent/experience/ExperienceActivity;->access$902(Lhost/exp/exponent/experience/ExperienceActivity;Lversioned/host/exp/exponent/ReactUnthemedRootView;)Lversioned/host/exp/exponent/ReactUnthemedRootView;

    .line 694
    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity$7;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-static {v1}, Lhost/exp/exponent/experience/ExperienceActivity;->access$900(Lhost/exp/exponent/experience/ExperienceActivity;)Lversioned/host/exp/exponent/ReactUnthemedRootView;

    move-result-object v1

    const-string v2, "ExperienceNuxApp"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lversioned/host/exp/exponent/ReactUnthemedRootView;->startReactApplication(Lcom/facebook/react/ReactInstanceManager;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 699
    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity$7;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-virtual {v0, v1, v1}, Lcom/facebook/react/ReactInstanceManager;->onHostResume(Landroid/app/Activity;Lcom/facebook/react/modules/core/DefaultHardwareBackBtnHandler;)V

    .line 700
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$7;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-static {v0}, Lhost/exp/exponent/experience/ExperienceActivity;->access$900(Lhost/exp/exponent/experience/ExperienceActivity;)Lversioned/host/exp/exponent/ReactUnthemedRootView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhost/exp/exponent/experience/ExperienceActivity;->addView(Landroid/view/View;)V

    const-string v0, "NUX_EXPERIENCE_OVERLAY_SHOWN"

    .line 701
    invoke-static {v0}, Lhost/exp/exponent/analytics/Analytics;->logEvent(Ljava/lang/String;)V

    return-void
.end method

.method public isReady()Z
    .locals 1

    .line 687
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$7;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v0, v0, Lhost/exp/exponent/experience/ExperienceActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0}, Lhost/exp/exponent/kernel/Kernel;->isRunning()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
