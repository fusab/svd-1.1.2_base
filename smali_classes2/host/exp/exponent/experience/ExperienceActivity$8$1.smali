.class Lhost/exp/exponent/experience/ExperienceActivity$8$1;
.super Ljava/lang/Object;
.source "ExperienceActivity.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/experience/ExperienceActivity$8;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lhost/exp/exponent/experience/ExperienceActivity$8;


# direct methods
.method constructor <init>(Lhost/exp/exponent/experience/ExperienceActivity$8;)V
    .locals 0

    .line 728
    iput-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity$8$1;->this$1:Lhost/exp/exponent/experience/ExperienceActivity$8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .line 730
    iget-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity$8$1;->this$1:Lhost/exp/exponent/experience/ExperienceActivity$8;

    iget-object p1, p1, Lhost/exp/exponent/experience/ExperienceActivity$8;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$8$1;->this$1:Lhost/exp/exponent/experience/ExperienceActivity$8;

    iget-object v0, v0, Lhost/exp/exponent/experience/ExperienceActivity$8;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-static {v0}, Lhost/exp/exponent/experience/ExperienceActivity;->access$900(Lhost/exp/exponent/experience/ExperienceActivity;)Lversioned/host/exp/exponent/ReactUnthemedRootView;

    move-result-object v0

    invoke-virtual {p1, v0}, Lhost/exp/exponent/experience/ExperienceActivity;->removeViewFromParent(Landroid/view/View;)V

    .line 731
    iget-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity$8$1;->this$1:Lhost/exp/exponent/experience/ExperienceActivity$8;

    iget-object p1, p1, Lhost/exp/exponent/experience/ExperienceActivity$8;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lhost/exp/exponent/experience/ExperienceActivity;->access$902(Lhost/exp/exponent/experience/ExperienceActivity;Lversioned/host/exp/exponent/ReactUnthemedRootView;)Lversioned/host/exp/exponent/ReactUnthemedRootView;

    .line 733
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v0, "IS_FROM_NOTIFICATION"

    .line 735
    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity$8$1;->this$1:Lhost/exp/exponent/experience/ExperienceActivity$8;

    iget-boolean v1, v1, Lhost/exp/exponent/experience/ExperienceActivity$8;->val$isFromNotification:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 737
    invoke-static {}, Lhost/exp/exponent/experience/ExperienceActivity;->access$1000()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string v0, "NUX_EXPERIENCE_OVERLAY_DISMISSED"

    .line 739
    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/Analytics;->logEvent(Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method
