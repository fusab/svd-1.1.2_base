.class Lhost/exp/exponent/experience/ExperienceActivity$5;
.super Ljava/lang/Object;
.source "ExperienceActivity.java"

# interfaces
.implements Lhost/exp/exponent/utils/AsyncCondition$AsyncConditionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/experience/ExperienceActivity;->setBundle(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/experience/ExperienceActivity;

.field final synthetic val$finalIsReadyForBundle:Z

.field final synthetic val$localBundlePath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhost/exp/exponent/experience/ExperienceActivity;ZLjava/lang/String;)V
    .locals 0

    .line 487
    iput-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity$5;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iput-boolean p2, p0, Lhost/exp/exponent/experience/ExperienceActivity$5;->val$finalIsReadyForBundle:Z

    iput-object p3, p0, Lhost/exp/exponent/experience/ExperienceActivity$5;->val$localBundlePath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 2

    .line 495
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$5;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-static {v0}, Lhost/exp/exponent/experience/ExperienceActivity;->access$200(Lhost/exp/exponent/experience/ExperienceActivity;)Lhost/exp/exponent/notifications/ExponentNotification;

    move-result-object v1

    invoke-static {v0, v1}, Lhost/exp/exponent/experience/ExperienceActivity;->access$102(Lhost/exp/exponent/experience/ExperienceActivity;Lhost/exp/exponent/notifications/ExponentNotification;)Lhost/exp/exponent/notifications/ExponentNotification;

    .line 496
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$5;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lhost/exp/exponent/experience/ExperienceActivity;->access$202(Lhost/exp/exponent/experience/ExperienceActivity;Lhost/exp/exponent/notifications/ExponentNotification;)Lhost/exp/exponent/notifications/ExponentNotification;

    .line 497
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$5;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity$5;->val$localBundlePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhost/exp/exponent/experience/ExperienceActivity;->waitForDrawOverOtherAppPermission(Ljava/lang/String;)V

    .line 498
    invoke-static {}, Lhost/exp/exponent/experience/ExperienceActivity;->access$400()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lhost/exp/exponent/utils/AsyncCondition;->remove(Ljava/lang/String;)V

    return-void
.end method

.method public isReady()Z
    .locals 1

    .line 490
    iget-boolean v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$5;->val$finalIsReadyForBundle:Z

    return v0
.end method
