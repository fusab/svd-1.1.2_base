.class Lhost/exp/exponent/experience/BaseExperienceActivity$2;
.super Ljava/lang/Object;
.source "BaseExperienceActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/experience/BaseExperienceActivity;->consumeErrorQueue()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/experience/BaseExperienceActivity;


# direct methods
.method constructor <init>(Lhost/exp/exponent/experience/BaseExperienceActivity;)V
    .locals 0

    .line 175
    iput-object p1, p0, Lhost/exp/exponent/experience/BaseExperienceActivity$2;->this$0:Lhost/exp/exponent/experience/BaseExperienceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 178
    sget-object v0, Lhost/exp/exponent/experience/ReactNativeActivity;->sErrorQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 182
    :cond_0
    invoke-static {}, Lhost/exp/exponent/experience/BaseExperienceActivity;->access$000()Landroid/util/Pair;

    move-result-object v0

    .line 183
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 184
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lhost/exp/exponent/kernel/ExponentErrorMessage;

    .line 186
    iget-object v2, p0, Lhost/exp/exponent/experience/BaseExperienceActivity$2;->this$0:Lhost/exp/exponent/experience/BaseExperienceActivity;

    invoke-virtual {v2, v0}, Lhost/exp/exponent/experience/BaseExperienceActivity;->shouldShowErrorScreen(Lhost/exp/exponent/kernel/ExponentErrorMessage;)Z

    move-result v2

    if-nez v2, :cond_1

    return-void

    :cond_1
    if-nez v1, :cond_2

    return-void

    .line 196
    :cond_2
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lhost/exp/exponent/experience/BaseExperienceActivity$2;->this$0:Lhost/exp/exponent/experience/BaseExperienceActivity;

    invoke-virtual {v1}, Lhost/exp/exponent/experience/BaseExperienceActivity;->isDebugModeEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    .line 197
    :cond_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expo encountered a fatal error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lhost/exp/exponent/kernel/ExponentErrorMessage;->developerErrorMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 200
    :cond_4
    :goto_0
    iget-object v1, p0, Lhost/exp/exponent/experience/BaseExperienceActivity$2;->this$0:Lhost/exp/exponent/experience/BaseExperienceActivity;

    invoke-virtual {v1}, Lhost/exp/exponent/experience/BaseExperienceActivity;->isDebugModeEnabled()Z

    move-result v1

    if-nez v1, :cond_5

    .line 201
    iget-object v1, p0, Lhost/exp/exponent/experience/BaseExperienceActivity$2;->this$0:Lhost/exp/exponent/experience/BaseExperienceActivity;

    invoke-virtual {v1}, Lhost/exp/exponent/experience/BaseExperienceActivity;->removeViews()V

    .line 202
    iget-object v1, p0, Lhost/exp/exponent/experience/BaseExperienceActivity$2;->this$0:Lhost/exp/exponent/experience/BaseExperienceActivity;

    iget-object v1, v1, Lhost/exp/exponent/experience/BaseExperienceActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lhost/exp/exponent/RNObject;->assign(Ljava/lang/Object;)V

    .line 203
    iget-object v1, p0, Lhost/exp/exponent/experience/BaseExperienceActivity$2;->this$0:Lhost/exp/exponent/experience/BaseExperienceActivity;

    iget-object v1, v1, Lhost/exp/exponent/experience/BaseExperienceActivity;->mReactRootView:Lhost/exp/exponent/RNObject;

    invoke-virtual {v1, v2}, Lhost/exp/exponent/RNObject;->assign(Ljava/lang/Object;)V

    .line 206
    :cond_5
    iget-object v1, p0, Lhost/exp/exponent/experience/BaseExperienceActivity$2;->this$0:Lhost/exp/exponent/experience/BaseExperienceActivity;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lhost/exp/exponent/experience/BaseExperienceActivity;->mIsCrashed:Z

    const/4 v2, 0x0

    .line 207
    iput-boolean v2, v1, Lhost/exp/exponent/experience/BaseExperienceActivity;->mIsLoading:Z

    .line 209
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lhost/exp/exponent/experience/ErrorActivity;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x24000000

    .line 210
    invoke-virtual {v2, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 211
    iget-object v1, p0, Lhost/exp/exponent/experience/BaseExperienceActivity$2;->this$0:Lhost/exp/exponent/experience/BaseExperienceActivity;

    invoke-virtual {v1, v2}, Lhost/exp/exponent/experience/BaseExperienceActivity;->onError(Landroid/content/Intent;)V

    .line 212
    iget-object v1, p0, Lhost/exp/exponent/experience/BaseExperienceActivity$2;->this$0:Lhost/exp/exponent/experience/BaseExperienceActivity;

    invoke-virtual {v1}, Lhost/exp/exponent/experience/BaseExperienceActivity;->isDebugModeEnabled()Z

    move-result v1

    const-string v3, "isDebugModeEnabled"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 213
    invoke-virtual {v0}, Lhost/exp/exponent/kernel/ExponentErrorMessage;->userErrorMessage()Ljava/lang/String;

    move-result-object v1

    const-string v3, "userErrorMessage"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 214
    invoke-virtual {v0}, Lhost/exp/exponent/kernel/ExponentErrorMessage;->developerErrorMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "developerErrorMessage"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    iget-object v0, p0, Lhost/exp/exponent/experience/BaseExperienceActivity$2;->this$0:Lhost/exp/exponent/experience/BaseExperienceActivity;

    invoke-virtual {v0, v2}, Lhost/exp/exponent/experience/BaseExperienceActivity;->startActivity(Landroid/content/Intent;)V

    .line 217
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lhost/exp/exponent/experience/ReactNativeActivity$ExperienceDoneLoadingEvent;

    invoke-direct {v1}, Lhost/exp/exponent/experience/ReactNativeActivity$ExperienceDoneLoadingEvent;-><init>()V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    return-void
.end method
