.class Lhost/exp/exponent/experience/ExperienceActivity$6;
.super Ljava/lang/Object;
.source "ExperienceActivity.java"

# interfaces
.implements Lhost/exp/expoview/Exponent$PackagerStatusCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/experience/ExperienceActivity;->startReactInstance()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/experience/ExperienceActivity;


# direct methods
.method constructor <init>(Lhost/exp/exponent/experience/ExperienceActivity;)V
    .locals 0

    .line 567
    iput-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity$6;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/String;)V
    .locals 1

    .line 575
    invoke-static {}, Lhost/exp/exponent/kernel/KernelProvider;->getInstance()Lhost/exp/exponent/kernel/KernelInterface;

    move-result-object v0

    invoke-virtual {v0, p1}, Lhost/exp/exponent/kernel/KernelInterface;->handleError(Ljava/lang/String;)V

    return-void
.end method

.method public onSuccess()V
    .locals 10

    .line 570
    iget-object v9, p0, Lhost/exp/exponent/experience/ExperienceActivity$6;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-static {v9}, Lhost/exp/exponent/experience/ExperienceActivity;->access$600(Lhost/exp/exponent/experience/ExperienceActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$6;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v3, v0, Lhost/exp/exponent/experience/ExperienceActivity;->mDetachSdkVersion:Ljava/lang/String;

    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$6;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-static {v0}, Lhost/exp/exponent/experience/ExperienceActivity;->access$100(Lhost/exp/exponent/experience/ExperienceActivity;)Lhost/exp/exponent/notifications/ExponentNotification;

    move-result-object v4

    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$6;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-static {v0}, Lhost/exp/exponent/experience/ExperienceActivity;->access$700(Lhost/exp/exponent/experience/ExperienceActivity;)Z

    move-result v5

    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$6;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-virtual {v0}, Lhost/exp/exponent/experience/ExperienceActivity;->reactPackages()Ljava/util/List;

    move-result-object v6

    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$6;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-virtual {v0}, Lhost/exp/exponent/experience/ExperienceActivity;->expoPackages()Ljava/util/List;

    move-result-object v7

    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$6;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-static {v0}, Lhost/exp/exponent/experience/ExperienceActivity;->access$800(Lhost/exp/exponent/experience/ExperienceActivity;)Lhost/exp/exponent/experience/DevBundleDownloadProgressListener;

    move-result-object v8

    move-object v0, v9

    move-object v1, v9

    invoke-virtual/range {v0 .. v8}, Lhost/exp/exponent/experience/ExperienceActivity;->startReactInstance(Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/notifications/ExponentNotification;ZLjava/util/List;Ljava/util/List;Lhost/exp/exponent/experience/DevBundleDownloadProgressListener;)Lhost/exp/exponent/RNObject;

    move-result-object v0

    iput-object v0, v9, Lhost/exp/exponent/experience/ExperienceActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    return-void
.end method
