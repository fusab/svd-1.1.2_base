.class public Lhost/exp/exponent/experience/DetachedModuleRegistryAdapter;
.super Lversioned/host/exp/exponent/modules/universal/ExpoModuleRegistryAdapter;
.source "DetachedModuleRegistryAdapter.java"


# direct methods
.method public constructor <init>(Lorg/unimodules/adapters/react/ReactModuleRegistryProvider;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lversioned/host/exp/exponent/modules/universal/ExpoModuleRegistryAdapter;-><init>(Lorg/unimodules/adapters/react/ReactModuleRegistryProvider;)V

    return-void
.end method


# virtual methods
.method protected configureModuleRegistry(Lorg/unimodules/core/ModuleRegistry;Lcom/facebook/react/bridge/ReactApplicationContext;)V
    .locals 0

    return-void
.end method

.method public createNativeModules(Lhost/exp/exponent/utils/ScopedContext;Lhost/exp/exponent/kernel/ExperienceId;Ljava/util/Map;Lorg/json/JSONObject;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhost/exp/exponent/utils/ScopedContext;",
            "Lhost/exp/exponent/kernel/ExperienceId;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/json/JSONObject;",
            "Ljava/util/List<",
            "Lcom/facebook/react/bridge/NativeModule;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/facebook/react/bridge/NativeModule;",
            ">;"
        }
    .end annotation

    .line 31
    invoke-virtual {p1}, Lhost/exp/exponent/utils/ScopedContext;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/bridge/ReactApplicationContext;

    .line 34
    iget-object v1, p0, Lhost/exp/exponent/experience/DetachedModuleRegistryAdapter;->mModuleRegistryProvider:Lorg/unimodules/adapters/react/ReactModuleRegistryProvider;

    invoke-virtual {v1, p1}, Lorg/unimodules/adapters/react/ReactModuleRegistryProvider;->get(Landroid/content/Context;)Lorg/unimodules/core/ModuleRegistry;

    move-result-object v1

    .line 36
    new-instance v2, Lversioned/host/exp/exponent/modules/universal/ConstantsBinding;

    invoke-direct {v2, p1, p3, p4}, Lversioned/host/exp/exponent/modules/universal/ConstantsBinding;-><init>(Landroid/content/Context;Ljava/util/Map;Lorg/json/JSONObject;)V

    invoke-virtual {v1, v2}, Lorg/unimodules/core/ModuleRegistry;->registerInternalModule(Lorg/unimodules/core/interfaces/InternalModule;)V

    .line 39
    invoke-virtual {p1}, Lhost/exp/exponent/utils/ScopedContext;->getContext()Landroid/content/Context;

    move-result-object p3

    check-cast p3, Lcom/facebook/react/bridge/ReactApplicationContext;

    .line 40
    iget-object v2, p0, Lhost/exp/exponent/experience/DetachedModuleRegistryAdapter;->mReactAdapterPackage:Lorg/unimodules/adapters/react/ReactAdapterPackage;

    invoke-virtual {v2, p3}, Lorg/unimodules/adapters/react/ReactAdapterPackage;->createInternalModules(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/unimodules/core/interfaces/InternalModule;

    .line 41
    invoke-virtual {v1, v3}, Lorg/unimodules/core/ModuleRegistry;->registerInternalModule(Lorg/unimodules/core/interfaces/InternalModule;)V

    goto :goto_0

    .line 45
    :cond_0
    new-instance v2, Lversioned/host/exp/exponent/modules/universal/ScopedUIManagerModuleWrapper;

    const-string v3, "name"

    invoke-virtual {p4, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    invoke-direct {v2, p3, p2, p4}, Lversioned/host/exp/exponent/modules/universal/ScopedUIManagerModuleWrapper;-><init>(Lcom/facebook/react/bridge/ReactContext;Lhost/exp/exponent/kernel/ExperienceId;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lorg/unimodules/core/ModuleRegistry;->registerInternalModule(Lorg/unimodules/core/interfaces/InternalModule;)V

    .line 48
    new-instance p2, Lversioned/host/exp/exponent/modules/universal/ScopedFileSystemModule;

    invoke-direct {p2, p1}, Lversioned/host/exp/exponent/modules/universal/ScopedFileSystemModule;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p2}, Lorg/unimodules/core/ModuleRegistry;->registerExportedModule(Lorg/unimodules/core/ExportedModule;)V

    :try_start_0
    const-string p2, "expo.modules.securestore.SecureStoreModule"

    .line 53
    invoke-static {p2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 54
    new-instance p2, Lversioned/host/exp/exponent/modules/universal/SecureStoreModuleBinding;

    invoke-direct {p2, p1}, Lversioned/host/exp/exponent/modules/universal/SecureStoreModuleBinding;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p2}, Lorg/unimodules/core/ModuleRegistry;->registerExportedModule(Lorg/unimodules/core/ExportedModule;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :catch_0
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/facebook/react/bridge/NativeModule;

    .line 62
    instance-of p3, p2, Lorg/unimodules/core/interfaces/RegistryLifecycleListener;

    if-eqz p3, :cond_1

    .line 63
    check-cast p2, Lorg/unimodules/core/interfaces/RegistryLifecycleListener;

    invoke-virtual {v1, p2}, Lorg/unimodules/core/ModuleRegistry;->registerExtraListener(Lorg/unimodules/core/interfaces/RegistryLifecycleListener;)V

    goto :goto_1

    .line 67
    :cond_2
    invoke-virtual {p0, v1, v0}, Lhost/exp/exponent/experience/DetachedModuleRegistryAdapter;->configureModuleRegistry(Lorg/unimodules/core/ModuleRegistry;Lcom/facebook/react/bridge/ReactApplicationContext;)V

    .line 69
    invoke-virtual {p0, v0, v1}, Lhost/exp/exponent/experience/DetachedModuleRegistryAdapter;->getNativeModulesFromModuleRegistry(Lcom/facebook/react/bridge/ReactApplicationContext;Lorg/unimodules/core/ModuleRegistry;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
