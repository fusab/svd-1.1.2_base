.class public abstract Lhost/exp/exponent/experience/BaseExperienceActivity;
.super Lhost/exp/exponent/experience/MultipleVersionReactNativeActivity;
.source "BaseExperienceActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhost/exp/exponent/experience/BaseExperienceActivity$ExperienceContentLoaded;,
        Lhost/exp/exponent/experience/BaseExperienceActivity$ExperienceBackgroundedEvent;,
        Lhost/exp/exponent/experience/BaseExperienceActivity$ExperienceForegroundedEvent;,
        Lhost/exp/exponent/experience/BaseExperienceActivity$ExperienceEvent;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String; = "BaseExperienceActivity"

.field private static sVisibleActivity:Lhost/exp/exponent/experience/BaseExperienceActivity;


# instance fields
.field mKernel:Lhost/exp/exponent/kernel/Kernel;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mOnResumeTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Lhost/exp/exponent/experience/MultipleVersionReactNativeActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Landroid/util/Pair;
    .locals 1

    .line 29
    invoke-static {}, Lhost/exp/exponent/experience/BaseExperienceActivity;->sendErrorsToErrorActivity()Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public static addError(Lhost/exp/exponent/kernel/ExponentError;)V
    .locals 1

    .line 61
    sget-object v0, Lhost/exp/exponent/experience/BaseExperienceActivity;->sErrorQueue:Ljava/util/Queue;

    invoke-interface {v0, p0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 63
    sget-object p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->sVisibleActivity:Lhost/exp/exponent/experience/BaseExperienceActivity;

    if-eqz p0, :cond_0

    .line 64
    invoke-virtual {p0}, Lhost/exp/exponent/experience/BaseExperienceActivity;->consumeErrorQueue()V

    goto :goto_0

    .line 65
    :cond_0
    invoke-static {}, Lhost/exp/exponent/experience/ErrorActivity;->getVisibleActivity()Lhost/exp/exponent/experience/ErrorActivity;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 67
    invoke-static {}, Lhost/exp/exponent/experience/BaseExperienceActivity;->sendErrorsToErrorActivity()Landroid/util/Pair;

    :cond_1
    :goto_0
    return-void
.end method

.method public static getVisibleActivity()Lhost/exp/exponent/experience/BaseExperienceActivity;
    .locals 1

    .line 75
    sget-object v0, Lhost/exp/exponent/experience/BaseExperienceActivity;->sVisibleActivity:Lhost/exp/exponent/experience/BaseExperienceActivity;

    return-object v0
.end method

.method private static sendErrorsToErrorActivity()Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair<",
            "Ljava/lang/Boolean;",
            "Lhost/exp/exponent/kernel/ExponentErrorMessage;",
            ">;"
        }
    .end annotation

    const-string v0, ""

    .line 224
    invoke-static {v0}, Lhost/exp/exponent/kernel/ExponentErrorMessage;->developerErrorMessage(Ljava/lang/String;)Lhost/exp/exponent/kernel/ExponentErrorMessage;

    move-result-object v0

    .line 226
    sget-object v1, Lhost/exp/exponent/experience/BaseExperienceActivity;->sErrorQueue:Ljava/util/Queue;

    monitor-enter v1

    const/4 v2, 0x0

    .line 227
    :goto_0
    :try_start_0
    sget-object v3, Lhost/exp/exponent/experience/BaseExperienceActivity;->sErrorQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 228
    sget-object v0, Lhost/exp/exponent/experience/BaseExperienceActivity;->sErrorQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhost/exp/exponent/kernel/ExponentError;

    .line 229
    invoke-static {v0}, Lhost/exp/exponent/experience/ErrorActivity;->addError(Lhost/exp/exponent/kernel/ExponentError;)V

    .line 230
    sget-object v3, Lhost/exp/exponent/experience/BaseExperienceActivity;->sVisibleActivity:Lhost/exp/exponent/experience/BaseExperienceActivity;

    if-eqz v3, :cond_0

    .line 231
    sget-object v3, Lhost/exp/exponent/experience/BaseExperienceActivity;->sVisibleActivity:Lhost/exp/exponent/experience/BaseExperienceActivity;

    invoke-virtual {v3, v0}, Lhost/exp/exponent/experience/BaseExperienceActivity;->onError(Lhost/exp/exponent/kernel/ExponentError;)V

    .line 235
    :cond_0
    iget-object v3, v0, Lhost/exp/exponent/kernel/ExponentError;->errorMessage:Lhost/exp/exponent/kernel/ExponentErrorMessage;

    .line 236
    iget-boolean v0, v0, Lhost/exp/exponent/kernel/ExponentError;->isFatal:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    const/4 v2, 0x1

    :cond_1
    move-object v0, v3

    goto :goto_0

    .line 240
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242
    new-instance v1, Landroid/util/Pair;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1

    :catchall_0
    move-exception v0

    .line 240
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected consumeErrorQueue()V
    .locals 1

    .line 171
    sget-object v0, Lhost/exp/exponent/experience/BaseExperienceActivity;->sErrorQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 175
    :cond_0
    new-instance v0, Lhost/exp/exponent/experience/BaseExperienceActivity$2;

    invoke-direct {v0, p0}, Lhost/exp/exponent/experience/BaseExperienceActivity$2;-><init>(Lhost/exp/exponent/experience/BaseExperienceActivity;)V

    invoke-virtual {p0, v0}, Lhost/exp/exponent/experience/BaseExperienceActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public invokeDefaultOnBackPressed()V
    .locals 1

    const/4 v0, 0x1

    .line 146
    invoke-virtual {p0, v0}, Lhost/exp/exponent/experience/BaseExperienceActivity;->moveTaskToBack(Z)Z

    return-void
.end method

.method public isDebugModeEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onBackPressed()V
    .locals 3

    .line 137
    iget-object v0, p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->isNotNull()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mIsCrashed:Z

    if-nez v0, :cond_0

    .line 138
    iget-object v0, p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "onBackPressed"

    invoke-virtual {v0, v2, v1}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 140
    invoke-virtual {p0, v0}, Lhost/exp/exponent/experience/BaseExperienceActivity;->moveTaskToBack(Z)Z

    :goto_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 80
    invoke-super {p0, p1}, Lhost/exp/exponent/experience/MultipleVersionReactNativeActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    .line 81
    iput-boolean p1, p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mIsInForeground:Z

    .line 82
    new-instance p1, Lhost/exp/exponent/RNObject;

    const-string v0, "com.facebook.react.ReactRootView"

    invoke-direct {p1, v0}, Lhost/exp/exponent/RNObject;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mReactRootView:Lhost/exp/exponent/RNObject;

    .line 84
    invoke-static {}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->getInstance()Lhost/exp/exponent/di/NativeModuleDepsProvider;

    move-result-object p1

    const-class v0, Lhost/exp/exponent/experience/BaseExperienceActivity;

    invoke-virtual {p1, v0, p0}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->inject(Ljava/lang/Class;Ljava/lang/Object;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .line 151
    invoke-super {p0}, Lhost/exp/exponent/experience/MultipleVersionReactNativeActivity;->onDestroy()V

    .line 153
    instance-of v0, p0, Lhost/exp/exponent/experience/HomeActivity;

    if-eqz v0, :cond_0

    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->isNotNull()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 159
    iget-object v0, p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->onHostDestroy()V

    .line 160
    iget-object v0, p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    invoke-virtual {v0, v1}, Lhost/exp/exponent/RNObject;->assign(Ljava/lang/Object;)V

    .line 162
    :cond_1
    iget-object v0, p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mReactRootView:Lhost/exp/exponent/RNObject;

    invoke-virtual {v0, v1}, Lhost/exp/exponent/RNObject;->assign(Ljava/lang/Object;)V

    .line 165
    invoke-virtual {p0}, Lhost/exp/exponent/experience/BaseExperienceActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/drawee/backends/pipeline/Fresco;->initialize(Landroid/content/Context;)V

    return-void
.end method

.method protected onError(Landroid/content/Intent;)V
    .locals 0

    return-void
.end method

.method protected onError(Lhost/exp/exponent/kernel/ExponentError;)V
    .locals 0

    return-void
.end method

.method protected onPause()V
    .locals 5

    .line 115
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lhost/exp/exponent/experience/BaseExperienceActivity$ExperienceBackgroundedEvent;

    iget-object v2, p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    invoke-direct {v1, v2}, Lhost/exp/exponent/experience/BaseExperienceActivity$ExperienceBackgroundedEvent;-><init>(Lhost/exp/exponent/kernel/ExperienceId;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 116
    invoke-super {p0}, Lhost/exp/exponent/experience/MultipleVersionReactNativeActivity;->onPause()V

    .line 126
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mOnResumeTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1f4

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x0

    .line 128
    iput-boolean v0, p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mIsInForeground:Z

    .line 129
    sget-object v0, Lhost/exp/exponent/experience/BaseExperienceActivity;->sVisibleActivity:Lhost/exp/exponent/experience/BaseExperienceActivity;

    if-ne v0, p0, :cond_0

    const/4 v0, 0x0

    .line 130
    sput-object v0, Lhost/exp/exponent/experience/BaseExperienceActivity;->sVisibleActivity:Lhost/exp/exponent/experience/BaseExperienceActivity;

    :cond_0
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 1

    .line 281
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lhost/exp/expoview/Exponent;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 89
    invoke-super {p0}, Lhost/exp/exponent/experience/MultipleVersionReactNativeActivity;->onResume()V

    .line 90
    iget-object v0, p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0, p0}, Lhost/exp/exponent/kernel/Kernel;->setActivityContext(Landroid/app/Activity;)V

    .line 91
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    invoke-virtual {v0, p0}, Lhost/exp/expoview/Exponent;->setCurrentActivity(Landroid/app/Activity;)V

    .line 93
    sput-object p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->sVisibleActivity:Lhost/exp/exponent/experience/BaseExperienceActivity;

    .line 96
    invoke-virtual {p0}, Lhost/exp/exponent/experience/BaseExperienceActivity;->consumeErrorQueue()V

    const/4 v0, 0x1

    .line 97
    iput-boolean v0, p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mIsInForeground:Z

    .line 99
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mOnResumeTime:J

    .line 100
    new-instance v0, Lhost/exp/exponent/experience/BaseExperienceActivity$1;

    invoke-direct {v0, p0}, Lhost/exp/exponent/experience/BaseExperienceActivity$1;-><init>(Lhost/exp/exponent/experience/BaseExperienceActivity;)V

    const-string v1, "experienceIdSetForActivity"

    invoke-static {v1, v0}, Lhost/exp/exponent/utils/AsyncCondition;->wait(Ljava/lang/String;Lhost/exp/exponent/utils/AsyncCondition$AsyncConditionListener;)V

    return-void
.end method

.method protected registerForNotifications()V
    .locals 3

    .line 262
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    .line 264
    sget-boolean v0, Lhost/exp/exponent/Constants;->FCM_ENABLED:Z

    if-nez v0, :cond_0

    .line 265
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lhost/exp/exponent/gcm/GcmRegistrationIntentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 266
    invoke-virtual {p0, v0}, Lhost/exp/exponent/experience/BaseExperienceActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 275
    sget-object v1, Lhost/exp/exponent/experience/BaseExperienceActivity;->TAG:Ljava/lang/String;

    const-string v2, "Failed to register for GCM notifications"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void
.end method
