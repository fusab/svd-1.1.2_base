.class public Lhost/exp/exponent/experience/HomeActivity;
.super Lhost/exp/exponent/experience/BaseExperienceActivity;
.source "HomeActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Lhost/exp/exponent/experience/BaseExperienceActivity;-><init>()V

    return-void
.end method

.method public static homeExpoPackages()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/unimodules/core/interfaces/Package;",
            ">;"
        }
    .end annotation

    const/16 v0, 0xb

    .line 107
    new-array v0, v0, [Lorg/unimodules/core/interfaces/Package;

    new-instance v1, Lexpo/modules/constants/ConstantsPackage;

    invoke-direct {v1}, Lexpo/modules/constants/ConstantsPackage;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lexpo/modules/permissions/PermissionsPackage;

    invoke-direct {v1}, Lexpo/modules/permissions/PermissionsPackage;-><init>()V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lexpo/modules/filesystem/FileSystemPackage;

    invoke-direct {v1}, Lexpo/modules/filesystem/FileSystemPackage;-><init>()V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lexpo/modules/font/FontLoaderPackage;

    invoke-direct {v1}, Lexpo/modules/font/FontLoaderPackage;-><init>()V

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lexpo/modules/barcodescanner/BarCodeScannerPackage;

    invoke-direct {v1}, Lexpo/modules/barcodescanner/BarCodeScannerPackage;-><init>()V

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lexpo/modules/keepawake/KeepAwakePackage;

    invoke-direct {v1}, Lexpo/modules/keepawake/KeepAwakePackage;-><init>()V

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lexpo/modules/analytics/amplitude/AmplitudePackage;

    invoke-direct {v1}, Lexpo/modules/analytics/amplitude/AmplitudePackage;-><init>()V

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lexpo/modules/camera/CameraPackage;

    invoke-direct {v1}, Lexpo/modules/camera/CameraPackage;-><init>()V

    const/4 v2, 0x7

    aput-object v1, v0, v2

    new-instance v1, Lexpo/modules/facedetector/FaceDetectorPackage;

    invoke-direct {v1}, Lexpo/modules/facedetector/FaceDetectorPackage;-><init>()V

    const/16 v2, 0x8

    aput-object v1, v0, v2

    new-instance v1, Lexpo/modules/medialibrary/MediaLibraryPackage;

    invoke-direct {v1}, Lexpo/modules/medialibrary/MediaLibraryPackage;-><init>()V

    const/16 v2, 0x9

    aput-object v1, v0, v2

    new-instance v1, Lexpo/modules/taskManager/TaskManagerPackage;

    invoke-direct {v1}, Lexpo/modules/taskManager/TaskManagerPackage;-><init>()V

    const/16 v2, 0xa

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private tryInstallLeakCanary(Z)V
    .locals 0

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 44
    invoke-super {p0, p1}, Lhost/exp/exponent/experience/BaseExperienceActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x0

    .line 45
    iput-boolean p1, p0, Lhost/exp/exponent/experience/HomeActivity;->mShouldDestroyRNInstanceOnExit:Z

    const-string p1, "UNVERSIONED"

    .line 46
    iput-object p1, p0, Lhost/exp/exponent/experience/HomeActivity;->mSDKVersion:Ljava/lang/String;

    .line 48
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object p1

    invoke-virtual {p1, p0}, Lde/greenrobot/event/EventBus;->registerSticky(Ljava/lang/Object;)V

    .line 49
    iget-object p1, p0, Lhost/exp/exponent/experience/HomeActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {p1}, Lhost/exp/exponent/kernel/Kernel;->startJSKernel()V

    const/4 p1, 0x0

    .line 50
    invoke-virtual {p0, p1}, Lhost/exp/exponent/experience/HomeActivity;->showLoadingScreen(Lorg/json/JSONObject;)V

    const/4 p1, 0x1

    .line 52
    invoke-direct {p0, p1}, Lhost/exp/exponent/experience/HomeActivity;->tryInstallLeakCanary(Z)V

    return-void
.end method

.method protected onError(Landroid/content/Intent;)V
    .locals 2

    const-string v0, "isHome"

    const/4 v1, 0x1

    .line 102
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 103
    iget-object p1, p0, Lhost/exp/exponent/experience/HomeActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {p1}, Lhost/exp/exponent/kernel/Kernel;->setHasError()V

    return-void
.end method

.method public onEventMainThread(Lhost/exp/exponent/kernel/Kernel$KernelStartedRunningEvent;)V
    .locals 1

    .line 89
    iget-object p1, p0, Lhost/exp/exponent/experience/HomeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    iget-object v0, p0, Lhost/exp/exponent/experience/HomeActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0}, Lhost/exp/exponent/kernel/Kernel;->getReactInstanceManager()Lcom/facebook/react/ReactInstanceManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Lhost/exp/exponent/RNObject;->assign(Ljava/lang/Object;)V

    .line 90
    iget-object p1, p0, Lhost/exp/exponent/experience/HomeActivity;->mReactRootView:Lhost/exp/exponent/RNObject;

    iget-object v0, p0, Lhost/exp/exponent/experience/HomeActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0}, Lhost/exp/exponent/kernel/Kernel;->getReactRootView()Lcom/facebook/react/ReactRootView;

    move-result-object v0

    invoke-virtual {p1, v0}, Lhost/exp/exponent/RNObject;->assign(Ljava/lang/Object;)V

    .line 91
    iget-object p1, p0, Lhost/exp/exponent/experience/HomeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    invoke-virtual {p1, p0, p0}, Lhost/exp/exponent/RNObject;->onHostResume(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 92
    iget-object p1, p0, Lhost/exp/exponent/experience/HomeActivity;->mReactRootView:Lhost/exp/exponent/RNObject;

    invoke-virtual {p1}, Lhost/exp/exponent/RNObject;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lhost/exp/exponent/experience/HomeActivity;->setView(Landroid/view/View;)V

    .line 93
    invoke-virtual {p0}, Lhost/exp/exponent/experience/HomeActivity;->checkForReactViews()V

    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 0

    .line 72
    invoke-super {p0, p1, p2, p3}, Lhost/exp/exponent/experience/BaseExperienceActivity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    const/4 p1, 0x0

    .line 74
    invoke-direct {p0, p1}, Lhost/exp/exponent/experience/HomeActivity;->tryInstallLeakCanary(Z)V

    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 79
    invoke-super {p0}, Lhost/exp/exponent/experience/BaseExperienceActivity;->onResume()V

    const/4 v0, 0x0

    .line 81
    invoke-static {p0, v0}, Lcom/facebook/soloader/SoLoader;->init(Landroid/content/Context;Z)V

    const-string v0, "HOME_APPEARED"

    .line 83
    invoke-static {v0}, Lhost/exp/exponent/analytics/Analytics;->logEvent(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p0}, Lhost/exp/exponent/experience/HomeActivity;->registerForNotifications()V

    return-void
.end method
