.class public Lhost/exp/exponent/experience/InfoActivity_ViewBinding;
.super Ljava/lang/Object;
.source "InfoActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lhost/exp/exponent/experience/InfoActivity;

.field private view7f0b003d:Landroid/view/View;

.field private view7f0b0107:Landroid/view/View;


# direct methods
.method public constructor <init>(Lhost/exp/exponent/experience/InfoActivity;)V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .line 27
    invoke-virtual {p1}, Lhost/exp/exponent/experience/InfoActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lhost/exp/exponent/experience/InfoActivity_ViewBinding;-><init>(Lhost/exp/exponent/experience/InfoActivity;Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Lhost/exp/exponent/experience/InfoActivity;Landroid/view/View;)V
    .locals 4
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lhost/exp/exponent/experience/InfoActivity_ViewBinding;->target:Lhost/exp/exponent/experience/InfoActivity;

    .line 35
    sget v0, Lhost/exp/expoview/R$id;->toolbar:I

    const-class v1, Landroidx/appcompat/widget/Toolbar;

    const-string v2, "field \'mToolbar\'"

    invoke-static {p2, v0, v2, v1}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    iput-object v0, p1, Lhost/exp/exponent/experience/InfoActivity;->mToolbar:Landroidx/appcompat/widget/Toolbar;

    .line 36
    sget v0, Lhost/exp/expoview/R$id;->app_icon_small:I

    const-class v1, Landroid/widget/ImageView;

    const-string v2, "field \'mImageView\'"

    invoke-static {p2, v0, v2, v1}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lhost/exp/exponent/experience/InfoActivity;->mImageView:Landroid/widget/ImageView;

    .line 37
    sget v0, Lhost/exp/expoview/R$id;->app_name:I

    const-class v1, Landroid/widget/TextView;

    const-string v2, "field \'mAppNameView\'"

    invoke-static {p2, v0, v2, v1}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lhost/exp/exponent/experience/InfoActivity;->mAppNameView:Landroid/widget/TextView;

    .line 38
    sget v0, Lhost/exp/expoview/R$id;->experience_id:I

    const-class v1, Landroid/widget/TextView;

    const-string v2, "field \'mExperienceIdView\'"

    invoke-static {p2, v0, v2, v1}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lhost/exp/exponent/experience/InfoActivity;->mExperienceIdView:Landroid/widget/TextView;

    .line 39
    sget v0, Lhost/exp/expoview/R$id;->sdk_version:I

    const-class v1, Landroid/widget/TextView;

    const-string v2, "field \'mSdkVersionView\'"

    invoke-static {p2, v0, v2, v1}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lhost/exp/exponent/experience/InfoActivity;->mSdkVersionView:Landroid/widget/TextView;

    .line 40
    sget v0, Lhost/exp/expoview/R$id;->published_time:I

    const-class v1, Landroid/widget/TextView;

    const-string v2, "field \'mPublishedTimeView\'"

    invoke-static {p2, v0, v2, v1}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lhost/exp/exponent/experience/InfoActivity;->mPublishedTimeView:Landroid/widget/TextView;

    .line 41
    sget v0, Lhost/exp/expoview/R$id;->is_verified:I

    const-class v1, Landroid/widget/TextView;

    const-string v2, "field \'mIsVerifiedView\'"

    invoke-static {p2, v0, v2, v1}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lhost/exp/exponent/experience/InfoActivity;->mIsVerifiedView:Landroid/widget/TextView;

    .line 42
    sget v0, Lhost/exp/expoview/R$id;->manifest:I

    const-class v1, Landroid/widget/TextView;

    const-string v2, "field \'mManifestTextView\'"

    invoke-static {p2, v0, v2, v1}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lhost/exp/exponent/experience/InfoActivity;->mManifestTextView:Landroid/widget/TextView;

    .line 43
    sget v0, Lhost/exp/expoview/R$id;->toggle_manifest:I

    const-string v1, "field \'mToggleManifestButton\' and method \'onClickToggleManifest\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 44
    sget v1, Lhost/exp/expoview/R$id;->toggle_manifest:I

    const-class v2, Landroid/widget/Button;

    const-string v3, "field \'mToggleManifestButton\'"

    invoke-static {v0, v1, v3, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p1, Lhost/exp/exponent/experience/InfoActivity;->mToggleManifestButton:Landroid/widget/Button;

    .line 45
    iput-object v0, p0, Lhost/exp/exponent/experience/InfoActivity_ViewBinding;->view7f0b0107:Landroid/view/View;

    .line 46
    new-instance v1, Lhost/exp/exponent/experience/InfoActivity_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lhost/exp/exponent/experience/InfoActivity_ViewBinding$1;-><init>(Lhost/exp/exponent/experience/InfoActivity_ViewBinding;Lhost/exp/exponent/experience/InfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    sget v0, Lhost/exp/expoview/R$id;->clear_data:I

    const-string v1, "method \'onClickClearData\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object p2

    .line 53
    iput-object p2, p0, Lhost/exp/exponent/experience/InfoActivity_ViewBinding;->view7f0b003d:Landroid/view/View;

    .line 54
    new-instance v0, Lhost/exp/exponent/experience/InfoActivity_ViewBinding$2;

    invoke-direct {v0, p0, p1}, Lhost/exp/exponent/experience/InfoActivity_ViewBinding$2;-><init>(Lhost/exp/exponent/experience/InfoActivity_ViewBinding;Lhost/exp/exponent/experience/InfoActivity;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    .line 65
    iget-object v0, p0, Lhost/exp/exponent/experience/InfoActivity_ViewBinding;->target:Lhost/exp/exponent/experience/InfoActivity;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 67
    iput-object v1, p0, Lhost/exp/exponent/experience/InfoActivity_ViewBinding;->target:Lhost/exp/exponent/experience/InfoActivity;

    .line 69
    iput-object v1, v0, Lhost/exp/exponent/experience/InfoActivity;->mToolbar:Landroidx/appcompat/widget/Toolbar;

    .line 70
    iput-object v1, v0, Lhost/exp/exponent/experience/InfoActivity;->mImageView:Landroid/widget/ImageView;

    .line 71
    iput-object v1, v0, Lhost/exp/exponent/experience/InfoActivity;->mAppNameView:Landroid/widget/TextView;

    .line 72
    iput-object v1, v0, Lhost/exp/exponent/experience/InfoActivity;->mExperienceIdView:Landroid/widget/TextView;

    .line 73
    iput-object v1, v0, Lhost/exp/exponent/experience/InfoActivity;->mSdkVersionView:Landroid/widget/TextView;

    .line 74
    iput-object v1, v0, Lhost/exp/exponent/experience/InfoActivity;->mPublishedTimeView:Landroid/widget/TextView;

    .line 75
    iput-object v1, v0, Lhost/exp/exponent/experience/InfoActivity;->mIsVerifiedView:Landroid/widget/TextView;

    .line 76
    iput-object v1, v0, Lhost/exp/exponent/experience/InfoActivity;->mManifestTextView:Landroid/widget/TextView;

    .line 77
    iput-object v1, v0, Lhost/exp/exponent/experience/InfoActivity;->mToggleManifestButton:Landroid/widget/Button;

    .line 79
    iget-object v0, p0, Lhost/exp/exponent/experience/InfoActivity_ViewBinding;->view7f0b0107:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iput-object v1, p0, Lhost/exp/exponent/experience/InfoActivity_ViewBinding;->view7f0b0107:Landroid/view/View;

    .line 81
    iget-object v0, p0, Lhost/exp/exponent/experience/InfoActivity_ViewBinding;->view7f0b003d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    iput-object v1, p0, Lhost/exp/exponent/experience/InfoActivity_ViewBinding;->view7f0b003d:Landroid/view/View;

    return-void

    .line 66
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
