.class Lhost/exp/exponent/experience/ExperienceActivity$3;
.super Ljava/lang/Object;
.source "ExperienceActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/experience/ExperienceActivity;->setLoadingScreenManifest(Lorg/json/JSONObject;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/experience/ExperienceActivity;

.field final synthetic val$manifest:Lorg/json/JSONObject;


# direct methods
.method constructor <init>(Lhost/exp/exponent/experience/ExperienceActivity;Lorg/json/JSONObject;)V
    .locals 0

    .line 313
    iput-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity$3;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iput-object p2, p0, Lhost/exp/exponent/experience/ExperienceActivity$3;->val$manifest:Lorg/json/JSONObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 316
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$3;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-virtual {v0}, Lhost/exp/exponent/experience/ExperienceActivity;->isInForeground()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 320
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$3;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-static {v0}, Lhost/exp/exponent/experience/ExperienceActivity;->access$000(Lhost/exp/exponent/experience/ExperienceActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 325
    :cond_1
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$3;->val$manifest:Lorg/json/JSONObject;

    const-string v1, "sdkVersion"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 326
    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity$3;->val$manifest:Lorg/json/JSONObject;

    iget-object v2, p0, Lhost/exp/exponent/experience/ExperienceActivity$3;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-static {v0, v1, v2}, Lhost/exp/exponent/utils/ExperienceActivityUtils;->setWindowTransparency(Ljava/lang/String;Lorg/json/JSONObject;Landroid/app/Activity;)V

    .line 327
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$3;->val$manifest:Lorg/json/JSONObject;

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity$3;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-static {v0, v1}, Lhost/exp/exponent/utils/ExperienceActivityUtils;->setNavigationBar(Lorg/json/JSONObject;Landroid/app/Activity;)V

    .line 329
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$3;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity$3;->val$manifest:Lorg/json/JSONObject;

    invoke-virtual {v0, v1}, Lhost/exp/exponent/experience/ExperienceActivity;->showLoadingScreen(Lorg/json/JSONObject;)V

    .line 331
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$3;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v0, v0, Lhost/exp/exponent/experience/ExperienceActivity;->mExponentManifest:Lhost/exp/exponent/ExponentManifest;

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity$3;->val$manifest:Lorg/json/JSONObject;

    iget-object v2, p0, Lhost/exp/exponent/experience/ExperienceActivity$3;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-static {v0, v1, v2}, Lhost/exp/exponent/utils/ExperienceActivityUtils;->setTaskDescription(Lhost/exp/exponent/ExponentManifest;Lorg/json/JSONObject;Landroid/app/Activity;)V

    return-void
.end method
