.class Lhost/exp/exponent/experience/ExperienceActivity$4;
.super Ljava/lang/Object;
.source "ExperienceActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/experience/ExperienceActivity;->setManifest(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONObject;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/experience/ExperienceActivity;

.field final synthetic val$finalNotificationObject:Lhost/exp/exponent/notifications/ExponentNotification;

.field final synthetic val$kernelOptions:Lorg/json/JSONObject;

.field final synthetic val$manifest:Lorg/json/JSONObject;


# direct methods
.method constructor <init>(Lhost/exp/exponent/experience/ExperienceActivity;Lhost/exp/exponent/notifications/ExponentNotification;Lorg/json/JSONObject;Lorg/json/JSONObject;)V
    .locals 0

    .line 435
    iput-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iput-object p2, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->val$finalNotificationObject:Lhost/exp/exponent/notifications/ExponentNotification;

    iput-object p3, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->val$manifest:Lorg/json/JSONObject;

    iput-object p4, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->val$kernelOptions:Lorg/json/JSONObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 438
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-virtual {v0}, Lhost/exp/exponent/experience/ExperienceActivity;->isInForeground()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 442
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v0, v0, Lhost/exp/exponent/experience/ExperienceActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->isNotNull()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 443
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v0, v0, Lhost/exp/exponent/experience/ExperienceActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->onHostDestroy()V

    .line 444
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v0, v0, Lhost/exp/exponent/experience/ExperienceActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhost/exp/exponent/RNObject;->assign(Ljava/lang/Object;)V

    .line 447
    :cond_1
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    new-instance v1, Lhost/exp/exponent/RNObject;

    const-string v2, "host.exp.exponent.ReactUnthemedRootView"

    invoke-direct {v1, v2}, Lhost/exp/exponent/RNObject;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lhost/exp/exponent/experience/ExperienceActivity;->mReactRootView:Lhost/exp/exponent/RNObject;

    .line 448
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v0, v0, Lhost/exp/exponent/experience/ExperienceActivity;->mReactRootView:Lhost/exp/exponent/RNObject;

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v1, v1, Lhost/exp/exponent/experience/ExperienceActivity;->mDetachSdkVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhost/exp/exponent/RNObject;->loadVersion(Ljava/lang/String;)Lhost/exp/exponent/RNObject;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Lhost/exp/exponent/RNObject;->construct([Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    .line 449
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v2, v0, Lhost/exp/exponent/experience/ExperienceActivity;->mReactRootView:Lhost/exp/exponent/RNObject;

    invoke-virtual {v2}, Lhost/exp/exponent/RNObject;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v0, v2}, Lhost/exp/exponent/experience/ExperienceActivity;->setView(Landroid/view/View;)V

    .line 453
    :try_start_0
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    iget-object v2, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v2, v2, Lhost/exp/exponent/experience/ExperienceActivity;->mExperienceIdString:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lhost/exp/expoview/Exponent;->encodeExperienceId(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 460
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-virtual {v0}, Lhost/exp/exponent/experience/ExperienceActivity;->isDebugModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 462
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->val$finalNotificationObject:Lhost/exp/exponent/notifications/ExponentNotification;

    invoke-static {v0, v1}, Lhost/exp/exponent/experience/ExperienceActivity;->access$102(Lhost/exp/exponent/experience/ExperienceActivity;Lhost/exp/exponent/notifications/ExponentNotification;)Lhost/exp/exponent/notifications/ExponentNotification;

    .line 463
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lhost/exp/exponent/experience/ExperienceActivity;->waitForDrawOverOtherAppPermission(Ljava/lang/String;)V

    goto :goto_0

    .line 465
    :cond_2
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v2, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->val$finalNotificationObject:Lhost/exp/exponent/notifications/ExponentNotification;

    invoke-static {v0, v2}, Lhost/exp/exponent/experience/ExperienceActivity;->access$202(Lhost/exp/exponent/experience/ExperienceActivity;Lhost/exp/exponent/notifications/ExponentNotification;)Lhost/exp/exponent/notifications/ExponentNotification;

    .line 466
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-static {v0, v1}, Lhost/exp/exponent/experience/ExperienceActivity;->access$302(Lhost/exp/exponent/experience/ExperienceActivity;Z)Z

    .line 467
    invoke-static {}, Lhost/exp/exponent/experience/ExperienceActivity;->access$400()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lhost/exp/exponent/utils/AsyncCondition;->notify(Ljava/lang/String;)V

    .line 470
    :goto_0
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v0, v0, Lhost/exp/exponent/experience/ExperienceActivity;->mDetachSdkVersion:Ljava/lang/String;

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->val$manifest:Lorg/json/JSONObject;

    iget-object v2, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-static {v0, v1, v2}, Lhost/exp/exponent/utils/ExperienceActivityUtils;->setWindowTransparency(Ljava/lang/String;Lorg/json/JSONObject;Landroid/app/Activity;)V

    .line 471
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->val$manifest:Lorg/json/JSONObject;

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-static {v0, v1}, Lhost/exp/exponent/utils/ExperienceActivityUtils;->setNavigationBar(Lorg/json/JSONObject;Landroid/app/Activity;)V

    .line 473
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->val$manifest:Lorg/json/JSONObject;

    invoke-virtual {v0, v1}, Lhost/exp/exponent/experience/ExperienceActivity;->showLoadingScreen(Lorg/json/JSONObject;)V

    .line 475
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v0, v0, Lhost/exp/exponent/experience/ExperienceActivity;->mExponentManifest:Lhost/exp/exponent/ExponentManifest;

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->val$manifest:Lorg/json/JSONObject;

    iget-object v2, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-static {v0, v1, v2}, Lhost/exp/exponent/utils/ExperienceActivityUtils;->setTaskDescription(Lhost/exp/exponent/ExponentManifest;Lorg/json/JSONObject;Landroid/app/Activity;)V

    .line 476
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity$4;->val$kernelOptions:Lorg/json/JSONObject;

    invoke-static {v0, v1}, Lhost/exp/exponent/experience/ExperienceActivity;->access$500(Lhost/exp/exponent/experience/ExperienceActivity;Lorg/json/JSONObject;)V

    return-void

    .line 455
    :catch_0
    invoke-static {}, Lhost/exp/exponent/kernel/KernelProvider;->getInstance()Lhost/exp/exponent/kernel/KernelInterface;

    move-result-object v0

    const-string v1, "Can\'t URL encode manifest ID"

    invoke-virtual {v0, v1}, Lhost/exp/exponent/kernel/KernelInterface;->handleError(Ljava/lang/String;)V

    return-void
.end method
