.class Lhost/exp/exponent/experience/ExperienceActivity$2;
.super Lhost/exp/exponent/AppLoader;
.source "ExperienceActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/experience/ExperienceActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/experience/ExperienceActivity;


# direct methods
.method constructor <init>(Lhost/exp/exponent/experience/ExperienceActivity;Ljava/lang/String;Z)V
    .locals 0

    .line 195
    iput-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity$2;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-direct {p0, p2, p3}, Lhost/exp/exponent/AppLoader;-><init>(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public emitEvent(Lorg/json/JSONObject;)V
    .locals 1

    .line 229
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$2;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-virtual {v0, p1}, Lhost/exp/exponent/experience/ExperienceActivity;->emitUpdatesEvent(Lorg/json/JSONObject;)V

    return-void
.end method

.method public onBundleCompleted(Ljava/lang/String;)V
    .locals 1

    .line 224
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$2;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-virtual {v0, p1}, Lhost/exp/exponent/experience/ExperienceActivity;->setBundle(Ljava/lang/String;)V

    return-void
.end method

.method public onError(Ljava/lang/Exception;)V
    .locals 1

    .line 234
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$2;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v0, v0, Lhost/exp/exponent/experience/ExperienceActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0, p1}, Lhost/exp/exponent/kernel/Kernel;->handleError(Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;)V
    .locals 1

    .line 239
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity$2;->this$0:Lhost/exp/exponent/experience/ExperienceActivity;

    iget-object v0, v0, Lhost/exp/exponent/experience/ExperienceActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0, p1}, Lhost/exp/exponent/kernel/Kernel;->handleError(Ljava/lang/String;)V

    return-void
.end method

.method public onManifestCompleted(Lorg/json/JSONObject;)V
    .locals 2

    .line 208
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    new-instance v1, Lhost/exp/exponent/experience/ExperienceActivity$2$2;

    invoke-direct {v1, p0, p1}, Lhost/exp/exponent/experience/ExperienceActivity$2$2;-><init>(Lhost/exp/exponent/experience/ExperienceActivity$2;Lorg/json/JSONObject;)V

    invoke-virtual {v0, v1}, Lhost/exp/expoview/Exponent;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onOptimisticManifest(Lorg/json/JSONObject;)V
    .locals 2

    .line 198
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    new-instance v1, Lhost/exp/exponent/experience/ExperienceActivity$2$1;

    invoke-direct {v1, p0, p1}, Lhost/exp/exponent/experience/ExperienceActivity$2$1;-><init>(Lhost/exp/exponent/experience/ExperienceActivity$2;Lorg/json/JSONObject;)V

    invoke-virtual {v0, v1}, Lhost/exp/expoview/Exponent;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method
