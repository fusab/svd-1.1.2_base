.class public Lhost/exp/exponent/experience/ErrorActivity;
.super Landroidx/fragment/app/FragmentActivity;
.source "ErrorActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhost/exp/exponent/experience/ErrorActivity$ViewPagerAdapter;
    }
.end annotation


# static fields
.field public static final DEBUG_MODE_KEY:Ljava/lang/String; = "isDebugModeEnabled"

.field public static final DEVELOPER_ERROR_MESSAGE_KEY:Ljava/lang/String; = "developerErrorMessage"

.field public static final IS_HOME_KEY:Ljava/lang/String; = "isHome"

.field public static final MANIFEST_URL_KEY:Ljava/lang/String; = "manifestUrl"

.field public static final USER_ERROR_MESSAGE_KEY:Ljava/lang/String; = "userErrorMessage"

.field private static mErrorConsoleFragment:Lhost/exp/exponent/experience/ErrorConsoleFragment;

.field private static sErrorList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lhost/exp/exponent/kernel/ExponentError;",
            ">;"
        }
    .end annotation
.end field

.field private static sVisibleActivity:Lhost/exp/exponent/experience/ErrorActivity;


# instance fields
.field mContext:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mKernel:Lhost/exp/exponent/kernel/Kernel;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mManifestUrl:Ljava/lang/String;

.field mPager:Landroidx/viewpager/widget/ViewPager;
    .annotation runtime Lbutterknife/BindView;
        value = 0x7f0b0061
    .end annotation
.end field

.field private mPagerAdapter:Landroidx/viewpager/widget/PagerAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 40
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lhost/exp/exponent/experience/ErrorActivity;->sErrorList:Ljava/util/LinkedList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Landroidx/fragment/app/FragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lhost/exp/exponent/experience/ErrorConsoleFragment;
    .locals 1

    .line 30
    sget-object v0, Lhost/exp/exponent/experience/ErrorActivity;->mErrorConsoleFragment:Lhost/exp/exponent/experience/ErrorConsoleFragment;

    return-object v0
.end method

.method static synthetic access$002(Lhost/exp/exponent/experience/ErrorConsoleFragment;)Lhost/exp/exponent/experience/ErrorConsoleFragment;
    .locals 0

    .line 30
    sput-object p0, Lhost/exp/exponent/experience/ErrorActivity;->mErrorConsoleFragment:Lhost/exp/exponent/experience/ErrorConsoleFragment;

    return-object p0
.end method

.method static synthetic access$100(Lhost/exp/exponent/experience/ErrorActivity;)Ljava/lang/String;
    .locals 0

    .line 30
    iget-object p0, p0, Lhost/exp/exponent/experience/ErrorActivity;->mManifestUrl:Ljava/lang/String;

    return-object p0
.end method

.method public static addError(Lhost/exp/exponent/kernel/ExponentError;)V
    .locals 2

    .line 63
    sget-object v0, Lhost/exp/exponent/experience/ErrorActivity;->sErrorList:Ljava/util/LinkedList;

    monitor-enter v0

    .line 64
    :try_start_0
    sget-object v1, Lhost/exp/exponent/experience/ErrorActivity;->sErrorList:Ljava/util/LinkedList;

    invoke-virtual {v1, p0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 65
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    sget-object p0, Lhost/exp/exponent/experience/ErrorActivity;->sVisibleActivity:Lhost/exp/exponent/experience/ErrorActivity;

    if-eqz p0, :cond_0

    sget-object v0, Lhost/exp/exponent/experience/ErrorActivity;->mErrorConsoleFragment:Lhost/exp/exponent/experience/ErrorConsoleFragment;

    if-eqz v0, :cond_0

    .line 68
    new-instance v0, Lhost/exp/exponent/experience/ErrorActivity$1;

    invoke-direct {v0}, Lhost/exp/exponent/experience/ErrorActivity$1;-><init>()V

    invoke-virtual {p0, v0}, Lhost/exp/exponent/experience/ErrorActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void

    :catchall_0
    move-exception p0

    .line 65
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p0
.end method

.method public static clearErrorList()V
    .locals 2

    .line 80
    sget-object v0, Lhost/exp/exponent/experience/ErrorActivity;->sErrorList:Ljava/util/LinkedList;

    monitor-enter v0

    .line 81
    :try_start_0
    sget-object v1, Lhost/exp/exponent/experience/ErrorActivity;->sErrorList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 82
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getErrorList()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Lhost/exp/exponent/kernel/ExponentError;",
            ">;"
        }
    .end annotation

    .line 86
    sget-object v0, Lhost/exp/exponent/experience/ErrorActivity;->sErrorList:Ljava/util/LinkedList;

    return-object v0
.end method

.method public static getVisibleActivity()Lhost/exp/exponent/experience/ErrorActivity;
    .locals 1

    .line 55
    sget-object v0, Lhost/exp/exponent/experience/ErrorActivity;->sVisibleActivity:Lhost/exp/exponent/experience/ErrorActivity;

    return-object v0
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    .line 59
    iget-object v0, p0, Lhost/exp/exponent/experience/ErrorActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 2

    .line 128
    iget-object v0, p0, Lhost/exp/exponent/experience/ErrorActivity;->mPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v0

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Lhost/exp/exponent/experience/ErrorActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0, p0}, Lhost/exp/exponent/kernel/Kernel;->killActivityStack(Landroid/app/Activity;)V

    goto :goto_0

    .line 131
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/experience/ErrorActivity;->mPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    :goto_0
    return-void
.end method

.method public onClickHome()V
    .locals 2

    .line 136
    invoke-static {}, Lhost/exp/exponent/experience/ErrorActivity;->clearErrorList()V

    .line 138
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lhost/exp/exponent/LauncherActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 139
    invoke-virtual {p0, v0}, Lhost/exp/exponent/experience/ErrorActivity;->startActivity(Landroid/content/Intent;)V

    .line 142
    sget-object v0, Lhost/exp/exponent/experience/ErrorActivity;->sVisibleActivity:Lhost/exp/exponent/experience/ErrorActivity;

    if-ne v0, p0, :cond_0

    const/4 v0, 0x0

    .line 143
    sput-object v0, Lhost/exp/exponent/experience/ErrorActivity;->sVisibleActivity:Lhost/exp/exponent/experience/ErrorActivity;

    .line 146
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/experience/ErrorActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0, p0}, Lhost/exp/exponent/kernel/Kernel;->killActivityStack(Landroid/app/Activity;)V

    return-void
.end method

.method public onClickReload()V
    .locals 2

    .line 150
    iget-object v0, p0, Lhost/exp/exponent/experience/ErrorActivity;->mManifestUrl:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 151
    invoke-static {}, Lhost/exp/exponent/experience/ErrorActivity;->clearErrorList()V

    .line 154
    sget-object v0, Lhost/exp/exponent/experience/ErrorActivity;->sVisibleActivity:Lhost/exp/exponent/experience/ErrorActivity;

    if-ne v0, p0, :cond_0

    .line 155
    sput-object v1, Lhost/exp/exponent/experience/ErrorActivity;->sVisibleActivity:Lhost/exp/exponent/experience/ErrorActivity;

    .line 158
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/experience/ErrorActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0, p0}, Lhost/exp/exponent/kernel/Kernel;->killActivityStack(Landroid/app/Activity;)V

    .line 159
    iget-object v0, p0, Lhost/exp/exponent/experience/ErrorActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    iget-object v1, p0, Lhost/exp/exponent/experience/ErrorActivity;->mManifestUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhost/exp/exponent/kernel/Kernel;->reloadVisibleExperience(Ljava/lang/String;)Z

    goto :goto_0

    .line 162
    :cond_1
    sget-object v0, Lhost/exp/exponent/experience/ErrorActivity;->sVisibleActivity:Lhost/exp/exponent/experience/ErrorActivity;

    if-ne v0, p0, :cond_2

    .line 163
    sput-object v1, Lhost/exp/exponent/experience/ErrorActivity;->sVisibleActivity:Lhost/exp/exponent/experience/ErrorActivity;

    .line 166
    :cond_2
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ErrorActivity;->finish()V

    :goto_0
    return-void
.end method

.method public onClickViewErrorLog()V
    .locals 2

    .line 171
    iget-object v0, p0, Lhost/exp/exponent/experience/ErrorActivity;->mPager:Landroidx/viewpager/widget/ViewPager;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v0

    if-nez v0, :cond_0

    .line 172
    iget-object v0, p0, Lhost/exp/exponent/experience/ErrorActivity;->mPager:Landroidx/viewpager/widget/ViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 91
    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 92
    sget p1, Lhost/exp/expoview/R$layout;->error_activity_new:I

    invoke-virtual {p0, p1}, Lhost/exp/exponent/experience/ErrorActivity;->setContentView(I)V

    .line 93
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 95
    invoke-static {}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->getInstance()Lhost/exp/exponent/di/NativeModuleDepsProvider;

    move-result-object p1

    const-class v0, Lhost/exp/exponent/experience/ErrorActivity;

    invoke-virtual {p1, v0, p0}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->inject(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 97
    invoke-static {p0}, Lhost/exp/exponent/experience/ExperienceActivity;->removeNotification(Landroid/content/Context;)V

    .line 99
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ErrorActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "manifestUrl"

    .line 100
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lhost/exp/exponent/experience/ErrorActivity;->mManifestUrl:Ljava/lang/String;

    .line 101
    iget-object p1, p0, Lhost/exp/exponent/experience/ErrorActivity;->mManifestUrl:Ljava/lang/String;

    if-nez p1, :cond_0

    sget-object p1, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 102
    sget-object p1, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    iput-object p1, p0, Lhost/exp/exponent/experience/ErrorActivity;->mManifestUrl:Ljava/lang/String;

    .line 105
    :cond_0
    new-instance p1, Lhost/exp/exponent/experience/ErrorActivity$ViewPagerAdapter;

    invoke-virtual {p0}, Lhost/exp/exponent/experience/ErrorActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-direct {p1, p0, v0}, Lhost/exp/exponent/experience/ErrorActivity$ViewPagerAdapter;-><init>(Lhost/exp/exponent/experience/ErrorActivity;Landroidx/fragment/app/FragmentManager;)V

    iput-object p1, p0, Lhost/exp/exponent/experience/ErrorActivity;->mPagerAdapter:Landroidx/viewpager/widget/PagerAdapter;

    .line 106
    iget-object p1, p0, Lhost/exp/exponent/experience/ErrorActivity;->mPager:Landroidx/viewpager/widget/ViewPager;

    iget-object v0, p0, Lhost/exp/exponent/experience/ErrorActivity;->mPagerAdapter:Landroidx/viewpager/widget/PagerAdapter;

    invoke-virtual {p1, v0}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 119
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onPause()V

    .line 121
    sget-object v0, Lhost/exp/exponent/experience/ErrorActivity;->sVisibleActivity:Lhost/exp/exponent/experience/ErrorActivity;

    if-ne v0, p0, :cond_0

    const/4 v0, 0x0

    .line 122
    sput-object v0, Lhost/exp/exponent/experience/ErrorActivity;->sVisibleActivity:Lhost/exp/exponent/experience/ErrorActivity;

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 111
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onResume()V

    .line 113
    sput-object p0, Lhost/exp/exponent/experience/ErrorActivity;->sVisibleActivity:Lhost/exp/exponent/experience/ErrorActivity;

    .line 114
    iget-object v0, p0, Lhost/exp/exponent/experience/ErrorActivity;->mManifestUrl:Ljava/lang/String;

    const-string v1, "ERROR_APPEARED"

    invoke-static {v1, v0}, Lhost/exp/exponent/analytics/Analytics;->logEventWithManifestUrl(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
