.class Lhost/exp/exponent/experience/BaseExperienceActivity$1;
.super Ljava/lang/Object;
.source "BaseExperienceActivity.java"

# interfaces
.implements Lhost/exp/exponent/utils/AsyncCondition$AsyncConditionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/experience/BaseExperienceActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/experience/BaseExperienceActivity;


# direct methods
.method constructor <init>(Lhost/exp/exponent/experience/BaseExperienceActivity;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lhost/exp/exponent/experience/BaseExperienceActivity$1;->this$0:Lhost/exp/exponent/experience/BaseExperienceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 3

    .line 108
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lhost/exp/exponent/experience/BaseExperienceActivity$ExperienceForegroundedEvent;

    iget-object v2, p0, Lhost/exp/exponent/experience/BaseExperienceActivity$1;->this$0:Lhost/exp/exponent/experience/BaseExperienceActivity;

    iget-object v2, v2, Lhost/exp/exponent/experience/BaseExperienceActivity;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    invoke-direct {v1, v2}, Lhost/exp/exponent/experience/BaseExperienceActivity$ExperienceForegroundedEvent;-><init>(Lhost/exp/exponent/kernel/ExperienceId;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    return-void
.end method

.method public isReady()Z
    .locals 1

    .line 103
    iget-object v0, p0, Lhost/exp/exponent/experience/BaseExperienceActivity$1;->this$0:Lhost/exp/exponent/experience/BaseExperienceActivity;

    iget-object v0, v0, Lhost/exp/exponent/experience/BaseExperienceActivity;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    if-nez v0, :cond_1

    iget-object v0, p0, Lhost/exp/exponent/experience/BaseExperienceActivity$1;->this$0:Lhost/exp/exponent/experience/BaseExperienceActivity;

    instance-of v0, v0, Lhost/exp/exponent/experience/HomeActivity;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
