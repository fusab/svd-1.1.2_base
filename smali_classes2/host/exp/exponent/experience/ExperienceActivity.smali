.class public Lhost/exp/exponent/experience/ExperienceActivity;
.super Lhost/exp/exponent/experience/BaseExperienceActivity;
.source "ExperienceActivity.java"

# interfaces
.implements Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;


# static fields
.field private static final KERNEL_STARTED_RUNNING_KEY:Ljava/lang/String; = "experienceActivityKernelDidLoad"

.field private static final NOTIFICATION_ID:I = 0x2775

.field private static final NUX_REACT_MODULE_NAME:Ljava/lang/String; = "ExperienceNuxApp"

.field private static READY_FOR_BUNDLE:Ljava/lang/String; = "readyForBundle"

.field private static final TAG:Ljava/lang/String; = "ExperienceActivity"


# instance fields
.field private mDevBundleDownloadProgressListener:Lhost/exp/exponent/experience/DevBundleDownloadProgressListener;

.field mExponentManifest:Lhost/exp/exponent/ExponentManifest;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mIntentUri:Ljava/lang/String;

.field private mIsLoadExperienceAllowedToRun:Z

.field private mIsReadyForBundle:Z

.field private mIsShellApp:Z

.field private mNotification:Lhost/exp/exponent/notifications/ExponentNotification;

.field private mNotificationAnimationFrame:I

.field private mNotificationAnimationHandler:Landroid/os/Handler;

.field private mNotificationAnimator:Ljava/lang/Runnable;

.field private mNotificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

.field private mNotificationRemoteViews:Landroid/widget/RemoteViews;

.field private mNuxOverlayView:Lversioned/host/exp/exponent/ReactUnthemedRootView;

.field private mShouldShowLoadingScreenWithOptimisticManifest:Z

.field private mTempNotification:Lhost/exp/exponent/notifications/ExponentNotification;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 73
    invoke-direct {p0}, Lhost/exp/exponent/experience/BaseExperienceActivity;-><init>()V

    const/4 v0, 0x0

    .line 108
    iput-boolean v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIsLoadExperienceAllowedToRun:Z

    .line 109
    iput-boolean v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mShouldShowLoadingScreenWithOptimisticManifest:Z

    .line 114
    new-instance v0, Lhost/exp/exponent/experience/ExperienceActivity$1;

    invoke-direct {v0, p0}, Lhost/exp/exponent/experience/ExperienceActivity$1;-><init>(Lhost/exp/exponent/experience/ExperienceActivity;)V

    iput-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mDevBundleDownloadProgressListener:Lhost/exp/exponent/experience/DevBundleDownloadProgressListener;

    return-void
.end method

.method static synthetic access$000(Lhost/exp/exponent/experience/ExperienceActivity;)Z
    .locals 0

    .line 73
    iget-boolean p0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mShouldShowLoadingScreenWithOptimisticManifest:Z

    return p0
.end method

.method static synthetic access$100(Lhost/exp/exponent/experience/ExperienceActivity;)Lhost/exp/exponent/notifications/ExponentNotification;
    .locals 0

    .line 73
    iget-object p0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mNotification:Lhost/exp/exponent/notifications/ExponentNotification;

    return-object p0
.end method

.method static synthetic access$1000()Ljava/lang/String;
    .locals 1

    .line 73
    sget-object v0, Lhost/exp/exponent/experience/ExperienceActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lhost/exp/exponent/experience/ExperienceActivity;Lhost/exp/exponent/notifications/ExponentNotification;)Lhost/exp/exponent/notifications/ExponentNotification;
    .locals 0

    .line 73
    iput-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mNotification:Lhost/exp/exponent/notifications/ExponentNotification;

    return-object p1
.end method

.method static synthetic access$200(Lhost/exp/exponent/experience/ExperienceActivity;)Lhost/exp/exponent/notifications/ExponentNotification;
    .locals 0

    .line 73
    iget-object p0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mTempNotification:Lhost/exp/exponent/notifications/ExponentNotification;

    return-object p0
.end method

.method static synthetic access$202(Lhost/exp/exponent/experience/ExperienceActivity;Lhost/exp/exponent/notifications/ExponentNotification;)Lhost/exp/exponent/notifications/ExponentNotification;
    .locals 0

    .line 73
    iput-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mTempNotification:Lhost/exp/exponent/notifications/ExponentNotification;

    return-object p1
.end method

.method static synthetic access$302(Lhost/exp/exponent/experience/ExperienceActivity;Z)Z
    .locals 0

    .line 73
    iput-boolean p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIsReadyForBundle:Z

    return p1
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .line 73
    sget-object v0, Lhost/exp/exponent/experience/ExperienceActivity;->READY_FOR_BUNDLE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lhost/exp/exponent/experience/ExperienceActivity;Lorg/json/JSONObject;)V
    .locals 0

    .line 73
    invoke-direct {p0, p1}, Lhost/exp/exponent/experience/ExperienceActivity;->handleExperienceOptions(Lorg/json/JSONObject;)V

    return-void
.end method

.method static synthetic access$600(Lhost/exp/exponent/experience/ExperienceActivity;)Ljava/lang/String;
    .locals 0

    .line 73
    iget-object p0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIntentUri:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$700(Lhost/exp/exponent/experience/ExperienceActivity;)Z
    .locals 0

    .line 73
    iget-boolean p0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIsShellApp:Z

    return p0
.end method

.method static synthetic access$800(Lhost/exp/exponent/experience/ExperienceActivity;)Lhost/exp/exponent/experience/DevBundleDownloadProgressListener;
    .locals 0

    .line 73
    iget-object p0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mDevBundleDownloadProgressListener:Lhost/exp/exponent/experience/DevBundleDownloadProgressListener;

    return-object p0
.end method

.method static synthetic access$900(Lhost/exp/exponent/experience/ExperienceActivity;)Lversioned/host/exp/exponent/ReactUnthemedRootView;
    .locals 0

    .line 73
    iget-object p0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mNuxOverlayView:Lversioned/host/exp/exponent/ReactUnthemedRootView;

    return-object p0
.end method

.method static synthetic access$902(Lhost/exp/exponent/experience/ExperienceActivity;Lversioned/host/exp/exponent/ReactUnthemedRootView;)Lversioned/host/exp/exponent/ReactUnthemedRootView;
    .locals 0

    .line 73
    iput-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mNuxOverlayView:Lversioned/host/exp/exponent/ReactUnthemedRootView;

    return-object p1
.end method

.method private addNotification(Lorg/json/JSONObject;)V
    .locals 7

    .line 600
    iget-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    if-eqz p1, :cond_5

    iget-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifest:Lorg/json/JSONObject;

    if-nez p1, :cond_0

    goto/16 :goto_1

    .line 604
    :cond_0
    iget-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifest:Lorg/json/JSONObject;

    const/4 v0, 0x0

    const-string v1, "name"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    .line 609
    :cond_1
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifest:Lorg/json/JSONObject;

    const-string v1, "androidShowExponentNotificationInShellApp"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIsShellApp:Z

    if-eqz v0, :cond_2

    return-void

    .line 613
    :cond_2
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Lhost/exp/exponent/experience/ExperienceActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIsShellApp:Z

    if-eqz v2, :cond_3

    sget v2, Lhost/exp/expoview/R$layout;->notification_shell_app:I

    goto :goto_0

    :cond_3
    sget v2, Lhost/exp/expoview/R$layout;->notification:I

    :goto_0
    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 614
    sget v1, Lhost/exp/expoview/R$id;->home_text_button:I

    const-string v2, "setText"

    invoke-virtual {v0, v1, v2, p1}, Landroid/widget/RemoteViews;->setCharSequence(ILjava/lang/String;Ljava/lang/CharSequence;)V

    .line 617
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lhost/exp/exponent/LauncherActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 618
    sget v2, Lhost/exp/expoview/R$id;->home_image_button:I

    const/4 v3, 0x0

    invoke-static {p0, v3, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 624
    sget v1, Lhost/exp/expoview/R$id;->home_text_button:I

    iget-object v2, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    .line 625
    invoke-static {p0, v2}, Lhost/exp/exponent/ExponentIntentService;->getActionInfoScreen(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v4, 0x8000000

    .line 624
    invoke-static {p0, v3, v2, v4}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 627
    iget-boolean v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIsShellApp:Z

    if-nez v1, :cond_4

    .line 630
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "text/plain"

    .line 631
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 632
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " on Exponent"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v5, "android.intent.extra.SUBJECT"

    invoke-virtual {v1, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 633
    iget-object v2, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    const-string v5, "android.intent.extra.TEXT"

    invoke-virtual {v1, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 634
    sget v2, Lhost/exp/expoview/R$id;->share_button:I

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Share a link to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 635
    invoke-static {v1, p1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object p1

    .line 634
    invoke-static {p0, v3, p1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p1

    invoke-virtual {v0, v2, p1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 638
    sget p1, Lhost/exp/expoview/R$id;->save_button:I

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    .line 639
    invoke-static {p0, v1}, Lhost/exp/exponent/ExponentIntentService;->getActionSaveExperience(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 638
    invoke-static {p0, v3, v1, v4}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 644
    :cond_4
    sget p1, Lhost/exp/expoview/R$id;->reload_button:I

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    .line 645
    invoke-static {p0, v1}, Lhost/exp/exponent/ExponentIntentService;->getActionReloadExperience(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 644
    invoke-static {p0, v3, v1, v4}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 647
    iput-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mNotificationRemoteViews:Landroid/widget/RemoteViews;

    const-string p1, "notification"

    .line 650
    invoke-virtual {p0, p1}, Lhost/exp/exponent/experience/ExperienceActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/NotificationManager;

    const/16 v0, 0x2775

    .line 651
    invoke-virtual {p1, v0}, Landroid/app/NotificationManager;->cancel(I)V

    .line 653
    new-instance v1, Lhost/exp/exponent/notifications/ExponentNotificationManager;

    invoke-direct {v1, p0}, Lhost/exp/exponent/notifications/ExponentNotificationManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lhost/exp/exponent/notifications/ExponentNotificationManager;->maybeCreateExpoPersistentNotificationChannel()V

    .line 654
    new-instance v1, Landroidx/core/app/NotificationCompat$Builder;

    const-string v2, "expo-experience"

    invoke-direct {v1, p0, v2}, Landroidx/core/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v2, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mNotificationRemoteViews:Landroid/widget/RemoteViews;

    .line 655
    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setContent(Landroid/widget/RemoteViews;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    sget v2, Lhost/exp/expoview/R$drawable;->notification_icon:I

    .line 656
    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setSmallIcon(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    .line 657
    invoke-virtual {v1, v3}, Landroidx/core/app/NotificationCompat$Builder;->setShowWhen(Z)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 658
    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setOngoing(Z)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    const/4 v2, 0x2

    .line 659
    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setPriority(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    iput-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mNotificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

    .line 661
    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mNotificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

    sget v2, Lhost/exp/expoview/R$color;->colorPrimary:I

    invoke-static {p0, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setColor(I)Landroidx/core/app/NotificationCompat$Builder;

    .line 662
    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mNotificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

    invoke-virtual {v1}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    :cond_5
    :goto_1
    return-void
.end method

.method private addNuxView()V
    .locals 2

    .line 684
    new-instance v0, Lhost/exp/exponent/experience/ExperienceActivity$7;

    invoke-direct {v0, p0}, Lhost/exp/exponent/experience/ExperienceActivity$7;-><init>(Lhost/exp/exponent/experience/ExperienceActivity;)V

    const-string v1, "experienceActivityKernelDidLoad"

    invoke-static {v1, v0}, Lhost/exp/exponent/utils/AsyncCondition;->wait(Ljava/lang/String;Lhost/exp/exponent/utils/AsyncCondition$AsyncConditionListener;)V

    return-void
.end method

.method private handleExperienceOptions(Lorg/json/JSONObject;)V
    .locals 1

    if-eqz p1, :cond_0

    :try_start_0
    const-string v0, "loadNux"

    .line 674
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-boolean p1, Lhost/exp/exponent/Constants;->DISABLE_NUX:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 678
    sget-object v0, Lhost/exp/exponent/experience/ExperienceActivity;->TAG:Ljava/lang/String;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void
.end method

.method private handleUri(Ljava/lang/String;)V
    .locals 2

    .line 552
    new-instance v0, Landroid/content/Intent;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 553
    invoke-super {p0, v0}, Lhost/exp/exponent/experience/BaseExperienceActivity;->onNewIntent(Landroid/content/Intent;)V

    return-void
.end method

.method private removeNotification()V
    .locals 1

    const/4 v0, 0x0

    .line 666
    iput-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mNotificationRemoteViews:Landroid/widget/RemoteViews;

    .line 667
    iput-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mNotificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

    .line 668
    invoke-static {p0}, Lhost/exp/exponent/experience/ExperienceActivity;->removeNotification(Landroid/content/Context;)V

    return-void
.end method

.method public static removeNotification(Landroid/content/Context;)V
    .locals 1

    const-string v0, "notification"

    .line 707
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/app/NotificationManager;

    const/16 v0, 0x2775

    .line 708
    invoke-virtual {p0, v0}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method


# virtual methods
.method public dismissNuxViewIfVisible(Z)V
    .locals 1

    .line 720
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mNuxOverlayView:Lversioned/host/exp/exponent/ReactUnthemedRootView;

    if-eqz v0, :cond_0

    .line 721
    new-instance v0, Lhost/exp/exponent/experience/ExperienceActivity$8;

    invoke-direct {v0, p0, p1}, Lhost/exp/exponent/experience/ExperienceActivity$8;-><init>(Lhost/exp/exponent/experience/ExperienceActivity;Z)V

    invoke-virtual {p0, v0}, Lhost/exp/exponent/experience/ExperienceActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public emitUpdatesEvent(Lorg/json/JSONObject;)V
    .locals 4

    .line 557
    invoke-static {}, Lhost/exp/exponent/kernel/KernelProvider;->getInstance()Lhost/exp/exponent/kernel/KernelInterface;

    move-result-object v0

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    new-instance v2, Lhost/exp/exponent/kernel/KernelConstants$ExperienceEvent;

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v3, "Exponent.nativeUpdatesEvent"

    invoke-direct {v2, v3, p1}, Lhost/exp/exponent/kernel/KernelConstants$ExperienceEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lhost/exp/exponent/kernel/KernelInterface;->addEventForExperience(Ljava/lang/String;Lhost/exp/exponent/kernel/KernelConstants$ExperienceEvent;)V

    return-void
.end method

.method public expoPackages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/unimodules/core/interfaces/Package;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getExperienceId()Ljava/lang/String;
    .locals 1

    .line 805
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mExperienceIdString:Ljava/lang/String;

    return-object v0
.end method

.method public getExponentPackageDelegate()Lversioned/host/exp/exponent/ExponentPackageDelegate;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public handleOptions(Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;)V
    .locals 11

    .line 521
    :try_start_0
    iget-object v0, p1, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->uri:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x2

    const-string v2, "emit"

    const-string v3, "getJSModule"

    const-string v4, "getCurrentReactContext"

    const-string v5, "com.facebook.react.modules.core.DeviceEventManagerModule$RCTDeviceEventEmitter"

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eqz v0, :cond_1

    .line 522
    :try_start_1
    iget-object v0, p1, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->uri:Ljava/lang/String;

    invoke-direct {p0, v0}, Lhost/exp/exponent/experience/ExperienceActivity;->handleUri(Ljava/lang/String;)V

    .line 523
    iget-object v0, p1, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->uri:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 526
    new-instance v8, Lhost/exp/exponent/RNObject;

    invoke-direct {v8, v5}, Lhost/exp/exponent/RNObject;-><init>(Ljava/lang/String;)V

    .line 527
    iget-object v9, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mDetachSdkVersion:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lhost/exp/exponent/RNObject;->loadVersion(Ljava/lang/String;)Lhost/exp/exponent/RNObject;

    .line 529
    iget-object v9, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    new-array v10, v7, [Ljava/lang/Object;

    invoke-virtual {v9, v4, v10}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object v9

    new-array v10, v6, [Ljava/lang/Object;

    .line 530
    invoke-virtual {v8}, Lhost/exp/exponent/RNObject;->rnClass()Ljava/lang/Class;

    move-result-object v8

    aput-object v8, v10, v7

    invoke-virtual {v9, v3, v10}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object v8

    new-array v9, v1, [Ljava/lang/Object;

    const-string v10, "Exponent.openUri"

    aput-object v10, v9, v7

    aput-object v0, v9, v6

    .line 531
    invoke-virtual {v8, v2, v9}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 534
    :cond_0
    iget-object v0, p1, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->uri:Ljava/lang/String;

    iget-object v8, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mDetachSdkVersion:Ljava/lang/String;

    invoke-static {p0, v0, v8}, Lhost/exp/exponent/branch/BranchManager;->handleLink(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    :cond_1
    iget-object v0, p1, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->notification:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p1, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->notificationObject:Lhost/exp/exponent/notifications/ExponentNotification;

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mDetachSdkVersion:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 538
    new-instance v0, Lhost/exp/exponent/RNObject;

    invoke-direct {v0, v5}, Lhost/exp/exponent/RNObject;-><init>(Ljava/lang/String;)V

    .line 539
    iget-object v5, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mDetachSdkVersion:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lhost/exp/exponent/RNObject;->loadVersion(Ljava/lang/String;)Lhost/exp/exponent/RNObject;

    .line 541
    iget-object v5, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    new-array v8, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v4, v8}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    .line 542
    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->rnClass()Ljava/lang/Class;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {v4, v3, v5}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "Exponent.notification"

    aput-object v3, v1, v7

    iget-object p1, p1, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->notificationObject:Lhost/exp/exponent/notifications/ExponentNotification;

    iget-object v3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mDetachSdkVersion:Ljava/lang/String;

    const-string v4, "selected"

    .line 543
    invoke-virtual {p1, v3, v4}, Lhost/exp/exponent/notifications/ExponentNotification;->toWriteableMap(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    aput-object p1, v1, v6

    invoke-virtual {v0, v2, v1}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 546
    sget-object v0, Lhost/exp/exponent/experience/ExperienceActivity;->TAG:Ljava/lang/String;

    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public handleUnreadNotifications(Lorg/json/JSONArray;)V
    .locals 1

    .line 582
    invoke-static {}, Lhost/exp/exponent/notifications/PushNotificationHelper;->getInstance()Lhost/exp/exponent/notifications/PushNotificationHelper;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 584
    invoke-virtual {v0, p0, p1}, Lhost/exp/exponent/notifications/PushNotificationHelper;->removeNotifications(Landroid/content/Context;Lorg/json/JSONArray;)V

    :cond_0
    return-void
.end method

.method public isDebugModeEnabled()Z
    .locals 1

    .line 562
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifest:Lorg/json/JSONObject;

    invoke-static {v0}, Lhost/exp/exponent/ExponentManifest;->isDebugModeEnabled(Lorg/json/JSONObject;)Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .line 154
    invoke-super {p0, p1}, Lhost/exp/exponent/experience/BaseExperienceActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    .line 156
    iput-boolean v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIsLoadExperienceAllowedToRun:Z

    .line 157
    iput-boolean v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mShouldShowLoadingScreenWithOptimisticManifest:Z

    .line 159
    invoke-static {}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->getInstance()Lhost/exp/exponent/di/NativeModuleDepsProvider;

    move-result-object v1

    const-class v2, Lhost/exp/exponent/experience/ExperienceActivity;

    invoke-virtual {v1, v2, p0}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->inject(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 160
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v1

    invoke-virtual {v1, p0}, Lde/greenrobot/event/EventBus;->registerSticky(Ljava/lang/Object;)V

    .line 162
    invoke-static {}, Lhost/exp/exponent/utils/ExpoActivityIds;->getNextAppActivityId()I

    move-result v1

    iput v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mActivityId:I

    const-string v1, "experienceUrl"

    if-eqz p1, :cond_0

    .line 170
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 172
    iput-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    .line 178
    :cond_0
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ExperienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const/4 v2, 0x0

    if-eqz p1, :cond_2

    .line 179
    iget-object v3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 180
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 182
    iput-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    :cond_1
    const-string v1, "isOptimistic"

    .line 187
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 v0, 0x0

    .line 193
    :cond_2
    iget-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    if-eqz p1, :cond_3

    if-eqz v0, :cond_3

    .line 194
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ExperienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "loadFromCache"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    .line 195
    new-instance v0, Lhost/exp/exponent/experience/ExperienceActivity$2;

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    invoke-direct {v0, p0, v1, p1}, Lhost/exp/exponent/experience/ExperienceActivity$2;-><init>(Lhost/exp/exponent/experience/ExperienceActivity;Ljava/lang/String;Z)V

    .line 241
    invoke-virtual {v0}, Lhost/exp/exponent/experience/ExperienceActivity$2;->start()V

    .line 244
    :cond_3
    iget-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {p0}, Lhost/exp/exponent/experience/ExperienceActivity;->getTaskId()I

    move-result v0

    invoke-virtual {p1, p0, v0}, Lhost/exp/exponent/kernel/Kernel;->setOptimisticActivity(Lhost/exp/exponent/experience/ExperienceActivity;I)V

    return-void
.end method

.method protected onDoneLoading()V
    .locals 1

    .line 302
    sget-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_LOADING_REACT_NATIVE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0}, Lhost/exp/exponent/analytics/Analytics;->markEvent(Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    .line 303
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    invoke-static {v0}, Lhost/exp/exponent/analytics/Analytics;->sendTimedEvents(Ljava/lang/String;)V

    return-void
.end method

.method protected onError(Landroid/content/Intent;)V
    .locals 2

    .line 763
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 764
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    const-string v1, "manifestUrl"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method protected onError(Lhost/exp/exponent/kernel/ExponentError;)V
    .locals 4

    .line 770
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifest:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    return-void

    .line 774
    :cond_0
    invoke-virtual {p1}, Lhost/exp/exponent/kernel/ExponentError;->toJSONObject()Lorg/json/JSONObject;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    .line 779
    :cond_1
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifest:Lorg/json/JSONObject;

    const-string v1, "id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    return-void

    .line 784
    :cond_2
    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-virtual {v1, v0}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getExperienceMetadata(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    if-nez v1, :cond_3

    .line 786
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    :cond_3
    const-string v2, "lastErrors"

    .line 789
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    if-nez v3, :cond_4

    .line 791
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 794
    :cond_4
    invoke-virtual {v3, p1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 797
    :try_start_0
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 798
    iget-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-virtual {p1, v0, v1}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->updateExperienceMetadata(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 800
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public onEvent(Lhost/exp/exponent/experience/ReactNativeActivity$ExperienceDoneLoadingEvent;)V
    .locals 0

    .line 590
    iget-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {p1}, Lhost/exp/exponent/kernel/Kernel;->startJSKernel()V

    return-void
.end method

.method public onEventMainThread(Lhost/exp/exponent/kernel/Kernel$KernelStartedRunningEvent;)V
    .locals 0

    const-string p1, "experienceActivityKernelDidLoad"

    .line 297
    invoke-static {p1}, Lhost/exp/exponent/utils/AsyncCondition;->notify(Ljava/lang/String;)V

    return-void
.end method

.method public onEventMainThread(Lhost/exp/exponent/notifications/ReceivedNotificationEvent;)V
    .locals 6

    .line 505
    iget-object v0, p1, Lhost/exp/exponent/notifications/ReceivedNotificationEvent;->experienceId:Ljava/lang/String;

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mExperienceIdString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 507
    :try_start_0
    new-instance v0, Lhost/exp/exponent/RNObject;

    const-string v1, "com.facebook.react.modules.core.DeviceEventManagerModule$RCTDeviceEventEmitter"

    invoke-direct {v0, v1}, Lhost/exp/exponent/RNObject;-><init>(Ljava/lang/String;)V

    .line 508
    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mDetachSdkVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhost/exp/exponent/RNObject;->loadVersion(Ljava/lang/String;)Lhost/exp/exponent/RNObject;

    .line 510
    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    const-string v2, "getCurrentReactContext"

    const/4 v3, 0x0

    new-array v4, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v4}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object v1

    const-string v2, "getJSModule"

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    .line 511
    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->rnClass()Ljava/lang/Class;

    move-result-object v0

    aput-object v0, v5, v3

    invoke-virtual {v1, v2, v5}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object v0

    const-string v1, "emit"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const-string v5, "Exponent.notification"

    aput-object v5, v2, v3

    iget-object v3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mDetachSdkVersion:Ljava/lang/String;

    const-string v5, "received"

    .line 512
    invoke-virtual {p1, v3, v5}, Lhost/exp/exponent/notifications/ReceivedNotificationEvent;->toWriteableMap(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    aput-object p1, v2, v4

    invoke-virtual {v0, v1, v2}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 514
    sget-object v0, Lhost/exp/exponent/experience/ExperienceActivity;->TAG:Ljava/lang/String;

    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .line 288
    invoke-super {p0, p1}, Lhost/exp/exponent/experience/BaseExperienceActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 290
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 292
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lhost/exp/exponent/experience/ExperienceActivity;->handleUri(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onNotificationAction()V
    .locals 1

    const/4 v0, 0x1

    .line 712
    invoke-virtual {p0, v0}, Lhost/exp/exponent/experience/ExperienceActivity;->dismissNuxViewIfVisible(Z)V

    return-void
.end method

.method protected onPause()V
    .locals 0

    .line 273
    invoke-super {p0}, Lhost/exp/exponent/experience/BaseExperienceActivity;->onPause()V

    .line 275
    invoke-direct {p0}, Lhost/exp/exponent/experience/ExperienceActivity;->removeNotification()V

    .line 276
    invoke-static {}, Lhost/exp/exponent/analytics/Analytics;->clearTimedEvents()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 249
    invoke-super {p0}, Lhost/exp/exponent/experience/BaseExperienceActivity;->onResume()V

    .line 251
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ExperienceActivity;->soloaderInit()V

    const/4 v0, 0x0

    .line 253
    invoke-direct {p0, v0}, Lhost/exp/exponent/experience/ExperienceActivity;->addNotification(Lorg/json/JSONObject;)V

    .line 254
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    const-string v1, "EXPERIENCE_APPEARED"

    invoke-static {v1, v0}, Lhost/exp/exponent/analytics/Analytics;->logEventWithManifestUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ExperienceActivity;->registerForNotifications()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 281
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    const-string v1, "experienceUrl"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    invoke-super {p0, p1}, Lhost/exp/exponent/experience/BaseExperienceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public reactPackages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/facebook/react/ReactPackage;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public setBundle(Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x0

    .line 484
    iput-boolean v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mShouldShowLoadingScreenWithOptimisticManifest:Z

    .line 485
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ExperienceActivity;->isDebugModeEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 486
    iget-boolean v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIsReadyForBundle:Z

    .line 487
    sget-object v1, Lhost/exp/exponent/experience/ExperienceActivity;->READY_FOR_BUNDLE:Ljava/lang/String;

    new-instance v2, Lhost/exp/exponent/experience/ExperienceActivity$5;

    invoke-direct {v2, p0, v0, p1}, Lhost/exp/exponent/experience/ExperienceActivity$5;-><init>(Lhost/exp/exponent/experience/ExperienceActivity;ZLjava/lang/String;)V

    invoke-static {v1, v2}, Lhost/exp/exponent/utils/AsyncCondition;->wait(Ljava/lang/String;Lhost/exp/exponent/utils/AsyncCondition$AsyncConditionListener;)V

    :cond_0
    return-void
.end method

.method public setLoadingScreenManifest(Lorg/json/JSONObject;)V
    .locals 1

    .line 313
    new-instance v0, Lhost/exp/exponent/experience/ExperienceActivity$3;

    invoke-direct {v0, p0, p1}, Lhost/exp/exponent/experience/ExperienceActivity$3;-><init>(Lhost/exp/exponent/experience/ExperienceActivity;Lorg/json/JSONObject;)V

    invoke-virtual {p0, v0}, Lhost/exp/exponent/experience/ExperienceActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setManifest(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 4

    .line 337
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ExperienceActivity;->isInForeground()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 341
    :cond_0
    iget-boolean v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIsLoadExperienceAllowedToRun:Z

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x0

    .line 347
    iput-boolean v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIsLoadExperienceAllowedToRun:Z

    .line 349
    iput-boolean v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIsReadyForBundle:Z

    .line 351
    iput-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    .line 352
    iput-object p2, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifest:Lorg/json/JSONObject;

    .line 354
    new-instance v1, Lhost/exp/exponent/notifications/ExponentNotificationManager;

    invoke-direct {v1, p0}, Lhost/exp/exponent/notifications/ExponentNotificationManager;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifest:Lorg/json/JSONObject;

    invoke-virtual {v1, v2}, Lhost/exp/exponent/notifications/ExponentNotificationManager;->maybeCreateNotificationChannelGroup(Lorg/json/JSONObject;)V

    .line 356
    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    iget-object v2, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lhost/exp/exponent/kernel/Kernel;->getExperienceActivityTask(Ljava/lang/String;)Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;

    move-result-object v1

    .line 357
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ExperienceActivity;->getTaskId()I

    move-result v2

    iput v2, v1, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->taskId:I

    .line 358
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v1, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->experienceActivity:Ljava/lang/ref/WeakReference;

    .line 359
    iget v2, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mActivityId:I

    iput v2, v1, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->activityId:I

    .line 360
    iput-object p3, v1, Lhost/exp/exponent/kernel/Kernel$ExperienceActivityTask;->bundleUrl:Ljava/lang/String;

    const-string p3, "sdkVersion"

    .line 362
    invoke-virtual {p2, p3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    iput-object p3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mSDKVersion:Ljava/lang/String;

    .line 363
    sget-object p3, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    iput-boolean p3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIsShellApp:Z

    .line 367
    iget-object p3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mSDKVersion:Ljava/lang/String;

    const-string v1, "35.0.0"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    const-string v1, "UNVERSIONED"

    if-eqz p3, :cond_2

    .line 368
    iput-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mSDKVersion:Ljava/lang/String;

    .line 371
    :cond_2
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result p3

    if-eqz p3, :cond_3

    move-object p3, v1

    goto :goto_0

    :cond_3
    iget-object p3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mSDKVersion:Ljava/lang/String;

    :goto_0
    iput-object p3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mDetachSdkVersion:Ljava/lang/String;

    .line 373
    iget-object p3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mSDKVersion:Ljava/lang/String;

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_6

    .line 375
    sget-object p3, Lhost/exp/exponent/Constants;->SDK_VERSIONS_LIST:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_4
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 376
    iget-object v3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mSDKVersion:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 p3, 0x1

    goto :goto_1

    :cond_5
    const/4 p3, 0x0

    :goto_1
    if-nez p3, :cond_6

    .line 383
    invoke-static {}, Lhost/exp/exponent/kernel/KernelProvider;->getInstance()Lhost/exp/exponent/kernel/KernelInterface;

    move-result-object p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mSDKVersion:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, " is not a valid SDK version. Options are "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p3, Lhost/exp/exponent/Constants;->SDK_VERSIONS_LIST:Ljava/util/List;

    const-string p4, ", "

    .line 384
    invoke-static {p4, p3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "."

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 383
    invoke-virtual {p1, p2}, Lhost/exp/exponent/kernel/KernelInterface;->handleError(Ljava/lang/String;)V

    return-void

    .line 389
    :cond_6
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ExperienceActivity;->soloaderInit()V

    :try_start_0
    const-string p3, "id"

    .line 392
    invoke-virtual {p2, p3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    iput-object p3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mExperienceIdString:Ljava/lang/String;

    .line 393
    iget-object p3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mExperienceIdString:Ljava/lang/String;

    invoke-static {p3}, Lhost/exp/exponent/kernel/ExperienceId;->create(Ljava/lang/String;)Lhost/exp/exponent/kernel/ExperienceId;

    move-result-object p3

    iput-object p3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    const-string p3, "experienceIdSetForActivity"

    .line 394
    invoke-static {p3}, Lhost/exp/exponent/utils/AsyncCondition;->notify(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 399
    iput-boolean v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIsCrashed:Z

    .line 401
    iget-object p3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mSDKVersion:Ljava/lang/String;

    const-string v1, "LOAD_EXPERIENCE"

    invoke-static {v1, p3, v0}, Lhost/exp/exponent/analytics/Analytics;->logEventWithManifestUrlSdkVersion(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    iget-object p3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifest:Lorg/json/JSONObject;

    invoke-static {p3, p0}, Lhost/exp/exponent/utils/ExperienceActivityUtils;->updateOrientation(Lorg/json/JSONObject;Landroid/app/Activity;)V

    .line 404
    invoke-direct {p0, p4}, Lhost/exp/exponent/experience/ExperienceActivity;->addNotification(Lorg/json/JSONObject;)V

    const/4 p3, 0x0

    .line 407
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {v0, p1}, Lhost/exp/exponent/kernel/Kernel;->hasOptionsForManifestUrl(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 408
    iget-object p3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    invoke-virtual {p3, p1}, Lhost/exp/exponent/kernel/Kernel;->popOptionsForManifestUrl(Ljava/lang/String;)Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;

    move-result-object p1

    .line 412
    iget-object p3, p1, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->uri:Ljava/lang/String;

    if-eqz p3, :cond_7

    .line 413
    iget-object p3, p1, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->uri:Ljava/lang/String;

    iput-object p3, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIntentUri:Ljava/lang/String;

    .line 416
    :cond_7
    iget-object p3, p1, Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;->notificationObject:Lhost/exp/exponent/notifications/ExponentNotification;

    .line 423
    :cond_8
    iget-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIntentUri:Ljava/lang/String;

    if-eqz p1, :cond_9

    sget-object v0, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_b

    .line 424
    :cond_9
    sget-object p1, Lhost/exp/exponent/Constants;->SHELL_APP_SCHEME:Ljava/lang/String;

    if-eqz p1, :cond_a

    .line 425
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lhost/exp/exponent/Constants;->SHELL_APP_SCHEME:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "://"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIntentUri:Ljava/lang/String;

    goto :goto_2

    .line 427
    :cond_a
    iget-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    iput-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIntentUri:Ljava/lang/String;

    .line 433
    :cond_b
    :goto_2
    iget-object p1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mIntentUri:Ljava/lang/String;

    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mDetachSdkVersion:Ljava/lang/String;

    invoke-static {p0, p1, v0}, Lhost/exp/exponent/branch/BranchManager;->handleLink(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    new-instance p1, Lhost/exp/exponent/experience/ExperienceActivity$4;

    invoke-direct {p1, p0, p3, p2, p4}, Lhost/exp/exponent/experience/ExperienceActivity$4;-><init>(Lhost/exp/exponent/experience/ExperienceActivity;Lhost/exp/exponent/notifications/ExponentNotification;Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    invoke-virtual {p0, p1}, Lhost/exp/exponent/experience/ExperienceActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void

    .line 396
    :catch_0
    invoke-static {}, Lhost/exp/exponent/kernel/KernelProvider;->getInstance()Lhost/exp/exponent/kernel/KernelInterface;

    move-result-object p1

    const-string p2, "No ID found in manifest."

    invoke-virtual {p1, p2}, Lhost/exp/exponent/kernel/KernelInterface;->handleError(Ljava/lang/String;)V

    return-void
.end method

.method public shouldCheckOptions()V
    .locals 2

    .line 266
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhost/exp/exponent/kernel/Kernel;->hasOptionsForManifestUrl(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mKernel:Lhost/exp/exponent/kernel/Kernel;

    iget-object v1, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifestUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhost/exp/exponent/kernel/Kernel;->popOptionsForManifestUrl(Ljava/lang/String;)Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhost/exp/exponent/experience/ExperienceActivity;->handleOptions(Lhost/exp/exponent/kernel/KernelConstants$ExperienceOptions;)V

    :cond_0
    return-void
.end method

.method public soloaderInit()V
    .locals 1

    .line 260
    iget-object v0, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mDetachSdkVersion:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 261
    invoke-static {p0, v0}, Lcom/facebook/soloader/SoLoader;->init(Landroid/content/Context;Z)V

    :cond_0
    return-void
.end method

.method protected startReactInstance()V
    .locals 4

    .line 567
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    invoke-virtual {p0}, Lhost/exp/exponent/experience/ExperienceActivity;->isDebugModeEnabled()Z

    move-result v1

    iget-object v2, p0, Lhost/exp/exponent/experience/ExperienceActivity;->mManifest:Lorg/json/JSONObject;

    new-instance v3, Lhost/exp/exponent/experience/ExperienceActivity$6;

    invoke-direct {v3, p0}, Lhost/exp/exponent/experience/ExperienceActivity$6;-><init>(Lhost/exp/exponent/experience/ExperienceActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Lhost/exp/expoview/Exponent;->testPackagerStatus(ZLorg/json/JSONObject;Lhost/exp/expoview/Exponent$PackagerStatusCallback;)V

    return-void
.end method
