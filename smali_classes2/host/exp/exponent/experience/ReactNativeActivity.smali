.class public abstract Lhost/exp/exponent/experience/ReactNativeActivity;
.super Landroidx/fragment/app/FragmentActivity;
.source "ReactNativeActivity.java"

# interfaces
.implements Lcom/facebook/react/modules/core/DefaultHardwareBackBtnHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhost/exp/exponent/experience/ReactNativeActivity$ExperienceDoneLoadingEvent;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ReactNativeActivity"

.field private static final VIEW_TEST_INTERVAL_MS:J = 0x14L

.field protected static sErrorQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lhost/exp/exponent/kernel/ExponentError;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected mActivityId:I

.field private mContainer:Landroid/widget/FrameLayout;

.field protected mDetachSdkVersion:Ljava/lang/String;

.field private mDoubleTapReloadRecognizer:Lcom/facebook/react/devsupport/DoubleTapReloadRecognizer;

.field protected mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

.field protected mExperienceIdString:Ljava/lang/String;

.field mExpoKernelServiceRegistry:Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field protected mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field protected mIsCrashed:Z

.field protected mIsInForeground:Z

.field protected mIsLoading:Z

.field protected mJSBundlePath:Ljava/lang/String;

.field private mLayout:Landroid/widget/FrameLayout;

.field private mLoadingHandler:Landroid/os/Handler;

.field private mLoadingView:Lhost/exp/exponent/LoadingView;

.field protected mManifest:Lorg/json/JSONObject;

.field protected mManifestUrl:Ljava/lang/String;

.field protected mReactInstanceManager:Lhost/exp/exponent/RNObject;

.field protected mReactRootView:Lhost/exp/exponent/RNObject;

.field protected mSDKVersion:Ljava/lang/String;

.field protected mShouldDestroyRNInstanceOnExit:Z

.field private mSplashScreenKernelService:Lhost/exp/exponent/kernel/services/SplashScreenKernelService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 115
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lhost/exp/exponent/experience/ReactNativeActivity;->sErrorQueue:Ljava/util/Queue;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 64
    invoke-direct {p0}, Landroidx/fragment/app/FragmentActivity;-><init>()V

    .line 89
    new-instance v0, Lhost/exp/exponent/RNObject;

    const-string v1, "com.facebook.react.ReactInstanceManager"

    invoke-direct {v0, v1}, Lhost/exp/exponent/RNObject;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    const/4 v0, 0x0

    .line 90
    iput-boolean v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsCrashed:Z

    const/4 v1, 0x1

    .line 91
    iput-boolean v1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mShouldDestroyRNInstanceOnExit:Z

    .line 107
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mHandler:Landroid/os/Handler;

    .line 108
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLoadingHandler:Landroid/os/Handler;

    .line 111
    iput-boolean v1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsLoading:Z

    .line 114
    iput-boolean v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsInForeground:Z

    return-void
.end method

.method static synthetic access$000(Lhost/exp/exponent/experience/ReactNativeActivity;)V
    .locals 0

    .line 64
    invoke-direct {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->hideLoadingScreen()V

    return-void
.end method

.method private canHideLoadingScreen()Z
    .locals 1

    .line 268
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mSplashScreenKernelService:Lhost/exp/exponent/kernel/services/SplashScreenKernelService;

    invoke-virtual {v0}, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;->isAppLoadingStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mSplashScreenKernelService:Lhost/exp/exponent/kernel/services/SplashScreenKernelService;

    invoke-virtual {v0}, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;->isAppLoadingFinished()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mSplashScreenKernelService:Lhost/exp/exponent/kernel/services/SplashScreenKernelService;

    invoke-virtual {v0}, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;->shouldAutoHide()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private fadeLoadingScreen()V
    .locals 1

    .line 237
    iget-boolean v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsLoading:Z

    if-nez v0, :cond_0

    return-void

    .line 240
    :cond_0
    new-instance v0, Lhost/exp/exponent/experience/ReactNativeActivity$2;

    invoke-direct {v0, p0}, Lhost/exp/exponent/experience/ReactNativeActivity$2;-><init>(Lhost/exp/exponent/experience/ReactNativeActivity;)V

    invoke-virtual {p0, v0}, Lhost/exp/exponent/experience/ReactNativeActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private getLinkingUri()Ljava/lang/String;
    .locals 4

    .line 599
    sget-object v0, Lhost/exp/exponent/Constants;->SHELL_APP_SCHEME:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 600
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lhost/exp/exponent/Constants;->SHELL_APP_SCHEME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 602
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mManifestUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 603
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    const-string v2, "exp.host"

    .line 604
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "expo.io"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "exp.direct"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "expo.test"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, ".exp.host"

    .line 605
    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, ".expo.io"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, ".exp.direct"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, ".expo.test"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 606
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    .line 607
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v2, 0x0

    .line 608
    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 610
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "--"

    .line 611
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 614
    :cond_2
    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    :cond_3
    :goto_1
    const-string v1, "--/"

    .line 617
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 619
    :cond_4
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mManifestUrl:Ljava/lang/String;

    return-object v0
.end method

.method private hideLoadingScreen()V
    .locals 3

    .line 250
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lhost/exp/exponent/Constants;->SHOW_LOADING_VIEW_IN_SHELL_APP:Z

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 252
    iget-object v1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 253
    iget-object v1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 256
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLoadingView:Lhost/exp/exponent/LoadingView;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lhost/exp/exponent/LoadingView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v2, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLayout:Landroid/widget/FrameLayout;

    if-ne v0, v2, :cond_1

    .line 257
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLoadingView:Lhost/exp/exponent/LoadingView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lhost/exp/exponent/LoadingView;->setAlpha(F)V

    .line 258
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLoadingView:Lhost/exp/exponent/LoadingView;

    invoke-virtual {v0, v1}, Lhost/exp/exponent/LoadingView;->setShowIcon(Z)V

    .line 259
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLoadingView:Lhost/exp/exponent/LoadingView;

    invoke-virtual {v0}, Lhost/exp/exponent/LoadingView;->setDoneLoading()V

    .line 262
    :cond_1
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mSplashScreenKernelService:Lhost/exp/exponent/kernel/services/SplashScreenKernelService;

    invoke-virtual {v0}, Lhost/exp/exponent/kernel/services/SplashScreenKernelService;->reset()V

    .line 263
    iput-boolean v1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsLoading:Z

    .line 264
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLoadingHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method

.method private pollForEventsToSendToRN()V
    .locals 8

    .line 574
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mManifestUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    return-void

    .line 579
    :cond_0
    :try_start_0
    new-instance v0, Lhost/exp/exponent/RNObject;

    const-string v1, "com.facebook.react.modules.core.DeviceEventManagerModule$RCTDeviceEventEmitter"

    invoke-direct {v0, v1}, Lhost/exp/exponent/RNObject;-><init>(Ljava/lang/String;)V

    .line 580
    iget-object v1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mDetachSdkVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhost/exp/exponent/RNObject;->loadVersion(Ljava/lang/String;)Lhost/exp/exponent/RNObject;

    .line 581
    iget-object v1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    const-string v2, "getCurrentReactContext"

    const/4 v3, 0x0

    new-array v4, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v4}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object v1

    const-string v2, "getJSModule"

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    .line 582
    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->rnClass()Ljava/lang/Class;

    move-result-object v0

    aput-object v0, v5, v3

    invoke-virtual {v1, v2, v5}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 585
    invoke-static {}, Lhost/exp/exponent/kernel/KernelProvider;->getInstance()Lhost/exp/exponent/kernel/KernelInterface;

    move-result-object v1

    iget-object v2, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mManifestUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lhost/exp/exponent/kernel/KernelInterface;->consumeExperienceEvents(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    .line 587
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhost/exp/exponent/kernel/KernelConstants$ExperienceEvent;

    const-string v5, "emit"

    const/4 v6, 0x2

    .line 588
    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, v2, Lhost/exp/exponent/kernel/KernelConstants$ExperienceEvent;->eventName:Ljava/lang/String;

    aput-object v7, v6, v3

    iget-object v2, v2, Lhost/exp/exponent/kernel/KernelConstants$ExperienceEvent;->eventPayload:Ljava/lang/String;

    aput-object v2, v6, v4

    invoke-virtual {v0, v5, v6}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 592
    sget-object v1, Lhost/exp/exponent/experience/ReactNativeActivity;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected addView(Landroid/view/View;)V
    .locals 1

    .line 175
    invoke-virtual {p0, p1}, Lhost/exp/exponent/experience/ReactNativeActivity;->removeViewFromParent(Landroid/view/View;)V

    .line 176
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method protected checkForReactViews()V
    .locals 4

    .line 206
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactRootView:Lhost/exp/exponent/RNObject;

    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->isNull()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 210
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactRootView:Lhost/exp/exponent/RNObject;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "getChildCount"

    invoke-virtual {v0, v2, v1}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_2

    .line 211
    invoke-direct {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->canHideLoadingScreen()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 212
    invoke-direct {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->fadeLoadingScreen()V

    .line 214
    :cond_1
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->onDoneLoading()V

    .line 215
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    invoke-static {v0}, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->getInstance(Lhost/exp/exponent/kernel/ExperienceId;)Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;

    move-result-object v0

    invoke-virtual {v0}, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->markExperienceLoaded()V

    .line 217
    invoke-direct {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->pollForEventsToSendToRN()V

    goto :goto_0

    .line 219
    :cond_2
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lhost/exp/exponent/experience/ReactNativeActivity$1;

    invoke-direct {v1, p0}, Lhost/exp/exponent/experience/ReactNativeActivity$1;-><init>(Lhost/exp/exponent/experience/ReactNativeActivity;)V

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void
.end method

.method public getRootView()Landroid/view/View;
    .locals 1

    .line 132
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactRootView:Lhost/exp/exponent/RNObject;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 135
    :cond_0
    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public initialProps(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0

    return-object p1
.end method

.method public invokeDefaultOnBackPressed()V
    .locals 0

    .line 305
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onBackPressed()V

    return-void
.end method

.method public isDebugModeEnabled()Z
    .locals 1

    .line 355
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mManifest:Lorg/json/JSONObject;

    invoke-static {v0}, Lhost/exp/exponent/ExponentManifest;->isDebugModeEnabled(Lorg/json/JSONObject;)Z

    move-result v0

    return v0
.end method

.method public isInForeground()Z
    .locals 1

    .line 128
    iget-boolean v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsInForeground:Z

    return v0
.end method

.method public isLoading()Z
    .locals 1

    .line 124
    iget-boolean v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsLoading:Z

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .line 383
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 385
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lhost/exp/expoview/Exponent;->onActivityResult(IILandroid/content/Intent;)V

    .line 387
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->isNotNull()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsCrashed:Z

    if-nez v0, :cond_0

    .line 388
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v3, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, v3

    const/4 p2, 0x3

    aput-object p3, v2, p2

    const-string p2, "onActivityResult"

    invoke-virtual {v0, p2, v2}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const/16 p2, 0x7b

    if-ne p1, p2, :cond_1

    .line 395
    iput-boolean v1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsInForeground:Z

    .line 396
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->startReactInstance()V

    :cond_1
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .line 296
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->isNotNull()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsCrashed:Z

    if-nez v0, :cond_0

    .line 297
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "onBackPressed"

    invoke-virtual {v0, v2, v1}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 299
    :cond_0
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onBackPressed()V

    :goto_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    const/4 p1, 0x0

    .line 141
    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 143
    new-instance p1, Landroid/widget/FrameLayout;

    invoke-direct {p1, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLayout:Landroid/widget/FrameLayout;

    .line 144
    iget-object p1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0, p1}, Lhost/exp/exponent/experience/ReactNativeActivity;->setContentView(Landroid/view/View;)V

    .line 146
    new-instance p1, Landroid/widget/FrameLayout;

    invoke-direct {p1, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mContainer:Landroid/widget/FrameLayout;

    .line 147
    iget-object p1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLayout:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 148
    new-instance p1, Lhost/exp/exponent/LoadingView;

    invoke-direct {p1, p0}, Lhost/exp/exponent/LoadingView;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLoadingView:Lhost/exp/exponent/LoadingView;

    .line 149
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-boolean p1, Lhost/exp/exponent/Constants;->SHOW_LOADING_VIEW_IN_SHELL_APP:Z

    if-eqz p1, :cond_1

    .line 150
    :cond_0
    iget-object p1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mContainer:Landroid/widget/FrameLayout;

    sget v0, Lhost/exp/expoview/R$color;->splashBackground:I

    invoke-static {p0, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 151
    iget-object p1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLayout:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLoadingView:Lhost/exp/exponent/LoadingView;

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 154
    :cond_1
    new-instance p1, Lcom/facebook/react/devsupport/DoubleTapReloadRecognizer;

    invoke-direct {p1}, Lcom/facebook/react/devsupport/DoubleTapReloadRecognizer;-><init>()V

    iput-object p1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mDoubleTapReloadRecognizer:Lcom/facebook/react/devsupport/DoubleTapReloadRecognizer;

    .line 155
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->getApplication()Landroid/app/Application;

    move-result-object p1

    invoke-static {p0, p1}, Lhost/exp/expoview/Exponent;->initialize(Landroid/content/Context;Landroid/app/Application;)V

    .line 156
    invoke-static {}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->getInstance()Lhost/exp/exponent/di/NativeModuleDepsProvider;

    move-result-object p1

    const-class v0, Lhost/exp/exponent/experience/ReactNativeActivity;

    invoke-virtual {p1, v0, p0}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->inject(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 157
    iget-object p1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mExpoKernelServiceRegistry:Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;

    invoke-virtual {p1}, Lhost/exp/exponent/kernel/services/ExpoKernelServiceRegistry;->getSplashScreenKernelService()Lhost/exp/exponent/kernel/services/SplashScreenKernelService;

    move-result-object p1

    iput-object p1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mSplashScreenKernelService:Lhost/exp/exponent/kernel/services/SplashScreenKernelService;

    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .line 329
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onDestroy()V

    .line 331
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->isNotNull()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsCrashed:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mShouldDestroyRNInstanceOnExit:Z

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "destroy"

    invoke-virtual {v0, v2, v1}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 336
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLoadingHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 337
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->unregister(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDoneLoading()V
    .locals 0

    return-void
.end method

.method public onEvent(Lhost/exp/exponent/experience/BaseExperienceActivity$ExperienceContentLoaded;)V
    .locals 0

    .line 570
    invoke-direct {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->fadeLoadingScreen()V

    return-void
.end method

.method public onEventMainThread(Lhost/exp/exponent/kernel/KernelConstants$AddedExperienceEventEvent;)V
    .locals 1

    .line 564
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mManifestUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object p1, p1, Lhost/exp/exponent/kernel/KernelConstants$AddedExperienceEventEvent;->manifestUrl:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 565
    invoke-direct {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->pollForEventsToSendToRN()V

    :cond_0
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 5

    .line 273
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->isNotNull()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsCrashed:Z

    if-nez v0, :cond_1

    const/16 v0, 0x52

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p1, v0, :cond_0

    .line 275
    iget-object p1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    new-array p2, v2, [Ljava/lang/Object;

    const-string v0, "showDevOptionsDialog"

    invoke-virtual {p1, v0, p2}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    return v1

    .line 278
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "getDevSupportManager"

    invoke-virtual {v0, v4, v3}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 279
    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "getDevSupportEnabled"

    invoke-virtual {v0, v4, v3}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 280
    iget-object v3, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mDoubleTapReloadRecognizer:Lcom/facebook/react/devsupport/DoubleTapReloadRecognizer;

    invoke-static {v3}, Lcom/facebook/infer/annotation/Assertions;->assertNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/react/devsupport/DoubleTapReloadRecognizer;

    .line 281
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Lcom/facebook/react/devsupport/DoubleTapReloadRecognizer;->didDoubleTapR(ILandroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 285
    iget-object p1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mManifest:Lorg/json/JSONObject;

    invoke-virtual {p0, p1}, Lhost/exp/exponent/experience/ReactNativeActivity;->showLoadingScreen(Lorg/json/JSONObject;)V

    .line 286
    new-array p1, v2, [Ljava/lang/Object;

    const-string p2, "handleReloadJS"

    invoke-virtual {v0, p2, p1}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    return v1

    .line 291
    :cond_1
    invoke-super {p0, p1, p2}, Landroidx/fragment/app/FragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 4

    .line 342
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->isNotNull()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsCrashed:Z

    if-nez v0, :cond_0

    .line 344
    :try_start_0
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    const-string v1, "onNewIntent"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 346
    sget-object v1, Lhost/exp/exponent/experience/ReactNativeActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    goto :goto_0

    .line 350
    :cond_0
    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    :goto_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 310
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onPause()V

    .line 312
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->isNotNull()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsCrashed:Z

    if-nez v0, :cond_0

    .line 313
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->onHostPause()V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 320
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onResume()V

    .line 322
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->isNotNull()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsCrashed:Z

    if-nez v0, :cond_0

    .line 323
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    invoke-virtual {v0, p0, p0}, Lhost/exp/exponent/RNObject;->onHostResume(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method protected removeViewFromParent(Landroid/view/View;)V
    .locals 1

    .line 180
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 181
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method protected removeViews()V
    .locals 1

    .line 201
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    return-void
.end method

.method protected setView(Landroid/view/View;)V
    .locals 2

    .line 165
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 166
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lhost/exp/exponent/Constants;->SHOW_LOADING_VIEW_IN_SHELL_APP:Z

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, 0x0

    .line 168
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 169
    iget-object v1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 171
    :cond_0
    invoke-virtual {p0, p1}, Lhost/exp/exponent/experience/ReactNativeActivity;->addView(Landroid/view/View;)V

    return-void
.end method

.method protected shouldShowErrorScreen(Lhost/exp/exponent/kernel/ExponentErrorMessage;)Z
    .locals 3

    .line 530
    iget-boolean v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsLoading:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 536
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    invoke-static {v0}, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->getInstance(Lhost/exp/exponent/kernel/ExperienceId;)Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;

    move-result-object v0

    .line 538
    invoke-virtual {v0}, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->markErrored()V

    .line 540
    invoke-virtual {v0}, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->shouldReloadOnError()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 541
    invoke-static {}, Lhost/exp/exponent/kernel/KernelProvider;->getInstance()Lhost/exp/exponent/kernel/KernelInterface;

    move-result-object v0

    iget-object v2, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mManifestUrl:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lhost/exp/exponent/kernel/KernelInterface;->reloadVisibleExperience(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    .line 545
    :cond_1
    sget-object v0, Lhost/exp/exponent/experience/ReactNativeActivity;->sErrorQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 548
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "USER_ERROR_MESSAGE"

    .line 549
    invoke-virtual {p1}, Lhost/exp/exponent/kernel/ExponentErrorMessage;->userErrorMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "DEVELOPER_ERROR_MESSAGE"

    .line 550
    invoke-virtual {p1}, Lhost/exp/exponent/kernel/ExponentErrorMessage;->developerErrorMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "MANIFEST_URL"

    .line 551
    iget-object v1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mManifestUrl:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "ERROR_RELOADED"

    .line 552
    invoke-static {p1, v0}, Lhost/exp/exponent/analytics/Analytics;->logEvent(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 554
    sget-object v0, Lhost/exp/exponent/experience/ReactNativeActivity;->TAG:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const/4 p1, 0x0

    return p1

    :cond_2
    return v1
.end method

.method public showLoadingScreen(Lorg/json/JSONObject;)V
    .locals 2

    .line 229
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLoadingView:Lhost/exp/exponent/LoadingView;

    invoke-virtual {v0, p1}, Lhost/exp/exponent/LoadingView;->setManifest(Lorg/json/JSONObject;)V

    .line 230
    iget-object p1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLoadingView:Lhost/exp/exponent/LoadingView;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lhost/exp/exponent/LoadingView;->setShowIcon(Z)V

    .line 231
    iget-object p1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLoadingView:Lhost/exp/exponent/LoadingView;

    invoke-virtual {p1}, Lhost/exp/exponent/LoadingView;->clearAnimation()V

    .line 232
    iget-object p1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLoadingView:Lhost/exp/exponent/LoadingView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p1, v1}, Lhost/exp/exponent/LoadingView;->setAlpha(F)V

    .line 233
    iput-boolean v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsLoading:Z

    return-void
.end method

.method public startReactInstance(Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/notifications/ExponentNotification;ZLjava/util/List;Ljava/util/List;Lhost/exp/exponent/experience/DevBundleDownloadProgressListener;)Lhost/exp/exponent/RNObject;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lhost/exp/exponent/notifications/ExponentNotification;",
            "Z",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/List<",
            "Lorg/unimodules/core/interfaces/Package;",
            ">;",
            "Lhost/exp/exponent/experience/DevBundleDownloadProgressListener;",
            ")",
            "Lhost/exp/exponent/RNObject;"
        }
    .end annotation

    move-object v1, p0

    move-object/from16 v0, p3

    move-object/from16 v2, p4

    move-object/from16 v3, p7

    .line 404
    iget-boolean v4, v1, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsCrashed:Z

    const-string v5, "com.facebook.react.ReactInstanceManager"

    if-nez v4, :cond_a

    invoke-interface {p1}, Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;->isInForeground()Z

    move-result v4

    if-nez v4, :cond_0

    goto/16 :goto_a

    .line 410
    :cond_0
    invoke-direct {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->getLinkingUri()Ljava/lang/String;

    move-result-object v9

    .line 411
    iget-object v7, v1, Lhost/exp/exponent/experience/ReactNativeActivity;->mManifestUrl:Ljava/lang/String;

    const/4 v4, 0x0

    .line 415
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    const-string v6, "experienceUrl"

    const-string v8, "linkingUri"

    const-string v10, "intentUri"

    const-string v12, "isHeadless"

    move-object/from16 v11, p2

    .line 411
    invoke-static/range {v6 .. v13}, Lcom/facebook/react/common/MapBuilder;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v6

    .line 418
    new-instance v7, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;

    invoke-direct {v7}, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;-><init>()V

    .line 419
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->getApplication()Landroid/app/Application;

    move-result-object v8

    iput-object v8, v7, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;->application:Landroid/app/Application;

    .line 420
    iget-object v8, v1, Lhost/exp/exponent/experience/ReactNativeActivity;->mJSBundlePath:Ljava/lang/String;

    iput-object v8, v7, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;->jsBundlePath:Ljava/lang/String;

    .line 421
    iput-object v6, v7, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;->experienceProperties:Ljava/util/Map;

    .line 422
    iput-object v3, v7, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;->expoPackages:Ljava/util/List;

    .line 423
    invoke-interface {p1}, Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;->getExponentPackageDelegate()Lversioned/host/exp/exponent/ExponentPackageDelegate;

    move-result-object v6

    iput-object v6, v7, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;->exponentPackageDelegate:Lversioned/host/exp/exponent/ExponentPackageDelegate;

    .line 424
    iget-object v6, v1, Lhost/exp/exponent/experience/ReactNativeActivity;->mManifest:Lorg/json/JSONObject;

    iput-object v6, v7, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;->manifest:Lorg/json/JSONObject;

    .line 425
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    iget-object v8, v1, Lhost/exp/exponent/experience/ReactNativeActivity;->mManifest:Lorg/json/JSONObject;

    invoke-static {v6, v8, v3}, Lversioned/host/exp/exponent/ExponentPackage;->getOrCreateSingletonModules(Landroid/content/Context;Lorg/json/JSONObject;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v7, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;->singletonModules:Ljava/util/List;

    .line 427
    new-instance v3, Lhost/exp/exponent/RNObject;

    const-string v6, "host.exp.exponent.VersionedUtils"

    invoke-direct {v3, v6}, Lhost/exp/exponent/RNObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Lhost/exp/exponent/RNObject;->loadVersion(Ljava/lang/String;)Lhost/exp/exponent/RNObject;

    move-result-object v3

    const/4 v6, 0x1

    .line 428
    new-array v8, v6, [Ljava/lang/Object;

    aput-object v7, v8, v4

    const-string v7, "getReactInstanceManagerBuilder"

    invoke-virtual {v3, v7, v8}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object v3

    if-eqz p6, :cond_1

    .line 431
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 432
    new-array v9, v6, [Ljava/lang/Object;

    aput-object v8, v9, v4

    const-string v8, "addPackage"

    invoke-virtual {v3, v8, v9}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 436
    :cond_1
    invoke-interface {p1}, Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;->isDebugModeEnabled()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 437
    iget-object v7, v1, Lhost/exp/exponent/experience/ReactNativeActivity;->mManifest:Lorg/json/JSONObject;

    const-string v8, "debuggerHost"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 438
    iget-object v8, v1, Lhost/exp/exponent/experience/ReactNativeActivity;->mManifest:Lorg/json/JSONObject;

    const-string v9, "mainModuleName"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 439
    invoke-static {v0, v7, v8, v3}, Lhost/exp/expoview/Exponent;->enableDeveloperSupport(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/RNObject;)V

    .line 441
    new-instance v7, Lhost/exp/exponent/RNObject;

    const-string v8, "com.facebook.react.devsupport.DevLoadingViewController"

    invoke-direct {v7, v8}, Lhost/exp/exponent/RNObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Lhost/exp/exponent/RNObject;->loadVersion(Ljava/lang/String;)Lhost/exp/exponent/RNObject;

    move-result-object v7

    .line 442
    new-array v8, v6, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v8, v4

    const-string v9, "setDevLoadingEnabled"

    invoke-virtual {v7, v9, v8}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    .line 444
    new-instance v7, Lhost/exp/exponent/RNObject;

    const-string v8, "host.exp.exponent.ExponentDevBundleDownloadListener"

    invoke-direct {v7, v8}, Lhost/exp/exponent/RNObject;-><init>(Ljava/lang/String;)V

    .line 445
    invoke-virtual {v7, v0}, Lhost/exp/exponent/RNObject;->loadVersion(Ljava/lang/String;)Lhost/exp/exponent/RNObject;

    move-result-object v0

    new-array v7, v6, [Ljava/lang/Object;

    aput-object p8, v7, v4

    .line 446
    invoke-virtual {v0, v7}, Lhost/exp/exponent/RNObject;->construct([Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object v0

    .line 447
    new-array v7, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->get()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v7, v4

    const-string v0, "setDevBundleDownloadListener"

    invoke-virtual {v3, v0, v7}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    goto :goto_1

    .line 449
    :cond_2
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->checkForReactViews()V

    .line 452
    :goto_1
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 453
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    if-eqz v2, :cond_3

    .line 455
    iget-object v0, v2, Lhost/exp/exponent/notifications/ExponentNotification;->body:Ljava/lang/String;

    const-string v9, "notification"

    invoke-virtual {v7, v9, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v0, "selected"

    .line 457
    invoke-virtual {v2, v0}, Lhost/exp/exponent/notifications/ExponentNotification;->toJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v8, v9, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 459
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    :cond_3
    :goto_2
    :try_start_1
    const-string v0, "manifest"

    .line 464
    iget-object v2, v1, Lhost/exp/exponent/experience/ReactNativeActivity;->mManifest:Lorg/json/JSONObject;

    invoke-virtual {v8, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "shell"

    move/from16 v2, p5

    .line 465
    invoke-virtual {v8, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "initialUri"

    if-nez p2, :cond_4

    const/4 v2, 0x0

    goto :goto_3

    .line 466
    :cond_4
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_3
    invoke-virtual {v8, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "errorRecovery"

    .line 467
    iget-object v2, v1, Lhost/exp/exponent/experience/ReactNativeActivity;->mExperienceId:Lhost/exp/exponent/kernel/ExperienceId;

    invoke-static {v2}, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->getInstance(Lhost/exp/exponent/kernel/ExperienceId;)Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;

    move-result-object v2

    invoke-virtual {v2}, Lhost/exp/exponent/kernel/services/ErrorRecoveryManager;->popRecoveryProps()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v8, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    :catch_1
    move-exception v0

    .line 469
    sget-object v2, Lhost/exp/exponent/experience/ReactNativeActivity;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 472
    :goto_4
    iget-object v0, v1, Lhost/exp/exponent/experience/ReactNativeActivity;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    iget-object v2, v1, Lhost/exp/exponent/experience/ReactNativeActivity;->mExperienceIdString:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getExperienceMetadata(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_7

    const-string v9, "lastErrors"

    .line 474
    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 477
    :try_start_2
    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 476
    invoke-virtual {v8, v9, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_5

    :catch_2
    move-exception v0

    .line 479
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 482
    :goto_5
    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    :cond_5
    const-string v9, "unreadNotifications"

    .line 488
    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 490
    :try_start_3
    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 491
    invoke-virtual {v8, v9, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_4

    move-object v10, p1

    .line 493
    :try_start_4
    invoke-interface {p1, v0}, Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;->handleUnreadNotifications(Lorg/json/JSONArray;)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_7

    :catch_3
    move-exception v0

    goto :goto_6

    :catch_4
    move-exception v0

    move-object v10, p1

    .line 495
    :goto_6
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 498
    :goto_7
    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    goto :goto_8

    :cond_6
    move-object v10, p1

    .line 501
    :goto_8
    iget-object v0, v1, Lhost/exp/exponent/experience/ReactNativeActivity;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    iget-object v9, v1, Lhost/exp/exponent/experience/ReactNativeActivity;->mExperienceIdString:Ljava/lang/String;

    invoke-virtual {v0, v9, v2}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->updateExperienceMetadata(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_9

    :cond_7
    move-object v10, p1

    .line 504
    :goto_9
    invoke-static {v8}, Lhost/exp/exponent/utils/JSONBundleConverter;->JSONToBundle(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "exp"

    invoke-virtual {v7, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 506
    invoke-interface {p1}, Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;->isInForeground()Z

    move-result v0

    if-nez v0, :cond_8

    .line 507
    new-instance v0, Lhost/exp/exponent/RNObject;

    invoke-direct {v0, v5}, Lhost/exp/exponent/RNObject;-><init>(Ljava/lang/String;)V

    return-object v0

    .line 510
    :cond_8
    sget-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_LOADING_REACT_NATIVE:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0}, Lhost/exp/exponent/analytics/Analytics;->markEvent(Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    .line 511
    new-array v0, v4, [Ljava/lang/Object;

    const-string v2, "build"

    invoke-virtual {v3, v2, v0}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object v0

    .line 512
    new-array v2, v4, [Ljava/lang/Object;

    const-string v3, "getDevSupportManager"

    invoke-virtual {v0, v3, v2}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const-string v5, "getDevSettings"

    invoke-virtual {v2, v5, v3}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 514
    iget v3, v1, Lhost/exp/exponent/experience/ReactNativeActivity;->mActivityId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v5, "exponentActivityId"

    invoke-virtual {v2, v5, v3}, Lhost/exp/exponent/RNObject;->setField(Ljava/lang/String;Ljava/lang/Object;)V

    .line 515
    new-array v3, v4, [Ljava/lang/Object;

    const-string v5, "isRemoteJSDebugEnabled"

    invoke-virtual {v2, v5, v3}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 516
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->checkForReactViews()V

    .line 520
    :cond_9
    invoke-virtual {v0, p0, p0}, Lhost/exp/exponent/RNObject;->onHostResume(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 521
    iget-object v2, v1, Lhost/exp/exponent/experience/ReactNativeActivity;->mReactRootView:Lhost/exp/exponent/RNObject;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    .line 522
    invoke-virtual {v0}, Lhost/exp/exponent/RNObject;->get()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    iget-object v4, v1, Lhost/exp/exponent/experience/ReactNativeActivity;->mManifest:Lorg/json/JSONObject;

    const-string v5, "appKey"

    const-string v8, "main"

    .line 523
    invoke-virtual {v4, v5, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x2

    .line 524
    invoke-virtual {p0, v7}, Lhost/exp/exponent/experience/ReactNativeActivity;->initialProps(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v5

    aput-object v5, v3, v4

    const-string v4, "startReactApplication"

    .line 521
    invoke-virtual {v2, v4, v3}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0

    .line 407
    :cond_a
    :goto_a
    new-instance v0, Lhost/exp/exponent/RNObject;

    invoke-direct {v0, v5}, Lhost/exp/exponent/RNObject;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method protected startReactInstance()V
    .locals 0

    return-void
.end method

.method protected stopLoading()V
    .locals 2

    .line 186
    iget-boolean v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsLoading:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->canHideLoadingScreen()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 189
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 190
    invoke-direct {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->hideLoadingScreen()V

    :cond_1
    :goto_0
    return-void
.end method

.method protected updateLoadingProgress(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1

    .line 194
    iget-boolean v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mIsLoading:Z

    if-nez v0, :cond_0

    .line 195
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mManifest:Lorg/json/JSONObject;

    invoke-virtual {p0, v0}, Lhost/exp/exponent/experience/ReactNativeActivity;->showLoadingScreen(Lorg/json/JSONObject;)V

    .line 197
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mLoadingView:Lhost/exp/exponent/LoadingView;

    invoke-virtual {v0, p1, p2, p3}, Lhost/exp/exponent/LoadingView;->updateProgress(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method protected waitForDrawOverOtherAppPermission(Ljava/lang/String;)V
    .locals 2

    .line 359
    iput-object p1, p0, Lhost/exp/exponent/experience/ReactNativeActivity;->mJSBundlePath:Ljava/lang/String;

    .line 361
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->isDebugModeEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object p1

    invoke-virtual {p1}, Lhost/exp/expoview/Exponent;->shouldRequestDrawOverOtherAppsPermission()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 362
    new-instance p1, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v0, "Please enable \"Permit drawing over other apps\""

    .line 363
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    const-string v0, "Click \"ok\" to open settings. Press the back button once you\'ve enabled the setting."

    .line 364
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x104000a

    new-instance v1, Lhost/exp/exponent/experience/ReactNativeActivity$3;

    invoke-direct {v1, p0}, Lhost/exp/exponent/experience/ReactNativeActivity$3;-><init>(Lhost/exp/exponent/experience/ReactNativeActivity;)V

    .line 365
    invoke-virtual {p1, v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 372
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    .line 373
    invoke-virtual {p1}, Landroidx/appcompat/app/AlertDialog$Builder;->show()Landroidx/appcompat/app/AlertDialog;

    return-void

    .line 378
    :cond_0
    invoke-virtual {p0}, Lhost/exp/exponent/experience/ReactNativeActivity;->startReactInstance()V

    return-void
.end method
