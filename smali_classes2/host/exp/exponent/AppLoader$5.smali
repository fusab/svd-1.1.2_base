.class Lhost/exp/exponent/AppLoader$5;
.super Ljava/lang/Object;
.source "AppLoader.java"

# interfaces
.implements Lhost/exp/expoview/Exponent$BundleListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/AppLoader;->fetchJSBundle(ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/AppLoader;

.field final synthetic val$finalManifest:Lorg/json/JSONObject;

.field final synthetic val$forceCache:Z

.field final synthetic val$shouldFailOnError:Z

.field final synthetic val$wasUpdated:Z


# direct methods
.method constructor <init>(Lhost/exp/exponent/AppLoader;ZZZLorg/json/JSONObject;)V
    .locals 0

    .line 292
    iput-object p1, p0, Lhost/exp/exponent/AppLoader$5;->this$0:Lhost/exp/exponent/AppLoader;

    iput-boolean p2, p0, Lhost/exp/exponent/AppLoader$5;->val$shouldFailOnError:Z

    iput-boolean p3, p0, Lhost/exp/exponent/AppLoader$5;->val$forceCache:Z

    iput-boolean p4, p0, Lhost/exp/exponent/AppLoader$5;->val$wasUpdated:Z

    iput-object p5, p0, Lhost/exp/exponent/AppLoader$5;->val$finalManifest:Lorg/json/JSONObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBundleLoaded(Ljava/lang/String;)V
    .locals 2

    .line 321
    iget-object v0, p0, Lhost/exp/exponent/AppLoader$5;->this$0:Lhost/exp/exponent/AppLoader;

    invoke-static {v0, p1}, Lhost/exp/exponent/AppLoader;->access$1102(Lhost/exp/exponent/AppLoader;Ljava/lang/String;)Ljava/lang/String;

    .line 322
    iget-object p1, p0, Lhost/exp/exponent/AppLoader$5;->this$0:Lhost/exp/exponent/AppLoader;

    invoke-static {p1}, Lhost/exp/exponent/AppLoader;->access$900(Lhost/exp/exponent/AppLoader;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 323
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    .line 325
    :try_start_0
    iget-boolean v0, p0, Lhost/exp/exponent/AppLoader$5;->val$wasUpdated:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "type"

    if-eqz v0, :cond_0

    :try_start_1
    const-string v0, "downloadFinished"

    .line 326
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "manifestString"

    .line 327
    iget-object v1, p0, Lhost/exp/exponent/AppLoader$5;->val$finalManifest:Lorg/json/JSONObject;

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    :cond_0
    const-string v0, "noUpdateAvailable"

    .line 329
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 331
    :goto_0
    iget-object v0, p0, Lhost/exp/exponent/AppLoader$5;->this$0:Lhost/exp/exponent/AppLoader;

    invoke-virtual {v0, p1}, Lhost/exp/exponent/AppLoader;->emitEvent(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 333
    invoke-static {}, Lhost/exp/exponent/AppLoader;->access$700()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 335
    :goto_1
    iget-object p1, p0, Lhost/exp/exponent/AppLoader$5;->this$0:Lhost/exp/exponent/AppLoader;

    iget-object p1, p1, Lhost/exp/exponent/AppLoader;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    iget-object v0, p0, Lhost/exp/exponent/AppLoader$5;->this$0:Lhost/exp/exponent/AppLoader;

    invoke-static {v0}, Lhost/exp/exponent/AppLoader;->access$1200(Lhost/exp/exponent/AppLoader;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lhost/exp/exponent/AppLoader$5;->val$finalManifest:Lorg/json/JSONObject;

    invoke-virtual {p1, v0, v1}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->updateSafeManifest(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 337
    :cond_1
    iget-object p1, p0, Lhost/exp/exponent/AppLoader$5;->this$0:Lhost/exp/exponent/AppLoader;

    invoke-static {p1}, Lhost/exp/exponent/AppLoader;->access$000(Lhost/exp/exponent/AppLoader;)V

    return-void
.end method

.method public onError(Ljava/lang/Exception;)V
    .locals 3

    .line 298
    iget-boolean v0, p0, Lhost/exp/exponent/AppLoader$5;->val$shouldFailOnError:Z

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lhost/exp/exponent/AppLoader$5;->this$0:Lhost/exp/exponent/AppLoader;

    invoke-static {v0, p1}, Lhost/exp/exponent/AppLoader;->access$300(Lhost/exp/exponent/AppLoader;Ljava/lang/Exception;)V

    return-void

    .line 302
    :cond_0
    iget-boolean v0, p0, Lhost/exp/exponent/AppLoader$5;->val$forceCache:Z

    if-eqz v0, :cond_1

    .line 303
    iget-object p1, p0, Lhost/exp/exponent/AppLoader$5;->this$0:Lhost/exp/exponent/AppLoader;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lhost/exp/exponent/AppLoader;->access$200(Lhost/exp/exponent/AppLoader;ZZ)V

    return-void

    .line 306
    :cond_1
    iget-object v0, p0, Lhost/exp/exponent/AppLoader$5;->this$0:Lhost/exp/exponent/AppLoader;

    invoke-static {v0}, Lhost/exp/exponent/AppLoader;->access$900(Lhost/exp/exponent/AppLoader;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 307
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "type"

    const-string v2, "error"

    .line 309
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "message"

    .line 310
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 311
    iget-object p1, p0, Lhost/exp/exponent/AppLoader$5;->this$0:Lhost/exp/exponent/AppLoader;

    invoke-virtual {p1, v0}, Lhost/exp/exponent/AppLoader;->emitEvent(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 313
    invoke-static {}, Lhost/exp/exponent/AppLoader;->access$700()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 316
    :cond_2
    :goto_0
    iget-object p1, p0, Lhost/exp/exponent/AppLoader$5;->this$0:Lhost/exp/exponent/AppLoader;

    invoke-static {p1}, Lhost/exp/exponent/AppLoader;->access$000(Lhost/exp/exponent/AppLoader;)V

    return-void
.end method
