.class Lhost/exp/exponent/LoadingView$3;
.super Ljava/lang/Object;
.source "LoadingView.java"

# interfaces
.implements Lcom/squareup/picasso/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/LoadingView;->setManifest(Lorg/json/JSONObject;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/LoadingView;

.field final synthetic val$iconUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhost/exp/exponent/LoadingView;Ljava/lang/String;)V
    .locals 0

    .line 141
    iput-object p1, p0, Lhost/exp/exponent/LoadingView$3;->this$0:Lhost/exp/exponent/LoadingView;

    iput-object p2, p0, Lhost/exp/exponent/LoadingView$3;->val$iconUrl:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError()V
    .locals 3

    .line 151
    invoke-static {}, Lhost/exp/exponent/LoadingView;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t load image at url "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lhost/exp/exponent/LoadingView$3;->val$iconUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onSuccess()V
    .locals 2

    .line 144
    iget-object v0, p0, Lhost/exp/exponent/LoadingView$3;->this$0:Lhost/exp/exponent/LoadingView;

    iget-object v1, v0, Lhost/exp/exponent/LoadingView;->mImageView:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Lhost/exp/exponent/LoadingView;->access$000(Lhost/exp/exponent/LoadingView;Landroid/view/View;)V

    .line 145
    iget-object v0, p0, Lhost/exp/exponent/LoadingView$3;->this$0:Lhost/exp/exponent/LoadingView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lhost/exp/exponent/LoadingView;->access$102(Lhost/exp/exponent/LoadingView;Z)Z

    const-string v0, "loadingViewImage"

    .line 146
    invoke-static {v0}, Lhost/exp/exponent/utils/AsyncCondition;->notify(Ljava/lang/String;)V

    return-void
.end method
