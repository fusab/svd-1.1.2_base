.class public Lhost/exp/exponent/LoadingView;
.super Landroid/widget/RelativeLayout;
.source "LoadingView.java"


# static fields
.field private static final ASYNC_CONDITION_KEY:Ljava/lang/String; = "loadingViewImage"

.field private static final PROGRESS_BAR_DELAY_MS:J = 0x9c4L

.field private static final TAG:Ljava/lang/String; = "LoadingView"


# instance fields
.field mBackgroundImageView:Landroid/widget/ImageView;

.field mImageView:Landroid/widget/ImageView;

.field private mIsLoading:Z

.field private mIsLoadingImageView:Z

.field mPercentageTextView:Landroid/widget/TextView;

.field mProgressBar:Landroid/widget/ProgressBar;

.field private mProgressBarHandler:Landroid/os/Handler;

.field private mShowIcon:Z

.field mStatusBarView:Landroid/view/View;

.field mStatusTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 45
    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lhost/exp/exponent/LoadingView;->mProgressBarHandler:Landroid/os/Handler;

    const/4 p1, 0x0

    .line 46
    iput-boolean p1, p0, Lhost/exp/exponent/LoadingView;->mShowIcon:Z

    .line 47
    iput-boolean p1, p0, Lhost/exp/exponent/LoadingView;->mIsLoading:Z

    .line 48
    iput-boolean p1, p0, Lhost/exp/exponent/LoadingView;->mIsLoadingImageView:Z

    .line 52
    invoke-direct {p0}, Lhost/exp/exponent/LoadingView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 56
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lhost/exp/exponent/LoadingView;->mProgressBarHandler:Landroid/os/Handler;

    const/4 p1, 0x0

    .line 46
    iput-boolean p1, p0, Lhost/exp/exponent/LoadingView;->mShowIcon:Z

    .line 47
    iput-boolean p1, p0, Lhost/exp/exponent/LoadingView;->mIsLoading:Z

    .line 48
    iput-boolean p1, p0, Lhost/exp/exponent/LoadingView;->mIsLoadingImageView:Z

    .line 57
    invoke-direct {p0}, Lhost/exp/exponent/LoadingView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lhost/exp/exponent/LoadingView;->mProgressBarHandler:Landroid/os/Handler;

    const/4 p1, 0x0

    .line 46
    iput-boolean p1, p0, Lhost/exp/exponent/LoadingView;->mShowIcon:Z

    .line 47
    iput-boolean p1, p0, Lhost/exp/exponent/LoadingView;->mIsLoading:Z

    .line 48
    iput-boolean p1, p0, Lhost/exp/exponent/LoadingView;->mIsLoadingImageView:Z

    .line 62
    invoke-direct {p0}, Lhost/exp/exponent/LoadingView;->init()V

    return-void
.end method

.method static synthetic access$000(Lhost/exp/exponent/LoadingView;Landroid/view/View;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lhost/exp/exponent/LoadingView;->revealView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$100(Lhost/exp/exponent/LoadingView;)Z
    .locals 0

    .line 31
    iget-boolean p0, p0, Lhost/exp/exponent/LoadingView;->mIsLoadingImageView:Z

    return p0
.end method

.method static synthetic access$102(Lhost/exp/exponent/LoadingView;Z)Z
    .locals 0

    .line 31
    iput-boolean p1, p0, Lhost/exp/exponent/LoadingView;->mIsLoadingImageView:Z

    return p1
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .line 31
    sget-object v0, Lhost/exp/exponent/LoadingView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lhost/exp/exponent/LoadingView;)Z
    .locals 0

    .line 31
    iget-boolean p0, p0, Lhost/exp/exponent/LoadingView;->mShowIcon:Z

    return p0
.end method

.method static synthetic access$302(Lhost/exp/exponent/LoadingView;Z)Z
    .locals 0

    .line 31
    iput-boolean p1, p0, Lhost/exp/exponent/LoadingView;->mShowIcon:Z

    return p1
.end method

.method static synthetic access$400(Lhost/exp/exponent/LoadingView;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lhost/exp/exponent/LoadingView;->showIcon()V

    return-void
.end method

.method static synthetic access$500(Lhost/exp/exponent/LoadingView;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lhost/exp/exponent/LoadingView;->hideIcon()V

    return-void
.end method

.method private backgroundImageURL(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 10

    .line 272
    invoke-direct {p0, p1}, Lhost/exp/exponent/LoadingView;->isUsingNewSplashScreenStyle(Lorg/json/JSONObject;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    const-string v0, "android"

    .line 275
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "splash"

    if-eqz v2, :cond_1

    .line 276
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 277
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 278
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v4, "xxxhdpiUrl"

    const-string v5, "xxhdpiUrl"

    const-string v6, "xhdpiUrl"

    const-string v7, "hdpiUrl"

    const-string v8, "mdpiUrl"

    const-string v9, "ldpiUrl"

    .line 281
    filled-new-array/range {v4 .. v9}, [Ljava/lang/String;

    move-result-object v2

    .line 283
    array-length v4, v2

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_1

    aget-object v6, v2, v5

    .line 284
    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 285
    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 290
    :cond_1
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 291
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    const-string v0, "imageUrl"

    .line 292
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 293
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    const-string v0, "loading"

    .line 297
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 298
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-eqz p1, :cond_3

    const-string v0, "backgroundImageUrl"

    .line 300
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    return-object v1
.end method

.method private hideIcon()V
    .locals 3

    .line 373
    iget-object v0, p0, Lhost/exp/exponent/LoadingView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 375
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v1, 0x12c

    .line 376
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 377
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    const/4 v1, 0x1

    .line 378
    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 379
    new-instance v1, Lhost/exp/exponent/LoadingView$6;

    invoke-direct {v1, p0}, Lhost/exp/exponent/LoadingView$6;-><init>(Lhost/exp/exponent/LoadingView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 395
    iget-object v1, p0, Lhost/exp/exponent/LoadingView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private hideProgressBar()V
    .locals 3

    .line 92
    iget-object v0, p0, Lhost/exp/exponent/LoadingView;->mProgressBarHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 93
    iget-object v0, p0, Lhost/exp/exponent/LoadingView;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->clearAnimation()V

    .line 95
    iget-object v0, p0, Lhost/exp/exponent/LoadingView;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 96
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v1, 0xfa

    .line 97
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 98
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    const/4 v1, 0x1

    .line 99
    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 100
    new-instance v1, Lhost/exp/exponent/LoadingView$2;

    invoke-direct {v1, p0}, Lhost/exp/exponent/LoadingView$2;-><init>(Lhost/exp/exponent/LoadingView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 115
    iget-object v1, p0, Lhost/exp/exponent/LoadingView;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method private init()V
    .locals 2

    .line 66
    invoke-virtual {p0}, Lhost/exp/exponent/LoadingView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lhost/exp/expoview/R$layout;->loading_view:I

    invoke-static {v0, v1, p0}, Lhost/exp/exponent/LoadingView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 67
    sget v0, Lhost/exp/expoview/R$id;->progress_bar:I

    invoke-virtual {p0, v0}, Lhost/exp/exponent/LoadingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lhost/exp/exponent/LoadingView;->mProgressBar:Landroid/widget/ProgressBar;

    .line 68
    sget v0, Lhost/exp/expoview/R$id;->image_view:I

    invoke-virtual {p0, v0}, Lhost/exp/exponent/LoadingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lhost/exp/exponent/LoadingView;->mImageView:Landroid/widget/ImageView;

    .line 69
    sget v0, Lhost/exp/expoview/R$id;->background_image_view:I

    invoke-virtual {p0, v0}, Lhost/exp/exponent/LoadingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lhost/exp/exponent/LoadingView;->mBackgroundImageView:Landroid/widget/ImageView;

    .line 70
    sget v0, Lhost/exp/expoview/R$id;->status_bar:I

    invoke-virtual {p0, v0}, Lhost/exp/exponent/LoadingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhost/exp/exponent/LoadingView;->mStatusBarView:Landroid/view/View;

    .line 71
    sget v0, Lhost/exp/expoview/R$id;->status_text_view:I

    invoke-virtual {p0, v0}, Lhost/exp/exponent/LoadingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhost/exp/exponent/LoadingView;->mStatusTextView:Landroid/widget/TextView;

    .line 72
    sget v0, Lhost/exp/expoview/R$id;->percentage_text_view:I

    invoke-virtual {p0, v0}, Lhost/exp/exponent/LoadingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhost/exp/exponent/LoadingView;->mPercentageTextView:Landroid/widget/TextView;

    const/4 v0, -0x1

    .line 73
    invoke-virtual {p0, v0}, Lhost/exp/exponent/LoadingView;->setBackgroundColor(I)V

    .line 74
    invoke-direct {p0}, Lhost/exp/exponent/LoadingView;->showProgressBar()V

    return-void
.end method

.method private isUsingNewSplashScreenStyle(Lorg/json/JSONObject;)Ljava/lang/Boolean;
    .locals 5

    const-string v0, "splash"

    .line 309
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    .line 310
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 311
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_0
    const-string v1, "android"

    .line 315
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_2

    .line 316
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    .line 317
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 320
    :cond_2
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method private revealView(Landroid/view/View;)V
    .locals 3

    const/4 v0, 0x0

    .line 404
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 405
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v1, 0x12c

    .line 406
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 407
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    const/4 v1, 0x1

    .line 408
    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 409
    invoke-virtual {p1, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private scaleType(Lorg/json/JSONObject;)Landroid/widget/ImageView$ScaleType;
    .locals 4

    const-string v0, "android"

    .line 211
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x0

    const-string v3, "splash"

    if-eqz v1, :cond_0

    .line 212
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 213
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    if-nez v0, :cond_1

    .line 219
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 220
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_2

    const-string p1, "resizeMode"

    .line 224
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "contain"

    .line 225
    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_2
    if-nez v2, :cond_3

    .line 229
    sget-object p1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    return-object p1

    :cond_3
    const-string p1, "cover"

    .line 232
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 233
    sget-object p1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    return-object p1

    .line 235
    :cond_4
    sget-object p1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    return-object p1
.end method

.method private setBackgroundColor(Lorg/json/JSONObject;)V
    .locals 5

    .line 241
    invoke-direct {p0, p1}, Lhost/exp/exponent/LoadingView;->isUsingNewSplashScreenStyle(Lorg/json/JSONObject;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const-string v1, "backgroundColor"

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    const-string v0, "android"

    .line 244
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    const-string v4, "splash"

    if-eqz v3, :cond_0

    .line 245
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 246
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    if-nez v0, :cond_1

    .line 248
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 249
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_3

    .line 253
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_2
    const-string v0, "loading"

    .line 255
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 256
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 258
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_3
    move-object p1, v2

    :goto_1
    if-eqz p1, :cond_4

    .line 262
    invoke-static {p1}, Lhost/exp/exponent/utils/ColorParser;->isValid(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    const-string p1, "#FFFFFF"

    .line 266
    :cond_5
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lhost/exp/exponent/LoadingView;->setBackgroundColor(I)V

    .line 268
    iget-object p1, p0, Lhost/exp/exponent/LoadingView;->mImageView:Landroid/widget/ImageView;

    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, Landroid/widget/ImageView;->setLayerType(ILandroid/graphics/Paint;)V

    return-void
.end method

.method private setBackgroundImage(Lorg/json/JSONObject;)V
    .locals 3

    .line 181
    invoke-direct {p0, p1}, Lhost/exp/exponent/LoadingView;->isUsingNewSplashScreenStyle(Lorg/json/JSONObject;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    invoke-direct {p0, p1}, Lhost/exp/exponent/LoadingView;->scaleType(Lorg/json/JSONObject;)Landroid/widget/ImageView$ScaleType;

    move-result-object v0

    .line 183
    iget-object v1, p0, Lhost/exp/exponent/LoadingView;->mBackgroundImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 186
    :cond_0
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187
    iget-object p1, p0, Lhost/exp/exponent/LoadingView;->mBackgroundImageView:Landroid/widget/ImageView;

    invoke-direct {p0, p1}, Lhost/exp/exponent/LoadingView;->revealView(Landroid/view/View;)V

    return-void

    .line 191
    :cond_1
    invoke-direct {p0, p1}, Lhost/exp/exponent/LoadingView;->backgroundImageURL(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 193
    invoke-virtual {p0}, Lhost/exp/exponent/LoadingView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v1, p0, Lhost/exp/exponent/LoadingView;->mBackgroundImageView:Landroid/widget/ImageView;

    new-instance v2, Lhost/exp/exponent/LoadingView$4;

    invoke-direct {v2, p0, p1}, Lhost/exp/exponent/LoadingView$4;-><init>(Lhost/exp/exponent/LoadingView;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    :cond_2
    return-void
.end method

.method private showIcon()V
    .locals 3

    .line 358
    iget-boolean v0, p0, Lhost/exp/exponent/LoadingView;->mShowIcon:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lhost/exp/exponent/LoadingView;->mIsLoading:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 362
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/LoadingView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 363
    iget-object v0, p0, Lhost/exp/exponent/LoadingView;->mImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 365
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v1, 0x12c

    .line 366
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 367
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    const/4 v1, 0x1

    .line 368
    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 369
    iget-object v1, p0, Lhost/exp/exponent/LoadingView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private showProgressBar()V
    .locals 4

    .line 78
    iget-object v0, p0, Lhost/exp/exponent/LoadingView;->mProgressBarHandler:Landroid/os/Handler;

    new-instance v1, Lhost/exp/exponent/LoadingView$1;

    invoke-direct {v1, p0}, Lhost/exp/exponent/LoadingView$1;-><init>(Lhost/exp/exponent/LoadingView;)V

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method public setDoneLoading()V
    .locals 1

    const/4 v0, 0x0

    .line 399
    iput-boolean v0, p0, Lhost/exp/exponent/LoadingView;->mIsLoading:Z

    const-string v0, "loadingViewImage"

    .line 400
    invoke-static {v0}, Lhost/exp/exponent/utils/AsyncCondition;->remove(Ljava/lang/String;)V

    return-void
.end method

.method public setManifest(Lorg/json/JSONObject;)V
    .locals 7

    .line 120
    invoke-direct {p0}, Lhost/exp/exponent/LoadingView;->hideProgressBar()V

    .line 122
    iget-boolean v0, p0, Lhost/exp/exponent/LoadingView;->mIsLoading:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 125
    iput-boolean v0, p0, Lhost/exp/exponent/LoadingView;->mIsLoading:Z

    const/4 v1, 0x0

    .line 126
    iput-boolean v1, p0, Lhost/exp/exponent/LoadingView;->mIsLoadingImageView:Z

    if-nez p1, :cond_1

    return-void

    :cond_1
    const-string v2, "loading"

    .line 132
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 134
    invoke-direct {p0, p1}, Lhost/exp/exponent/LoadingView;->isUsingNewSplashScreenStyle(Lorg/json/JSONObject;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 136
    iget-object v0, p0, Lhost/exp/exponent/LoadingView;->mImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    goto/16 :goto_1

    :cond_2
    if-eqz v2, :cond_3

    const-string v3, "iconUrl"

    .line 137
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 138
    iget-object v1, p0, Lhost/exp/exponent/LoadingView;->mImageView:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 139
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 140
    iput-boolean v0, p0, Lhost/exp/exponent/LoadingView;->mIsLoadingImageView:Z

    .line 141
    invoke-virtual {p0}, Lhost/exp/exponent/LoadingView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v2, p0, Lhost/exp/exponent/LoadingView;->mImageView:Landroid/widget/ImageView;

    new-instance v3, Lhost/exp/exponent/LoadingView$3;

    invoke-direct {v3, p0, v1}, Lhost/exp/exponent/LoadingView$3;-><init>(Lhost/exp/exponent/LoadingView;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    goto :goto_1

    :cond_3
    if-eqz v2, :cond_6

    const-string v0, "exponentIconGrayscale"

    .line 154
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 155
    iget-object v3, p0, Lhost/exp/exponent/LoadingView;->mImageView:Landroid/widget/ImageView;

    sget v4, Lhost/exp/expoview/R$drawable;->big_logo_dark:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    const-wide v3, 0x406fe00000000000L    # 255.0

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    .line 157
    invoke-virtual {v2, v0, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v5

    mul-double v5, v5, v3

    double-to-int v0, v5

    const/16 v2, 0xff

    if-gez v0, :cond_4

    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    if-le v0, v2, :cond_5

    const/16 v0, 0xff

    .line 163
    :cond_5
    :goto_0
    iget-object v1, p0, Lhost/exp/exponent/LoadingView;->mImageView:Landroid/widget/ImageView;

    invoke-static {v2, v0, v0, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto :goto_1

    :cond_6
    if-eqz v2, :cond_9

    const/4 v0, 0x0

    const-string v1, "exponentIconColor"

    .line 166
    invoke-virtual {v2, v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    const-string v1, "white"

    .line 168
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 169
    iget-object v0, p0, Lhost/exp/exponent/LoadingView;->mImageView:Landroid/widget/ImageView;

    sget v1, Lhost/exp/expoview/R$drawable;->big_logo_new_filled:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_7
    const-string v1, "navy"

    .line 170
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "blue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 171
    :cond_8
    iget-object v0, p0, Lhost/exp/exponent/LoadingView;->mImageView:Landroid/widget/ImageView;

    sget v1, Lhost/exp/expoview/R$drawable;->big_logo_dark_filled:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 176
    :cond_9
    :goto_1
    invoke-direct {p0, p1}, Lhost/exp/exponent/LoadingView;->setBackgroundImage(Lorg/json/JSONObject;)V

    .line 177
    invoke-direct {p0, p1}, Lhost/exp/exponent/LoadingView;->setBackgroundColor(Lorg/json/JSONObject;)V

    return-void
.end method

.method public setShowIcon(Z)V
    .locals 2

    const-string v0, "loadingViewImage"

    .line 324
    invoke-static {v0}, Lhost/exp/exponent/utils/AsyncCondition;->remove(Ljava/lang/String;)V

    .line 325
    new-instance v1, Lhost/exp/exponent/LoadingView$5;

    invoke-direct {v1, p0, p1}, Lhost/exp/exponent/LoadingView$5;-><init>(Lhost/exp/exponent/LoadingView;Z)V

    invoke-static {v0, v1}, Lhost/exp/exponent/utils/AsyncCondition;->wait(Ljava/lang/String;Lhost/exp/exponent/utils/AsyncCondition$AsyncConditionListener;)V

    return-void
.end method

.method public updateProgress(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 349
    iget-object v0, p0, Lhost/exp/exponent/LoadingView;->mStatusBarView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 350
    iget-object v0, p0, Lhost/exp/exponent/LoadingView;->mStatusTextView:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, "Building JavaScript bundle..."

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    .line 351
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-lez p1, :cond_1

    .line 352
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-float p1, p1

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p2

    int-to-float p2, p2

    div-float/2addr p1, p2

    const/high16 p2, 0x42c80000    # 100.0f

    mul-float p1, p1, p2

    .line 353
    iget-object p2, p0, Lhost/exp/exponent/LoadingView;->mPercentageTextView:Landroid/widget/TextView;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "%.2f%%"

    invoke-static {p3, p1, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method
