.class Lhost/exp/exponent/headless/HeadlessAppLoader$1;
.super Lhost/exp/exponent/AppLoader;
.source "HeadlessAppLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/headless/HeadlessAppLoader;->loadApp(Ljava/lang/String;Ljava/util/Map;Lexpo/loaders/provider/AppLoaderProvider$Callback;)Lexpo/loaders/provider/interfaces/AppRecordInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/headless/HeadlessAppLoader;


# direct methods
.method constructor <init>(Lhost/exp/exponent/headless/HeadlessAppLoader;Ljava/lang/String;Z)V
    .locals 0

    .line 83
    iput-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader$1;->this$0:Lhost/exp/exponent/headless/HeadlessAppLoader;

    invoke-direct {p0, p2, p3}, Lhost/exp/exponent/AppLoader;-><init>(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public emitEvent(Lorg/json/JSONObject;)V
    .locals 0

    return-void
.end method

.method public onBundleCompleted(Ljava/lang/String;)V
    .locals 1

    .line 107
    iget-object v0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader$1;->this$0:Lhost/exp/exponent/headless/HeadlessAppLoader;

    invoke-virtual {v0, p1}, Lhost/exp/exponent/headless/HeadlessAppLoader;->setBundle(Ljava/lang/String;)V

    return-void
.end method

.method public onError(Ljava/lang/Exception;)V
    .locals 2

    .line 116
    iget-object v0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader$1;->this$0:Lhost/exp/exponent/headless/HeadlessAppLoader;

    invoke-static {v0}, Lhost/exp/exponent/headless/HeadlessAppLoader;->access$300(Lhost/exp/exponent/headless/HeadlessAppLoader;)Lexpo/loaders/provider/AppLoaderProvider$Callback;

    move-result-object v0

    new-instance v1, Ljava/lang/Exception;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x0

    invoke-virtual {v0, p1, v1}, Lexpo/loaders/provider/AppLoaderProvider$Callback;->onComplete(ZLjava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;)V
    .locals 2

    .line 121
    iget-object v0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader$1;->this$0:Lhost/exp/exponent/headless/HeadlessAppLoader;

    invoke-static {v0}, Lhost/exp/exponent/headless/HeadlessAppLoader;->access$300(Lhost/exp/exponent/headless/HeadlessAppLoader;)Lexpo/loaders/provider/AppLoaderProvider$Callback;

    move-result-object v0

    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x0

    invoke-virtual {v0, p1, v1}, Lexpo/loaders/provider/AppLoaderProvider$Callback;->onComplete(ZLjava/lang/Exception;)V

    return-void
.end method

.method public onManifestCompleted(Lorg/json/JSONObject;)V
    .locals 2

    .line 90
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    new-instance v1, Lhost/exp/exponent/headless/HeadlessAppLoader$1$1;

    invoke-direct {v1, p0, p1}, Lhost/exp/exponent/headless/HeadlessAppLoader$1$1;-><init>(Lhost/exp/exponent/headless/HeadlessAppLoader$1;Lorg/json/JSONObject;)V

    invoke-virtual {v0, v1}, Lhost/exp/expoview/Exponent;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onOptimisticManifest(Lorg/json/JSONObject;)V
    .locals 0

    return-void
.end method
