.class Lhost/exp/exponent/headless/HeadlessAppLoader$1$1;
.super Ljava/lang/Object;
.source "HeadlessAppLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/headless/HeadlessAppLoader$1;->onManifestCompleted(Lorg/json/JSONObject;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lhost/exp/exponent/headless/HeadlessAppLoader$1;

.field final synthetic val$manifest:Lorg/json/JSONObject;


# direct methods
.method constructor <init>(Lhost/exp/exponent/headless/HeadlessAppLoader$1;Lorg/json/JSONObject;)V
    .locals 0

    .line 90
    iput-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader$1$1;->this$1:Lhost/exp/exponent/headless/HeadlessAppLoader$1;

    iput-object p2, p0, Lhost/exp/exponent/headless/HeadlessAppLoader$1$1;->val$manifest:Lorg/json/JSONObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 94
    :try_start_0
    iget-object v0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader$1$1;->val$manifest:Lorg/json/JSONObject;

    const-string v1, "bundleUrl"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lhost/exp/exponent/kernel/ExponentUrls;->toHttp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 96
    invoke-static {}, Lhost/exp/exponent/headless/HeadlessAppLoader;->access$100()Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lhost/exp/exponent/headless/HeadlessAppLoader$1$1;->this$1:Lhost/exp/exponent/headless/HeadlessAppLoader$1;

    iget-object v2, v2, Lhost/exp/exponent/headless/HeadlessAppLoader$1;->this$0:Lhost/exp/exponent/headless/HeadlessAppLoader;

    invoke-static {v2}, Lhost/exp/exponent/headless/HeadlessAppLoader;->access$000(Lhost/exp/exponent/headless/HeadlessAppLoader;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    iget-object v1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader$1$1;->this$1:Lhost/exp/exponent/headless/HeadlessAppLoader$1;

    iget-object v1, v1, Lhost/exp/exponent/headless/HeadlessAppLoader$1;->this$0:Lhost/exp/exponent/headless/HeadlessAppLoader;

    iget-object v2, p0, Lhost/exp/exponent/headless/HeadlessAppLoader$1$1;->this$1:Lhost/exp/exponent/headless/HeadlessAppLoader$1;

    iget-object v2, v2, Lhost/exp/exponent/headless/HeadlessAppLoader$1;->this$0:Lhost/exp/exponent/headless/HeadlessAppLoader;

    invoke-static {v2}, Lhost/exp/exponent/headless/HeadlessAppLoader;->access$200(Lhost/exp/exponent/headless/HeadlessAppLoader;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lhost/exp/exponent/headless/HeadlessAppLoader$1$1;->val$manifest:Lorg/json/JSONObject;

    invoke-virtual {v1, v2, v3, v0}, Lhost/exp/exponent/headless/HeadlessAppLoader;->setManifest(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 99
    iget-object v1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader$1$1;->this$1:Lhost/exp/exponent/headless/HeadlessAppLoader$1;

    iget-object v1, v1, Lhost/exp/exponent/headless/HeadlessAppLoader$1;->this$0:Lhost/exp/exponent/headless/HeadlessAppLoader;

    invoke-static {v1}, Lhost/exp/exponent/headless/HeadlessAppLoader;->access$300(Lhost/exp/exponent/headless/HeadlessAppLoader;)Lexpo/loaders/provider/AppLoaderProvider$Callback;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/Exception;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lexpo/loaders/provider/AppLoaderProvider$Callback;->onComplete(ZLjava/lang/Exception;)V

    :goto_0
    return-void
.end method
