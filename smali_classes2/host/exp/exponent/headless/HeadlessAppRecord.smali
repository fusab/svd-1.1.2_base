.class public Lhost/exp/exponent/headless/HeadlessAppRecord;
.super Ljava/lang/Object;
.source "HeadlessAppRecord.java"

# interfaces
.implements Lexpo/loaders/provider/interfaces/AppRecordInterface;


# instance fields
.field private mReactInstanceManager:Lhost/exp/exponent/RNObject;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invalidate()V
    .locals 3

    .line 17
    iget-object v0, p0, Lhost/exp/exponent/headless/HeadlessAppRecord;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 19
    iput-object v1, p0, Lhost/exp/exponent/headless/HeadlessAppRecord;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    .line 22
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lhost/exp/exponent/headless/HeadlessAppRecord$1;

    invoke-direct {v2, p0, v0}, Lhost/exp/exponent/headless/HeadlessAppRecord$1;-><init>(Lhost/exp/exponent/headless/HeadlessAppRecord;Lhost/exp/exponent/RNObject;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public setReactInstanceManager(Lhost/exp/exponent/RNObject;)V
    .locals 0

    .line 13
    iput-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppRecord;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    return-void
.end method
