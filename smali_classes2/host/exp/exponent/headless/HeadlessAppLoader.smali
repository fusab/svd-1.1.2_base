.class public Lhost/exp/exponent/headless/HeadlessAppLoader;
.super Ljava/lang/Object;
.source "HeadlessAppLoader.java"

# interfaces
.implements Lexpo/loaders/provider/interfaces/AppLoaderInterface;
.implements Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;


# static fields
.field private static READY_FOR_BUNDLE:Ljava/lang/String; = "headlessAppReadyForBundle"

.field private static final sActivityIdToBundleUrl:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActivityId:I

.field private mAppRecord:Lhost/exp/exponent/headless/HeadlessAppRecord;

.field private mCallback:Lexpo/loaders/provider/AppLoaderProvider$Callback;

.field private mContext:Landroid/content/Context;

.field private mDetachSdkVersion:Ljava/lang/String;

.field private mIntentUri:Ljava/lang/String;

.field private mIsReadyForBundle:Z

.field private mJSBundlePath:Ljava/lang/String;

.field private mManifest:Lorg/json/JSONObject;

.field private mManifestUrl:Ljava/lang/String;

.field private mReactInstanceManager:Lhost/exp/exponent/RNObject;

.field private mSdkVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lhost/exp/exponent/headless/HeadlessAppLoader;->sActivityIdToBundleUrl:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Lhost/exp/exponent/RNObject;

    const-string v1, "com.facebook.react.ReactInstanceManager"

    invoke-direct {v0, v1}, Lhost/exp/exponent/RNObject;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    .line 65
    iput-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lhost/exp/exponent/headless/HeadlessAppLoader;)I
    .locals 0

    .line 46
    iget p0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mActivityId:I

    return p0
.end method

.method static synthetic access$100()Ljava/util/Map;
    .locals 1

    .line 46
    sget-object v0, Lhost/exp/exponent/headless/HeadlessAppLoader;->sActivityIdToBundleUrl:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1000(Lhost/exp/exponent/headless/HeadlessAppLoader;)Ljava/lang/String;
    .locals 0

    .line 46
    iget-object p0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mDetachSdkVersion:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1100(Lhost/exp/exponent/headless/HeadlessAppLoader;)Ljava/util/List;
    .locals 0

    .line 46
    invoke-direct {p0}, Lhost/exp/exponent/headless/HeadlessAppLoader;->reactPackages()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$1200(Lhost/exp/exponent/headless/HeadlessAppLoader;Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lhost/exp/exponent/RNObject;
    .locals 0

    .line 46
    invoke-direct/range {p0 .. p5}, Lhost/exp/exponent/headless/HeadlessAppLoader;->startReactInstance(Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lhost/exp/exponent/RNObject;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lhost/exp/exponent/headless/HeadlessAppLoader;)Ljava/lang/String;
    .locals 0

    .line 46
    iget-object p0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mManifestUrl:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lhost/exp/exponent/headless/HeadlessAppLoader;)Lexpo/loaders/provider/AppLoaderProvider$Callback;
    .locals 0

    .line 46
    iget-object p0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mCallback:Lexpo/loaders/provider/AppLoaderProvider$Callback;

    return-object p0
.end method

.method static synthetic access$400(Lhost/exp/exponent/headless/HeadlessAppLoader;)Lhost/exp/exponent/RNObject;
    .locals 0

    .line 46
    iget-object p0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    return-object p0
.end method

.method static synthetic access$402(Lhost/exp/exponent/headless/HeadlessAppLoader;Lhost/exp/exponent/RNObject;)Lhost/exp/exponent/RNObject;
    .locals 0

    .line 46
    iput-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mReactInstanceManager:Lhost/exp/exponent/RNObject;

    return-object p1
.end method

.method static synthetic access$502(Lhost/exp/exponent/headless/HeadlessAppLoader;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 46
    iput-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mJSBundlePath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lhost/exp/exponent/headless/HeadlessAppLoader;)V
    .locals 0

    .line 46
    invoke-direct {p0}, Lhost/exp/exponent/headless/HeadlessAppLoader;->startReactInstance()V

    return-void
.end method

.method static synthetic access$700(Lhost/exp/exponent/headless/HeadlessAppLoader;)Z
    .locals 0

    .line 46
    iget-boolean p0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mIsReadyForBundle:Z

    return p0
.end method

.method static synthetic access$702(Lhost/exp/exponent/headless/HeadlessAppLoader;Z)Z
    .locals 0

    .line 46
    iput-boolean p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mIsReadyForBundle:Z

    return p1
.end method

.method static synthetic access$800()Ljava/lang/String;
    .locals 1

    .line 46
    sget-object v0, Lhost/exp/exponent/headless/HeadlessAppLoader;->READY_FOR_BUNDLE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lhost/exp/exponent/headless/HeadlessAppLoader;)Ljava/lang/String;
    .locals 0

    .line 46
    iget-object p0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mIntentUri:Ljava/lang/String;

    return-object p0
.end method

.method public static getBundleUrlForActivityId(I)Ljava/lang/String;
    .locals 1

    .line 73
    sget-object v0, Lhost/exp/exponent/headless/HeadlessAppLoader;->sActivityIdToBundleUrl:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method

.method private getLinkingUri()Ljava/lang/String;
    .locals 4

    .line 332
    sget-object v0, Lhost/exp/exponent/Constants;->SHELL_APP_SCHEME:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 333
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lhost/exp/exponent/Constants;->SHELL_APP_SCHEME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 335
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mManifestUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 336
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    const-string v2, "exp.host"

    .line 337
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "expo.io"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "exp.direct"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "expo.test"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, ".exp.host"

    .line 338
    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, ".expo.io"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, ".exp.direct"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, ".expo.test"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 339
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    .line 340
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v2, 0x0

    .line 341
    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 343
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "--"

    .line 344
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 347
    :cond_2
    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    :cond_3
    :goto_1
    const-string v1, "--/"

    .line 350
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 352
    :cond_4
    iget-object v0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mManifestUrl:Ljava/lang/String;

    return-object v0
.end method

.method public static hasBundleUrlForActivityId(I)Z
    .locals 1

    const/4 v0, -0x1

    if-ge p0, v0, :cond_0

    .line 69
    sget-object v0, Lhost/exp/exponent/headless/HeadlessAppLoader;->sActivityIdToBundleUrl:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private reactPackages()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/facebook/react/ReactPackage;",
            ">;"
        }
    .end annotation

    .line 220
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 225
    :cond_0
    :try_start_0
    iget-object v0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lexpo/loaders/provider/interfaces/AppLoaderPackagesProviderInterface;

    invoke-interface {v0}, Lexpo/loaders/provider/interfaces/AppLoaderPackagesProviderInterface;->getPackages()Ljava/util/List;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 227
    invoke-virtual {v0}, Ljava/lang/ClassCastException;->printStackTrace()V

    return-object v1
.end method

.method private startReactInstance(Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lhost/exp/exponent/RNObject;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/List<",
            "Lorg/unimodules/core/interfaces/Package;",
            ">;)",
            "Lhost/exp/exponent/RNObject;"
        }
    .end annotation

    .line 282
    invoke-direct {p0}, Lhost/exp/exponent/headless/HeadlessAppLoader;->getLinkingUri()Ljava/lang/String;

    move-result-object v3

    .line 283
    iget-object v1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mManifestUrl:Ljava/lang/String;

    const/4 v8, 0x1

    .line 287
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    const-string v0, "experienceUrl"

    const-string v2, "linkingUri"

    const-string v4, "intentUri"

    const-string v6, "isHeadless"

    move-object v5, p2

    .line 283
    invoke-static/range {v0 .. v7}, Lcom/facebook/react/common/MapBuilder;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object p2

    .line 290
    new-instance v0, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;

    invoke-direct {v0}, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;-><init>()V

    .line 291
    iget-object v1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Application;

    iput-object v1, v0, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;->application:Landroid/app/Application;

    .line 292
    iget-object v1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mJSBundlePath:Ljava/lang/String;

    iput-object v1, v0, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;->jsBundlePath:Ljava/lang/String;

    .line 293
    iput-object p2, v0, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;->experienceProperties:Ljava/util/Map;

    .line 294
    iput-object p5, v0, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;->expoPackages:Ljava/util/List;

    .line 295
    invoke-interface {p1}, Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;->getExponentPackageDelegate()Lversioned/host/exp/exponent/ExponentPackageDelegate;

    move-result-object p2

    iput-object p2, v0, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;->exponentPackageDelegate:Lversioned/host/exp/exponent/ExponentPackageDelegate;

    .line 296
    iget-object p2, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mManifest:Lorg/json/JSONObject;

    iput-object p2, v0, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;->manifest:Lorg/json/JSONObject;

    .line 297
    iget-object v1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mContext:Landroid/content/Context;

    invoke-static {v1, p2, p5}, Lversioned/host/exp/exponent/ExponentPackage;->getOrCreateSingletonModules(Landroid/content/Context;Lorg/json/JSONObject;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, v0, Lhost/exp/expoview/Exponent$InstanceManagerBuilderProperties;->singletonModules:Ljava/util/List;

    .line 299
    new-instance p2, Lhost/exp/exponent/RNObject;

    const-string p5, "host.exp.exponent.VersionedUtils"

    invoke-direct {p2, p5}, Lhost/exp/exponent/RNObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p3}, Lhost/exp/exponent/RNObject;->loadVersion(Ljava/lang/String;)Lhost/exp/exponent/RNObject;

    move-result-object p2

    .line 300
    new-array p5, v8, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v0, p5, v1

    const-string v0, "getReactInstanceManagerBuilder"

    invoke-virtual {p2, v0, p5}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object p2

    if-eqz p4, :cond_0

    .line 303
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :goto_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result p5

    if-eqz p5, :cond_0

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p5

    .line 304
    new-array v0, v8, [Ljava/lang/Object;

    aput-object p5, v0, v1

    const-string p5, "addPackage"

    invoke-virtual {p2, p5, v0}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 308
    :cond_0
    invoke-interface {p1}, Lhost/exp/expoview/Exponent$StartReactInstanceDelegate;->isDebugModeEnabled()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 309
    iget-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mManifest:Lorg/json/JSONObject;

    const-string p4, "debuggerHost"

    invoke-virtual {p1, p4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 310
    iget-object p4, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mManifest:Lorg/json/JSONObject;

    const-string p5, "mainModuleName"

    invoke-virtual {p4, p5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    .line 311
    invoke-static {p3, p1, p4, p2}, Lhost/exp/expoview/Exponent;->enableDeveloperSupport(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/RNObject;)V

    .line 314
    :cond_1
    new-array p1, v1, [Ljava/lang/Object;

    const-string p3, "build"

    invoke-virtual {p2, p3, p1}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object p1

    .line 315
    new-array p2, v1, [Ljava/lang/Object;

    const-string p3, "getDevSupportManager"

    invoke-virtual {p1, p3, p2}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object p2

    new-array p3, v1, [Ljava/lang/Object;

    const-string p4, "getDevSettings"

    invoke-virtual {p2, p4, p3}, Lhost/exp/exponent/RNObject;->callRecursive(Ljava/lang/String;[Ljava/lang/Object;)Lhost/exp/exponent/RNObject;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 317
    iget p3, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mActivityId:I

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    const-string p4, "exponentActivityId"

    invoke-virtual {p2, p4, p3}, Lhost/exp/exponent/RNObject;->setField(Ljava/lang/String;Ljava/lang/Object;)V

    .line 320
    :cond_2
    new-array p2, v1, [Ljava/lang/Object;

    const-string p3, "createReactContextInBackground"

    invoke-virtual {p1, p3, p2}, Lhost/exp/exponent/RNObject;->call(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    iget-object p2, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mAppRecord:Lhost/exp/exponent/headless/HeadlessAppRecord;

    invoke-virtual {p2, p1}, Lhost/exp/exponent/headless/HeadlessAppRecord;->setReactInstanceManager(Lhost/exp/exponent/RNObject;)V

    .line 324
    iget-object p2, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mCallback:Lexpo/loaders/provider/AppLoaderProvider$Callback;

    const/4 p3, 0x0

    invoke-virtual {p2, v8, p3}, Lexpo/loaders/provider/AppLoaderProvider$Callback;->onComplete(ZLjava/lang/Exception;)V

    return-object p1
.end method

.method private startReactInstance()V
    .locals 4

    .line 267
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    invoke-virtual {p0}, Lhost/exp/exponent/headless/HeadlessAppLoader;->isDebugModeEnabled()Z

    move-result v1

    iget-object v2, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mManifest:Lorg/json/JSONObject;

    new-instance v3, Lhost/exp/exponent/headless/HeadlessAppLoader$4;

    invoke-direct {v3, p0}, Lhost/exp/exponent/headless/HeadlessAppLoader$4;-><init>(Lhost/exp/exponent/headless/HeadlessAppLoader;)V

    invoke-virtual {v0, v1, v2, v3}, Lhost/exp/expoview/Exponent;->testPackagerStatus(ZLorg/json/JSONObject;Lhost/exp/expoview/Exponent$PackagerStatusCallback;)V

    return-void
.end method


# virtual methods
.method public expoPackages()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/unimodules/core/interfaces/Package;",
            ">;"
        }
    .end annotation

    .line 235
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 240
    :cond_0
    :try_start_0
    iget-object v0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lexpo/loaders/provider/interfaces/AppLoaderPackagesProviderInterface;

    invoke-interface {v0}, Lexpo/loaders/provider/interfaces/AppLoaderPackagesProviderInterface;->getExpoPackages()Ljava/util/List;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 242
    invoke-virtual {v0}, Ljava/lang/ClassCastException;->printStackTrace()V

    return-object v1
.end method

.method public getExponentPackageDelegate()Lversioned/host/exp/exponent/ExponentPackageDelegate;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public handleUnreadNotifications(Lorg/json/JSONArray;)V
    .locals 0

    return-void
.end method

.method public isDebugModeEnabled()Z
    .locals 1

    .line 208
    iget-object v0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mManifest:Lorg/json/JSONObject;

    invoke-static {v0}, Lhost/exp/exponent/ExponentManifest;->isDebugModeEnabled(Lorg/json/JSONObject;)Z

    move-result v0

    return v0
.end method

.method public isInForeground()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public loadApp(Ljava/lang/String;Ljava/util/Map;Lexpo/loaders/provider/AppLoaderProvider$Callback;)Lexpo/loaders/provider/interfaces/AppRecordInterface;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lexpo/loaders/provider/AppLoaderProvider$Callback;",
            ")",
            "Lexpo/loaders/provider/interfaces/AppRecordInterface;"
        }
    .end annotation

    .line 78
    iput-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mManifestUrl:Ljava/lang/String;

    .line 79
    new-instance p1, Lhost/exp/exponent/headless/HeadlessAppRecord;

    invoke-direct {p1}, Lhost/exp/exponent/headless/HeadlessAppRecord;-><init>()V

    iput-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mAppRecord:Lhost/exp/exponent/headless/HeadlessAppRecord;

    .line 80
    iput-object p3, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mCallback:Lexpo/loaders/provider/AppLoaderProvider$Callback;

    .line 81
    invoke-static {}, Lhost/exp/exponent/utils/ExpoActivityIds;->getNextHeadlessActivityId()I

    move-result p1

    iput p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mActivityId:I

    .line 83
    new-instance p1, Lhost/exp/exponent/headless/HeadlessAppLoader$1;

    iget-object p2, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mManifestUrl:Ljava/lang/String;

    const/4 p3, 0x1

    invoke-direct {p1, p0, p2, p3}, Lhost/exp/exponent/headless/HeadlessAppLoader$1;-><init>(Lhost/exp/exponent/headless/HeadlessAppLoader;Ljava/lang/String;Z)V

    .line 123
    invoke-virtual {p1}, Lhost/exp/exponent/headless/HeadlessAppLoader$1;->start()V

    .line 125
    iget-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mAppRecord:Lhost/exp/exponent/headless/HeadlessAppRecord;

    return-object p1
.end method

.method public setBundle(Ljava/lang/String;)V
    .locals 2

    .line 190
    invoke-virtual {p0}, Lhost/exp/exponent/headless/HeadlessAppLoader;->isDebugModeEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    sget-object v0, Lhost/exp/exponent/headless/HeadlessAppLoader;->READY_FOR_BUNDLE:Ljava/lang/String;

    new-instance v1, Lhost/exp/exponent/headless/HeadlessAppLoader$3;

    invoke-direct {v1, p0, p1}, Lhost/exp/exponent/headless/HeadlessAppLoader$3;-><init>(Lhost/exp/exponent/headless/HeadlessAppLoader;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lhost/exp/exponent/utils/AsyncCondition;->wait(Ljava/lang/String;Lhost/exp/exponent/utils/AsyncCondition$AsyncConditionListener;)V

    :cond_0
    return-void
.end method

.method public setManifest(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 2

    .line 129
    iput-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mManifestUrl:Ljava/lang/String;

    .line 130
    iput-object p2, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mManifest:Lorg/json/JSONObject;

    const-string p1, "sdkVersion"

    .line 131
    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mSdkVersion:Ljava/lang/String;

    .line 135
    iget-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mSdkVersion:Ljava/lang/String;

    const-string p2, "35.0.0"

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    const-string p2, "UNVERSIONED"

    if-eqz p1, :cond_0

    .line 136
    iput-object p2, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mSdkVersion:Ljava/lang/String;

    .line 139
    :cond_0
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result p1

    if-eqz p1, :cond_1

    move-object p1, p2

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mSdkVersion:Ljava/lang/String;

    :goto_0
    iput-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mDetachSdkVersion:Ljava/lang/String;

    .line 141
    iget-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mSdkVersion:Ljava/lang/String;

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 143
    sget-object p1, Lhost/exp/exponent/Constants;->SDK_VERSIONS_LIST:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    const/4 p3, 0x0

    if-eqz p2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 144
    iget-object v0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mSdkVersion:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_3
    const/4 p1, 0x0

    :goto_1
    if-nez p1, :cond_4

    .line 151
    iget-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mCallback:Lexpo/loaders/provider/AppLoaderProvider$Callback;

    new-instance p2, Ljava/lang/Exception;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mSdkVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " is not a valid SDK version."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p3, p2}, Lexpo/loaders/provider/AppLoaderProvider$Callback;->onComplete(ZLjava/lang/Exception;)V

    return-void

    .line 156
    :cond_4
    invoke-virtual {p0}, Lhost/exp/exponent/headless/HeadlessAppLoader;->soloaderInit()V

    .line 162
    iget-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mIntentUri:Ljava/lang/String;

    if-eqz p1, :cond_5

    sget-object p2, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 163
    :cond_5
    sget-object p1, Lhost/exp/exponent/Constants;->SHELL_APP_SCHEME:Ljava/lang/String;

    if-eqz p1, :cond_6

    .line 164
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p2, Lhost/exp/exponent/Constants;->SHELL_APP_SCHEME:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "://"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mIntentUri:Ljava/lang/String;

    goto :goto_2

    .line 166
    :cond_6
    iget-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mManifestUrl:Ljava/lang/String;

    iput-object p1, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mIntentUri:Ljava/lang/String;

    .line 170
    :cond_7
    :goto_2
    new-instance p1, Lhost/exp/exponent/headless/HeadlessAppLoader$2;

    invoke-direct {p1, p0}, Lhost/exp/exponent/headless/HeadlessAppLoader$2;-><init>(Lhost/exp/exponent/headless/HeadlessAppLoader;)V

    invoke-static {p1}, Lcom/facebook/react/bridge/UiThreadUtil;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public soloaderInit()V
    .locals 2

    .line 212
    iget-object v0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mDetachSdkVersion:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lhost/exp/exponent/headless/HeadlessAppLoader;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/soloader/SoLoader;->init(Landroid/content/Context;Z)V

    :cond_0
    return-void
.end method
