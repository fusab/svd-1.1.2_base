.class public Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;
.super Lcom/raizlabs/android/dbflow/structure/BaseModel;
.source "IntervalSchedulerModel.java"

# interfaces
.implements Lhost/exp/exponent/notifications/schedulers/SchedulerModel;


# static fields
.field private static triggeringActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private details:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field experienceId:Ljava/lang/String;

.field id:I

.field interval:J

.field private mApplicationContext:Landroid/content/Context;

.field notificationId:I

.field repeat:Z

.field scheduledTime:J

.field serializedDetails:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x0

    const-string v1, "android.intent.action.REBOOT"

    const-string v2, "android.intent.action.BOOT_COMPLETED"

    .line 25
    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->triggeringActions:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/raizlabs/android/dbflow/structure/BaseModel;-><init>()V

    return-void
.end method


# virtual methods
.method public canBeRescheduled()Z
    .locals 5

    .line 61
    iget-boolean v0, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->repeat:Z

    if-nez v0, :cond_1

    invoke-static {}, Lorg/joda/time/DateTime;->now()Lorg/joda/time/DateTime;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->toDate()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iget-wide v2, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->scheduledTime:J

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public getDetails()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 180
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->serializedDetails:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->setSerializedDetails(Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->details:Ljava/util/HashMap;

    return-object v0
.end method

.method public getExperienceId()Ljava/lang/String;
    .locals 1

    .line 135
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->experienceId:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .line 119
    iget v0, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->id:I

    return v0
.end method

.method public getIdAsString()Ljava/lang/String;
    .locals 2

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->id:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInterval()J
    .locals 2

    .line 172
    iget-wide v0, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->interval:J

    return-wide v0
.end method

.method public getNextAppearanceTime()J
    .locals 8

    .line 91
    invoke-static {}, Lorg/joda/time/DateTime;->now()Lorg/joda/time/DateTime;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->toDate()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 93
    iget-wide v2, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->scheduledTime:J

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    goto :goto_0

    .line 97
    :cond_0
    iget-wide v0, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->interval:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    .line 101
    invoke-static {}, Lorg/joda/time/DateTime;->now()Lorg/joda/time/DateTime;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->toDate()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 102
    iget-wide v2, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->scheduledTime:J

    sub-long/2addr v0, v2

    .line 103
    iget-wide v4, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->interval:J

    div-long/2addr v0, v4

    const-wide/16 v6, 0x1

    add-long/2addr v0, v6

    mul-long v4, v4, v0

    add-long/2addr v2, v4

    .line 107
    :goto_0
    invoke-static {}, Lorg/joda/time/DateTime;->now()Lorg/joda/time/DateTime;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/time/DateTime;->toDate()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v0, v4

    sub-long/2addr v2, v0

    return-wide v2

    .line 98
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public getNotificationId()I
    .locals 1

    .line 127
    iget v0, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->notificationId:I

    return v0
.end method

.method public getOwnerExperienceId()Ljava/lang/String;
    .locals 1

    .line 75
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->experienceId:Ljava/lang/String;

    return-object v0
.end method

.method public getScheduledTime()J
    .locals 2

    .line 164
    iget-wide v0, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->scheduledTime:J

    return-wide v0
.end method

.method public getSerializedDetails()Ljava/lang/String;
    .locals 1

    .line 151
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->serializedDetails:Ljava/lang/String;

    return-object v0
.end method

.method public isRepeat()Z
    .locals 1

    .line 143
    iget-boolean v0, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->repeat:Z

    return v0
.end method

.method public remove()V
    .locals 0

    .line 85
    invoke-virtual {p0}, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->delete()Z

    return-void
.end method

.method public saveAndGetId()Ljava/lang/String;
    .locals 3

    .line 66
    invoke-virtual {p0}, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->save()Z

    .line 67
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->details:Ljava/util/HashMap;

    invoke-virtual {p0}, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->getIdAsString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "scheduler_id"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->details:Ljava/util/HashMap;

    invoke-virtual {p0, v0}, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->setDetails(Ljava/util/HashMap;)V

    .line 69
    invoke-virtual {p0}, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->save()Z

    .line 70
    invoke-virtual {p0}, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->getIdAsString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setDetails(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 185
    iput-object p1, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->details:Ljava/util/HashMap;

    .line 186
    invoke-static {p1}, Lhost/exp/exponent/notifications/schedulers/HashMapSerializer;->serialize(Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->serializedDetails:Ljava/lang/String;

    return-void
.end method

.method public setExperienceId(Ljava/lang/String;)V
    .locals 0

    .line 139
    iput-object p1, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->experienceId:Ljava/lang/String;

    return-void
.end method

.method public setId(I)V
    .locals 0

    .line 123
    iput p1, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->id:I

    return-void
.end method

.method public setInterval(J)V
    .locals 0

    .line 176
    iput-wide p1, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->interval:J

    return-void
.end method

.method public setNotificationId(I)V
    .locals 0

    .line 131
    iput p1, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->notificationId:I

    return-void
.end method

.method public setRepeat(Z)V
    .locals 0

    .line 147
    iput-boolean p1, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->repeat:Z

    return-void
.end method

.method public setScheduledTime(J)V
    .locals 0

    .line 168
    iput-wide p1, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->scheduledTime:J

    return-void
.end method

.method public setSerializedDetails(Ljava/lang/String;)V
    .locals 1

    .line 156
    :try_start_0
    invoke-static {p1}, Lhost/exp/exponent/notifications/schedulers/HashMapSerializer;->deserialize(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->details:Ljava/util/HashMap;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 158
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 160
    :goto_0
    iput-object p1, p0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->serializedDetails:Ljava/lang/String;

    return-void
.end method

.method public shouldBeTriggeredByAction(Ljava/lang/String;)Z
    .locals 1

    .line 113
    sget-object v0, Lhost/exp/exponent/notifications/schedulers/IntervalSchedulerModel;->triggeringActions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method
