.class public final Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;
.super Lcom/raizlabs/android/dbflow/structure/ModelAdapter;
.source "CalendarSchedulerModel_Table.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/raizlabs/android/dbflow/structure/ModelAdapter<",
        "Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;",
        ">;"
    }
.end annotation


# static fields
.field public static final ALL_COLUMN_PROPERTIES:[Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;

.field public static final calendarData:Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/Property<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final experienceId:Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/Property<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final id:Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/Property<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final notificationId:Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/Property<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final repeat:Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/Property<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final serializedDetails:Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/Property<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 31
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const-class v1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    const-string v2, "id"

    invoke-direct {v0, v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->id:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    .line 33
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const-class v1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    const-string v2, "notificationId"

    invoke-direct {v0, v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->notificationId:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    .line 35
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const-class v1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    const-string v2, "experienceId"

    invoke-direct {v0, v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->experienceId:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    .line 37
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const-class v1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    const-string v2, "repeat"

    invoke-direct {v0, v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->repeat:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    .line 39
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const-class v1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    const-string v2, "serializedDetails"

    invoke-direct {v0, v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->serializedDetails:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    .line 41
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const-class v1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    const-string v2, "calendarData"

    invoke-direct {v0, v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->calendarData:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/4 v0, 0x6

    .line 43
    new-array v0, v0, [Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;

    sget-object v1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->id:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->notificationId:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->experienceId:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->repeat:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->serializedDetails:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->calendarData:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sput-object v0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->ALL_COLUMN_PROPERTIES:[Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;

    return-void
.end method

.method public constructor <init>(Lcom/raizlabs/android/dbflow/config/DatabaseDefinition;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/raizlabs/android/dbflow/structure/ModelAdapter;-><init>(Lcom/raizlabs/android/dbflow/config/DatabaseDefinition;)V

    return-void
.end method


# virtual methods
.method public final bindToContentValues(Landroid/content/ContentValues;Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)V
    .locals 2

    .line 128
    iget v0, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->id:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "`id`"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 129
    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->bindToInsertValues(Landroid/content/ContentValues;Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)V

    return-void
.end method

.method public bridge synthetic bindToContentValues(Landroid/content/ContentValues;Ljava/lang/Object;)V
    .locals 0

    .line 27
    check-cast p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->bindToContentValues(Landroid/content/ContentValues;Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)V

    return-void
.end method

.method public final bindToDeleteStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)V
    .locals 2

    .line 164
    iget p2, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->id:I

    int-to-long v0, p2

    const/4 p2, 0x1

    invoke-interface {p1, p2, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindLong(IJ)V

    return-void
.end method

.method public bridge synthetic bindToDeleteStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Ljava/lang/Object;)V
    .locals 0

    .line 27
    check-cast p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->bindToDeleteStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)V

    return-void
.end method

.method public final bindToInsertStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;I)V
    .locals 3

    add-int/lit8 v0, p3, 0x1

    .line 135
    iget v1, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->notificationId:I

    int-to-long v1, v1

    invoke-interface {p1, v0, v1, v2}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindLong(IJ)V

    add-int/lit8 v0, p3, 0x2

    .line 136
    iget-object v1, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->experienceId:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    add-int/lit8 v0, p3, 0x3

    .line 137
    iget-boolean v1, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->repeat:Z

    if-eqz v1, :cond_0

    const-wide/16 v1, 0x1

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x0

    :goto_0
    invoke-interface {p1, v0, v1, v2}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindLong(IJ)V

    add-int/lit8 v0, p3, 0x4

    .line 138
    iget-object v1, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->serializedDetails:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    add-int/lit8 p3, p3, 0x5

    .line 139
    iget-object p2, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->calendarData:Ljava/lang/String;

    invoke-interface {p1, p3, p2}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    return-void
.end method

.method public bridge synthetic bindToInsertStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Ljava/lang/Object;I)V
    .locals 0

    .line 27
    check-cast p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    invoke-virtual {p0, p1, p2, p3}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->bindToInsertStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;I)V

    return-void
.end method

.method public final bindToInsertValues(Landroid/content/ContentValues;Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)V
    .locals 2

    .line 119
    iget v0, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->notificationId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "`notificationId`"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 120
    iget-object v0, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->experienceId:Ljava/lang/String;

    const-string v1, "`experienceId`"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-boolean v0, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->repeat:Z

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "`repeat`"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 122
    iget-object v0, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->serializedDetails:Ljava/lang/String;

    const-string v1, "`serializedDetails`"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object p2, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->calendarData:Ljava/lang/String;

    const-string v0, "`calendarData`"

    invoke-virtual {p1, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic bindToInsertValues(Landroid/content/ContentValues;Ljava/lang/Object;)V
    .locals 0

    .line 27
    check-cast p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->bindToInsertValues(Landroid/content/ContentValues;Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)V

    return-void
.end method

.method public final bindToStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)V
    .locals 3

    .line 145
    iget v0, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->id:I

    int-to-long v0, v0

    const/4 v2, 0x1

    invoke-interface {p1, v2, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindLong(IJ)V

    .line 146
    invoke-virtual {p0, p1, p2, v2}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->bindToInsertStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;I)V

    return-void
.end method

.method public bridge synthetic bindToStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Ljava/lang/Object;)V
    .locals 0

    .line 27
    check-cast p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->bindToStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)V

    return-void
.end method

.method public final bindToUpdateStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)V
    .locals 3

    .line 152
    iget v0, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->id:I

    int-to-long v0, v0

    const/4 v2, 0x1

    invoke-interface {p1, v2, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindLong(IJ)V

    .line 153
    iget v0, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->notificationId:I

    int-to-long v0, v0

    const/4 v2, 0x2

    invoke-interface {p1, v2, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindLong(IJ)V

    .line 154
    iget-object v0, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->experienceId:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-interface {p1, v1, v0}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    .line 155
    iget-boolean v0, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->repeat:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x1

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    const/4 v2, 0x4

    invoke-interface {p1, v2, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindLong(IJ)V

    const/4 v0, 0x5

    .line 156
    iget-object v1, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->serializedDetails:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    const/4 v0, 0x6

    .line 157
    iget-object v1, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->calendarData:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    const/4 v0, 0x7

    .line 158
    iget p2, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->id:I

    int-to-long v1, p2

    invoke-interface {p1, v0, v1, v2}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindLong(IJ)V

    return-void
.end method

.method public bridge synthetic bindToUpdateStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Ljava/lang/Object;)V
    .locals 0

    .line 27
    check-cast p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->bindToUpdateStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)V

    return-void
.end method

.method public final createSingleModelSaver()Lcom/raizlabs/android/dbflow/sql/saveable/ModelSaver;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/raizlabs/android/dbflow/sql/saveable/ModelSaver<",
            "Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;",
            ">;"
        }
    .end annotation

    .line 109
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/saveable/AutoIncrementModelSaver;

    invoke-direct {v0}, Lcom/raizlabs/android/dbflow/sql/saveable/AutoIncrementModelSaver;-><init>()V

    return-object v0
.end method

.method public final exists(Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;)Z
    .locals 4

    .line 209
    iget v0, p1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->id:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez v0, :cond_0

    new-array v0, v2, [Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;

    .line 210
    invoke-static {v0}, Lcom/raizlabs/android/dbflow/sql/language/SQLite;->selectCountOf([Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;)Lcom/raizlabs/android/dbflow/sql/language/Select;

    move-result-object v0

    const-class v3, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    .line 211
    invoke-virtual {v0, v3}, Lcom/raizlabs/android/dbflow/sql/language/Select;->from(Ljava/lang/Class;)Lcom/raizlabs/android/dbflow/sql/language/From;

    move-result-object v0

    new-array v3, v1, [Lcom/raizlabs/android/dbflow/sql/language/SQLOperator;

    .line 212
    invoke-virtual {p0, p1}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->getPrimaryConditionClause(Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;

    move-result-object p1

    aput-object p1, v3, v2

    invoke-virtual {v0, v3}, Lcom/raizlabs/android/dbflow/sql/language/From;->where([Lcom/raizlabs/android/dbflow/sql/language/SQLOperator;)Lcom/raizlabs/android/dbflow/sql/language/Where;

    move-result-object p1

    .line 213
    invoke-virtual {p1, p2}, Lcom/raizlabs/android/dbflow/sql/language/Where;->hasData(Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public bridge synthetic exists(Ljava/lang/Object;Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;)Z
    .locals 0

    .line 27
    check-cast p1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->exists(Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;)Z

    move-result p1

    return p1
.end method

.method public final getAllColumnProperties()[Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;
    .locals 1

    .line 114
    sget-object v0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->ALL_COLUMN_PROPERTIES:[Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;

    return-object v0
.end method

.method public final getAutoIncrementingColumnName()Ljava/lang/String;
    .locals 1

    const-string v0, "id"

    return-object v0
.end method

.method public final getAutoIncrementingId(Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)Ljava/lang/Number;
    .locals 0

    .line 99
    iget p1, p1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->id:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getAutoIncrementingId(Ljava/lang/Object;)Ljava/lang/Number;
    .locals 0

    .line 27
    check-cast p1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    invoke-virtual {p0, p1}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->getAutoIncrementingId(Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)Ljava/lang/Number;

    move-result-object p1

    return-object p1
.end method

.method public final getCompiledStatementQuery()Ljava/lang/String;
    .locals 1

    const-string v0, "INSERT INTO `CalendarSchedulerModel`(`id`,`notificationId`,`experienceId`,`repeat`,`serializedDetails`,`calendarData`) VALUES (?,?,?,?,?,?)"

    return-object v0
.end method

.method public final getCreationQuery()Ljava/lang/String;
    .locals 1

    const-string v0, "CREATE TABLE IF NOT EXISTS `CalendarSchedulerModel`(`id` INTEGER PRIMARY KEY AUTOINCREMENT, `notificationId` INTEGER, `experienceId` TEXT, `repeat` INTEGER, `serializedDetails` TEXT, `calendarData` TEXT)"

    return-object v0
.end method

.method public final getDeleteStatementQuery()Ljava/lang/String;
    .locals 1

    const-string v0, "DELETE FROM `CalendarSchedulerModel` WHERE `id`=?"

    return-object v0
.end method

.method public final getInsertStatementQuery()Ljava/lang/String;
    .locals 1

    const-string v0, "INSERT INTO `CalendarSchedulerModel`(`notificationId`,`experienceId`,`repeat`,`serializedDetails`,`calendarData`) VALUES (?,?,?,?,?)"

    return-object v0
.end method

.method public final getModelClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;",
            ">;"
        }
    .end annotation

    .line 51
    const-class v0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    return-object v0
.end method

.method public final getPrimaryConditionClause(Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;
    .locals 2

    .line 218
    invoke-static {}, Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;->clause()Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;

    move-result-object v0

    .line 219
    sget-object v1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->id:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    iget p1, p1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->id:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;->eq(Ljava/lang/Object;)Lcom/raizlabs/android/dbflow/sql/language/Operator;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;->and(Lcom/raizlabs/android/dbflow/sql/language/SQLOperator;)Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;

    return-object v0
.end method

.method public bridge synthetic getPrimaryConditionClause(Ljava/lang/Object;)Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;
    .locals 0

    .line 27
    check-cast p1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    invoke-virtual {p0, p1}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->getPrimaryConditionClause(Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;

    move-result-object p1

    return-object p1
.end method

.method public final getProperty(Ljava/lang/String;)Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .locals 6

    .line 66
    invoke-static {p1}, Lcom/raizlabs/android/dbflow/sql/QueryBuilder;->quoteIfNeeded(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 67
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x5

    const/4 v2, 0x4

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v0, "`serializedDetails`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x4

    goto :goto_1

    :sswitch_1
    const-string v0, "`notificationId`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_1

    :sswitch_2
    const-string v0, "`repeat`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x3

    goto :goto_1

    :sswitch_3
    const-string v0, "`experienceId`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    goto :goto_1

    :sswitch_4
    const-string v0, "`calendarData`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x5

    goto :goto_1

    :sswitch_5
    const-string v0, "`id`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_1

    :cond_0
    :goto_0
    const/4 p1, -0x1

    :goto_1
    if-eqz p1, :cond_6

    if-eq p1, v5, :cond_5

    if-eq p1, v4, :cond_4

    if-eq p1, v3, :cond_3

    if-eq p1, v2, :cond_2

    if-ne p1, v1, :cond_1

    .line 84
    sget-object p1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->calendarData:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    return-object p1

    .line 87
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Invalid column name passed. Ensure you are calling the correct table\'s column"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 81
    :cond_2
    sget-object p1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->serializedDetails:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    return-object p1

    .line 78
    :cond_3
    sget-object p1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->repeat:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    return-object p1

    .line 75
    :cond_4
    sget-object p1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->experienceId:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    return-object p1

    .line 72
    :cond_5
    sget-object p1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->notificationId:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    return-object p1

    .line 69
    :cond_6
    sget-object p1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->id:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    return-object p1

    :sswitch_data_0
    .sparse-switch
        0x2d3a45 -> :sswitch_5
        0x26aa0cf8 -> :sswitch_4
        0x2f2bf07b -> :sswitch_3
        0x35aa59c5 -> :sswitch_2
        0x47379d5a -> :sswitch_1
        0x6d641462 -> :sswitch_0
    .end sparse-switch
.end method

.method public final getTableName()Ljava/lang/String;
    .locals 1

    const-string v0, "`CalendarSchedulerModel`"

    return-object v0
.end method

.method public final getUpdateStatementQuery()Ljava/lang/String;
    .locals 1

    const-string v0, "UPDATE `CalendarSchedulerModel` SET `id`=?,`notificationId`=?,`experienceId`=?,`repeat`=?,`serializedDetails`=?,`calendarData`=? WHERE `id`=?"

    return-object v0
.end method

.method public final loadFromCursor(Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)V
    .locals 2

    const-string v0, "id"

    .line 194
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getIntOrDefault(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->id:I

    const-string v0, "notificationId"

    .line 195
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getIntOrDefault(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->notificationId:I

    const-string v0, "experienceId"

    .line 196
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getStringOrDefault(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->experienceId:Ljava/lang/String;

    const-string v0, "repeat"

    .line 197
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 198
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 199
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->repeat:Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 201
    iput-boolean v0, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->repeat:Z

    :goto_0
    const-string v0, "serializedDetails"

    .line 203
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getStringOrDefault(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->serializedDetails:Ljava/lang/String;

    const-string v0, "calendarData"

    .line 204
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getStringOrDefault(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->calendarData:Ljava/lang/String;

    return-void
.end method

.method public bridge synthetic loadFromCursor(Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;Ljava/lang/Object;)V
    .locals 0

    .line 27
    check-cast p2, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->loadFromCursor(Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;)V

    return-void
.end method

.method public final newInstance()Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;
    .locals 1

    .line 61
    new-instance v0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    invoke-direct {v0}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->newInstance()Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    move-result-object v0

    return-object v0
.end method

.method public final updateAutoIncrement(Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;Ljava/lang/Number;)V
    .locals 0

    .line 94
    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    iput p2, p1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->id:I

    return-void
.end method

.method public bridge synthetic updateAutoIncrement(Ljava/lang/Object;Ljava/lang/Number;)V
    .locals 0

    .line 27
    check-cast p1, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel_Table;->updateAutoIncrement(Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;Ljava/lang/Number;)V

    return-void
.end method
