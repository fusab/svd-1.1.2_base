.class public Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;
.super Lcom/raizlabs/android/dbflow/structure/BaseModel;
.source "CalendarSchedulerModel.java"

# interfaces
.implements Lhost/exp/exponent/notifications/schedulers/SchedulerModel;


# static fields
.field private static triggeringActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field calendarData:Ljava/lang/String;

.field private details:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field experienceId:Ljava/lang/String;

.field id:I

.field notificationId:I

.field repeat:Z

.field serializedDetails:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x0

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    const-string v2, "android.intent.action.REBOOT"

    const-string v3, "android.intent.action.TIME_SET"

    const-string v4, "android.intent.action.TIMEZONE_CHANGED"

    .line 29
    filled-new-array {v0, v1, v2, v3, v4}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->triggeringActions:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/raizlabs/android/dbflow/structure/BaseModel;-><init>()V

    return-void
.end method


# virtual methods
.method public canBeRescheduled()Z
    .locals 1

    .line 72
    iget-boolean v0, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->repeat:Z

    return v0
.end method

.method public getCalendarData()Ljava/lang/String;
    .locals 1

    .line 154
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->calendarData:Ljava/lang/String;

    return-object v0
.end method

.method public getDetails()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 162
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->serializedDetails:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->setSerializedDetails(Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->details:Ljava/util/HashMap;

    return-object v0
.end method

.method public getExperienceId()Ljava/lang/String;
    .locals 1

    .line 125
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->experienceId:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .line 109
    iget v0, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->id:I

    return v0
.end method

.method public getIdAsString()Ljava/lang/String;
    .locals 2

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->id:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNextAppearanceTime()J
    .locals 6

    .line 90
    invoke-static {}, Lhost/exp/exponent/notifications/helpers/ExpoCronDefinitionBuilder;->getCronDefinition()Lcom/cronutils/model/definition/CronDefinition;

    move-result-object v0

    .line 91
    new-instance v1, Lcom/cronutils/parser/CronParser;

    invoke-direct {v1, v0}, Lcom/cronutils/parser/CronParser;-><init>(Lcom/cronutils/model/definition/CronDefinition;)V

    .line 92
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->calendarData:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/cronutils/parser/CronParser;->parse(Ljava/lang/String;)Lcom/cronutils/model/Cron;

    move-result-object v0

    .line 94
    invoke-static {}, Lorg/joda/time/DateTime;->now()Lorg/joda/time/DateTime;

    move-result-object v1

    .line 95
    invoke-static {v0}, Lcom/cronutils/model/time/ExecutionTime;->forCron(Lcom/cronutils/model/Cron;)Lcom/cronutils/model/time/ExecutionTime;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/cronutils/model/time/ExecutionTime;->nextExecution(Lorg/joda/time/DateTime;)Lorg/joda/time/DateTime;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lorg/joda/time/DateTime;->toDate()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 97
    invoke-static {}, Lorg/joda/time/DateTime;->now()Lorg/joda/time/DateTime;

    move-result-object v2

    invoke-virtual {v2}, Lorg/joda/time/DateTime;->toDate()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public getNotificationId()I
    .locals 1

    .line 117
    iget v0, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->notificationId:I

    return v0
.end method

.method public getOwnerExperienceId()Ljava/lang/String;
    .locals 1

    .line 67
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->experienceId:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedDetails()Ljava/lang/String;
    .locals 1

    .line 141
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->serializedDetails:Ljava/lang/String;

    return-object v0
.end method

.method public isRepeat()Z
    .locals 1

    .line 133
    iget-boolean v0, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->repeat:Z

    return v0
.end method

.method public remove()V
    .locals 0

    .line 86
    invoke-virtual {p0}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->delete()Z

    return-void
.end method

.method public saveAndGetId()Ljava/lang/String;
    .locals 3

    .line 77
    invoke-virtual {p0}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->save()Z

    .line 78
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->details:Ljava/util/HashMap;

    invoke-virtual {p0}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->getIdAsString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "scheduler_id"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->details:Ljava/util/HashMap;

    invoke-virtual {p0, v0}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->setDetails(Ljava/util/HashMap;)V

    .line 80
    invoke-virtual {p0}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->save()Z

    .line 81
    invoke-virtual {p0}, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->getIdAsString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setCalendarData(Ljava/lang/String;)V
    .locals 0

    .line 158
    iput-object p1, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->calendarData:Ljava/lang/String;

    return-void
.end method

.method public setDetails(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 167
    iput-object p1, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->details:Ljava/util/HashMap;

    .line 168
    invoke-static {p1}, Lhost/exp/exponent/notifications/schedulers/HashMapSerializer;->serialize(Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->serializedDetails:Ljava/lang/String;

    return-void
.end method

.method public setExperienceId(Ljava/lang/String;)V
    .locals 0

    .line 129
    iput-object p1, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->experienceId:Ljava/lang/String;

    return-void
.end method

.method public setId(I)V
    .locals 0

    .line 113
    iput p1, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->id:I

    return-void
.end method

.method public setNotificationId(I)V
    .locals 0

    .line 121
    iput p1, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->notificationId:I

    return-void
.end method

.method public setRepeat(Z)V
    .locals 0

    .line 137
    iput-boolean p1, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->repeat:Z

    return-void
.end method

.method public setSerializedDetails(Ljava/lang/String;)V
    .locals 1

    .line 146
    :try_start_0
    invoke-static {p1}, Lhost/exp/exponent/notifications/schedulers/HashMapSerializer;->deserialize(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->details:Ljava/util/HashMap;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 148
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 150
    :goto_0
    iput-object p1, p0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->serializedDetails:Ljava/lang/String;

    return-void
.end method

.method public shouldBeTriggeredByAction(Ljava/lang/String;)Z
    .locals 1

    .line 103
    sget-object v0, Lhost/exp/exponent/notifications/schedulers/CalendarSchedulerModel;->triggeringActions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method
