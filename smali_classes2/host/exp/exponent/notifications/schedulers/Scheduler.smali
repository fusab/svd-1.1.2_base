.class public interface abstract Lhost/exp/exponent/notifications/schedulers/Scheduler;
.super Ljava/lang/Object;
.source "Scheduler.java"


# virtual methods
.method public abstract canBeRescheduled()Z
.end method

.method public abstract cancel()V
.end method

.method public abstract getIdAsString()Ljava/lang/String;
.end method

.method public abstract getOwnerExperienceId()Ljava/lang/String;
.end method

.method public abstract remove()V
.end method

.method public abstract saveAndGetId()Ljava/lang/String;
.end method

.method public abstract schedule(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lhost/exp/exponent/notifications/exceptions/UnableToScheduleException;
        }
    .end annotation
.end method

.method public abstract setApplicationContext(Landroid/content/Context;)V
.end method
