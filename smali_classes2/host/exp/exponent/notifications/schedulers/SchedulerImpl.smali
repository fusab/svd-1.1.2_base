.class public Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;
.super Ljava/lang/Object;
.source "SchedulerImpl.java"

# interfaces
.implements Lhost/exp/exponent/notifications/schedulers/Scheduler;


# instance fields
.field private mApplicationContext:Landroid/content/Context;

.field private mSchedulerModel:Lhost/exp/exponent/notifications/schedulers/SchedulerModel;


# direct methods
.method public constructor <init>(Lhost/exp/exponent/notifications/schedulers/SchedulerModel;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->mSchedulerModel:Lhost/exp/exponent/notifications/schedulers/SchedulerModel;

    return-void
.end method

.method private getManager()Lhost/exp/exponent/notifications/ExponentNotificationManager;
    .locals 2

    .line 83
    new-instance v0, Lhost/exp/exponent/notifications/ExponentNotificationManager;

    iget-object v1, p0, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->mApplicationContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lhost/exp/exponent/notifications/ExponentNotificationManager;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public canBeRescheduled()Z
    .locals 1

    .line 63
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->mSchedulerModel:Lhost/exp/exponent/notifications/schedulers/SchedulerModel;

    invoke-interface {v0}, Lhost/exp/exponent/notifications/schedulers/SchedulerModel;->canBeRescheduled()Z

    move-result v0

    return v0
.end method

.method public cancel()V
    .locals 3

    .line 56
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->mSchedulerModel:Lhost/exp/exponent/notifications/schedulers/SchedulerModel;

    invoke-interface {v0}, Lhost/exp/exponent/notifications/schedulers/SchedulerModel;->getOwnerExperienceId()Ljava/lang/String;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->mSchedulerModel:Lhost/exp/exponent/notifications/schedulers/SchedulerModel;

    invoke-interface {v1}, Lhost/exp/exponent/notifications/schedulers/SchedulerModel;->getNotificationId()I

    move-result v1

    .line 58
    invoke-direct {p0}, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->getManager()Lhost/exp/exponent/notifications/ExponentNotificationManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lhost/exp/exponent/notifications/ExponentNotificationManager;->cancel(Ljava/lang/String;I)V

    return-void
.end method

.method public getIdAsString()Ljava/lang/String;
    .locals 1

    .line 46
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->mSchedulerModel:Lhost/exp/exponent/notifications/schedulers/SchedulerModel;

    invoke-interface {v0}, Lhost/exp/exponent/notifications/schedulers/SchedulerModel;->getIdAsString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOwnerExperienceId()Ljava/lang/String;
    .locals 1

    .line 51
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->mSchedulerModel:Lhost/exp/exponent/notifications/schedulers/SchedulerModel;

    invoke-interface {v0}, Lhost/exp/exponent/notifications/schedulers/SchedulerModel;->getOwnerExperienceId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .line 78
    invoke-virtual {p0}, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->cancel()V

    .line 79
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->mSchedulerModel:Lhost/exp/exponent/notifications/schedulers/SchedulerModel;

    invoke-interface {v0}, Lhost/exp/exponent/notifications/schedulers/SchedulerModel;->remove()V

    return-void
.end method

.method public saveAndGetId()Ljava/lang/String;
    .locals 1

    .line 68
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->mSchedulerModel:Lhost/exp/exponent/notifications/schedulers/SchedulerModel;

    invoke-interface {v0}, Lhost/exp/exponent/notifications/schedulers/SchedulerModel;->saveAndGetId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public schedule(Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lhost/exp/exponent/notifications/exceptions/UnableToScheduleException;
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->mSchedulerModel:Lhost/exp/exponent/notifications/schedulers/SchedulerModel;

    invoke-interface {v0, p1}, Lhost/exp/exponent/notifications/schedulers/SchedulerModel;->shouldBeTriggeredByAction(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 28
    :cond_0
    :try_start_0
    iget-object p1, p0, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->mSchedulerModel:Lhost/exp/exponent/notifications/schedulers/SchedulerModel;

    invoke-interface {p1}, Lhost/exp/exponent/notifications/schedulers/SchedulerModel;->getNextAppearanceTime()J

    move-result-wide v4
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 33
    iget-object p1, p0, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->mSchedulerModel:Lhost/exp/exponent/notifications/schedulers/SchedulerModel;

    invoke-interface {p1}, Lhost/exp/exponent/notifications/schedulers/SchedulerModel;->getOwnerExperienceId()Ljava/lang/String;

    move-result-object v1

    .line 34
    iget-object p1, p0, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->mSchedulerModel:Lhost/exp/exponent/notifications/schedulers/SchedulerModel;

    invoke-interface {p1}, Lhost/exp/exponent/notifications/schedulers/SchedulerModel;->getNotificationId()I

    move-result v2

    .line 35
    iget-object p1, p0, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->mSchedulerModel:Lhost/exp/exponent/notifications/schedulers/SchedulerModel;

    invoke-interface {p1}, Lhost/exp/exponent/notifications/schedulers/SchedulerModel;->getDetails()Ljava/util/HashMap;

    move-result-object v3

    .line 38
    :try_start_1
    invoke-direct {p0}, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->getManager()Lhost/exp/exponent/notifications/ExponentNotificationManager;

    move-result-object v0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lhost/exp/exponent/notifications/ExponentNotificationManager;->schedule(Ljava/lang/String;ILjava/util/HashMap;JLjava/lang/Long;)V
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 40
    invoke-virtual {p1}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    :goto_0
    return-void

    .line 30
    :catch_1
    new-instance p1, Lhost/exp/exponent/notifications/exceptions/UnableToScheduleException;

    invoke-direct {p1}, Lhost/exp/exponent/notifications/exceptions/UnableToScheduleException;-><init>()V

    throw p1
.end method

.method public setApplicationContext(Landroid/content/Context;)V
    .locals 0

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lhost/exp/exponent/notifications/schedulers/SchedulerImpl;->mApplicationContext:Landroid/content/Context;

    return-void
.end method
