.class public interface abstract Lhost/exp/exponent/notifications/schedulers/SchedulerModel;
.super Ljava/lang/Object;
.source "SchedulerModel.java"


# virtual methods
.method public abstract canBeRescheduled()Z
.end method

.method public abstract getDetails()Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getIdAsString()Ljava/lang/String;
.end method

.method public abstract getNextAppearanceTime()J
.end method

.method public abstract getNotificationId()I
.end method

.method public abstract getOwnerExperienceId()Ljava/lang/String;
.end method

.method public abstract remove()V
.end method

.method public abstract saveAndGetId()Ljava/lang/String;
.end method

.method public abstract shouldBeTriggeredByAction(Ljava/lang/String;)Z
.end method
