.class Lhost/exp/exponent/notifications/NotificationHelper$2$1$1;
.super Ljava/lang/Object;
.source "NotificationHelper.java"

# interfaces
.implements Lhost/exp/exponent/notifications/IntentProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/notifications/NotificationHelper$2$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lhost/exp/exponent/notifications/NotificationHelper$2$1;

.field final synthetic val$body:Ljava/lang/String;

.field final synthetic val$manifestUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhost/exp/exponent/notifications/NotificationHelper$2$1;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 504
    iput-object p1, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$1;->this$1:Lhost/exp/exponent/notifications/NotificationHelper$2$1;

    iput-object p2, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$1;->val$manifestUrl:Ljava/lang/String;

    iput-object p3, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$1;->val$body:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public provide()Landroid/content/Intent;
    .locals 9

    .line 507
    sget-object v0, Lhost/exp/exponent/kernel/KernelConstants;->MAIN_ACTIVITY_CLASS:Ljava/lang/Class;

    .line 508
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$1;->this$1:Lhost/exp/exponent/notifications/NotificationHelper$2$1;

    iget-object v2, v2, Lhost/exp/exponent/notifications/NotificationHelper$2$1;->this$0:Lhost/exp/exponent/notifications/NotificationHelper$2;

    iget-object v2, v2, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$context:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 509
    iget-object v0, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$1;->val$manifestUrl:Ljava/lang/String;

    const-string v2, "notificationExperienceUrl"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 510
    new-instance v0, Lhost/exp/exponent/notifications/ReceivedNotificationEvent;

    iget-object v2, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$1;->this$1:Lhost/exp/exponent/notifications/NotificationHelper$2$1;

    iget-object v2, v2, Lhost/exp/exponent/notifications/NotificationHelper$2$1;->this$0:Lhost/exp/exponent/notifications/NotificationHelper$2;

    iget-object v4, v2, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$experienceId:Ljava/lang/String;

    iget-object v5, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$1;->val$body:Ljava/lang/String;

    iget-object v2, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$1;->this$1:Lhost/exp/exponent/notifications/NotificationHelper$2$1;

    iget-object v2, v2, Lhost/exp/exponent/notifications/NotificationHelper$2$1;->this$0:Lhost/exp/exponent/notifications/NotificationHelper$2;

    iget v6, v2, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$id:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v0

    invoke-direct/range {v3 .. v8}, Lhost/exp/exponent/notifications/ReceivedNotificationEvent;-><init>(Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 511
    iget-object v2, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$1;->val$body:Ljava/lang/String;

    const-string v3, "notification"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v2, 0x0

    .line 512
    invoke-virtual {v0, v2}, Lhost/exp/exponent/notifications/ReceivedNotificationEvent;->toJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "notification_object"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v1
.end method
