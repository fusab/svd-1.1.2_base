.class public Lhost/exp/exponent/notifications/helpers/ExpoCronDefinitionBuilder;
.super Ljava/lang/Object;
.source "ExpoCronDefinitionBuilder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCronConstraint()Lcom/cronutils/model/definition/CronConstraint;
    .locals 2

    .line 35
    new-instance v0, Lhost/exp/exponent/notifications/helpers/ExpoCronDefinitionBuilder$1;

    const-string v1, "Both, a day-of-week AND a day-of-month parameter, are not supported."

    invoke-direct {v0, v1}, Lhost/exp/exponent/notifications/helpers/ExpoCronDefinitionBuilder$1;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static getCronDefinition()Lcom/cronutils/model/definition/CronDefinition;
    .locals 3

    .line 20
    invoke-static {}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->defineCron()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 21
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withSeconds()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withMinutes()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 23
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withHours()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withDayOfMonth()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->supportsHash()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->supportsL()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->supportsW()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->supportsLW()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->supportsQuestionMark()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withMonth()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withDayOfWeek()Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->withValidRange(II)Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->withMondayDoWValue(I)Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDayOfWeekDefinitionBuilder;->supportsHash()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->supportsL()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->supportsW()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->supportsQuestionMark()Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldSpecialCharsDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withYear()Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    const/16 v1, 0x7b2

    const/16 v2, 0x833

    invoke-virtual {v0, v1, v2}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->withValidRange(II)Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/definition/FieldDefinitionBuilder;->and()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->lastFieldOptional()Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 30
    invoke-static {}, Lhost/exp/exponent/notifications/helpers/ExpoCronDefinitionBuilder;->getCronConstraint()Lcom/cronutils/model/definition/CronConstraint;

    move-result-object v1

    .line 29
    invoke-virtual {v0, v1}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->withCronValidation(Lcom/cronutils/model/definition/CronConstraint;)Lcom/cronutils/model/definition/CronDefinitionBuilder;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lcom/cronutils/model/definition/CronDefinitionBuilder;->instance()Lcom/cronutils/model/definition/CronDefinition;

    move-result-object v0

    return-object v0
.end method
