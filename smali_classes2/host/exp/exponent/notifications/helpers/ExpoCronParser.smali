.class public Lhost/exp/exponent/notifications/helpers/ExpoCronParser;
.super Ljava/lang/Object;
.source "ExpoCronParser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createCronInstance(Ljava/util/HashMap;)Lcom/cronutils/model/Cron;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/cronutils/model/Cron;"
        }
    .end annotation

    .line 14
    invoke-static {}, Lhost/exp/exponent/notifications/helpers/ExpoCronDefinitionBuilder;->getCronDefinition()Lcom/cronutils/model/definition/CronDefinition;

    move-result-object v0

    invoke-static {v0}, Lcom/cronutils/builder/CronBuilder;->cron(Lcom/cronutils/model/definition/CronDefinition;)Lcom/cronutils/builder/CronBuilder;

    move-result-object v0

    const-string v1, "year"

    .line 16
    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Ljava/lang/Number;

    if-eqz v2, :cond_0

    .line 17
    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/cronutils/model/field/expression/FieldExpressionFactory;->on(I)Lcom/cronutils/model/field/expression/On;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cronutils/builder/CronBuilder;->withYear(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    goto :goto_0

    .line 19
    :cond_0
    invoke-static {}, Lcom/cronutils/model/field/expression/FieldExpressionFactory;->always()Lcom/cronutils/model/field/expression/Always;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cronutils/builder/CronBuilder;->withYear(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    :goto_0
    const-string v1, "hour"

    .line 22
    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Ljava/lang/Number;

    if-eqz v2, :cond_1

    .line 23
    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/cronutils/model/field/expression/FieldExpressionFactory;->on(I)Lcom/cronutils/model/field/expression/On;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cronutils/builder/CronBuilder;->withHour(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    goto :goto_1

    .line 25
    :cond_1
    invoke-static {}, Lcom/cronutils/model/field/expression/FieldExpressionFactory;->always()Lcom/cronutils/model/field/expression/Always;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cronutils/builder/CronBuilder;->withHour(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    :goto_1
    const-string v1, "minute"

    .line 28
    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Ljava/lang/Number;

    if-eqz v2, :cond_2

    .line 29
    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/cronutils/model/field/expression/FieldExpressionFactory;->on(I)Lcom/cronutils/model/field/expression/On;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cronutils/builder/CronBuilder;->withMinute(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    goto :goto_2

    .line 31
    :cond_2
    invoke-static {}, Lcom/cronutils/model/field/expression/FieldExpressionFactory;->always()Lcom/cronutils/model/field/expression/Always;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cronutils/builder/CronBuilder;->withMinute(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    :goto_2
    const-string v1, "second"

    .line 34
    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Ljava/lang/Number;

    if-eqz v2, :cond_3

    .line 35
    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/cronutils/model/field/expression/FieldExpressionFactory;->on(I)Lcom/cronutils/model/field/expression/On;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cronutils/builder/CronBuilder;->withSecond(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    goto :goto_3

    .line 37
    :cond_3
    invoke-static {}, Lcom/cronutils/model/field/expression/FieldExpressionFactory;->always()Lcom/cronutils/model/field/expression/Always;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cronutils/builder/CronBuilder;->withSecond(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    :goto_3
    const-string v1, "month"

    .line 40
    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Ljava/lang/Number;

    if-eqz v2, :cond_4

    .line 41
    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/cronutils/model/field/expression/FieldExpressionFactory;->on(I)Lcom/cronutils/model/field/expression/On;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cronutils/builder/CronBuilder;->withMonth(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    goto :goto_4

    .line 43
    :cond_4
    invoke-static {}, Lcom/cronutils/model/field/expression/FieldExpressionFactory;->always()Lcom/cronutils/model/field/expression/Always;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cronutils/builder/CronBuilder;->withMonth(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    :goto_4
    const-string v1, "day"

    .line 46
    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Ljava/lang/Number;

    const-string v3, "weekDay"

    if-eqz v2, :cond_5

    .line 47
    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/cronutils/model/field/expression/FieldExpressionFactory;->on(I)Lcom/cronutils/model/field/expression/On;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cronutils/builder/CronBuilder;->withDoM(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    goto :goto_5

    .line 48
    :cond_5
    invoke-virtual {p0, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 49
    invoke-static {}, Lcom/cronutils/model/field/expression/FieldExpressionFactory;->questionMark()Lcom/cronutils/model/field/expression/QuestionMark;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cronutils/builder/CronBuilder;->withDoM(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    goto :goto_5

    .line 51
    :cond_6
    invoke-static {}, Lcom/cronutils/model/field/expression/FieldExpressionFactory;->always()Lcom/cronutils/model/field/expression/Always;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cronutils/builder/CronBuilder;->withDoM(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    .line 54
    :goto_5
    invoke-virtual {p0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Number;

    if-eqz v1, :cond_7

    .line 55
    invoke-virtual {p0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Number;

    invoke-virtual {p0}, Ljava/lang/Number;->intValue()I

    move-result p0

    invoke-static {p0}, Lcom/cronutils/model/field/expression/FieldExpressionFactory;->on(I)Lcom/cronutils/model/field/expression/On;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/cronutils/builder/CronBuilder;->withDoW(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    goto :goto_6

    .line 57
    :cond_7
    invoke-static {}, Lcom/cronutils/model/field/expression/FieldExpressionFactory;->questionMark()Lcom/cronutils/model/field/expression/QuestionMark;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/cronutils/builder/CronBuilder;->withDoW(Lcom/cronutils/model/field/expression/FieldExpression;)Lcom/cronutils/builder/CronBuilder;

    .line 60
    :goto_6
    invoke-virtual {v0}, Lcom/cronutils/builder/CronBuilder;->instance()Lcom/cronutils/model/Cron;

    move-result-object p0

    return-object p0
.end method
