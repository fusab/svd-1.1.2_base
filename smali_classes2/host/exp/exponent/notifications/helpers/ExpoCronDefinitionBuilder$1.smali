.class final Lhost/exp/exponent/notifications/helpers/ExpoCronDefinitionBuilder$1;
.super Lcom/cronutils/model/definition/CronConstraint;
.source "ExpoCronDefinitionBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/notifications/helpers/ExpoCronDefinitionBuilder;->getCronConstraint()Lcom/cronutils/model/definition/CronConstraint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/cronutils/model/definition/CronConstraint;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public validate(Lcom/cronutils/model/Cron;)Z
    .locals 4

    .line 38
    sget-object v0, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_MONTH:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p1, v0}, Lcom/cronutils/model/Cron;->retrieve(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/CronField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object v0

    instance-of v0, v0, Lcom/cronutils/model/field/expression/QuestionMark;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 39
    :goto_0
    sget-object v3, Lcom/cronutils/model/field/CronFieldName;->DAY_OF_WEEK:Lcom/cronutils/model/field/CronFieldName;

    invoke-virtual {p1, v3}, Lcom/cronutils/model/Cron;->retrieve(Lcom/cronutils/model/field/CronFieldName;)Lcom/cronutils/model/field/CronField;

    move-result-object p1

    invoke-virtual {p1}, Lcom/cronutils/model/field/CronField;->getExpression()Lcom/cronutils/model/field/expression/FieldExpression;

    move-result-object p1

    instance-of p1, p1, Lcom/cronutils/model/field/expression/QuestionMark;

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_3

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :cond_3
    :goto_2
    return v1
.end method
