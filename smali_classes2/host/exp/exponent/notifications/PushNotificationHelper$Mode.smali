.class final enum Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;
.super Ljava/lang/Enum;
.source "PushNotificationHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/exponent/notifications/PushNotificationHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;

.field public static final enum COLLAPSE:Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;

.field public static final enum DEFAULT:Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 49
    new-instance v0, Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;

    const/4 v1, 0x0

    const-string v2, "DEFAULT"

    invoke-direct {v0, v2, v1}, Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;->DEFAULT:Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;

    .line 50
    new-instance v0, Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;

    const/4 v2, 0x1

    const-string v3, "COLLAPSE"

    invoke-direct {v0, v3, v2}, Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;->COLLAPSE:Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;

    const/4 v0, 0x2

    .line 48
    new-array v0, v0, [Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;

    sget-object v3, Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;->DEFAULT:Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;

    aput-object v3, v0, v1

    sget-object v1, Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;->COLLAPSE:Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;

    aput-object v1, v0, v2

    sput-object v0, Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;->$VALUES:[Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;
    .locals 1

    .line 48
    const-class v0, Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;

    return-object p0
.end method

.method public static values()[Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;
    .locals 1

    .line 48
    sget-object v0, Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;->$VALUES:[Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;

    invoke-virtual {v0}, [Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhost/exp/exponent/notifications/PushNotificationHelper$Mode;

    return-object v0
.end method
