.class public interface abstract Lhost/exp/exponent/notifications/managers/SchedulersManager;
.super Ljava/lang/Object;
.source "SchedulersManager.java"


# virtual methods
.method public abstract addScheduler(Lhost/exp/exponent/notifications/schedulers/Scheduler;Lorg/unimodules/core/interfaces/Function;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhost/exp/exponent/notifications/schedulers/Scheduler;",
            "Lorg/unimodules/core/interfaces/Function<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract cancelAlreadyScheduled(Ljava/lang/String;)V
.end method

.method public abstract removeAll(Ljava/lang/String;)V
.end method

.method public abstract removeScheduler(Ljava/lang/String;)V
.end method

.method public abstract rescheduleOrDelete(Ljava/lang/String;)V
.end method

.method public abstract triggerAll(Ljava/lang/String;)V
.end method
