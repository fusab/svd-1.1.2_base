.class public Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;
.super Ljava/lang/Object;
.source "SchedulersManagerProxy.java"

# interfaces
.implements Lhost/exp/exponent/notifications/managers/SchedulersManager;


# static fields
.field public static final SCHEDULER_ID:Ljava/lang/String; = "scheduler_id"

.field private static volatile instance:Lhost/exp/exponent/notifications/managers/SchedulersManager;


# instance fields
.field private mSchedulersManager:Lhost/exp/exponent/notifications/managers/SchedulersManager;

.field private mSingleThreadExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Lhost/exp/exponent/notifications/managers/SchedulersManager;)V
    .locals 1

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;->mSingleThreadExecutor:Ljava/util/concurrent/Executor;

    .line 23
    iput-object p1, p0, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;->mSchedulersManager:Lhost/exp/exponent/notifications/managers/SchedulersManager;

    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lhost/exp/exponent/notifications/managers/SchedulersManager;
    .locals 3

    const-class v0, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;

    monitor-enter v0

    .line 27
    :try_start_0
    sget-object v1, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;->instance:Lhost/exp/exponent/notifications/managers/SchedulersManager;

    if-nez v1, :cond_0

    .line 28
    new-instance v1, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;

    new-instance v2, Lhost/exp/exponent/notifications/managers/SchedulerManagerImpl;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v2, p0}, Lhost/exp/exponent/notifications/managers/SchedulerManagerImpl;-><init>(Landroid/content/Context;)V

    invoke-direct {v1, v2}, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;-><init>(Lhost/exp/exponent/notifications/managers/SchedulersManager;)V

    sput-object v1, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;->instance:Lhost/exp/exponent/notifications/managers/SchedulersManager;

    .line 30
    :cond_0
    sget-object p0, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;->instance:Lhost/exp/exponent/notifications/managers/SchedulersManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method


# virtual methods
.method public addScheduler(Lhost/exp/exponent/notifications/schedulers/Scheduler;Lorg/unimodules/core/interfaces/Function;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhost/exp/exponent/notifications/schedulers/Scheduler;",
            "Lorg/unimodules/core/interfaces/Function<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;->mSingleThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhost/exp/exponent/notifications/managers/-$$Lambda$SchedulersManagerProxy$GMwKAjfRgwGMPfVqtNGeAFqosw4;

    invoke-direct {v1, p0, p1, p2}, Lhost/exp/exponent/notifications/managers/-$$Lambda$SchedulersManagerProxy$GMwKAjfRgwGMPfVqtNGeAFqosw4;-><init>(Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;Lhost/exp/exponent/notifications/schedulers/Scheduler;Lorg/unimodules/core/interfaces/Function;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public cancelAlreadyScheduled(Ljava/lang/String;)V
    .locals 2

    .line 45
    iget-object v0, p0, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;->mSingleThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhost/exp/exponent/notifications/managers/-$$Lambda$SchedulersManagerProxy$wRDMzZAGCuuFs6p_JLsHwPQ3sQA;

    invoke-direct {v1, p0, p1}, Lhost/exp/exponent/notifications/managers/-$$Lambda$SchedulersManagerProxy$wRDMzZAGCuuFs6p_JLsHwPQ3sQA;-><init>(Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$addScheduler$5$SchedulersManagerProxy(Lhost/exp/exponent/notifications/schedulers/Scheduler;Lorg/unimodules/core/interfaces/Function;)V
    .locals 1

    .line 60
    iget-object v0, p0, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;->mSchedulersManager:Lhost/exp/exponent/notifications/managers/SchedulersManager;

    invoke-interface {v0, p1, p2}, Lhost/exp/exponent/notifications/managers/SchedulersManager;->addScheduler(Lhost/exp/exponent/notifications/schedulers/Scheduler;Lorg/unimodules/core/interfaces/Function;)V

    return-void
.end method

.method public synthetic lambda$cancelAlreadyScheduled$2$SchedulersManagerProxy(Ljava/lang/String;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;->mSchedulersManager:Lhost/exp/exponent/notifications/managers/SchedulersManager;

    invoke-interface {v0, p1}, Lhost/exp/exponent/notifications/managers/SchedulersManager;->cancelAlreadyScheduled(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$removeAll$1$SchedulersManagerProxy(Ljava/lang/String;)V
    .locals 1

    .line 40
    iget-object v0, p0, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;->mSchedulersManager:Lhost/exp/exponent/notifications/managers/SchedulersManager;

    invoke-interface {v0, p1}, Lhost/exp/exponent/notifications/managers/SchedulersManager;->removeAll(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$removeScheduler$4$SchedulersManagerProxy(Ljava/lang/String;)V
    .locals 1

    .line 55
    iget-object v0, p0, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;->mSchedulersManager:Lhost/exp/exponent/notifications/managers/SchedulersManager;

    invoke-interface {v0, p1}, Lhost/exp/exponent/notifications/managers/SchedulersManager;->removeScheduler(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$rescheduleOrDelete$3$SchedulersManagerProxy(Ljava/lang/String;)V
    .locals 1

    .line 50
    iget-object v0, p0, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;->mSchedulersManager:Lhost/exp/exponent/notifications/managers/SchedulersManager;

    invoke-interface {v0, p1}, Lhost/exp/exponent/notifications/managers/SchedulersManager;->rescheduleOrDelete(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$triggerAll$0$SchedulersManagerProxy(Ljava/lang/String;)V
    .locals 1

    .line 35
    iget-object v0, p0, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;->mSchedulersManager:Lhost/exp/exponent/notifications/managers/SchedulersManager;

    invoke-interface {v0, p1}, Lhost/exp/exponent/notifications/managers/SchedulersManager;->triggerAll(Ljava/lang/String;)V

    return-void
.end method

.method public removeAll(Ljava/lang/String;)V
    .locals 2

    .line 40
    iget-object v0, p0, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;->mSingleThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhost/exp/exponent/notifications/managers/-$$Lambda$SchedulersManagerProxy$TpEzYb_osYUaKmj5Tdn3S5NbEgI;

    invoke-direct {v1, p0, p1}, Lhost/exp/exponent/notifications/managers/-$$Lambda$SchedulersManagerProxy$TpEzYb_osYUaKmj5Tdn3S5NbEgI;-><init>(Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public removeScheduler(Ljava/lang/String;)V
    .locals 2

    .line 55
    iget-object v0, p0, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;->mSingleThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhost/exp/exponent/notifications/managers/-$$Lambda$SchedulersManagerProxy$87AlR5QjXPuxVwucHHb5YyRbl6I;

    invoke-direct {v1, p0, p1}, Lhost/exp/exponent/notifications/managers/-$$Lambda$SchedulersManagerProxy$87AlR5QjXPuxVwucHHb5YyRbl6I;-><init>(Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public rescheduleOrDelete(Ljava/lang/String;)V
    .locals 2

    .line 50
    iget-object v0, p0, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;->mSingleThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhost/exp/exponent/notifications/managers/-$$Lambda$SchedulersManagerProxy$13-vzlgGqUZMZn9HPZwZhVB4KDY;

    invoke-direct {v1, p0, p1}, Lhost/exp/exponent/notifications/managers/-$$Lambda$SchedulersManagerProxy$13-vzlgGqUZMZn9HPZwZhVB4KDY;-><init>(Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public triggerAll(Ljava/lang/String;)V
    .locals 2

    .line 35
    iget-object v0, p0, Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;->mSingleThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhost/exp/exponent/notifications/managers/-$$Lambda$SchedulersManagerProxy$AuypDvCV4ZRJHq4zqbWPDtkTvqQ;

    invoke-direct {v1, p0, p1}, Lhost/exp/exponent/notifications/managers/-$$Lambda$SchedulersManagerProxy$AuypDvCV4ZRJHq4zqbWPDtkTvqQ;-><init>(Lhost/exp/exponent/notifications/managers/SchedulersManagerProxy;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
