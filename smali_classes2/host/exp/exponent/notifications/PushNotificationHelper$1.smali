.class Lhost/exp/exponent/notifications/PushNotificationHelper$1;
.super Ljava/lang/Object;
.source "PushNotificationHelper.java"

# interfaces
.implements Lhost/exp/exponent/storage/ExponentDB$ExperienceResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/notifications/PushNotificationHelper;->onMessageReceived(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/notifications/PushNotificationHelper;

.field final synthetic val$body:Ljava/lang/String;

.field final synthetic val$categoryId:Ljava/lang/String;

.field final synthetic val$channelId:Ljava/lang/String;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$experienceId:Ljava/lang/String;

.field final synthetic val$message:Ljava/lang/String;

.field final synthetic val$title:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhost/exp/exponent/notifications/PushNotificationHelper;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->this$0:Lhost/exp/exponent/notifications/PushNotificationHelper;

    iput-object p2, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->val$message:Ljava/lang/String;

    iput-object p4, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->val$experienceId:Ljava/lang/String;

    iput-object p5, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->val$channelId:Ljava/lang/String;

    iput-object p6, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->val$body:Ljava/lang/String;

    iput-object p7, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->val$title:Ljava/lang/String;

    iput-object p8, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->val$categoryId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure()V
    .locals 3

    .line 77
    invoke-static {}, Lhost/exp/exponent/notifications/PushNotificationHelper;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No experience found for id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->val$experienceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onSuccess(Lhost/exp/exponent/storage/ExperienceDBObject;)V
    .locals 10

    .line 68
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    iget-object v0, p1, Lhost/exp/exponent/storage/ExperienceDBObject;->manifest:Ljava/lang/String;

    invoke-direct {v6, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->this$0:Lhost/exp/exponent/notifications/PushNotificationHelper;

    iget-object v1, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->val$message:Ljava/lang/String;

    iget-object v3, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->val$experienceId:Ljava/lang/String;

    iget-object v4, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->val$channelId:Ljava/lang/String;

    iget-object v5, p1, Lhost/exp/exponent/storage/ExperienceDBObject;->manifestUrl:Ljava/lang/String;

    iget-object v7, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->val$body:Ljava/lang/String;

    iget-object v8, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->val$title:Ljava/lang/String;

    iget-object v9, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->val$categoryId:Ljava/lang/String;

    invoke-static/range {v0 .. v9}, Lhost/exp/exponent/notifications/PushNotificationHelper;->access$000(Lhost/exp/exponent/notifications/PushNotificationHelper;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 71
    :catch_0
    invoke-static {}, Lhost/exp/exponent/notifications/PushNotificationHelper;->access$100()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Couldn\'t deserialize JSON for experience id "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$1;->val$experienceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method
