.class Lhost/exp/exponent/notifications/PushNotificationHelper$2$1$1;
.super Ljava/lang/Object;
.source "PushNotificationHelper.java"

# interfaces
.implements Lhost/exp/exponent/notifications/IntentProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;


# direct methods
.method constructor <init>(Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;)V
    .locals 0

    .line 229
    iput-object p1, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1$1;->this$2:Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public provide()Landroid/content/Intent;
    .locals 3

    .line 232
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1$1;->this$2:Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;

    iget-object v1, v1, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->this$1:Lhost/exp/exponent/notifications/PushNotificationHelper$2;

    iget-object v1, v1, Lhost/exp/exponent/notifications/PushNotificationHelper$2;->val$context:Landroid/content/Context;

    sget-object v2, Lhost/exp/exponent/kernel/KernelConstants;->MAIN_ACTIVITY_CLASS:Ljava/lang/Class;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 233
    iget-object v1, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1$1;->this$2:Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;

    iget-object v1, v1, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->this$1:Lhost/exp/exponent/notifications/PushNotificationHelper$2;

    iget-object v1, v1, Lhost/exp/exponent/notifications/PushNotificationHelper$2;->val$manifestUrl:Ljava/lang/String;

    const-string v2, "notificationExperienceUrl"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 234
    iget-object v1, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1$1;->this$2:Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;

    iget-object v1, v1, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->this$1:Lhost/exp/exponent/notifications/PushNotificationHelper$2;

    iget-object v1, v1, Lhost/exp/exponent/notifications/PushNotificationHelper$2;->val$body:Ljava/lang/String;

    const-string v2, "notification"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 235
    iget-object v1, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1$1;->this$2:Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;

    iget-object v1, v1, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->val$notificationEvent:Lhost/exp/exponent/notifications/ReceivedNotificationEvent;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lhost/exp/exponent/notifications/ReceivedNotificationEvent;->toJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "notification_object"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method
