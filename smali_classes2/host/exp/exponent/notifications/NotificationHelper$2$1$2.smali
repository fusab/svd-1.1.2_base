.class Lhost/exp/exponent/notifications/NotificationHelper$2$1$2;
.super Ljava/lang/Object;
.source "NotificationHelper.java"

# interfaces
.implements Lhost/exp/exponent/ExponentManifest$BitmapListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/notifications/NotificationHelper$2$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lhost/exp/exponent/notifications/NotificationHelper$2$1;

.field final synthetic val$notificationEvent:Lhost/exp/exponent/notifications/ReceivedNotificationEvent;


# direct methods
.method constructor <init>(Lhost/exp/exponent/notifications/NotificationHelper$2$1;Lhost/exp/exponent/notifications/ReceivedNotificationEvent;)V
    .locals 0

    .line 529
    iput-object p1, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$2;->this$1:Lhost/exp/exponent/notifications/NotificationHelper$2$1;

    iput-object p2, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$2;->val$notificationEvent:Lhost/exp/exponent/notifications/ReceivedNotificationEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadBitmap(Landroid/graphics/Bitmap;)V
    .locals 3

    .line 532
    iget-object v0, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$2;->this$1:Lhost/exp/exponent/notifications/NotificationHelper$2$1;

    iget-object v0, v0, Lhost/exp/exponent/notifications/NotificationHelper$2$1;->this$0:Lhost/exp/exponent/notifications/NotificationHelper$2;

    iget-object v0, v0, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$data:Ljava/util/HashMap;

    const-string v1, "icon"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 533
    iget-object v0, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$2;->this$1:Lhost/exp/exponent/notifications/NotificationHelper$2$1;

    iget-object v0, v0, Lhost/exp/exponent/notifications/NotificationHelper$2$1;->this$0:Lhost/exp/exponent/notifications/NotificationHelper$2;

    iget-object v0, v0, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$builder:Landroidx/core/app/NotificationCompat$Builder;

    invoke-virtual {v0, p1}, Landroidx/core/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroidx/core/app/NotificationCompat$Builder;

    .line 535
    :cond_0
    iget-object p1, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$2;->this$1:Lhost/exp/exponent/notifications/NotificationHelper$2$1;

    iget-object p1, p1, Lhost/exp/exponent/notifications/NotificationHelper$2$1;->this$0:Lhost/exp/exponent/notifications/NotificationHelper$2;

    iget-object p1, p1, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$manager:Lhost/exp/exponent/notifications/ExponentNotificationManager;

    iget-object v0, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$2;->this$1:Lhost/exp/exponent/notifications/NotificationHelper$2$1;

    iget-object v0, v0, Lhost/exp/exponent/notifications/NotificationHelper$2$1;->this$0:Lhost/exp/exponent/notifications/NotificationHelper$2;

    iget-object v0, v0, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$experienceId:Ljava/lang/String;

    iget-object v1, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$2;->this$1:Lhost/exp/exponent/notifications/NotificationHelper$2$1;

    iget-object v1, v1, Lhost/exp/exponent/notifications/NotificationHelper$2$1;->this$0:Lhost/exp/exponent/notifications/NotificationHelper$2;

    iget v1, v1, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$id:I

    iget-object v2, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$2;->this$1:Lhost/exp/exponent/notifications/NotificationHelper$2$1;

    iget-object v2, v2, Lhost/exp/exponent/notifications/NotificationHelper$2$1;->this$0:Lhost/exp/exponent/notifications/NotificationHelper$2;

    iget-object v2, v2, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$builder:Landroidx/core/app/NotificationCompat$Builder;

    invoke-virtual {v2}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lhost/exp/exponent/notifications/ExponentNotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 536
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object p1

    iget-object v0, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$2;->val$notificationEvent:Lhost/exp/exponent/notifications/ReceivedNotificationEvent;

    invoke-virtual {p1, v0}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 537
    iget-object p1, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$2;->this$1:Lhost/exp/exponent/notifications/NotificationHelper$2$1;

    iget-object p1, p1, Lhost/exp/exponent/notifications/NotificationHelper$2$1;->this$0:Lhost/exp/exponent/notifications/NotificationHelper$2;

    iget-object p1, p1, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$listener:Lhost/exp/exponent/notifications/NotificationHelper$Listener;

    iget-object v0, p0, Lhost/exp/exponent/notifications/NotificationHelper$2$1$2;->this$1:Lhost/exp/exponent/notifications/NotificationHelper$2$1;

    iget-object v0, v0, Lhost/exp/exponent/notifications/NotificationHelper$2$1;->this$0:Lhost/exp/exponent/notifications/NotificationHelper$2;

    iget v0, v0, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$id:I

    invoke-interface {p1, v0}, Lhost/exp/exponent/notifications/NotificationHelper$Listener;->onSuccess(I)V

    return-void
.end method
