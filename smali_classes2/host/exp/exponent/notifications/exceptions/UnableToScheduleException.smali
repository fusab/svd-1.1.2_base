.class public Lhost/exp/exponent/notifications/exceptions/UnableToScheduleException;
.super Lorg/unimodules/core/errors/CodedException;
.source "UnableToScheduleException.java"


# static fields
.field static final message:Ljava/lang/String; = "Probably there won\'t be any time in the future when notification can be scheduled"


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "Probably there won\'t be any time in the future when notification can be scheduled"

    .line 10
    invoke-direct {p0, v0}, Lorg/unimodules/core/errors/CodedException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1}, Lorg/unimodules/core/errors/CodedException;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getCode()Ljava/lang/String;
    .locals 1

    const-string v0, "E_NOTIFICATION_CANNOT_BE_SCHEDULED"

    return-object v0
.end method
