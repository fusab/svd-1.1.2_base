.class Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;
.super Ljava/lang/Object;
.source "PushNotificationHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/notifications/PushNotificationHelper$2;->onLoadBitmap(Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lhost/exp/exponent/notifications/PushNotificationHelper$2;

.field final synthetic val$bitmap:Landroid/graphics/Bitmap;

.field final synthetic val$notificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

.field final synthetic val$notificationEvent:Lhost/exp/exponent/notifications/ReceivedNotificationEvent;

.field final synthetic val$notificationId:I


# direct methods
.method constructor <init>(Lhost/exp/exponent/notifications/PushNotificationHelper$2;Landroidx/core/app/NotificationCompat$Builder;Lhost/exp/exponent/notifications/ReceivedNotificationEvent;Landroid/graphics/Bitmap;I)V
    .locals 0

    .line 224
    iput-object p1, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->this$1:Lhost/exp/exponent/notifications/PushNotificationHelper$2;

    iput-object p2, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->val$notificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

    iput-object p3, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->val$notificationEvent:Lhost/exp/exponent/notifications/ReceivedNotificationEvent;

    iput-object p4, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->val$bitmap:Landroid/graphics/Bitmap;

    iput p5, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->val$notificationId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 228
    iget-object v0, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->this$1:Lhost/exp/exponent/notifications/PushNotificationHelper$2;

    iget-object v0, v0, Lhost/exp/exponent/notifications/PushNotificationHelper$2;->val$categoryId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->this$1:Lhost/exp/exponent/notifications/PushNotificationHelper$2;

    iget-object v0, v0, Lhost/exp/exponent/notifications/PushNotificationHelper$2;->val$categoryId:Ljava/lang/String;

    iget-object v1, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->val$notificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

    iget-object v2, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->this$1:Lhost/exp/exponent/notifications/PushNotificationHelper$2;

    iget-object v2, v2, Lhost/exp/exponent/notifications/PushNotificationHelper$2;->val$context:Landroid/content/Context;

    new-instance v3, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1$1;

    invoke-direct {v3, p0}, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1$1;-><init>(Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;)V

    invoke-static {v0, v1, v2, v3}, Lhost/exp/exponent/notifications/NotificationActionCenter;->setCategory(Ljava/lang/String;Landroidx/core/app/NotificationCompat$Builder;Landroid/content/Context;Lhost/exp/exponent/notifications/IntentProvider;)V

    .line 243
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->this$1:Lhost/exp/exponent/notifications/PushNotificationHelper$2;

    iget-object v0, v0, Lhost/exp/exponent/notifications/PushNotificationHelper$2;->val$manifestUrl:Ljava/lang/String;

    sget-object v1, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 244
    iget-object v0, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->val$notificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

    iget-object v1, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    goto :goto_0

    .line 247
    :cond_1
    iget-object v0, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->val$notificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

    invoke-virtual {v0}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 251
    :goto_0
    iget-object v1, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->this$1:Lhost/exp/exponent/notifications/PushNotificationHelper$2;

    iget-object v1, v1, Lhost/exp/exponent/notifications/PushNotificationHelper$2;->val$manager:Lhost/exp/exponent/notifications/ExponentNotificationManager;

    iget-object v2, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->this$1:Lhost/exp/exponent/notifications/PushNotificationHelper$2;

    iget-object v2, v2, Lhost/exp/exponent/notifications/PushNotificationHelper$2;->val$experienceId:Ljava/lang/String;

    iget v3, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->val$notificationId:I

    invoke-virtual {v1, v2, v3, v0}, Lhost/exp/exponent/notifications/ExponentNotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 254
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    iget-object v1, p0, Lhost/exp/exponent/notifications/PushNotificationHelper$2$1;->val$notificationEvent:Lhost/exp/exponent/notifications/ReceivedNotificationEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    return-void
.end method
