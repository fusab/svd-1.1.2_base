.class public Lhost/exp/exponent/notifications/NotificationConstants;
.super Ljava/lang/Object;
.source "NotificationConstants.java"


# static fields
.field public static final MAX_COLLAPSED_NOTIFICATIONS:I = 0x5

.field public static final NOTIFICATION_ACTION_TYPE:Ljava/lang/String; = "actionId"

.field public static final NOTIFICATION_CHANNEL_BADGE:Ljava/lang/String; = "badge"

.field public static final NOTIFICATION_CHANNEL_DESCRIPTION:Ljava/lang/String; = "description"

.field public static final NOTIFICATION_CHANNEL_NAME:Ljava/lang/String; = "name"

.field public static final NOTIFICATION_CHANNEL_PRIORITY:Ljava/lang/String; = "priority"

.field public static final NOTIFICATION_CHANNEL_PRIORITY_HIGH:Ljava/lang/String; = "high"

.field public static final NOTIFICATION_CHANNEL_PRIORITY_LOW:Ljava/lang/String; = "low"

.field public static final NOTIFICATION_CHANNEL_PRIORITY_MAX:Ljava/lang/String; = "max"

.field public static final NOTIFICATION_CHANNEL_PRIORITY_MIN:Ljava/lang/String; = "min"

.field public static final NOTIFICATION_CHANNEL_SOUND:Ljava/lang/String; = "sound"

.field public static final NOTIFICATION_CHANNEL_VIBRATE:Ljava/lang/String; = "vibrate"

.field public static final NOTIFICATION_COLLAPSE_MODE:Ljava/lang/String; = "collapse"

.field public static final NOTIFICATION_DATA_KEY:Ljava/lang/String; = "data"

.field public static final NOTIFICATION_DEFAULT_CHANNEL_ID:Ljava/lang/String; = "expo-default"

.field public static final NOTIFICATION_EXPERIENCE_CHANNEL_GROUP_ID:Ljava/lang/String; = "expo-experience-group"

.field public static final NOTIFICATION_EXPERIENCE_CHANNEL_ID:Ljava/lang/String; = "expo-experience"

.field public static final NOTIFICATION_EXPERIENCE_ID_KEY:Ljava/lang/String; = "experienceId"

.field public static final NOTIFICATION_ID_KEY:Ljava/lang/String; = "notificationId"

.field public static final NOTIFICATION_INPUT_TEXT:Ljava/lang/Object;

.field public static final NOTIFICATION_IS_MULTIPLE_KEY:Ljava/lang/String; = "isMultiple"

.field public static final NOTIFICATION_MESSAGE_KEY:Ljava/lang/String; = "message"

.field public static final NOTIFICATION_ORIGIN_KEY:Ljava/lang/String; = "origin"

.field public static final NOTIFICATION_REMOTE_KEY:Ljava/lang/String; = "remote"

.field public static final NOTIFICATION_UNREAD_COUNT_KEY:Ljava/lang/String; = "#{unread_notifications}"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "userText"

    .line 17
    sput-object v0, Lhost/exp/exponent/notifications/NotificationConstants;->NOTIFICATION_INPUT_TEXT:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
