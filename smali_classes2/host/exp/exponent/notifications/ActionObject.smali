.class public Lhost/exp/exponent/notifications/ActionObject;
.super Lcom/raizlabs/android/dbflow/structure/BaseModel;
.source "ActionObject.java"


# instance fields
.field private actionId:Ljava/lang/String;

.field private buttonTitle:Ljava/lang/String;

.field private categoryId:Ljava/lang/String;

.field private isAuthenticationRequired:Ljava/lang/Boolean;

.field private isDestructive:Ljava/lang/Boolean;

.field private placeholder:Ljava/lang/String;

.field private position:I

.field private shouldShowTextInput:Ljava/lang/Boolean;

.field private submitButtonTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 40
    invoke-direct {p0}, Lcom/raizlabs/android/dbflow/structure/BaseModel;-><init>()V

    const/4 v0, 0x0

    .line 41
    iput v0, p0, Lhost/exp/exponent/notifications/ActionObject;->position:I

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;I)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Lcom/raizlabs/android/dbflow/structure/BaseModel;-><init>()V

    const-string v0, "categoryId"

    .line 45
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lhost/exp/exponent/notifications/ActionObject;->categoryId:Ljava/lang/String;

    const-string v0, "actionId"

    .line 46
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lhost/exp/exponent/notifications/ActionObject;->actionId:Ljava/lang/String;

    const-string v0, "buttonTitle"

    .line 47
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lhost/exp/exponent/notifications/ActionObject;->buttonTitle:Ljava/lang/String;

    const-string v0, "isDestructive"

    .line 48
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lhost/exp/exponent/notifications/ActionObject;->isDestructive:Ljava/lang/Boolean;

    const-string v0, "isAuthenticationRequired"

    .line 49
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lhost/exp/exponent/notifications/ActionObject;->isAuthenticationRequired:Ljava/lang/Boolean;

    const-string v0, "textInput"

    .line 50
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lhost/exp/exponent/notifications/ActionObject;->shouldShowTextInput:Ljava/lang/Boolean;

    .line 51
    iget-object v1, p0, Lhost/exp/exponent/notifications/ActionObject;->shouldShowTextInput:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/util/Map;

    if-eqz v1, :cond_1

    .line 52
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    const-string v0, "placeholder"

    .line 53
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lhost/exp/exponent/notifications/ActionObject;->placeholder:Ljava/lang/String;

    const-string v0, "submitButtonTitle"

    .line 54
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lhost/exp/exponent/notifications/ActionObject;->submitButtonTitle:Ljava/lang/String;

    .line 56
    :cond_1
    iput p2, p0, Lhost/exp/exponent/notifications/ActionObject;->position:I

    return-void
.end method


# virtual methods
.method public getActionId()Ljava/lang/String;
    .locals 1

    .line 60
    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject;->actionId:Ljava/lang/String;

    return-object v0
.end method

.method public getButtonTitle()Ljava/lang/String;
    .locals 1

    .line 64
    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject;->buttonTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryId()Ljava/lang/String;
    .locals 1

    .line 96
    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject;->categoryId:Ljava/lang/String;

    return-object v0
.end method

.method public getPlaceholder()Ljava/lang/String;
    .locals 1

    .line 124
    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject;->placeholder:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .line 72
    iget v0, p0, Lhost/exp/exponent/notifications/ActionObject;->position:I

    return v0
.end method

.method public getSubmitButtonTitle()Ljava/lang/String;
    .locals 1

    .line 88
    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject;->submitButtonTitle:Ljava/lang/String;

    return-object v0
.end method

.method public isAuthenticationRequired()Ljava/lang/Boolean;
    .locals 1

    .line 116
    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject;->isAuthenticationRequired:Ljava/lang/Boolean;

    return-object v0
.end method

.method public isDestructive()Ljava/lang/Boolean;
    .locals 1

    .line 108
    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject;->isDestructive:Ljava/lang/Boolean;

    return-object v0
.end method

.method public isShouldShowTextInput()Ljava/lang/Boolean;
    .locals 1

    .line 68
    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject;->shouldShowTextInput:Ljava/lang/Boolean;

    return-object v0
.end method

.method public setActionId(Ljava/lang/String;)V
    .locals 0

    .line 84
    iput-object p1, p0, Lhost/exp/exponent/notifications/ActionObject;->actionId:Ljava/lang/String;

    return-void
.end method

.method public setAuthenticationRequired(Ljava/lang/Boolean;)V
    .locals 0

    .line 120
    iput-object p1, p0, Lhost/exp/exponent/notifications/ActionObject;->isAuthenticationRequired:Ljava/lang/Boolean;

    return-void
.end method

.method public setButtonTitle(Ljava/lang/String;)V
    .locals 0

    .line 104
    iput-object p1, p0, Lhost/exp/exponent/notifications/ActionObject;->buttonTitle:Ljava/lang/String;

    return-void
.end method

.method public setCategoryId(Ljava/lang/String;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lhost/exp/exponent/notifications/ActionObject;->categoryId:Ljava/lang/String;

    return-void
.end method

.method public setDestructive(Ljava/lang/Boolean;)V
    .locals 0

    .line 112
    iput-object p1, p0, Lhost/exp/exponent/notifications/ActionObject;->isDestructive:Ljava/lang/Boolean;

    return-void
.end method

.method public setPlaceholder(Ljava/lang/String;)V
    .locals 0

    .line 128
    iput-object p1, p0, Lhost/exp/exponent/notifications/ActionObject;->placeholder:Ljava/lang/String;

    return-void
.end method

.method public setPosition(I)V
    .locals 0

    .line 76
    iput p1, p0, Lhost/exp/exponent/notifications/ActionObject;->position:I

    return-void
.end method

.method public setShouldShowTextInput(Ljava/lang/Boolean;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lhost/exp/exponent/notifications/ActionObject;->shouldShowTextInput:Ljava/lang/Boolean;

    return-void
.end method

.method public setSubmitButtonTitle(Ljava/lang/String;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lhost/exp/exponent/notifications/ActionObject;->submitButtonTitle:Ljava/lang/String;

    return-void
.end method
