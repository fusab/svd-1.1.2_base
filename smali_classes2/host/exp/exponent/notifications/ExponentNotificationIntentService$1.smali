.class Lhost/exp/exponent/notifications/ExponentNotificationIntentService$1;
.super Ljava/lang/Object;
.source "ExponentNotificationIntentService.java"

# interfaces
.implements Lhost/exp/exponent/network/ExpoHttpCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/notifications/ExponentNotificationIntentService;->onHandleIntent(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/notifications/ExponentNotificationIntentService;

.field final synthetic val$token:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhost/exp/exponent/notifications/ExponentNotificationIntentService;Ljava/lang/String;)V
    .locals 0

    .line 111
    iput-object p1, p0, Lhost/exp/exponent/notifications/ExponentNotificationIntentService$1;->this$0:Lhost/exp/exponent/notifications/ExponentNotificationIntentService;

    iput-object p2, p0, Lhost/exp/exponent/notifications/ExponentNotificationIntentService$1;->val$token:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/io/IOException;)V
    .locals 1

    .line 114
    iget-object v0, p0, Lhost/exp/exponent/notifications/ExponentNotificationIntentService$1;->this$0:Lhost/exp/exponent/notifications/ExponentNotificationIntentService;

    invoke-static {v0, p1}, Lhost/exp/exponent/notifications/ExponentNotificationIntentService;->access$000(Lhost/exp/exponent/notifications/ExponentNotificationIntentService;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lhost/exp/exponent/network/ExpoResponse;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 119
    invoke-interface {p1}, Lhost/exp/exponent/network/ExpoResponse;->isSuccessful()Z

    move-result p1

    if-nez p1, :cond_0

    .line 120
    iget-object p1, p0, Lhost/exp/exponent/notifications/ExponentNotificationIntentService$1;->this$0:Lhost/exp/exponent/notifications/ExponentNotificationIntentService;

    const-string v0, "Failed to update the native device token with the Expo push notification service"

    invoke-static {p1, v0}, Lhost/exp/exponent/notifications/ExponentNotificationIntentService;->access$100(Lhost/exp/exponent/notifications/ExponentNotificationIntentService;Ljava/lang/String;)V

    return-void

    .line 123
    :cond_0
    iget-object p1, p0, Lhost/exp/exponent/notifications/ExponentNotificationIntentService$1;->this$0:Lhost/exp/exponent/notifications/ExponentNotificationIntentService;

    iget-object p1, p1, Lhost/exp/exponent/notifications/ExponentNotificationIntentService;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    iget-object v0, p0, Lhost/exp/exponent/notifications/ExponentNotificationIntentService$1;->this$0:Lhost/exp/exponent/notifications/ExponentNotificationIntentService;

    invoke-virtual {v0}, Lhost/exp/exponent/notifications/ExponentNotificationIntentService;->getSharedPrefsKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lhost/exp/exponent/notifications/ExponentNotificationIntentService$1;->val$token:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 124
    invoke-static {p1}, Lhost/exp/exponent/notifications/ExponentNotificationIntentService;->access$202(Z)Z

    const-string p1, "devicePushToken"

    .line 125
    invoke-static {p1}, Lhost/exp/exponent/utils/AsyncCondition;->notify(Ljava/lang/String;)V

    return-void
.end method
