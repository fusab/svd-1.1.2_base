.class final Lhost/exp/exponent/notifications/NotificationHelper$2;
.super Ljava/lang/Object;
.source "NotificationHelper.java"

# interfaces
.implements Lhost/exp/exponent/storage/ExponentDB$ExperienceResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/notifications/NotificationHelper;->showNotification(Landroid/content/Context;ILjava/util/HashMap;Lhost/exp/exponent/ExponentManifest;Lhost/exp/exponent/notifications/NotificationHelper$Listener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$builder:Landroidx/core/app/NotificationCompat$Builder;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$data:Ljava/util/HashMap;

.field final synthetic val$experienceId:Ljava/lang/String;

.field final synthetic val$exponentManifest:Lhost/exp/exponent/ExponentManifest;

.field final synthetic val$id:I

.field final synthetic val$listener:Lhost/exp/exponent/notifications/NotificationHelper$Listener;

.field final synthetic val$manager:Lhost/exp/exponent/notifications/ExponentNotificationManager;


# direct methods
.method constructor <init>(Ljava/util/HashMap;Landroid/content/Context;Ljava/lang/String;ILandroidx/core/app/NotificationCompat$Builder;Lhost/exp/exponent/ExponentManifest;Lhost/exp/exponent/notifications/ExponentNotificationManager;Lhost/exp/exponent/notifications/NotificationHelper$Listener;)V
    .locals 0

    .line 473
    iput-object p1, p0, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$data:Ljava/util/HashMap;

    iput-object p2, p0, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$experienceId:Ljava/lang/String;

    iput p4, p0, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$id:I

    iput-object p5, p0, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$builder:Landroidx/core/app/NotificationCompat$Builder;

    iput-object p6, p0, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$exponentManifest:Lhost/exp/exponent/ExponentManifest;

    iput-object p7, p0, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$manager:Lhost/exp/exponent/notifications/ExponentNotificationManager;

    iput-object p8, p0, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$listener:Lhost/exp/exponent/notifications/NotificationHelper$Listener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure()V
    .locals 4

    .line 549
    iget-object v0, p0, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$listener:Lhost/exp/exponent/notifications/NotificationHelper$Listener;

    new-instance v1, Ljava/lang/Exception;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No experience found for id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lhost/exp/exponent/notifications/NotificationHelper$2;->val$experienceId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lhost/exp/exponent/notifications/NotificationHelper$Listener;->onFailure(Ljava/lang/Exception;)V

    return-void
.end method

.method public onSuccess(Lhost/exp/exponent/storage/ExperienceDBObject;)V
    .locals 2

    .line 476
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lhost/exp/exponent/notifications/NotificationHelper$2$1;

    invoke-direct {v1, p0, p1}, Lhost/exp/exponent/notifications/NotificationHelper$2$1;-><init>(Lhost/exp/exponent/notifications/NotificationHelper$2;Lhost/exp/exponent/storage/ExperienceDBObject;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 544
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method
