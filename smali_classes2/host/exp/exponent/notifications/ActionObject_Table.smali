.class public final Lhost/exp/exponent/notifications/ActionObject_Table;
.super Lcom/raizlabs/android/dbflow/structure/ModelAdapter;
.source "ActionObject_Table.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/raizlabs/android/dbflow/structure/ModelAdapter<",
        "Lhost/exp/exponent/notifications/ActionObject;",
        ">;"
    }
.end annotation


# static fields
.field public static final ALL_COLUMN_PROPERTIES:[Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;

.field public static final actionId:Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/Property<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final buttonTitle:Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/Property<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final categoryId:Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/Property<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final isAuthenticationRequired:Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final isDestructive:Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final placeholder:Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/Property<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final position:Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/Property<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final shouldShowTextInput:Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final submitButtonTitle:Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/raizlabs/android/dbflow/sql/language/property/Property<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 32
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const-class v1, Lhost/exp/exponent/notifications/ActionObject;

    const-string v2, "categoryId"

    invoke-direct {v0, v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lhost/exp/exponent/notifications/ActionObject_Table;->categoryId:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    .line 36
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const-class v1, Lhost/exp/exponent/notifications/ActionObject;

    const-string v2, "actionId"

    invoke-direct {v0, v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lhost/exp/exponent/notifications/ActionObject_Table;->actionId:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    .line 38
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const-class v1, Lhost/exp/exponent/notifications/ActionObject;

    const-string v2, "buttonTitle"

    invoke-direct {v0, v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lhost/exp/exponent/notifications/ActionObject_Table;->buttonTitle:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    .line 40
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;

    const-class v1, Lhost/exp/exponent/notifications/ActionObject;

    new-instance v2, Lhost/exp/exponent/notifications/ActionObject_Table$1;

    invoke-direct {v2}, Lhost/exp/exponent/notifications/ActionObject_Table$1;-><init>()V

    const/4 v3, 0x1

    const-string v4, "isDestructive"

    invoke-direct {v0, v1, v4, v3, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;-><init>(Ljava/lang/Class;Ljava/lang/String;ZLcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty$TypeConverterGetter;)V

    sput-object v0, Lhost/exp/exponent/notifications/ActionObject_Table;->isDestructive:Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;

    .line 49
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;

    const-class v1, Lhost/exp/exponent/notifications/ActionObject;

    new-instance v2, Lhost/exp/exponent/notifications/ActionObject_Table$2;

    invoke-direct {v2}, Lhost/exp/exponent/notifications/ActionObject_Table$2;-><init>()V

    const-string v4, "isAuthenticationRequired"

    invoke-direct {v0, v1, v4, v3, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;-><init>(Ljava/lang/Class;Ljava/lang/String;ZLcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty$TypeConverterGetter;)V

    sput-object v0, Lhost/exp/exponent/notifications/ActionObject_Table;->isAuthenticationRequired:Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;

    .line 58
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const-class v1, Lhost/exp/exponent/notifications/ActionObject;

    const-string v2, "submitButtonTitle"

    invoke-direct {v0, v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lhost/exp/exponent/notifications/ActionObject_Table;->submitButtonTitle:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    .line 60
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const-class v1, Lhost/exp/exponent/notifications/ActionObject;

    const-string v2, "placeholder"

    invoke-direct {v0, v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lhost/exp/exponent/notifications/ActionObject_Table;->placeholder:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    .line 62
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;

    const-class v1, Lhost/exp/exponent/notifications/ActionObject;

    new-instance v2, Lhost/exp/exponent/notifications/ActionObject_Table$3;

    invoke-direct {v2}, Lhost/exp/exponent/notifications/ActionObject_Table$3;-><init>()V

    const-string v4, "shouldShowTextInput"

    invoke-direct {v0, v1, v4, v3, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;-><init>(Ljava/lang/Class;Ljava/lang/String;ZLcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty$TypeConverterGetter;)V

    sput-object v0, Lhost/exp/exponent/notifications/ActionObject_Table;->shouldShowTextInput:Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;

    .line 71
    new-instance v0, Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const-class v1, Lhost/exp/exponent/notifications/ActionObject;

    const-string v2, "position"

    invoke-direct {v0, v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lhost/exp/exponent/notifications/ActionObject_Table;->position:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/16 v0, 0x9

    .line 73
    new-array v0, v0, [Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;

    sget-object v1, Lhost/exp/exponent/notifications/ActionObject_Table;->categoryId:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lhost/exp/exponent/notifications/ActionObject_Table;->actionId:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    aput-object v1, v0, v3

    sget-object v1, Lhost/exp/exponent/notifications/ActionObject_Table;->buttonTitle:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lhost/exp/exponent/notifications/ActionObject_Table;->isDestructive:Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lhost/exp/exponent/notifications/ActionObject_Table;->isAuthenticationRequired:Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lhost/exp/exponent/notifications/ActionObject_Table;->submitButtonTitle:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lhost/exp/exponent/notifications/ActionObject_Table;->placeholder:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lhost/exp/exponent/notifications/ActionObject_Table;->shouldShowTextInput:Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lhost/exp/exponent/notifications/ActionObject_Table;->position:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sput-object v0, Lhost/exp/exponent/notifications/ActionObject_Table;->ALL_COLUMN_PROPERTIES:[Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;

    return-void
.end method

.method public constructor <init>(Lcom/raizlabs/android/dbflow/config/DatabaseHolder;Lcom/raizlabs/android/dbflow/config/DatabaseDefinition;)V
    .locals 0

    .line 78
    invoke-direct {p0, p2}, Lcom/raizlabs/android/dbflow/structure/ModelAdapter;-><init>(Lcom/raizlabs/android/dbflow/config/DatabaseDefinition;)V

    .line 79
    const-class p2, Ljava/lang/Boolean;

    invoke-virtual {p1, p2}, Lcom/raizlabs/android/dbflow/config/DatabaseHolder;->getTypeConverterForClass(Ljava/lang/Class;)Lcom/raizlabs/android/dbflow/converter/TypeConverter;

    move-result-object p1

    check-cast p1, Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    iput-object p1, p0, Lhost/exp/exponent/notifications/ActionObject_Table;->global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    return-void
.end method

.method static synthetic access$000(Lhost/exp/exponent/notifications/ActionObject_Table;)Lcom/raizlabs/android/dbflow/converter/BooleanConverter;
    .locals 0

    .line 31
    iget-object p0, p0, Lhost/exp/exponent/notifications/ActionObject_Table;->global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    return-object p0
.end method


# virtual methods
.method public final bindToDeleteStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/notifications/ActionObject;)V
    .locals 1

    .line 191
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getActionId()Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x1

    invoke-interface {p1, v0, p2}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    return-void
.end method

.method public bridge synthetic bindToDeleteStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Ljava/lang/Object;)V
    .locals 0

    .line 30
    check-cast p2, Lhost/exp/exponent/notifications/ActionObject;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/notifications/ActionObject_Table;->bindToDeleteStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/notifications/ActionObject;)V

    return-void
.end method

.method public final bindToInsertStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/notifications/ActionObject;I)V
    .locals 3

    add-int/lit8 v0, p3, 0x1

    .line 158
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getCategoryId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    add-int/lit8 v0, p3, 0x2

    .line 159
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getActionId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    add-int/lit8 v0, p3, 0x3

    .line 160
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getButtonTitle()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    .line 161
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isDestructive()Ljava/lang/Boolean;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject_Table;->global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isDestructive()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/raizlabs/android/dbflow/converter/BooleanConverter;->getDBValue(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    add-int/lit8 v2, p3, 0x4

    .line 162
    invoke-interface {p1, v2, v0}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindNumberOrNull(ILjava/lang/Number;)V

    .line 163
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isAuthenticationRequired()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject_Table;->global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isAuthenticationRequired()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/raizlabs/android/dbflow/converter/BooleanConverter;->getDBValue(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_1
    add-int/lit8 v2, p3, 0x5

    .line 164
    invoke-interface {p1, v2, v0}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindNumberOrNull(ILjava/lang/Number;)V

    add-int/lit8 v0, p3, 0x6

    .line 165
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getSubmitButtonTitle()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    add-int/lit8 v0, p3, 0x7

    .line 166
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getPlaceholder()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    .line 167
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isShouldShowTextInput()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject_Table;->global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isShouldShowTextInput()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/raizlabs/android/dbflow/converter/BooleanConverter;->getDBValue(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v1

    :cond_2
    add-int/lit8 v0, p3, 0x8

    .line 168
    invoke-interface {p1, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindNumberOrNull(ILjava/lang/Number;)V

    add-int/lit8 p3, p3, 0x9

    .line 169
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getPosition()I

    move-result p2

    int-to-long v0, p2

    invoke-interface {p1, p3, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindLong(IJ)V

    return-void
.end method

.method public bridge synthetic bindToInsertStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Ljava/lang/Object;I)V
    .locals 0

    .line 30
    check-cast p2, Lhost/exp/exponent/notifications/ActionObject;

    invoke-virtual {p0, p1, p2, p3}, Lhost/exp/exponent/notifications/ActionObject_Table;->bindToInsertStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/notifications/ActionObject;I)V

    return-void
.end method

.method public final bindToInsertValues(Landroid/content/ContentValues;Lhost/exp/exponent/notifications/ActionObject;)V
    .locals 3

    .line 141
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getCategoryId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "`categoryId`"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getActionId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "`actionId`"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getButtonTitle()Ljava/lang/String;

    move-result-object v0

    const-string v1, "`buttonTitle`"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isDestructive()Ljava/lang/Boolean;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject_Table;->global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isDestructive()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/raizlabs/android/dbflow/converter/BooleanConverter;->getDBValue(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    const-string v2, "`isDestructive`"

    .line 145
    invoke-virtual {p1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 146
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isAuthenticationRequired()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject_Table;->global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isAuthenticationRequired()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/raizlabs/android/dbflow/converter/BooleanConverter;->getDBValue(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_1
    const-string v2, "`isAuthenticationRequired`"

    .line 147
    invoke-virtual {p1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 148
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getSubmitButtonTitle()Ljava/lang/String;

    move-result-object v0

    const-string v2, "`submitButtonTitle`"

    invoke-virtual {p1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getPlaceholder()Ljava/lang/String;

    move-result-object v0

    const-string v2, "`placeholder`"

    invoke-virtual {p1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isShouldShowTextInput()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject_Table;->global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isShouldShowTextInput()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/raizlabs/android/dbflow/converter/BooleanConverter;->getDBValue(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v1

    :cond_2
    const-string v0, "`shouldShowTextInput`"

    .line 151
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 152
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getPosition()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const-string v0, "`position`"

    invoke-virtual {p1, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-void
.end method

.method public bridge synthetic bindToInsertValues(Landroid/content/ContentValues;Ljava/lang/Object;)V
    .locals 0

    .line 30
    check-cast p2, Lhost/exp/exponent/notifications/ActionObject;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/notifications/ActionObject_Table;->bindToInsertValues(Landroid/content/ContentValues;Lhost/exp/exponent/notifications/ActionObject;)V

    return-void
.end method

.method public final bindToUpdateStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/notifications/ActionObject;)V
    .locals 3

    .line 174
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getCategoryId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p1, v1, v0}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    .line 175
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getActionId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {p1, v1, v0}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    .line 176
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getButtonTitle()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {p1, v1, v0}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    .line 177
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isDestructive()Ljava/lang/Boolean;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject_Table;->global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isDestructive()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/raizlabs/android/dbflow/converter/BooleanConverter;->getDBValue(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    const/4 v2, 0x4

    .line 178
    invoke-interface {p1, v2, v0}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindNumberOrNull(ILjava/lang/Number;)V

    .line 179
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isAuthenticationRequired()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject_Table;->global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isAuthenticationRequired()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/raizlabs/android/dbflow/converter/BooleanConverter;->getDBValue(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_1
    const/4 v2, 0x5

    .line 180
    invoke-interface {p1, v2, v0}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindNumberOrNull(ILjava/lang/Number;)V

    const/4 v0, 0x6

    .line 181
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getSubmitButtonTitle()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    const/4 v0, 0x7

    .line 182
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getPlaceholder()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    .line 183
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isShouldShowTextInput()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject_Table;->global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->isShouldShowTextInput()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/raizlabs/android/dbflow/converter/BooleanConverter;->getDBValue(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v1

    :cond_2
    const/16 v0, 0x8

    .line 184
    invoke-interface {p1, v0, v1}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindNumberOrNull(ILjava/lang/Number;)V

    const/16 v0, 0x9

    .line 185
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getPosition()I

    move-result v1

    int-to-long v1, v1

    invoke-interface {p1, v0, v1, v2}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindLong(IJ)V

    const/16 v0, 0xa

    .line 186
    invoke-virtual {p2}, Lhost/exp/exponent/notifications/ActionObject;->getActionId()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, v0, p2}, Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;->bindStringOrNull(ILjava/lang/String;)V

    return-void
.end method

.method public bridge synthetic bindToUpdateStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Ljava/lang/Object;)V
    .locals 0

    .line 30
    check-cast p2, Lhost/exp/exponent/notifications/ActionObject;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/notifications/ActionObject_Table;->bindToUpdateStatement(Lcom/raizlabs/android/dbflow/structure/database/DatabaseStatement;Lhost/exp/exponent/notifications/ActionObject;)V

    return-void
.end method

.method public final exists(Lhost/exp/exponent/notifications/ActionObject;Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;)Z
    .locals 3

    const/4 v0, 0x0

    .line 244
    new-array v1, v0, [Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;

    invoke-static {v1}, Lcom/raizlabs/android/dbflow/sql/language/SQLite;->selectCountOf([Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;)Lcom/raizlabs/android/dbflow/sql/language/Select;

    move-result-object v1

    const-class v2, Lhost/exp/exponent/notifications/ActionObject;

    .line 245
    invoke-virtual {v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/Select;->from(Ljava/lang/Class;)Lcom/raizlabs/android/dbflow/sql/language/From;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/raizlabs/android/dbflow/sql/language/SQLOperator;

    .line 246
    invoke-virtual {p0, p1}, Lhost/exp/exponent/notifications/ActionObject_Table;->getPrimaryConditionClause(Lhost/exp/exponent/notifications/ActionObject;)Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;

    move-result-object p1

    aput-object p1, v2, v0

    invoke-virtual {v1, v2}, Lcom/raizlabs/android/dbflow/sql/language/From;->where([Lcom/raizlabs/android/dbflow/sql/language/SQLOperator;)Lcom/raizlabs/android/dbflow/sql/language/Where;

    move-result-object p1

    .line 247
    invoke-virtual {p1, p2}, Lcom/raizlabs/android/dbflow/sql/language/Where;->hasData(Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic exists(Ljava/lang/Object;Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;)Z
    .locals 0

    .line 30
    check-cast p1, Lhost/exp/exponent/notifications/ActionObject;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/notifications/ActionObject_Table;->exists(Lhost/exp/exponent/notifications/ActionObject;Lcom/raizlabs/android/dbflow/structure/database/DatabaseWrapper;)Z

    move-result p1

    return p1
.end method

.method public final getAllColumnProperties()[Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;
    .locals 1

    .line 136
    sget-object v0, Lhost/exp/exponent/notifications/ActionObject_Table;->ALL_COLUMN_PROPERTIES:[Lcom/raizlabs/android/dbflow/sql/language/property/IProperty;

    return-object v0
.end method

.method public final getCompiledStatementQuery()Ljava/lang/String;
    .locals 1

    const-string v0, "INSERT INTO `ActionObject`(`categoryId`,`actionId`,`buttonTitle`,`isDestructive`,`isAuthenticationRequired`,`submitButtonTitle`,`placeholder`,`shouldShowTextInput`,`position`) VALUES (?,?,?,?,?,?,?,?,?)"

    return-object v0
.end method

.method public final getCreationQuery()Ljava/lang/String;
    .locals 1

    const-string v0, "CREATE TABLE IF NOT EXISTS `ActionObject`(`categoryId` TEXT, `actionId` TEXT, `buttonTitle` TEXT, `isDestructive` INTEGER, `isAuthenticationRequired` INTEGER, `submitButtonTitle` TEXT, `placeholder` TEXT, `shouldShowTextInput` INTEGER, `position` INTEGER, PRIMARY KEY(`actionId`))"

    return-object v0
.end method

.method public final getDeleteStatementQuery()Ljava/lang/String;
    .locals 1

    const-string v0, "DELETE FROM `ActionObject` WHERE `actionId`=?"

    return-object v0
.end method

.method public final getModelClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "Lhost/exp/exponent/notifications/ActionObject;",
            ">;"
        }
    .end annotation

    .line 84
    const-class v0, Lhost/exp/exponent/notifications/ActionObject;

    return-object v0
.end method

.method public final getPrimaryConditionClause(Lhost/exp/exponent/notifications/ActionObject;)Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;
    .locals 2

    .line 252
    invoke-static {}, Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;->clause()Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;

    move-result-object v0

    .line 253
    sget-object v1, Lhost/exp/exponent/notifications/ActionObject_Table;->actionId:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    invoke-virtual {p1}, Lhost/exp/exponent/notifications/ActionObject;->getActionId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/raizlabs/android/dbflow/sql/language/property/Property;->eq(Ljava/lang/Object;)Lcom/raizlabs/android/dbflow/sql/language/Operator;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;->and(Lcom/raizlabs/android/dbflow/sql/language/SQLOperator;)Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;

    return-object v0
.end method

.method public bridge synthetic getPrimaryConditionClause(Ljava/lang/Object;)Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;
    .locals 0

    .line 30
    check-cast p1, Lhost/exp/exponent/notifications/ActionObject;

    invoke-virtual {p0, p1}, Lhost/exp/exponent/notifications/ActionObject_Table;->getPrimaryConditionClause(Lhost/exp/exponent/notifications/ActionObject;)Lcom/raizlabs/android/dbflow/sql/language/OperatorGroup;

    move-result-object p1

    return-object p1
.end method

.method public final getProperty(Ljava/lang/String;)Lcom/raizlabs/android/dbflow/sql/language/property/Property;
    .locals 1

    .line 99
    invoke-static {p1}, Lcom/raizlabs/android/dbflow/sql/QueryBuilder;->quoteIfNeeded(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 100
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v0, "`isDestructive`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x3

    goto :goto_1

    :sswitch_1
    const-string v0, "`categoryId`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_1

    :sswitch_2
    const-string v0, "`shouldShowTextInput`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x7

    goto :goto_1

    :sswitch_3
    const-string v0, "`position`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 p1, 0x8

    goto :goto_1

    :sswitch_4
    const-string v0, "`submitButtonTitle`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x5

    goto :goto_1

    :sswitch_5
    const-string v0, "`placeholder`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x6

    goto :goto_1

    :sswitch_6
    const-string v0, "`buttonTitle`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    goto :goto_1

    :sswitch_7
    const-string v0, "`actionId`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_1

    :sswitch_8
    const-string v0, "`isAuthenticationRequired`"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x4

    goto :goto_1

    :cond_0
    :goto_0
    const/4 p1, -0x1

    :goto_1
    packed-switch p1, :pswitch_data_0

    .line 129
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Invalid column name passed. Ensure you are calling the correct table\'s column"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 126
    :pswitch_0
    sget-object p1, Lhost/exp/exponent/notifications/ActionObject_Table;->position:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    return-object p1

    .line 123
    :pswitch_1
    sget-object p1, Lhost/exp/exponent/notifications/ActionObject_Table;->shouldShowTextInput:Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;

    return-object p1

    .line 120
    :pswitch_2
    sget-object p1, Lhost/exp/exponent/notifications/ActionObject_Table;->placeholder:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    return-object p1

    .line 117
    :pswitch_3
    sget-object p1, Lhost/exp/exponent/notifications/ActionObject_Table;->submitButtonTitle:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    return-object p1

    .line 114
    :pswitch_4
    sget-object p1, Lhost/exp/exponent/notifications/ActionObject_Table;->isAuthenticationRequired:Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;

    return-object p1

    .line 111
    :pswitch_5
    sget-object p1, Lhost/exp/exponent/notifications/ActionObject_Table;->isDestructive:Lcom/raizlabs/android/dbflow/sql/language/property/TypeConvertedProperty;

    return-object p1

    .line 108
    :pswitch_6
    sget-object p1, Lhost/exp/exponent/notifications/ActionObject_Table;->buttonTitle:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    return-object p1

    .line 105
    :pswitch_7
    sget-object p1, Lhost/exp/exponent/notifications/ActionObject_Table;->actionId:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    return-object p1

    .line 102
    :pswitch_8
    sget-object p1, Lhost/exp/exponent/notifications/ActionObject_Table;->categoryId:Lcom/raizlabs/android/dbflow/sql/language/property/Property;

    return-object p1

    :sswitch_data_0
    .sparse-switch
        -0x5ca76be1 -> :sswitch_8
        -0x58a427d1 -> :sswitch_7
        -0x552800c6 -> :sswitch_6
        -0x3d756653 -> :sswitch_5
        -0x1dd5988e -> :sswitch_4
        0x14af7f7 -> :sswitch_3
        0x30afd933 -> :sswitch_2
        0x3e465c67 -> :sswitch_1
        0x4f8905c8 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getTableName()Ljava/lang/String;
    .locals 1

    const-string v0, "`ActionObject`"

    return-object v0
.end method

.method public final getUpdateStatementQuery()Ljava/lang/String;
    .locals 1

    const-string v0, "UPDATE `ActionObject` SET `categoryId`=?,`actionId`=?,`buttonTitle`=?,`isDestructive`=?,`isAuthenticationRequired`=?,`submitButtonTitle`=?,`placeholder`=?,`shouldShowTextInput`=?,`position`=? WHERE `actionId`=?"

    return-object v0
.end method

.method public final loadFromCursor(Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;Lhost/exp/exponent/notifications/ActionObject;)V
    .locals 4

    const-string v0, "categoryId"

    .line 216
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getStringOrDefault(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lhost/exp/exponent/notifications/ActionObject;->setCategoryId(Ljava/lang/String;)V

    const-string v0, "actionId"

    .line 217
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getStringOrDefault(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lhost/exp/exponent/notifications/ActionObject;->setActionId(Ljava/lang/String;)V

    const-string v0, "buttonTitle"

    .line 218
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getStringOrDefault(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lhost/exp/exponent/notifications/ActionObject;->setButtonTitle(Ljava/lang/String;)V

    const-string v0, "isDestructive"

    .line 219
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 220
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 221
    iget-object v3, p0, Lhost/exp/exponent/notifications/ActionObject_Table;->global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/raizlabs/android/dbflow/converter/BooleanConverter;->getModelValue(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Lhost/exp/exponent/notifications/ActionObject;->setDestructive(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 223
    :cond_0
    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject_Table;->global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    invoke-virtual {v0, v1}, Lcom/raizlabs/android/dbflow/converter/BooleanConverter;->getModelValue(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Lhost/exp/exponent/notifications/ActionObject;->setDestructive(Ljava/lang/Boolean;)V

    :goto_0
    const-string v0, "isAuthenticationRequired"

    .line 225
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v2, :cond_1

    .line 226
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 227
    iget-object v3, p0, Lhost/exp/exponent/notifications/ActionObject_Table;->global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/raizlabs/android/dbflow/converter/BooleanConverter;->getModelValue(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Lhost/exp/exponent/notifications/ActionObject;->setAuthenticationRequired(Ljava/lang/Boolean;)V

    goto :goto_1

    .line 229
    :cond_1
    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject_Table;->global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    invoke-virtual {v0, v1}, Lcom/raizlabs/android/dbflow/converter/BooleanConverter;->getModelValue(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Lhost/exp/exponent/notifications/ActionObject;->setAuthenticationRequired(Ljava/lang/Boolean;)V

    :goto_1
    const-string v0, "submitButtonTitle"

    .line 231
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getStringOrDefault(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lhost/exp/exponent/notifications/ActionObject;->setSubmitButtonTitle(Ljava/lang/String;)V

    const-string v0, "placeholder"

    .line 232
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getStringOrDefault(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lhost/exp/exponent/notifications/ActionObject;->setPlaceholder(Ljava/lang/String;)V

    const-string v0, "shouldShowTextInput"

    .line 233
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v2, :cond_2

    .line 234
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 235
    iget-object v1, p0, Lhost/exp/exponent/notifications/ActionObject_Table;->global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/raizlabs/android/dbflow/converter/BooleanConverter;->getModelValue(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Lhost/exp/exponent/notifications/ActionObject;->setShouldShowTextInput(Ljava/lang/Boolean;)V

    goto :goto_2

    .line 237
    :cond_2
    iget-object v0, p0, Lhost/exp/exponent/notifications/ActionObject_Table;->global_typeConverterBooleanConverter:Lcom/raizlabs/android/dbflow/converter/BooleanConverter;

    invoke-virtual {v0, v1}, Lcom/raizlabs/android/dbflow/converter/BooleanConverter;->getModelValue(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Lhost/exp/exponent/notifications/ActionObject;->setShouldShowTextInput(Ljava/lang/Boolean;)V

    :goto_2
    const-string v0, "position"

    .line 239
    invoke-virtual {p1, v0}, Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;->getIntOrDefault(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p2, p1}, Lhost/exp/exponent/notifications/ActionObject;->setPosition(I)V

    return-void
.end method

.method public bridge synthetic loadFromCursor(Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;Ljava/lang/Object;)V
    .locals 0

    .line 30
    check-cast p2, Lhost/exp/exponent/notifications/ActionObject;

    invoke-virtual {p0, p1, p2}, Lhost/exp/exponent/notifications/ActionObject_Table;->loadFromCursor(Lcom/raizlabs/android/dbflow/structure/database/FlowCursor;Lhost/exp/exponent/notifications/ActionObject;)V

    return-void
.end method

.method public final newInstance()Lhost/exp/exponent/notifications/ActionObject;
    .locals 1

    .line 94
    new-instance v0, Lhost/exp/exponent/notifications/ActionObject;

    invoke-direct {v0}, Lhost/exp/exponent/notifications/ActionObject;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lhost/exp/exponent/notifications/ActionObject_Table;->newInstance()Lhost/exp/exponent/notifications/ActionObject;

    move-result-object v0

    return-object v0
.end method
