.class public Lhost/exp/exponent/test/TestActionEvent;
.super Ljava/lang/Object;
.source "TestActionEvent.java"


# instance fields
.field public final actionType:Ljava/lang/String;

.field public final actionValue:Ljava/lang/String;

.field public final delay:I

.field public final id:I

.field public final selectorType:Ljava/lang/String;

.field public final selectorValue:Ljava/lang/String;

.field public final timeout:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, Lhost/exp/exponent/test/TestActionEvent;->id:I

    .line 17
    iput-object p2, p0, Lhost/exp/exponent/test/TestActionEvent;->selectorType:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lhost/exp/exponent/test/TestActionEvent;->selectorValue:Ljava/lang/String;

    .line 19
    iput-object p4, p0, Lhost/exp/exponent/test/TestActionEvent;->actionType:Ljava/lang/String;

    .line 20
    iput-object p5, p0, Lhost/exp/exponent/test/TestActionEvent;->actionValue:Ljava/lang/String;

    .line 21
    iput p6, p0, Lhost/exp/exponent/test/TestActionEvent;->delay:I

    .line 22
    iput p7, p0, Lhost/exp/exponent/test/TestActionEvent;->timeout:I

    return-void
.end method
