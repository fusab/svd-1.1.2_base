.class public abstract Lhost/exp/exponent/ExpoApplication;
.super Landroidx/multidex/MultiDexApplication;
.source "ExpoApplication.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ExpoApplication"


# instance fields
.field mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Landroidx/multidex/MultiDexApplication;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract gcmSenderId()Ljava/lang/String;
.end method

.method public abstract isDebug()Z
.end method

.method public onCreate()V
    .locals 4

    .line 49
    invoke-super {p0}, Landroidx/multidex/MultiDexApplication;->onCreate()V

    .line 51
    invoke-virtual {p0}, Lhost/exp/exponent/ExpoApplication;->isDebug()Z

    move-result v0

    sput-boolean v0, Lhost/exp/expoview/ExpoViewBuildConfig;->DEBUG:Z

    .line 52
    invoke-virtual {p0}, Lhost/exp/exponent/ExpoApplication;->shouldUseInternetKernel()Z

    move-result v0

    sput-boolean v0, Lhost/exp/expoview/ExpoViewBuildConfig;->USE_INTERNET_KERNEL:Z

    .line 54
    sget-boolean v0, Lhost/exp/expoview/ExpoViewBuildConfig;->DEBUG:Z

    .line 58
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    const-class v0, Lhost/exp/exponent/LauncherActivity;

    sput-object v0, Lhost/exp/exponent/kernel/KernelConstants;->MAIN_ACTIVITY_CLASS:Ljava/lang/Class;

    .line 62
    :cond_0
    const-class v0, Lhost/exp/exponent/headless/HeadlessAppLoader;

    const-string v1, "react-native-experience"

    invoke-static {v1, v0}, Lexpo/loaders/provider/AppLoaderProvider;->registerLoader(Ljava/lang/String;Ljava/lang/Class;)V

    .line 63
    new-instance v0, Lhost/exp/exponent/ExpoApplication$1;

    invoke-direct {v0, p0}, Lhost/exp/exponent/ExpoApplication$1;-><init>(Lhost/exp/exponent/ExpoApplication;)V

    invoke-static {v0}, Lhost/exp/exponent/kernel/KernelProvider;->setFactory(Lhost/exp/exponent/kernel/KernelProvider$KernelFactory;)V

    .line 70
    new-instance v0, Lhost/exp/exponent/ExpoApplication$2;

    invoke-direct {v0, p0}, Lhost/exp/exponent/ExpoApplication$2;-><init>(Lhost/exp/exponent/ExpoApplication;)V

    invoke-static {v0}, Lhost/exp/exponent/kernel/ExponentKernelModuleProvider;->setFactory(Lhost/exp/exponent/kernel/ExponentKernelModuleProvider$ExponentKernelModuleFactory;)V

    .line 77
    invoke-static {p0, p0}, Lhost/exp/expoview/Exponent;->initialize(Landroid/content/Context;Landroid/app/Application;)V

    .line 78
    invoke-static {}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->getInstance()Lhost/exp/exponent/di/NativeModuleDepsProvider;

    move-result-object v0

    const-class v1, Lhost/exp/exponent/kernel/Kernel;

    invoke-static {}, Lhost/exp/exponent/kernel/KernelProvider;->getInstance()Lhost/exp/exponent/kernel/KernelInterface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->add(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 79
    invoke-static {}, Lhost/exp/expoview/Exponent;->getInstance()Lhost/exp/expoview/Exponent;

    move-result-object v0

    invoke-virtual {p0}, Lhost/exp/exponent/ExpoApplication;->gcmSenderId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhost/exp/expoview/Exponent;->setGCMSenderId(Ljava/lang/String;)V

    .line 81
    invoke-static {}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->getInstance()Lhost/exp/exponent/di/NativeModuleDepsProvider;

    move-result-object v0

    const-class v1, Lhost/exp/exponent/ExpoApplication;

    invoke-virtual {v0, v1, p0}, Lhost/exp/exponent/di/NativeModuleDepsProvider;->inject(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 83
    sget-boolean v0, Lhost/exp/expoview/ExpoViewBuildConfig;->DEBUG:Z

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 84
    new-instance v0, Lhost/exp/exponent/ExpoApplication$3;

    invoke-direct {v0, p0}, Lhost/exp/exponent/ExpoApplication$3;-><init>(Lhost/exp/exponent/ExpoApplication;)V

    .line 91
    new-instance v2, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;

    invoke-direct {v2}, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;-><init>()V

    .line 93
    invoke-virtual {v2, v0}, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;->listener(Lcom/crashlytics/android/core/CrashlyticsListener;)Lcom/crashlytics/android/core/CrashlyticsCore$Builder;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;->build()Lcom/crashlytics/android/core/CrashlyticsCore;

    move-result-object v0

    const/4 v2, 0x1

    .line 96
    new-array v2, v2, [Lio/fabric/sdk/android/Kit;

    new-instance v3, Lcom/crashlytics/android/Crashlytics$Builder;

    invoke-direct {v3}, Lcom/crashlytics/android/Crashlytics$Builder;-><init>()V

    invoke-virtual {v3, v0}, Lcom/crashlytics/android/Crashlytics$Builder;->core(Lcom/crashlytics/android/core/CrashlyticsCore;)Lcom/crashlytics/android/Crashlytics$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/Crashlytics$Builder;->build()Lcom/crashlytics/android/Crashlytics;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-static {p0, v2}, Lio/fabric/sdk/android/Fabric;->with(Landroid/content/Context;[Lio/fabric/sdk/android/Kit;)Lio/fabric/sdk/android/Fabric;

    .line 99
    :try_start_0
    invoke-static {p0}, Lhost/exp/exponent/Constants;->getVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "exp_client_version"

    .line 100
    invoke-static {v2, v0}, Lcom/crashlytics/android/Crashlytics;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    sget-object v0, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "initial_url"

    .line 102
    sget-object v2, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/crashlytics/android/Crashlytics;->setString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 105
    sget-object v2, Lhost/exp/exponent/ExpoApplication;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :cond_1
    :goto_0
    invoke-static {p0}, Lhost/exp/exponent/branch/BranchManager;->initialize(Landroid/app/Application;)V

    .line 110
    invoke-static {p0}, Lcom/facebook/ads/AudienceNetworkAds;->initialize(Landroid/content/Context;)V

    .line 115
    :try_start_1
    invoke-static {p0}, Lme/leolin/shortcutbadger/ShortcutBadger;->removeCount(Landroid/content/Context;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    .line 117
    sget-object v2, Lhost/exp/exponent/ExpoApplication;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 124
    :goto_1
    sget-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->LAUNCHER_ACTIVITY_STARTED:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0}, Lhost/exp/exponent/analytics/Analytics;->markEvent(Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    .line 126
    invoke-virtual {p0}, Lhost/exp/exponent/ExpoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/facebook/soloader/SoLoader;->init(Landroid/content/Context;Z)V

    .line 130
    new-instance v0, Lhost/exp/exponent/ExponentUncaughtExceptionHandler;

    invoke-virtual {p0}, Lhost/exp/exponent/ExpoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lhost/exp/exponent/ExponentUncaughtExceptionHandler;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    return-void
.end method

.method public shouldUseInternetKernel()Z
    .locals 1

    .line 136
    invoke-virtual {p0}, Lhost/exp/exponent/ExpoApplication;->isDebug()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
