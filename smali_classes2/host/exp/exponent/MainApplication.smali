.class public Lhost/exp/exponent/MainApplication;
.super Lhost/exp/exponent/ExpoApplication;
.source "MainApplication.java"

# interfaces
.implements Lexpo/loaders/provider/interfaces/AppLoaderPackagesProviderInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Lhost/exp/exponent/ExpoApplication;-><init>()V

    return-void
.end method

.method public static okHttpClientBuilder(Lokhttp3/OkHttpClient$Builder;)Lokhttp3/OkHttpClient$Builder;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public gcmSenderId()Ljava/lang/String;
    .locals 1

    const v0, 0x7f1000d0

    .line 107
    invoke-virtual {p0, v0}, Lhost/exp/exponent/MainApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExpoPackages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/unimodules/core/interfaces/Package;",
            ">;"
        }
    .end annotation

    .line 102
    new-instance v0, Lhost/exp/exponent/generated/BasePackageList;

    invoke-direct {v0}, Lhost/exp/exponent/generated/BasePackageList;-><init>()V

    invoke-virtual {v0}, Lhost/exp/exponent/generated/BasePackageList;->getPackageList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPackages()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/facebook/react/ReactPackage;",
            ">;"
        }
    .end annotation

    const/16 v0, 0x10

    .line 53
    new-array v0, v0, [Lcom/facebook/react/ReactPackage;

    new-instance v1, Lcom/rajivshah/safetynet/RNGoogleSafetyNetPackage;

    invoke-direct {v1}, Lcom/rajivshah/safetynet/RNGoogleSafetyNetPackage;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lcom/dylanvann/fastimage/FastImageViewPackage;

    invoke-direct {v1}, Lcom/dylanvann/fastimage/FastImageViewPackage;-><init>()V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/adjust/nativemodule/AdjustPackage;

    invoke-direct {v1}, Lcom/adjust/nativemodule/AdjustPackage;-><init>()V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 63
    invoke-static {}, Lcom/bugsnag/BugsnagReactNative;->getPackage()Lcom/facebook/react/ReactPackage;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lcom/calendarevents/CalendarEventsPackage;

    invoke-direct {v1}, Lcom/calendarevents/CalendarEventsPackage;-><init>()V

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lio/invertase/firebase/RNFirebasePackage;

    invoke-direct {v1}, Lio/invertase/firebase/RNFirebasePackage;-><init>()V

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lcom/airbnb/android/react/maps/MapsPackage;

    invoke-direct {v1}, Lcom/airbnb/android/react/maps/MapsPackage;-><init>()V

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsPackage;

    invoke-direct {v1}, Lcom/github/wumke/RNLocalNotifications/RNLocalNotificationsPackage;-><init>()V

    const/4 v2, 0x7

    aput-object v1, v0, v2

    new-instance v1, Lcom/learnium/RNDeviceInfo/RNDeviceInfo;

    invoke-direct {v1}, Lcom/learnium/RNDeviceInfo/RNDeviceInfo;-><init>()V

    const/16 v2, 0x8

    aput-object v1, v0, v2

    new-instance v1, Lcom/polidea/reactnativeble/BlePackage;

    invoke-direct {v1}, Lcom/polidea/reactnativeble/BlePackage;-><init>()V

    const/16 v2, 0x9

    aput-object v1, v0, v2

    new-instance v1, Lcom/mackentoch/beaconsandroid/BeaconsAndroidPackage;

    invoke-direct {v1}, Lcom/mackentoch/beaconsandroid/BeaconsAndroidPackage;-><init>()V

    const/16 v2, 0xa

    aput-object v1, v0, v2

    new-instance v1, Les/iagt/adyen/RNAdyenIagtPackage;

    invoke-direct {v1}, Les/iagt/adyen/RNAdyenIagtPackage;-><init>()V

    const/16 v2, 0xb

    aput-object v1, v0, v2

    new-instance v1, Lio/invertase/firebase/messaging/RNFirebaseMessagingPackage;

    invoke-direct {v1}, Lio/invertase/firebase/messaging/RNFirebaseMessagingPackage;-><init>()V

    const/16 v2, 0xc

    aput-object v1, v0, v2

    new-instance v1, Lio/invertase/firebase/notifications/RNFirebaseNotificationsPackage;

    invoke-direct {v1}, Lio/invertase/firebase/notifications/RNFirebaseNotificationsPackage;-><init>()V

    const/16 v2, 0xd

    aput-object v1, v0, v2

    new-instance v1, Lio/invertase/firebase/analytics/RNFirebaseAnalyticsPackage;

    invoke-direct {v1}, Lio/invertase/firebase/analytics/RNFirebaseAnalyticsPackage;-><init>()V

    const/16 v2, 0xe

    aput-object v1, v0, v2

    new-instance v1, Lhost/exp/exponent/RNAmplitudePackage;

    invoke-direct {v1, p0}, Lhost/exp/exponent/RNAmplitudePackage;-><init>(Landroid/app/Application;)V

    const/16 v2, 0xf

    aput-object v1, v0, v2

    .line 53
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isDebug()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onCreate()V
    .locals 3

    .line 42
    invoke-super {p0}, Lhost/exp/exponent/ExpoApplication;->onCreate()V

    const/4 v0, 0x1

    .line 43
    new-array v0, v0, [Lio/fabric/sdk/android/Kit;

    new-instance v1, Lcom/crashlytics/android/Crashlytics;

    invoke-direct {v1}, Lcom/crashlytics/android/Crashlytics;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p0, v0}, Lio/fabric/sdk/android/Fabric;->with(Landroid/content/Context;[Lio/fabric/sdk/android/Kit;)Lio/fabric/sdk/android/Fabric;

    return-void
.end method
