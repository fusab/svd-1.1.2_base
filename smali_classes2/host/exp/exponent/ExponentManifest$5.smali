.class Lhost/exp/exponent/ExponentManifest$5;
.super Landroid/os/AsyncTask;
.source "ExponentManifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhost/exp/exponent/ExponentManifest;->loadIconBitmap(Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$BitmapListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lhost/exp/exponent/ExponentManifest;

.field final synthetic val$iconUrl:Ljava/lang/String;

.field final synthetic val$listener:Lhost/exp/exponent/ExponentManifest$BitmapListener;


# direct methods
.method constructor <init>(Lhost/exp/exponent/ExponentManifest;Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$BitmapListener;)V
    .locals 0

    .line 605
    iput-object p1, p0, Lhost/exp/exponent/ExponentManifest$5;->this$0:Lhost/exp/exponent/ExponentManifest;

    iput-object p2, p0, Lhost/exp/exponent/ExponentManifest$5;->val$iconUrl:Ljava/lang/String;

    iput-object p3, p0, Lhost/exp/exponent/ExponentManifest$5;->val$listener:Lhost/exp/exponent/ExponentManifest$BitmapListener;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 5

    .line 611
    :try_start_0
    new-instance p1, Ljava/net/URL;

    iget-object v0, p0, Lhost/exp/exponent/ExponentManifest$5;->val$iconUrl:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 612
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object p1

    check-cast p1, Ljava/net/HttpURLConnection;

    const/4 v0, 0x1

    .line 613
    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 614
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->connect()V

    .line 615
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object p1

    .line 617
    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 618
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 619
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    const/16 v3, 0xc0

    if-gt v1, v3, :cond_0

    if-gt v2, v3, :cond_0

    .line 621
    iget-object v0, p0, Lhost/exp/exponent/ExponentManifest$5;->this$0:Lhost/exp/exponent/ExponentManifest;

    invoke-static {v0}, Lhost/exp/exponent/ExponentManifest;->access$300(Lhost/exp/exponent/ExponentManifest;)Landroid/util/LruCache;

    move-result-object v0

    iget-object v1, p0, Lhost/exp/exponent/ExponentManifest$5;->val$iconUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p1

    .line 625
    :cond_0
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    int-to-float v1, v1

    const/high16 v4, 0x43400000    # 192.0f

    mul-float v1, v1, v4

    int-to-float v3, v3

    div-float/2addr v1, v3

    int-to-float v2, v2

    mul-float v2, v2, v4

    div-float/2addr v2, v3

    float-to-int v1, v1

    float-to-int v2, v2

    .line 628
    invoke-static {p1, v1, v2, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 629
    iget-object v0, p0, Lhost/exp/exponent/ExponentManifest$5;->this$0:Lhost/exp/exponent/ExponentManifest;

    invoke-static {v0}, Lhost/exp/exponent/ExponentManifest;->access$300(Lhost/exp/exponent/ExponentManifest;)Landroid/util/LruCache;

    move-result-object v0

    iget-object v1, p0, Lhost/exp/exponent/ExponentManifest$5;->val$iconUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 635
    invoke-static {}, Lhost/exp/exponent/ExponentManifest;->access$200()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 636
    iget-object p1, p0, Lhost/exp/exponent/ExponentManifest$5;->this$0:Lhost/exp/exponent/ExponentManifest;

    iget-object p1, p1, Lhost/exp/exponent/ExponentManifest;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lhost/exp/expoview/R$mipmap;->ic_launcher:I

    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1

    :catch_1
    move-exception p1

    .line 632
    invoke-static {}, Lhost/exp/exponent/ExponentManifest;->access$200()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 633
    iget-object p1, p0, Lhost/exp/exponent/ExponentManifest$5;->this$0:Lhost/exp/exponent/ExponentManifest;

    iget-object p1, p1, Lhost/exp/exponent/ExponentManifest;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lhost/exp/expoview/R$mipmap;->ic_launcher:I

    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 605
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lhost/exp/exponent/ExponentManifest$5;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 642
    iget-object v0, p0, Lhost/exp/exponent/ExponentManifest$5;->val$listener:Lhost/exp/exponent/ExponentManifest$BitmapListener;

    invoke-interface {v0, p1}, Lhost/exp/exponent/ExponentManifest$BitmapListener;->onLoadBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 605
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lhost/exp/exponent/ExponentManifest$5;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
