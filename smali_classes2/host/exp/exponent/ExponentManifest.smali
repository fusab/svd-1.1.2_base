.class public Lhost/exp/exponent/ExponentManifest;
.super Ljava/lang/Object;
.source "ExponentManifest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhost/exp/exponent/ExponentManifest$BitmapListener;,
        Lhost/exp/exponent/ExponentManifest$ManifestListener;
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final ANONYMOUS_EXPERIENCE_PREFIX:Ljava/lang/String; = "@anonymous/"

.field public static final DEEP_LINK_SEPARATOR:Ljava/lang/String; = "--"

.field public static final DEEP_LINK_SEPARATOR_WITH_SLASH:Ljava/lang/String; = "--/"

.field private static final EMBEDDED_KERNEL_MANIFEST_ASSET:Ljava/lang/String; = "kernel-manifest.json"

.field private static final EXPONENT_SERVER_HEADER:Ljava/lang/String; = "Exponent-Server"

.field public static final MANIFEST_APP_KEY_KEY:Ljava/lang/String; = "appKey"

.field public static final MANIFEST_BUNDLE_URL_KEY:Ljava/lang/String; = "bundleUrl"

.field public static final MANIFEST_COMMIT_TIME_KEY:Ljava/lang/String; = "commitTime"

.field public static final MANIFEST_DEBUGGER_HOST_KEY:Ljava/lang/String; = "debuggerHost"

.field public static final MANIFEST_DEVELOPER_KEY:Ljava/lang/String; = "developer"

.field public static final MANIFEST_ICON_URL_KEY:Ljava/lang/String; = "iconUrl"

.field public static final MANIFEST_ID_KEY:Ljava/lang/String; = "id"

.field public static final MANIFEST_IS_VERIFIED_KEY:Ljava/lang/String; = "isVerified"

.field public static final MANIFEST_LOADED_FROM_CACHE_KEY:Ljava/lang/String; = "loadedFromCache"

.field public static final MANIFEST_LOADING_BACKGROUND_COLOR:Ljava/lang/String; = "backgroundColor"

.field public static final MANIFEST_LOADING_BACKGROUND_IMAGE_URL:Ljava/lang/String; = "backgroundImageUrl"

.field public static final MANIFEST_LOADING_EXPONENT_ICON_COLOR:Ljava/lang/String; = "exponentIconColor"

.field public static final MANIFEST_LOADING_EXPONENT_ICON_GRAYSCALE:Ljava/lang/String; = "exponentIconGrayscale"

.field public static final MANIFEST_LOADING_ICON_URL:Ljava/lang/String; = "iconUrl"

.field public static final MANIFEST_LOADING_INFO_KEY:Ljava/lang/String; = "loading"

.field public static final MANIFEST_MAIN_MODULE_NAME_KEY:Ljava/lang/String; = "mainModuleName"

.field public static final MANIFEST_NAME_KEY:Ljava/lang/String; = "name"

.field public static final MANIFEST_NAVIGATION_BAR_APPEARANCE:Ljava/lang/String; = "barStyle"

.field public static final MANIFEST_NAVIGATION_BAR_BACKGROUND_COLOR:Ljava/lang/String; = "backgroundColor"

.field public static final MANIFEST_NAVIGATION_BAR_KEY:Ljava/lang/String; = "androidNavigationBar"

.field public static final MANIFEST_NAVIGATION_BAR_VISIBLILITY:Ljava/lang/String; = "visible"

.field public static final MANIFEST_NOTIFICATION_ANDROID_COLLAPSED_TITLE:Ljava/lang/String; = "androidCollapsedTitle"

.field public static final MANIFEST_NOTIFICATION_ANDROID_MODE:Ljava/lang/String; = "androidMode"

.field public static final MANIFEST_NOTIFICATION_COLOR_KEY:Ljava/lang/String; = "color"

.field public static final MANIFEST_NOTIFICATION_ICON_URL_KEY:Ljava/lang/String; = "iconUrl"

.field public static final MANIFEST_NOTIFICATION_INFO_KEY:Ljava/lang/String; = "notification"

.field public static final MANIFEST_ORIENTATION_KEY:Ljava/lang/String; = "orientation"

.field public static final MANIFEST_PACKAGER_OPTS_DEV_KEY:Ljava/lang/String; = "dev"

.field public static final MANIFEST_PACKAGER_OPTS_KEY:Ljava/lang/String; = "packagerOpts"

.field public static final MANIFEST_PRIMARY_COLOR_KEY:Ljava/lang/String; = "primaryColor"

.field public static final MANIFEST_PUBLISHED_TIME_KEY:Ljava/lang/String; = "publishedTime"

.field public static final MANIFEST_REVISION_ID_KEY:Ljava/lang/String; = "revisionId"

.field public static final MANIFEST_SDK_VERSION_KEY:Ljava/lang/String; = "sdkVersion"

.field public static final MANIFEST_SHOW_EXPONENT_NOTIFICATION_KEY:Ljava/lang/String; = "androidShowExponentNotificationInShellApp"

.field public static final MANIFEST_SIGNATURE_KEY:Ljava/lang/String; = "signature"

.field public static final MANIFEST_SLUG:Ljava/lang/String; = "slug"

.field public static final MANIFEST_SPLASH_BACKGROUND_COLOR:Ljava/lang/String; = "backgroundColor"

.field public static final MANIFEST_SPLASH_IMAGE_URL:Ljava/lang/String; = "imageUrl"

.field public static final MANIFEST_SPLASH_INFO_KEY:Ljava/lang/String; = "splash"

.field public static final MANIFEST_SPLASH_RESIZE_MODE:Ljava/lang/String; = "resizeMode"

.field public static final MANIFEST_STATUS_BAR_APPEARANCE:Ljava/lang/String; = "barStyle"

.field public static final MANIFEST_STATUS_BAR_BACKGROUND_COLOR:Ljava/lang/String; = "backgroundColor"

.field public static final MANIFEST_STATUS_BAR_COLOR:Ljava/lang/String; = "androidStatusBarColor"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MANIFEST_STATUS_BAR_KEY:Ljava/lang/String; = "androidStatusBar"

.field public static final MANIFEST_STRING_KEY:Ljava/lang/String; = "manifestString"

.field public static final MANIFEST_UPDATES_CHECK_AUTOMATICALLY_KEY:Ljava/lang/String; = "checkAutomatically"

.field public static final MANIFEST_UPDATES_CHECK_AUTOMATICALLY_ON_ERROR:Ljava/lang/String; = "ON_ERROR_RECOVERY"

.field public static final MANIFEST_UPDATES_CHECK_AUTOMATICALLY_ON_LOAD:Ljava/lang/String; = "ON_LOAD"

.field public static final MANIFEST_UPDATES_INFO_KEY:Ljava/lang/String; = "updates"

.field public static final MANIFEST_UPDATES_TIMEOUT_KEY:Ljava/lang/String; = "fallbackToCacheTimeout"

.field private static final MAX_BITMAP_SIZE:I = 0xc0

.field public static final QUERY_PARAM_KEY_RELEASE_CHANNEL:Ljava/lang/String; = "release-channel"

.field private static final REDIRECT_SNIPPET:Ljava/lang/String; = "exp.host/--/to-exp/"

.field private static final TAG:Ljava/lang/String; = "ExponentManifest"

.field private static hasShownKernelManifestLog:Z = false


# instance fields
.field mContext:Landroid/content/Context;

.field mCrypto:Lhost/exp/exponent/kernel/Crypto;

.field mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;

.field mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

.field private mMemoryCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lhost/exp/exponent/network/ExponentNetwork;Lhost/exp/exponent/kernel/Crypto;Lhost/exp/exponent/storage/ExponentSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput-object p1, p0, Lhost/exp/exponent/ExponentManifest;->mContext:Landroid/content/Context;

    .line 155
    iput-object p2, p0, Lhost/exp/exponent/ExponentManifest;->mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;

    .line 156
    iput-object p3, p0, Lhost/exp/exponent/ExponentManifest;->mCrypto:Lhost/exp/exponent/kernel/Crypto;

    .line 157
    iput-object p4, p0, Lhost/exp/exponent/ExponentManifest;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    .line 159
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide p1

    const-wide/16 p3, 0x400

    div-long/2addr p1, p3

    long-to-int p2, p1

    .line 161
    div-int/lit8 p2, p2, 0x10

    .line 162
    new-instance p1, Lhost/exp/exponent/ExponentManifest$1;

    invoke-direct {p1, p0, p2}, Lhost/exp/exponent/ExponentManifest$1;-><init>(Lhost/exp/exponent/ExponentManifest;I)V

    iput-object p1, p0, Lhost/exp/exponent/ExponentManifest;->mMemoryCache:Landroid/util/LruCache;

    return-void
.end method

.method static synthetic access$000(Lhost/exp/exponent/ExponentManifest;Lhost/exp/exponent/network/ExpoResponse;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$ManifestListener;ZZ)V
    .locals 0

    .line 53
    invoke-direct/range {p0 .. p6}, Lhost/exp/exponent/ExponentManifest;->handleManifestResponse(Lhost/exp/exponent/network/ExpoResponse;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$ManifestListener;ZZ)V

    return-void
.end method

.method static synthetic access$100(Lhost/exp/exponent/ExponentManifest;Ljava/lang/String;Lorg/json/JSONObject;ZLhost/exp/exponent/ExponentManifest$ManifestListener;)V
    .locals 0

    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Lhost/exp/exponent/ExponentManifest;->fetchManifestStep3(Ljava/lang/String;Lorg/json/JSONObject;ZLhost/exp/exponent/ExponentManifest$ManifestListener;)V

    return-void
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .line 53
    sget-object v0, Lhost/exp/exponent/ExponentManifest;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lhost/exp/exponent/ExponentManifest;)Landroid/util/LruCache;
    .locals 0

    .line 53
    iget-object p0, p0, Lhost/exp/exponent/ExponentManifest;->mMemoryCache:Landroid/util/LruCache;

    return-object p0
.end method

.method private extractManifest(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 420
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 428
    :catch_0
    :try_start_1
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 429
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 430
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "sdkVersion"

    .line 431
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 432
    sget-object v4, Lhost/exp/exponent/Constants;->SDK_VERSIONS_LIST:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v3, :cond_0

    return-object v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 439
    :cond_1
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No compatible manifest found. SDK Versions supported: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lhost/exp/exponent/Constants;->SDK_VERSIONS:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " Provided manifestString: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v0

    .line 437
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Manifest string is not a valid JSONObject or JSONArray: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private fetchManifestStep2(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/network/ExpoHeaders;Lhost/exp/exponent/ExponentManifest$ManifestListener;ZZ)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/net/URISyntaxException;,
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v7, p0

    move-object/from16 v4, p1

    move-object/from16 v8, p4

    move-object/from16 v6, p5

    if-eqz v8, :cond_0

    .line 447
    sget-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->FINISHED_MANIFEST_NETWORK_REQUEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0}, Lhost/exp/exponent/analytics/Analytics;->markEvent(Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    :cond_0
    move-object/from16 v0, p3

    .line 450
    invoke-direct {v7, v0}, Lhost/exp/exponent/ExponentManifest;->extractManifest(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 451
    sget-object v0, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 452
    new-instance v3, Ljava/net/URI;

    invoke-direct {v3, v4}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    const-string v5, "manifestString"

    .line 454
    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    const-string v9, "signature"

    const/4 v10, 0x0

    const/4 v11, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {v1, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v12, 0x1

    goto :goto_0

    :cond_1
    const/4 v12, 0x0

    :goto_0
    if-eqz v12, :cond_2

    .line 459
    new-instance v0, Lorg/json/JSONObject;

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v0, v13}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    move-object v13, v0

    goto :goto_1

    :cond_2
    move-object v13, v1

    :goto_1
    if-nez p6, :cond_5

    if-eqz p7, :cond_5

    .line 466
    iget-object v0, v7, Lhost/exp/exponent/ExponentManifest;->mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;

    invoke-virtual {v0}, Lhost/exp/exponent/network/ExponentNetwork;->getClient()Lhost/exp/exponent/network/ExponentHttpClient;

    move-result-object v0

    move-object/from16 v14, p2

    invoke-virtual {v0, v14}, Lhost/exp/exponent/network/ExponentHttpClient;->getHardCodedResponse(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 469
    :try_start_0
    new-instance v14, Lorg/json/JSONObject;

    invoke-direct {v14, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 470
    invoke-direct {v7, v13}, Lhost/exp/exponent/ExponentManifest;->isManifestSDKVersionValid(Lorg/json/JSONObject;)Z

    move-result v0

    if-nez v0, :cond_3

    move-object v0, v14

    goto :goto_2

    .line 476
    :cond_3
    invoke-direct {v7, v14, v13}, Lhost/exp/exponent/ExponentManifest;->newerManifest(Lorg/json/JSONObject;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    if-ne v14, v0, :cond_4

    const/4 v13, 0x1

    goto :goto_3

    :cond_4
    const/4 v13, 0x0

    :goto_3
    move/from16 v16, v13

    move-object v13, v0

    move/from16 v0, v16

    goto :goto_4

    :catch_0
    move-exception v0

    .line 480
    sget-object v14, Lhost/exp/exponent/ExponentManifest;->TAG:Ljava/lang/String;

    invoke-static {v14, v0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_5
    move/from16 v0, p6

    :goto_4
    if-nez p7, :cond_7

    if-eqz v0, :cond_6

    goto :goto_5

    :cond_6
    const/4 v14, 0x0

    goto :goto_6

    :cond_7
    :goto_5
    const/4 v14, 0x1

    :goto_6
    const-string v15, "loadedFromCache"

    .line 484
    invoke-virtual {v13, v15, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    if-eqz v12, :cond_a

    .line 487
    iget-object v3, v7, Lhost/exp/exponent/ExponentManifest;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lhost/exp/exponent/network/ExponentNetwork;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result v3

    xor-int/2addr v3, v11

    .line 489
    invoke-direct {v7, v13}, Lhost/exp/exponent/ExponentManifest;->isAnonymousExperience(Lorg/json/JSONObject;)Z

    move-result v10

    if-nez v10, :cond_9

    if-nez v2, :cond_9

    if-eqz v0, :cond_8

    goto :goto_7

    .line 494
    :cond_8
    iget-object v0, v7, Lhost/exp/exponent/ExponentManifest;->mCrypto:Lhost/exp/exponent/kernel/Crypto;

    .line 495
    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance v11, Lhost/exp/exponent/ExponentManifest$4;

    move-object v1, v11

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    move-object v5, v13

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v6}, Lhost/exp/exponent/ExponentManifest$4;-><init>(Lhost/exp/exponent/ExponentManifest;ZLjava/lang/String;Lorg/json/JSONObject;Lhost/exp/exponent/ExponentManifest$ManifestListener;)V

    const-string v1, "https://exp.host/--/manifest-public-key"

    .line 494
    invoke-virtual {v0, v1, v10, v9, v11}, Lhost/exp/exponent/kernel/Crypto;->verifyPublicRSASignature(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/kernel/Crypto$RSASignatureListener;)V

    goto/16 :goto_c

    .line 491
    :cond_9
    :goto_7
    invoke-direct {v7, v4, v13, v11, v6}, Lhost/exp/exponent/ExponentManifest;->fetchManifestStep3(Ljava/lang/String;Lorg/json/JSONObject;ZLhost/exp/exponent/ExponentManifest$ManifestListener;)V

    goto/16 :goto_c

    :cond_a
    if-nez p7, :cond_12

    if-nez v0, :cond_12

    if-eqz v2, :cond_b

    goto/16 :goto_b

    .line 519
    :cond_b
    invoke-direct {v7, v3}, Lhost/exp/exponent/ExponentManifest;->isThirdPartyHosted(Ljava/net/URI;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 523
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result v0

    if-nez v0, :cond_10

    .line 524
    invoke-virtual {v3}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https"

    .line 525
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, ""

    if-nez v1, :cond_d

    const-string v1, "exps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    goto :goto_8

    :cond_c
    const-string v0, "UNVERIFIED-"

    goto :goto_9

    :cond_d
    :goto_8
    move-object v0, v2

    .line 526
    :goto_9
    invoke-virtual {v3}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_e

    invoke-virtual {v3}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v1

    goto :goto_a

    :cond_e
    move-object v1, v2

    :goto_a
    const-string v5, "slug"

    .line 527
    invoke-virtual {v13, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_f

    invoke-virtual {v13, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 528
    :cond_f
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "-"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "id"

    .line 529
    invoke-virtual {v13, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 531
    :cond_10
    invoke-direct {v7, v4, v13, v11, v6}, Lhost/exp/exponent/ExponentManifest;->fetchManifestStep3(Ljava/lang/String;Lorg/json/JSONObject;ZLhost/exp/exponent/ExponentManifest$ManifestListener;)V

    goto :goto_c

    .line 533
    :cond_11
    invoke-direct {v7, v4, v13, v10, v6}, Lhost/exp/exponent/ExponentManifest;->fetchManifestStep3(Ljava/lang/String;Lorg/json/JSONObject;ZLhost/exp/exponent/ExponentManifest$ManifestListener;)V

    goto :goto_c

    .line 518
    :cond_12
    :goto_b
    invoke-direct {v7, v4, v13, v11, v6}, Lhost/exp/exponent/ExponentManifest;->fetchManifestStep3(Ljava/lang/String;Lorg/json/JSONObject;ZLhost/exp/exponent/ExponentManifest$ManifestListener;)V

    :goto_c
    if-eqz v8, :cond_13

    const-string v0, "Exponent-Server"

    .line 538
    invoke-interface {v8, v0}, Lhost/exp/exponent/network/ExpoHeaders;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 541
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "LOAD_DEVELOPER_MANIFEST"

    .line 542
    invoke-static {v0, v1}, Lhost/exp/exponent/analytics/Analytics;->logEvent(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_d

    :catch_1
    move-exception v0

    .line 544
    sget-object v1, Lhost/exp/exponent/ExponentManifest;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_13
    :goto_d
    return-void
.end method

.method private fetchManifestStep3(Ljava/lang/String;Lorg/json/JSONObject;ZLhost/exp/exponent/ExponentManifest$ManifestListener;)V
    .locals 0

    const-string p1, "bundleUrl"

    .line 558
    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "No bundleUrl in manifest"

    .line 559
    invoke-interface {p4, p1}, Lhost/exp/exponent/ExponentManifest$ManifestListener;->onError(Ljava/lang/String;)V

    return-void

    :cond_0
    :try_start_0
    const-string p1, "isVerified"

    .line 564
    invoke-virtual {p2, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 570
    invoke-interface {p4, p2}, Lhost/exp/exponent/ExponentManifest$ManifestListener;->onCompleted(Lorg/json/JSONObject;)V

    return-void

    :catch_0
    move-exception p1

    .line 566
    invoke-interface {p4, p1}, Lhost/exp/exponent/ExponentManifest$ManifestListener;->onError(Ljava/lang/Exception;)V

    return-void
.end method

.method private getLocalKernelManifest()Lorg/json/JSONObject;
    .locals 4

    .line 673
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    const-string v1, "{\"android\":{\"package\":\"host.exp.exponent\"},\"dependencies\":[\"@expo/react-native-action-sheet\",\"@expo/react-native-touchable-native-feedback-safe\",\"@react-navigation/web\",\"apollo-boost\",\"apollo-cache-inmemory\",\"dedent\",\"es6-error\",\"expo\",\"expo-analytics-amplitude\",\"expo-asset\",\"expo-barcode-scanner\",\"expo-blur\",\"expo-camera\",\"expo-constants\",\"expo-font\",\"expo-linear-gradient\",\"expo-location\",\"expo-permissions\",\"expo-task-manager\",\"expo-web-browser\",\"graphql\",\"graphql-tag\",\"immutable\",\"lodash\",\"prop-types\",\"querystring\",\"react\",\"react-apollo\",\"react-native\",\"react-native-appearance\",\"react-native-fade-in-image\",\"react-native-gesture-handler\",\"react-native-infinite-scroll-view\",\"react-native-maps\",\"react-navigation\",\"react-navigation-material-bottom-tabs\",\"react-navigation-stack\",\"react-navigation-tabs\",\"react-redux\",\"redux\",\"redux-thunk\",\"semver\",\"sha1\",\"url\"],\"description\":\"\",\"extra\":{\"amplitudeApiKey\":\"081e5ec53f869b440b225d5e40ec73f9\"},\"icon\":\"https://s3.amazonaws.com/exp-brand-assets/ExponentEmptyManifest_192.png\",\"iconUrl\":\"https://s3.amazonaws.com/exp-brand-assets/ExponentEmptyManifest_192.png\",\"ios\":{\"bundleIdentifier\":\"host.exp.exponent\",\"supportsTablet\":true},\"locales\":{},\"name\":\"expo-home\",\"orientation\":\"portrait\",\"packagerOpts\":{\"config\":\"metro.config.js\"},\"platforms\":[\"ios\",\"android\"],\"primaryColor\":\"#cccccc\",\"privacy\":\"unlisted\",\"scheme\":\"exp\",\"sdkVersion\":\"UNVERSIONED\",\"slug\":\"expo-home-dev-4207c5b7dad576285280451b76c6b0fdd7ad99fe\",\"updates\":{\"checkAutomatically\":\"ON_LOAD\",\"fallbackToCacheTimeout\":0},\"version\":\"35.0.0\",\"id\":\"@expo-home-dev/expo-home-dev-4207c5b7dad576285280451b76c6b0fdd7ad99fe\",\"revisionId\":\"35.0.0-r.z6BOjJRda8\",\"publishedTime\":\"2019-09-17T17:10:35.846Z\",\"commitTime\":\"2019-09-17T17:10:35.892Z\",\"bundleUrl\":\"https://d1wp6m56sqw74a.cloudfront.net/%40expo-home-dev%2Fexpo-home-dev-4207c5b7dad576285280451b76c6b0fdd7ad99fe%2F35.0.0%2Ffb6e6731bafa66b1c5b253b2789e02eb-35.0.0-android.js\",\"releaseChannel\":\"default\",\"hostUri\":\"expo.io/@expo-home-dev/expo-home-dev-4207c5b7dad576285280451b76c6b0fdd7ad99fe\"}"

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "isVerified"

    const/4 v2, 0x1

    .line 674
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 677
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can\'t get local manifest: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private getRemoteKernelManifest()Lorg/json/JSONObject;
    .locals 3

    .line 683
    :try_start_0
    iget-object v0, p0, Lhost/exp/exponent/ExponentManifest;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "kernel-manifest.json"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 684
    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->toString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    .line 685
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "isVerified"

    const/4 v2, 0x1

    .line 686
    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    .line 689
    invoke-static {}, Lhost/exp/exponent/kernel/KernelProvider;->getInstance()Lhost/exp/exponent/kernel/KernelInterface;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhost/exp/exponent/kernel/KernelInterface;->handleError(Ljava/lang/Exception;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method private handleManifestResponse(Lhost/exp/exponent/network/ExpoResponse;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$ManifestListener;ZZ)V
    .locals 9

    .line 353
    invoke-interface {p1}, Lhost/exp/exponent/network/ExpoResponse;->isSuccessful()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p3, 0x0

    .line 356
    :try_start_0
    new-instance p5, Lorg/json/JSONObject;

    invoke-interface {p1}, Lhost/exp/exponent/network/ExpoResponse;->body()Lhost/exp/exponent/network/ExpoBody;

    move-result-object p1

    invoke-interface {p1}, Lhost/exp/exponent/network/ExpoBody;->string()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 357
    new-instance p1, Lhost/exp/exponent/exceptions/ManifestException;

    invoke-direct {p1, p3, p2, p5}, Lhost/exp/exponent/exceptions/ManifestException;-><init>(Ljava/lang/Exception;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 359
    :catch_0
    new-instance p1, Lhost/exp/exponent/exceptions/ManifestException;

    invoke-direct {p1, p3, p2}, Lhost/exp/exponent/exceptions/ManifestException;-><init>(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 361
    :goto_0
    invoke-interface {p4, p1}, Lhost/exp/exponent/ExponentManifest$ManifestListener;->onError(Ljava/lang/Exception;)V

    return-void

    .line 366
    :cond_0
    :try_start_1
    invoke-interface {p1}, Lhost/exp/exponent/network/ExpoResponse;->body()Lhost/exp/exponent/network/ExpoBody;

    move-result-object v0

    invoke-interface {v0}, Lhost/exp/exponent/network/ExpoBody;->string()Ljava/lang/String;

    move-result-object v4

    .line 367
    invoke-interface {p1}, Lhost/exp/exponent/network/ExpoResponse;->headers()Lhost/exp/exponent/network/ExpoHeaders;

    move-result-object v5

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v6, p4

    move v7, p5

    move v8, p6

    invoke-direct/range {v1 .. v8}, Lhost/exp/exponent/ExponentManifest;->fetchManifestStep2(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/network/ExpoHeaders;Lhost/exp/exponent/ExponentManifest$ManifestListener;ZZ)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    .line 373
    invoke-interface {p4, p1}, Lhost/exp/exponent/ExponentManifest$ManifestListener;->onError(Ljava/lang/Exception;)V

    goto :goto_1

    :catch_2
    move-exception p1

    .line 371
    invoke-interface {p4, p1}, Lhost/exp/exponent/ExponentManifest$ManifestListener;->onError(Ljava/lang/Exception;)V

    goto :goto_1

    :catch_3
    move-exception p1

    .line 369
    invoke-interface {p4, p1}, Lhost/exp/exponent/ExponentManifest$ManifestListener;->onError(Ljava/lang/Exception;)V

    :goto_1
    return-void
.end method

.method private httpManifestUrlBuilder(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .locals 3

    const-string v0, "exp.host/--/to-exp/"

    .line 172
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 184
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x13

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 187
    :cond_0
    invoke-static {p1}, Lhost/exp/exponent/kernel/ExponentUrls;->toHttp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 189
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 190
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    :cond_1
    const-string v1, "--/"

    .line 194
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-le v1, v2, :cond_2

    const/4 v2, 0x0

    .line 196
    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 198
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p1

    return-object p1
.end method

.method private isAnonymousExperience(Lorg/json/JSONObject;)Z
    .locals 2

    const-string v0, "id"

    .line 661
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 662
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "@anonymous/"

    .line 663
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public static isDebugModeEnabled(Lorg/json/JSONObject;)Z
    .locals 3

    const-string v0, "packagerOpts"

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    :try_start_0
    const-string v2, "developer"

    .line 725
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 726
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 727
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0

    const-string v0, "dev"

    .line 728
    invoke-virtual {p0, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result p0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    nop

    :catch_0
    :cond_0
    return v1
.end method

.method private isManifestSDKVersionValid(Lorg/json/JSONObject;)Z
    .locals 3

    const-string v0, "sdkVersion"

    .line 405
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "UNVERSIONED"

    .line 406
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 409
    :cond_0
    sget-object v0, Lhost/exp/exponent/Constants;->SDK_VERSIONS_LIST:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 410
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    return v1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method private isThirdPartyHosted(Ljava/net/URI;)Z
    .locals 2

    .line 551
    invoke-virtual {p1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object p1

    const-string v0, "exp.host"

    .line 552
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    const-string v0, "expo.io"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "exp.direct"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "expo.test"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, ".exp.host"

    .line 553
    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, ".expo.io"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, ".exp.direct"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, ".expo.test"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    xor-int/2addr p1, v1

    return p1
.end method

.method private newerManifest(Lorg/json/JSONObject;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "commitTime"

    .line 381
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "publishedTime"

    if-nez v1, :cond_0

    .line 383
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 385
    :cond_0
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 387
    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 393
    :cond_1
    new-instance v2, Ljava/text/SimpleDateFormat;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'"

    invoke-direct {v2, v4, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 394
    invoke-virtual {v2, v1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 395
    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 397
    invoke-virtual {v1, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_2

    return-object p1

    :cond_2
    return-object p2
.end method


# virtual methods
.method public fetchCachedManifest(Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$ManifestListener;)Z
    .locals 15

    move-object v9, p0

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    .line 262
    invoke-direct/range {p0 .. p1}, Lhost/exp/exponent/ExponentManifest;->httpManifestUrlBuilder(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 263
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 265
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v2, "Could not load manifest."

    .line 267
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 268
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " Are you sure this experience has been published?"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 270
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " Are you sure this is a valid URL?"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 272
    :goto_0
    invoke-interface {v11, v2}, Lhost/exp/exponent/ExponentManifest$ManifestListener;->onError(Ljava/lang/String;)V

    .line 275
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    const-string v3, "localhost"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v2, ".exp.direct"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    .line 281
    :cond_2
    sget-object v0, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    .line 283
    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iget-object v2, v9, Lhost/exp/exponent/ExponentManifest;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    .line 284
    invoke-virtual {v2}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getSessionSecret()Ljava/lang/String;

    move-result-object v2

    .line 281
    invoke-static {v1, v0, v2}, Lhost/exp/exponent/kernel/ExponentUrls;->addExponentHeadersToManifestUrl(Ljava/lang/String;ZLjava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v0

    const-string v1, "true"

    const-string v2, "Exponent-Accept-Signature"

    .line 286
    invoke-virtual {v0, v2, v1}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    const-string v2, "Expo-JSON-Error"

    .line 287
    invoke-virtual {v0, v2, v1}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 289
    invoke-virtual {v0}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v12

    .line 290
    invoke-virtual {v12}, Lokhttp3/Request;->url()Lokhttp3/HttpUrl;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/HttpUrl;->toString()Ljava/lang/String;

    move-result-object v13

    .line 294
    iget-object v0, v9, Lhost/exp/exponent/ExponentManifest;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-virtual {v0, v10}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getSafeManifestString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v14, 0x1

    if-eqz v4, :cond_3

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v1, p0

    move-object/from16 v2, p1

    move-object v3, v13

    move-object/from16 v6, p2

    .line 299
    :try_start_0
    invoke-direct/range {v1 .. v8}, Lhost/exp/exponent/ExponentManifest;->fetchManifestStep2(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhost/exp/exponent/network/ExpoHeaders;Lhost/exp/exponent/ExponentManifest$ManifestListener;ZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v14

    :catch_0
    move-exception v0

    move-object v1, v0

    .line 302
    sget-object v0, Lhost/exp/exponent/ExponentManifest;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 307
    :cond_3
    iget-object v0, v9, Lhost/exp/exponent/ExponentManifest;->mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;

    invoke-virtual {v0}, Lhost/exp/exponent/network/ExponentNetwork;->getClient()Lhost/exp/exponent/network/ExponentHttpClient;

    move-result-object v3

    new-instance v6, Lhost/exp/exponent/ExponentManifest$3;

    invoke-direct {v6, p0, v11, v10, v13}, Lhost/exp/exponent/ExponentManifest$3;-><init>(Lhost/exp/exponent/ExponentManifest;Lhost/exp/exponent/ExponentManifest$ManifestListener;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v4, v13

    move-object v5, v12

    invoke-virtual/range {v3 .. v8}, Lhost/exp/exponent/network/ExponentHttpClient;->tryForcedCachedResponse(Ljava/lang/String;Lokhttp3/Request;Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;Lokhttp3/Response;Ljava/io/IOException;)V

    return v14

    :cond_4
    :goto_1
    const/4 v0, 0x0

    return v0
.end method

.method public fetchEmbeddedManifest(Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$ManifestListener;)V
    .locals 3

    .line 329
    invoke-direct {p0, p1}, Lhost/exp/exponent/ExponentManifest;->httpManifestUrlBuilder(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 331
    sget-object v1, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    .line 333
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iget-object v2, p0, Lhost/exp/exponent/ExponentManifest;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    .line 334
    invoke-virtual {v2}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getSessionSecret()Ljava/lang/String;

    move-result-object v2

    .line 331
    invoke-static {v0, v1, v2}, Lhost/exp/exponent/kernel/ExponentUrls;->addExponentHeadersToManifestUrl(Ljava/lang/String;ZLjava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v0

    const-string v1, "true"

    const-string v2, "Exponent-Accept-Signature"

    .line 336
    invoke-virtual {v0, v2, v1}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    const-string v2, "Expo-JSON-Error"

    .line 337
    invoke-virtual {v0, v2, v1}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 338
    invoke-virtual {v0}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Request;->url()Lokhttp3/HttpUrl;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/HttpUrl;->toString()Ljava/lang/String;

    move-result-object v0

    .line 340
    iget-object v1, p0, Lhost/exp/exponent/ExponentManifest;->mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;

    invoke-virtual {v1}, Lhost/exp/exponent/network/ExponentNetwork;->getClient()Lhost/exp/exponent/network/ExponentHttpClient;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhost/exp/exponent/network/ExponentHttpClient;->getHardCodedResponse(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 343
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "loadedFromCache"

    const/4 v2, 0x1

    .line 344
    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 345
    invoke-direct {p0, p1, v1, v2, p2}, Lhost/exp/exponent/ExponentManifest;->fetchManifestStep3(Ljava/lang/String;Lorg/json/JSONObject;ZLhost/exp/exponent/ExponentManifest$ManifestListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 347
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Could not load embedded manifest. Are you sure this experience has been published?"

    invoke-direct {v0, v1, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {p2, v0}, Lhost/exp/exponent/ExponentManifest$ManifestListener;->onError(Ljava/lang/Exception;)V

    .line 348
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public fetchManifest(Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$ManifestListener;)V
    .locals 1

    const/4 v0, 0x1

    .line 202
    invoke-virtual {p0, p1, p2, v0}, Lhost/exp/exponent/ExponentManifest;->fetchManifest(Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$ManifestListener;Z)V

    return-void
.end method

.method public fetchManifest(Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$ManifestListener;Z)V
    .locals 3

    .line 206
    sget-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_FETCHING_MANIFEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0}, Lhost/exp/exponent/analytics/Analytics;->markEvent(Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    .line 208
    invoke-direct {p0, p1}, Lhost/exp/exponent/ExponentManifest;->httpManifestUrlBuilder(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    if-nez p3, :cond_0

    const-string p3, "cache"

    const-string v1, "false"

    .line 213
    invoke-virtual {v0, p3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 215
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p3

    invoke-virtual {p3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p3

    .line 218
    sget-object v0, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    .line 220
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iget-object v1, p0, Lhost/exp/exponent/ExponentManifest;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    .line 221
    invoke-virtual {v1}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->getSessionSecret()Ljava/lang/String;

    move-result-object v1

    .line 218
    invoke-static {p3, v0, v1}, Lhost/exp/exponent/kernel/ExponentUrls;->addExponentHeadersToManifestUrl(Ljava/lang/String;ZLjava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object p3

    const-string v0, "true"

    const-string v1, "Exponent-Accept-Signature"

    .line 223
    invoke-virtual {p3, v1, v0}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    const-string v1, "Expo-JSON-Error"

    .line 224
    invoke-virtual {p3, v1, v0}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 225
    sget-object v0, Lokhttp3/CacheControl;->FORCE_NETWORK:Lokhttp3/CacheControl;

    invoke-virtual {p3, v0}, Lokhttp3/Request$Builder;->cacheControl(Lokhttp3/CacheControl;)Lokhttp3/Request$Builder;

    .line 227
    sget-object v0, Lhost/exp/exponent/analytics/Analytics$TimedEvent;->STARTED_MANIFEST_NETWORK_REQUEST:Lhost/exp/exponent/analytics/Analytics$TimedEvent;

    invoke-static {v0}, Lhost/exp/exponent/analytics/Analytics;->markEvent(Lhost/exp/exponent/analytics/Analytics$TimedEvent;)V

    .line 232
    invoke-virtual {p3}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object p3

    .line 233
    invoke-virtual {p3}, Lokhttp3/Request;->url()Lokhttp3/HttpUrl;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/HttpUrl;->toString()Ljava/lang/String;

    move-result-object v0

    .line 235
    iget-object v1, p0, Lhost/exp/exponent/ExponentManifest;->mExponentNetwork:Lhost/exp/exponent/network/ExponentNetwork;

    invoke-virtual {v1}, Lhost/exp/exponent/network/ExponentNetwork;->getClient()Lhost/exp/exponent/network/ExponentHttpClient;

    move-result-object v1

    new-instance v2, Lhost/exp/exponent/ExponentManifest$2;

    invoke-direct {v2, p0, p2, p1, v0}, Lhost/exp/exponent/ExponentManifest$2;-><init>(Lhost/exp/exponent/ExponentManifest;Lhost/exp/exponent/ExponentManifest$ManifestListener;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, p3, v2}, Lhost/exp/exponent/network/ExponentHttpClient;->callSafe(Lokhttp3/Request;Lhost/exp/exponent/network/ExponentHttpClient$SafeCallback;)V

    return-void
.end method

.method public getColorFromManifest(Lorg/json/JSONObject;)I
    .locals 1

    const-string v0, "primaryColor"

    .line 652
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 653
    invoke-static {p1}, Lhost/exp/exponent/utils/ColorParser;->isValid(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 654
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 656
    :cond_0
    sget p1, Lhost/exp/expoview/R$color;->colorPrimary:I

    return p1
.end method

.method public getKernelManifest()Lorg/json/JSONObject;
    .locals 4

    .line 697
    iget-object v0, p0, Lhost/exp/exponent/ExponentManifest;->mExponentSharedPreferences:Lhost/exp/exponent/storage/ExponentSharedPreferences;

    invoke-virtual {v0}, Lhost/exp/exponent/storage/ExponentSharedPreferences;->shouldUseInternetKernel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 699
    invoke-direct {p0}, Lhost/exp/exponent/ExponentManifest;->getRemoteKernelManifest()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "Using remote Expo kernel manifest"

    goto :goto_0

    .line 702
    :cond_0
    invoke-direct {p0}, Lhost/exp/exponent/ExponentManifest;->getLocalKernelManifest()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "Using local Expo kernel manifest"

    .line 705
    :goto_0
    sget-boolean v2, Lhost/exp/exponent/ExponentManifest;->hasShownKernelManifestLog:Z

    if-nez v2, :cond_1

    const/4 v2, 0x1

    .line 706
    sput-boolean v2, Lhost/exp/exponent/ExponentManifest;->hasShownKernelManifestLog:Z

    .line 707
    sget-object v2, Lhost/exp/exponent/ExponentManifest;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ": "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lhost/exp/exponent/analytics/EXL;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-object v0
.end method

.method public getKernelManifestField(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 715
    :try_start_0
    invoke-virtual {p0}, Lhost/exp/exponent/ExponentManifest;->getKernelManifest()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 717
    invoke-static {}, Lhost/exp/exponent/kernel/KernelProvider;->getInstance()Lhost/exp/exponent/kernel/KernelInterface;

    move-result-object v0

    invoke-virtual {v0, p1}, Lhost/exp/exponent/kernel/KernelInterface;->handleError(Ljava/lang/Exception;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public loadIconBitmap(Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$BitmapListener;)V
    .locals 1

    if-eqz p1, :cond_1

    .line 598
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 599
    iget-object v0, p0, Lhost/exp/exponent/ExponentManifest;->mMemoryCache:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 601
    invoke-interface {p2, v0}, Lhost/exp/exponent/ExponentManifest$BitmapListener;->onLoadBitmap(Landroid/graphics/Bitmap;)V

    return-void

    .line 605
    :cond_0
    new-instance v0, Lhost/exp/exponent/ExponentManifest$5;

    invoke-direct {v0, p0, p1, p2}, Lhost/exp/exponent/ExponentManifest$5;-><init>(Lhost/exp/exponent/ExponentManifest;Ljava/lang/String;Lhost/exp/exponent/ExponentManifest$BitmapListener;)V

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Void;

    .line 644
    invoke-virtual {v0, p1}, Lhost/exp/exponent/ExponentManifest$5;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 646
    :cond_1
    iget-object p1, p0, Lhost/exp/exponent/ExponentManifest;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lhost/exp/expoview/R$mipmap;->ic_launcher:I

    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 647
    invoke-interface {p2, p1}, Lhost/exp/exponent/ExponentManifest$BitmapListener;->onLoadBitmap(Landroid/graphics/Bitmap;)V

    :goto_0
    return-void
.end method

.method public normalizeManifest(Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const-string v0, "id"

    .line 574
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 575
    invoke-virtual {p2, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    const-string p1, "name"

    .line 578
    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "My New Experience"

    .line 579
    invoke-virtual {p2, p1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    const-string p1, "primaryColor"

    .line 582
    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "#023C69"

    .line 583
    invoke-virtual {p2, p1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_2
    const-string p1, "iconUrl"

    .line 586
    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "https://d3lwq5rlu14cro.cloudfront.net/ExponentEmptyManifest_192.png"

    .line 587
    invoke-virtual {p2, p1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_3
    const-string p1, "orientation"

    .line 590
    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "default"

    .line 591
    invoke-virtual {p2, p1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_4
    return-object p2
.end method
