.class public Lhost/exp/exponent/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhost/exp/exponent/Constants$EmbeddedResponse;,
        Lhost/exp/exponent/Constants$ExpoViewAppConstants;
    }
.end annotation


# static fields
.field public static ABI_VERSIONS:Ljava/lang/String; = null

.field public static ANALYTICS_ENABLED:Z = false

.field public static ANDROID_VERSION_CODE:I = 0x0

.field public static final API_HOST:Ljava/lang/String; = "https://exp.host"

.field public static ARE_REMOTE_UPDATES_ENABLED:Z = true

.field public static final DEBUG_COLD_START_METHOD_TRACING:Z = false

.field public static final DEBUG_MANIFEST_METHOD_TRACING:Z = false

.field public static final DEBUG_METHOD_TRACING:Z = false

.field public static DISABLE_NUX:Z = false

.field public static final EMBEDDED_KERNEL_PATH:Ljava/lang/String; = "assets://kernel.android.bundle"

.field public static EMBEDDED_RESPONSES:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lhost/exp/exponent/Constants$EmbeddedResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final ENABLE_LEAK_CANARY:Z = false

.field public static FCM_ENABLED:Z = false

.field public static INITIAL_URL:Ljava/lang/String; = null

.field public static RELEASE_CHANNEL:Ljava/lang/String; = "default"

.field public static SDK_VERSIONS:Ljava/lang/String; = null

.field public static SDK_VERSIONS_LIST:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final SHELL_APP_EMBEDDED_MANIFEST_PATH:Ljava/lang/String; = "shell-app-manifest.json"

.field public static SHELL_APP_SCHEME:Ljava/lang/String; = null

.field public static SHOW_LOADING_VIEW_IN_SHELL_APP:Z = false

.field private static final TAG:Ljava/lang/String; = "Constants"

.field public static final TEMPORARY_ABI_VERSION:Ljava/lang/String; = "35.0.0"

.field public static VERSION_NAME:Ljava/lang/String; = null

.field public static final WAIT_FOR_DEBUGGER:Z = false

.field public static final WRITE_BUNDLE_TO_LOG:Z = false

.field private static sIsTest:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 67
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const-string v1, "35.0.0"

    .line 84
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 87
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1}, Lhost/exp/exponent/Constants;->setSdkVersions(Ljava/util/List;)V

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 91
    new-instance v1, Lhost/exp/exponent/Constants$EmbeddedResponse;

    const-string v2, "https://exp.host/@exponent/home/bundle"

    const-string v3, "assets://kernel.android.bundle"

    const-string v4, "application/javascript"

    invoke-direct {v1, v2, v3, v4}, Lhost/exp/exponent/Constants$EmbeddedResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "host.exp.exponent.generated.AppConstants"

    .line 99
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "get"

    .line 100
    new-array v4, v1, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v3, 0x0

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhost/exp/exponent/Constants$ExpoViewAppConstants;

    .line 101
    iget-object v3, v2, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->VERSION_NAME:Ljava/lang/String;

    sput-object v3, Lhost/exp/exponent/Constants;->VERSION_NAME:Ljava/lang/String;

    .line 102
    iget-object v3, v2, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->INITIAL_URL:Ljava/lang/String;

    sput-object v3, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    .line 103
    iget-object v3, v2, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->SHELL_APP_SCHEME:Ljava/lang/String;

    sput-object v3, Lhost/exp/exponent/Constants;->SHELL_APP_SCHEME:Ljava/lang/String;

    .line 104
    iget-object v3, v2, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->RELEASE_CHANNEL:Ljava/lang/String;

    sput-object v3, Lhost/exp/exponent/Constants;->RELEASE_CHANNEL:Ljava/lang/String;

    .line 105
    iget-boolean v3, v2, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->SHOW_LOADING_VIEW_IN_SHELL_APP:Z

    sput-boolean v3, Lhost/exp/exponent/Constants;->SHOW_LOADING_VIEW_IN_SHELL_APP:Z

    .line 106
    iget-boolean v3, v2, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->ARE_REMOTE_UPDATES_ENABLED:Z

    sput-boolean v3, Lhost/exp/exponent/Constants;->ARE_REMOTE_UPDATES_ENABLED:Z

    .line 107
    iget v3, v2, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->ANDROID_VERSION_CODE:I

    sput v3, Lhost/exp/exponent/Constants;->ANDROID_VERSION_CODE:I

    .line 108
    iget-boolean v3, v2, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->FCM_ENABLED:Z

    sput-boolean v3, Lhost/exp/exponent/Constants;->FCM_ENABLED:Z

    .line 109
    invoke-static {}, Lhost/exp/exponent/Constants;->isStandaloneApp()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    sput-boolean v3, Lhost/exp/exponent/Constants;->ANALYTICS_ENABLED:Z

    .line 111
    iget-object v2, v2, Lhost/exp/exponent/Constants$ExpoViewAppConstants;->EMBEDDED_RESPONSES:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 112
    sput-object v0, Lhost/exp/exponent/Constants;->EMBEDDED_RESPONSES:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 120
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    .line 118
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    .line 116
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v0

    .line 114
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    .line 162
    :goto_1
    sput-boolean v1, Lhost/exp/exponent/Constants;->sIsTest:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getVersionName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 148
    sget-object v0, Lhost/exp/exponent/Constants;->VERSION_NAME:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    .line 153
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p0

    .line 154
    iget-object p0, p0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 156
    sget-object v0, Lhost/exp/exponent/Constants;->TAG:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lhost/exp/exponent/analytics/EXL;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, ""

    return-object p0
.end method

.method public static isStandaloneApp()Z
    .locals 1

    .line 132
    sget-object v0, Lhost/exp/exponent/Constants;->INITIAL_URL:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static isTest()Z
    .locals 1

    .line 169
    sget-boolean v0, Lhost/exp/exponent/Constants;->sIsTest:Z

    return v0
.end method

.method public static setInTest()V
    .locals 1

    const/4 v0, 0x1

    .line 165
    sput-boolean v0, Lhost/exp/exponent/Constants;->sIsTest:Z

    return-void
.end method

.method public static setSdkVersions(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, ","

    .line 58
    invoke-static {v0, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lhost/exp/exponent/Constants;->ABI_VERSIONS:Ljava/lang/String;

    .line 62
    sget-object v0, Lhost/exp/exponent/Constants;->ABI_VERSIONS:Ljava/lang/String;

    sput-object v0, Lhost/exp/exponent/Constants;->SDK_VERSIONS:Ljava/lang/String;

    .line 63
    sput-object p0, Lhost/exp/exponent/Constants;->SDK_VERSIONS_LIST:Ljava/util/List;

    return-void
.end method
