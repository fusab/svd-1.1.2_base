.class public final Lhost/exp/exponent/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhost/exp/exponent/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f100000

.field public static final abc_action_bar_up_description:I = 0x7f100001

.field public static final abc_action_menu_overflow_description:I = 0x7f100002

.field public static final abc_action_mode_done:I = 0x7f100003

.field public static final abc_activity_chooser_view_see_all:I = 0x7f100004

.field public static final abc_activitychooserview_choose_application:I = 0x7f100005

.field public static final abc_capital_off:I = 0x7f100006

.field public static final abc_capital_on:I = 0x7f100007

.field public static final abc_font_family_body_1_material:I = 0x7f100008

.field public static final abc_font_family_body_2_material:I = 0x7f100009

.field public static final abc_font_family_button_material:I = 0x7f10000a

.field public static final abc_font_family_caption_material:I = 0x7f10000b

.field public static final abc_font_family_display_1_material:I = 0x7f10000c

.field public static final abc_font_family_display_2_material:I = 0x7f10000d

.field public static final abc_font_family_display_3_material:I = 0x7f10000e

.field public static final abc_font_family_display_4_material:I = 0x7f10000f

.field public static final abc_font_family_headline_material:I = 0x7f100010

.field public static final abc_font_family_menu_material:I = 0x7f100011

.field public static final abc_font_family_subhead_material:I = 0x7f100012

.field public static final abc_font_family_title_material:I = 0x7f100013

.field public static final abc_menu_alt_shortcut_label:I = 0x7f100014

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f100015

.field public static final abc_menu_delete_shortcut_label:I = 0x7f100016

.field public static final abc_menu_enter_shortcut_label:I = 0x7f100017

.field public static final abc_menu_function_shortcut_label:I = 0x7f100018

.field public static final abc_menu_meta_shortcut_label:I = 0x7f100019

.field public static final abc_menu_shift_shortcut_label:I = 0x7f10001a

.field public static final abc_menu_space_shortcut_label:I = 0x7f10001b

.field public static final abc_menu_sym_shortcut_label:I = 0x7f10001c

.field public static final abc_prepend_shortcut_label:I = 0x7f10001d

.field public static final abc_search_hint:I = 0x7f10001e

.field public static final abc_searchview_description_clear:I = 0x7f10001f

.field public static final abc_searchview_description_query:I = 0x7f100020

.field public static final abc_searchview_description_search:I = 0x7f100021

.field public static final abc_searchview_description_submit:I = 0x7f100022

.field public static final abc_searchview_description_voice:I = 0x7f100023

.field public static final abc_shareactionprovider_share_with:I = 0x7f100024

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f100025

.field public static final abc_toolbar_collapse_description:I = 0x7f100026

.field public static final adjustable_description:I = 0x7f100027

.field public static final allow_experience_permissions:I = 0x7f100028

.field public static final android_pay_unavaliable:I = 0x7f100029

.field public static final app_name:I = 0x7f10002a

.field public static final appbar_scrolling_view_behavior:I = 0x7f10002b

.field public static final bottom_sheet_behavior:I = 0x7f10002c

.field public static final build_config_package:I = 0x7f10002d

.field public static final cancelButton:I = 0x7f10002e

.field public static final cardNumber:I = 0x7f10002f

.field public static final catalyst_copy_button:I = 0x7f100030

.field public static final catalyst_debugjs:I = 0x7f100031

.field public static final catalyst_debugjs_nuclide:I = 0x7f100032

.field public static final catalyst_debugjs_nuclide_failure:I = 0x7f100033

.field public static final catalyst_debugjs_off:I = 0x7f100034

.field public static final catalyst_dismiss_button:I = 0x7f100035

.field public static final catalyst_element_inspector:I = 0x7f100036

.field public static final catalyst_heap_capture:I = 0x7f100037

.field public static final catalyst_hot_module_replacement:I = 0x7f100038

.field public static final catalyst_hot_module_replacement_off:I = 0x7f100039

.field public static final catalyst_jsload_error:I = 0x7f10003a

.field public static final catalyst_live_reload:I = 0x7f10003b

.field public static final catalyst_live_reload_off:I = 0x7f10003c

.field public static final catalyst_loading_from_url:I = 0x7f10003d

.field public static final catalyst_perf_monitor:I = 0x7f10003e

.field public static final catalyst_perf_monitor_off:I = 0x7f10003f

.field public static final catalyst_poke_sampling_profiler:I = 0x7f100040

.field public static final catalyst_reload_button:I = 0x7f100041

.field public static final catalyst_reloadjs:I = 0x7f100042

.field public static final catalyst_remotedbg_error:I = 0x7f100043

.field public static final catalyst_remotedbg_message:I = 0x7f100044

.field public static final catalyst_report_button:I = 0x7f100045

.field public static final catalyst_settings:I = 0x7f100046

.field public static final catalyst_settings_title:I = 0x7f100047

.field public static final character_counter_content_description:I = 0x7f100048

.field public static final character_counter_pattern:I = 0x7f100049

.field public static final com_crashlytics_android_build_id:I = 0x7f10004a

.field public static final com_facebook_device_auth_instructions:I = 0x7f10004b

.field public static final com_facebook_image_download_unknown_error:I = 0x7f10004c

.field public static final com_facebook_internet_permission_error_message:I = 0x7f10004d

.field public static final com_facebook_internet_permission_error_title:I = 0x7f10004e

.field public static final com_facebook_like_button_liked:I = 0x7f10004f

.field public static final com_facebook_like_button_not_liked:I = 0x7f100050

.field public static final com_facebook_loading:I = 0x7f100051

.field public static final com_facebook_loginview_cancel_action:I = 0x7f100052

.field public static final com_facebook_loginview_log_in_button:I = 0x7f100053

.field public static final com_facebook_loginview_log_in_button_continue:I = 0x7f100054

.field public static final com_facebook_loginview_log_in_button_long:I = 0x7f100055

.field public static final com_facebook_loginview_log_out_action:I = 0x7f100056

.field public static final com_facebook_loginview_log_out_button:I = 0x7f100057

.field public static final com_facebook_loginview_logged_in_as:I = 0x7f100058

.field public static final com_facebook_loginview_logged_in_using_facebook:I = 0x7f100059

.field public static final com_facebook_send_button_text:I = 0x7f10005b

.field public static final com_facebook_share_button_text:I = 0x7f10005c

.field public static final com_facebook_smart_device_instructions:I = 0x7f10005d

.field public static final com_facebook_smart_device_instructions_or:I = 0x7f10005e

.field public static final com_facebook_smart_login_confirmation_cancel:I = 0x7f10005f

.field public static final com_facebook_smart_login_confirmation_continue_as:I = 0x7f100060

.field public static final com_facebook_smart_login_confirmation_title:I = 0x7f100061

.field public static final com_facebook_tooltip_default:I = 0x7f100062

.field public static final common_google_play_services_enable_button:I = 0x7f100063

.field public static final common_google_play_services_enable_text:I = 0x7f100064

.field public static final common_google_play_services_enable_title:I = 0x7f100065

.field public static final common_google_play_services_install_button:I = 0x7f100066

.field public static final common_google_play_services_install_text:I = 0x7f100067

.field public static final common_google_play_services_install_title:I = 0x7f100068

.field public static final common_google_play_services_notification_channel_name:I = 0x7f100069

.field public static final common_google_play_services_notification_ticker:I = 0x7f10006a

.field public static final common_google_play_services_unknown_issue:I = 0x7f10006b

.field public static final common_google_play_services_unsupported_text:I = 0x7f10006c

.field public static final common_google_play_services_update_button:I = 0x7f10006d

.field public static final common_google_play_services_update_text:I = 0x7f10006e

.field public static final common_google_play_services_update_title:I = 0x7f10006f

.field public static final common_google_play_services_updating_text:I = 0x7f100070

.field public static final common_google_play_services_wear_update_text:I = 0x7f100071

.field public static final common_open_on_phone:I = 0x7f100072

.field public static final common_signin_button_text:I = 0x7f100073

.field public static final common_signin_button_text_long:I = 0x7f100074

.field public static final country_code:I = 0x7f100075

.field public static final creditCard_cvcField_placeholder:I = 0x7f100076

.field public static final creditCard_cvcField_title:I = 0x7f100077

.field public static final creditCard_expiryDateField_invalid:I = 0x7f100078

.field public static final creditCard_expiryDateField_month:I = 0x7f100079

.field public static final creditCard_expiryDateField_month_placeholder:I = 0x7f10007a

.field public static final creditCard_expiryDateField_placeholder:I = 0x7f10007b

.field public static final creditCard_expiryDateField_title:I = 0x7f10007c

.field public static final creditCard_expiryDateField_year:I = 0x7f10007d

.field public static final creditCard_expiryDateField_year_placeholder:I = 0x7f10007e

.field public static final creditCard_holderName:I = 0x7f10007f

.field public static final creditCard_holderName_placeholder:I = 0x7f100080

.field public static final creditCard_installmentsField:I = 0x7f100081

.field public static final creditCard_numberField_invalid:I = 0x7f100082

.field public static final creditCard_numberField_placeholder:I = 0x7f100083

.field public static final creditCard_numberField_title:I = 0x7f100084

.field public static final creditCard_oneClickVerification_invalidInput_message:I = 0x7f100085

.field public static final creditCard_oneClickVerification_invalidInput_title:I = 0x7f100086

.field public static final creditCard_oneClickVerification_message:I = 0x7f100087

.field public static final creditCard_oneClickVerification_title:I = 0x7f100088

.field public static final creditCard_storeDetailsButton:I = 0x7f100089

.field public static final creditCard_success:I = 0x7f10008a

.field public static final creditCard_title:I = 0x7f10008b

.field public static final crop_image_activity_no_permissions:I = 0x7f10008c

.field public static final crop_image_activity_title:I = 0x7f10008d

.field public static final crop_image_menu_crop:I = 0x7f10008e

.field public static final crop_image_menu_flip:I = 0x7f10008f

.field public static final crop_image_menu_flip_horizontally:I = 0x7f100090

.field public static final crop_image_menu_flip_vertically:I = 0x7f100091

.field public static final crop_image_menu_rotate_left:I = 0x7f100092

.field public static final crop_image_menu_rotate_right:I = 0x7f100093

.field public static final cvc:I = 0x7f100094

.field public static final default_media_controller_time:I = 0x7f100095

.field public static final default_notification_channel_group:I = 0x7f100096

.field public static final default_web_client_id:I = 0x7f100097

.field public static final deny_experience_permissions:I = 0x7f100098

.field public static final dev_activity_name:I = 0x7f100099

.field public static final dismissButton:I = 0x7f10009a

.field public static final enter_fullscreen_mode:I = 0x7f10009b

.field public static final error_default_client:I = 0x7f10009c

.field public static final error_default_shell:I = 0x7f10009d

.field public static final error_header:I = 0x7f10009e

.field public static final error_message_cannotConnectToHost:I = 0x7f10009f

.field public static final error_message_cannotConnectToInternet:I = 0x7f1000a0

.field public static final error_message_unknown:I = 0x7f1000a1

.field public static final error_retryButton:I = 0x7f1000a2

.field public static final error_subtitle_payment:I = 0x7f1000a3

.field public static final error_subtitle_redirect:I = 0x7f1000a4

.field public static final error_subtitle_refused:I = 0x7f1000a5

.field public static final error_title:I = 0x7f1000a6

.field public static final error_unable_to_load_experience:I = 0x7f1000a7

.field public static final error_uncaught:I = 0x7f1000a8

.field public static final example:I = 0x7f1000a9

.field public static final exit_fullscreen_mode:I = 0x7f1000aa

.field public static final exo_controls_fastforward_description:I = 0x7f1000ab

.field public static final exo_controls_fullscreen_description:I = 0x7f1000ac

.field public static final exo_controls_next_description:I = 0x7f1000ad

.field public static final exo_controls_pause_description:I = 0x7f1000ae

.field public static final exo_controls_play_description:I = 0x7f1000af

.field public static final exo_controls_previous_description:I = 0x7f1000b0

.field public static final exo_controls_repeat_all_description:I = 0x7f1000b1

.field public static final exo_controls_repeat_off_description:I = 0x7f1000b2

.field public static final exo_controls_repeat_one_description:I = 0x7f1000b3

.field public static final exo_controls_rewind_description:I = 0x7f1000b4

.field public static final exo_controls_shuffle_description:I = 0x7f1000b5

.field public static final exo_controls_stop_description:I = 0x7f1000b6

.field public static final exo_download_completed:I = 0x7f1000b7

.field public static final exo_download_description:I = 0x7f1000b8

.field public static final exo_download_downloading:I = 0x7f1000b9

.field public static final exo_download_failed:I = 0x7f1000ba

.field public static final exo_download_notification_channel_name:I = 0x7f1000bb

.field public static final exo_download_removing:I = 0x7f1000bc

.field public static final exo_item_list:I = 0x7f1000bd

.field public static final exo_track_bitrate:I = 0x7f1000be

.field public static final exo_track_mono:I = 0x7f1000bf

.field public static final exo_track_resolution:I = 0x7f1000c0

.field public static final exo_track_selection_auto:I = 0x7f1000c1

.field public static final exo_track_selection_none:I = 0x7f1000c2

.field public static final exo_track_selection_title_audio:I = 0x7f1000c3

.field public static final exo_track_selection_title_text:I = 0x7f1000c4

.field public static final exo_track_selection_title_video:I = 0x7f1000c5

.field public static final exo_track_stereo:I = 0x7f1000c6

.field public static final exo_track_surround:I = 0x7f1000c7

.field public static final exo_track_surround_5_point_1:I = 0x7f1000c8

.field public static final exo_track_surround_7_point_1:I = 0x7f1000c9

.field public static final exo_track_unknown:I = 0x7f1000ca

.field public static final experience_needs_permissions:I = 0x7f1000cb

.field public static final fab_transformation_scrim_behavior:I = 0x7f1000cc

.field public static final fab_transformation_sheet_behavior:I = 0x7f1000cd

.field public static final fcm_fallback_notification_channel_label:I = 0x7f1000ce

.field public static final firebase_database_url:I = 0x7f1000cf

.field public static final gcm_defaultSenderId:I = 0x7f1000d0

.field public static final gcm_fallback_notification_channel_label:I = 0x7f1000d1

.field public static final giropay_minimumLength:I = 0x7f1000d2

.field public static final giropay_noResults:I = 0x7f1000d3

.field public static final giropay_searchField_placeholder:I = 0x7f1000d4

.field public static final google_api_key:I = 0x7f1000d5

.field public static final google_app_id:I = 0x7f1000d6

.field public static final google_crash_reporting_api_key:I = 0x7f1000d7

.field public static final google_storage_bucket:I = 0x7f1000d8

.field public static final header_description:I = 0x7f1000d9

.field public static final hide_bottom_view_on_scroll_behavior:I = 0x7f1000da

.field public static final hint_telehone_number:I = 0x7f1000db

.field public static final holderName:I = 0x7f1000dc

.field public static final idealIssuer_selectField_placeholder:I = 0x7f1000dd

.field public static final idealIssuer_selectField_title:I = 0x7f1000de

.field public static final image_button_description:I = 0x7f1000df

.field public static final image_description:I = 0x7f1000e0

.field public static final info_activity_name:I = 0x7f1000e1

.field public static final info_app_name_placeholder:I = 0x7f1000e2

.field public static final info_clear_data:I = 0x7f1000e3

.field public static final info_hide_manifest:I = 0x7f1000e4

.field public static final info_id:I = 0x7f1000e5

.field public static final info_is_verified:I = 0x7f1000e6

.field public static final info_published_time:I = 0x7f1000e7

.field public static final info_sdk_version:I = 0x7f1000e8

.field public static final info_show_manifest:I = 0x7f1000e9

.field public static final link_description:I = 0x7f1000ea

.field public static final messenger_send_button_text:I = 0x7f1000eb

.field public static final mtrl_chip_close_icon_content_description:I = 0x7f1000ec

.field public static final oneClick_confirmationAlert_title:I = 0x7f1000ed

.field public static final password_toggle_content_description:I = 0x7f1000ee

.field public static final path_password_eye:I = 0x7f1000ef

.field public static final path_password_eye_mask_strike_through:I = 0x7f1000f0

.field public static final path_password_eye_mask_visible:I = 0x7f1000f1

.field public static final path_password_strike_through:I = 0x7f1000f2

.field public static final payButton:I = 0x7f1000f3

.field public static final payButton_formatted:I = 0x7f1000f4

.field public static final paymentMethods_moreMethodsButton:I = 0x7f1000f5

.field public static final paymentMethods_otherMethods:I = 0x7f1000f6

.field public static final paymentMethods_storedMethods:I = 0x7f1000f7

.field public static final paymentMethods_title:I = 0x7f1000f8

.field public static final payment_processing:I = 0x7f1000f9

.field public static final payment_redirecting:I = 0x7f1000fa

.field public static final perm_audio_recording:I = 0x7f1000fb

.field public static final perm_calendar_read:I = 0x7f1000fc

.field public static final perm_calendar_write:I = 0x7f1000fd

.field public static final perm_camera:I = 0x7f1000fe

.field public static final perm_camera_roll_read:I = 0x7f1000ff

.field public static final perm_camera_roll_write:I = 0x7f100100

.field public static final perm_coarse_location:I = 0x7f100101

.field public static final perm_contacts:I = 0x7f100102

.field public static final perm_fine_location:I = 0x7f100103

.field public static final perm_system_brightness:I = 0x7f100104

.field public static final persistent_notification_channel_desc:I = 0x7f100105

.field public static final persistent_notification_channel_group:I = 0x7f100106

.field public static final persistent_notification_channel_name:I = 0x7f100107

.field public static final pick_image_intent_chooser_title:I = 0x7f100108

.field public static final preference_file_key:I = 0x7f100109

.field public static final project_id:I = 0x7f10010a

.field public static final redirect_cannotOpenApp_appNotInstalledMessage:I = 0x7f10010b

.field public static final redirect_cannotOpenApp_title:I = 0x7f10010c

.field public static final save:I = 0x7f10010d

.field public static final search_description:I = 0x7f10010e

.field public static final search_menu_title:I = 0x7f10010f

.field public static final sepaDirectDebit_consentButton:I = 0x7f100110

.field public static final sepaDirectDebit_ibanField_invalid:I = 0x7f100111

.field public static final sepaDirectDebit_ibanField_placeholder:I = 0x7f100112

.field public static final sepaDirectDebit_ibanField_title:I = 0x7f100113

.field public static final sepaDirectDebit_nameField_placeholder:I = 0x7f100114

.field public static final sepaDirectDebit_nameField_title:I = 0x7f100115

.field public static final sepa_ibanNumber:I = 0x7f100116

.field public static final sepa_ownerName:I = 0x7f100117

.field public static final status_bar_notification_info_overflow:I = 0x7f100118

.field public static final storeDetails:I = 0x7f100119

.field public static final telephone_number:I = 0x7f10011a

.field public static final user_cancel_dialog:I = 0x7f10011b

.field public static final view_error_log:I = 0x7f10011c

.field public static final wallet_buy_button_place_holder:I = 0x7f10011d


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
