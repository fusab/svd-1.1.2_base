.class abstract Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;
.super Ljava/lang/Object;
.source "ConnectivityReceiver.java"


# static fields
.field static final CELLULAR_GENERATION_2G:Ljava/lang/String; = "2g"

.field static final CELLULAR_GENERATION_3G:Ljava/lang/String; = "3g"

.field static final CELLULAR_GENERATION_4G:Ljava/lang/String; = "4g"

.field static final CONNECTION_TYPE_BLUETOOTH:Ljava/lang/String; = "bluetooth"

.field static final CONNECTION_TYPE_CELLULAR:Ljava/lang/String; = "cellular"

.field static final CONNECTION_TYPE_ETHERNET:Ljava/lang/String; = "ethernet"

.field static final CONNECTION_TYPE_NONE:Ljava/lang/String; = "none"

.field static final CONNECTION_TYPE_OTHER:Ljava/lang/String; = "other"

.field static final CONNECTION_TYPE_UNKNOWN:Ljava/lang/String; = "unknown"

.field static final CONNECTION_TYPE_VPN:Ljava/lang/String; = "vpn"

.field static final CONNECTION_TYPE_WIFI:Ljava/lang/String; = "wifi"

.field static final CONNECTION_TYPE_WIMAX:Ljava/lang/String; = "wimax"

.field static final ERROR_MISSING_PERMISSION:Ljava/lang/String; = "E_MISSING_PERMISSION"

.field static final MISSING_PERMISSION_MESSAGE:Ljava/lang/String; = "To use NetInfo on Android, add the following to your AndroidManifest.xml:\n<uses-permission android:name=\"android.permission.ACCESS_NETWORK_STATE\" />"


# instance fields
.field private mCellularGeneration:Ljava/lang/String;

.field private mConnectionType:Ljava/lang/String;

.field private final mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mNoNetworkPermission:Z

.field private final mReactContext:Lcom/facebook/react/bridge/ReactApplicationContext;


# direct methods
.method constructor <init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V
    .locals 1

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 49
    iput-boolean v0, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mNoNetworkPermission:Z

    const-string v0, "unknown"

    .line 50
    iput-object v0, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mConnectionType:Ljava/lang/String;

    const/4 v0, 0x0

    .line 51
    iput-object v0, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mCellularGeneration:Ljava/lang/String;

    .line 54
    iput-object p1, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mReactContext:Lcom/facebook/react/bridge/ReactApplicationContext;

    const-string v0, "connectivity"

    .line 56
    invoke-virtual {p1, v0}, Lcom/facebook/react/bridge/ReactApplicationContext;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/ConnectivityManager;

    iput-object p1, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mConnectivityManager:Landroid/net/ConnectivityManager;

    return-void
.end method

.method private createConnectivityEventMap()Lcom/facebook/react/bridge/WritableMap;
    .locals 4

    .line 138
    new-instance v0, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v0}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 141
    iget-object v1, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mConnectionType:Ljava/lang/String;

    const-string v2, "type"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-object v1, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mConnectionType:Ljava/lang/String;

    const-string v2, "none"

    .line 145
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mConnectionType:Ljava/lang/String;

    const-string v2, "unknown"

    .line 146
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "isConnected"

    .line 147
    invoke-interface {v0, v2, v1}, Lcom/facebook/react/bridge/WritableMap;->putBoolean(Ljava/lang/String;Z)V

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 152
    new-instance v2, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v2}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 155
    invoke-virtual {p0}, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->getConnectivityManager()Landroid/net/ConnectivityManager;

    move-result-object v1

    invoke-static {v1}, Landroidx/core/net/ConnectivityManagerCompat;->isActiveNetworkMetered(Landroid/net/ConnectivityManager;)Z

    move-result v1

    const-string v3, "isConnectionExpensive"

    .line 156
    invoke-interface {v2, v3, v1}, Lcom/facebook/react/bridge/WritableMap;->putBoolean(Ljava/lang/String;Z)V

    .line 158
    iget-object v1, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mConnectionType:Ljava/lang/String;

    const-string v3, "cellular"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 159
    iget-object v1, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mCellularGeneration:Ljava/lang/String;

    const-string v3, "cellularGeneration"

    invoke-interface {v2, v3, v1}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v1, "details"

    .line 162
    invoke-interface {v0, v1, v2}, Lcom/facebook/react/bridge/WritableMap;->putMap(Ljava/lang/String;Lcom/facebook/react/bridge/WritableMap;)V

    return-object v0
.end method

.method private sendConnectivityChangedEvent()V
    .locals 3

    .line 132
    invoke-virtual {p0}, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->getReactContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    const-class v1, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    .line 133
    invoke-virtual {v0, v1}, Lcom/facebook/react/bridge/ReactApplicationContext;->getJSModule(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    .line 134
    invoke-direct {p0}, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->createConnectivityEventMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v1

    const-string v2, "netInfo.networkStatusDidChange"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getConnectivityManager()Landroid/net/ConnectivityManager;
    .locals 1

    .line 76
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mConnectivityManager:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method public getCurrentState(Lcom/facebook/react/bridge/Promise;)V
    .locals 2

    .line 64
    iget-boolean v0, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mNoNetworkPermission:Z

    if-eqz v0, :cond_0

    const-string v0, "E_MISSING_PERMISSION"

    const-string v1, "To use NetInfo on Android, add the following to your AndroidManifest.xml:\n<uses-permission android:name=\"android.permission.ACCESS_NETWORK_STATE\" />"

    .line 65
    invoke-interface {p1, v0, v1}, Lcom/facebook/react/bridge/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 68
    :cond_0
    invoke-direct {p0}, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->createConnectivityEventMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    return-void
.end method

.method getEffectiveConnectionType(Landroid/net/NetworkInfo;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 88
    :cond_0
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result p1

    packed-switch p1, :pswitch_data_0

    return-object v0

    :pswitch_0
    const-string p1, "4g"

    return-object p1

    :pswitch_1
    const-string p1, "3g"

    return-object p1

    :pswitch_2
    const-string p1, "2g"

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getReactContext()Lcom/facebook/react/bridge/ReactApplicationContext;
    .locals 1

    .line 72
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mReactContext:Lcom/facebook/react/bridge/ReactApplicationContext;

    return-object v0
.end method

.method abstract register()V
.end method

.method public setNoNetworkPermission()V
    .locals 1

    const/4 v0, 0x1

    .line 80
    iput-boolean v0, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mNoNetworkPermission:Z

    return-void
.end method

.method abstract unregister()V
.end method

.method updateConnectivity(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    .line 116
    iget-object v2, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mConnectionType:Ljava/lang/String;

    if-nez v2, :cond_1

    :cond_0
    if-eqz p1, :cond_2

    iget-object v2, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mConnectionType:Ljava/lang/String;

    .line 119
    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    if-nez p2, :cond_3

    .line 120
    iget-object v3, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mCellularGeneration:Ljava/lang/String;

    if-nez v3, :cond_5

    :cond_3
    if-eqz p2, :cond_4

    iget-object v3, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mCellularGeneration:Ljava/lang/String;

    .line 123
    invoke-virtual {p2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    :cond_5
    :goto_1
    if-nez v2, :cond_6

    if-eqz v0, :cond_7

    .line 125
    :cond_6
    iput-object p1, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mConnectionType:Ljava/lang/String;

    .line 126
    iput-object p2, p0, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->mCellularGeneration:Ljava/lang/String;

    .line 127
    invoke-direct {p0}, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->sendConnectivityChangedEvent()V

    :cond_7
    return-void
.end method
