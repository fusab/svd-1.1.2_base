.class public Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver;
.super Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;
.source "BroadcastReceiverConnectivityReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver$ConnectivityBroadcastReceiver;
    }
.end annotation


# instance fields
.field private final mConnectivityBroadcastReceiver:Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver$ConnectivityBroadcastReceiver;


# direct methods
.method public constructor <init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V
    .locals 1

    .line 32
    invoke-direct {p0, p1}, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    .line 33
    new-instance p1, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver$ConnectivityBroadcastReceiver;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver$ConnectivityBroadcastReceiver;-><init>(Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver;Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver$1;)V

    iput-object p1, p0, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver;->mConnectivityBroadcastReceiver:Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver$ConnectivityBroadcastReceiver;

    return-void
.end method

.method static synthetic access$100(Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver;->updateAndSendConnectionType()V

    return-void
.end method

.method private updateAndSendConnectionType()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingPermission"
        }
    .end annotation

    const/4 v0, 0x0

    .line 59
    :try_start_0
    invoke-virtual {p0}, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver;->getConnectivityManager()Landroid/net/ConnectivityManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 60
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 63
    :cond_0
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v2
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_6

    const/4 v3, 0x1

    if-eq v2, v3, :cond_5

    const/4 v3, 0x4

    if-eq v2, v3, :cond_6

    const/16 v1, 0x9

    if-eq v2, v1, :cond_4

    const/16 v1, 0x11

    if-eq v2, v1, :cond_3

    const/4 v1, 0x6

    if-eq v2, v1, :cond_2

    const/4 v1, 0x7

    if-eq v2, v1, :cond_1

    const-string v1, "other"

    goto :goto_1

    :cond_1
    :try_start_1
    const-string v1, "bluetooth"

    goto :goto_1

    :cond_2
    const-string v1, "wimax"

    goto :goto_1

    :cond_3
    const-string v1, "vpn"

    goto :goto_1

    :cond_4
    const-string v1, "ethernet"

    goto :goto_1

    :cond_5
    const-string v1, "wifi"

    goto :goto_1

    :cond_6
    const-string v2, "cellular"

    .line 74
    invoke-virtual {p0, v1}, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver;->getEffectiveConnectionType(Landroid/net/NetworkInfo;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v2

    goto :goto_1

    :cond_7
    :goto_0
    const-string v1, "none"
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 87
    :catch_0
    invoke-virtual {p0}, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver;->setNoNetworkPermission()V

    const-string v1, "unknown"

    .line 91
    :goto_1
    invoke-virtual {p0, v1, v0}, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver;->updateConnectivity(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getConnectivityManager()Landroid/net/ConnectivityManager;
    .locals 1

    .line 27
    invoke-super {p0}, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->getConnectivityManager()Landroid/net/ConnectivityManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getCurrentState(Lcom/facebook/react/bridge/Promise;)V
    .locals 0

    .line 27
    invoke-super {p0, p1}, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->getCurrentState(Lcom/facebook/react/bridge/Promise;)V

    return-void
.end method

.method public bridge synthetic getReactContext()Lcom/facebook/react/bridge/ReactApplicationContext;
    .locals 1

    .line 27
    invoke-super {p0}, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->getReactContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    return-object v0
.end method

.method public register()V
    .locals 3

    .line 38
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    .line 39
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0}, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver;->getReactContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v1

    iget-object v2, p0, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver;->mConnectivityBroadcastReceiver:Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver$ConnectivityBroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/react/bridge/ReactApplicationContext;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 41
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver;->mConnectivityBroadcastReceiver:Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver$ConnectivityBroadcastReceiver;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver$ConnectivityBroadcastReceiver;->setRegistered(Z)V

    .line 42
    invoke-direct {p0}, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver;->updateAndSendConnectionType()V

    return-void
.end method

.method public bridge synthetic setNoNetworkPermission()V
    .locals 0

    .line 27
    invoke-super {p0}, Lversioned/host/exp/exponent/modules/api/netinfo/ConnectivityReceiver;->setNoNetworkPermission()V

    return-void
.end method

.method public unregister()V
    .locals 2

    .line 47
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver;->mConnectivityBroadcastReceiver:Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver$ConnectivityBroadcastReceiver;

    invoke-virtual {v0}, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver$ConnectivityBroadcastReceiver;->isRegistered()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {p0}, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver;->getReactContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    iget-object v1, p0, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver;->mConnectivityBroadcastReceiver:Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver$ConnectivityBroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/facebook/react/bridge/ReactApplicationContext;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 49
    iget-object v0, p0, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver;->mConnectivityBroadcastReceiver:Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver$ConnectivityBroadcastReceiver;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lversioned/host/exp/exponent/modules/api/netinfo/BroadcastReceiverConnectivityReceiver$ConnectivityBroadcastReceiver;->setRegistered(Z)V

    :cond_0
    return-void
.end method
